.class public Lcom/samsung/sec/android/application/csc/CscVerifierLog;
.super Ljava/lang/Object;
.source "CscVerifierLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;
    }
.end annotation


# static fields
.field private static final LOG_FILE_PATH:Ljava/lang/String;

.field private static sCscVerifierLog:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

.field private static sEnableLog:Z


# instance fields
.field mCSCEdition:Ljava/lang/String;

.field mCscVersion:Ljava/lang/String;

.field mCscXMLParser:Lcom/samsung/sec/android/application/csc/CscXMLParser;

.field private mFailLog:Ljava/lang/String;

.field mFullTagSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLog:Ljava/lang/String;

.field private mLogFailItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLogItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;",
            ">;"
        }
    .end annotation
.end field

.field private mNotSupportedItemMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mPdaVersion:Ljava/lang/String;

.field mSalesCode:Ljava/lang/String;

.field private mSummaryPassMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSummaryTotalMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mUnusedItemSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mbSavingLog:Z

.field private mnFailLogCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/CscVerifierLog.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->LOG_FILE_PATH:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sCscVerifierLog:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    .line 41
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sEnableLog:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mbSavingLog:Z

    .line 46
    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mnFailLogCount:I

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLog:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mFailLog:Ljava/lang/String;

    .line 52
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogItems:Ljava/util/List;

    .line 54
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogFailItems:Ljava/util/List;

    .line 56
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryTotalMap:Ljava/util/Map;

    .line 58
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryPassMap:Ljava/util/Map;

    .line 60
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCscXMLParser:Lcom/samsung/sec/android/application/csc/CscXMLParser;

    .line 62
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    .line 64
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    .line 66
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mPdaVersion:Ljava/lang/String;

    .line 68
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCscVersion:Ljava/lang/String;

    .line 70
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSalesCode:Ljava/lang/String;

    .line 72
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCSCEdition:Ljava/lang/String;

    .line 74
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mFullTagSet:Ljava/util/Set;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogItems:Ljava/util/List;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogFailItems:Ljava/util/List;

    .line 104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryTotalMap:Ljava/util/Map;

    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryPassMap:Ljava/util/Map;

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    .line 108
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    .line 110
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->initCscVerifierLog()V

    .line 111
    return-void
.end method

.method private _endLog(ZLjava/lang/String;)V
    .locals 4
    .param p1, "success"    # Z
    .param p2, "failResultStr"    # Ljava/lang/String;

    .prologue
    .line 288
    const-string v1, "----- Verify Compare Result for Customer.xml -----"

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 289
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getLogFailItems()Ljava/lang/String;

    move-result-object v0

    .line 290
    .local v0, "failItem":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 291
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printLogFailItems()V

    .line 292
    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->addLog(Ljava/lang/String;)V

    .line 297
    :goto_0
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printNotSupportedItems()V

    .line 298
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printUnusedItems()V

    .line 299
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printNewOrUnidentifiedItems()V

    .line 302
    const-string v1, "--------- Verify Log ---------\n"

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->addLog(Ljava/lang/String;)V

    .line 303
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getLogItems()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->addLog(Ljava/lang/String;)V

    .line 305
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printSummary()V

    .line 308
    const-string v1, ""

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 309
    const-string v1, "--------- Verify Result (Only for developer) ---------"

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 310
    if-eqz p1, :cond_2

    .line 311
    const-string v1, "CscVerifierLog"

    const-string v2, "endLog() Result : PASS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    const-string v1, "PASS"

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 315
    const-string v1, "Succeed"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextView(Ljava/lang/String;I)V

    .line 316
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->isEnableUI()Z

    move-result v1

    if-nez v1, :cond_0

    .line 317
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->saveLogFile()V

    .line 338
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->endLog()V

    .line 339
    return-void

    .line 294
    :cond_1
    const-string v1, "No Fail"

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    goto :goto_0

    .line 319
    :cond_2
    const-string v1, "CscVerifierLog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "endLog() Result : FAIL, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FAIL : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextView(Ljava/lang/String;I)V

    .line 322
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FAIL : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->addLog(Ljava/lang/String;)V

    .line 332
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mFailLog:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLog(Ljava/lang/String;)V

    .line 334
    const-string v1, "Failed"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextView(Ljava/lang/String;I)V

    .line 335
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->saveLogFile()V

    goto :goto_1
.end method

.method private _startLog()V
    .locals 2

    .prologue
    .line 263
    const-string v0, "CscVerifierLog"

    const-string v1, "startLog"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->deleteLogFile()V

    .line 266
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->clearLog()V

    .line 268
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sEnableLog:Z

    .line 269
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mnFailLogCount:I

    .line 271
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printVersionInfo()V

    .line 272
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->compare()V

    .line 273
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->LOG_FILE_PATH:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/sec/android/application/csc/CscVerifierLog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLog:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/sec/android/application/csc/CscVerifierLog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->clearLog()V

    return-void
.end method

.method static synthetic access$302(Lcom/samsung/sec/android/application/csc/CscVerifierLog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscVerifierLog;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mbSavingLog:Z

    return p1
.end method

.method private addFailLog(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 684
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mFailLog:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mnFailLogCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mnFailLogCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mFailLog:Ljava/lang/String;

    .line 685
    return-void
.end method

.method public static addFailLogLine(Ljava/lang/String;)V
    .locals 3
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 678
    sget-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sEnableLog:Z

    if-eqz v0, :cond_0

    .line 679
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->addFailLog(Ljava/lang/String;)V

    .line 681
    :cond_0
    return-void
.end method

.method private addLog(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 672
    sget-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sEnableLog:Z

    if-eqz v0, :cond_0

    .line 673
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLog:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLog:Ljava/lang/String;

    .line 675
    :cond_0
    return-void
.end method

.method private addLogItem(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "bPass"    # Z
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "vaule"    # Ljava/lang/String;
    .param p4, "phoneValue"    # Ljava/lang/String;

    .prologue
    .line 607
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    .local v0, "logItem":Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogItems:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 609
    if-nez p1, :cond_0

    .line 610
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogFailItems:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 612
    :cond_0
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    invoke-interface {v5, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 615
    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mTag:Ljava/lang/String;

    const-string v6, "\\."

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 616
    .local v3, "tagStrs":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 617
    .local v2, "tagStr":Ljava/lang/String;
    const/4 v1, 0x0

    .line 622
    .local v1, "strIndex":I
    array-length v5, v3

    if-lez v5, :cond_4

    .line 623
    const-string v5, "Settings"

    aget-object v6, v3, v1

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 624
    add-int/lit8 v1, v1, 0x1

    .line 626
    :cond_1
    array-length v5, v3

    if-ge v1, v5, :cond_2

    .line 627
    aget-object v2, v3, v1

    .line 628
    :cond_2
    add-int/lit8 v1, v1, 0x1

    array-length v5, v3

    if-ge v1, v5, :cond_3

    .line 629
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 631
    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 634
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryTotalMap:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 635
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryTotalMap:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v4, v5, 0x1

    .line 636
    .local v4, "value":I
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryTotalMap:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 642
    .end local v4    # "value":I
    :goto_0
    iget-boolean v5, v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mbPass:Z

    if-eqz v5, :cond_4

    .line 643
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryPassMap:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v4, v5, 0x1

    .line 644
    .restart local v4    # "value":I
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryPassMap:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 648
    .end local v4    # "value":I
    :cond_4
    return-void

    .line 638
    :cond_5
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryTotalMap:Ljava/util/Map;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 639
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryPassMap:Ljava/util/Map;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private clearLog()V
    .locals 2

    .prologue
    .line 350
    const-string v0, "CscVerifierLog"

    const-string v1, "clearLog"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 353
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogFailItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 354
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryTotalMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 355
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryPassMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 357
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLog:Ljava/lang/String;

    .line 358
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mFailLog:Ljava/lang/String;

    .line 359
    return-void
.end method

.method private compare()V
    .locals 6

    .prologue
    .line 217
    const-string v1, "GeneralInfo.CSCEdition"

    .line 218
    .local v1, "tag":Ljava/lang/String;
    const/4 v0, 0x0

    .line 219
    .local v0, "phone":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getCustomerXmlParser()Lcom/samsung/sec/android/application/csc/CscXMLParser;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/sec/android/application/csc/CscXMLParser;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 220
    .local v2, "xml":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 221
    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :goto_0
    const-string v1, "GeneralInfo.Region"

    .line 227
    const/4 v0, 0x0

    .line 228
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getCustomerXmlParser()Lcom/samsung/sec/android/application/csc/CscXMLParser;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/sec/android/application/csc/CscXMLParser;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 229
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 230
    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_0
    const-string v1, "GeneralInfo.SalesCode"

    .line 234
    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 235
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getCustomerXmlParser()Lcom/samsung/sec/android/application/csc/CscXMLParser;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/sec/android/application/csc/CscXMLParser;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 236
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 237
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3, v1, v2, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_1
    const-string v1, "GeneralInfo.Country"

    .line 241
    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "&amp;"

    const-string v5, "&"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 242
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getCustomerXmlParser()Lcom/samsung/sec/android/application/csc/CscXMLParser;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/sec/android/application/csc/CscXMLParser;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 243
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 244
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3, v1, v2, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_2
    const-string v1, "GeneralInfo.CountryISO"

    .line 248
    const-string v3, "ro.csc.countryiso_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 249
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getCustomerXmlParser()Lcom/samsung/sec/android/application/csc/CscXMLParser;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/sec/android/application/csc/CscXMLParser;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 250
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 251
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3, v1, v2, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :cond_3
    return-void

    .line 223
    :cond_4
    invoke-static {v1, v2, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteLogFile()V
    .locals 3

    .prologue
    .line 510
    const-string v1, "CscVerifierLog"

    const-string v2, "deleteLogFile"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->LOG_FILE_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 512
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 513
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 514
    const-string v1, "CscVerifierLog"

    const-string v2, "Failed to delete the log file."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    :cond_0
    return-void
.end method

.method private endLog()V
    .locals 2

    .prologue
    .line 342
    const-string v0, "CscVerifierLog"

    const-string v1, "endLog"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->endUI()V

    .line 344
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sEnableLog:Z

    .line 345
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCscXMLParser:Lcom/samsung/sec/android/application/csc/CscXMLParser;

    .line 347
    return-void
.end method

.method public static endLog(ZLjava/lang/String;)V
    .locals 1
    .param p0, "success"    # Z
    .param p1, "failResultStr"    # Ljava/lang/String;

    .prologue
    .line 276
    sget-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sEnableLog:Z

    if-eqz v0, :cond_0

    .line 277
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    move-result-object v0

    invoke-direct {v0, p0, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->_endLog(ZLjava/lang/String;)V

    .line 279
    :cond_0
    return-void
.end method

.method public static fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "tagPath"    # Ljava/lang/String;
    .param p1, "xmlValue"    # Ljava/lang/String;
    .param p2, "phoneVale"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 710
    sget-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sEnableLog:Z

    if-eqz v0, :cond_0

    .line 711
    const-string v0, "[Fail]"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextView(Ljava/lang/String;I)V

    .line 712
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Xml:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Phone:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextViewLine(Ljava/lang/String;I)V

    .line 715
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    move-result-object v0

    invoke-direct {v0, v2, p0, p1, p2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->addLogItem(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    :cond_0
    return-void
.end method

.method private getCustomerXmlParser()Lcom/samsung/sec/android/application/csc/CscXMLParser;
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCscXMLParser:Lcom/samsung/sec/android/application/csc/CscXMLParser;

    if-nez v0, :cond_0

    .line 206
    const-string v0, "CscVerifierLog"

    const-string v1, "CscVerifierLog.getCustomerXmlParser() : new CscXMLParser()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscXMLParser;

    const-string v1, "/system/csc/customer.xml"

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCscXMLParser:Lcom/samsung/sec/android/application/csc/CscXMLParser;

    .line 208
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCscXMLParser:Lcom/samsung/sec/android/application/csc/CscXMLParser;

    invoke-virtual {v0}, Lcom/samsung/sec/android/application/csc/CscXMLParser;->openXML()V

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCscXMLParser:Lcom/samsung/sec/android/application/csc/CscXMLParser;

    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierLog;
    .locals 3

    .prologue
    .line 94
    const-class v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sCscVerifierLog:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    if-nez v0, :cond_0

    .line 95
    const-string v0, "CscVerifierLog"

    const-string v2, "CscVerifierLog.getInstance() : new CscVerifierLog()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    invoke-direct {v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;-><init>()V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sCscVerifierLog:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    .line 98
    :cond_0
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sCscVerifierLog:Lcom/samsung/sec/android/application/csc/CscVerifierLog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getLogFailItems()Ljava/lang/String;
    .locals 6

    .prologue
    .line 575
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 576
    .local v3, "logStr":Ljava/lang/StringBuffer;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogFailItems:Ljava/util/List;

    .line 578
    .local v2, "logItemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;>;"
    if-nez v2, :cond_0

    .line 579
    const-string v4, ""

    .line 585
    :goto_0
    return-object v4

    .line 581
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;

    .line 582
    .local v1, "logItem":Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fail;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mTag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mValue:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mPhoneValue:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 585
    .end local v1    # "logItem":Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private getLogItemSet()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 651
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 652
    .local v3, "logItemSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogItems:Ljava/util/List;

    .line 654
    .local v2, "logItemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;>;"
    if-eqz v2, :cond_0

    .line 655
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;

    .line 656
    .local v1, "logItem":Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;
    iget-object v4, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mTag:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 659
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "logItem":Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;
    :cond_0
    return-object v3
.end method

.method private getLogItems()Ljava/lang/String;
    .locals 6

    .prologue
    .line 589
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 590
    .local v3, "logStr":Ljava/lang/StringBuffer;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogItems:Ljava/util/List;

    .line 592
    .local v2, "logItemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;>;"
    if-nez v2, :cond_0

    .line 593
    const-string v4, ""

    .line 603
    :goto_0
    return-object v4

    .line 595
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;

    .line 596
    .local v1, "logItem":Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;
    iget-boolean v4, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mbPass:Z

    if-eqz v4, :cond_1

    .line 597
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Pass;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mTag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mValue:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 599
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fail;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mTag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mValue:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mPhoneValue:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 603
    .end local v1    # "logItem":Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private initCscVerifierLog()V
    .locals 3

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Main.Phone.DefLanguageNoSIM"

    const-string v2, "Main Settings/Phone Settings/Setting/Phone Language(No SIM)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Main.Phone.DefLanguage"

    const-string v2, "Main Settings/Phone Settings/Setting/Phone Language"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Main.Network.SOSNumber"

    const-string v2, "Main Settings/Phone Settings/Emergence Call Number"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Main.Network.SOSNumberNoSIM"

    const-string v2, "Main Settings/Phone Settings/Emergence Call Number(NoSIM)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Messages.SMS.SSMS"

    const-string v2, "Main Settings/SMS/message/SSMS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Messages.SMS.ImeiTracker"

    const-string v2, "Main Settings/SMS/message/IMEI Tracker"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Messages.SMS.TextTemplate"

    const-string v2, "Main Settings/SMS/message/Text Template"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Messages.Voicemail.VoicemailName"

    const-string v2, "Main Settings/Voicemail/Voicemail Name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Messages.Voicemail.NetworkName"

    const-string v2, "Main Settings/Voicemail/Network Name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Messages.Voicemail.TelNum"

    const-string v2, "Main Settings/Voicemail/Voicemail Number"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.GPS.AGPS.TLS"

    const-string v2, "AGPS/Connection/GPS/TLS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.GPS.AGPS.ServAddr"

    const-string v2, "AGPS/Connection/GPS/Server"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.GPS.AGPS.Port"

    const-string v2, "AGPS/Connection/GPS/Port"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.GPS.AGPS.SuplVer"

    const-string v2, "AGPS/Connection/GPS/SuplVer"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.GPS.AGPS.LPP"

    const-string v2, "AGPS/Connection/GPS/LPP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Main.Sound.MessageTone"

    const-string v2, "Customer Data/Sound/message/Message Tone"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Main.Network.AutoAttach"

    const-string v2, "Main Settings/Phone Settings/PS Auto Attach"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Main.Sound.AMRVoiceCodec"

    const-string v2, "Main Settings/Sound Settings/AMR Voice Codec"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Main.Security.NetworkLock"

    const-string v2, "Main Settings/Security/Network Lock"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Main.Security.SPLock"

    const-string v2, "Main Settings/Security/SP Lock"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Main.Security.UnlockCnt"

    const-string v2, "Main Settings/Security/Unlock Count"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.WifiProfile.NetworkName"

    const-string v2, "Connectivity/Wifi/Network Name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.WifiProfile.WifiSSID"

    const-string v2, "Connectivity/Wifi/SSID"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.WifiProfile.WifiKeyMgmt"

    const-string v2, "Connectivity/Wifi/Key Management"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.WifiProfile.WifiEAPMethod"

    const-string v2, "Connectivity/Wifi/EAP Method"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.WifiProfile.WifiPriority"

    const-string v2, "Connectivity/Wifi/Priority"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    const-string v1, "Settings.Wifi.CheckInternet"

    const-string v2, "Main Settings/Wifi/Check Internet"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Network.AutoLink"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 160
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.BT.Activation"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.BT.Visibility"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Network.FastDormancy.LCDStatus"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Network.FastDormancy.NetworkName"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Network.FastDormancy.TimeoutValue"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 165
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Network.FastDormancy.Version"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Network.WbAmr.NetworkName"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Messages.Email.Account.AccountName"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 168
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Messages.Email.Account.EmailAddr"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 169
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Messages.Email.Account.Incoming.MailboxType"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Messages.Email.Account.Incoming.Port"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Messages.Email.Account.Incoming.Secure"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Messages.Email.Account.Incoming.ServAddr"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Messages.Email.Account.NetworkName"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 174
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Messages.Email.Account.Outgoing.Port"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 175
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Messages.Email.Account.Outgoing.Secure"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 176
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Messages.Email.Account.Outgoing.ServAddr"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 177
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Messages.Email.Account.Outgoing.SmtpAuth"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 178
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Connections.Profile.PSparam.TrafficClass"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 179
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Network.MaxPDP.NetworkName"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 180
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Display.Wallpaper.FileType"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Display.Wallpaper.Position"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 182
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Sound.RingTone.FileType"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 183
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Sound.RingTone.Position"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 184
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Sound.PowerOnTone.FileType"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 185
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Sound.PowerOffTone.FileType"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 186
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Sound.MessageTone.FileType"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 187
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Sound.MessageTone.Position"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Security.OneSIMLock"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 189
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.DateDelimiter"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 190
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.TimeDelimiter"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 191
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Connections.ProfileHandle.ProfChatOn"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 192
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.Main.Display.LockScreenBG.FileType"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 193
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    const-string v1, "Settings.GPS.GPSMode"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 196
    const-string v0, "ro.build.PDA"

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mPdaVersion:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mPdaVersion:Ljava/lang/String;

    .line 197
    const-string v0, "ril.official_cscver"

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCscVersion:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCscVersion:Ljava/lang/String;

    .line 198
    const-string v0, "ro.csc.sales_code"

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSalesCode:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSalesCode:Ljava/lang/String;

    .line 199
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getCustomerXmlParser()Lcom/samsung/sec/android/application/csc/CscXMLParser;

    move-result-object v0

    const-string v1, "GeneralInfo.CSCEdition"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLParser;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCSCEdition:Ljava/lang/String;

    .line 201
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getCustomerXmlParser()Lcom/samsung/sec/android/application/csc/CscXMLParser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/sec/android/application/csc/CscXMLParser;->getFullTagSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mFullTagSet:Ljava/util/Set;

    .line 202
    return-void
.end method

.method public static isEnableLog()Z
    .locals 1

    .prologue
    .line 362
    sget-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sEnableLog:Z

    return v0
.end method

.method private isUnusedItems(Ljava/lang/String;)Z
    .locals 6
    .param p1, "tagPath"    # Ljava/lang/String;

    .prologue
    .line 423
    const/4 v0, 0x0

    .line 426
    .local v0, "result":Z
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 427
    const/16 v4, 0x2e

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v2, v4, 0x1

    .line 428
    .local v2, "tagNameIdx":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 429
    .local v3, "tagPathLen":I
    sub-int v4, v3, v2

    const/4 v5, 0x2

    if-le v4, v5, :cond_0

    .line 430
    add-int/lit8 v4, v2, 0x2

    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 431
    .local v1, "tagNameFirstTwoCh":Ljava/lang/String;
    const-string v4, "Nb"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 432
    const/4 v0, 0x1

    .line 436
    .end local v1    # "tagNameFirstTwoCh":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 437
    const/4 v0, 0x1

    .line 440
    .end local v2    # "tagNameIdx":I
    .end local v3    # "tagPathLen":I
    :cond_1
    return v0
.end method

.method public static log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "bPass"    # Z
    .param p1, "tagPath"    # Ljava/lang/String;
    .param p2, "xmlValue"    # Ljava/lang/String;
    .param p3, "phoneVale"    # Ljava/lang/String;

    .prologue
    .line 688
    sget-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sEnableLog:Z

    if-eqz v0, :cond_0

    .line 689
    if-eqz p0, :cond_1

    .line 690
    invoke-static {p1, p2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    :cond_0
    :goto_0
    return-void

    .line 692
    :cond_1
    invoke-static {p1, p2, p3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static pass(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "tagPath"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 699
    sget-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sEnableLog:Z

    if-eqz v0, :cond_0

    .line 700
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Pass]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Value:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextViewLine(Ljava/lang/String;I)V

    .line 703
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, p1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->addLogItem(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    :cond_0
    return-void
.end method

.method private printAndAddLog(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 667
    invoke-direct {p0, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->addLog(Ljava/lang/String;)V

    .line 668
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextView(Ljava/lang/String;I)V

    .line 669
    return-void
.end method

.method private printAndAddLogLine(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 663
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLog(Ljava/lang/String;)V

    .line 664
    return-void
.end method

.method private printLogFailItems()V
    .locals 5

    .prologue
    .line 496
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLogFailItems:Ljava/util/List;

    .line 498
    .local v2, "logItemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;>;"
    if-nez v2, :cond_1

    .line 506
    :cond_0
    return-void

    .line 501
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;

    .line 502
    .local v1, "logItem":Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;
    const-string v3, "[Fail]"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextView(Ljava/lang/String;I)V

    .line 503
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mTag:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Xml:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Phone:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/samsung/sec/android/application/csc/CscVerifierLog$VerifyLogItem;->mPhoneValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextViewLine(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private printNewOrUnidentifiedItems()V
    .locals 7

    .prologue
    .line 467
    const/4 v0, 0x0

    .line 469
    .local v0, "bExistItems":Z
    const-string v6, "-------- New or unidentified items --------"

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 471
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mFullTagSet:Ljava/util/Set;

    .line 472
    .local v2, "fullTagSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getLogItemSet()Ljava/util/Set;

    move-result-object v4

    .line 475
    .local v4, "logItemSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5, v2}, Ljava/util/Vector;-><init>(Ljava/util/Collection;)V

    .line 476
    .local v5, "v":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 477
    invoke-virtual {v5}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 479
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 480
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 482
    .local v1, "customerTag":Ljava/lang/String;
    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_0

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->isUnusedItems(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 485
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 486
    const/4 v0, 0x1

    goto :goto_0

    .line 490
    .end local v1    # "customerTag":Ljava/lang/String;
    :cond_1
    if-nez v0, :cond_2

    .line 491
    const-string v6, "No item"

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 492
    :cond_2
    return-void
.end method

.method private printNotSupportedItems()V
    .locals 7

    .prologue
    .line 400
    const/4 v0, 0x0

    .line 402
    .local v0, "bExistNotSupportedItems":Z
    const-string v6, "-------- Unsupported items --------"

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 404
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getCustomerXmlParser()Lcom/samsung/sec/android/application/csc/CscXMLParser;

    move-result-object v1

    .line 405
    .local v1, "cscXMLParser":Lcom/samsung/sec/android/application/csc/CscXMLParser;
    new-instance v5, Ljava/util/Vector;

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/Vector;-><init>(Ljava/util/Collection;)V

    .line 406
    .local v5, "v":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 407
    invoke-virtual {v5}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 409
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 410
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 411
    .local v3, "key":Ljava/lang/String;
    invoke-virtual {v1, v3}, Lcom/samsung/sec/android/application/csc/CscXMLParser;->getNode(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 412
    .local v4, "node":Lorg/w3c/dom/Node;
    if-eqz v4, :cond_0

    .line 413
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mNotSupportedItemMap:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 414
    const/4 v0, 0x1

    goto :goto_0

    .line 418
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "node":Lorg/w3c/dom/Node;
    :cond_1
    if-nez v0, :cond_2

    .line 419
    const-string v6, "No item"

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 420
    :cond_2
    return-void
.end method

.method private printSummary()V
    .locals 9

    .prologue
    .line 378
    const-string v7, "-------- Customer.xml Summary --------"

    invoke-direct {p0, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 380
    new-instance v6, Ljava/util/Vector;

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryTotalMap:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/Vector;-><init>(Ljava/util/Collection;)V

    .line 381
    .local v6, "v":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 382
    invoke-virtual {v6}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 384
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 385
    .local v5, "resultTotal":I
    const/4 v4, 0x0

    .line 386
    .local v4, "resultPass":I
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 387
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 388
    .local v3, "key":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryTotalMap:Ljava/util/Map;

    invoke-interface {v7, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 389
    .local v2, "itemTotal":I
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSummaryPassMap:Ljava/util/Map;

    invoke-interface {v7, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 390
    .local v1, "itemPass":I
    add-int/2addr v5, v2

    .line 391
    add-int/2addr v4, v1

    .line 392
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "P/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sub-int v8, v2, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "F"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    goto :goto_0

    .line 395
    .end local v1    # "itemPass":I
    .end local v2    # "itemTotal":I
    .end local v3    # "key":Ljava/lang/String;
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Total : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "P/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sub-int v8, v5, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "F"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 397
    return-void
.end method

.method private printUnusedItems()V
    .locals 7

    .prologue
    .line 444
    const/4 v0, 0x0

    .line 446
    .local v0, "bExistItems":Z
    const-string v6, "-------- Unused items --------"

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 448
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getCustomerXmlParser()Lcom/samsung/sec/android/application/csc/CscXMLParser;

    move-result-object v1

    .line 449
    .local v1, "cscXMLParser":Lcom/samsung/sec/android/application/csc/CscXMLParser;
    new-instance v5, Ljava/util/Vector;

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    invoke-direct {v5, v6}, Ljava/util/Vector;-><init>(Ljava/util/Collection;)V

    .line 450
    .local v5, "v":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 451
    invoke-virtual {v5}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 453
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 454
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 455
    .local v3, "key":Ljava/lang/String;
    invoke-virtual {v1, v3}, Lcom/samsung/sec/android/application/csc/CscXMLParser;->getNode(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 456
    .local v4, "node":Lorg/w3c/dom/Node;
    if-eqz v4, :cond_0

    .line 457
    invoke-direct {p0, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 458
    const/4 v0, 0x1

    goto :goto_0

    .line 462
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "node":Lorg/w3c/dom/Node;
    :cond_1
    if-nez v0, :cond_2

    .line 463
    const-string v6, "No item"

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 464
    :cond_2
    return-void
.end method

.method public static startLog()V
    .locals 1

    .prologue
    .line 257
    sget-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->sEnableLog:Z

    if-nez v0, :cond_0

    .line 258
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    move-result-object v0

    invoke-direct {v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->_startLog()V

    .line 260
    :cond_0
    return-void
.end method


# virtual methods
.method public addUnusedItems(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 734
    invoke-static {p1}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscTgManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getFullList()Ljava/util/ArrayList;

    move-result-object v2

    .line 735
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    .line 736
    .local v1, "item":Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;
    iget-object v3, v1, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;->tgObject:Lcom/samsung/sec/android/application/csc/CscTgBase;

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mUnusedItemSet:Ljava/util/Set;

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscTgBase;->addUnusedItem(Ljava/util/Set;)V

    goto :goto_0

    .line 738
    .end local v1    # "item":Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;
    :cond_0
    return-void
.end method

.method public endLog(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 282
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextView(Ljava/lang/String;I)V

    .line 283
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->endLog()V

    .line 284
    return-void
.end method

.method public isSavingLog()Z
    .locals 1

    .prologue
    .line 519
    iget-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mbSavingLog:Z

    return v0
.end method

.method public printVersionInfo()V
    .locals 2

    .prologue
    .line 366
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PDA Version : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mPdaVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CSC Version : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCscVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 368
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sales Code : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 369
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CSC Edition : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mCSCEdition:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 370
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printAndAddLogLine(Ljava/lang/String;)V

    .line 371
    return-void
.end method

.method public saveLogFile()V
    .locals 3

    .prologue
    .line 524
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLog:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 525
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    move-result-object v0

    .line 526
    .local v0, "cscVerifierActivity":Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
    if-eqz v0, :cond_0

    .line 527
    const-string v1, "Log is empty or Log already had been saved."

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->updateToast(Ljava/lang/String;)V

    .line 571
    .end local v0    # "cscVerifierActivity":Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
    :cond_0
    :goto_0
    return-void

    .line 531
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mbSavingLog:Z

    if-eqz v1, :cond_2

    .line 532
    const-string v1, "CscVerifierLog"

    const-string v2, "It\'s already saving log."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 536
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mbSavingLog:Z

    .line 537
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscVerifierLog$1;

    invoke-direct {v2, p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog$1;-><init>(Lcom/samsung/sec/android/application/csc/CscVerifierLog;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
