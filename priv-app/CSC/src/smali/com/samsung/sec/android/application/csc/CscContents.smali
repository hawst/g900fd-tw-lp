.class public Lcom/samsung/sec/android/application/csc/CscContents;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscContents.java"


# instance fields
.field private dbCursor:Landroid/database/Cursor;

.field private isUpdate:Z

.field private mContentsDb:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mIgnoreList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mResolver:Landroid/content/ContentResolver;

.field private numFileSdcard:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->isUpdate:Z

    .line 94
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mContext:Landroid/content/Context;

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mResolver:Landroid/content/ContentResolver;

    .line 96
    return-void
.end method

.method private checkDefaultSnotebooks()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 589
    const-string v0, "/data/media/0/SnoteData"

    .line 591
    .local v0, "SRCPATH":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    const-string v6, "/data/media/0/SnoteData"

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 592
    .local v4, "srcdir":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_2

    .line 593
    :cond_0
    const-string v6, "CscContents"

    const-string v7, "Snotebooks was not made."

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    :cond_1
    :goto_0
    return v5

    .line 597
    :cond_2
    new-instance v1, Ljava/io/File;

    const-string v6, "/data/media/0/SnoteData"

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 598
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 599
    .local v2, "files":[Ljava/io/File;
    if-nez v2, :cond_3

    .line 600
    const-string v6, "CscContents"

    const-string v7, "Snotebooks was not made."

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 603
    :cond_3
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v6, v2

    if-ge v3, v6, :cond_1

    .line 604
    aget-object v6, v2, v3

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 605
    const-string v6, "Directory : "

    aget-object v7, v2, v3

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 607
    :cond_5
    aget-object v6, v2, v3

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "spd"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 608
    const-string v5, "File :"

    aget-object v6, v2, v3

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    const-string v5, "CscContents"

    const-string v6, "Snotebooks created."

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private compareWithContentsDb(Ljava/io/File;)V
    .locals 6
    .param p1, "sdFile"    # Ljava/io/File;

    .prologue
    .line 392
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mContentsDb:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 393
    .local v0, "file":Ljava/io/File;
    invoke-virtual {p1, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 397
    .end local v0    # "file":Ljava/io/File;
    :goto_0
    return-void

    .line 396
    :cond_1
    const-string v2, "CscContents"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unexpected file found : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "bytes)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isIgnoreFile(Ljava/io/File;)Z
    .locals 8
    .param p1, "exceptFile"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x1

    .line 339
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 340
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 341
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 342
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 343
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 344
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "dir"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    .line 388
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    return v2

    .line 351
    .restart local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "dir"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    .line 355
    goto :goto_0

    .line 360
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-nez v2, :cond_5

    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, ".apk"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, ".odex"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v4, "/system/app"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v4, "/system/priv-app"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v2, v3

    .line 364
    goto :goto_0

    .line 365
    :cond_5
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 366
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "file"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v3

    .line 370
    goto/16 :goto_0

    .line 371
    :cond_6
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "dir"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    .line 373
    goto/16 :goto_0

    .line 376
    :cond_7
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "file"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    move v2, v3

    .line 380
    goto/16 :goto_0

    .line 381
    :cond_8
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "dir"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    .line 383
    goto/16 :goto_0

    .line 388
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private makeExceptList()V
    .locals 3

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/com.cooliris.media"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/com.google.android"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/.thumbnails"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/albumthumbs"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Application/Gallery/.thumbnails"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/LOST.DIR"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/.ngmoco"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/DCIM/Camera/cache"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/VaultService/Cache"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Android/data"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Notifications"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/autonavidata50"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/DioDict3B"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/qqmusic"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Application/SMemo"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/.face"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "smvvm"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/.RetailMode"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/RetailMode"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/melon"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Tstore"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/preload/snote"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/S Note"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    :goto_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/SMemo/initialize.txt"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/TMemo/initialize.txt"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/TMemo/.memo"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/TMemo/.backgrounds"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/adhub/adhubk.db"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/amazonmp3"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "Application"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "DiskCache"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "devicefriendlyname"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, ".nomedia"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/null/.prefs.bak"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, ".filesize"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, ".version"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Samsung/Image/.thumb"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Samsung/Video/.thumb"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/DCIM/.thumbnails"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/ntsProxy"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/ASF/DMR/config"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/.albumart"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/cacheData"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/MMS/MMS.log"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Ringtones"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/.enref"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/SamsungHub/Books/Books"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Autodesk/SketchBookProGalaxy"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/SnoteData"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/My Documents"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Tencent/MobileQQ/diskcache/journal"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Tencent/MobileQQ/data/srvAddr.ini"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/webex"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/Tmap4"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/ollehnavimap.zip"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/0001/INST0001"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/LISMO/metadata.db"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/LISMO/metadata.db-journal"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/MapMyFitness/MapMyRun.mmf.log.lck"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/MapMyFitness/MapMyRun.mmf.log"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/yahoo/mail/imgCache"

    const-string v2, "dir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/.bugsense"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/.devicesalt"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/.adobe-digital-editions/.devicesalt"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/media/audio/notifications/facebook_ringtone_pop.m4a"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/.SMT/STUB_RESPONSE.xml"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    return-void

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    const-string v1, "/sdcard/S Note/init.txt"

    const-string v2, "file"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private queryCountUri(Landroid/net/Uri;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 421
    const/4 v2, 0x0

    .line 422
    .local v2, "projectionNull":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 423
    .local v4, "selectionNull":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 424
    .local v3, "stringNull":Ljava/lang/String;
    const/4 v8, 0x0

    .line 425
    .local v8, "queryCursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 428
    .local v7, "queryCount":I
    :try_start_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mResolver:Landroid/content/ContentResolver;

    move-object v1, p1

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 430
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 431
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 436
    :goto_0
    return v7

    .line 432
    :catch_0
    move-exception v6

    .line 433
    .local v6, "ex":Ljava/lang/NullPointerException;
    const/4 v7, -0x1

    goto :goto_0
.end method

.method private queryCountUri(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "strWhereIs"    # Ljava/lang/String;
    .param p4, "selection"    # [Ljava/lang/String;
    .param p5, "strParam"    # Ljava/lang/String;

    .prologue
    .line 441
    const/4 v8, 0x0

    .line 442
    .local v8, "queryCursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 445
    .local v7, "queryCount":I
    :try_start_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mResolver:Landroid/content/ContentResolver;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 446
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 447
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 453
    :goto_0
    return v7

    .line 448
    :catch_0
    move-exception v6

    .line 449
    .local v6, "ex":Ljava/lang/NullPointerException;
    const/4 v7, -0x1

    goto :goto_0
.end method


# virtual methods
.method public CountFiles(Ljava/lang/String;)I
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 400
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 401
    .local v6, "sdRoot":Ljava/io/File;
    const/4 v4, 0x0

    .line 402
    .local v4, "numOfRealFiles":I
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 403
    .local v7, "sdRootFiles":[Ljava/io/File;
    if-nez v7, :cond_0

    move v5, v4

    .line 417
    .end local v4    # "numOfRealFiles":I
    .local v5, "numOfRealFiles":I
    :goto_0
    return v5

    .line 406
    .end local v5    # "numOfRealFiles":I
    .restart local v4    # "numOfRealFiles":I
    :cond_0
    move-object v0, v7

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v1, v0, v2

    .line 407
    .local v1, "file":Ljava/io/File;
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscContents;->isIgnoreFile(Ljava/io/File;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 406
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 410
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 411
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscContents;->CountFiles(Ljava/lang/String;)I

    move-result v8

    add-int/2addr v4, v8

    goto :goto_2

    .line 413
    :cond_2
    add-int/lit8 v4, v4, 0x1

    .line 414
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscContents;->compareWithContentsDb(Ljava/io/File;)V

    goto :goto_2

    .end local v1    # "file":Ljava/io/File;
    :cond_3
    move v5, v4

    .line 417
    .end local v4    # "numOfRealFiles":I
    .restart local v5    # "numOfRealFiles":I
    goto :goto_0
.end method

.method public SetUpdateStatus(Z)V
    .locals 0
    .param p1, "status"    # Z

    .prologue
    .line 457
    iput-boolean p1, p0, Lcom/samsung/sec/android/application/csc/CscContents;->isUpdate:Z

    .line 458
    return-void
.end method

.method checkContentsDB()Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v12, 0x0

    .line 127
    const-string v0, "NOERROR"

    .line 128
    .local v0, "answer":Ljava/lang/String;
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->numFileSdcard:I

    .line 131
    :try_start_0
    const-string v9, "/system/csc/contents.db"

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mDB:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v10, "SELECT * from contents"

    invoke-virtual {v9, v10, v12}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    .line 140
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    const-string v10, "filePath"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 141
    .local v6, "pathIndex":I
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    const-string v10, "fileSize"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 143
    .local v8, "sizeIndex":I
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-nez v9, :cond_1

    .line 144
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 145
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 191
    .end local v0    # "answer":Ljava/lang/String;
    .end local v6    # "pathIndex":I
    .end local v8    # "sizeIndex":I
    :cond_0
    :goto_0
    return-object v0

    .line 132
    .restart local v0    # "answer":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 133
    .local v4, "e":Landroid/database/sqlite/SQLiteException;
    const-string v9, "CscContents"

    const-string v10, "DB for contents NOT found!"

    invoke-static {v9, v10}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v9, "CscContents : DB for contents NOT found!"

    invoke-static {v9}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 135
    const-string v0, "OPERATOR_CONTENTS:DB_NOT_FOUND"

    goto :goto_0

    .line 149
    .end local v4    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v6    # "pathIndex":I
    .restart local v8    # "sizeIndex":I
    :cond_1
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 150
    const/4 v7, 0x0

    .line 151
    .local v7, "position":I
    :goto_1
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v9

    if-nez v9, :cond_6

    .line 152
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "currentPath":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    invoke-interface {v9, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 155
    .local v2, "currentSize":J
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 156
    .local v5, "mFile":Ljava/io/File;
    invoke-direct {p0, v5}, Lcom/samsung/sec/android/application/csc/CscContents;->isIgnoreFile(Ljava/io/File;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 157
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 158
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v10

    cmp-long v9, v10, v2

    if-eqz v9, :cond_2

    .line 159
    const-string v9, "CscContents"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Size not matched : contetns.db : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " bytes, sdcard : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " bytes)"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CscContents : Size not matched : contetns.db : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " bytes, sdcard : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " bytes)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 164
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 165
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 166
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "OPERATOR_CONTENTS:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 168
    :cond_2
    const-string v9, "/sdcard"

    invoke-virtual {v1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "sdcard"

    invoke-virtual {v1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 169
    :cond_3
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mContentsDb:Ljava/util/List;

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    iget v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->numFileSdcard:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->numFileSdcard:I

    .line 182
    :cond_4
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_1

    .line 173
    :cond_5
    const-string v9, "CscContents"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "File not exist  : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CscContents : File not exist  : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 176
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 177
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 178
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "OPERATOR_CONTENTS:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 185
    .end local v1    # "currentPath":Ljava/lang/String;
    .end local v2    # "currentSize":J
    .end local v5    # "mFile":Ljava/io/File;
    :cond_6
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v9

    if-nez v9, :cond_7

    .line 186
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->dbCursor:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 188
    :cond_7
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 189
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto/16 :goto_0
.end method

.method public checkDefaultSMemoCount()Z
    .locals 22

    .prologue
    .line 462
    const/4 v9, -0x1

    .line 463
    .local v9, "actualMemoCount":I
    const/4 v13, 0x7

    .line 464
    .local v13, "expectedMemoCount":I
    const-string v8, "content://com.sec.android.widgetapp.q1_penmemo/PenMemo"

    .line 466
    .local v8, "MemoUri":Ljava/lang/String;
    new-instance v14, Ljava/io/File;

    const-string v2, "/system/hdic/pen_memo.db.cnt"

    invoke-direct {v14, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 467
    .local v14, "f":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 468
    const/4 v15, 0x0

    .line 470
    .local v15, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v16, Ljava/io/FileInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v14}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471
    .end local v15    # "fis":Ljava/io/FileInputStream;
    .local v16, "fis":Ljava/io/FileInputStream;
    const/16 v2, 0xa

    :try_start_1
    new-array v10, v2, [B

    fill-array-data v10, :array_0

    .line 474
    .local v10, "buf":[B
    const/4 v2, 0x0

    array-length v3, v10

    move-object/from16 v0, v16

    invoke-virtual {v0, v10, v2, v3}, Ljava/io/FileInputStream;->read([BII)I

    move-result v17

    .line 475
    .local v17, "readed":I
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "UTF-8"

    move/from16 v0, v17

    invoke-direct {v2, v10, v3, v0, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v13

    .line 479
    if-eqz v16, :cond_0

    .line 481
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3

    .line 488
    .end local v10    # "buf":[B
    .end local v16    # "fis":Ljava/io/FileInputStream;
    .end local v17    # "readed":I
    :cond_0
    :goto_0
    const/16 v19, 0xa

    .local v19, "retryCnt":I
    move/from16 v20, v19

    .line 489
    .end local v19    # "retryCnt":I
    .local v20, "retryCnt":I
    :goto_1
    add-int/lit8 v19, v20, -0x1

    .end local v20    # "retryCnt":I
    .restart local v19    # "retryCnt":I
    if-lez v20, :cond_1

    .line 490
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContents;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 491
    .local v11, "c":Landroid/database/Cursor;
    if-eqz v11, :cond_4

    .line 492
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 493
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 503
    .end local v11    # "c":Landroid/database/Cursor;
    :cond_1
    :goto_2
    if-ne v13, v9, :cond_5

    const/16 v18, 0x1

    .line 505
    .local v18, "result":Z
    :goto_3
    const-string v2, "CscContents"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", actualMemoCount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", expectedMemoCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", retryCnt="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    if-nez v18, :cond_2

    .line 510
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CscContents : SMEMO result="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", actualMemoCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", expectedMemoCount = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", retryCnt="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 515
    :cond_2
    return v18

    .line 476
    .end local v18    # "result":Z
    .end local v19    # "retryCnt":I
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v21

    .line 477
    .local v21, "t":Ljava/lang/Throwable;
    :goto_4
    :try_start_3
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 479
    if-eqz v15, :cond_0

    .line 481
    :try_start_4
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 482
    :catch_1
    move-exception v2

    goto/16 :goto_0

    .line 479
    .end local v21    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    :goto_5
    if-eqz v15, :cond_3

    .line 481
    :try_start_5
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4

    .line 483
    :cond_3
    :goto_6
    throw v2

    .line 497
    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "c":Landroid/database/Cursor;
    .restart local v19    # "retryCnt":I
    :cond_4
    const-wide/16 v2, 0x32

    :try_start_6
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_2

    move/from16 v20, v19

    .line 501
    .end local v19    # "retryCnt":I
    .restart local v20    # "retryCnt":I
    goto/16 :goto_1

    .line 498
    .end local v20    # "retryCnt":I
    .restart local v19    # "retryCnt":I
    :catch_2
    move-exception v12

    .line 499
    .local v12, "e":Ljava/lang/InterruptedException;
    goto/16 :goto_2

    .line 503
    .end local v11    # "c":Landroid/database/Cursor;
    .end local v12    # "e":Ljava/lang/InterruptedException;
    :cond_5
    const/16 v18, 0x0

    goto/16 :goto_3

    .line 482
    .end local v19    # "retryCnt":I
    .restart local v10    # "buf":[B
    .restart local v16    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "readed":I
    :catch_3
    move-exception v2

    goto/16 :goto_0

    .end local v10    # "buf":[B
    .end local v16    # "fis":Ljava/io/FileInputStream;
    .end local v17    # "readed":I
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v3

    goto :goto_6

    .line 479
    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v16    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v2

    move-object/from16 v15, v16

    .end local v16    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .line 476
    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v16    # "fis":Ljava/io/FileInputStream;
    :catch_5
    move-exception v21

    move-object/from16 v15, v16

    .end local v16    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 471
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method checkSDCard()Ljava/lang/String;
    .locals 3

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 197
    .local v0, "noFiles":I
    const-string v1, "/sdcard/"

    invoke-virtual {p0, v1}, Lcom/samsung/sec/android/application/csc/CscContents;->CountFiles(Ljava/lang/String;)I

    move-result v0

    .line 198
    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscContents;->numFileSdcard:I

    if-eq v0, v1, :cond_0

    .line 199
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CscContents : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (sdcard : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",contents.db : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/sec/android/application/csc/CscContents;->numFileSdcard:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 201
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SDCARD_CONTENTS:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (sdcard : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",contents.db : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/sec/android/application/csc/CscContents;->numFileSdcard:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 205
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "NOERROR"

    goto :goto_0
.end method

.method public compare()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    const-string v0, "NOERROR"

    .line 104
    .local v0, "answer":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mContentsDb:Ljava/util/List;

    .line 105
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    .line 107
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscContents;->makeExceptList()V

    .line 110
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscContents;->checkContentsDB()Ljava/lang/String;

    move-result-object v0

    .line 114
    const-string v1, "NOERROR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscContents;->checkSDCard()Ljava/lang/String;

    move-result-object v0

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mContentsDb:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 121
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mIgnoreList:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 123
    return-object v0
.end method

.method public update()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public verifyResetDone()Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 618
    const-string v6, ""

    .line 619
    .local v6, "answer":Ljava/lang/String;
    const/4 v8, 0x0

    .line 620
    .local v8, "projectionNull":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 621
    .local v11, "selectionNull":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 622
    .local v12, "stringNull":Ljava/lang/String;
    const/4 v9, 0x0

    .line 624
    .local v9, "queryCount":I
    const-string v0, "CscContents"

    const-string v1, "verifyResetDone() start"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    iget-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->isUpdate:Z

    if-nez v0, :cond_8

    .line 629
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, " account_name != \'vnd.sec.contact.sim\' AND account_type != \'vnd.sec.contact.sim\' AND deleted==0"

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/sec/android/application/csc/CscContents;->queryCountUri(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 631
    const-string v0, "CscContents"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Contacts counted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    if-lez v9, :cond_0

    .line 633
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CscContents : Contacts counted : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 635
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CONTACT/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 648
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscUtil;->isWifiOnly(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 649
    sget-object v0, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContents;->queryCountUri(Landroid/net/Uri;)I

    move-result v9

    .line 650
    const-string v0, "CscContents"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Calls counted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    if-lez v9, :cond_1

    .line 652
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CscContents : Calls counted : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 654
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CALLS/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 659
    :cond_1
    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContents;->queryCountUri(Landroid/net/Uri;)I

    move-result v9

    .line 660
    const-string v0, "CscContents"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SMS counted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    if-lez v9, :cond_2

    .line 662
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CscContents : SMS counted : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 663
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SMS/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 667
    :cond_2
    sget-object v0, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContents;->queryCountUri(Landroid/net/Uri;)I

    move-result v9

    .line 668
    const-string v0, "CscContents"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MMS counted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    if-lez v9, :cond_3

    .line 670
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CscContents : MMS counted : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 671
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MMS/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 695
    :cond_3
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/data/com.samsung.android.app.memo"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 696
    const/4 v13, 0x0

    .line 698
    .local v13, "uri":Landroid/net/Uri;
    const-string v0, "content://com.samsung.android.memo/memo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 700
    invoke-direct {p0, v13}, Lcom/samsung/sec/android/application/csc/CscContents;->queryCountUri(Landroid/net/Uri;)I

    move-result v9

    .line 701
    const-string v0, "CscContents"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Memo counted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    if-lez v9, :cond_4

    .line 703
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CscContents : Memo counted : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 705
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MEMO/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 710
    .end local v13    # "uri":Landroid/net/Uri;
    :cond_4
    new-instance v0, Ljava/io/File;

    const-string v1, "/sdcard/Application/SMemo"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 711
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscContents;->checkDefaultSMemoCount()Z

    move-result v0

    if-nez v0, :cond_5

    .line 712
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SMEMO/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 717
    :cond_5
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/media/0/SnoteData"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscContents;->checkDefaultSnotebooks()Z

    move-result v0

    if-nez v0, :cond_6

    .line 721
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SNOTE/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 725
    :cond_6
    const-string v0, "content://com.samsung.sec.android.clockpackage/alarm"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContents;->queryCountUri(Landroid/net/Uri;)I

    move-result v9

    .line 727
    const-string v0, "CscContents"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Alarm counted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    if-lez v9, :cond_7

    .line 729
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CscContents : Alarm counted : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 730
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ALARM/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 743
    :cond_7
    const-string v0, "CscContents"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "verifyResetDone() end / answer : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    const-string v0, ""

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 746
    const-string v0, "OK"

    .line 748
    :goto_1
    return-object v0

    .line 638
    :cond_8
    const-string v10, ""

    .line 639
    .local v10, "result":Ljava/lang/String;
    new-instance v7, Lcom/samsung/sec/android/application/csc/CscContacts;

    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContents;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Lcom/samsung/sec/android/application/csc/CscContacts;-><init>(Landroid/content/Context;)V

    .line 640
    .local v7, "contact":Lcom/samsung/sec/android/application/csc/CscContacts;
    invoke-virtual {v7}, Lcom/samsung/sec/android/application/csc/CscContacts;->compare()Ljava/lang/String;

    move-result-object v10

    .line 641
    const-string v0, "NOERROR"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 642
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CscContents :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 643
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CONTACT/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 748
    .end local v7    # "contact":Lcom/samsung/sec/android/application/csc/CscContacts;
    .end local v10    # "result":Ljava/lang/String;
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NG,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
