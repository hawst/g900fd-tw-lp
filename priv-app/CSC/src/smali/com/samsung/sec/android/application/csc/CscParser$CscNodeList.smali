.class public Lcom/samsung/sec/android/application/csc/CscParser$CscNodeList;
.super Ljava/lang/Object;
.source "CscParser.java"

# interfaces
.implements Lorg/w3c/dom/NodeList;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CscNodeList"
.end annotation


# instance fields
.field private children:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/w3c/dom/Node;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscParser$CscNodeList;->children:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method appendChild(Lorg/w3c/dom/Node;)V
    .locals 1
    .param p1, "newChild"    # Lorg/w3c/dom/Node;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscParser$CscNodeList;->children:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    return-void
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscParser$CscNodeList;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public item(I)Lorg/w3c/dom/Node;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscParser$CscNodeList;->children:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Node;

    return-object v0
.end method
