.class Lcom/samsung/sec/android/application/csc/CscVerifierActivity$2;
.super Ljava/util/TimerTask;
.source "CscVerifierActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;


# direct methods
.method constructor <init>(Lcom/samsung/sec/android/application/csc/CscVerifierActivity;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$2;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 132
    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextQueue:Ljava/util/Queue;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$100()Ljava/util/Queue;

    move-result-object v2

    monitor-enter v2

    .line 133
    :try_start_0
    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextQueue:Ljava/util/Queue;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$100()Ljava/util/Queue;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextResult:Landroid/text/SpannableStringBuilder;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$200()Landroid/text/SpannableStringBuilder;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$2;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    iget-boolean v3, v3, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mbUiSvUpdate:Z

    if-eqz v3, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 134
    .local v0, "bUiRefresh":Z
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    if-eqz v0, :cond_1

    .line 136
    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sCscVerifierActivity:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$000()Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 137
    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sCscVerifierActivity:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$000()Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 139
    :cond_1
    return-void

    .end local v0    # "bUiRefresh":Z
    :cond_2
    move v0, v1

    .line 133
    goto :goto_0

    .line 134
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
