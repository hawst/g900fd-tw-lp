.class public Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
.super Landroid/app/Activity;
.source "CscVerifierActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static sCscVerifierActivity:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

.field private static sEnableUI:Z

.field private static sTextQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/text/SpannableStringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private static sTextResult:Landroid/text/SpannableStringBuilder;


# instance fields
.field mCscVerifierLog:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

.field mHandler:Landroid/os/Handler;

.field mUiEncodeButton:Landroid/widget/Button;

.field mUiResultTv:Landroid/widget/TextView;

.field mUiStartButton:Landroid/widget/Button;

.field mUiSv:Landroid/widget/ScrollView;

.field mUiTv:Landroid/widget/TextView;

.field mUiUpdateTimer:Ljava/util/Timer;

.field mUiUpdateTimerTask:Ljava/util/TimerTask;

.field mbUiSvUpdate:Z

.field mnUiSvHeight:I

.field mnUiSvOffset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sEnableUI:Z

    .line 52
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextQueue:Ljava/util/Queue;

    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextResult:Landroid/text/SpannableStringBuilder;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 56
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mCscVerifierLog:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    .line 58
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mbUiSvUpdate:Z

    .line 60
    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mnUiSvOffset:I

    .line 62
    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mnUiSvHeight:I

    .line 72
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;-><init>(Lcom/samsung/sec/android/application/csc/CscVerifierActivity;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mHandler:Landroid/os/Handler;

    .line 127
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiUpdateTimer:Ljava/util/Timer;

    .line 129
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$2;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$2;-><init>(Lcom/samsung/sec/android/application/csc/CscVerifierActivity;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiUpdateTimerTask:Ljava/util/TimerTask;

    return-void
.end method

.method static synthetic access$000()Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sCscVerifierActivity:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/Queue;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$200()Landroid/text/SpannableStringBuilder;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextResult:Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method static synthetic access$202(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 0
    .param p0, "x0"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 33
    sput-object p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextResult:Landroid/text/SpannableStringBuilder;

    return-object p0
.end method

.method public static addTextView(Ljava/lang/String;I)V
    .locals 3
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "nTextType"    # I

    .prologue
    .line 341
    const/4 v0, 0x0

    .line 342
    .local v0, "bResult":Z
    sget-boolean v2, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sEnableUI:Z

    if-eqz v2, :cond_0

    .line 343
    const/4 v1, 0x0

    .line 344
    .local v1, "color":I
    packed-switch p1, :pswitch_data_0

    .line 359
    :goto_0
    :pswitch_0
    invoke-static {p0, v1, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->updateTextView(Ljava/lang/String;IZ)V

    .line 361
    .end local v1    # "color":I
    :cond_0
    return-void

    .line 346
    .restart local v1    # "color":I
    :pswitch_1
    const/high16 v1, -0x10000

    .line 347
    goto :goto_0

    .line 349
    :pswitch_2
    const v1, -0xff0100

    .line 350
    const/4 v0, 0x1

    .line 351
    goto :goto_0

    .line 353
    :pswitch_3
    const/4 v0, 0x1

    .line 354
    const/high16 v1, -0x10000

    .line 355
    goto :goto_0

    .line 344
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static addTextViewLine(Ljava/lang/String;I)V
    .locals 2
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "nTextType"    # I

    .prologue
    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextView(Ljava/lang/String;I)V

    .line 338
    return-void
.end method

.method private static clearUiTextBuffer()V
    .locals 2

    .prologue
    .line 386
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextResult:Landroid/text/SpannableStringBuilder;

    .line 387
    sget-object v1, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextQueue:Ljava/util/Queue;

    monitor-enter v1

    .line 388
    :try_start_0
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 389
    monitor-exit v1

    .line 390
    return-void

    .line 389
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static endUI()V
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sEnableUI:Z

    .line 330
    return-void
.end method

.method public static getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
    .locals 1

    .prologue
    .line 325
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sCscVerifierActivity:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    return-object v0
.end method

.method public static isEnableUI()Z
    .locals 1

    .prologue
    .line 333
    sget-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sEnableUI:Z

    return v0
.end method

.method private saveLogFile()V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mCscVerifierLog:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    invoke-virtual {v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->saveLogFile()V

    .line 311
    return-void
.end method

.method private static updateTextView(Ljava/lang/String;IZ)V
    .locals 5
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "color"    # I
    .param p2, "bResult"    # Z

    .prologue
    .line 368
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 369
    .local v1, "sp":Landroid/text/SpannableStringBuilder;
    if-eqz p1, :cond_0

    .line 370
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 371
    .local v0, "fcolor":Landroid/text/style/ForegroundColorSpan;
    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 374
    .end local v0    # "fcolor":Landroid/text/style/ForegroundColorSpan;
    :cond_0
    if-eqz p2, :cond_1

    .line 375
    sput-object v1, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextResult:Landroid/text/SpannableStringBuilder;

    .line 381
    :goto_0
    return-void

    .line 377
    :cond_1
    sget-object v3, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextQueue:Ljava/util/Queue;

    monitor-enter v3

    .line 378
    :try_start_0
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextQueue:Ljava/util/Queue;

    invoke-interface {v2, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 379
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method


# virtual methods
.method checkHiddenMenuEnable()Z
    .locals 9

    .prologue
    .line 143
    const-string v0, "/efs/carrier/HiddenMenu"

    .line 144
    .local v0, "HIDDENMENU_ENABLE_PATH":Ljava/lang/String;
    const-string v1, "ON"

    .line 145
    .local v1, "HIDDEN_MENU_ON":Ljava/lang/String;
    const/4 v5, 0x0

    .line 146
    .local v5, "result":Ljava/lang/String;
    const/4 v3, 0x0

    .line 149
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const-string v8, "UTF-8"

    invoke-direct {v6, v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    if-eqz v4, :cond_0

    .line 152
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 153
    if-eqz v5, :cond_0

    .line 154
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    .line 159
    :cond_0
    if-eqz v4, :cond_3

    .line 161
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 168
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    const-string v6, "CscVerifierActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "checkHiddenMenuEnable() : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    return v6

    .line 162
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v2

    .line 163
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 164
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 156
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 157
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 159
    if-eqz v3, :cond_1

    .line 161
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 162
    :catch_2
    move-exception v2

    .line 163
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 159
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_2
    if-eqz v3, :cond_2

    .line 161
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 164
    :cond_2
    :goto_3
    throw v6

    .line 162
    :catch_3
    move-exception v2

    .line 163
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 159
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 156
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 264
    const-string v1, "CscVerifierActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CscVerifierActivity.onClick()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->isEnableLog()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->isEnableLog()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mCscVerifierLog:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->isSavingLog()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->clearUiTextBuffer()V

    .line 270
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sEnableUI:Z

    .line 271
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->startLog()V

    .line 273
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiTv:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiResultTv:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f05000b

    if-ne v1, v2, :cond_2

    .line 278
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/sec/android/application/csc/CscCompareService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 279
    .local v0, "serviceIntent":Landroid/content/Intent;
    const-string v1, "MODE"

    const-string v2, "KEYSTRING"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 281
    .end local v0    # "serviceIntent":Landroid/content/Intent;
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f05000c

    if-ne v1, v2, :cond_0

    .line 283
    const-string v1, "CscVerifierActivity"

    const-string v2, "onClick() : startService(CscEncodeService)"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/samsung/sec/android/application/csc/CscEncodeService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 285
    const-string v1, "/sdcard/customer_enc.xml Encoding..."

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextViewLine(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    .line 232
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 234
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiSv:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mnUiSvOffset:I

    .line 235
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiSv:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiSv:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mnUiSvHeight:I

    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mbUiSvUpdate:Z

    .line 237
    const-string v0, "CscVerifierActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfigurationChanged() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mnUiSvOffset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiSv:Landroid/widget/ScrollView;

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiSv:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 174
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 176
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 178
    .local v6, "salesCode":Ljava/lang/String;
    const-string v0, "VZW"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "USC"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CRI"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "XAR"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "ro.build.type"

    const-string v1, "user"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->checkHiddenMenuEnable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    const-string v0, "CscVerifierActivity"

    const-string v1, "onCreate() : Hiddnemenu is blocked."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->finish()V

    .line 221
    :goto_0
    return-void

    .line 187
    :cond_1
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->setContentView(I)V

    .line 189
    const v0, 0x7f050007

    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiSv:Landroid/widget/ScrollView;

    .line 190
    const v0, 0x7f050009

    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiTv:Landroid/widget/TextView;

    .line 191
    const v0, 0x7f05000a

    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiResultTv:Landroid/widget/TextView;

    .line 193
    const v0, 0x7f05000b

    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiStartButton:Landroid/widget/Button;

    .line 194
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiStartButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiStartButton:Landroid/widget/Button;

    const-string v1, "Verify"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 197
    const v0, 0x7f05000c

    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiEncodeButton:Landroid/widget/Button;

    .line 198
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiEncodeButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiEncodeButton:Landroid/widget/Button;

    const-string v1, "Encode"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 201
    sput-object p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sCscVerifierActivity:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    .line 202
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sEnableUI:Z

    .line 203
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mCscVerifierLog:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    .line 205
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiUpdateTimer:Ljava/util/Timer;

    .line 206
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiUpdateTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiUpdateTimerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xfa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 208
    if-nez p1, :cond_2

    .line 211
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->clearUiTextBuffer()V

    .line 215
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mCscVerifierLog:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    invoke-virtual {v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->printVersionInfo()V

    .line 216
    const-string v0, "To verify the CSC PreConfig, Press the Verify button"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->addTextViewLine(Ljava/lang/String;I)V

    .line 220
    :cond_2
    const-string v0, "CscVerifierActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sCscVerifierActivity:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x0

    .line 291
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 292
    .local v0, "result":Z
    const/4 v1, 0x1

    const-string v2, "Save Log"

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 293
    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 224
    const-string v0, "CscVerifierActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sCscVerifierActivity:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sCscVerifierActivity:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    .line 226
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiUpdateTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiUpdateTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 228
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 229
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 298
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 306
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 300
    :pswitch_0
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->isEnableLog()Z

    move-result v0

    if-nez v0, :cond_0

    .line 301
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->saveLogFile()V

    goto :goto_0

    .line 298
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 243
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 245
    const-string v0, "CscVerifierActivity"

    const-string v1, "onRestoreInstanceState"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiTv:Landroid/widget/TextView;

    const-string v1, "tv"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiResultTv:Landroid/widget/TextView;

    const-string v1, "resultTv"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    const-string v0, "scrollY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mnUiSvOffset:I

    .line 249
    const-string v0, "scrollH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mnUiSvHeight:I

    .line 250
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mbUiSvUpdate:Z

    .line 251
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 254
    const-string v0, "CscVerifierActivity"

    const-string v1, "onSaveInstanceState"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const-string v0, "resultTv"

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiResultTv:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 256
    const-string v0, "tv"

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiTv:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 257
    const-string v0, "scrollY"

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiSv:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 258
    const-string v0, "scrollH"

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiSv:Landroid/widget/ScrollView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiSv:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 260
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 261
    return-void
.end method

.method public updateToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "toastMsg"    # Ljava/lang/String;

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$3;

    invoke-direct {v1, p0, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$3;-><init>(Lcom/samsung/sec/android/application/csc/CscVerifierActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 319
    return-void
.end method
