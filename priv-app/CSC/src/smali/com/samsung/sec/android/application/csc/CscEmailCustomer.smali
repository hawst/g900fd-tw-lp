.class public Lcom/samsung/sec/android/application/csc/CscEmailCustomer;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscEmailCustomer.java"


# static fields
.field private static mCustomerCount:I

.field static mCustomerList:Lorg/w3c/dom/NodeList;

.field static mCustomerNode:Lorg/w3c/dom/Node;

.field private static mDoc:Lorg/w3c/dom/Document;

.field private static mFilePath:Ljava/lang/String;

.field private static mRoot:Lorg/w3c/dom/Node;


# instance fields
.field private final CSC_FILE:Ljava/lang/String;

.field private final EMAILDATA_FILENAME:Ljava/lang/String;

.field private csc_context:Landroid/content/Context;

.field private sEmailPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 38
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->CSC_FILE:Ljava/lang/String;

    .line 42
    const-string v0, "CSCDATA_EmailAccountSetting"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->EMAILDATA_FILENAME:Ljava/lang/String;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->sEmailPrefs:Landroid/content/SharedPreferences;

    .line 118
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->csc_context:Landroid/content/Context;

    .line 119
    return-void
.end method

.method public static getCustomerFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/system/csc/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "filePath":Ljava/lang/String;
    const-string v1, "CSC_DEBUG"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "filePath: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    return-object v0
.end method


# virtual methods
.method public compare()Ljava/lang/String;
    .locals 34

    .prologue
    .line 383
    const/16 v18, 0x0

    .line 384
    .local v18, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const/4 v10, 0x0

    .line 385
    .local v10, "compare_context":Landroid/content/Context;
    const/16 v20, 0x0

    .line 387
    .local v20, "num_CustomerCount":Ljava/lang/String;
    const-string v4, "CSC_LOG"

    const-string v6, " started compare_EmailAccount()"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    const-string v25, "NONE"

    .line 391
    .local v25, "send_data":Ljava/lang/String;
    const-string v11, "NOERROR"

    .line 393
    .local v11, "compare_result":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->loadXMLFile()V

    .line 395
    const-string v4, "CSC_LOG"

    const-string v6, "loadXMLFile end compare_EmailAccount "

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    new-instance v18, Lcom/samsung/sec/android/application/csc/CscParser;

    .end local v18    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->CSC_FILE:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 398
    .restart local v18    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const-string v4, "CSC_LOG"

    const-string v6, "-------------------------- mParser "

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    :try_start_0
    const-string v4, "CSC_LOG"

    const-string v6, " started  compare_EmailAccount() Try "

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->csc_context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v6, "com.android.email"

    const/4 v7, 0x2

    invoke-virtual {v4, v6, v7}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v10

    .line 403
    if-nez v10, :cond_0

    .line 404
    const-string v4, "CSC_LOG"

    const-string v6, "compare_context == null"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :cond_0
    const/4 v5, 0x0

    .line 415
    .local v5, "CustomerCount":Ljava/lang/String;
    sget v4, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerCount:I

    if-nez v4, :cond_1

    .line 416
    const-string v4, "NOERROR"

    .line 687
    .end local v5    # "CustomerCount":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 418
    .restart local v5    # "CustomerCount":Ljava/lang/String;
    :cond_1
    sget v4, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerCount:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    .line 419
    const-string v4, "Settings.Messages.Email.NbAccount"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    const-string v4, "content://com.android.email.service.PreferenceProvider/listall_account"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 424
    .local v3, "EmailAccount_PREFERENCE":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->csc_context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 425
    .local v2, "resolver":Landroid/content/ContentResolver;
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 429
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_1c

    .line 430
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_1b

    .line 431
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 432
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-ge v15, v4, :cond_1b

    .line 433
    const-string v4, "CSC_LOG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " started compare_EmailAccount() + i : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    invoke-interface {v4, v15}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    .line 436
    .local v9, "accountNodeListChild":Lorg/w3c/dom/Node;
    const/16 v30, 0x0

    .line 437
    .local v30, "serverType":Ljava/lang/String;
    const/16 v24, 0x0

    .line 438
    .local v24, "secureType":Ljava/lang/String;
    const/16 v26, 0x0

    .line 439
    .local v26, "serverAddrIncoming":Ljava/lang/String;
    const/16 v27, 0x0

    .line 440
    .local v27, "serverAddrOutgoing":Ljava/lang/String;
    const/16 v28, 0x0

    .line 441
    .local v28, "serverPortIncoming":Ljava/lang/String;
    const/16 v29, 0x0

    .line 442
    .local v29, "serverPortOutgoing":Ljava/lang/String;
    const/16 v31, 0x0

    .line 444
    .local v31, "smtpAuth":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 446
    .local v23, "rcv_cmp":Ljava/lang/String;
    const-string v4, "CSC_LOG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " inserted rcv_str value "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    const-string v4, "NONE"

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1a

    .line 448
    const-string v4, "#"

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v32

    .line 450
    .local v32, "splitRcvStr":[Ljava/lang/String;
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    invoke-interface {v4, v15}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    const-string v6, "NetworkName"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    .line 452
    .local v19, "networkName":Ljava/lang/String;
    const-string v4, "CSC_LOG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " networkName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    if-eqz v19, :cond_2

    const/4 v4, 0x0

    aget-object v4, v32, v4

    if-eqz v4, :cond_2

    .line 454
    const/4 v4, 0x0

    aget-object v4, v32, v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    .line 455
    const-string v4, "Settings.Messages.Email.Account.NetworkName"

    const/4 v6, 0x0

    aget-object v6, v32, v6

    move-object/from16 v0, v19

    invoke-static {v4, v6, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.NetworkName_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 459
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 466
    :cond_2
    :goto_2
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    invoke-interface {v4, v15}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    const-string v6, "AccountName"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    .line 469
    .local v8, "accountName":Ljava/lang/String;
    if-eqz v8, :cond_3

    const/4 v4, 0x1

    aget-object v4, v32, v4

    if-eqz v4, :cond_3

    .line 470
    const/4 v4, 0x1

    aget-object v4, v32, v4

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 471
    const-string v4, "Settings.Messages.Email.Account.AccountName"

    const/4 v6, 0x1

    aget-object v6, v32, v6

    invoke-static {v4, v6, v8}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.AccountName_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 475
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 480
    :goto_3
    const-string v4, "CSC_LOG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " networkName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    :cond_3
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    invoke-interface {v4, v15}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    const-string v6, "EmailAddr"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v14

    .line 485
    .local v14, "emailAddr":Ljava/lang/String;
    if-eqz v14, :cond_4

    const/4 v4, 0x2

    aget-object v4, v32, v4

    if-eqz v4, :cond_4

    .line 486
    const/4 v4, 0x2

    aget-object v4, v32, v4

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 487
    const-string v4, "Settings.Messages.Email.Account.EmailAddr"

    const/4 v6, 0x2

    aget-object v6, v32, v6

    invoke-static {v4, v6, v14}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.EmailAddr_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 491
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 497
    :cond_4
    :goto_4
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    invoke-interface {v4, v15}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    const-string v6, "LoginType"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v33

    .line 499
    .local v33, "userName":Ljava/lang/String;
    if-eqz v33, :cond_5

    const/4 v4, 0x3

    aget-object v4, v32, v4

    if-eqz v4, :cond_5

    .line 500
    const/4 v4, 0x3

    aget-object v4, v32, v4

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 501
    const-string v4, "Settings.Messages.Email.Account.LoginType"

    const/4 v6, 0x3

    aget-object v6, v32, v6

    move-object/from16 v0, v33

    invoke-static {v4, v6, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.LoginType_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 505
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 511
    :cond_5
    :goto_5
    const-string v4, "CSC_LOG"

    const-string v6, "------------------ networkName, accountName , emailAddr, userName"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    const-string v4, "Incoming"

    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v17

    .line 521
    .local v17, "incoming_node":Lorg/w3c/dom/Node;
    const-string v4, "MailboxType"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v16

    .line 523
    .local v16, "incoming_child_node":Lorg/w3c/dom/Node;
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v30

    .line 524
    if-eqz v30, :cond_6

    const/4 v4, 0x4

    aget-object v4, v32, v4

    if-eqz v4, :cond_6

    .line 525
    const/4 v4, 0x4

    aget-object v4, v32, v4

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_12

    .line 526
    const-string v4, "Settings.Messages.Email.Account.Incoming.MailboxType"

    const/4 v6, 0x4

    aget-object v6, v32, v6

    move-object/from16 v0, v30

    invoke-static {v4, v6, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.Incoming.MailboxType_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 530
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 540
    :cond_6
    :goto_6
    const-string v4, "ServAddr"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v16

    .line 541
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v26

    .line 542
    if-eqz v26, :cond_7

    const/4 v4, 0x5

    aget-object v4, v32, v4

    if-eqz v4, :cond_7

    .line 543
    const/4 v4, 0x5

    aget-object v4, v32, v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 544
    const-string v4, "Settings.Messages.Email.Account.Incoming.ServAddr"

    const/4 v6, 0x5

    aget-object v6, v32, v6

    move-object/from16 v0, v26

    invoke-static {v4, v6, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.Incoming.ServAddr_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 548
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 553
    :goto_7
    const-string v4, "CSC_LOG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " networkName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    :cond_7
    const-string v4, "Port"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v16

    .line 560
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v28

    .line 561
    if-eqz v28, :cond_8

    const/4 v4, 0x6

    aget-object v4, v32, v4

    if-eqz v4, :cond_8

    .line 562
    const/4 v4, 0x6

    aget-object v4, v32, v4

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_14

    .line 563
    const-string v4, "Settings.Messages.Email.Account.Incoming.Port"

    const/4 v6, 0x6

    aget-object v6, v32, v6

    move-object/from16 v0, v28

    invoke-static {v4, v6, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.Incoming.Port_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 567
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 577
    :cond_8
    :goto_8
    const-string v4, "Secure"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v16

    .line 578
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v24

    .line 579
    if-eqz v24, :cond_9

    const/4 v4, 0x7

    aget-object v4, v32, v4

    if-eqz v4, :cond_9

    .line 580
    const/4 v4, 0x7

    aget-object v4, v32, v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_15

    .line 581
    const-string v4, "Settings.Messages.Email.Account.Incoming.Secure"

    const/4 v6, 0x7

    aget-object v6, v32, v6

    move-object/from16 v0, v24

    invoke-static {v4, v6, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.Incoming.Secure_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 585
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 591
    :cond_9
    :goto_9
    const-string v4, "CSC_LOG"

    const-string v6, "------------------ incomingList"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    const-string v4, "Outgoing"

    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    .line 601
    .local v22, "outcoming_node":Lorg/w3c/dom/Node;
    const-string v4, "ServAddr"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v21

    .line 603
    .local v21, "outcoming_child_node":Lorg/w3c/dom/Node;
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v27

    .line 604
    if-eqz v27, :cond_a

    const/16 v4, 0x8

    aget-object v4, v32, v4

    if-eqz v4, :cond_a

    .line 605
    const/16 v4, 0x8

    aget-object v4, v32, v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_16

    .line 606
    const-string v4, "Settings.Messages.Email.Account.Outgoing.ServAddr"

    const/16 v6, 0x8

    aget-object v6, v32, v6

    move-object/from16 v0, v27

    invoke-static {v4, v6, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.Outgoing.ServAddr_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 610
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 620
    :cond_a
    :goto_a
    const-string v4, "Port"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v21

    .line 621
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v29

    .line 622
    if-eqz v29, :cond_b

    const/16 v4, 0x9

    aget-object v4, v32, v4

    if-eqz v4, :cond_b

    .line 623
    const/16 v4, 0x9

    aget-object v4, v32, v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_17

    .line 624
    const-string v4, "Settings.Messages.Email.Account.Outgoing.Port"

    const/16 v6, 0x9

    aget-object v6, v32, v6

    move-object/from16 v0, v29

    invoke-static {v4, v6, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.Outgoing.Port_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 628
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 638
    :cond_b
    :goto_b
    const-string v4, "SmtpAuth"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v21

    .line 639
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v31

    .line 640
    if-eqz v31, :cond_c

    const/16 v4, 0xa

    aget-object v4, v32, v4

    if-eqz v4, :cond_c

    .line 641
    const/16 v4, 0xa

    aget-object v4, v32, v4

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_18

    .line 642
    const-string v4, "Settings.Messages.Email.Account.Outgoing.SmtpAuth"

    const/16 v6, 0xa

    aget-object v6, v32, v6

    move-object/from16 v0, v31

    invoke-static {v4, v6, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.Outgoing.SmtpAuth_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 646
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 656
    :cond_c
    :goto_c
    const-string v4, "Secure"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v21

    .line 657
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v24

    .line 658
    if-eqz v24, :cond_d

    const/16 v4, 0xb

    aget-object v4, v32, v4

    if-eqz v4, :cond_d

    .line 659
    const/16 v4, 0xb

    aget-object v4, v32, v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_19

    .line 660
    const-string v4, "Settings.Messages.Email.Account.Outgoing.Secure"

    const/16 v6, 0xb

    aget-object v6, v32, v6

    move-object/from16 v0, v24

    invoke-static {v4, v6, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Messages.Email.Account.Outgoing.Secure_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 664
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscEmailCustomer : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 670
    :cond_d
    :goto_d
    const-string v4, "CSC_LOG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Finished Email Account["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    .end local v8    # "accountName":Ljava/lang/String;
    .end local v14    # "emailAddr":Ljava/lang/String;
    .end local v16    # "incoming_child_node":Lorg/w3c/dom/Node;
    .end local v17    # "incoming_node":Lorg/w3c/dom/Node;
    .end local v19    # "networkName":Ljava/lang/String;
    .end local v21    # "outcoming_child_node":Lorg/w3c/dom/Node;
    .end local v22    # "outcoming_node":Lorg/w3c/dom/Node;
    .end local v32    # "splitRcvStr":[Ljava/lang/String;
    .end local v33    # "userName":Ljava/lang/String;
    :goto_e
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    .line 432
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_1

    .line 462
    .restart local v19    # "networkName":Ljava/lang/String;
    .restart local v32    # "splitRcvStr":[Ljava/lang/String;
    :cond_e
    const-string v4, "Settings.Messages.Email.Account.NetworkName"

    move-object/from16 v0, v19

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_2

    .line 680
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    .end local v3    # "EmailAccount_PREFERENCE":Landroid/net/Uri;
    .end local v5    # "CustomerCount":Ljava/lang/String;
    .end local v9    # "accountNodeListChild":Lorg/w3c/dom/Node;
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v15    # "i":I
    .end local v19    # "networkName":Ljava/lang/String;
    .end local v23    # "rcv_cmp":Ljava/lang/String;
    .end local v24    # "secureType":Ljava/lang/String;
    .end local v26    # "serverAddrIncoming":Ljava/lang/String;
    .end local v27    # "serverAddrOutgoing":Ljava/lang/String;
    .end local v28    # "serverPortIncoming":Ljava/lang/String;
    .end local v29    # "serverPortOutgoing":Ljava/lang/String;
    .end local v30    # "serverType":Ljava/lang/String;
    .end local v31    # "smtpAuth":Ljava/lang/String;
    .end local v32    # "splitRcvStr":[Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 681
    .local v13, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "CSC_DEBUG"

    const-string v6, "NameNotFoundException---------------------->>>>>>>"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    const-string v4, "CscEmailCustomer : NOCSC"

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 683
    const-string v4, "NOCSC_EmalCust"

    goto/16 :goto_0

    .line 478
    .end local v13    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "resolver":Landroid/content/ContentResolver;
    .restart local v3    # "EmailAccount_PREFERENCE":Landroid/net/Uri;
    .restart local v5    # "CustomerCount":Ljava/lang/String;
    .restart local v8    # "accountName":Ljava/lang/String;
    .restart local v9    # "accountNodeListChild":Lorg/w3c/dom/Node;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v15    # "i":I
    .restart local v19    # "networkName":Ljava/lang/String;
    .restart local v23    # "rcv_cmp":Ljava/lang/String;
    .restart local v24    # "secureType":Ljava/lang/String;
    .restart local v26    # "serverAddrIncoming":Ljava/lang/String;
    .restart local v27    # "serverAddrOutgoing":Ljava/lang/String;
    .restart local v28    # "serverPortIncoming":Ljava/lang/String;
    .restart local v29    # "serverPortOutgoing":Ljava/lang/String;
    .restart local v30    # "serverType":Ljava/lang/String;
    .restart local v31    # "smtpAuth":Ljava/lang/String;
    .restart local v32    # "splitRcvStr":[Ljava/lang/String;
    :cond_f
    :try_start_1
    const-string v4, "Settings.Messages.Email.Account.AccountName"

    invoke-static {v4, v8}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_3

    .line 684
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    .end local v3    # "EmailAccount_PREFERENCE":Landroid/net/Uri;
    .end local v5    # "CustomerCount":Ljava/lang/String;
    .end local v8    # "accountName":Ljava/lang/String;
    .end local v9    # "accountNodeListChild":Lorg/w3c/dom/Node;
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v15    # "i":I
    .end local v19    # "networkName":Ljava/lang/String;
    .end local v23    # "rcv_cmp":Ljava/lang/String;
    .end local v24    # "secureType":Ljava/lang/String;
    .end local v26    # "serverAddrIncoming":Ljava/lang/String;
    .end local v27    # "serverAddrOutgoing":Ljava/lang/String;
    .end local v28    # "serverPortIncoming":Ljava/lang/String;
    .end local v29    # "serverPortOutgoing":Ljava/lang/String;
    .end local v30    # "serverType":Ljava/lang/String;
    .end local v31    # "smtpAuth":Ljava/lang/String;
    .end local v32    # "splitRcvStr":[Ljava/lang/String;
    :catch_1
    move-exception v13

    .line 685
    .local v13, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v4, "CSC_DEBUG"

    const-string v6, "IndexOutOfBoundsException---------------------->>>>>>>"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    const-string v4, "CscEmailCustomer : NOCSC"

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 687
    const-string v4, "NOCSC_EmalCust"

    goto/16 :goto_0

    .line 494
    .end local v13    # "e":Ljava/lang/IndexOutOfBoundsException;
    .restart local v2    # "resolver":Landroid/content/ContentResolver;
    .restart local v3    # "EmailAccount_PREFERENCE":Landroid/net/Uri;
    .restart local v5    # "CustomerCount":Ljava/lang/String;
    .restart local v8    # "accountName":Ljava/lang/String;
    .restart local v9    # "accountNodeListChild":Lorg/w3c/dom/Node;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v14    # "emailAddr":Ljava/lang/String;
    .restart local v15    # "i":I
    .restart local v19    # "networkName":Ljava/lang/String;
    .restart local v23    # "rcv_cmp":Ljava/lang/String;
    .restart local v24    # "secureType":Ljava/lang/String;
    .restart local v26    # "serverAddrIncoming":Ljava/lang/String;
    .restart local v27    # "serverAddrOutgoing":Ljava/lang/String;
    .restart local v28    # "serverPortIncoming":Ljava/lang/String;
    .restart local v29    # "serverPortOutgoing":Ljava/lang/String;
    .restart local v30    # "serverType":Ljava/lang/String;
    .restart local v31    # "smtpAuth":Ljava/lang/String;
    .restart local v32    # "splitRcvStr":[Ljava/lang/String;
    :cond_10
    :try_start_2
    const-string v4, "Settings.Messages.Email.Account.EmailAddr"

    invoke-static {v4, v14}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 508
    .restart local v33    # "userName":Ljava/lang/String;
    :cond_11
    const-string v4, "Settings.Messages.Email.Account.LoginType"

    move-object/from16 v0, v33

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 533
    .restart local v16    # "incoming_child_node":Lorg/w3c/dom/Node;
    .restart local v17    # "incoming_node":Lorg/w3c/dom/Node;
    :cond_12
    const-string v4, "Settings.Messages.Email.Account.Incoming.MailboxType"

    move-object/from16 v0, v30

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 551
    :cond_13
    const-string v4, "Settings.Messages.Email.Account.Incoming.ServAddr"

    move-object/from16 v0, v26

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 570
    :cond_14
    const-string v4, "Settings.Messages.Email.Account.Incoming.Port"

    move-object/from16 v0, v28

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 588
    :cond_15
    const-string v4, "Settings.Messages.Email.Account.Incoming.Secure"

    move-object/from16 v0, v24

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 613
    .restart local v21    # "outcoming_child_node":Lorg/w3c/dom/Node;
    .restart local v22    # "outcoming_node":Lorg/w3c/dom/Node;
    :cond_16
    const-string v4, "Settings.Messages.Email.Account.Outgoing.ServAddr"

    move-object/from16 v0, v27

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 631
    :cond_17
    const-string v4, "Settings.Messages.Email.Account.Outgoing.Port"

    move-object/from16 v0, v29

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 649
    :cond_18
    const-string v4, "Settings.Messages.Email.Account.Outgoing.SmtpAuth"

    move-object/from16 v0, v31

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 667
    :cond_19
    const-string v4, "Settings.Messages.Email.Account.Outgoing.Secure"

    move-object/from16 v0, v24

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 672
    .end local v8    # "accountName":Ljava/lang/String;
    .end local v14    # "emailAddr":Ljava/lang/String;
    .end local v16    # "incoming_child_node":Lorg/w3c/dom/Node;
    .end local v17    # "incoming_node":Lorg/w3c/dom/Node;
    .end local v19    # "networkName":Ljava/lang/String;
    .end local v21    # "outcoming_child_node":Lorg/w3c/dom/Node;
    .end local v22    # "outcoming_node":Lorg/w3c/dom/Node;
    .end local v32    # "splitRcvStr":[Ljava/lang/String;
    .end local v33    # "userName":Ljava/lang/String;
    :cond_1a
    const-string v4, "CSC_LOG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Passed Email Account["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] => From Emaildata = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_e

    .line 677
    .end local v9    # "accountNodeListChild":Lorg/w3c/dom/Node;
    .end local v15    # "i":I
    .end local v23    # "rcv_cmp":Ljava/lang/String;
    .end local v24    # "secureType":Ljava/lang/String;
    .end local v26    # "serverAddrIncoming":Ljava/lang/String;
    .end local v27    # "serverAddrOutgoing":Ljava/lang/String;
    .end local v28    # "serverPortIncoming":Ljava/lang/String;
    .end local v29    # "serverPortOutgoing":Ljava/lang/String;
    .end local v30    # "serverType":Ljava/lang/String;
    .end local v31    # "smtpAuth":Ljava/lang/String;
    :cond_1b
    invoke-interface {v12}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1c
    move-object v4, v11

    .line 679
    goto/16 :goto_0
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 0
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 127
    return-void
.end method

.method public getTagCount(Lorg/w3c/dom/NodeList;)I
    .locals 3
    .param p1, "list"    # Lorg/w3c/dom/NodeList;

    .prologue
    .line 252
    const/4 v0, 0x0

    .line 254
    .local v0, "count":I
    if-eqz p1, :cond_0

    .line 255
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    .line 256
    const-string v1, "CSC_DEBUG"

    const-string v2, "getTagCount----------------------"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    :cond_0
    return v0
.end method

.method public getTagList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;
    .locals 7
    .param p1, "parent"    # Lorg/w3c/dom/Node;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 225
    const-string v5, "CSC_DEBUG"

    const-string v6, "getTagList---------------------"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    sget-object v5, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mDoc:Lorg/w3c/dom/Document;

    if-eqz v5, :cond_0

    if-nez p1, :cond_1

    .line 227
    :cond_0
    const/4 v5, 0x0

    .line 240
    :goto_0
    return-object v5

    .line 229
    :cond_1
    sget-object v5, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mDoc:Lorg/w3c/dom/Document;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v3

    .line 230
    .local v3, "list":Lorg/w3c/dom/Element;
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 231
    .local v1, "children":Lorg/w3c/dom/NodeList;
    if-eqz v1, :cond_3

    .line 232
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 233
    .local v4, "n":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_3

    .line 234
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 235
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 236
    invoke-interface {v3, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 233
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 240
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v2    # "i":I
    .end local v4    # "n":I
    :cond_3
    invoke-interface {v3}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    goto :goto_0
.end method

.method public getTagNode(Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 6
    .param p1, "tagFullName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 182
    const-string v4, "CSC_DEBUG"

    const-string v5, "getTagNode---------------------"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mRoot:Lorg/w3c/dom/Node;

    if-nez v4, :cond_1

    move-object v0, v3

    .line 198
    :cond_0
    :goto_0
    return-object v0

    .line 187
    :cond_1
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mRoot:Lorg/w3c/dom/Node;

    .line 188
    .local v0, "node":Lorg/w3c/dom/Node;
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v4, "."

    invoke-direct {v2, p1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    .local v2, "tokenizer":Ljava/util/StringTokenizer;
    :goto_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 191
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 193
    .local v1, "tagName":Ljava/lang/String;
    if-nez v0, :cond_2

    move-object v0, v3

    .line 194
    goto :goto_0

    .line 196
    :cond_2
    invoke-virtual {p0, v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 197
    goto :goto_1
.end method

.method public getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 9
    .param p1, "parent"    # Lorg/w3c/dom/Node;
    .param p2, "tagName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 203
    const-string v6, "CSC_DEBUG"

    const-string v7, "getTagNode---------------------"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    if-nez p1, :cond_1

    move-object v0, v5

    .line 221
    :cond_0
    :goto_0
    return-object v0

    .line 208
    :cond_1
    :try_start_0
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 209
    .local v1, "children":Lorg/w3c/dom/NodeList;
    if-eqz v1, :cond_2

    .line 210
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 211
    .local v4, "n":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v4, :cond_2

    .line 212
    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 213
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_0

    .line 211
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 218
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v1    # "children":Lorg/w3c/dom/NodeList;
    .end local v3    # "i":I
    .end local v4    # "n":I
    :catch_0
    move-exception v2

    .line 219
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "CSC_DEBUG"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getTagNode: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    move-object v0, v5

    .line 221
    goto :goto_0
.end method

.method public getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 2
    .param p1, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 244
    const-string v0, "CSC_DEBUG"

    const-string v1, "getTagValue----------------------"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    if-nez p1, :cond_0

    .line 246
    const/4 v0, 0x0

    .line 248
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public loadXMLFile()V
    .locals 6

    .prologue
    .line 137
    :try_start_0
    const-string v3, "CSC_DEBUG"

    const-string v4, "loadXMLFilezzzz--------start----------"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    const-string v3, "customer.xml"

    invoke-static {v3}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getCustomerFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mFilePath:Ljava/lang/String;

    .line 139
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    .line 140
    .local v2, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 141
    .local v0, "builder":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mFilePath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v3

    sput-object v3, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mDoc:Lorg/w3c/dom/Document;

    .line 142
    sget-object v3, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mDoc:Lorg/w3c/dom/Document;

    invoke-interface {v3}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v3

    sput-object v3, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mRoot:Lorg/w3c/dom/Node;

    .line 143
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->setAccountSetupCustomer()V

    .line 144
    const-string v3, "CSC_DEBUG"

    const-string v4, "loadXMLFilezzz--------end----------"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 152
    .end local v0    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v2    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :goto_0
    return-void

    .line 145
    :catch_0
    move-exception v1

    .line 146
    .local v1, "ex":Ljavax/xml/parsers/ParserConfigurationException;
    const-string v3, "CSC_DEBUG"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ParserConfigurationException:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 147
    .end local v1    # "ex":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_1
    move-exception v1

    .line 148
    .local v1, "ex":Lorg/xml/sax/SAXException;
    const-string v3, "CSC_DEBUG"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SAXException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 149
    .end local v1    # "ex":Lorg/xml/sax/SAXException;
    :catch_2
    move-exception v1

    .line 150
    .local v1, "ex":Ljava/io/IOException;
    const-string v3, "CSC_DEBUG"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setAccountSetupCustomer()V
    .locals 2

    .prologue
    .line 262
    const-string v0, "Settings.Messages.Email"

    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerNode:Lorg/w3c/dom/Node;

    .line 264
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerNode:Lorg/w3c/dom/Node;

    const-string v1, "Account"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    .line 265
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagCount(Lorg/w3c/dom/NodeList;)I

    move-result v0

    sput v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerCount:I

    .line 266
    const-string v0, "CSC_DEBUG"

    const-string v1, "setAccountSetupCustomer----------------------"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    return-void
.end method

.method public update()V
    .locals 27

    .prologue
    .line 276
    const-string v22, "CSC_DEBUG"

    const-string v23, "started getProviderCustomer function"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    new-instance v3, Landroid/content/Intent;

    const-string v22, "com.android.email.service.EmailAccountSetupReceiver"

    move-object/from16 v0, v22

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 279
    .local v3, "Emailintent":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->loadXMLFile()V

    .line 281
    const-string v22, "CSC_DEBUG"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "mCustomerCount: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget v24, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerCount:I

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    const-string v13, "NONE"

    .line 283
    .local v13, "send_data":Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    .line 285
    .local v20, "typesbuf":Ljava/lang/StringBuffer;
    const/16 v18, 0x0

    .line 286
    .local v18, "serverType":Ljava/lang/String;
    const/4 v14, 0x0

    .line 287
    .local v14, "serverAddrIncoming":Ljava/lang/String;
    const/4 v15, 0x0

    .line 288
    .local v15, "serverAddrOutgoing":Ljava/lang/String;
    const/16 v16, 0x0

    .line 289
    .local v16, "serverPortIncoming":Ljava/lang/String;
    const/16 v17, 0x0

    .line 290
    .local v17, "serverPortOutgoing":Ljava/lang/String;
    const/16 v19, 0x0

    .line 292
    .local v19, "smtpAuth":Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    sget v22, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerCount:I

    move/from16 v0, v22

    if-ge v7, v0, :cond_e

    .line 293
    const-string v22, "CSC_DEBUG"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "started getProviderCustomer i : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 296
    .local v5, "accountNodeListChild":Lorg/w3c/dom/Node;
    const-string v13, "NONE"

    .line 298
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "NetworkName"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v10

    .line 299
    .local v10, "networkName":Ljava/lang/String;
    if-nez v10, :cond_0

    .line 300
    const-string v10, ""

    .line 301
    :cond_0
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "AccountName"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 302
    .local v4, "accountName":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 303
    const-string v4, ""

    .line 304
    :cond_1
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "EmailAddr"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    .line 305
    .local v6, "emailAddr":Ljava/lang/String;
    if-nez v6, :cond_2

    .line 306
    const-string v6, ""

    .line 307
    :cond_2
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "LoginType"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v21

    .line 308
    .local v21, "userName":Ljava/lang/String;
    if-nez v21, :cond_3

    .line 309
    const-string v21, ""

    .line 311
    :cond_3
    const-string v22, "CSC_DEBUG"

    const-string v23, "networkName, accountName,emailAddr, userName  "

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "Incoming"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    .line 314
    .local v8, "incomingList":Lorg/w3c/dom/NodeList;
    if-nez v8, :cond_4

    .line 315
    const-string v22, "CscEmailCustomer "

    const-string v23, "incomingList == null"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->csc_context:Landroid/content/Context;

    move-object/from16 v22, v0

    const-string v23, "TYPE"

    const-string v24, "STRING"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v23

    const-string v24, "KEY"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Saved Account["

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "]"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v23

    const-string v24, "VALUE"

    const-string v25, "incomingList == NULL"

    invoke-virtual/range {v23 .. v25}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 319
    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 292
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 323
    :cond_4
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v8, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "MailboxType"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v18

    .line 324
    if-nez v18, :cond_5

    .line 325
    const-string v18, ""

    .line 326
    :cond_5
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v8, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "ServAddr"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v14

    .line 327
    if-nez v14, :cond_6

    .line 328
    const-string v14, ""

    .line 329
    :cond_6
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v8, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "Port"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v16

    .line 330
    if-nez v16, :cond_7

    .line 331
    const-string v16, ""

    .line 332
    :cond_7
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v8, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "Secure"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    .line 333
    .local v9, "incomingSecureType":Ljava/lang/String;
    if-nez v9, :cond_8

    .line 334
    const-string v9, ""

    .line 335
    :cond_8
    const-string v22, "CSC_DEBUG"

    const-string v23, "incomingList"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->mCustomerList:Lorg/w3c/dom/NodeList;

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "Outgoing"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v11

    .line 338
    .local v11, "outgoingList":Lorg/w3c/dom/NodeList;
    if-nez v11, :cond_9

    .line 339
    const-string v22, "CscEmailCustomer "

    const-string v23, "outgoingList == null"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->csc_context:Landroid/content/Context;

    move-object/from16 v22, v0

    const-string v23, "TYPE"

    const-string v24, "STRING"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v23

    const-string v24, "KEY"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Saved Account["

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "]"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v23

    const-string v24, "VALUE"

    const-string v25, "outgoingList == NULL"

    invoke-virtual/range {v23 .. v25}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 343
    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    goto/16 :goto_1

    .line 347
    :cond_9
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v11, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "ServAddr"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v15

    .line 348
    if-nez v15, :cond_a

    .line 349
    const-string v15, ""

    .line 350
    :cond_a
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v11, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "Port"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v17

    .line 351
    if-nez v17, :cond_b

    .line 352
    const-string v17, ""

    .line 353
    :cond_b
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v11, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "SmtpAuth"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    .line 354
    if-nez v19, :cond_c

    .line 355
    const-string v19, ""

    .line 356
    :cond_c
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v11, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    const-string v23, "Secure"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->getTagValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v12

    .line 357
    .local v12, "outgoingSecureType":Ljava/lang/String;
    if-nez v12, :cond_d

    .line 358
    const-string v12, ""

    .line 359
    :cond_d
    const-string v22, "CSC_DEBUG"

    const-string v23, "outgoingList"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string v23, "#"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    .line 367
    const-string v22, "CSC_DEBUG"

    const-string v23, "send_data"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;->csc_context:Landroid/content/Context;

    move-object/from16 v22, v0

    const-string v23, "TYPE"

    const-string v24, "STRING"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v23

    const-string v24, "KEY"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Saved Account["

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "]"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v23

    const-string v24, "VALUE"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 371
    const-string v22, "CSC_DEBUG"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "EMAIL DATA FOR VERIFY = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 374
    const-string v22, "CSC_DEBUG"

    const-string v23, "and buff"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 377
    .end local v4    # "accountName":Ljava/lang/String;
    .end local v5    # "accountNodeListChild":Lorg/w3c/dom/Node;
    .end local v6    # "emailAddr":Ljava/lang/String;
    .end local v8    # "incomingList":Lorg/w3c/dom/NodeList;
    .end local v9    # "incomingSecureType":Ljava/lang/String;
    .end local v10    # "networkName":Ljava/lang/String;
    .end local v11    # "outgoingList":Lorg/w3c/dom/NodeList;
    .end local v12    # "outgoingSecureType":Ljava/lang/String;
    .end local v21    # "userName":Ljava/lang/String;
    :cond_e
    return-void
.end method
