.class public Lcom/samsung/sec/android/application/csc/CscEncodeService;
.super Landroid/app/Service;
.source "CscEncodeService.java"


# instance fields
.field private mCscXMLEncoder:Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

.field mEncodables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscTgBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/sec/android/application/csc/CscEncodeService;)Lcom/samsung/sec/android/application/csc/CscXMLEncoder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscEncodeService;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscEncodeService;->mCscXMLEncoder:Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/sec/android/application/csc/CscEncodeService;Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)Lcom/samsung/sec/android/application/csc/CscXMLEncoder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscEncodeService;
    .param p1, "x1"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscEncodeService;->mCscXMLEncoder:Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    return-object p1
.end method

.method private doEncode()V
    .locals 3

    .prologue
    .line 38
    const-string v1, "CscEncodeService"

    const-string v2, "doEncode()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-static {p0}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscTgManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscEncodeService;->mEncodables:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscEncodeService$1;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscEncodeService$1;-><init>(Lcom/samsung/sec/android/application/csc/CscEncodeService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 58
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 59
    return-void
.end method


# virtual methods
.method copyOrgCustomerXml()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    .line 62
    const/4 v7, 0x0

    .line 63
    .local v7, "inputStream":Ljava/io/FileInputStream;
    const/4 v9, 0x0

    .line 64
    .local v9, "outputStream":Ljava/io/FileOutputStream;
    const/4 v1, 0x0

    .line 65
    .local v1, "fcin":Ljava/nio/channels/FileChannel;
    const/4 v6, 0x0

    .line 68
    .local v6, "fcout":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    const-string v2, "/system/csc/customer.xml"

    invoke-direct {v8, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .local v8, "inputStream":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v10, Ljava/io/FileOutputStream;

    const-string v2, "/sdcard/customer_org.xml"

    invoke-direct {v10, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 70
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .local v10, "outputStream":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 71
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    .line 72
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    .line 74
    .local v4, "size":J
    const-wide/16 v2, 0x0

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    move-result-wide v2

    cmp-long v2, v2, v12

    if-nez v2, :cond_0

    .line 75
    const-string v2, "CscEncodeService"

    const-string v3, "copyOrgCustomerXml() - fcin.transferTo 0 byte"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 81
    :cond_0
    if-eqz v1, :cond_1

    .line 82
    :try_start_3
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 83
    :cond_1
    if-eqz v8, :cond_2

    .line 84
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 89
    :cond_2
    :goto_0
    if-eqz v6, :cond_3

    .line 90
    :try_start_4
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V

    .line 91
    :cond_3
    if-eqz v10, :cond_4

    .line 92
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_4
    move-object v9, v10

    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 97
    .end local v4    # "size":J
    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    :cond_5
    :goto_1
    return-void

    .line 85
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "size":J
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 86
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "CscEncodeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "copyOrgCustomerXml() InClose - Exception:"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 93
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 94
    .restart local v0    # "ex":Ljava/lang/Exception;
    const-string v2, "CscEncodeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "copyOrgCustomerXml() OutClose - Exception:"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v9, v10

    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 96
    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_1

    .line 77
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v4    # "size":J
    :catch_2
    move-exception v0

    .line 78
    .restart local v0    # "ex":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    const-string v2, "CscEncodeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "copyOrgCustomerXml() - Exception:"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 81
    if-eqz v1, :cond_6

    .line 82
    :try_start_6
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 83
    :cond_6
    if-eqz v7, :cond_7

    .line 84
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 89
    :cond_7
    :goto_3
    if-eqz v6, :cond_8

    .line 90
    :try_start_7
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V

    .line 91
    :cond_8
    if-eqz v9, :cond_5

    .line 92
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 93
    :catch_3
    move-exception v0

    .line 94
    const-string v2, "CscEncodeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "copyOrgCustomerXml() OutClose - Exception:"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 85
    :catch_4
    move-exception v0

    .line 86
    const-string v2, "CscEncodeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "copyOrgCustomerXml() InClose - Exception:"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 80
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 81
    :goto_4
    if-eqz v1, :cond_9

    .line 82
    :try_start_8
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 83
    :cond_9
    if-eqz v7, :cond_a

    .line 84
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    .line 89
    :cond_a
    :goto_5
    if-eqz v6, :cond_b

    .line 90
    :try_start_9
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V

    .line 91
    :cond_b
    if-eqz v9, :cond_c

    .line 92
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    .line 95
    :cond_c
    :goto_6
    throw v2

    .line 85
    :catch_5
    move-exception v0

    .line 86
    .restart local v0    # "ex":Ljava/lang/Exception;
    const-string v3, "CscEncodeService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "copyOrgCustomerXml() InClose - Exception:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v3, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 93
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_6
    move-exception v0

    .line 94
    .restart local v0    # "ex":Ljava/lang/Exception;
    const-string v3, "CscEncodeService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "copyOrgCustomerXml() OutClose - Exception:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v3, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 80
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v2

    move-object v7, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v2

    move-object v9, v10

    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_4

    .line 77
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catch_7
    move-exception v0

    move-object v7, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    :catch_8
    move-exception v0

    move-object v9, v10

    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 29
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 33
    const-string v0, "CscEncodeService"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscEncodeService;->doEncode()V

    .line 35
    return-void
.end method
