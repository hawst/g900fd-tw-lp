.class public Lcom/samsung/sec/android/application/csc/CscGPS;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscGPS.java"


# instance fields
.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final OTHERS_CSC_FILE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field mExtra:Landroid/os/Bundle;

.field private mResolver:Landroid/content/ContentResolver;

.field private sPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 44
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 48
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->OTHERS_CSC_FILE:Ljava/lang/String;

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->sPrefs:Landroid/content/SharedPreferences;

    .line 90
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mExtra:Landroid/os/Bundle;

    .line 93
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mResolver:Landroid/content/ContentResolver;

    .line 94
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mContext:Landroid/content/Context;

    .line 96
    return-void
.end method


# virtual methods
.method public GPSUpdate()V
    .locals 4

    .prologue
    .line 168
    const-string v1, "CscGPS"

    const-string v2, "CSCGPS.GPSUpdate"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mContext:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 172
    .local v0, "lm":Landroid/location/LocationManager;
    const-string v1, "gps"

    const-string v2, "set_csc_parameters"

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mExtra:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, v3}, Landroid/location/LocationManager;->sendExtraCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 173
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    const-string v0, "NOERROR"

    .line 177
    .local v0, "answer":Ljava/lang/String;
    return-object v0
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 0
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 186
    return-void
.end method

.method public update()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 101
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 104
    .local v0, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const-string v2, "CscGPS"

    const-string v3, "CSCGPS.updateParameters"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    if-nez v0, :cond_0

    .line 164
    :goto_0
    return-void

    .line 110
    :cond_0
    const-string v2, "Settings.GPS.AGPS.ServAddr"

    invoke-virtual {v0, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 111
    .local v1, "valueStr":Ljava/lang/String;
    const-string v2, "CscGPS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "supl host : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    if-eqz v1, :cond_3

    .line 113
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mExtra:Landroid/os/Bundle;

    const-string v3, "operation_mode"

    const-string v4, "MSBASED"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mExtra:Landroid/os/Bundle;

    const-string v3, "hslp_addr"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v2, "geoloc2.sfr.fr"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 116
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mExtra:Landroid/os/Bundle;

    const-string v3, "ssl"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 126
    :goto_1
    const-string v2, "Settings.GPS.AGPS.SuplVer"

    invoke-virtual {v0, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 127
    const-string v2, "CscGPS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "supl_ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    if-eqz v1, :cond_5

    .line 129
    const-string v2, "2"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "2.0"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 130
    :cond_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mExtra:Landroid/os/Bundle;

    const-string v3, "supl_ver"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    :goto_2
    const-string v2, "Settings.GPS.AGPS.Port"

    invoke-virtual {v0, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 141
    const-string v2, "CscGPS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "supl port : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    if-eqz v1, :cond_6

    .line 143
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mExtra:Landroid/os/Bundle;

    const-string v3, "hslp_port"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 149
    :goto_3
    const-string v2, "Settings.GPS.AGPS.LPP"

    invoke-virtual {v0, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 150
    const-string v2, "CscGPS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LPP : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    if-eqz v1, :cond_7

    .line 152
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mExtra:Landroid/os/Bundle;

    const-string v3, "LPP"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :goto_4
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mExtra:Landroid/os/Bundle;

    invoke-virtual {v2}, Landroid/os/Bundle;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 159
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscGPS;->GPSUpdate()V

    goto/16 :goto_0

    .line 118
    :cond_2
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mExtra:Landroid/os/Bundle;

    const-string v3, "ssl"

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 121
    :cond_3
    const-string v2, "CscGPS"

    const-string v3, "SUPL Host is null or invalid"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 132
    :cond_4
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscGPS;->mExtra:Landroid/os/Bundle;

    const-string v3, "supl_ver"

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    .line 135
    :cond_5
    const-string v2, "CscGPS"

    const-string v3, "SUPL Ver is null or invalid"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 145
    :cond_6
    const-string v2, "CscGPS"

    const-string v3, "SUPL PORT is null or invalid"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 154
    :cond_7
    const-string v2, "CscGPS"

    const-string v3, "LPP is null or invalid"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 161
    :cond_8
    const-string v2, "CscGPS"

    const-string v3, "CSC don\'t have any AGPS value."

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
