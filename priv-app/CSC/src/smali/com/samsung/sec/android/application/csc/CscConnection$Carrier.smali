.class Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
.super Ljava/lang/Object;
.source "CscConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Carrier"
.end annotation


# instance fields
.field apn:Ljava/lang/String;

.field authType:I

.field bearer:I

.field carrier:Ljava/lang/String;

.field carrierEnabled:Z

.field carrierEnabledRoaming:Z

.field editable:Z

.field homeurl:Ljava/lang/String;

.field maxConns:I

.field maxConnsTime:I

.field mmsPort:Ljava/lang/String;

.field mmsProxy:Ljava/lang/String;

.field mmsc:Ljava/lang/String;

.field modemCognitive:I

.field mtu:I

.field mvnoMatchData:Ljava/lang/String;

.field mvnoType:Ljava/lang/String;

.field numeric:Ljava/lang/String;

.field nwkname:Ljava/lang/String;

.field password:Ljava/lang/String;

.field port:Ljava/lang/String;

.field preferred:Z

.field profileId:I

.field protocol:Ljava/lang/String;

.field proxy:Ljava/lang/String;

.field roamingProtocol:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscConnection;

.field types:[Ljava/lang/String;

.field user:Ljava/lang/String;

.field waitTime:I


# direct methods
.method private constructor <init>(Lcom/samsung/sec/android/application/csc/CscConnection;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->this$0:Lcom/samsung/sec/android/application/csc/CscConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/sec/android/application/csc/CscConnection;Lcom/samsung/sec/android/application/csc/CscConnection$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/sec/android/application/csc/CscConnection;
    .param p2, "x1"    # Lcom/samsung/sec/android/application/csc/CscConnection$1;

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;-><init>(Lcom/samsung/sec/android/application/csc/CscConnection;)V

    return-void
.end method
