.class public Lcom/samsung/sec/android/application/csc/CscBrowser;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscBrowser.java"


# static fields
.field static final BROWSER_BOOKMARK_CONTENT_URI:Landroid/net/Uri;

.field static final BROWSER_OPBOOKMARK_CONTENT_URI:Landroid/net/Uri;

.field private static final SEND_PACKAGE_BROWSER:Landroid/content/ComponentName;

.field private static final SEND_PACKAGE_CHROME:Landroid/content/ComponentName;


# instance fields
.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final OTHERS_CSC_FILE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mHomepageUrlRcvr:Landroid/content/BroadcastReceiver;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 126
    const-string v0, "content://com.android.browser/bookmarks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->BROWSER_BOOKMARK_CONTENT_URI:Landroid/net/Uri;

    .line 129
    const-string v0, "content://com.android.browser/opbookmarks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->BROWSER_OPBOOKMARK_CONTENT_URI:Landroid/net/Uri;

    .line 134
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.browser"

    const-string v2, "com.android.browser.BrowserActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->SEND_PACKAGE_BROWSER:Landroid/content/ComponentName;

    .line 137
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.chrome"

    const-string v2, "com.android.chrome.Main"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->SEND_PACKAGE_CHROME:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 141
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 61
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 89
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->OTHERS_CSC_FILE:Ljava/lang/String;

    .line 118
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 120
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 142
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    .line 143
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mContext:Landroid/content/Context;

    .line 148
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mIntentFilter:Landroid/content/IntentFilter;

    .line 149
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.CSC_BROWSER_HOMEPAGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 150
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscBrowser$1;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscBrowser$1;-><init>(Lcom/samsung/sec/android/application/csc/CscBrowser;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mHomepageUrlRcvr:Landroid/content/BroadcastReceiver;

    .line 168
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/sec/android/application/csc/CscBrowser;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscBrowser;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/sec/android/application/csc/CscBrowser;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscBrowser;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mHomepageUrlRcvr:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method private getCurrentNetworkName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 885
    const-string v0, "none"

    .line 887
    .local v0, "currentNwtName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mContext:Landroid/content/Context;

    const-string v3, "simprof.preferences_name"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 889
    .local v1, "sp":Landroid/content/SharedPreferences;
    const-string v2, "simprof.key.nwkname"

    const-string v3, "none"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 891
    return-object v0
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 739
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Browser.Bookmark.Editable"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 740
    const-string v0, "Settings.Browser.Bookmark.Index"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 741
    const-string v0, "Settings.Browser.Bookmark.NetworkName"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 742
    const-string v0, "Settings.Browser.CookieOption"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 743
    const-string v0, "Settings.Browser.HistoryList"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 744
    const-string v0, "Settings.Main.Network.AutoBookmark"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 745
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 19

    .prologue
    .line 558
    const-string v1, "CSCBrowser"

    const-string v2, " ** compare runs.."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    new-instance v10, Ljava/lang/String;

    const-string v1, "NOERROR"

    invoke-direct {v10, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 566
    .local v10, "answer":Ljava/lang/String;
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 569
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v2, "BrowserData.HomePage.URL"

    invoke-virtual {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 571
    .local v14, "homeUrl":Ljava/lang/String;
    if-eqz v14, :cond_2

    .line 572
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mContext:Landroid/content/Context;

    const-string v2, "csc.preferences_name"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v17

    .line 573
    .local v17, "sp":Landroid/content/SharedPreferences;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v2, "BrowserData.HomePage.URL"

    invoke-virtual {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 576
    .local v18, "url":Ljava/lang/String;
    if-eqz v18, :cond_1

    .line 577
    const-string v1, "CSCBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " ** compare : homeUrl - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 583
    const-string v1, "CSCBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " ** compare : different homepage URL ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ").."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    const-string v1, "BrowserData.HomePage.URL"

    .line 733
    .end local v17    # "sp":Landroid/content/SharedPreferences;
    .end local v18    # "url":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 588
    .restart local v17    # "sp":Landroid/content/SharedPreferences;
    .restart local v18    # "url":Ljava/lang/String;
    :cond_0
    const-string v1, "CSCBrowser"

    const-string v2, " ** compare : Homepage NOERROR.."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    .end local v17    # "sp":Landroid/content/SharedPreferences;
    .end local v18    # "url":Ljava/lang/String;
    :goto_1
    const/4 v8, 0x0

    .line 601
    .local v8, "FOUND_NO":I
    const/4 v9, 0x1

    .line 602
    .local v9, "FOUND_TITLE":I
    const/4 v7, 0x2

    .line 603
    .local v7, "FOUND_ALL":I
    const/4 v13, 0x0

    .line 608
    .local v13, "fnd":I
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 610
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscBrowser;->getBookmarks()Ljava/util/ArrayList;

    move-result-object v11

    .line 612
    .local v11, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_b

    .line 618
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/samsung/sec/android/application/csc/CscBrowser;->BROWSER_BOOKMARK_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "title"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "url"

    aput-object v5, v3, v4

    const-string v4, "deleted = 0 AND parent IS NOT NULL"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 622
    .local v12, "dbCur":Landroid/database/Cursor;
    if-nez v12, :cond_3

    .line 623
    const-string v1, "CSCBrowser"

    const-string v2, "dbCur is null. so could not check the bookmarks in DB"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v10

    .line 624
    goto :goto_0

    .line 591
    .end local v7    # "FOUND_ALL":I
    .end local v8    # "FOUND_NO":I
    .end local v9    # "FOUND_TITLE":I
    .end local v11    # "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v12    # "dbCur":Landroid/database/Cursor;
    .end local v13    # "fnd":I
    .restart local v17    # "sp":Landroid/content/SharedPreferences;
    .restart local v18    # "url":Ljava/lang/String;
    :cond_1
    const-string v1, "CSCBrowser"

    const-string v2, " ** compare : homepage url in preference is null.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    const-string v1, "BrowserData.HomePage.URL"

    goto :goto_0

    .line 596
    .end local v17    # "sp":Landroid/content/SharedPreferences;
    .end local v18    # "url":Ljava/lang/String;
    :cond_2
    const-string v1, "CSCBrowser"

    const-string v2, " ** compare : homepage url in xml is null, NOERROR"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 627
    .restart local v7    # "FOUND_ALL":I
    .restart local v8    # "FOUND_NO":I
    .restart local v9    # "FOUND_TITLE":I
    .restart local v11    # "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "dbCur":Landroid/database/Cursor;
    .restart local v13    # "fnd":I
    :cond_3
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    if-eq v1, v2, :cond_4

    .line 628
    const-string v1, "CSCBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "compare dbCur.getCount() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bookmarks.size()/2 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    new-instance v10, Ljava/lang/String;

    .end local v10    # "answer":Ljava/lang/String;
    const-string v1, "Bookmarks count not matching"

    invoke-direct {v10, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 631
    .restart local v10    # "answer":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bookmarkFromDb: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BookmarkFromCsc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v10, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    move-object v1, v10

    .line 634
    goto/16 :goto_0

    .line 638
    :cond_4
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_a

    .line 642
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 652
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_2
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v15, v1, :cond_6

    .line 653
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_3
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v1

    move/from16 v0, v16

    if-ge v0, v1, :cond_5

    .line 661
    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 662
    const/4 v13, 0x1

    .line 673
    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 674
    const/4 v13, 0x2

    .line 677
    const-string v2, "Settings.Browser.Bookmark.BookmarkName"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v3, v15, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") BookmarkName: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    const-string v2, "Settings.Browser.Bookmark.URL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v3, v15, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") URL: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    :cond_5
    const/4 v1, 0x1

    if-ne v13, v1, :cond_8

    .line 693
    const-string v1, "CSCBrowser"

    const-string v2, " ** compare : Bookmark URL is missied.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    const-string v1, "CSCBrowser : ** compare : Bookmark URL is missied.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 697
    const-string v10, "Settings.Browser.Bookmark.URL"

    .line 699
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-int/lit8 v2, v15, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v10, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    .end local v16    # "j":I
    :cond_6
    :goto_4
    const-string v1, "CSCBrowser"

    const-string v2, " ** compare : Bookmarks DONE.."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    .end local v15    # "i":I
    :goto_5
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 728
    const-string v1, "Settings.Browser.eManual.URL"

    const-string v2, "eManual.URL pass"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .end local v12    # "dbCur":Landroid/database/Cursor;
    :goto_6
    move-object v1, v10

    .line 733
    goto/16 :goto_0

    .line 689
    .restart local v12    # "dbCur":Landroid/database/Cursor;
    .restart local v15    # "i":I
    .restart local v16    # "j":I
    :cond_7
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    .line 653
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_3

    .line 703
    :cond_8
    if-nez v13, :cond_9

    .line 704
    const-string v1, "CSCBrowser"

    const-string v2, " ** compare : Bookmark Name is missed.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    const-string v1, "CSCBrowser : ** compare : Bookmark Name is missed.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 708
    const-string v10, "Settings.Browser.Bookmark.BookmarkName"

    .line 710
    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v10, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 714
    :cond_9
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 715
    const/4 v13, 0x0

    .line 652
    add-int/lit8 v15, v15, 0x2

    goto/16 :goto_2

    .line 721
    .end local v15    # "i":I
    .end local v16    # "j":I
    :cond_a
    const-string v1, "CSCBrowser"

    const-string v2, " ** compare : Browser DB is empty.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    const-string v1, "CSCBrowser : ** compare : Browser DB is empty.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 725
    const-string v10, "Settings.Browser."

    goto :goto_5

    .line 730
    .end local v12    # "dbCur":Landroid/database/Cursor;
    :cond_b
    const-string v1, "CSCBrowser"

    const-string v2, " ** compare : No bookmarks were given. NOERROR.."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 12
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 848
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "title"

    aput-object v3, v2, v10

    const-string v3, "url"

    aput-object v3, v2, v11

    const/4 v3, 0x2

    const-string v5, "bookmark"

    aput-object v5, v2, v3

    const-string v3, "bookmark == 1"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 853
    .local v8, "dbCur2":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 855
    .local v7, "NumBookmark":I
    if-lez v7, :cond_0

    .line 857
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 860
    const-string v0, "Settings.Browser.NbBookmark"

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-le v7, v9, :cond_0

    .line 865
    const-string v0, "Bookmark"

    invoke-virtual {p1, v0}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->newElement(Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscXMLElement;

    move-result-object v6

    .line 866
    .local v6, "Bookmarknode":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    const-string v0, "NetworkName"

    const-string v1, "default"

    invoke-virtual {v6, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    const-string v0, "Index"

    add-int/lit8 v1, v9, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    const-string v0, "BookmarkName"

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    const-string v0, "URL"

    invoke-interface {v8, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    const-string v0, "Settings.Browser."

    invoke-virtual {p1, v0, v6}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addElement(Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscXMLElement;)V

    .line 872
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    .line 862
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 878
    .end local v6    # "Bookmarknode":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    .end local v9    # "i":I
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 880
    return-void
.end method

.method public getBookmarks()Ljava/util/ArrayList;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    const/16 v17, 0x0

    .line 179
    .local v17, "isAutoBookmarkFeature":Z
    const-string v11, "none"

    .line 181
    .local v11, "currentNtwName":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 184
    .local v7, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v19, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "Settings.Browser."

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 187
    .local v8, "browserNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "Bookmark"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v9

    .line 188
    .local v9, "browserNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v9, :cond_5

    .line 192
    if-eqz v17, :cond_0

    .line 193
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscBrowser;->getCurrentNetworkName()Ljava/lang/String;

    move-result-object v11

    .line 196
    :cond_0
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v19

    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_5

    .line 197
    move/from16 v0, v16

    invoke-interface {v9, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 199
    .local v10, "browserNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "BookmarkName"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v10, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 200
    .local v3, "bookmarkName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 202
    .local v4, "bookmarkNames":Ljava/lang/String;
    if-eqz v4, :cond_4

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "URL"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v10, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 204
    .local v5, "bookmarkUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    .line 205
    .local v6, "bookmarkUrls":Ljava/lang/String;
    if-eqz v6, :cond_3

    .line 208
    if-eqz v17, :cond_2

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "NetworkName"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v10, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v18

    .line 210
    .local v18, "nodeNtwName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    .line 211
    .local v2, "NtwName":Ljava/lang/String;
    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 212
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    .end local v2    # "NtwName":Ljava/lang/String;
    .end local v5    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .end local v6    # "bookmarkUrls":Ljava/lang/String;
    .end local v18    # "nodeNtwName":Lorg/w3c/dom/Node;
    :cond_1
    :goto_1
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 216
    .restart local v5    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .restart local v6    # "bookmarkUrls":Ljava/lang/String;
    :cond_2
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 223
    :cond_3
    const-string v19, "CSCBrowser"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " ** getBookmarks ("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ") : bookmark url is null. Skip.."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 227
    .end local v5    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .end local v6    # "bookmarkUrls":Ljava/lang/String;
    :cond_4
    const-string v19, "CSCBrowser"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " ** getBookmarks ("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ") : bookmark name is null. Skip.."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 235
    .end local v3    # "bookmarkName":Lorg/w3c/dom/Node;
    .end local v4    # "bookmarkNames":Ljava/lang/String;
    .end local v10    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .end local v16    # "i":I
    :cond_5
    if-eqz v17, :cond_6

    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->isEnableLog()Z

    move-result v19

    if-eqz v19, :cond_7

    const-string v19, "gsm.sim.operator.numeric"

    const-string v20, ""

    invoke-static/range {v19 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    if-lez v19, :cond_7

    .line 238
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "eManual"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v13

    .line 239
    .local v13, "eManualBookmarkNode":Lorg/w3c/dom/Node;
    if-eqz v13, :cond_7

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "URL"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v13, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    .line 241
    .local v15, "eManualUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v20, 0x7f04000e

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 242
    .local v12, "eManualBookmarkName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v14

    .line 243
    .local v14, "eManualBookmarkUrl":Ljava/lang/String;
    if-eqz v14, :cond_8

    .line 244
    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    invoke-virtual {v7, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    const-string v19, "CSCBrowser"

    const-string v20, " ** getBookmarks : eManual bookmark added"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    .end local v12    # "eManualBookmarkName":Ljava/lang/String;
    .end local v13    # "eManualBookmarkNode":Lorg/w3c/dom/Node;
    .end local v14    # "eManualBookmarkUrl":Ljava/lang/String;
    .end local v15    # "eManualUrl":Lorg/w3c/dom/Node;
    :cond_7
    :goto_2
    return-object v7

    .line 248
    .restart local v12    # "eManualBookmarkName":Ljava/lang/String;
    .restart local v13    # "eManualBookmarkNode":Lorg/w3c/dom/Node;
    .restart local v14    # "eManualBookmarkUrl":Ljava/lang/String;
    .restart local v15    # "eManualUrl":Lorg/w3c/dom/Node;
    :cond_8
    const-string v19, "CSCBrowser"

    const-string v20, " ** getBookmarks : eManual bookmark url is null. Skip.."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public setHomeUrl(Ljava/lang/String;)V
    .locals 6
    .param p1, "homeurl"    # Ljava/lang/String;

    .prologue
    .line 301
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 302
    const-string v3, "CSCBrowser"

    const-string v4, "setHomeUrl : nothing to configurate."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :goto_0
    return-void

    .line 306
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.CSC_BROWSER_SET_HOMEPAGE"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 307
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "homepage"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 310
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mContext:Landroid/content/Context;

    const-string v4, "csc.preferences_name"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 311
    .local v2, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 312
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v3, "homepage"

    invoke-interface {v0, v3, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 313
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 315
    const-string v3, "CSCBrowser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setHomeUrl done.. ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public update()V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 758
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 760
    .local v10, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->SEND_PACKAGE_BROWSER:Landroid/content/ComponentName;

    const/16 v1, 0x80

    invoke-virtual {v10, v0, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 762
    const-string v0, "CSCBrowser"

    const-string v1, "find com.android.browser/com.android.browser.BrowserActivity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 770
    const/4 v7, 0x0

    .line 772
    .local v7, "cpClient":Landroid/content/ContentProviderClient;
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 775
    :try_start_1
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscBrowser;->BROWSER_BOOKMARK_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    .line 781
    :goto_0
    if-nez v7, :cond_1

    .line 782
    const-string v0, "CSCBrowser"

    const-string v1, " update : Phone does not ready to acquire Browser Provider"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    .end local v7    # "cpClient":Landroid/content/ContentProviderClient;
    :cond_0
    :goto_1
    return-void

    .line 763
    :catch_0
    move-exception v9

    .line 764
    .local v9, "e":Ljava/lang/Exception;
    const-string v0, "CSCBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cannot find com.android.browser/com.android.browser.BrowserActivity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 776
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v7    # "cpClient":Landroid/content/ContentProviderClient;
    :catch_1
    move-exception v9

    .line 777
    .local v9, "e":Ljava/lang/SecurityException;
    const-string v0, "CSCBrowser"

    const-string v1, " update :  BROWSER_OPBOOKMARK_CONTENT_URI SecurityException"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    const/4 v7, 0x0

    goto :goto_0

    .line 786
    .end local v9    # "e":Ljava/lang/SecurityException;
    :cond_1
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mHomepageUrlRcvr:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 792
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscBrowser;->updateTimeDateformat()V

    .line 793
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscBrowser;->updateBookmarks()V

    .line 796
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscBrowser;->getBookmarks()Ljava/util/ArrayList;

    move-result-object v6

    .line 798
    .local v6, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 804
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "title"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "url"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "bookmark"

    aput-object v5, v2, v3

    const-string v3, "bookmark == 1"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 809
    .local v8, "dbCur":Landroid/database/Cursor;
    if-nez v8, :cond_3

    .line 810
    const-string v0, "CSCBrowser"

    const-string v1, "dbCur is null. so could not check the bookmarks in DB"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    .end local v8    # "dbCur":Landroid/database/Cursor;
    :cond_2
    :goto_2
    if-eqz v7, :cond_0

    .line 835
    invoke-virtual {v7}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_1

    .line 812
    .restart local v8    # "dbCur":Landroid/database/Cursor;
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    if-eq v0, v1, :cond_4

    .line 813
    const-string v0, "CSCBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compare dbCur.getCount() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bookmarks.size()/2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscBrowser;->updateBookmarks()V

    .line 819
    :cond_4
    if-eqz v8, :cond_2

    .line 820
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2
.end method

.method public updateBookmarks()V
    .locals 28

    .prologue
    .line 325
    new-instance v24, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-direct/range {v24 .. v25}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    const-string v25, "Settings.Browser.HistoryList"

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 328
    .local v22, "keepOriginalDB":Ljava/lang/String;
    const-string v24, "CSCBrowser"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "updateBookmarks : keepOriginalDB = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v24, v0

    sget-object v25, Lcom/samsung/sec/android/application/csc/CscBrowser;->BROWSER_OPBOOKMARK_CONTENT_URI:Landroid/net/Uri;

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v24 .. v27}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 336
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks : opbookmarks are deleted.."

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 348
    :goto_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v24, v0

    sget-object v25, Lcom/samsung/sec/android/application/csc/CscBrowser;->BROWSER_BOOKMARK_CONTENT_URI:Landroid/net/Uri;

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v24 .. v27}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 349
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks : Google default bookmarks are deleted.."

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_6

    .line 362
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    const-string v25, "Settings.Browser."

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v11

    .line 363
    .local v11, "browserNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    const-string v25, "Bookmark"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v11, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v12

    .line 364
    .local v12, "browserNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v12, :cond_5

    .line 365
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_2
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v24

    move/from16 v0, v19

    move/from16 v1, v24

    if-ge v0, v1, :cond_5

    .line 367
    move/from16 v0, v19

    invoke-interface {v12, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v13

    .line 369
    .local v13, "browserNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    const-string v25, "BookmarkName"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v13, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 370
    .local v6, "bookmarkName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    const-string v25, "URL"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v13, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 371
    .local v8, "bookmarkUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    const-string v25, "Editable"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v13, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 373
    .local v4, "bookmarkEditable":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v7

    .line 374
    .local v7, "bookmarkNames":Ljava/lang/String;
    if-nez v7, :cond_0

    .line 375
    const-string v24, "CSCBrowser"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, " ** getBookmarks ("

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ") : bookmark name is null. Skip.."

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    .line 379
    .local v9, "bookmarkUrls":Ljava/lang/String;
    if-nez v9, :cond_1

    .line 380
    const-string v24, "CSCBrowser"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, " ** getBookmarks ("

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ") : bookmark url is null. Skip.."

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    .line 384
    .local v5, "bookmarkEditables":Ljava/lang/String;
    if-nez v5, :cond_2

    .line 385
    const-string v24, "CSCBrowser"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, " ** getBookmarks ("

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ") : bookmark editable is null. Skip.."

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :cond_2
    if-eqz v9, :cond_3

    if-eqz v7, :cond_3

    .line 389
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 390
    .local v10, "bookmarkmap":Landroid/content/ContentValues;
    const-string v24, "title"

    move-object/from16 v0, v24

    invoke-virtual {v10, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    const-string v24, "url"

    move-object/from16 v0, v24

    invoke-virtual {v10, v0, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const-string v24, "folder"

    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 397
    const-string v24, "parent"

    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 400
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v24, v0

    sget-object v25, Lcom/samsung/sec/android/application/csc/CscBrowser;->BROWSER_BOOKMARK_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_7

    move-result-object v21

    .line 408
    .local v21, "inserted_uri":Landroid/net/Uri;
    :goto_3
    if-eqz v21, :cond_3

    invoke-virtual/range {v21 .. v21}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v24

    if-eqz v24, :cond_3

    .line 409
    if-eqz v5, :cond_4

    const-string v24, "no"

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v24

    if-nez v24, :cond_4

    .line 410
    new-instance v23, Landroid/content/ContentValues;

    invoke-direct/range {v23 .. v23}, Landroid/content/ContentValues;-><init>()V

    .line 412
    .local v23, "opbookmarkmap":Landroid/content/ContentValues;
    invoke-virtual/range {v21 .. v21}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v20

    .line 413
    .local v20, "inserted_id":Ljava/lang/String;
    const-string v24, "bookmark_id"

    invoke-virtual/range {v21 .. v21}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 420
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v24, v0

    sget-object v25, Lcom/samsung/sec/android/application/csc/CscBrowser;->BROWSER_OPBOOKMARK_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 421
    const-string v24, "CSCBrowser"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, " ** getBookmarks ("

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ") : opbookmark inserted"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_8

    .line 365
    .end local v10    # "bookmarkmap":Landroid/content/ContentValues;
    .end local v20    # "inserted_id":Ljava/lang/String;
    .end local v21    # "inserted_uri":Landroid/net/Uri;
    .end local v23    # "opbookmarkmap":Landroid/content/ContentValues;
    :cond_3
    :goto_4
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_2

    .line 337
    .end local v4    # "bookmarkEditable":Lorg/w3c/dom/Node;
    .end local v5    # "bookmarkEditables":Ljava/lang/String;
    .end local v6    # "bookmarkName":Lorg/w3c/dom/Node;
    .end local v7    # "bookmarkNames":Ljava/lang/String;
    .end local v8    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .end local v9    # "bookmarkUrls":Ljava/lang/String;
    .end local v11    # "browserNode":Lorg/w3c/dom/Node;
    .end local v12    # "browserNodeList":Lorg/w3c/dom/NodeList;
    .end local v13    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .end local v19    # "i":I
    :catch_0
    move-exception v14

    .line 338
    .local v14, "e":Ljava/lang/UnsupportedOperationException;
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks :  UnsupportedOperationException"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 339
    .end local v14    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_1
    move-exception v14

    .line 340
    .local v14, "e":Ljava/lang/SecurityException;
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks :  BROWSER_OPBOOKMARK_CONTENT_URI SecurityException"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 342
    .end local v14    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v14

    .line 343
    .local v14, "e":Ljava/lang/NullPointerException;
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks :  BROWSER_OPBOOKMARK_CONTENT_URI NullPointerException"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 350
    .end local v14    # "e":Ljava/lang/NullPointerException;
    :catch_3
    move-exception v14

    .line 351
    .local v14, "e":Ljava/lang/UnsupportedOperationException;
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks :  there is no Google default bookmarks"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 352
    .end local v14    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_4
    move-exception v14

    .line 353
    .local v14, "e":Ljava/lang/IllegalArgumentException;
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks :  IllegalArgumentException"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 354
    .end local v14    # "e":Ljava/lang/IllegalArgumentException;
    :catch_5
    move-exception v14

    .line 355
    .local v14, "e":Ljava/lang/SecurityException;
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks :  BROWSER_BOOKMARK_CONTENT_URI SecurityException"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 357
    .end local v14    # "e":Ljava/lang/SecurityException;
    :catch_6
    move-exception v14

    .line 358
    .local v14, "e":Ljava/lang/NullPointerException;
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks :  BROWSER_OPBOOKMARK_CONTENT_URI NullPointerException"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 401
    .end local v14    # "e":Ljava/lang/NullPointerException;
    .restart local v4    # "bookmarkEditable":Lorg/w3c/dom/Node;
    .restart local v5    # "bookmarkEditables":Ljava/lang/String;
    .restart local v6    # "bookmarkName":Lorg/w3c/dom/Node;
    .restart local v7    # "bookmarkNames":Ljava/lang/String;
    .restart local v8    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .restart local v9    # "bookmarkUrls":Ljava/lang/String;
    .restart local v10    # "bookmarkmap":Landroid/content/ContentValues;
    .restart local v11    # "browserNode":Lorg/w3c/dom/Node;
    .restart local v12    # "browserNodeList":Lorg/w3c/dom/NodeList;
    .restart local v13    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .restart local v19    # "i":I
    :catch_7
    move-exception v14

    .line 402
    .local v14, "e":Ljava/lang/SecurityException;
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks :  BROWSER_BOOKMARK_CONTENT_URI SecurityException"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    const/16 v21, 0x0

    .restart local v21    # "inserted_uri":Landroid/net/Uri;
    goto/16 :goto_3

    .line 423
    .end local v14    # "e":Ljava/lang/SecurityException;
    .restart local v20    # "inserted_id":Ljava/lang/String;
    .restart local v23    # "opbookmarkmap":Landroid/content/ContentValues;
    :catch_8
    move-exception v14

    .line 424
    .restart local v14    # "e":Ljava/lang/SecurityException;
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks :  BROWSER_OPBOOKMARK_CONTENT_URI SecurityException"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 428
    .end local v14    # "e":Ljava/lang/SecurityException;
    .end local v20    # "inserted_id":Ljava/lang/String;
    .end local v23    # "opbookmarkmap":Landroid/content/ContentValues;
    :cond_4
    const-string v24, "CSCBrowser"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, " ** getBookmarks ("

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ") : bookmark is not op"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 435
    .end local v4    # "bookmarkEditable":Lorg/w3c/dom/Node;
    .end local v5    # "bookmarkEditables":Ljava/lang/String;
    .end local v6    # "bookmarkName":Lorg/w3c/dom/Node;
    .end local v7    # "bookmarkNames":Ljava/lang/String;
    .end local v8    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .end local v9    # "bookmarkUrls":Ljava/lang/String;
    .end local v10    # "bookmarkmap":Landroid/content/ContentValues;
    .end local v13    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .end local v19    # "i":I
    .end local v21    # "inserted_uri":Landroid/net/Uri;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    const-string v25, "eManual"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v11, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v16

    .line 436
    .local v16, "eManualBookmarkNode":Lorg/w3c/dom/Node;
    if-eqz v16, :cond_6

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    const-string v25, "URL"

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v18

    .line 439
    .local v18, "eManualUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f04000e

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 440
    .local v15, "eManualBookmarkName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v17

    .line 441
    .local v17, "eManualBookmarkUrl":Ljava/lang/String;
    if-eqz v17, :cond_7

    .line 442
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 443
    .restart local v10    # "bookmarkmap":Landroid/content/ContentValues;
    const-string v24, "title"

    move-object/from16 v0, v24

    invoke-virtual {v10, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const-string v24, "url"

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const-string v24, "folder"

    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 446
    const-string v24, "parent"

    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 449
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v24, v0

    sget-object v25, Lcom/samsung/sec/android/application/csc/CscBrowser;->BROWSER_BOOKMARK_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_9

    .line 459
    .end local v10    # "bookmarkmap":Landroid/content/ContentValues;
    .end local v15    # "eManualBookmarkName":Ljava/lang/String;
    .end local v17    # "eManualBookmarkUrl":Ljava/lang/String;
    .end local v18    # "eManualUrl":Lorg/w3c/dom/Node;
    :cond_6
    :goto_5
    return-void

    .line 450
    .restart local v10    # "bookmarkmap":Landroid/content/ContentValues;
    .restart local v15    # "eManualBookmarkName":Ljava/lang/String;
    .restart local v17    # "eManualBookmarkUrl":Ljava/lang/String;
    .restart local v18    # "eManualUrl":Lorg/w3c/dom/Node;
    :catch_9
    move-exception v14

    .line 451
    .restart local v14    # "e":Ljava/lang/SecurityException;
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks :  BROWSER_BOOKMARK_CONTENT_URI eManualBookmarkNode SecurityException"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 456
    .end local v10    # "bookmarkmap":Landroid/content/ContentValues;
    .end local v14    # "e":Ljava/lang/SecurityException;
    :cond_7
    const-string v24, "CSCBrowser"

    const-string v25, " ** updateBookmarks : eManual bookmark url is null. Skip.."

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public updateTimeDateformat()V
    .locals 3

    .prologue
    .line 264
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 265
    const-string v0, "CSCSettings"

    const-string v1, " start time date format setting "

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    const-string v0, "CSCSettings"

    const-string v1, "Setting time format"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 270
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "24h"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 271
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "time_12_24"

    const-string v2, "24"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 280
    :cond_0
    :goto_0
    const-string v0, "CSCSettings"

    const-string v1, "Setting Date format"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 283
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ddmmyyyy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 284
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "date_format"

    const-string v2, "dd-MM-yyyy"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 294
    :cond_1
    :goto_1
    const-string v0, "CSCSettings"

    const-string v1, " end time date format setting"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    return-void

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "12h"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "time_12_24"

    const-string v2, "12"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 276
    :cond_3
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "time_12_24"

    const-string v2, "24"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 277
    const-string v0, "CSCSettings"

    const-string v1, "Time format not found"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 285
    :cond_4
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mmddyyyy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 286
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "date_format"

    const-string v2, "MM-dd-yyyy"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1

    .line 287
    :cond_5
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "yyyymmdd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 288
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "date_format"

    const-string v2, "yyyy-MM-dd"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1

    .line 291
    :cond_6
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscBrowser;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "date_format"

    const-string v2, "dd-MM-yyyy"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 292
    const-string v0, "CSCSettings"

    const-string v1, "Date format not found"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
