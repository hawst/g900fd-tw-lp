.class public Lcom/samsung/sec/android/application/csc/CscTgManager;
.super Ljava/lang/Object;
.source "CscTgManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscTgManager$1;,
        Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;,
        Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;
    }
.end annotation


# static fields
.field static instance:Lcom/samsung/sec/android/application/csc/CscTgManager;


# instance fields
.field mTgTable:Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 143
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscTgManager;

    invoke-direct {v0}, Lcom/samsung/sec/android/application/csc/CscTgManager;-><init>()V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscTgManager;->instance:Lcom/samsung/sec/android/application/csc/CscTgManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscTgManager;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 146
    sget-object v1, Lcom/samsung/sec/android/application/csc/CscTgManager;->instance:Lcom/samsung/sec/android/application/csc/CscTgManager;

    monitor-enter v1

    .line 147
    :try_start_0
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscTgManager;->instance:Lcom/samsung/sec/android/application/csc/CscTgManager;

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscTgManager;->mTgTable:Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;

    if-nez v0, :cond_0

    .line 148
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscTgManager;->instance:Lcom/samsung/sec/android/application/csc/CscTgManager;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;

    invoke-direct {v2, p0}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;-><init>(Landroid/content/Context;)V

    iput-object v2, v0, Lcom/samsung/sec/android/application/csc/CscTgManager;->mTgTable:Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;

    .line 150
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscTgManager;->instance:Lcom/samsung/sec/android/application/csc/CscTgManager;

    return-object v0

    .line 150
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getFullList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscTgManager;->mTgTable:Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;

    invoke-virtual {v0}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->getFullList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscTgBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscTgManager;->mTgTable:Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;

    invoke-virtual {v0}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->getCscTgList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
