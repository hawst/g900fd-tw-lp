.class public Lcom/samsung/sec/android/application/csc/CscContacts;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscContacts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscContacts$1;,
        Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;,
        Lcom/samsung/sec/android/application/csc/CscContacts$Contact;,
        Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;,
        Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    }
.end annotation


# static fields
.field public static final DATA_URI:Landroid/net/Uri;


# instance fields
.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final NOERROR:Ljava/lang/String;

.field private final OTHERS_CSC_FILE:Ljava/lang/String;

.field private final TAG_BASE:Ljava/lang/String;

.field private final TAG_CONTACT:Ljava/lang/String;

.field private final TAG_CONTACTSORT:Ljava/lang/String;

.field private final TAG_DIALPOSITION:Ljava/lang/String;

.field private final TAG_NBPRECONTACT:Ljava/lang/String;

.field private final TAG_NBSPEEDDIAL:Ljava/lang/String;

.field private final TAG_PREINSTALLEDCONTACT:Ljava/lang/String;

.field private final TAG_SAVEPOSITION:Ljava/lang/String;

.field private final TAG_SPEEDDIAL:Ljava/lang/String;

.field private final TAG_TELNAME:Ljava/lang/String;

.field private final TAG_TELNUM:Ljava/lang/String;

.field private final TAG_VIEWFROM:Ljava/lang/String;

.field private final TRACE_TAG:Ljava/lang/String;

.field private final URI_CONTACTS:Ljava/lang/String;

.field private final URI_CONTACTS_DATA:Ljava/lang/String;

.field private final URI_CONTACTS_RAW:Ljava/lang/String;

.field private final URI_CONTACTS_SPEEDDIAL:Ljava/lang/String;

.field private final VALUE_ASK:Ljava/lang/String;

.field private final VALUE_FIRSTNAME:Ljava/lang/String;

.field private final VALUE_LASTNAME:Ljava/lang/String;

.field private final VALUE_PHONE:Ljava/lang/String;

.field private final VALUE_SIM:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mResolver:Landroid/content/ContentResolver;

.field private numberOfContacts:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    const-string v0, "content://com.android.contacts/data/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscContacts;->DATA_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 177
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 39
    const-string v0, "NOERROR"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->NOERROR:Ljava/lang/String;

    .line 44
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 48
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->OTHERS_CSC_FILE:Ljava/lang/String;

    .line 64
    const-string v0, "CscContacts"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TRACE_TAG:Ljava/lang/String;

    .line 67
    const-string v0, "Settings.Contact"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_BASE:Ljava/lang/String;

    .line 69
    const-string v0, "Contact"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_CONTACT:Ljava/lang/String;

    .line 72
    const-string v0, "SavePosition"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_SAVEPOSITION:Ljava/lang/String;

    .line 74
    const-string v0, "ContactSort"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_CONTACTSORT:Ljava/lang/String;

    .line 76
    const-string v0, "ViewFrom"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_VIEWFROM:Ljava/lang/String;

    .line 79
    const-string v0, "SpeedDial"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_SPEEDDIAL:Ljava/lang/String;

    .line 81
    const-string v0, "NbSpeedDial"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_NBSPEEDDIAL:Ljava/lang/String;

    .line 83
    const-string v0, "DialPosition"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_DIALPOSITION:Ljava/lang/String;

    .line 85
    const-string v0, "TelName"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_TELNAME:Ljava/lang/String;

    .line 87
    const-string v0, "TelNum"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_TELNUM:Ljava/lang/String;

    .line 90
    const-string v0, "PreInstalledContact"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_PREINSTALLEDCONTACT:Ljava/lang/String;

    .line 92
    const-string v0, "NbPreInstalledContact"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->TAG_NBPRECONTACT:Ljava/lang/String;

    .line 97
    const-string v0, "phone"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->VALUE_PHONE:Ljava/lang/String;

    .line 99
    const-string v0, "sim"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->VALUE_SIM:Ljava/lang/String;

    .line 101
    const-string v0, "ask"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->VALUE_ASK:Ljava/lang/String;

    .line 103
    const-string v0, "firstname"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->VALUE_FIRSTNAME:Ljava/lang/String;

    .line 105
    const-string v0, "lastname"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->VALUE_LASTNAME:Ljava/lang/String;

    .line 108
    const-string v0, "content://com.android.contacts"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->URI_CONTACTS:Ljava/lang/String;

    .line 110
    const-string v0, "content://com.android.contacts/data/"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->URI_CONTACTS_DATA:Ljava/lang/String;

    .line 112
    const-string v0, "content://com.android.contacts/contacts/speeddial"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->URI_CONTACTS_SPEEDDIAL:Ljava/lang/String;

    .line 114
    const-string v0, "content://com.android.contacts/raw_contact_entities"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->URI_CONTACTS_RAW:Ljava/lang/String;

    .line 137
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 139
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 178
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mContext:Landroid/content/Context;

    .line 179
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    .line 180
    return-void
.end method

.method private checkContactExist()Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 287
    const/4 v7, 0x0

    .line 288
    .local v7, "numberOfLocalContacts":I
    const-string v0, "content://com.android.contacts/raw_contact_entities"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 289
    .local v1, "rawContactsEntryUri":Landroid/net/Uri;
    const-string v3, "account_type=\'vnd.sec.contact.phone\'"

    .line 290
    .local v3, "selectString":Ljava/lang/String;
    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "data1"

    aput-object v0, v2, v8

    .line 294
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 296
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 297
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 298
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 299
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 304
    :goto_0
    if-nez v7, :cond_1

    move v0, v8

    .line 311
    :goto_1
    return v0

    .line 301
    :cond_0
    const-string v0, "CscContacts"

    const-string v4, " ** Cursor of Information Contact is null **"

    invoke-static {v0, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 307
    :cond_1
    const-string v0, "TMB"

    const-string v4, "ro.csc.sales_code"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscContacts;->isPreloadTMB()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 308
    const/4 v0, 0x3

    if-ne v7, v0, :cond_2

    move v0, v8

    .line 309
    goto :goto_1

    :cond_2
    move v0, v9

    .line 311
    goto :goto_1
.end method

.method private comparePreInstalledContacts()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x1

    .line 735
    const/4 v8, 0x0

    .line 736
    .local v8, "numberOfCscContacts":I
    const/4 v9, 0x0

    .line 738
    .local v9, "numberOfLocalContacts":I
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscContacts;->getContacts()Ljava/util/ArrayList;

    move-result-object v7

    .line 740
    .local v7, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$Contact;>;"
    if-eqz v7, :cond_0

    .line 741
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 743
    :cond_0
    if-lez v8, :cond_2

    .line 744
    const-string v0, "content://com.android.contacts/raw_contact_entities"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 745
    .local v1, "rawContactsEntryUri":Landroid/net/Uri;
    const-string v3, "account_type=\'vnd.sec.contact.phone\' AND mimetype=\'vnd.android.cursor.item/name\'"

    .line 747
    .local v3, "selectString":Ljava/lang/String;
    new-array v2, v11, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "contact_id"

    aput-object v5, v2, v0

    .line 754
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 756
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 757
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 758
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 769
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 779
    .end local v1    # "rawContactsEntryUri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selectString":Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :goto_0
    if-eqz v8, :cond_3

    if-eq v9, v8, :cond_3

    .line 780
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "comparePreinstalledContacts Error: Local="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " CSC="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 782
    .local v10, "strTrace":Ljava/lang/String;
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;

    invoke-direct {v0, p0, v10}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Ljava/lang/String;)V

    throw v0

    .line 771
    .end local v10    # "strTrace":Ljava/lang/String;
    .restart local v1    # "rawContactsEntryUri":Landroid/net/Uri;
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "selectString":Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    const-string v0, "CscContacts"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ** Cursor of Information Contact is null **"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 775
    .end local v1    # "rawContactsEntryUri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selectString":Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_2
    const-string v0, "CscContacts"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ** Multiple preinstalled contacts are not needed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 785
    :cond_3
    const-string v0, "CscContacts"

    const-string v4, "comparePreinstalledContacts success"

    invoke-static {v0, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    return v11
.end method

.method private comparePreloadContact(Ljava/util/ArrayList;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
        }
    .end annotation

    .prologue
    .local p1, "arListContact":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;>;"
    const/4 v8, 0x0

    .line 1007
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscContacts;->getContactArrayFromDB()Ljava/util/ArrayList;

    move-result-object v0

    .line 1009
    .local v0, "arPhoneData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1010
    .local v2, "cscListSize":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1012
    .local v6, "phoneListSize":I
    if-eq v2, v6, :cond_0

    .line 1013
    const-string v7, "Settings.Contact.NbPreInstalledContact"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 1043
    :goto_0
    return v7

    .line 1018
    :cond_0
    const/4 v5, 0x0

    .line 1020
    .local v5, "match":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v2, :cond_4

    .line 1021
    const/4 v5, 0x0

    .line 1022
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;

    .line 1024
    .local v1, "compContact":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_2
    if-ge v4, v6, :cond_1

    .line 1025
    iget-object v9, v1, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1026
    iget-object v9, v1, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1027
    const/4 v5, 0x1

    .line 1028
    const-string v7, "Settings.Contact.PreInstalledContact.TelName"

    iget-object v9, v1, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    invoke-static {v7, v9}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    const-string v7, "Settings.Contact.PreInstalledContact.TelNum"

    iget-object v9, v1, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-static {v7, v9}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    :cond_1
    if-nez v5, :cond_3

    .line 1037
    const-string v7, "Settings.Contact.PreInstalledContact"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, v1, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v1, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "not match"

    invoke-static {v7, v9, v10}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 1040
    goto :goto_0

    .line 1024
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1020
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1043
    .end local v1    # "compContact":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    .end local v4    # "j":I
    :cond_4
    const/4 v7, 0x1

    goto :goto_0
.end method

.method private compareSavePosition(Ljava/lang/String;)Z
    .locals 6
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 791
    const-string v1, ""

    .line 792
    .local v1, "valueCSC":Ljava/lang/String;
    const-string v2, ""

    .line 794
    .local v2, "valueLocal":Ljava/lang/String;
    const-string v4, "CscContacts"

    const-string v5, "compareSavePosition start"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    if-nez p1, :cond_0

    .line 820
    :goto_0
    return v3

    .line 799
    :cond_0
    const-string v4, "phone"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 800
    const-string v1, "vnd.sec.contact.phone"

    .line 810
    :goto_1
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "contactsaveto"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 812
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 813
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SavePosition Error: Local="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " CSC="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 814
    .local v0, "strTrace":Ljava/lang/String;
    new-instance v3, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;

    invoke-direct {v3, p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Ljava/lang/String;)V

    throw v3

    .line 801
    .end local v0    # "strTrace":Ljava/lang/String;
    :cond_1
    const-string v4, "sim"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 802
    const-string v1, "vnd.sec.contact.sim"

    goto :goto_1

    .line 803
    :cond_2
    const-string v4, "ask"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 804
    const-string v1, "Always ask"

    goto :goto_1

    .line 806
    :cond_3
    const-string v3, "CscContacts"

    const-string v4, "compareSavePosition strValue is not defined."

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    const/4 v3, 0x0

    goto :goto_0

    .line 817
    :cond_4
    const-string v4, "Settings.Contact.SavePosition"

    invoke-static {v4, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    const-string v4, "CscContacts"

    const-string v5, "compareSavePosition success"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private compareSort(Ljava/lang/String;)Z
    .locals 6
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 825
    const/4 v1, -0x1

    .line 827
    .local v1, "valueLocal":I
    const-string v3, "CscContacts"

    const-string v4, "compareSort start"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    if-nez p1, :cond_0

    .line 852
    :goto_0
    return v2

    .line 831
    :cond_0
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "android.contacts.SORT_ORDER"

    const/4 v5, -0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 832
    const-string v3, "firstname"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 833
    if-eq v1, v2, :cond_3

    .line 834
    const-string v2, "Settings.Contact.ContactSort"

    const-string v3, "firstname"

    const-string v4, "lastname"

    invoke-static {v2, v3, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    const-string v0, "Sort Error: Local!=Primary CSC=FIRSTNAME"

    .line 836
    .local v0, "strTrace":Ljava/lang/String;
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;

    invoke-direct {v2, p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Ljava/lang/String;)V

    throw v2

    .line 838
    .end local v0    # "strTrace":Ljava/lang/String;
    :cond_1
    const-string v3, "lastname"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 839
    const/4 v3, 0x2

    if-eq v1, v3, :cond_3

    .line 840
    const-string v2, "Settings.Contact.ContactSort"

    const-string v3, "lastname"

    const-string v4, "firstname"

    invoke-static {v2, v3, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    const-string v0, "Sort Error: Local!=Alternative CSC=LASTNAME"

    .line 842
    .restart local v0    # "strTrace":Ljava/lang/String;
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;

    invoke-direct {v2, p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Ljava/lang/String;)V

    throw v2

    .line 845
    .end local v0    # "strTrace":Ljava/lang/String;
    :cond_2
    const-string v2, "CscContacts"

    const-string v3, "compareSort strValue is not defined."

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    const/4 v2, 0x0

    goto :goto_0

    .line 849
    :cond_3
    const-string v3, "Settings.Contact.ContactSort"

    invoke-static {v3, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    const-string v3, "CscContacts"

    const-string v4, "compareSort success"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private compareSpeedDial(Ljava/util/ArrayList;)Z
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
        }
    .end annotation

    .prologue
    .line 1049
    .local p1, "arListContact":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v23

    .line 1050
    .local v23, "size":I
    const/4 v13, 0x0

    .line 1053
    .local v13, "match":Z
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 1056
    .local v21, "preloadSpeedDialarray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;>;"
    const-string v2, "content://com.android.contacts/contacts/speeddial"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1057
    .local v3, "speedDialUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 1058
    .local v24, "speedDialCursor":Landroid/database/Cursor;
    const/4 v14, 0x0

    .line 1059
    .local v14, "name":Ljava/lang/String;
    const/16 v16, 0x0

    .line 1061
    .local v16, "number":Ljava/lang/String;
    if-eqz v24, :cond_0

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v2

    move/from16 v0, v23

    if-eq v0, v2, :cond_0

    .line 1062
    const-string v2, "Settings.Contact.SpeedDial"

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 1066
    const/4 v2, 0x0

    .line 1181
    :goto_0
    return v2

    .line 1067
    :cond_0
    if-nez v23, :cond_2

    .line 1069
    if-eqz v24, :cond_1

    .line 1070
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 1072
    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    .line 1075
    :cond_2
    const-string v7, "raw_contact_id=? AND mimetype=?"

    .line 1076
    .local v7, "selection":Ljava/lang/String;
    const/4 v2, 0x2

    new-array v8, v2, [Ljava/lang/String;

    .line 1078
    .local v8, "selectionArgs":[Ljava/lang/String;
    if-eqz v24, :cond_7

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_7

    .line 1079
    const/4 v2, -0x1

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1080
    :goto_1
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1081
    new-instance v20, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;

    const/4 v2, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Lcom/samsung/sec/android/application/csc/CscContacts$1;)V

    .line 1083
    .local v20, "preloadSpeedDial":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    const-string v2, "raw_contact_id"

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    .line 1085
    .local v22, "rawContactId":Ljava/lang/Long;
    const-string v2, "key_number"

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    .line 1088
    .local v25, "speeddialnum":Ljava/lang/Long;
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v28, 0x1

    sub-long v4, v4, v28

    long-to-int v2, v4

    move-object/from16 v0, v20

    iput v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    .line 1090
    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v2

    .line 1091
    const/4 v2, 0x1

    const-string v4, "vnd.android.cursor.item/name"

    aput-object v4, v8, v2

    .line 1092
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v9, "data2"

    aput-object v9, v6, v2

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 1095
    .local v15, "nameCursor":Landroid/database/Cursor;
    if-eqz v15, :cond_4

    .line 1096
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1097
    const-string v2, "data2"

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1100
    :cond_3
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 1103
    :cond_4
    move-object/from16 v0, v20

    iput-object v14, v0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    .line 1105
    const/4 v2, 0x1

    const-string v4, "vnd.android.cursor.item/phone_v2"

    aput-object v4, v8, v2

    .line 1106
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v9, "data1"

    aput-object v9, v6, v2

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 1109
    .local v18, "phoneCursor":Landroid/database/Cursor;
    if-eqz v18, :cond_6

    .line 1110
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1111
    const-string v2, "data1"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1113
    :cond_5
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1116
    :cond_6
    move-object/from16 v0, v16

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    .line 1117
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1120
    .end local v15    # "nameCursor":Landroid/database/Cursor;
    .end local v18    # "phoneCursor":Landroid/database/Cursor;
    .end local v20    # "preloadSpeedDial":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    .end local v22    # "rawContactId":Ljava/lang/Long;
    .end local v25    # "speeddialnum":Ljava/lang/Long;
    :cond_7
    if-eqz v24, :cond_8

    .line 1121
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 1124
    :cond_8
    const-string v2, "CscContacts"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Number of SpeedDial :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1126
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 1128
    .local v17, "pSize":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    move/from16 v0, v23

    if-ge v11, v0, :cond_10

    .line 1130
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;

    .line 1132
    .local v10, "cscContact":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    if-nez v10, :cond_a

    .line 1133
    const-string v2, "CscContacts"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateSpeedDial contacts is null:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1128
    :cond_9
    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1137
    :cond_a
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_4
    move/from16 v0, v17

    if-ge v12, v0, :cond_b

    .line 1139
    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;

    .line 1141
    .local v19, "phone_data":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    if-nez v19, :cond_c

    .line 1165
    .end local v19    # "phone_data":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    :cond_b
    :goto_5
    if-nez v13, :cond_9

    .line 1166
    const-string v2, "Settings.Contact.SpeedDial"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "not match"

    invoke-static {v2, v4, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1144
    .restart local v19    # "phone_data":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    :cond_c
    move-object/from16 v0, v19

    iget v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    iget v4, v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    if-ne v2, v4, :cond_e

    .line 1145
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    iget-object v4, v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1146
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    iget-object v4, v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1147
    const-string v2, "Settings.Contact.SpeedDial.DialPosition"

    iget v4, v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    const-string v2, "Settings.Contact.SpeedDial.TelName"

    iget-object v4, v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149
    const-string v2, "Settings.Contact.SpeedDial.TelNum"

    iget-object v4, v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    const/4 v13, 0x1

    .line 1151
    goto :goto_5

    .line 1153
    :cond_d
    const-string v2, "CscContacts"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mName is match but number is not match"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    const/4 v13, 0x0

    .line 1162
    :goto_6
    const/4 v13, 0x0

    .line 1137
    :cond_e
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_4

    .line 1158
    :cond_f
    const-string v2, "CscContacts"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mName is match but number is not match"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v10, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    const/4 v13, 0x0

    goto :goto_6

    .line 1173
    .end local v10    # "cscContact":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    .end local v12    # "j":I
    .end local v19    # "phone_data":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    :cond_10
    if-nez v13, :cond_11

    .line 1174
    const-string v26, "SpeedDial Error."

    .line 1175
    .local v26, "strTrace":Ljava/lang/String;
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v2, v0, v1}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Ljava/lang/String;)V

    throw v2

    .line 1178
    .end local v26    # "strTrace":Ljava/lang/String;
    :cond_11
    if-nez v13, :cond_12

    .line 1179
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1181
    :cond_12
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private compareViewFrom(Ljava/lang/String;)Z
    .locals 13
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
        }
    .end annotation

    .prologue
    .line 857
    const-string v8, ""

    .line 860
    .local v8, "accountName":Ljava/lang/String;
    const-string v0, "CscContacts"

    const-string v2, "compareViewFrom start"

    invoke-static {v0, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    if-nez p1, :cond_0

    .line 863
    const-string v0, "CscContacts"

    const-string v2, "updateSavePosition strValue is NULL"

    invoke-static {v0, v2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    const/4 v0, 0x0

    .line 922
    :goto_0
    return v0

    .line 866
    :cond_0
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 867
    const-string v0, "CscContacts"

    const-string v2, "Only Phone displayed"

    invoke-static {v0, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    const-string v8, "vnd.sec.contact.sim"

    .line 879
    :goto_1
    const-string v0, "CscContacts"

    const-string v2, "Update Setting"

    invoke-static {v0, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 881
    .local v12, "values":Landroid/content/ContentValues;
    sget-object v0, Landroid/provider/ContactsContract$Settings;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "account_name"

    invoke-virtual {v0, v2, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "account_type"

    invoke-virtual {v0, v2, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 885
    .local v1, "settingsUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "ungrouped_visible"

    aput-object v5, v2, v4

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 890
    .local v10, "cursorSetting":Landroid/database/Cursor;
    :cond_1
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 891
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    .line 892
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ViewFrom is error:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'s ungrouped is visible."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 894
    .local v11, "strTrace":Ljava/lang/String;
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;

    invoke-direct {v0, p0, v11}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 898
    .end local v11    # "strTrace":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    .line 869
    .end local v1    # "settingsUri":Landroid/net/Uri;
    .end local v10    # "cursorSetting":Landroid/database/Cursor;
    .end local v12    # "values":Landroid/content/ContentValues;
    :cond_2
    const-string v0, "sim"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 870
    const-string v0, "CscContacts"

    const-string v2, "Only SIM displayed"

    invoke-static {v0, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    const-string v8, "vnd.sec.contact.phone"

    goto :goto_1

    .line 873
    :cond_3
    const-string v0, "Settings.Contact.ViewFrom"

    invoke-static {v0, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    const-string v0, "CscContacts"

    const-string v2, "compareViewFrom strValue is not supported"

    invoke-static {v0, v2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 898
    .restart local v1    # "settingsUri":Landroid/net/Uri;
    .restart local v10    # "cursorSetting":Landroid/database/Cursor;
    .restart local v12    # "values":Landroid/content/ContentValues;
    :cond_4
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 902
    new-instance v12, Landroid/content/ContentValues;

    .end local v12    # "values":Landroid/content/ContentValues;
    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 903
    .restart local v12    # "values":Landroid/content/ContentValues;
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "account_name"

    invoke-virtual {v0, v2, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "account_type"

    invoke-virtual {v0, v2, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 906
    .local v3, "groupsUri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "group_visible"

    aput-object v5, v4, v0

    const/4 v0, 0x1

    const-string v5, "title"

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 911
    .local v9, "cursorGroup":Landroid/database/Cursor;
    :cond_5
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 912
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_5

    .line 913
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ViewFrom is error:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "is visible."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 915
    .restart local v11    # "strTrace":Ljava/lang/String;
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;

    invoke-direct {v0, p0, v11}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 919
    .end local v11    # "strTrace":Ljava/lang/String;
    :catchall_1
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_6
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 921
    const-string v0, "Settings.Contact.ViewFrom"

    invoke-static {v0, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private getContactArrayFromDB()Ljava/util/ArrayList;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 926
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 927
    .local v14, "arListContact":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;>;"
    const-string v2, "content://com.android.contacts/contacts/speeddial"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 928
    .local v3, "speedDialUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 930
    .local v22, "speedDialCursor":Landroid/database/Cursor;
    new-instance v20, Ljava/lang/StringBuffer;

    const-string v2, ""

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 933
    .local v20, "sbSelection":Ljava/lang/StringBuffer;
    if-eqz v22, :cond_2

    .line 934
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v21

    .line 936
    .local v21, "size":I
    const/4 v2, -0x1

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 937
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_1

    .line 938
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    .line 939
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NOT _id = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "raw_contact_id"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 945
    add-int/lit8 v2, v21, -0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_0

    .line 946
    const-string v2, " AND "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 937
    :cond_0
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 949
    :cond_1
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 952
    .end local v17    # "i":I
    .end local v21    # "size":I
    :cond_2
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    .line 953
    .local v7, "selection":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v10, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "mimetype"

    aput-object v4, v10, v2

    const/4 v2, 0x1

    const-string v4, "data2"

    aput-object v4, v10, v2

    const/4 v2, 0x2

    const-string v4, "data1"

    aput-object v4, v10, v2

    .line 958
    .local v10, "projection":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 961
    .local v19, "preinstallContactCursor":Landroid/database/Cursor;
    if-eqz v19, :cond_7

    .line 962
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v21

    .line 964
    .restart local v21    # "size":I
    const/4 v2, -0x1

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 965
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_7

    .line 966
    new-instance v15, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v2}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Lcom/samsung/sec/android/application/csc/CscContacts$1;)V

    .line 967
    .local v15, "contactData":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    .line 969
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "raw_contact_id = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 974
    .local v11, "rawContactId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    sget-object v9, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 978
    .local v16, "dataCursor":Landroid/database/Cursor;
    if-eqz v16, :cond_6

    .line 979
    const/4 v2, -0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 980
    :cond_3
    :goto_2
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 981
    const-string v2, "mimetype"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 983
    .local v18, "mimetype":Ljava/lang/String;
    const-string v2, "vnd.android.cursor.item/name"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 984
    const-string v2, "data2"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v15, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    goto :goto_2

    .line 986
    :cond_4
    const-string v2, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 987
    const-string v2, "data1"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v15, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    goto :goto_2

    .line 991
    .end local v18    # "mimetype":Ljava/lang/String;
    :cond_5
    const/4 v2, -0x1

    iput v2, v15, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    .line 992
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 993
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 965
    :cond_6
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_1

    .line 998
    .end local v11    # "rawContactId":Ljava/lang/String;
    .end local v15    # "contactData":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    .end local v16    # "dataCursor":Landroid/database/Cursor;
    .end local v17    # "i":I
    .end local v21    # "size":I
    :cond_7
    if-eqz v19, :cond_8

    .line 999
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1002
    :cond_8
    return-object v14
.end method

.method private getContactSort(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 638
    const-string v0, ""

    .line 640
    .local v0, "strValue":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v1, :cond_0

    .line 641
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 643
    :cond_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v2, "Settings.Contact.ContactSort"

    invoke-virtual {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 644
    const-string v1, "CscContacts"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getContactSortValue="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    return-object v0
.end method

.method private getPreContact(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/util/ArrayList;
    .locals 11
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/sec/android/application/csc/CscParser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 700
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 702
    .local v5, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;>;"
    new-instance v8, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v8, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 704
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "Settings.Contact"

    invoke-virtual {v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 705
    .local v1, "contactNode":Lorg/w3c/dom/Node;
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "PreInstalledContact"

    invoke-virtual {v8, v1, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 707
    .local v2, "contactNodeList":Lorg/w3c/dom/NodeList;
    const-string v8, "CscContacts"

    const-string v9, "getPreContact"

    invoke-static {v8, v9}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    if-eqz v2, :cond_0

    .line 710
    const-string v8, "CscContacts"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getPreContact"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-ge v7, v8, :cond_0

    .line 712
    invoke-interface {v2, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 713
    .local v3, "contactNodeListChild":Lorg/w3c/dom/Node;
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "TelName"

    invoke-virtual {v8, v3, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 714
    .local v0, "contactName":Lorg/w3c/dom/Node;
    new-instance v6, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;

    const/4 v8, 0x0

    invoke-direct {v6, p0, v8}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Lcom/samsung/sec/android/application/csc/CscContacts$1;)V

    .line 715
    .local v6, "cscContact":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v8, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    .line 717
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "TelNum"

    invoke-virtual {v8, v3, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 718
    .local v4, "contactNumber":Lorg/w3c/dom/Node;
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v8, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    .line 720
    const/4 v8, -0x1

    iput v8, v6, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    .line 722
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 723
    const-string v8, "CscContacts"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getPreContact: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 727
    .end local v0    # "contactName":Lorg/w3c/dom/Node;
    .end local v3    # "contactNodeListChild":Lorg/w3c/dom/Node;
    .end local v4    # "contactNumber":Lorg/w3c/dom/Node;
    .end local v6    # "cscContact":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    .end local v7    # "i":I
    :cond_0
    return-object v5
.end method

.method private getSavePosition(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 609
    const-string v0, ""

    .line 611
    .local v0, "strValue":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v1, :cond_0

    .line 612
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 614
    :cond_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v2, "Settings.Contact.SavePosition"

    invoke-virtual {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 615
    const-string v1, "CscContacts"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSavePositionValue="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    return-object v0
.end method

.method private getSpeedDial(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/util/ArrayList;
    .locals 12
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/sec/android/application/csc/CscParser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 661
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 663
    .local v6, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;>;"
    new-instance v9, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v10, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v9, v10}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 665
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v10, "Settings.Contact"

    invoke-virtual {v9, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 666
    .local v0, "contactNode":Lorg/w3c/dom/Node;
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v10, "SpeedDial"

    invoke-virtual {v9, v0, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 668
    .local v1, "contactNodeList":Lorg/w3c/dom/NodeList;
    const-string v9, "CscContacts"

    const-string v10, "getSpeedDial"

    invoke-static {v9, v10}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    if-eqz v1, :cond_0

    .line 672
    const-string v9, "CscContacts"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getSpeedDial"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-ge v8, v9, :cond_0

    .line 675
    new-instance v7, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;

    const/4 v9, 0x0

    invoke-direct {v7, p0, v9}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Lcom/samsung/sec/android/application/csc/CscContacts$1;)V

    .line 677
    .local v7, "cscContact":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    invoke-interface {v1, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 678
    .local v2, "contactNodeListChild":Lorg/w3c/dom/Node;
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v10, "TelName"

    invoke-virtual {v9, v2, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 679
    .local v3, "contactNodeName":Lorg/w3c/dom/Node;
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v9, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    .line 681
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v10, "TelNum"

    invoke-virtual {v9, v2, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 682
    .local v4, "contactNodeNumber":Lorg/w3c/dom/Node;
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v9, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    .line 684
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v10, "DialPosition"

    invoke-virtual {v9, v2, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 686
    .local v5, "contactNodedialPosition":Lorg/w3c/dom/Node;
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v9, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, v7, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    .line 689
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    const-string v9, "CscContacts"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getSpeedDial Contact: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v7, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v7, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v7, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 695
    .end local v2    # "contactNodeListChild":Lorg/w3c/dom/Node;
    .end local v3    # "contactNodeName":Lorg/w3c/dom/Node;
    .end local v4    # "contactNodeNumber":Lorg/w3c/dom/Node;
    .end local v5    # "contactNodedialPosition":Lorg/w3c/dom/Node;
    .end local v7    # "cscContact":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    .end local v8    # "i":I
    :cond_0
    return-object v6
.end method

.method private getViewFrom(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 649
    const-string v0, ""

    .line 651
    .local v0, "strValue":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v1, :cond_0

    .line 652
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 654
    :cond_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v2, "Settings.Contact.ViewFrom"

    invoke-virtual {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 655
    const-string v1, "CscContacts"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getViewFromValue="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    return-object v0
.end method

.method private updateOthers()V
    .locals 28

    .prologue
    .line 439
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v2, :cond_0

    .line 440
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 443
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "ContactsData.Information.Name"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 444
    .local v8, "InformationName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "ContactsData.Information.Number"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 447
    .local v9, "InformationNumber":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscContacts;->getContacts()Ljava/util/ArrayList;

    move-result-object v15

    .line 448
    .local v15, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$Contact;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v26

    .line 452
    .local v26, "size":I
    if-lez v26, :cond_b

    .line 453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "content://com.android.contacts"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v14

    .line 455
    .local v14, "client":Landroid/content/ContentProviderClient;
    const/16 v25, 0x0

    .line 456
    .local v25, "results":[Landroid/content/ContentProviderResult;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 459
    .local v11, "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move/from16 v0, v17

    move/from16 v1, v26

    if-ge v0, v1, :cond_5

    .line 465
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v24

    .line 467
    .local v24, "rawContactIdIndex":I
    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/sec/android/application/csc/CscContacts$Contact;

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscContacts$Contact;->getName()Ljava/lang/String;

    move-result-object v19

    .line 468
    .local v19, "name":Ljava/lang/String;
    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    .line 469
    .local v12, "builder":Landroid/content/ContentProviderOperation$Builder;
    const-string v2, "account_name"

    const-string v3, "vnd.sec.contact.phone"

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 470
    const-string v2, "account_type"

    const-string v3, "vnd.sec.contact.phone"

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 473
    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 474
    const-string v2, "CSCContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UpdateOthers() name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 476
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscContacts;->DATA_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    .line 477
    const-string v2, "raw_contact_id"

    move/from16 v0, v24

    invoke-virtual {v12, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 478
    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/name"

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 479
    const-string v2, "data1"

    move-object/from16 v0, v19

    invoke-virtual {v12, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 480
    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    :cond_1
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 486
    .local v23, "phoneDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;>;"
    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/sec/android/application/csc/CscContacts$Contact;

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscContacts$Contact;->getPhoneData()Ljava/util/ArrayList;

    move-result-object v23

    .line 487
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;

    .line 488
    .local v22, "phoneData":Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;->getNumber()Ljava/lang/String;

    move-result-object v21

    .line 489
    .local v21, "number":Ljava/lang/String;
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;->getType()Ljava/lang/String;

    move-result-object v27

    .line 490
    .local v27, "type":Ljava/lang/String;
    const-string v2, "CSCContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "number: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 492
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscContacts;->DATA_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    .line 493
    const-string v2, "raw_contact_id"

    move/from16 v0, v24

    invoke-virtual {v12, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 494
    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 495
    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 496
    const-string v2, "data2"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 497
    const-string v2, "data3"

    move-object/from16 v0, v27

    invoke-virtual {v12, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 501
    :goto_2
    const-string v2, "data1"

    move-object/from16 v0, v21

    invoke-virtual {v12, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 503
    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 499
    :cond_3
    const-string v2, "data2"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_2

    .line 459
    .end local v21    # "number":Ljava/lang/String;
    .end local v22    # "phoneData":Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;
    .end local v27    # "type":Ljava/lang/String;
    :cond_4
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_0

    .line 508
    .end local v12    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v19    # "name":Ljava/lang/String;
    .end local v23    # "phoneDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;>;"
    .end local v24    # "rawContactIdIndex":I
    :cond_5
    const/4 v10, 0x3

    .line 509
    .local v10, "TRIAL":I
    :goto_3
    if-lez v10, :cond_9

    .line 511
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "account_type LIKE \'vnd.sec.contact.phone\'"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 513
    .local v13, "c":Landroid/database/Cursor;
    if-eqz v13, :cond_7

    .line 514
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v20

    .line 515
    .local v20, "nbcontact":I
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 517
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->numberOfContacts:I

    move/from16 v0, v20

    if-ge v0, v2, :cond_8

    .line 518
    if-eqz v20, :cond_6

    .line 519
    const-string v2, "CSCContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Contacts remain :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "account_type LIKE \'vnd.sec.contact.phone\'"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 523
    :cond_6
    invoke-virtual {v14, v11}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 540
    .end local v13    # "c":Landroid/database/Cursor;
    .end local v20    # "nbcontact":I
    :cond_7
    :goto_4
    add-int/lit8 v10, v10, -0x1

    goto :goto_3

    .line 525
    .restart local v13    # "c":Landroid/database/Cursor;
    .restart local v20    # "nbcontact":I
    :cond_8
    const-string v2, "CSCContacts"

    const-string v3, "Insertion successed"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 542
    .end local v13    # "c":Landroid/database/Cursor;
    .end local v20    # "nbcontact":I
    :cond_9
    if-eqz v14, :cond_a

    .line 543
    invoke-virtual {v14}, Landroid/content/ContentProviderClient;->release()Z

    .line 604
    .end local v10    # "TRIAL":I
    .end local v11    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v14    # "client":Landroid/content/ContentProviderClient;
    .end local v17    # "i":I
    .end local v25    # "results":[Landroid/content/ContentProviderResult;
    :cond_a
    :goto_5
    return-void

    .line 531
    .restart local v10    # "TRIAL":I
    .restart local v11    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v14    # "client":Landroid/content/ContentProviderClient;
    .restart local v17    # "i":I
    .restart local v25    # "results":[Landroid/content/ContentProviderResult;
    :catch_0
    move-exception v16

    .line 532
    .local v16, "e":Landroid/content/OperationApplicationException;
    invoke-virtual/range {v16 .. v16}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_4

    .line 533
    .end local v16    # "e":Landroid/content/OperationApplicationException;
    :catch_1
    move-exception v16

    .line 537
    .local v16, "e":Ljava/lang/NullPointerException;
    const-string v2, "CSCContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to find ContactProvider."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    invoke-virtual/range {v16 .. v16}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_4

    .line 547
    .end local v10    # "TRIAL":I
    .end local v11    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v14    # "client":Landroid/content/ContentProviderClient;
    .end local v16    # "e":Ljava/lang/NullPointerException;
    .end local v17    # "i":I
    .end local v25    # "results":[Landroid/content/ContentProviderResult;
    :cond_b
    if-eqz v8, :cond_f

    .line 552
    const/16 v25, 0x0

    .line 554
    .restart local v25    # "results":[Landroid/content/ContentProviderResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "content://com.android.contacts"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v14

    .line 556
    .restart local v14    # "client":Landroid/content/ContentProviderClient;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 558
    .restart local v11    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v24

    .line 560
    .restart local v24    # "rawContactIdIndex":I
    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    .line 562
    .restart local v12    # "builder":Landroid/content/ContentProviderOperation$Builder;
    const-string v2, "account_name"

    const-string v3, "vnd.sec.contact.phone"

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 563
    const-string v2, "account_type"

    const-string v3, "vnd.sec.contact.phone"

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 566
    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 568
    if-eqz v8, :cond_c

    .line 569
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscContacts;->DATA_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    .line 570
    const-string v2, "raw_contact_id"

    move/from16 v0, v24

    invoke-virtual {v12, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 571
    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/name"

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 572
    const-string v2, "data1"

    invoke-virtual {v12, v2, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 573
    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 575
    :cond_c
    if-eqz v9, :cond_d

    .line 576
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscContacts;->DATA_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    .line 577
    const-string v2, "raw_contact_id"

    move/from16 v0, v24

    invoke-virtual {v12, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 578
    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 579
    const-string v2, "data2"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 580
    const-string v2, "data1"

    invoke-virtual {v12, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 582
    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 586
    :cond_d
    :try_start_1
    invoke-virtual {v14, v11}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v25

    .line 595
    :goto_6
    if-eqz v14, :cond_e

    .line 596
    invoke-virtual {v14}, Landroid/content/ContentProviderClient;->release()Z

    .line 598
    :cond_e
    const-string v2, "CSCContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " ** Information Contact "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 589
    :catch_2
    move-exception v16

    .line 590
    .local v16, "e":Landroid/content/OperationApplicationException;
    invoke-virtual/range {v16 .. v16}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_6

    .line 591
    .end local v16    # "e":Landroid/content/OperationApplicationException;
    :catch_3
    move-exception v16

    .line 592
    .local v16, "e":Ljava/lang/NullPointerException;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_6

    .line 601
    .end local v11    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v12    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .end local v14    # "client":Landroid/content/ContentProviderClient;
    .end local v16    # "e":Ljava/lang/NullPointerException;
    .end local v24    # "rawContactIdIndex":I
    .end local v25    # "results":[Landroid/content/ContentProviderResult;
    :cond_f
    const-string v2, "CSCContacts"

    const-string v3, " ** Information Contact is not existed "

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 587
    .restart local v11    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v12    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .restart local v14    # "client":Landroid/content/ContentProviderClient;
    .restart local v24    # "rawContactIdIndex":I
    .restart local v25    # "results":[Landroid/content/ContentProviderResult;
    :catch_4
    move-exception v2

    goto :goto_6

    .line 529
    .end local v12    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .end local v24    # "rawContactIdIndex":I
    .restart local v10    # "TRIAL":I
    .restart local v17    # "i":I
    :catch_5
    move-exception v2

    goto/16 :goto_4
.end method

.method private updateSavePosition(Ljava/lang/String;)Z
    .locals 4
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1188
    const-string v0, ""

    .line 1190
    .local v0, "value":Ljava/lang/String;
    const-string v2, "CscContacts"

    const-string v3, "updateSavePosition start"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1192
    if-nez p1, :cond_0

    .line 1193
    const-string v2, "CscContacts"

    const-string v3, "updateSavePosition strValue is NULL"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1209
    :goto_0
    return v1

    .line 1196
    :cond_0
    const-string v2, "phone"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1197
    const-string v0, "vnd.sec.contact.phone"

    .line 1207
    :goto_1
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "contactsaveto"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1209
    const/4 v1, 0x1

    goto :goto_0

    .line 1198
    :cond_1
    const-string v2, "sim"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1199
    const-string v0, "vnd.sec.contact.sim"

    goto :goto_1

    .line 1200
    :cond_2
    const-string v2, "ask"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1201
    const-string v0, "Always ask"

    goto :goto_1

    .line 1203
    :cond_3
    const-string v2, "CscContacts"

    const-string v3, "updateSavePosition strValue is not supported"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateSort(Ljava/lang/String;)Z
    .locals 4
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1214
    const/4 v0, -0x1

    .line 1216
    .local v0, "value":I
    const-string v2, "CscContacts"

    const-string v3, "updateSort start"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1218
    if-nez p1, :cond_0

    .line 1219
    const-string v2, "CscContacts"

    const-string v3, "updateSort strValue is NULL"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1233
    :goto_0
    return v1

    .line 1222
    :cond_0
    const-string v2, "firstname"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1223
    const/4 v0, 0x1

    .line 1231
    :goto_1
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "android.contacts.SORT_ORDER"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1232
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "android.contacts.DISPLAY_ORDER"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1233
    const/4 v1, 0x1

    goto :goto_0

    .line 1224
    :cond_1
    const-string v2, "lastname"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1225
    const/4 v0, 0x2

    goto :goto_1

    .line 1227
    :cond_2
    const-string v2, "CscContacts"

    const-string v3, "strValue is not supported"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateSpeedDial(Ljava/util/ArrayList;)Z
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
        }
    .end annotation

    .prologue
    .line 1284
    .local p1, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 1285
    .local v12, "size":I
    const/4 v11, 0x0

    .line 1286
    .local v11, "results":[Landroid/content/ContentProviderResult;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const-string v16, "content://com.android.contacts"

    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v4

    .line 1289
    .local v4, "client":Landroid/content/ContentProviderClient;
    const-string v15, "CscContacts"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "updateSpeedDial start size="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1291
    if-lez v12, :cond_5

    .line 1293
    const-string v15, "content://com.android.contacts/contacts/speeddial"

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 1295
    .local v13, "speedDialUri":Landroid/net/Uri;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v12, :cond_5

    .line 1298
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1299
    .local v2, "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 1301
    .local v10, "rawContactIdIndex":I
    sget-object v15, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v15}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    .line 1303
    .local v3, "builder":Landroid/content/ContentProviderOperation$Builder;
    const-string v15, "account_name"

    const-string v16, "vnd.sec.contact.phone"

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1304
    const-string v15, "account_type"

    const-string v16, "vnd.sec.contact.phone"

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1307
    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v15

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1309
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;

    .line 1310
    .local v5, "cscContact":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    if-nez v5, :cond_1

    .line 1311
    const-string v15, "CscContacts"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "updateSpeedDial contacts is null:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1295
    :cond_0
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 1315
    :cond_1
    iget-object v15, v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    if-eqz v15, :cond_2

    iget-object v15, v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    if-lez v15, :cond_2

    .line 1316
    const-string v15, "content://com.android.contacts/data/"

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-static {v15}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    .line 1317
    const-string v15, "raw_contact_id"

    invoke-virtual {v3, v15, v10}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 1318
    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/name"

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1319
    const-string v15, "data2"

    iget-object v0, v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1320
    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v15

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1322
    :cond_2
    iget-object v15, v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    if-eqz v15, :cond_3

    iget-object v15, v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    if-lez v15, :cond_3

    .line 1323
    const-string v15, "content://com.android.contacts/data/"

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-static {v15}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    .line 1324
    const-string v15, "raw_contact_id"

    invoke-virtual {v3, v15, v10}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 1325
    const-string v15, "mimetype"

    const-string v16, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1326
    const-string v15, "VZW"

    const-string v16, "ro.csc.sales_code"

    invoke-static/range {v16 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1327
    const-string v15, "data2"

    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1331
    :goto_2
    const-string v15, "data1"

    iget-object v0, v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1333
    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v15

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1336
    :cond_3
    const-string v15, "CscContacts"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " ** Information Contact Dial:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "Name : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget-object v0, v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mName:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " , "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "Number : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget-object v0, v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mNumber:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1341
    :try_start_0
    invoke-virtual {v4, v2}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 1349
    :goto_3
    iget v15, v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    if-ltz v15, :cond_0

    if-eqz v11, :cond_0

    const/4 v15, 0x2

    aget-object v15, v11, v15

    if-eqz v15, :cond_0

    .line 1350
    const/4 v15, 0x2

    aget-object v15, v11, v15

    iget-object v15, v15, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v15}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    .line 1355
    .local v6, "dataId":J
    const-string v15, "CscContacts"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "dataid="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    long-to-int v0, v6

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1356
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1357
    .local v14, "values":Landroid/content/ContentValues;
    const-string v15, "key_number"

    iget v0, v5, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;->mDialPosition:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1358
    const-string v15, "speed_dial_data_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1359
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v15, v13, v14}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_1

    .line 1329
    .end local v6    # "dataId":J
    .end local v14    # "values":Landroid/content/ContentValues;
    :cond_4
    const-string v15, "data2"

    const/16 v16, 0x2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto/16 :goto_2

    .line 1344
    :catch_0
    move-exception v8

    .line 1345
    .local v8, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v8}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_3

    .line 1364
    .end local v2    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v3    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .end local v5    # "cscContact":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    .end local v8    # "e":Landroid/content/OperationApplicationException;
    .end local v9    # "i":I
    .end local v10    # "rawContactIdIndex":I
    .end local v13    # "speedDialUri":Landroid/net/Uri;
    :cond_5
    const-string v15, "CscContacts"

    const-string v16, "updatePreContact success"

    invoke-static/range {v15 .. v16}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1365
    const/4 v15, 0x1

    return v15

    .line 1342
    .restart local v2    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v3    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .restart local v5    # "cscContact":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactData;
    .restart local v9    # "i":I
    .restart local v10    # "rawContactIdIndex":I
    .restart local v13    # "speedDialUri":Landroid/net/Uri;
    :catch_1
    move-exception v15

    goto :goto_3
.end method

.method private updateViewFrom(Ljava/lang/String;)Z
    .locals 8
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 1238
    const-string v0, ""

    .line 1240
    .local v0, "accountName":Ljava/lang/String;
    const-string v5, "CscContacts"

    const-string v6, "updateViewFrom start"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1242
    if-nez p1, :cond_0

    .line 1243
    const-string v5, "CscContacts"

    const-string v6, "updateSavePosition strValue is NULL"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279
    :goto_0
    return v4

    .line 1246
    :cond_0
    const-string v5, "phone"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1247
    const-string v5, "CscContacts"

    const-string v6, "Only Phone displayed"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1248
    const-string v0, "vnd.sec.contact.sim"

    .line 1258
    :goto_1
    const-string v5, "CscContacts"

    const-string v6, "Update Setting"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1259
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1260
    .local v3, "values":Landroid/content/ContentValues;
    sget-object v5, Landroid/provider/ContactsContract$Settings;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "account_name"

    invoke-virtual {v5, v6, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "account_type"

    invoke-virtual {v5, v6, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 1263
    .local v2, "settingsUri":Landroid/net/Uri;
    const-string v5, "ungrouped_visible"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1264
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v5, v2, v3, v7, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-gez v5, :cond_1

    .line 1265
    const-string v5, "CscContacts"

    const-string v6, "UNGROUPED_VISIBLE is not exist"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1269
    :cond_1
    new-instance v3, Landroid/content/ContentValues;

    .end local v3    # "values":Landroid/content/ContentValues;
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1270
    .restart local v3    # "values":Landroid/content/ContentValues;
    sget-object v5, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "account_name"

    invoke-virtual {v5, v6, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "account_type"

    invoke-virtual {v5, v6, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 1273
    .local v1, "groupsUri":Landroid/net/Uri;
    const-string v5, "group_visible"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1274
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v4, v1, v3, v7, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_2

    .line 1275
    const-string v4, "CscContacts"

    const-string v5, "Groups.GROUP_VISIBLE is not exist"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1277
    :cond_2
    const-string v4, "CscContacts"

    const-string v5, "updateSavePosition result"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1249
    .end local v1    # "groupsUri":Landroid/net/Uri;
    .end local v2    # "settingsUri":Landroid/net/Uri;
    .end local v3    # "values":Landroid/content/ContentValues;
    :cond_3
    const-string v5, "sim"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1250
    const-string v5, "CscContacts"

    const-string v6, "Only SIM displayed"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1251
    const-string v0, "vnd.sec.contact.phone"

    goto/16 :goto_1

    .line 1253
    :cond_4
    const-string v5, "CscContacts"

    const-string v6, "updateSavePosition strValue is not supported"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public compare()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 183
    const-string v8, "NOERROR"

    .line 187
    .local v8, "answer":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v0, :cond_0

    .line 188
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "ContactsData.Information.Name"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 190
    .local v6, "InformationName":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "ContactsData.Information.Number"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 192
    .local v7, "InformationNumber":Ljava/lang/String;
    if-eqz v6, :cond_4

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mimetype=\'vnd.android.cursor.item/phone_v2\' AND display_name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "data1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 197
    .local v3, "selectString":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "data1"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "display_name"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "mimetype"

    aput-object v1, v2, v0

    .line 201
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 203
    .local v9, "cscCursor":Landroid/database/Cursor;
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 204
    :cond_1
    const-string v0, "CscContacts"

    const-string v1, " ** Information Contact is not saved "

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_2
    const-string v0, "CscContacts"

    const-string v1, " ** Information Contact is saved "

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    if-eqz v9, :cond_3

    .line 209
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 215
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selectString":Ljava/lang/String;
    .end local v9    # "cscCursor":Landroid/database/Cursor;
    :cond_3
    :goto_0
    const-string v8, ""

    .line 217
    const-string v0, "CscContacts"

    const-string v1, "Customer compare start"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 222
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Contact"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "ContactsData_Multi.Information"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 224
    const-string v0, "CscContacts"

    const-string v1, "<Contact> is not exist."

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscContacts;->checkContactExist()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 226
    const-string v0, "CONTACTS"

    .line 281
    :goto_1
    return-object v0

    .line 212
    :cond_4
    const-string v0, "CscContacts"

    const-string v1, " ** Information Contact is not needed "

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 228
    :cond_5
    const-string v0, "NOERROR"

    goto :goto_1

    .line 232
    :cond_6
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "ContactsData_Multi.Information"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 234
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscContacts;->comparePreInstalledContacts()Z
    :try_end_0
    .catch Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    :cond_7
    :goto_2
    const-string v0, "CscContacts"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "compare result:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    .line 278
    const-string v8, "NOERROR"

    move-object v0, v8

    goto :goto_1

    .line 235
    :catch_0
    move-exception v10

    .line 236
    .local v10, "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    invoke-virtual {v10}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->getTrace()Ljava/lang/String;

    move-result-object v8

    .line 237
    goto :goto_2

    .line 241
    .end local v10    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    :cond_8
    :try_start_1
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts;->getSavePosition(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts;->compareSavePosition(Ljava/lang/String;)Z
    :try_end_1
    .catch Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException; {:try_start_1 .. :try_end_1} :catch_1

    .line 247
    :goto_3
    :try_start_2
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts;->getContactSort(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts;->compareSort(Ljava/lang/String;)Z
    :try_end_2
    .catch Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException; {:try_start_2 .. :try_end_2} :catch_2

    .line 253
    :goto_4
    :try_start_3
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts;->getViewFrom(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts;->compareViewFrom(Ljava/lang/String;)Z
    :try_end_3
    .catch Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException; {:try_start_3 .. :try_end_3} :catch_3

    .line 259
    :goto_5
    :try_start_4
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts;->getPreContact(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts;->comparePreloadContact(Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "comparePreloadContact Error"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v8

    .line 267
    :cond_9
    :goto_6
    :try_start_5
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts;->getSpeedDial(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscContacts;->compareSpeedDial(Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "compareSpeedDial Error"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_5
    .catch Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException; {:try_start_5 .. :try_end_5} :catch_5

    move-result-object v8

    goto/16 :goto_2

    .line 242
    :catch_1
    move-exception v10

    .line 243
    .restart local v10    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    invoke-virtual {v10}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->getTrace()Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    .line 248
    .end local v10    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    :catch_2
    move-exception v10

    .line 249
    .restart local v10    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->getTrace()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_4

    .line 254
    .end local v10    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    :catch_3
    move-exception v10

    .line 255
    .restart local v10    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->getTrace()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_5

    .line 262
    .end local v10    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    :catch_4
    move-exception v10

    .line 263
    .restart local v10    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->getTrace()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_6

    .line 270
    .end local v10    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    :catch_5
    move-exception v10

    .line 271
    .restart local v10    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->getTrace()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_2

    .line 280
    .end local v10    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CscContacts : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 281
    const-string v8, "CONTACTS"

    move-object v0, v8

    goto/16 :goto_1
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 40
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 1374
    const/16 v39, 0x0

    .line 1375
    .local v39, "valueLocalStr":Ljava/lang/String;
    const/16 v37, 0x0

    .line 1376
    .local v37, "valueCscStr":Ljava/lang/String;
    const/16 v38, 0x0

    .line 1379
    .local v38, "valueLocalInt":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "contactsaveto"

    invoke-static {v2, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    .line 1380
    const/16 v37, 0x0

    .line 1382
    const-string v2, "vnd.sec.contact.phone"

    move-object/from16 v0, v39

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1383
    const-string v37, "phone"

    .line 1390
    :cond_0
    :goto_0
    if-eqz v37, :cond_1

    .line 1391
    const-string v2, "Settings.Contact.SavePosition"

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v2, v1}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1395
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "android.contacts.SORT_ORDER"

    const/4 v6, -0x1

    invoke-static {v2, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v38

    .line 1396
    const/16 v37, 0x0

    .line 1398
    const/4 v2, 0x1

    move/from16 v0, v38

    if-ne v0, v2, :cond_6

    .line 1399
    const-string v37, "firstname"

    .line 1404
    :cond_2
    :goto_1
    if-eqz v37, :cond_3

    .line 1405
    const-string v2, "Settings.Contact.ContactSort"

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v2, v1}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    :cond_3
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v24

    .line 1411
    .local v24, "SIM_account_visible":Ljava/lang/Boolean;
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    .line 1414
    .local v21, "Phone_account_visible":Ljava/lang/Boolean;
    sget-object v2, Landroid/provider/ContactsContract$Settings;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "account_name"

    const-string v6, "primary.sim.account_name"

    invoke-virtual {v2, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "account_type"

    const-string v6, "vnd.sec.contact.sim"

    invoke-virtual {v2, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1419
    .local v3, "SIMsettingsUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v8, "ungrouped_visible"

    aput-object v8, v4, v6

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 1423
    .local v25, "SIMcursorSetting":Landroid/database/Cursor;
    const/4 v2, -0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1426
    :goto_2
    :try_start_0
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1427
    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const-wide/16 v16, 0x1

    cmp-long v2, v10, v16

    if-nez v2, :cond_7

    .line 1428
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v24

    goto :goto_2

    .line 1384
    .end local v3    # "SIMsettingsUri":Landroid/net/Uri;
    .end local v21    # "Phone_account_visible":Ljava/lang/Boolean;
    .end local v24    # "SIM_account_visible":Ljava/lang/Boolean;
    .end local v25    # "SIMcursorSetting":Landroid/database/Cursor;
    :cond_4
    const-string v2, "vnd.sec.contact.sim"

    move-object/from16 v0, v39

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1385
    const-string v37, "sim"

    goto/16 :goto_0

    .line 1386
    :cond_5
    const-string v2, "Always ask"

    move-object/from16 v0, v39

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1387
    const-string v37, "ask"

    goto/16 :goto_0

    .line 1400
    :cond_6
    const/4 v2, 0x2

    move/from16 v0, v38

    if-ne v0, v2, :cond_2

    .line 1401
    const-string v37, "lastname"

    goto/16 :goto_1

    .line 1430
    .restart local v3    # "SIMsettingsUri":Landroid/net/Uri;
    .restart local v21    # "Phone_account_visible":Ljava/lang/Boolean;
    .restart local v24    # "SIM_account_visible":Ljava/lang/Boolean;
    .restart local v25    # "SIMcursorSetting":Landroid/database/Cursor;
    :cond_7
    const/4 v2, 0x0

    :try_start_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v24

    goto :goto_2

    .line 1434
    :cond_8
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 1440
    sget-object v2, Landroid/provider/ContactsContract$Settings;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "account_name"

    const-string v6, "vnd.sec.contact.phone"

    invoke-virtual {v2, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "account_type"

    const-string v6, "vnd.sec.contact.phone"

    invoke-virtual {v2, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 1445
    .local v5, "PhonesettingsUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v8, "ungrouped_visible"

    aput-object v8, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 1449
    .local v23, "PhonecursorSetting":Landroid/database/Cursor;
    const/4 v2, -0x1

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1452
    :goto_3
    :try_start_2
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1453
    const/4 v2, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const-wide/16 v16, 0x1

    cmp-long v2, v10, v16

    if-nez v2, :cond_9

    .line 1454
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v21

    goto :goto_3

    .line 1434
    .end local v5    # "PhonesettingsUri":Landroid/net/Uri;
    .end local v23    # "PhonecursorSetting":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    throw v2

    .line 1456
    .restart local v5    # "PhonesettingsUri":Landroid/net/Uri;
    .restart local v23    # "PhonecursorSetting":Landroid/database/Cursor;
    :cond_9
    const/4 v2, 0x0

    :try_start_3
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v21

    goto :goto_3

    .line 1460
    :cond_a
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 1463
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1465
    sget-object v2, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "account_name"

    const-string v6, "vnd.sec.contact.phone"

    invoke-virtual {v2, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "account_type"

    const-string v6, "vnd.sec.contact.phone"

    invoke-virtual {v2, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    .line 1468
    .local v7, "groupsUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x2

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "group_visible"

    aput-object v4, v8, v2

    const/4 v2, 0x1

    const-string v4, "title"

    aput-object v4, v8, v2

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 1472
    .local v22, "PhonecursorGroup":Landroid/database/Cursor;
    const/4 v2, -0x1

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1475
    :cond_b
    :goto_4
    :try_start_4
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1476
    const/4 v2, 0x0

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const-wide/16 v16, 0x1

    cmp-long v2, v10, v16

    if-eqz v2, :cond_b

    .line 1478
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v21

    goto :goto_4

    .line 1460
    .end local v7    # "groupsUri":Landroid/net/Uri;
    .end local v22    # "PhonecursorGroup":Landroid/database/Cursor;
    :catchall_1
    move-exception v2

    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    throw v2

    .line 1482
    .restart local v7    # "groupsUri":Landroid/net/Uri;
    .restart local v22    # "PhonecursorGroup":Landroid/database/Cursor;
    :cond_c
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 1486
    .end local v7    # "groupsUri":Landroid/net/Uri;
    .end local v22    # "PhonecursorGroup":Landroid/database/Cursor;
    :cond_d
    const/16 v37, 0x0

    .line 1487
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1499
    :cond_e
    :goto_5
    if-eqz v37, :cond_f

    .line 1500
    const-string v2, "Settings.Contact.ViewFrom"

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v2, v1}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1504
    :cond_f
    const-string v2, "content://com.android.contacts/contacts/speeddial"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 1505
    .local v9, "speedDialUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v35

    .line 1506
    .local v35, "speedDialCursor":Landroid/database/Cursor;
    const/16 v30, 0x0

    .line 1507
    .local v30, "name":Ljava/lang/String;
    const/16 v32, 0x0

    .line 1509
    .local v32, "number":Ljava/lang/String;
    const-string v13, "raw_contact_id=? AND mimetype=?"

    .line 1510
    .local v13, "selection":Ljava/lang/String;
    const/4 v2, 0x2

    new-array v14, v2, [Ljava/lang/String;

    .line 1512
    .local v14, "selectionArgs":[Ljava/lang/String;
    if-eqz v35, :cond_16

    invoke-interface/range {v35 .. v35}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_16

    .line 1513
    const-string v2, "Settings.Contact.NbSpeedDial"

    invoke-interface/range {v35 .. v35}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1515
    const/4 v2, -0x1

    move-object/from16 v0, v35

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1516
    :goto_6
    invoke-interface/range {v35 .. v35}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1517
    const-string v2, "SpeedDial"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->newElement(Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscXMLElement;

    move-result-object v26

    .line 1518
    .local v26, "contactElement":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    const-string v2, "raw_contact_id"

    move-object/from16 v0, v35

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v35

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    .line 1520
    .local v34, "rawContactId":Ljava/lang/Long;
    const-string v2, "key_number"

    move-object/from16 v0, v35

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v35

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v36

    .line 1523
    .local v36, "speeddialnum":Ljava/lang/Long;
    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v14, v2

    .line 1524
    const/4 v2, 0x1

    const-string v4, "vnd.android.cursor.item/name"

    aput-object v4, v14, v2

    .line 1525
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    sget-object v11, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v12, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "data2"

    aput-object v4, v12, v2

    const/4 v15, 0x0

    invoke-virtual/range {v10 .. v15}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v31

    .line 1528
    .local v31, "nameCursor":Landroid/database/Cursor;
    if-eqz v31, :cond_11

    .line 1529
    invoke-interface/range {v31 .. v31}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1530
    const-string v2, "data2"

    move-object/from16 v0, v31

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v31

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v30

    .line 1533
    :cond_10
    invoke-interface/range {v31 .. v31}, Landroid/database/Cursor;->close()V

    .line 1537
    :cond_11
    const/4 v2, 0x1

    const-string v4, "vnd.android.cursor.item/phone_v2"

    aput-object v4, v14, v2

    .line 1538
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    sget-object v11, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v12, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "data1"

    aput-object v4, v12, v2

    const/4 v15, 0x0

    invoke-virtual/range {v10 .. v15}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v33

    .line 1541
    .local v33, "phoneCursor":Landroid/database/Cursor;
    if-eqz v33, :cond_13

    .line 1542
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1543
    const-string v2, "data1"

    move-object/from16 v0, v33

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v33

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    .line 1545
    :cond_12
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->close()V

    .line 1549
    :cond_13
    const-string v2, "DialPosition"

    invoke-virtual/range {v36 .. v36}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v16, 0x1

    sub-long v10, v10, v16

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v4}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1550
    const-string v2, "TelName"

    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-virtual {v0, v2, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1551
    const-string v2, "TelNum"

    move-object/from16 v0, v26

    move-object/from16 v1, v32

    invoke-virtual {v0, v2, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1552
    const-string v2, "Settings.Contact"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v2, v1}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addElement(Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscXMLElement;)V

    goto/16 :goto_6

    .line 1482
    .end local v9    # "speedDialUri":Landroid/net/Uri;
    .end local v13    # "selection":Ljava/lang/String;
    .end local v14    # "selectionArgs":[Ljava/lang/String;
    .end local v26    # "contactElement":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    .end local v30    # "name":Ljava/lang/String;
    .end local v31    # "nameCursor":Landroid/database/Cursor;
    .end local v32    # "number":Ljava/lang/String;
    .end local v33    # "phoneCursor":Landroid/database/Cursor;
    .end local v34    # "rawContactId":Ljava/lang/Long;
    .end local v35    # "speedDialCursor":Landroid/database/Cursor;
    .end local v36    # "speeddialnum":Ljava/lang/Long;
    .restart local v7    # "groupsUri":Landroid/net/Uri;
    .restart local v22    # "PhonecursorGroup":Landroid/database/Cursor;
    :catchall_2
    move-exception v2

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    throw v2

    .line 1489
    .end local v7    # "groupsUri":Landroid/net/Uri;
    .end local v22    # "PhonecursorGroup":Landroid/database/Cursor;
    :cond_14
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1491
    const-string v37, "phone"

    goto/16 :goto_5

    .line 1492
    :cond_15
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1494
    const-string v37, "sim"

    goto/16 :goto_5

    .line 1557
    .restart local v9    # "speedDialUri":Landroid/net/Uri;
    .restart local v13    # "selection":Ljava/lang/String;
    .restart local v14    # "selectionArgs":[Ljava/lang/String;
    .restart local v30    # "name":Ljava/lang/String;
    .restart local v32    # "number":Ljava/lang/String;
    .restart local v35    # "speedDialCursor":Landroid/database/Cursor;
    :cond_16
    if-eqz v35, :cond_17

    .line 1558
    invoke-interface/range {v35 .. v35}, Landroid/database/Cursor;->close()V

    .line 1562
    :cond_17
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    sget-object v16, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v15 .. v20}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v29

    .line 1563
    .local v29, "contactcursor":Landroid/database/Cursor;
    const/16 v27, 0x0

    .line 1564
    .local v27, "contact_name":Ljava/lang/String;
    const/16 v28, 0x0

    .line 1566
    .local v28, "contact_number":Ljava/lang/String;
    const-string v18, "raw_contact_id=? AND mimetype=?"

    .line 1567
    .local v18, "contactselection":Ljava/lang/String;
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v19, v0

    .line 1568
    .local v19, "contactselectionArgs":[Ljava/lang/String;
    if-eqz v29, :cond_1a

    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_1a

    .line 1569
    const-string v2, "Settings.Contact.NbPreInstalledContact"

    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1571
    const/4 v2, -0x1

    move-object/from16 v0, v29

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1572
    :goto_7
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1573
    const-string v2, "PreInstalledContact"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->newElement(Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscXMLElement;

    move-result-object v26

    .line 1574
    .restart local v26    # "contactElement":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    const-string v2, "display_name"

    move-object/from16 v0, v29

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v29

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 1578
    const-string v2, "_id"

    move-object/from16 v0, v29

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v29

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    .line 1581
    .restart local v34    # "rawContactId":Ljava/lang/Long;
    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v19, v2

    .line 1582
    const/4 v2, 0x1

    const-string v4, "vnd.android.cursor.item/phone_v2"

    aput-object v4, v19, v2

    .line 1583
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mResolver:Landroid/content/ContentResolver;

    sget-object v16, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/4 v2, 0x0

    const-string v4, "data1"

    aput-object v4, v17, v2

    const/16 v20, 0x0

    invoke-virtual/range {v15 .. v20}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v33

    .line 1586
    .restart local v33    # "phoneCursor":Landroid/database/Cursor;
    if-eqz v33, :cond_19

    .line 1587
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 1588
    const-string v2, "data1"

    move-object/from16 v0, v33

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v33

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 1591
    :cond_18
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->close()V

    .line 1595
    :cond_19
    const-string v2, "TelName"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v2, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1596
    const-string v2, "TelNum"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v2, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1597
    const-string v2, "Settings.Contact"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v2, v1}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addElement(Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscXMLElement;)V

    goto/16 :goto_7

    .line 1600
    .end local v26    # "contactElement":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    .end local v33    # "phoneCursor":Landroid/database/Cursor;
    .end local v34    # "rawContactId":Ljava/lang/Long;
    :cond_1a
    if-eqz v29, :cond_1b

    .line 1601
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->close()V

    .line 1603
    :cond_1b
    return-void
.end method

.method public getContacts()Ljava/util/ArrayList;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscContacts$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 386
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 387
    .local v6, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$Contact;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v18, v0

    const-string v19, "ContactsData_Multi.Information"

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 388
    .local v3, "contactNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v18, v0

    const-string v19, "Contacts"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 396
    .local v4, "contactNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v4, :cond_3

    .line 397
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/sec/android/application/csc/CscContacts;->numberOfContacts:I

    .line 398
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->numberOfContacts:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v7, v0, :cond_3

    .line 399
    invoke-interface {v4, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 400
    .local v5, "contactNodeListChild":Lorg/w3c/dom/Node;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 402
    .local v14, "phoneDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v18, v0

    const-string v19, "Name"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 403
    .local v10, "nameNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v18, v0

    const-string v19, "Number"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v12

    .line 404
    .local v12, "numberNodeList":Lorg/w3c/dom/NodeList;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v18, v0

    const-string v19, "Type"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v17

    .line 405
    .local v17, "typeNodeList":Lorg/w3c/dom/NodeList;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    .line 406
    .local v9, "name":Ljava/lang/String;
    const-string v18, "CSCContacts"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getContacts() #"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v18, "CSCContacts"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Name: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v15

    .line 409
    .local v15, "sizeOfNumberNodeList":I
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    if-ge v8, v15, :cond_1

    .line 410
    const/4 v11, 0x0

    .line 411
    .local v11, "number":Ljava/lang/String;
    const/16 v16, 0x0

    .line 412
    .local v16, "type":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v18, v0

    invoke-interface {v12, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v11

    .line 413
    if-eqz v17, :cond_0

    invoke-interface/range {v17 .. v17}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v18

    move/from16 v0, v18

    if-le v0, v8, :cond_0

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v16

    .line 415
    :cond_0
    new-instance v13, Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;

    move-object/from16 v0, v16

    invoke-direct {v13, v11, v0}, Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    .local v13, "phoneData":Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417
    const-string v18, "CSCContacts"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Number: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", Type: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 420
    .end local v11    # "number":Ljava/lang/String;
    .end local v13    # "phoneData":Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;
    .end local v16    # "type":Ljava/lang/String;
    :cond_1
    if-eqz v9, :cond_2

    .line 421
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscContacts$Contact;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v9, v14}, Lcom/samsung/sec/android/application/csc/CscContacts$Contact;-><init>(Lcom/samsung/sec/android/application/csc/CscContacts;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 422
    .local v2, "contact":Lcom/samsung/sec/android/application/csc/CscContacts$Contact;
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 398
    .end local v2    # "contact":Lcom/samsung/sec/android/application/csc/CscContacts$Contact;
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 424
    :cond_2
    const-string v18, "CSCContacts"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, " ** getContacts ("

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ") : contact name is null. Skip.."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 429
    .end local v5    # "contactNodeListChild":Lorg/w3c/dom/Node;
    .end local v7    # "i":I
    .end local v8    # "j":I
    .end local v9    # "name":Ljava/lang/String;
    .end local v10    # "nameNode":Lorg/w3c/dom/Node;
    .end local v12    # "numberNodeList":Lorg/w3c/dom/NodeList;
    .end local v14    # "phoneDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;>;"
    .end local v15    # "sizeOfNumberNodeList":I
    .end local v17    # "typeNodeList":Lorg/w3c/dom/NodeList;
    :cond_3
    return-object v6
.end method

.method public isPreloadTMB()Z
    .locals 5

    .prologue
    .line 316
    const/4 v0, 0x0

    .line 318
    .local v0, "isIn":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 319
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const-string v2, "com.tmobile.vvm.application"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    const/4 v0, 0x1

    .line 324
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    const-string v2, "CscContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " isPreloadTMB()? : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    return v0

    .line 322
    :catch_0
    move-exception v2

    goto :goto_0

    .line 321
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method public update()V
    .locals 5

    .prologue
    .line 330
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscContacts;->updateOthers()V

    .line 332
    const-string v0, ""

    .line 334
    .local v0, "answer":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v2, :cond_0

    .line 335
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 337
    :cond_0
    const-string v2, "CscContacts"

    const-string v3, "update start"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Settings.Contact"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 340
    const-string v2, "CscContacts"

    const-string v3, "<Contact> is not exist."

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    :goto_0
    return-void

    .line 345
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscContacts;->getSavePosition(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscContacts;->updateSavePosition(Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscContacts;->getContactSort(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscContacts;->updateSort(Ljava/lang/String;)Z
    :try_end_1
    .catch Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException; {:try_start_1 .. :try_end_1} :catch_1

    .line 357
    :goto_2
    :try_start_2
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscContacts;->getViewFrom(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscContacts;->updateViewFrom(Ljava/lang/String;)Z
    :try_end_2
    .catch Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException; {:try_start_2 .. :try_end_2} :catch_2

    .line 363
    :goto_3
    :try_start_3
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscContacts;->getPreContact(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscContacts;->updateSpeedDial(Ljava/util/ArrayList;)Z
    :try_end_3
    .catch Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException; {:try_start_3 .. :try_end_3} :catch_3

    .line 369
    :goto_4
    :try_start_4
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscContacts;->mParserCustomer:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscContacts;->getSpeedDial(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscContacts;->updateSpeedDial(Ljava/util/ArrayList;)Z
    :try_end_4
    .catch Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException; {:try_start_4 .. :try_end_4} :catch_4

    .line 374
    :goto_5
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 375
    const-string v0, "NOERROR"

    .line 377
    :cond_2
    const-string v2, "CscContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update Result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 346
    :catch_0
    move-exception v1

    .line 347
    .local v1, "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->getTrace()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 352
    .end local v1    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    :catch_1
    move-exception v1

    .line 353
    .restart local v1    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->getTrace()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 358
    .end local v1    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    :catch_2
    move-exception v1

    .line 359
    .restart local v1    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->getTrace()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 364
    .end local v1    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    :catch_3
    move-exception v1

    .line 365
    .restart local v1    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->getTrace()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 370
    .end local v1    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    :catch_4
    move-exception v1

    .line 371
    .restart local v1    # "e":Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->getTrace()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5
.end method

.method public updateForSubUser()V
    .locals 0

    .prologue
    .line 381
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscContacts;->update()V

    .line 382
    return-void
.end method
