.class public Lcom/samsung/sec/android/application/csc/CscIms;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscIms.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscIms$RCS;,
        Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;,
        Lcom/samsung/sec/android/application/csc/CscIms$Setting;,
        Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;,
        Lcom/samsung/sec/android/application/csc/CscIms$Common;,
        Lcom/samsung/sec/android/application/csc/CscIms$Profile;,
        Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;,
        Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;,
        Lcom/samsung/sec/android/application/csc/CscIms$NetworkInfo;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/samsung/sec/android/application/csc/CscIms;


# instance fields
.field private final CSCIMSXML_ERROR_NONE:I

.field private final CSCIMSXML_ERROR_NOTAGFOUND:I

.field private final CSCIMSXML_ERROR_VERSION_MISMATCH:I

.field private final CUSTOMER_CSC_FILE:Ljava/lang/String;

.field private final LOG_TAG:Ljava/lang/String;

.field private final PDN_TYPE_EMERGENCY:Ljava/lang/String;

.field private final PDN_TYPE_IMS:Ljava/lang/String;

.field private final PDN_TYPE_INTERNET:Ljava/lang/String;

.field private final PDN_TYPE_WIFI:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

.field private mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

.field private mNetworkInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscIms$NetworkInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mResolver:Landroid/content/ContentResolver;

.field private mbValidProfileXML:Z

.field private mbValidSettingXML:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscIms;->mInstance:Lcom/samsung/sec/android/application/csc/CscIms;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 270
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 18
    const-string v0, "CscIms"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->LOG_TAG:Ljava/lang/String;

    .line 145
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 156
    const-string v0, "IMS"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->PDN_TYPE_IMS:Ljava/lang/String;

    .line 157
    const-string v0, "INTERNET"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->PDN_TYPE_INTERNET:Ljava/lang/String;

    .line 158
    const-string v0, "WIFI"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->PDN_TYPE_WIFI:Ljava/lang/String;

    .line 159
    const-string v0, "EMERGENCY"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->PDN_TYPE_EMERGENCY:Ljava/lang/String;

    .line 161
    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscIms;->CSCIMSXML_ERROR_NONE:I

    .line 162
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->CSCIMSXML_ERROR_NOTAGFOUND:I

    .line 163
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->CSCIMSXML_ERROR_VERSION_MISMATCH:I

    .line 165
    iput-boolean v1, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mbValidProfileXML:Z

    .line 166
    iput-boolean v1, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mbValidSettingXML:Z

    .line 271
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mContext:Landroid/content/Context;

    .line 272
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mResolver:Landroid/content/ContentResolver;

    .line 274
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mNetworkInfos:Ljava/util/ArrayList;

    .line 275
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    .line 276
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    .line 277
    return-void
.end method

.method private ConvertEmergencyDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 1875
    if-nez p1, :cond_0

    .line 1876
    const/4 v0, 0x0

    .line 1884
    :goto_0
    return-object v0

    .line 1879
    :cond_0
    const-string v0, "CS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1880
    const-string v0, "0"

    goto :goto_0

    .line 1881
    :cond_1
    const-string v0, "PS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1882
    const-string v0, "1"

    goto :goto_0

    .line 1884
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method private ConvertSMSoIPDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 1889
    if-nez p1, :cond_0

    .line 1890
    const/4 v0, 0x0

    .line 1899
    :goto_0
    return-object v0

    .line 1894
    :cond_0
    const-string v0, "SMSoSGs"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1895
    const-string v0, "0"

    goto :goto_0

    .line 1896
    :cond_1
    const-string v0, "SMSoIP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1897
    const-string v0, "1"

    goto :goto_0

    .line 1899
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method private ConvertTRUEFALSE(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1842
    if-nez p1, :cond_0

    .line 1843
    const/4 v0, 0x0

    .line 1851
    :goto_0
    return-object v0

    .line 1846
    :cond_0
    const-string v0, "FALSE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1847
    const-string v0, "0"

    goto :goto_0

    .line 1848
    :cond_1
    const-string v0, "TRUE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1849
    const-string v0, "1"

    goto :goto_0

    .line 1851
    :cond_2
    const-string v0, "0"

    goto :goto_0
.end method

.method private ConvertVoiceDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 1856
    if-nez p1, :cond_0

    .line 1857
    const/4 v0, 0x0

    .line 1870
    :goto_0
    return-object v0

    .line 1861
    :cond_0
    const-string v0, "CSVoiceOnly"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1862
    const-string v0, "1"

    goto :goto_0

    .line 1863
    :cond_1
    const-string v0, "CSVoicePreferred"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1864
    const-string v0, "2"

    goto :goto_0

    .line 1865
    :cond_2
    const-string v0, "IMSPSVoicePreferred"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1866
    const-string v0, "3"

    goto :goto_0

    .line 1867
    :cond_3
    const-string v0, "IMSPSVoiceOnly"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1868
    const-string v0, "4"

    goto :goto_0

    .line 1870
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method private ConvertssDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 1904
    if-nez p1, :cond_0

    .line 1905
    const/4 v0, 0x0

    .line 1916
    :goto_0
    return-object v0

    .line 1909
    :cond_0
    const-string v0, "PS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1910
    const-string v0, "0"

    goto :goto_0

    .line 1911
    :cond_1
    const-string v0, "CS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1912
    const-string v0, "1"

    goto :goto_0

    .line 1913
    :cond_2
    const-string v0, "PSCS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1914
    const-string v0, "2"

    goto :goto_0

    .line 1916
    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method private ConvertssPsDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 1921
    if-nez p1, :cond_0

    .line 1922
    const/4 v0, 0x0

    .line 1931
    :goto_0
    return-object v0

    .line 1926
    :cond_0
    const-string v0, "UT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1927
    const-string v0, "0"

    goto :goto_0

    .line 1928
    :cond_1
    const-string v0, "SIP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1929
    const-string v0, "1"

    goto :goto_0

    .line 1931
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method private ConvertussdDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 1936
    if-nez p1, :cond_0

    .line 1937
    const/4 v0, 0x0

    .line 1947
    :goto_0
    return-object v0

    .line 1940
    :cond_0
    const-string v0, "CS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1941
    const-string v0, "0"

    goto :goto_0

    .line 1942
    :cond_1
    const-string v0, "PS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1943
    const-string v0, "1"

    goto :goto_0

    .line 1944
    :cond_2
    const-string v0, "PSCS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1945
    const-string v0, "2"

    goto :goto_0

    .line 1947
    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method private dataBaseUpdate()V
    .locals 5

    .prologue
    .line 1124
    const/4 v1, 0x0

    .line 1126
    .local v1, "updated":I
    const-string v2, "CscIms"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "update : parse completed, mbValidProfileXML = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mbValidProfileXML:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mbValidSettingXML ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mbValidSettingXML:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1128
    iget-boolean v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mbValidProfileXML:Z

    if-eqz v2, :cond_0

    .line 1129
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscIms;->profileDatabaseUpdate()I

    move-result v2

    add-int/2addr v1, v2

    .line 1132
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mbValidSettingXML:Z

    if-eqz v2, :cond_1

    .line 1133
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscIms;->settingDatabaseUpdate()I

    move-result v2

    add-int/2addr v1, v2

    .line 1136
    :cond_1
    const-string v2, "CscIms"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dataBaseUpdate completed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1138
    if-nez v1, :cond_2

    .line 1146
    :goto_0
    return-void

    .line 1143
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.imsservice.CSC_UPDATED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1144
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1145
    const-string v2, "CscIms"

    const-string v3, "intent sent : com.sec.imsservice.CSC_UPDATED"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private makeMccMnc(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "networkName"    # Ljava/lang/String;

    .prologue
    .line 1814
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 1818
    .local v2, "result":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mNetworkInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1820
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 1821
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mNetworkInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/sec/android/application/csc/CscIms$NetworkInfo;

    iget-object v3, v3, Lcom/samsung/sec/android/application/csc/CscIms$NetworkInfo;->networkName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1824
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mNetworkInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/sec/android/application/csc/CscIms$NetworkInfo;

    iget-object v2, v3, Lcom/samsung/sec/android/application/csc/CscIms$NetworkInfo;->mccmnc:Ljava/lang/String;

    .line 1838
    .end local v2    # "result":Ljava/lang/String;
    :cond_0
    return-object v2

    .line 1820
    .restart local v2    # "result":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private parse()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 311
    const/4 v0, 0x0

    .line 313
    .local v0, "ret":I
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscIms;->parseGeneralInfo()V

    .line 315
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscIms;->parseImsProfile()I

    move-result v0

    .line 316
    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mbValidProfileXML:Z

    .line 319
    :goto_0
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscIms;->parseImsSetting()I

    move-result v0

    .line 320
    if-nez v0, :cond_1

    iput-boolean v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mbValidSettingXML:Z

    .line 323
    :goto_1
    return v0

    .line 317
    :cond_0
    iput-boolean v1, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mbValidProfileXML:Z

    goto :goto_0

    .line 321
    :cond_1
    iput-boolean v1, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mbValidSettingXML:Z

    goto :goto_1
.end method

.method private parseGeneralInfo()V
    .locals 9

    .prologue
    .line 334
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "GeneralInfo"

    invoke-virtual {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 335
    .local v1, "node":Lorg/w3c/dom/Node;
    if-nez v1, :cond_1

    .line 366
    :cond_0
    return-void

    .line 339
    :cond_1
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "NetworkInfo"

    invoke-virtual {v6, v1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 340
    .local v3, "nodeList":Lorg/w3c/dom/NodeList;
    if-eqz v3, :cond_0

    .line 344
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    .line 346
    .local v2, "nodeLen":I
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mNetworkInfos:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 348
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 349
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscIms$NetworkInfo;

    invoke-direct {v4, p0}, Lcom/samsung/sec/android/application/csc/CscIms$NetworkInfo;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    .line 352
    .local v4, "nwkInfo":Lcom/samsung/sec/android/application/csc/CscIms$NetworkInfo;
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "MCCMNC"

    invoke-virtual {v6, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 353
    .local v5, "tempNode":Lorg/w3c/dom/Node;
    if-eqz v5, :cond_2

    .line 354
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/sec/android/application/csc/CscIms$NetworkInfo;->mccmnc:Ljava/lang/String;

    .line 357
    :cond_2
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "NetworkName"

    invoke-virtual {v6, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 358
    if-eqz v5, :cond_3

    .line 359
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/sec/android/application/csc/CscIms$NetworkInfo;->networkName:Ljava/lang/String;

    .line 364
    :cond_3
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mNetworkInfos:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private parseImsProfile()I
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 375
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "Settings.IMSprofile"

    invoke-virtual {v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 376
    .local v1, "node":Lorg/w3c/dom/Node;
    if-nez v1, :cond_1

    .line 410
    :cond_0
    :goto_0
    return v7

    .line 381
    :cond_1
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "NbProfile"

    invoke-virtual {v8, v1, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 382
    .local v5, "tempNode":Lorg/w3c/dom/Node;
    if-eqz v5, :cond_0

    .line 386
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v8, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 387
    .local v4, "str":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, v8, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->num:I

    .line 388
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v8, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    .line 391
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "Profile"

    invoke-virtual {v8, v1, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 392
    .local v3, "nodeList":Lorg/w3c/dom/NodeList;
    if-eqz v3, :cond_0

    .line 396
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    .line 398
    .local v2, "nodeLen":I
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 400
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_3

    .line 401
    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/samsung/sec/android/application/csc/CscIms;->parseProfile(Lorg/w3c/dom/Node;)Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    move-result-object v6

    .line 402
    .local v6, "tempProfile":Lcom/samsung/sec/android/application/csc/CscIms$Profile;
    if-nez v6, :cond_2

    .line 403
    const/4 v7, 0x2

    goto :goto_0

    .line 406
    :cond_2
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 400
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 410
    .end local v6    # "tempProfile":Lcom/samsung/sec/android/application/csc/CscIms$Profile;
    :cond_3
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private parseImsSetting()I
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 912
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "Settings.IMSSetting"

    invoke-virtual {v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 913
    .local v1, "node":Lorg/w3c/dom/Node;
    if-nez v1, :cond_1

    .line 945
    :cond_0
    :goto_0
    return v7

    .line 918
    :cond_1
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "NbSetting"

    invoke-virtual {v8, v1, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 919
    .local v5, "tempNode":Lorg/w3c/dom/Node;
    if-eqz v5, :cond_0

    .line 922
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v8, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 923
    .local v4, "str":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, v8, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->num:I

    .line 924
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v8, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    .line 927
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "Setting"

    invoke-virtual {v8, v1, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 928
    .local v3, "nodeList":Lorg/w3c/dom/NodeList;
    if-eqz v3, :cond_0

    .line 932
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    .line 934
    .local v2, "nodeLen":I
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 936
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_3

    .line 937
    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/samsung/sec/android/application/csc/CscIms;->parseSetting(Lorg/w3c/dom/Node;)Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    move-result-object v6

    .line 938
    .local v6, "tempSetting":Lcom/samsung/sec/android/application/csc/CscIms$Setting;
    if-nez v6, :cond_2

    .line 939
    const/4 v7, 0x2

    goto :goto_0

    .line 942
    :cond_2
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 936
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 945
    .end local v6    # "tempSetting":Lcom/samsung/sec/android/application/csc/CscIms$Setting;
    :cond_3
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private parsePdnProfile(Lorg/w3c/dom/Node;Lcom/samsung/sec/android/application/csc/CscIms$Profile;)V
    .locals 5
    .param p1, "node"    # Lorg/w3c/dom/Node;
    .param p2, "profile"    # Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    .prologue
    .line 551
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "ProfileIMS"

    invoke-virtual {v3, p1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 554
    .local v1, "nodePndProfile":Lorg/w3c/dom/Node;
    if-eqz v1, :cond_10

    .line 556
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    invoke-direct {v2, p0}, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    .line 558
    .local v2, "pdnProfile":Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;
    const-string v3, "IMS"

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pdnType:Ljava/lang/String;

    .line 559
    const-string v3, "ims"

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pdn:Ljava/lang/String;

    .line 561
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Enable"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 562
    .local v0, "nodeChild":Lorg/w3c/dom/Node;
    if-eqz v0, :cond_0

    .line 563
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->enabled:Ljava/lang/String;

    .line 566
    :cond_0
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "pcscf"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 567
    if-eqz v0, :cond_1

    .line 568
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pcscfAddr:Ljava/lang/String;

    .line 571
    :cond_1
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "port"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 572
    if-eqz v0, :cond_2

    .line 573
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->port:Ljava/lang/String;

    .line 576
    :cond_2
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "pcscf_pref"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 577
    if-eqz v0, :cond_3

    .line 578
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pcscfPref:Ljava/lang/String;

    .line 581
    :cond_3
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "ipver"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 582
    if-eqz v0, :cond_4

    .line 583
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->ipver:Ljava/lang/String;

    .line 586
    :cond_4
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "regi_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 587
    if-eqz v0, :cond_5

    .line 588
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->regiAlgo:Ljava/lang/String;

    .line 591
    :cond_5
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "auth_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 592
    if-eqz v0, :cond_6

    .line 593
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->authAlgo:Ljava/lang/String;

    .line 596
    :cond_6
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "enc_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 597
    if-eqz v0, :cond_7

    .line 598
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->encAlgo:Ljava/lang/String;

    .line 601
    :cond_7
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "transport"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 602
    if-eqz v0, :cond_8

    .line 603
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->transport:Ljava/lang/String;

    .line 606
    :cond_8
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "priority"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 607
    if-eqz v0, :cond_9

    .line 608
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->priority:Ljava/lang/String;

    .line 611
    :cond_9
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "support_ipsec"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 612
    if-eqz v0, :cond_a

    .line 613
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->ipsec:Ljava/lang/String;

    .line 616
    :cond_a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "services2G"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 617
    if-eqz v0, :cond_b

    .line 618
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services2G:Ljava/lang/String;

    .line 621
    :cond_b
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "services3G"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 622
    if-eqz v0, :cond_c

    .line 623
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services3G:Ljava/lang/String;

    .line 626
    :cond_c
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "servicesHSPA"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 627
    if-eqz v0, :cond_d

    .line 628
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->servicesHSPA:Ljava/lang/String;

    .line 631
    :cond_d
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "services4G"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 632
    if-eqz v0, :cond_e

    .line 633
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services4G:Ljava/lang/String;

    .line 636
    :cond_e
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "servicesWIFI"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 637
    if-eqz v0, :cond_f

    .line 638
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->servicesWIFI:Ljava/lang/String;

    .line 641
    :cond_f
    iget-object v3, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 645
    .end local v0    # "nodeChild":Lorg/w3c/dom/Node;
    .end local v2    # "pdnProfile":Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;
    :cond_10
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "ProfileInternet"

    invoke-virtual {v3, p1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 648
    if-eqz v1, :cond_21

    .line 650
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    invoke-direct {v2, p0}, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    .line 652
    .restart local v2    # "pdnProfile":Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;
    const-string v3, "INTERNET"

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pdnType:Ljava/lang/String;

    .line 653
    const-string v3, "internet"

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pdn:Ljava/lang/String;

    .line 655
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Enable"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 656
    .restart local v0    # "nodeChild":Lorg/w3c/dom/Node;
    if-eqz v0, :cond_11

    .line 657
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->enabled:Ljava/lang/String;

    .line 660
    :cond_11
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "pcscf"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 661
    if-eqz v0, :cond_12

    .line 662
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pcscfAddr:Ljava/lang/String;

    .line 665
    :cond_12
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "port"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 666
    if-eqz v0, :cond_13

    .line 667
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->port:Ljava/lang/String;

    .line 670
    :cond_13
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "pcscf_pref"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 671
    if-eqz v0, :cond_14

    .line 672
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pcscfPref:Ljava/lang/String;

    .line 675
    :cond_14
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "ipver"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 676
    if-eqz v0, :cond_15

    .line 677
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->ipver:Ljava/lang/String;

    .line 680
    :cond_15
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "regi_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 681
    if-eqz v0, :cond_16

    .line 682
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->regiAlgo:Ljava/lang/String;

    .line 685
    :cond_16
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "auth_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 686
    if-eqz v0, :cond_17

    .line 687
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->authAlgo:Ljava/lang/String;

    .line 690
    :cond_17
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "enc_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 691
    if-eqz v0, :cond_18

    .line 692
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->encAlgo:Ljava/lang/String;

    .line 695
    :cond_18
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "transport"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 696
    if-eqz v0, :cond_19

    .line 697
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->transport:Ljava/lang/String;

    .line 700
    :cond_19
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "priority"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 701
    if-eqz v0, :cond_1a

    .line 702
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->priority:Ljava/lang/String;

    .line 705
    :cond_1a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "support_ipsec"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 706
    if-eqz v0, :cond_1b

    .line 707
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->ipsec:Ljava/lang/String;

    .line 710
    :cond_1b
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "services2G"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 711
    if-eqz v0, :cond_1c

    .line 712
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services2G:Ljava/lang/String;

    .line 715
    :cond_1c
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "services3G"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 716
    if-eqz v0, :cond_1d

    .line 717
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services3G:Ljava/lang/String;

    .line 720
    :cond_1d
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "servicesHSPA"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 721
    if-eqz v0, :cond_1e

    .line 722
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->servicesHSPA:Ljava/lang/String;

    .line 725
    :cond_1e
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "services4G"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 726
    if-eqz v0, :cond_1f

    .line 727
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services4G:Ljava/lang/String;

    .line 730
    :cond_1f
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "servicesWIFI"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 731
    if-eqz v0, :cond_20

    .line 732
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->servicesWIFI:Ljava/lang/String;

    .line 735
    :cond_20
    iget-object v3, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 739
    .end local v0    # "nodeChild":Lorg/w3c/dom/Node;
    .end local v2    # "pdnProfile":Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;
    :cond_21
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "ProfileWIFI"

    invoke-virtual {v3, p1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 742
    if-eqz v1, :cond_2e

    .line 744
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    invoke-direct {v2, p0}, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    .line 746
    .restart local v2    # "pdnProfile":Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;
    const-string v3, "WIFI"

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pdnType:Ljava/lang/String;

    .line 747
    const-string v3, "wifi"

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pdn:Ljava/lang/String;

    .line 749
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Enable"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 750
    .restart local v0    # "nodeChild":Lorg/w3c/dom/Node;
    if-eqz v0, :cond_22

    .line 751
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->enabled:Ljava/lang/String;

    .line 754
    :cond_22
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "pcscf"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 755
    if-eqz v0, :cond_23

    .line 756
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pcscfAddr:Ljava/lang/String;

    .line 759
    :cond_23
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "port"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 760
    if-eqz v0, :cond_24

    .line 761
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->port:Ljava/lang/String;

    .line 764
    :cond_24
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "pcscf_pref"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 765
    if-eqz v0, :cond_25

    .line 766
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pcscfPref:Ljava/lang/String;

    .line 769
    :cond_25
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "ipver"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 770
    if-eqz v0, :cond_26

    .line 771
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->ipver:Ljava/lang/String;

    .line 773
    :cond_26
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "regi_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 774
    if-eqz v0, :cond_27

    .line 775
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->regiAlgo:Ljava/lang/String;

    .line 778
    :cond_27
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "auth_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 779
    if-eqz v0, :cond_28

    .line 780
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->authAlgo:Ljava/lang/String;

    .line 783
    :cond_28
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "enc_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 784
    if-eqz v0, :cond_29

    .line 785
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->encAlgo:Ljava/lang/String;

    .line 788
    :cond_29
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "transport"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 789
    if-eqz v0, :cond_2a

    .line 790
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->transport:Ljava/lang/String;

    .line 793
    :cond_2a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "priority"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 794
    if-eqz v0, :cond_2b

    .line 795
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->priority:Ljava/lang/String;

    .line 798
    :cond_2b
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "support_ipsec"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 799
    if-eqz v0, :cond_2c

    .line 800
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->ipsec:Ljava/lang/String;

    .line 803
    :cond_2c
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "servicesWIFI"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 804
    if-eqz v0, :cond_2d

    .line 805
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->servicesWIFI:Ljava/lang/String;

    .line 808
    :cond_2d
    iget-object v3, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 812
    .end local v0    # "nodeChild":Lorg/w3c/dom/Node;
    .end local v2    # "pdnProfile":Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;
    :cond_2e
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "ProfileEmergency"

    invoke-virtual {v3, p1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 815
    if-eqz v1, :cond_3f

    .line 817
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    invoke-direct {v2, p0}, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    .line 819
    .restart local v2    # "pdnProfile":Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;
    const-string v3, "EMERGENCY"

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pdnType:Ljava/lang/String;

    .line 820
    const-string v3, "emergency"

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pdn:Ljava/lang/String;

    .line 822
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Enable"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 823
    .restart local v0    # "nodeChild":Lorg/w3c/dom/Node;
    if-eqz v0, :cond_2f

    .line 824
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->enabled:Ljava/lang/String;

    .line 827
    :cond_2f
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "pcscf"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 828
    if-eqz v0, :cond_30

    .line 829
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pcscfAddr:Ljava/lang/String;

    .line 832
    :cond_30
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "port"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 833
    if-eqz v0, :cond_31

    .line 834
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->port:Ljava/lang/String;

    .line 837
    :cond_31
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "pcscf_pref"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 838
    if-eqz v0, :cond_32

    .line 839
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pcscfPref:Ljava/lang/String;

    .line 842
    :cond_32
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "ipver"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 843
    if-eqz v0, :cond_33

    .line 844
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->ipver:Ljava/lang/String;

    .line 846
    :cond_33
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "regi_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 847
    if-eqz v0, :cond_34

    .line 848
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->regiAlgo:Ljava/lang/String;

    .line 851
    :cond_34
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "auth_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 852
    if-eqz v0, :cond_35

    .line 853
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->authAlgo:Ljava/lang/String;

    .line 856
    :cond_35
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "enc_algo"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 857
    if-eqz v0, :cond_36

    .line 858
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->encAlgo:Ljava/lang/String;

    .line 861
    :cond_36
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "transport"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 862
    if-eqz v0, :cond_37

    .line 863
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->transport:Ljava/lang/String;

    .line 866
    :cond_37
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "priority"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 867
    if-eqz v0, :cond_38

    .line 868
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->priority:Ljava/lang/String;

    .line 871
    :cond_38
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "support_ipsec"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 872
    if-eqz v0, :cond_39

    .line 873
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->ipsec:Ljava/lang/String;

    .line 876
    :cond_39
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "services2G"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 877
    if-eqz v0, :cond_3a

    .line 878
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services2G:Ljava/lang/String;

    .line 881
    :cond_3a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "services3G"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 882
    if-eqz v0, :cond_3b

    .line 883
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services3G:Ljava/lang/String;

    .line 886
    :cond_3b
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "servicesHSPA"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 887
    if-eqz v0, :cond_3c

    .line 888
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->servicesHSPA:Ljava/lang/String;

    .line 891
    :cond_3c
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "services4G"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 892
    if-eqz v0, :cond_3d

    .line 893
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services4G:Ljava/lang/String;

    .line 896
    :cond_3d
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "servicesWIFI"

    invoke-virtual {v3, v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 897
    if-eqz v0, :cond_3e

    .line 898
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->servicesWIFI:Ljava/lang/String;

    .line 901
    :cond_3e
    iget-object v3, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 903
    .end local v0    # "nodeChild":Lorg/w3c/dom/Node;
    .end local v2    # "pdnProfile":Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;
    :cond_3f
    return-void
.end method

.method private parseProfile(Lorg/w3c/dom/Node;)Lcom/samsung/sec/android/application/csc/CscIms$Profile;
    .locals 8
    .param p1, "node"    # Lorg/w3c/dom/Node;

    .prologue
    const/4 v3, 0x0

    .line 416
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscIms$Profile;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    .line 417
    .local v0, "profile":Lcom/samsung/sec/android/application/csc/CscIms$Profile;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v0, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    .line 420
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "NetworkName"

    invoke-virtual {v4, p1, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 421
    .local v1, "tempNode":Lorg/w3c/dom/Node;
    if-eqz v1, :cond_0

    .line 422
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v4, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->networkName:Ljava/lang/String;

    .line 425
    :cond_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "Version"

    invoke-virtual {v4, p1, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 426
    if-eqz v1, :cond_1

    .line 427
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v4, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->version:Ljava/lang/String;

    .line 429
    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->version:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 430
    .local v2, "version":F
    const-string v4, "CscIms"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "parseImsProfile version = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    float-to-double v4, v2

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_2

    move-object v0, v3

    .line 444
    .end local v0    # "profile":Lcom/samsung/sec/android/application/csc/CscIms$Profile;
    .end local v2    # "version":F
    :goto_0
    return-object v0

    .line 436
    .restart local v0    # "profile":Lcom/samsung/sec/android/application/csc/CscIms$Profile;
    :cond_1
    const-string v4, "CscIms"

    const-string v5, "parseImsProfile : version : none"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 437
    goto :goto_0

    .line 440
    .restart local v2    # "version":F
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/samsung/sec/android/application/csc/CscIms;->parseProfileCommon(Lorg/w3c/dom/Node;Lcom/samsung/sec/android/application/csc/CscIms$Profile;)V

    .line 442
    invoke-direct {p0, p1, v0}, Lcom/samsung/sec/android/application/csc/CscIms;->parsePdnProfile(Lorg/w3c/dom/Node;Lcom/samsung/sec/android/application/csc/CscIms$Profile;)V

    goto :goto_0
.end method

.method private parseProfileCommon(Lorg/w3c/dom/Node;Lcom/samsung/sec/android/application/csc/CscIms$Profile;)V
    .locals 5
    .param p1, "node"    # Lorg/w3c/dom/Node;
    .param p2, "profile"    # Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    .prologue
    const/4 v4, 0x0

    .line 451
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;

    invoke-direct {v2, p0}, Lcom/samsung/sec/android/application/csc/CscIms$Common;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    iput-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    .line 454
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Common"

    invoke-virtual {v2, p1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 457
    .local v1, "nodeCommon":Lorg/w3c/dom/Node;
    if-eqz v1, :cond_d

    .line 458
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timer1"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 459
    .local v0, "nodeChild":Lorg/w3c/dom/Node;
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->t1:I

    .line 460
    if-eqz v0, :cond_0

    .line 461
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->t1:I

    .line 464
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timer2"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 465
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->t2:I

    .line 466
    if-eqz v0, :cond_1

    .line 467
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->t2:I

    .line 470
    :cond_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timer4"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 471
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->t4:I

    .line 472
    if-eqz v0, :cond_2

    .line 473
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->t4:I

    .line 476
    :cond_2
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timerA"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 477
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tA:I

    .line 478
    if-eqz v0, :cond_3

    .line 479
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tA:I

    .line 482
    :cond_3
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timerB"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 483
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tB:I

    .line 484
    if-eqz v0, :cond_4

    .line 485
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tB:I

    .line 488
    :cond_4
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timerC"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 489
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tC:I

    .line 490
    if-eqz v0, :cond_5

    .line 491
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tC:I

    .line 494
    :cond_5
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timerD"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 495
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tD:I

    .line 496
    if-eqz v0, :cond_6

    .line 497
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tD:I

    .line 500
    :cond_6
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timerE"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 501
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tE:I

    .line 502
    if-eqz v0, :cond_7

    .line 503
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tE:I

    .line 506
    :cond_7
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timerF"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 507
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tF:I

    .line 508
    if-eqz v0, :cond_8

    .line 509
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tF:I

    .line 512
    :cond_8
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timerG"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 513
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tG:I

    .line 514
    if-eqz v0, :cond_9

    .line 515
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tG:I

    .line 518
    :cond_9
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timerH"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 519
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tH:I

    .line 520
    if-eqz v0, :cond_a

    .line 521
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tH:I

    .line 524
    :cond_a
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timerI"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 525
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tI:I

    .line 526
    if-eqz v0, :cond_b

    .line 527
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tI:I

    .line 530
    :cond_b
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timerJ"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 531
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tJ:I

    .line 532
    if-eqz v0, :cond_c

    .line 533
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tJ:I

    .line 536
    :cond_c
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "timerK"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 537
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iput v4, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tK:I

    .line 538
    if-eqz v0, :cond_d

    .line 539
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tK:I

    .line 542
    .end local v0    # "nodeChild":Lorg/w3c/dom/Node;
    :cond_d
    return-void
.end method

.method private parseRCS(Lorg/w3c/dom/Node;Lcom/samsung/sec/android/application/csc/CscIms$Setting;)V
    .locals 4
    .param p1, "node"    # Lorg/w3c/dom/Node;
    .param p2, "setting"    # Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    .prologue
    .line 1073
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "RCS"

    invoke-virtual {v2, p1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 1076
    .local v1, "nodeRCS":Lorg/w3c/dom/Node;
    if-eqz v1, :cond_7

    .line 1078
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscIms$RCS;

    invoke-direct {v2, p0}, Lcom/samsung/sec/android/application/csc/CscIms$RCS;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    iput-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->rcs:Lcom/samsung/sec/android/application/csc/CscIms$RCS;

    .line 1080
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "EnableRCS"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1081
    .local v0, "nodeChild":Lorg/w3c/dom/Node;
    if-eqz v0, :cond_0

    .line 1082
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->rcs:Lcom/samsung/sec/android/application/csc/CscIms$RCS;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$RCS;->enable:Ljava/lang/String;

    .line 1085
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "EnableIM"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1086
    if-eqz v0, :cond_1

    .line 1087
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->rcs:Lcom/samsung/sec/android/application/csc/CscIms$RCS;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$RCS;->enableIM:Ljava/lang/String;

    .line 1090
    :cond_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "EnableFT"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1091
    if-eqz v0, :cond_2

    .line 1092
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->rcs:Lcom/samsung/sec/android/application/csc/CscIms$RCS;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$RCS;->enableFT:Ljava/lang/String;

    .line 1095
    :cond_2
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "EnableIS"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1096
    if-eqz v0, :cond_3

    .line 1097
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->rcs:Lcom/samsung/sec/android/application/csc/CscIms$RCS;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$RCS;->enableIS:Ljava/lang/String;

    .line 1100
    :cond_3
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "EnableVS"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1101
    if-eqz v0, :cond_4

    .line 1102
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->rcs:Lcom/samsung/sec/android/application/csc/CscIms$RCS;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$RCS;->enableVS:Ljava/lang/String;

    .line 1105
    :cond_4
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "EnablePresence"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1106
    if-eqz v0, :cond_5

    .line 1107
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->rcs:Lcom/samsung/sec/android/application/csc/CscIms$RCS;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$RCS;->enablePresence:Ljava/lang/String;

    .line 1110
    :cond_5
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "EnableSocialPresence"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1111
    if-eqz v0, :cond_6

    .line 1112
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->rcs:Lcom/samsung/sec/android/application/csc/CscIms$RCS;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$RCS;->enableSocialPresence:Ljava/lang/String;

    .line 1115
    :cond_6
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "EnableOPTIONS"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1116
    if-eqz v0, :cond_7

    .line 1117
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->rcs:Lcom/samsung/sec/android/application/csc/CscIms$RCS;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$RCS;->enableOPTIONS:Ljava/lang/String;

    .line 1120
    .end local v0    # "nodeChild":Lorg/w3c/dom/Node;
    :cond_7
    return-void
.end method

.method private parseSetting(Lorg/w3c/dom/Node;)Lcom/samsung/sec/android/application/csc/CscIms$Setting;
    .locals 8
    .param p1, "node"    # Lorg/w3c/dom/Node;

    .prologue
    const/4 v3, 0x0

    .line 952
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscIms$Setting;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    .line 954
    .local v0, "setting":Lcom/samsung/sec/android/application/csc/CscIms$Setting;
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "NetworkName"

    invoke-virtual {v4, p1, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 955
    .local v1, "tempNode":Lorg/w3c/dom/Node;
    if-eqz v1, :cond_0

    .line 956
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v4, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->networkName:Ljava/lang/String;

    .line 959
    :cond_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "Version"

    invoke-virtual {v4, p1, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 960
    if-eqz v1, :cond_1

    .line 961
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v4, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->version:Ljava/lang/String;

    .line 962
    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->version:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 963
    .local v2, "version":F
    const-string v4, "CscIms"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "parseImsSetting version = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 964
    float-to-double v4, v2

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_2

    move-object v0, v3

    .line 997
    .end local v0    # "setting":Lcom/samsung/sec/android/application/csc/CscIms$Setting;
    .end local v2    # "version":F
    :goto_0
    return-object v0

    .line 969
    .restart local v0    # "setting":Lcom/samsung/sec/android/application/csc/CscIms$Setting;
    :cond_1
    const-string v4, "CscIms"

    const-string v5, "parseImsSetting version = none"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 970
    goto :goto_0

    .line 974
    .restart local v2    # "version":F
    :cond_2
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "EnableIMS"

    invoke-virtual {v3, p1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 975
    if-eqz v1, :cond_3

    .line 976
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->enableIMS:Ljava/lang/String;

    .line 979
    :cond_3
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "EmergencyPDNtype"

    invoke-virtual {v3, p1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 980
    if-eqz v1, :cond_4

    .line 981
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->emergencyPDNtype:Ljava/lang/String;

    .line 984
    :cond_4
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "EnableEPDG"

    invoke-virtual {v3, p1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 985
    if-eqz v1, :cond_5

    .line 986
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->enableePDG:Ljava/lang/String;

    .line 989
    :cond_5
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "EnableEHRPD"

    invoke-virtual {v3, p1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 990
    if-eqz v1, :cond_6

    .line 991
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->enableeHRPD:Ljava/lang/String;

    .line 994
    :cond_6
    invoke-direct {p0, p1, v0}, Lcom/samsung/sec/android/application/csc/CscIms;->parseVolte(Lorg/w3c/dom/Node;Lcom/samsung/sec/android/application/csc/CscIms$Setting;)V

    .line 995
    invoke-direct {p0, p1, v0}, Lcom/samsung/sec/android/application/csc/CscIms;->parseRCS(Lorg/w3c/dom/Node;Lcom/samsung/sec/android/application/csc/CscIms$Setting;)V

    goto :goto_0
.end method

.method private parseVolte(Lorg/w3c/dom/Node;Lcom/samsung/sec/android/application/csc/CscIms$Setting;)V
    .locals 4
    .param p1, "node"    # Lorg/w3c/dom/Node;
    .param p2, "setting"    # Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    .prologue
    .line 1005
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "VoLTE"

    invoke-virtual {v2, p1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 1008
    .local v1, "nodeVolte":Lorg/w3c/dom/Node;
    if-eqz v1, :cond_a

    .line 1010
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    invoke-direct {v2, p0}, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;-><init>(Lcom/samsung/sec/android/application/csc/CscIms;)V

    iput-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    .line 1012
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "EnableVoLTE"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1013
    .local v0, "nodeChild":Lorg/w3c/dom/Node;
    if-eqz v0, :cond_0

    .line 1014
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->enable:Ljava/lang/String;

    .line 1017
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "EnableVoLTEIndicator"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1018
    if-eqz v0, :cond_1

    .line 1019
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->enableVoLTEindicator:Ljava/lang/String;

    .line 1022
    :cond_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Voice_Domain_Preference_EUTRAN"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1023
    if-eqz v0, :cond_2

    .line 1024
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->voiceDomainPrefEUTRAN:Ljava/lang/String;

    .line 1027
    :cond_2
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Voice_Domain_Preference_UTRAN"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1028
    if-eqz v0, :cond_3

    .line 1029
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->voiceDomainPrefUTRAN:Ljava/lang/String;

    .line 1032
    :cond_3
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Emregencycall_Domain"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1033
    if-eqz v0, :cond_4

    .line 1034
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->emergencycallDomain:Ljava/lang/String;

    .line 1037
    :cond_4
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "SMSoIP_Domain"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1038
    if-eqz v0, :cond_5

    .line 1039
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->SMSoIPDomain:Ljava/lang/String;

    .line 1042
    :cond_5
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "SMSoIP_Format"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1043
    if-eqz v0, :cond_6

    .line 1044
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->SMSoIPFormat:Ljava/lang/String;

    .line 1047
    :cond_6
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "SS_Domain_Preference"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1048
    if-eqz v0, :cond_7

    .line 1049
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->ssDomainPref:Ljava/lang/String;

    .line 1052
    :cond_7
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "SS_PS_Control_Preference"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1053
    if-eqz v0, :cond_8

    .line 1054
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->ssPsCtrlPref:Ljava/lang/String;

    .line 1057
    :cond_8
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "SS_CSFBwithIMSerror"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1058
    if-eqz v0, :cond_9

    .line 1059
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->ssCSFBwithIMSerror:Ljava/lang/String;

    .line 1062
    :cond_9
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "USSD_Domain_Preference"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1063
    if-eqz v0, :cond_a

    .line 1064
    iget-object v2, p2, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v3, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->ussdDomainPref:Ljava/lang/String;

    .line 1067
    .end local v0    # "nodeChild":Lorg/w3c/dom/Node;
    :cond_a
    return-void
.end method

.method private profileDatabaseUpdate()I
    .locals 18

    .prologue
    .line 1149
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1151
    .local v5, "bulk":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const-string v15, "content://com.sec.ims.settings/cscprofile"

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 1154
    .local v13, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 1162
    .local v6, "count":I
    const-string v3, "network_name"

    .line 1163
    .local v3, "NETWORK_NAME":Ljava/lang/String;
    const-string v2, "mccmnc"

    .line 1164
    .local v2, "MCCMNC":Ljava/lang/String;
    const-string v1, "field_name"

    .line 1165
    .local v1, "FIELD_NAME":Ljava/lang/String;
    const-string v4, "value"

    .line 1167
    .local v4, "VALUE":Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->num:I

    if-ge v8, v15, :cond_12

    .line 1168
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-ge v9, v15, :cond_11

    .line 1170
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v11, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->networkName:Ljava/lang/String;

    .line 1171
    .local v11, "networkName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->networkName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/sec/android/application/csc/CscIms;->makeMccMnc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1176
    .local v10, "mccmnc":Ljava/lang/String;
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1177
    .local v14, "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1179
    const-string v15, "field_name"

    const-string v16, "sip_timer_1"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->t1:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1181
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1182
    add-int/lit8 v6, v6, 0x1

    .line 1184
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1185
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1187
    const-string v15, "field_name"

    const-string v16, "sip_timer_2"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1188
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->t2:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1189
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1190
    add-int/lit8 v6, v6, 0x1

    .line 1192
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1193
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1195
    const-string v15, "field_name"

    const-string v16, "sip_timer_4"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->t4:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1197
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1198
    add-int/lit8 v6, v6, 0x1

    .line 1200
    const-string v16, "CscIms"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "profileDatabaseUpdate : tA = "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tA:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1202
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tA:I

    if-eqz v15, :cond_0

    .line 1204
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1205
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207
    const-string v15, "field_name"

    const-string v16, "sip_timer_a"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1208
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tA:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1209
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1210
    add-int/lit8 v6, v6, 0x1

    .line 1212
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1213
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    const-string v15, "field_name"

    const-string v16, "sip_timer_b"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tB:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1217
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1218
    add-int/lit8 v6, v6, 0x1

    .line 1220
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1221
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1223
    const-string v15, "field_name"

    const-string v16, "sip_timer_c"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tC:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1225
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1226
    add-int/lit8 v6, v6, 0x1

    .line 1228
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1229
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1230
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1231
    const-string v15, "field_name"

    const-string v16, "sip_timer_d"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tD:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1233
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1234
    add-int/lit8 v6, v6, 0x1

    .line 1236
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1237
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1239
    const-string v15, "field_name"

    const-string v16, "sip_timer_e"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tE:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1241
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1242
    add-int/lit8 v6, v6, 0x1

    .line 1244
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1245
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1246
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1247
    const-string v15, "field_name"

    const-string v16, "sip_timer_f"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1248
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tF:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1249
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1250
    add-int/lit8 v6, v6, 0x1

    .line 1252
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1253
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1254
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1255
    const-string v15, "field_name"

    const-string v16, "sip_timer_g"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1256
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tG:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1257
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1258
    add-int/lit8 v6, v6, 0x1

    .line 1260
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1261
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1263
    const-string v15, "field_name"

    const-string v16, "sip_timer_h"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1264
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tH:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1265
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1266
    add-int/lit8 v6, v6, 0x1

    .line 1268
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1269
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1270
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    const-string v15, "field_name"

    const-string v16, "sip_timer_i"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1272
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tI:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1273
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1274
    add-int/lit8 v6, v6, 0x1

    .line 1276
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1277
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1278
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279
    const-string v15, "field_name"

    const-string v16, "sip_timer_j"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tJ:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1281
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1282
    add-int/lit8 v6, v6, 0x1

    .line 1284
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1285
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1287
    const-string v15, "field_name"

    const-string v16, "sip_timer_k"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1288
    const-string v16, "value"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->common:Lcom/samsung/sec/android/application/csc/CscIms$Common;

    iget v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Common;->tK:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1289
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1290
    add-int/lit8 v6, v6, 0x1

    .line 1297
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->enabled:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertTRUEFALSE(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1298
    .local v12, "strvalue":Ljava/lang/String;
    if-eqz v12, :cond_1

    .line 1299
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1300
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1301
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1302
    const-string v15, "field_name"

    const-string v16, "enabled"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1303
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1304
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1305
    add-int/lit8 v6, v6, 0x1

    .line 1308
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pcscfAddr:Ljava/lang/String;

    .line 1309
    if-eqz v12, :cond_2

    .line 1310
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1311
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1313
    const-string v15, "field_name"

    const-string v16, "pcscf"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1315
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1316
    add-int/lit8 v6, v6, 0x1

    .line 1319
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->port:Ljava/lang/String;

    .line 1320
    if-eqz v12, :cond_3

    .line 1321
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1322
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1323
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1324
    const-string v15, "field_name"

    const-string v16, "sip_port"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1325
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1326
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1327
    add-int/lit8 v6, v6, 0x1

    .line 1330
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->pcscfPref:Ljava/lang/String;

    .line 1331
    if-eqz v12, :cond_4

    .line 1332
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1333
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1334
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    const-string v15, "field_name"

    const-string v16, "pcscf_pref"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1336
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1337
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1338
    add-int/lit8 v6, v6, 0x1

    .line 1341
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->ipver:Ljava/lang/String;

    .line 1342
    if-eqz v12, :cond_5

    .line 1343
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1344
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1345
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1346
    const-string v15, "field_name"

    const-string v16, "ipver"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1347
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1348
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1349
    add-int/lit8 v6, v6, 0x1

    .line 1352
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->regiAlgo:Ljava/lang/String;

    .line 1353
    if-eqz v12, :cond_6

    .line 1354
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1355
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1356
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1357
    const-string v15, "field_name"

    const-string v16, "regi_algo"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1358
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1359
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1360
    add-int/lit8 v6, v6, 0x1

    .line 1363
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->authAlgo:Ljava/lang/String;

    .line 1364
    if-eqz v12, :cond_7

    .line 1365
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1366
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1367
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1368
    const-string v15, "field_name"

    const-string v16, "auth_algo"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1369
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1370
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1371
    add-int/lit8 v6, v6, 0x1

    .line 1374
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->encAlgo:Ljava/lang/String;

    .line 1375
    if-eqz v12, :cond_8

    .line 1376
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1377
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1378
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1379
    const-string v15, "field_name"

    const-string v16, "enc_algo"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1380
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1381
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1382
    add-int/lit8 v6, v6, 0x1

    .line 1385
    :cond_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->transport:Ljava/lang/String;

    .line 1386
    if-eqz v12, :cond_9

    .line 1387
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1388
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1389
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1390
    const-string v15, "field_name"

    const-string v16, "transport"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1391
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1392
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1393
    add-int/lit8 v6, v6, 0x1

    .line 1396
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->priority:Ljava/lang/String;

    .line 1397
    if-eqz v12, :cond_a

    .line 1398
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1399
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1400
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401
    const-string v15, "field_name"

    const-string v16, "priority"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1402
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1403
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1404
    add-int/lit8 v6, v6, 0x1

    .line 1407
    :cond_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->ipsec:Ljava/lang/String;

    .line 1408
    if-eqz v12, :cond_b

    .line 1409
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1410
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1411
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1412
    const-string v15, "field_name"

    const-string v16, "ipsec"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1414
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1415
    add-int/lit8 v6, v6, 0x1

    .line 1418
    :cond_b
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services2G:Ljava/lang/String;

    .line 1419
    if-eqz v12, :cond_c

    .line 1420
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1421
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1422
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    const-string v15, "field_name"

    const-string v16, "services2G"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1426
    add-int/lit8 v6, v6, 0x1

    .line 1429
    :cond_c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services3G:Ljava/lang/String;

    .line 1430
    if-eqz v12, :cond_d

    .line 1431
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1432
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    const-string v15, "field_name"

    const-string v16, "services3G"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437
    add-int/lit8 v6, v6, 0x1

    .line 1440
    :cond_d
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->services4G:Ljava/lang/String;

    .line 1441
    if-eqz v12, :cond_e

    .line 1442
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1443
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1444
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1445
    const-string v15, "field_name"

    const-string v16, "services4G"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1447
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1448
    add-int/lit8 v6, v6, 0x1

    .line 1451
    :cond_e
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->servicesHSPA:Ljava/lang/String;

    .line 1452
    if-eqz v12, :cond_f

    .line 1453
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1454
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1455
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1456
    const-string v15, "field_name"

    const-string v16, "servicesHSPA"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1458
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1459
    add-int/lit8 v6, v6, 0x1

    .line 1462
    :cond_f
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsProfile:Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$ImsProfile;->profiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;

    iget-object v15, v15, Lcom/samsung/sec/android/application/csc/CscIms$Profile;->pdnProfiles:Ljava/util/ArrayList;

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;

    iget-object v12, v15, Lcom/samsung/sec/android/application/csc/CscIms$PdnProfile;->servicesWIFI:Ljava/lang/String;

    .line 1463
    if-eqz v12, :cond_10

    .line 1464
    new-instance v14, Landroid/content/ContentValues;

    .end local v14    # "values":Landroid/content/ContentValues;
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1465
    .restart local v14    # "values":Landroid/content/ContentValues;
    const-string v15, "network_name"

    invoke-virtual {v14, v15, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1466
    const-string v15, "mccmnc"

    invoke-virtual {v14, v15, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1467
    const-string v15, "field_name"

    const-string v16, "servicesWIFI"

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1468
    const-string v15, "value"

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1469
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1470
    add-int/lit8 v6, v6, 0x1

    .line 1168
    :cond_10
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 1167
    .end local v10    # "mccmnc":Ljava/lang/String;
    .end local v11    # "networkName":Ljava/lang/String;
    .end local v12    # "strvalue":Ljava/lang/String;
    .end local v14    # "values":Landroid/content/ContentValues;
    :cond_11
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 1475
    .end local v9    # "j":I
    :cond_12
    if-nez v6, :cond_13

    .line 1476
    const/4 v6, 0x0

    .line 1487
    .end local v6    # "count":I
    :goto_2
    return v6

    .line 1479
    .restart local v6    # "count":I
    :cond_13
    const-string v15, "CscIms"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "profileDatabaseUpdate : count = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1482
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v16, v0

    new-array v15, v6, [Landroid/content/ContentValues;

    invoke-virtual {v5, v15}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Landroid/content/ContentValues;

    move-object/from16 v0, v16

    invoke-virtual {v0, v13, v15}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1483
    :catch_0
    move-exception v7

    .line 1484
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v15, "CscIms"

    const-string v16, "profile SQL insert error!"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private settingDatabaseUpdate()I
    .locals 17

    .prologue
    .line 1491
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1493
    .local v5, "bulk":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const-string v14, "content://com.sec.ims.settings/cscsetting"

    invoke-static {v14}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 1496
    .local v12, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 1504
    .local v6, "count":I
    const-string v3, "network_name"

    .line 1505
    .local v3, "NETWORK_NAME":Ljava/lang/String;
    const-string v2, "mccmnc"

    .line 1506
    .local v2, "MCCMNC":Ljava/lang/String;
    const-string v1, "field_name"

    .line 1507
    .local v1, "FIELD_NAME":Ljava/lang/String;
    const-string v4, "value"

    .line 1509
    .local v4, "VALUE":Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->num:I

    if-ge v8, v14, :cond_e

    .line 1511
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v10, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->networkName:Ljava/lang/String;

    .line 1512
    .local v10, "networkName":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/sec/android/application/csc/CscIms;->makeMccMnc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1517
    .local v9, "mccmnc":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->enableIMS:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertTRUEFALSE(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1518
    .local v11, "strvalue":Ljava/lang/String;
    if-eqz v11, :cond_0

    .line 1519
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1520
    .local v13, "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1521
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522
    const-string v14, "field_name"

    const-string v15, "ims_enabled"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1523
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1524
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1525
    add-int/lit8 v6, v6, 0x1

    .line 1531
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->enableIMS:Ljava/lang/String;

    const-string v15, "FALSE"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 1509
    .end local v13    # "values":Landroid/content/ContentValues;
    :cond_0
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1550
    .restart local v13    # "values":Landroid/content/ContentValues;
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->enableePDG:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertTRUEFALSE(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1551
    if-eqz v11, :cond_2

    .line 1552
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1553
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1554
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1555
    const-string v14, "field_name"

    const-string v15, "epdg_enabled"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1556
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1557
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1558
    add-int/lit8 v6, v6, 0x1

    .line 1562
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->enableeHRPD:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertTRUEFALSE(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1563
    if-eqz v11, :cond_3

    .line 1564
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1565
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1567
    const-string v14, "field_name"

    const-string v15, "ehrpd_enabled"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1568
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1569
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1570
    add-int/lit8 v6, v6, 0x1

    .line 1576
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->enable:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertTRUEFALSE(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1577
    if-eqz v11, :cond_4

    .line 1578
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1579
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1580
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1581
    const-string v14, "field_name"

    const-string v15, "volte_enabled"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1582
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1583
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1584
    add-int/lit8 v6, v6, 0x1

    .line 1587
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->enableVoLTEindicator:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertTRUEFALSE(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1588
    if-eqz v11, :cond_5

    .line 1589
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1590
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1591
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1592
    const-string v14, "field_name"

    const-string v15, "show_volte_regi_icon"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1593
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1594
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1595
    add-int/lit8 v6, v6, 0x1

    .line 1598
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->voiceDomainPrefEUTRAN:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertVoiceDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1599
    if-eqz v11, :cond_6

    .line 1600
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1601
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1602
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1603
    const-string v14, "field_name"

    const-string v15, "voice_domain_pref_eutran"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1604
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1606
    add-int/lit8 v6, v6, 0x1

    .line 1609
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->voiceDomainPrefUTRAN:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertVoiceDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1610
    if-eqz v11, :cond_7

    .line 1611
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1612
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1613
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1614
    const-string v14, "field_name"

    const-string v15, "voice_domain_pref_utran"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1615
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1616
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1617
    add-int/lit8 v6, v6, 0x1

    .line 1620
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->emergencycallDomain:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertEmergencyDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1621
    if-eqz v11, :cond_8

    .line 1622
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1623
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1625
    const-string v14, "field_name"

    const-string v15, "emergency_domain_setting"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1627
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1628
    add-int/lit8 v6, v6, 0x1

    .line 1631
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->SMSoIPDomain:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertSMSoIPDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1632
    if-eqz v11, :cond_9

    .line 1633
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1634
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1635
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1636
    const-string v14, "field_name"

    const-string v15, "sms_over_ip_indication"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1637
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1638
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1639
    add-int/lit8 v6, v6, 0x1

    .line 1642
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v11, v14, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->SMSoIPFormat:Ljava/lang/String;

    .line 1643
    if-eqz v11, :cond_a

    .line 1644
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1645
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1647
    const-string v14, "field_name"

    const-string v15, "sms_format"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1648
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1649
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1650
    add-int/lit8 v6, v6, 0x1

    .line 1653
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->ssDomainPref:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertssDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1654
    if-eqz v11, :cond_b

    .line 1655
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1656
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1657
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1658
    const-string v14, "field_name"

    const-string v15, "ss_domain_setting"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1659
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1661
    add-int/lit8 v6, v6, 0x1

    .line 1664
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->ssPsCtrlPref:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertssPsDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1665
    if-eqz v11, :cond_c

    .line 1666
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1667
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1668
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1669
    const-string v14, "field_name"

    const-string v15, "ss_control_pref"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1670
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1671
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1672
    add-int/lit8 v6, v6, 0x1

    .line 1675
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->ssCSFBwithIMSerror:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertTRUEFALSE(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1676
    if-eqz v11, :cond_d

    .line 1678
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1679
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1680
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1681
    const-string v14, "field_name"

    const-string v15, "ss_csfb_with_imserror"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1682
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1683
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1684
    add-int/lit8 v6, v6, 0x1

    .line 1687
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mImsSetting:Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$ImsSetting;->settings:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$Setting;->volte:Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscIms$VoLTE;->ussdDomainPref:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscIms;->ConvertussdDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1688
    if-eqz v11, :cond_0

    .line 1689
    new-instance v13, Landroid/content/ContentValues;

    .end local v13    # "values":Landroid/content/ContentValues;
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1690
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v14, "network_name"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1691
    const-string v14, "mccmnc"

    invoke-virtual {v13, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1692
    const-string v14, "field_name"

    const-string v15, "ussd_domain_setting"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1693
    const-string v14, "value"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1694
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1695
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 1792
    .end local v9    # "mccmnc":Ljava/lang/String;
    .end local v10    # "networkName":Ljava/lang/String;
    .end local v11    # "strvalue":Ljava/lang/String;
    .end local v13    # "values":Landroid/content/ContentValues;
    :cond_e
    if-nez v6, :cond_f

    .line 1793
    const/4 v6, 0x0

    .line 1804
    .end local v6    # "count":I
    :goto_2
    return v6

    .line 1796
    .restart local v6    # "count":I
    :cond_f
    const-string v14, "CscIms"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "settingDatabaseUpdate : count = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1799
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscIms;->mResolver:Landroid/content/ContentResolver;

    new-array v14, v6, [Landroid/content/ContentValues;

    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [Landroid/content/ContentValues;

    invoke-virtual {v15, v12, v14}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1800
    :catch_0
    move-exception v7

    .line 1801
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v14, "CscIms"

    const-string v15, "setting SQL insert error!"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public compare()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    const-string v0, "NOERROR"

    return-object v0
.end method

.method public update()V
    .locals 2

    .prologue
    .line 289
    const-string v0, "CscIms"

    const-string v1, "update"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscIms;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 292
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscIms;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v0, :cond_0

    .line 301
    :goto_0
    return-void

    .line 297
    :cond_0
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscIms;->parse()I

    .line 300
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscIms;->dataBaseUpdate()V

    goto :goto_0
.end method
