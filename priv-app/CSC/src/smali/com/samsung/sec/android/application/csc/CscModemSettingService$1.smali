.class Lcom/samsung/sec/android/application/csc/CscModemSettingService$1;
.super Ljava/lang/Object;
.source "CscModemSettingService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/sec/android/application/csc/CscModemSettingService;->onStart(Landroid/content/Intent;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;


# direct methods
.method constructor <init>(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 178
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$000(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$000(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "MODE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 179
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$000(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "MODE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "keyString":Ljava/lang/String;
    const-string v1, "SET_LOCK_INFO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    const-string v1, "CscModemSettingService"

    const-string v2, "SET_LOCK_INFO for modem values. (PHASE 1 writing)"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # invokes: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->updateLockInfo()V
    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$100(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V

    .line 195
    :cond_0
    const-string v1, "SET_MODEM_INFO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 196
    const-string v1, "CscModemSettingService"

    const-string v2, "SET_MODEM_INFO for modem values. (PHASE 2 writing)"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # invokes: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->updateModemInfo()V
    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$200(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V

    .line 205
    :cond_1
    const-string v1, "COMPARE_VERIFY_MODEM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 206
    const-string v1, "CscModemSettingService"

    const-string v2, "COMPARE_VERIFY for modem values. "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # invokes: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->compareLockAndModemInfo()V
    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$300(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V

    .line 212
    .end local v0    # "keyString":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 210
    :cond_3
    const-string v1, "CscModemSettingService"

    const-string v2, "NO EXTRA"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
