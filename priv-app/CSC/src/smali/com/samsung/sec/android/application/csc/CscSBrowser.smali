.class public Lcom/samsung/sec/android/application/csc/CscSBrowser;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscSBrowser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;,
        Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;
    }
.end annotation


# static fields
.field static final SBROWSER_BOOKMARKS_CONTENT_URI_TABLE:Landroid/net/Uri;

.field static final SBROWSER_OPBOOKMARK_CONTENT_URI:Landroid/net/Uri;

.field static final SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

.field static final SBROWSER_QUICKACCESS_CONTENT_URI_TABLE:Landroid/net/Uri;

.field private static final SEND_PACKAGE_SBROWSER:Landroid/content/ComponentName;


# instance fields
.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final OTHERS_CSC_FILE:Ljava/lang/String;

.field private bNewSBrowser:Z

.field private mContext:Landroid/content/Context;

.field private mHomepageUrlRcvr:Landroid/content/BroadcastReceiver;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mProfileHandles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;",
            ">;"
        }
    .end annotation
.end field

.field private mProfiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;",
            ">;"
        }
    .end annotation
.end field

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 145
    const-string v0, "content://com.sec.android.app.sbrowser.operatorbookmarks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_OPBOOKMARK_CONTENT_URI:Landroid/net/Uri;

    .line 148
    const-string v0, "content://com.sec.android.app.sbrowser.quickaccesspinned/pintab"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_QUICKACCESS_CONTENT_URI_TABLE:Landroid/net/Uri;

    .line 150
    const-string v0, "content://com.sec.android.app.sbrowser/bookmarks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_BOOKMARKS_CONTENT_URI_TABLE:Landroid/net/Uri;

    .line 153
    const-string v0, "content://com.sec.android.app.sbrowser.operatorbookmarks/bookmarks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

    .line 156
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.sbrowser"

    const-string v2, "com.sec.android.app.sbrowser.SBrowserMainActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SEND_PACKAGE_SBROWSER:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 164
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 69
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 116
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->OTHERS_CSC_FILE:Ljava/lang/String;

    .line 137
    iput-object v6, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 139
    iput-object v6, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 159
    iput-boolean v5, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->bNewSBrowser:Z

    .line 165
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    .line 166
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    .line 169
    const-string v2, "com.sec.android.app.sbrowser"

    .line 171
    .local v2, "packageName":Ljava/lang/String;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 172
    .local v1, "info":Landroid/content/pm/ApplicationInfo;
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget-object v3, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 173
    .local v3, "version":Ljava/lang/String;
    if-eqz v3, :cond_0

    const-string v4, "1.5.28"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 174
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->bNewSBrowser:Z

    .line 175
    const-string v4, "CscSBrowser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscSBrowser: version - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mProfiles:Ljava/util/ArrayList;

    .line 182
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mProfileHandles:Ljava/util/ArrayList;

    .line 187
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mIntentFilter:Landroid/content/IntentFilter;

    .line 188
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.CSC_BROWSER_HOMEPAGE"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 189
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscSBrowser$1;

    invoke-direct {v4, p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser$1;-><init>(Lcom/samsung/sec/android/application/csc/CscSBrowser;)V

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mHomepageUrlRcvr:Landroid/content/BroadcastReceiver;

    .line 208
    .end local v1    # "info":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "version":Ljava/lang/String;
    :goto_0
    return-void

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "CscSBrowser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscSBrowser: packageName"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is not there"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/sec/android/application/csc/CscSBrowser;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscSBrowser;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/sec/android/application/csc/CscSBrowser;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscSBrowser;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mHomepageUrlRcvr:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method private getCurrentNetworkName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1171
    const-string v0, "none"

    .line 1173
    .local v0, "currentNwtName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    const-string v3, "simprof.preferences_name"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1175
    .local v1, "sp":Landroid/content/SharedPreferences;
    const-string v2, "simprof.key.nwkname"

    const-string v3, "none"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1177
    return-object v0
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 482
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Browser.Bookmark.Editable"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 483
    const-string v0, "Settings.Browser.Bookmark.Index"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 484
    const-string v0, "Settings.Browser.Bookmark.NetworkName"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 485
    const-string v0, "Settings.Browser.CookieOption"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 486
    const-string v0, "Settings.Browser.HistoryList"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 487
    const-string v0, "Settings.Main.Network.AutoBookmark"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 488
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 9

    .prologue
    .line 327
    const-string v6, "CscSBrowser"

    const-string v7, " ** compare runs.."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    new-instance v1, Ljava/lang/String;

    const-string v6, "NOERROR"

    invoke-direct {v1, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 330
    .local v1, "bookmark":Ljava/lang/String;
    new-instance v3, Ljava/lang/String;

    const-string v6, "NOERROR"

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 331
    .local v3, "homeurl":Ljava/lang/String;
    new-instance v0, Ljava/lang/String;

    const-string v6, "NOERROR"

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 333
    .local v0, "QuickAccess":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Web_EnableAutoSimHomeUrlInProfile"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    .line 334
    .local v4, "isAutoSimHomeUrlFeature":Z
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Web_SetHomepageURL"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 336
    .local v5, "seturl":Ljava/lang/String;
    if-eqz v5, :cond_0

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 337
    const-string v6, "Settings.Connections.URL"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CscFeatureTagWeb_SetHomepageURL: enabled, please compare homepage url manually. xml : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->compareBookmarks()Ljava/lang/String;

    move-result-object v1

    .line 359
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->compareQuickAccess()Ljava/lang/String;

    move-result-object v0

    .line 361
    return-object v1

    .line 339
    :cond_0
    if-nez v4, :cond_1

    .line 341
    const-string v6, "CscSBrowser"

    const-string v7, "CscFeature_Web_EnableAutoSimHomeUrlInProfile: disabled"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 343
    :cond_1
    const-string v6, "CscSBrowser"

    const-string v7, "CscFeature_Web_EnableAutoSimHomeUrlInProfile: enabled"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    const-string v2, "none"

    .line 345
    .local v2, "currentNtwName":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->getCurrentNetworkName()Ljava/lang/String;

    move-result-object v2

    .line 346
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 347
    const-string v6, "CscSBrowser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Current network is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    invoke-virtual {p0, v2}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->compareHomeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 350
    :cond_2
    const-string v6, "Settings.Connections.URL"

    const/4 v7, 0x0

    const-string v8, "Current network name is null. please test after checking customer & sim status & mobile network."

    invoke-static {v6, v7, v8}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public compareBookmarks()Ljava/lang/String;
    .locals 17

    .prologue
    .line 753
    new-instance v10, Ljava/lang/String;

    const-string v1, "NOERROR"

    invoke-direct {v10, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 755
    .local v10, "answer":Ljava/lang/String;
    const/4 v8, 0x0

    .line 756
    .local v8, "FOUND_NO":I
    const/4 v9, 0x1

    .line 757
    .local v9, "FOUND_TITLE":I
    const/4 v7, 0x2

    .line 758
    .local v7, "FOUND_ALL":I
    const/4 v14, 0x0

    .line 760
    .local v14, "fnd":I
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 762
    const/4 v12, 0x0

    .line 764
    .local v12, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->bNewSBrowser:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 765
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->getBookmarksEx()Ljava/util/ArrayList;

    move-result-object v12

    .line 768
    :goto_0
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_a

    .line 769
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_BOOKMARKS_CONTENT_URI_TABLE:Landroid/net/Uri;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "title"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "url"

    aput-object v5, v3, v4

    const-string v4, "FOLDER=0"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 773
    .local v13, "dbCur":Landroid/database/Cursor;
    if-nez v13, :cond_1

    .line 774
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareBookmarks : dbCur is null!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 775
    const-string v1, "CSCSBrowser : ** compare : SBrowser bookmarks cursor is null!"

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 777
    const-string v10, "Settings.Browser."

    move-object v11, v10

    .line 866
    .end local v10    # "answer":Ljava/lang/String;
    .end local v13    # "dbCur":Landroid/database/Cursor;
    .local v11, "answer":Ljava/lang/Object;
    :goto_1
    return-object v11

    .line 767
    .end local v11    # "answer":Ljava/lang/Object;
    .restart local v10    # "answer":Ljava/lang/String;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->getBookmarks()Ljava/util/ArrayList;

    move-result-object v12

    goto :goto_0

    .line 781
    .restart local v13    # "dbCur":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    if-eq v1, v2, :cond_2

    .line 782
    const-string v1, "CscSBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "compareBookmarks : dbCur.getCount() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bookmarks.size()/2 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    new-instance v10, Ljava/lang/String;

    .end local v10    # "answer":Ljava/lang/String;
    const-string v1, "SBrowser Bookmarks count not matched"

    invoke-direct {v10, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 785
    .restart local v10    # "answer":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bookmarkFromDb: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BookmarkFromCsc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v10, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    move-object v11, v10

    .line 788
    .restart local v11    # "answer":Ljava/lang/Object;
    goto :goto_1

    .line 790
    .end local v11    # "answer":Ljava/lang/Object;
    :cond_2
    const-string v1, "Settings.Browser.Bookmark.NetworkName"

    const-string v2, "NetworkName pass"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_9

    .line 793
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 794
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_2
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v15, v1, :cond_4

    .line 795
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_3
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v1

    move/from16 v0, v16

    if-ge v0, v1, :cond_3

    .line 796
    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 797
    const/4 v14, 0x1

    .line 799
    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 800
    const/4 v14, 0x2

    .line 801
    const-string v2, "Settings.Browser.Bookmark.BookmarkName"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v3, v15, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") BookmarkName: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    const-string v2, "Settings.Browser.Bookmark.URL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v3, v15, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") URL: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    const-string v1, "Settings.Browser.Bookmark.Editable"

    const-string v2, "editable pass"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    :cond_3
    :goto_4
    const/4 v1, 0x1

    if-ne v14, v1, :cond_7

    .line 830
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareBookmarks : Bookmark URL is missied.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    const-string v1, "CSCSBrowser : ** compare : Bookmark URL is missied.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 834
    const-string v10, "Settings.Browser.Bookmark.URL"

    .line 835
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-int/lit8 v2, v15, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v10, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    .end local v16    # "j":I
    :cond_4
    :goto_5
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareBookmarks : Bookmarks DONE.."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    .end local v15    # "i":I
    :goto_6
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 860
    const-string v1, "Settings.Browser.eManual.URL"

    const-string v2, "eManual.URL pass"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .end local v13    # "dbCur":Landroid/database/Cursor;
    :goto_7
    move-object v11, v10

    .line 866
    .restart local v11    # "answer":Ljava/lang/Object;
    goto/16 :goto_1

    .line 811
    .end local v11    # "answer":Ljava/lang/Object;
    .restart local v13    # "dbCur":Landroid/database/Cursor;
    .restart local v15    # "i":I
    .restart local v16    # "j":I
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->bNewSBrowser:Z

    if-eqz v1, :cond_6

    .line 813
    const/4 v14, 0x2

    .line 814
    const-string v2, "Settings.Browser.Bookmark.BookmarkName"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v3, v15, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") BookmarkName: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    const-string v2, "Settings.Browser.Bookmark.URL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v3, v15, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") URL: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    const-string v1, "Settings.Browser.Bookmark.Editable"

    const-string v2, "editable pass"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 826
    :cond_6
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    .line 795
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_3

    .line 838
    :cond_7
    if-nez v14, :cond_8

    .line 839
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareBookmarks : Bookmark Name is missed.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    const-string v1, "CSCSBrowser : ** compare : Bookmark Name is missed.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 843
    const-string v10, "Settings.Browser.Bookmark.BookmarkName"

    .line 844
    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v10, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 847
    :cond_8
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 848
    const/4 v14, 0x0

    .line 794
    add-int/lit8 v15, v15, 0x2

    goto/16 :goto_2

    .line 854
    .end local v15    # "i":I
    .end local v16    # "j":I
    :cond_9
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareBookmarks : Browser DB is empty.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    const-string v1, "CSCSBrowser : ** compare : Browser DB is empty.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 857
    const-string v10, "Settings.Browser."

    goto/16 :goto_6

    .line 863
    .end local v13    # "dbCur":Landroid/database/Cursor;
    :cond_a
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareBookmarks : No bookmarks were given. NOERROR.."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7
.end method

.method public compareHomeUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 19
    .param p1, "currentNtwName"    # Ljava/lang/String;

    .prologue
    .line 365
    new-instance v2, Ljava/lang/String;

    const-string v16, "NOERROR"

    move-object/from16 v0, v16

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 366
    .local v2, "answer":Ljava/lang/String;
    new-instance v3, Ljava/lang/String;

    const-string v16, "ERROR"

    move-object/from16 v0, v16

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 367
    .local v3, "error":Ljava/lang/String;
    const/4 v11, 0x0

    .line 368
    .local v11, "profbrowser":Ljava/lang/String;
    const/4 v7, 0x0

    .line 369
    .local v7, "mpsurl":Ljava/lang/String;
    const/4 v4, 0x0

    .line 374
    .local v4, "homeurl":Ljava/lang/String;
    const-string v16, "CscSBrowser"

    const-string v17, "--ProfileHandle parse start--"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    new-instance v16, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-direct/range {v16 .. v17}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 377
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    const-string v17, "Settings.Connections."

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 378
    .local v8, "node":Lorg/w3c/dom/Node;
    if-nez v8, :cond_0

    .line 379
    const-string v16, "CscSBrowser"

    const-string v17, "Conecction Node is null"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    .end local v3    # "error":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 383
    .restart local v3    # "error":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    const-string v17, "ProfileHandle"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v8, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v9

    .line 384
    .local v9, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v9, :cond_1

    .line 385
    const-string v16, "CscSBrowser"

    const-string v17, "ProfileHandle node is null"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 389
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mProfileHandles:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->clear()V

    .line 391
    const-string v16, "CscSBrowser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "the number of mProfileHandles = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v16

    move/from16 v0, v16

    if-ge v5, v0, :cond_4

    .line 394
    new-instance v10, Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;

    invoke-direct {v10}, Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;-><init>()V

    .line 397
    .local v10, "profHandle":Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    invoke-interface {v9, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v17

    const-string v18, "NetworkName"

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    .line 398
    .local v15, "tempNode":Lorg/w3c/dom/Node;
    if-eqz v15, :cond_2

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v10, Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;->nwkname:Ljava/lang/String;

    .line 401
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    invoke-interface {v9, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v17

    const-string v18, "ProfBrowser"

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    .line 402
    if-eqz v15, :cond_3

    .line 403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v10, Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;->profBrowser:Ljava/lang/String;

    .line 405
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mProfileHandles:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 408
    .end local v10    # "profHandle":Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;
    .end local v15    # "tempNode":Lorg/w3c/dom/Node;
    :cond_4
    const-string v16, "CscSBrowser"

    const-string v17, "--Profile parse start--"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    const-string v17, "Profile"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v8, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v9

    .line 411
    if-nez v9, :cond_5

    .line 412
    const-string v16, "CscSBrowser"

    const-string v17, "Profile node is null"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 415
    :cond_5
    const/4 v5, 0x0

    :goto_2
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v16

    move/from16 v0, v16

    if-ge v5, v0, :cond_9

    .line 416
    new-instance v12, Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;

    invoke-direct {v12}, Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;-><init>()V

    .line 420
    .local v12, "profile":Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    invoke-interface {v9, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v17

    const-string v18, "NetworkName"

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    .line 421
    .restart local v15    # "tempNode":Lorg/w3c/dom/Node;
    if-eqz v15, :cond_6

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v12, Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;->nwkname:Ljava/lang/String;

    .line 424
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    invoke-interface {v9, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v17

    const-string v18, "ProfileName"

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    .line 425
    if-eqz v15, :cond_7

    .line 426
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v12, Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;->profName:Ljava/lang/String;

    .line 428
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    invoke-interface {v9, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v17

    const-string v18, "URL"

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    .line 429
    if-eqz v15, :cond_8

    .line 430
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v12, Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;->url:Ljava/lang/String;

    .line 432
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mProfiles:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 415
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 434
    .end local v12    # "profile":Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;
    .end local v15    # "tempNode":Lorg/w3c/dom/Node;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mProfileHandles:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;

    .line 435
    .local v13, "profileHandle":Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;
    const-string v16, "CscSBrowser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "currentNtwName"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const-string v16, "CscSBrowser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "profileHandle.nwkname"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-object v0, v13, Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;->nwkname:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    iget-object v0, v13, Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;->nwkname:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_a

    iget-object v0, v13, Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;->nwkname:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 439
    iget-object v11, v13, Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;->profBrowser:Ljava/lang/String;

    .line 440
    const-string v16, "CscSBrowser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "ProfBrowser is "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    .end local v13    # "profileHandle":Lcom/samsung/sec/android/application/csc/CscSBrowser$ProfileHandle;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    const-string v17, "csc.preferences_name"

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 446
    .local v14, "sp":Landroid/content/SharedPreferences;
    const-string v16, "homepage"

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 448
    const-string v16, "CscSBrowser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Url setting is "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    if-nez v11, :cond_c

    .line 451
    const-string v16, "CscSBrowser"

    const-string v17, "profBrowser null"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 455
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mProfiles:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;

    .line 456
    .restart local v12    # "profile":Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;
    iget-object v0, v12, Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;->profName:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_d

    iget-object v0, v12, Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;->profName:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_d

    .line 457
    iget-object v7, v12, Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;->url:Ljava/lang/String;

    .line 458
    const-string v16, "CscSBrowser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Customer xml URL is "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    .end local v12    # "profile":Lcom/samsung/sec/android/application/csc/CscSBrowser$Profile;
    :cond_e
    if-nez v7, :cond_f

    .line 464
    const-string v16, "CscSBrowser"

    const-string v17, "mpsurl is null"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 468
    :cond_f
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_10

    .line 469
    const-string v16, "CscSBrowser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "MPS url is "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ",  Setting url is "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    const-string v16, "Settings.Connections.URL"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "MPS Url"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "Setting URL: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-static/range {v16 .. v18}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    move-object v3, v2

    .line 476
    goto/16 :goto_0

    .line 473
    :cond_10
    const-string v16, "Settings.Connections.URL"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "MPS Url : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " Setting URL: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public compareQuickAccess()Ljava/lang/String;
    .locals 17

    .prologue
    .line 870
    new-instance v11, Ljava/lang/String;

    const-string v1, "NOERROR"

    invoke-direct {v11, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 872
    .local v11, "answer":Ljava/lang/String;
    const/4 v8, 0x0

    .line 873
    .local v8, "FOUND_NO":I
    const/4 v9, 0x1

    .line 874
    .local v9, "FOUND_TITLE":I
    const/4 v7, 0x2

    .line 875
    .local v7, "FOUND_ALL":I
    const/4 v14, 0x0

    .line 877
    .local v14, "fnd":I
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 879
    const/4 v10, 0x0

    .line 881
    .local v10, "QuickAccess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->getQuickAccess()Ljava/util/ArrayList;

    move-result-object v10

    .line 882
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_8

    .line 883
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_QUICKACCESS_CONTENT_URI_TABLE:Landroid/net/Uri;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "title"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "url"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 887
    .local v13, "dbCur":Landroid/database/Cursor;
    if-nez v13, :cond_0

    .line 888
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareQuickAccess : dbCur is null!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    const-string v1, "CSCSBrowser : ** compare : SBrowser QuickAccess cursor is null!"

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 891
    const-string v11, "Settings.Browser."

    move-object v12, v11

    .line 966
    .end local v11    # "answer":Ljava/lang/String;
    .end local v13    # "dbCur":Landroid/database/Cursor;
    .local v12, "answer":Ljava/lang/Object;
    :goto_0
    return-object v12

    .line 895
    .end local v12    # "answer":Ljava/lang/Object;
    .restart local v11    # "answer":Ljava/lang/String;
    .restart local v13    # "dbCur":Landroid/database/Cursor;
    :cond_0
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    if-eq v1, v2, :cond_1

    .line 896
    const-string v1, "CscSBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "compareQuickAccess : dbCur.getCount() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " QuickAccess.size()/2 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    new-instance v11, Ljava/lang/String;

    .end local v11    # "answer":Ljava/lang/String;
    const-string v1, "QuickAccess count not matched"

    invoke-direct {v11, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 899
    .restart local v11    # "answer":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QuickAccessFromDb: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QuickAccessFromCsc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    move-object v12, v11

    .line 902
    .restart local v12    # "answer":Ljava/lang/Object;
    goto :goto_0

    .line 905
    .end local v12    # "answer":Ljava/lang/Object;
    :cond_1
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_7

    .line 906
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 907
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v15, v1, :cond_3

    .line 908
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_2
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v1

    move/from16 v0, v16

    if-ge v0, v1, :cond_2

    .line 909
    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 910
    const/4 v14, 0x1

    .line 912
    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 913
    const/4 v14, 0x2

    .line 914
    const-string v2, "Settings.Browser.QuickAccess.QATitle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v3, v15, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") QuickAccessName: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    const-string v2, "Settings.Browser.QuickAccess.QAURL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v3, v15, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") URL: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 920
    const-string v1, "Settings.Browser.QuickAccess.Index"

    const-string v2, "Index pass"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    const-string v1, "Settings.Browser.QuickAccess.Editable"

    const-string v2, "editable pass"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    :cond_2
    const/4 v1, 0x1

    if-ne v14, v1, :cond_5

    .line 931
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareQuickAccess : QuickAccess URL is missied.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    const-string v1, "CSCSBrowser : ** compare : QuickAccess URL is missied.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 935
    const-string v11, "Settings.Browser.Bookmark.URL"

    .line 936
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-int/lit8 v2, v15, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v11, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    .end local v16    # "j":I
    :cond_3
    :goto_3
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareQuickAccess : QuickAccess DONE.."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 960
    .end local v15    # "i":I
    :goto_4
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .end local v13    # "dbCur":Landroid/database/Cursor;
    :goto_5
    move-object v12, v11

    .line 966
    .restart local v12    # "answer":Ljava/lang/Object;
    goto/16 :goto_0

    .line 927
    .end local v12    # "answer":Ljava/lang/Object;
    .restart local v13    # "dbCur":Landroid/database/Cursor;
    .restart local v15    # "i":I
    .restart local v16    # "j":I
    :cond_4
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    .line 908
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_2

    .line 939
    :cond_5
    if-nez v14, :cond_6

    .line 940
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareQuickAccess : QuickAccess Name is missed.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    const-string v1, "CSCSBrowser : ** compare : QuickAccess Name is missed.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 944
    const-string v11, "Settings.Browser.QuickAccess.QATitle"

    .line 945
    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v11, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 948
    :cond_6
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 949
    const/4 v14, 0x0

    .line 907
    add-int/lit8 v15, v15, 0x2

    goto/16 :goto_1

    .line 955
    .end local v15    # "i":I
    .end local v16    # "j":I
    :cond_7
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareQuickAccess : QuickAccess DB is empty.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    const-string v1, "CSCSBrowser : ** compare : Browser DB is empty.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 958
    const-string v11, "Settings.Browser.QuickAccess."

    goto :goto_4

    .line 963
    .end local v13    # "dbCur":Landroid/database/Cursor;
    :cond_8
    const-string v1, "CscSBrowser"

    const-string v2, " ** compareQuickAccess : No QuickAccess were given. NOERROR.."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 12
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 492
    const-string v0, "CscSBrowser"

    const-string v1, " ** encode runs.."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "title"

    aput-object v4, v2, v10

    const-string v4, "url"

    aput-object v4, v2, v11

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 497
    .local v8, "dbCur":Landroid/database/Cursor;
    if-nez v8, :cond_0

    .line 517
    :goto_0
    return-void

    .line 500
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 502
    .local v7, "NumBookmark":I
    if-lez v7, :cond_1

    .line 503
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 504
    const-string v0, "Settings.Browser.NbBookmark"

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-le v7, v9, :cond_1

    .line 507
    const-string v0, "Bookmark"

    invoke-virtual {p1, v0}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->newElement(Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscXMLElement;

    move-result-object v6

    .line 508
    .local v6, "Bookmarknode":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    const-string v0, "NetworkName"

    const-string v1, "default"

    invoke-virtual {v6, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const-string v0, "Index"

    add-int/lit8 v1, v9, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    const-string v0, "BookmarkName"

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    const-string v0, "URL"

    invoke-interface {v8, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    const-string v0, "Settings.Browser."

    invoke-virtual {p1, v0, v6}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addElement(Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscXMLElement;)V

    .line 513
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    .line 505
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 516
    .end local v6    # "Bookmarknode":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    .end local v9    # "i":I
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getBookmarks()Ljava/util/ArrayList;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 971
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v19

    const-string v20, "CscFeature_Web_EnableAutoBookmarkSetBySim"

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v17

    .line 973
    .local v17, "isAutoBookmarkFeature":Z
    const-string v11, "none"

    .line 975
    .local v11, "currentNtwName":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 976
    .local v7, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v19, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 977
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "Settings.Browser."

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 978
    .local v8, "browserNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "Bookmark"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v9

    .line 979
    .local v9, "browserNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v9, :cond_6

    .line 982
    if-eqz v17, :cond_0

    .line 983
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->getCurrentNetworkName()Ljava/lang/String;

    move-result-object v11

    .line 986
    :cond_0
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v19

    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_7

    .line 987
    move/from16 v0, v16

    invoke-interface {v9, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 988
    .local v10, "browserNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "BookmarkName"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v10, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 989
    .local v3, "bookmarkName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "URL"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v10, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 991
    .local v5, "bookmarkUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 992
    .local v4, "bookmarkNames":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 993
    const-string v19, "CscSBrowser"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " getBookmarks: getBookmarks ("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ") : bookmark name is null. Skip.."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    .line 997
    .local v6, "bookmarkUrls":Ljava/lang/String;
    if-nez v6, :cond_2

    .line 998
    const-string v19, "CscSBrowser"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " getBookmarks: getBookmarks ("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ") : bookmark url is null. Skip.."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    :cond_2
    if-eqz v6, :cond_5

    if-eqz v4, :cond_5

    .line 1003
    if-eqz v17, :cond_4

    .line 1004
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "NetworkName"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v10, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v18

    .line 1005
    .local v18, "nodeNtwName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    .line 1006
    .local v2, "NtwName":Ljava/lang/String;
    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 1007
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1008
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 986
    .end local v2    # "NtwName":Ljava/lang/String;
    .end local v18    # "nodeNtwName":Lorg/w3c/dom/Node;
    :cond_3
    :goto_1
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    .line 1011
    :cond_4
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1015
    :cond_5
    const-string v19, "CscSBrowser"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " getBookmarks: getBookmarks ("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ") : bookmarkUrls or bookmarkNames is null. Skip.."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1020
    .end local v3    # "bookmarkName":Lorg/w3c/dom/Node;
    .end local v4    # "bookmarkNames":Ljava/lang/String;
    .end local v5    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .end local v6    # "bookmarkUrls":Ljava/lang/String;
    .end local v10    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .end local v16    # "i":I
    :cond_6
    const-string v19, "CscSBrowser"

    const-string v20, " ** getBookmarks : bookmarkNodeList is null. Skip.."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1025
    :cond_7
    if-eqz v17, :cond_8

    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->isEnableLog()Z

    move-result v19

    if-eqz v19, :cond_9

    const-string v19, "gsm.sim.operator.numeric"

    const-string v20, ""

    invoke-static/range {v19 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    if-lez v19, :cond_9

    .line 1029
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "eManual"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v13

    .line 1030
    .local v13, "eManualBookmarkNode":Lorg/w3c/dom/Node;
    if-eqz v13, :cond_9

    .line 1031
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "URL"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v13, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    .line 1032
    .local v15, "eManualUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v20, 0x7f04000e

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1033
    .local v12, "eManualBookmarkName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v14

    .line 1034
    .local v14, "eManualBookmarkUrl":Ljava/lang/String;
    if-eqz v14, :cond_a

    .line 1035
    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1036
    invoke-virtual {v7, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1037
    const-string v19, "CscSBrowser"

    const-string v20, " ** getBookmarks : eManual bookmark added"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1044
    .end local v12    # "eManualBookmarkName":Ljava/lang/String;
    .end local v13    # "eManualBookmarkNode":Lorg/w3c/dom/Node;
    .end local v14    # "eManualBookmarkUrl":Ljava/lang/String;
    .end local v15    # "eManualUrl":Lorg/w3c/dom/Node;
    :cond_9
    :goto_2
    return-object v7

    .line 1039
    .restart local v12    # "eManualBookmarkName":Ljava/lang/String;
    .restart local v13    # "eManualBookmarkNode":Lorg/w3c/dom/Node;
    .restart local v14    # "eManualBookmarkUrl":Ljava/lang/String;
    .restart local v15    # "eManualUrl":Lorg/w3c/dom/Node;
    :cond_a
    const-string v19, "CscSBrowser"

    const-string v20, " ** getBookmarks : eManual bookmark url is null. Skip.."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public getBookmarksEx()Ljava/util/ArrayList;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1049
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v22

    const-string v23, "CscFeature_Web_EnableAutoBookmarkSetBySim"

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v17

    .line 1051
    .local v17, "isAutoBookmarkFeature":Z
    const-string v11, "none"

    .line 1054
    .local v11, "currentNtwName":Ljava/lang/String;
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 1055
    .local v20, "title":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 1057
    .local v21, "url":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1058
    .local v7, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v22, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 1059
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    const-string v23, "Settings.Browser."

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 1060
    .local v8, "browserNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    const-string v23, "Bookmark"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v8, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v9

    .line 1061
    .local v9, "browserNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v9, :cond_4

    .line 1064
    if-eqz v17, :cond_0

    .line 1065
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->getCurrentNetworkName()Ljava/lang/String;

    move-result-object v11

    .line 1068
    :cond_0
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v22

    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    .line 1069
    move/from16 v0, v16

    invoke-interface {v9, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 1070
    .local v10, "browserNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    const-string v23, "BookmarkName"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v10, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 1071
    .local v3, "bookmarkName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    const-string v23, "URL"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v10, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 1073
    .local v5, "bookmarkUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 1074
    .local v4, "bookmarkNames":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    .line 1076
    .local v6, "bookmarkUrls":Ljava/lang/String;
    if-eqz v6, :cond_3

    if-eqz v4, :cond_3

    .line 1078
    if-eqz v17, :cond_2

    .line 1079
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    const-string v23, "NetworkName"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v10, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v19

    .line 1080
    .local v19, "nodeNtwName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    .line 1081
    .local v2, "NtwName":Ljava/lang/String;
    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 1082
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1083
    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1068
    .end local v2    # "NtwName":Ljava/lang/String;
    .end local v19    # "nodeNtwName":Lorg/w3c/dom/Node;
    :cond_1
    :goto_1
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 1086
    :cond_2
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1087
    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1090
    :cond_3
    const-string v22, "CscSBrowser"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, " getBookmarksEx: getBookmarks ("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ") : bookmarkUrls or bookmarkNames is null. Skip.."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1095
    .end local v3    # "bookmarkName":Lorg/w3c/dom/Node;
    .end local v4    # "bookmarkNames":Ljava/lang/String;
    .end local v5    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .end local v6    # "bookmarkUrls":Ljava/lang/String;
    .end local v10    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .end local v16    # "i":I
    :cond_4
    const-string v22, "CscSBrowser"

    const-string v23, " ** getBookmarksEx : bookmarkNodeList is null. Skip.."

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1100
    :cond_5
    if-eqz v17, :cond_6

    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->isEnableLog()Z

    move-result v22

    if-eqz v22, :cond_7

    const-string v22, "gsm.sim.operator.numeric"

    const-string v23, ""

    invoke-static/range {v22 .. v23}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    if-lez v22, :cond_7

    .line 1104
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    const-string v23, "eManual"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v8, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v13

    .line 1105
    .local v13, "eManualBookmarkNode":Lorg/w3c/dom/Node;
    if-eqz v13, :cond_7

    .line 1106
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    const-string v23, "URL"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v13, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    .line 1107
    .local v15, "eManualUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const v23, 0x7f04000e

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1108
    .local v12, "eManualBookmarkName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v14

    .line 1109
    .local v14, "eManualBookmarkUrl":Ljava/lang/String;
    if-eqz v14, :cond_8

    .line 1110
    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1111
    invoke-virtual {v7, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1112
    const-string v22, "CscSBrowser"

    const-string v23, " ** getBookmarksEx : eManual bookmark added"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1120
    .end local v12    # "eManualBookmarkName":Ljava/lang/String;
    .end local v13    # "eManualBookmarkNode":Lorg/w3c/dom/Node;
    .end local v14    # "eManualBookmarkUrl":Ljava/lang/String;
    .end local v15    # "eManualUrl":Lorg/w3c/dom/Node;
    :cond_7
    :goto_2
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v22

    if-lez v22, :cond_9

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v22

    if-lez v22, :cond_9

    .line 1121
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v22

    add-int/lit8 v18, v22, -0x1

    .local v18, "j":I
    :goto_3
    if-ltz v18, :cond_a

    .line 1122
    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1123
    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1121
    add-int/lit8 v18, v18, -0x1

    goto :goto_3

    .line 1114
    .end local v18    # "j":I
    .restart local v12    # "eManualBookmarkName":Ljava/lang/String;
    .restart local v13    # "eManualBookmarkNode":Lorg/w3c/dom/Node;
    .restart local v14    # "eManualBookmarkUrl":Ljava/lang/String;
    .restart local v15    # "eManualUrl":Lorg/w3c/dom/Node;
    :cond_8
    const-string v22, "CscSBrowser"

    const-string v23, " ** getBookmarksEx : eManual bookmark url is null. Skip.."

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1126
    .end local v12    # "eManualBookmarkName":Ljava/lang/String;
    .end local v13    # "eManualBookmarkNode":Lorg/w3c/dom/Node;
    .end local v14    # "eManualBookmarkUrl":Ljava/lang/String;
    .end local v15    # "eManualUrl":Lorg/w3c/dom/Node;
    :cond_9
    const-string v22, "CscSBrowser"

    const-string v23, " getBookmarksEx : arrarList size error"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1129
    :cond_a
    return-object v7
.end method

.method public getQuickAccess()Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1135
    .local v0, "QuickAccess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v9, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v10, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v9, v10}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 1136
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v10, "Settings.Browser."

    invoke-virtual {v9, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 1137
    .local v6, "browserNode":Lorg/w3c/dom/Node;
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v10, "QuickAccess"

    invoke-virtual {v9, v6, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 1138
    .local v3, "QuickAccessNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v3, :cond_3

    .line 1140
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-ge v8, v9, :cond_4

    .line 1141
    invoke-interface {v3, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 1142
    .local v7, "browserNodeListChild":Lorg/w3c/dom/Node;
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v10, "QATitle"

    invoke-virtual {v9, v7, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 1143
    .local v1, "QuickAccessName":Lorg/w3c/dom/Node;
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v10, "QAURL"

    invoke-virtual {v9, v7, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 1145
    .local v4, "QuickAccessUrl":Lorg/w3c/dom/Node;
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v9, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    .line 1146
    .local v2, "QuickAccessNames":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 1147
    const-string v9, "CscSBrowser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " getBookmarks: getBookmarks ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") : bookmark name is null. Skip.."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1150
    :cond_0
    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v9, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    .line 1151
    .local v5, "QuickAccessUrls":Ljava/lang/String;
    if-nez v5, :cond_1

    .line 1152
    const-string v9, "CscSBrowser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " getBookmarks: getBookmarks ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") : bookmark url is null. Skip.."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1155
    :cond_1
    if-eqz v5, :cond_2

    if-eqz v2, :cond_2

    .line 1156
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1157
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1140
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1159
    :cond_2
    const-string v9, "CscSBrowser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " getQuickAccess: getQuickAccess ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") : QuickAccessUrls or QuickAccessNames is null. Skip.."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1164
    .end local v1    # "QuickAccessName":Lorg/w3c/dom/Node;
    .end local v2    # "QuickAccessNames":Ljava/lang/String;
    .end local v4    # "QuickAccessUrl":Lorg/w3c/dom/Node;
    .end local v5    # "QuickAccessUrls":Ljava/lang/String;
    .end local v7    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .end local v8    # "i":I
    :cond_3
    const-string v9, "CscSBrowser"

    const-string v10, " ** getQuickAccess : QuickAccessNodeList is null. Skip.."

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1166
    :cond_4
    return-object v0
.end method

.method public update()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v3, 0x0

    .line 231
    const-string v0, "CscSBrowser"

    const-string v1, " ** update runs.."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 234
    const-string v0, "CscSBrowser"

    const-string v1, "update : mContext is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_2

    .line 239
    const-string v0, "CscSBrowser"

    const-string v1, "update : mResolver is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 243
    :cond_2
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    .line 245
    .local v12, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SEND_PACKAGE_SBROWSER:Landroid/content/ComponentName;

    const/16 v1, 0x80

    invoke-virtual {v12, v0, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 246
    const-string v0, "CscSBrowser"

    const-string v1, "update : find com.sec.android.app.sbrowser.SBrowserMainActivity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    const/4 v8, 0x0

    .line 254
    .local v8, "cpClient":Landroid/content/ContentProviderClient;
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 257
    :try_start_1
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_OPBOOKMARK_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    .line 263
    :goto_1
    if-nez v8, :cond_3

    .line 264
    const-string v0, "CscSBrowser"

    const-string v1, "update : Phone does not ready to acquire Browser Provider"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 247
    .end local v8    # "cpClient":Landroid/content/ContentProviderClient;
    :catch_0
    move-exception v10

    .line 248
    .local v10, "e":Ljava/lang/Exception;
    const-string v0, "CscSBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "update : com.sec.android.app.sbrowser.SBrowserMainActivity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 258
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v8    # "cpClient":Landroid/content/ContentProviderClient;
    :catch_1
    move-exception v10

    .line 259
    .restart local v10    # "e":Ljava/lang/Exception;
    const-string v0, "CscSBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "update : SBROWSER_OPBOOKMARK_CONTENT_URI Exception"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const/4 v8, 0x0

    goto :goto_1

    .line 267
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_3
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mHomepageUrlRcvr:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 270
    iget-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->bNewSBrowser:Z

    if-nez v0, :cond_7

    .line 271
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->updateBookmarks()V

    .line 286
    :goto_2
    :try_start_2
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 291
    :goto_3
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Browser."

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 292
    .local v7, "browserNode":Lorg/w3c/dom/Node;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "QuickAccess"

    invoke-virtual {v0, v7, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 293
    .local v6, "QuickAccessNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v6, :cond_4

    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-lt v0, v13, :cond_4

    .line 295
    :try_start_3
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_QUICKACCESS_CONTENT_URI_TABLE:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 300
    :cond_4
    :goto_4
    iget-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->bNewSBrowser:Z

    if-ne v0, v13, :cond_5

    .line 302
    const-wide/16 v0, 0x32

    :try_start_4
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5

    .line 307
    :cond_5
    :goto_5
    iget-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->bNewSBrowser:Z

    if-ne v0, v13, :cond_9

    .line 308
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->updateBookmarksEx()V

    .line 314
    :goto_6
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Web_EnableAutoBookmarkSetBySim"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 316
    new-instance v11, Landroid/content/Intent;

    const-string v0, "android.intent.action.BROWSER_AUTO_SET"

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 317
    .local v11, "it":Landroid/content/Intent;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 320
    .end local v11    # "it":Landroid/content/Intent;
    :cond_6
    if-eqz v8, :cond_0

    .line 321
    invoke-virtual {v8}, Landroid/content/ContentProviderClient;->release()Z

    goto/16 :goto_0

    .line 273
    .end local v6    # "QuickAccessNodeList":Lorg/w3c/dom/NodeList;
    .end local v7    # "browserNode":Lorg/w3c/dom/Node;
    :cond_7
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "title"

    aput-object v5, v2, v4

    const-string v4, "url"

    aput-object v4, v2, v13

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 276
    .local v9, "dbCur":Landroid/database/Cursor;
    if-eqz v9, :cond_8

    .line 277
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 279
    :cond_8
    const-wide/16 v0, 0x32

    :try_start_5
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 280
    :catch_2
    move-exception v0

    goto :goto_2

    .line 287
    .end local v9    # "dbCur":Landroid/database/Cursor;
    :catch_3
    move-exception v10

    .line 288
    .restart local v10    # "e":Ljava/lang/Exception;
    const-string v0, "update"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mResolver.delete : Exception"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 296
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v6    # "QuickAccessNodeList":Lorg/w3c/dom/NodeList;
    .restart local v7    # "browserNode":Lorg/w3c/dom/Node;
    :catch_4
    move-exception v10

    .line 297
    .restart local v10    # "e":Ljava/lang/Exception;
    const-string v0, "update"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mResolver.delete QuickAccess : Exception"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 311
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->updateBookmarks()V

    goto :goto_6

    .line 303
    :catch_5
    move-exception v0

    goto/16 :goto_5
.end method

.method public updateBookmarks()V
    .locals 38

    .prologue
    .line 520
    new-instance v34, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-direct/range {v34 .. v35}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 522
    const-string v34, "CscSBrowser"

    const-string v35, " updateBookmarks : It is called."

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    const-wide/16 v10, 0x1

    .line 528
    .local v10, "_id":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "Settings.Browser."

    invoke-virtual/range {v34 .. v35}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v21

    .line 529
    .local v21, "browserNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "Bookmark"

    move-object/from16 v0, v34

    move-object/from16 v1, v21

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v22

    .line 530
    .local v22, "browserNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v22, :cond_2

    .line 531
    const/16 v30, 0x0

    .local v30, "i":I
    :goto_0
    invoke-interface/range {v22 .. v22}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v34

    move/from16 v0, v30

    move/from16 v1, v34

    if-ge v0, v1, :cond_2

    .line 533
    move-object/from16 v0, v22

    move/from16 v1, v30

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v23

    .line 534
    .local v23, "browserNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "BookmarkName"

    move-object/from16 v0, v34

    move-object/from16 v1, v23

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v16

    .line 535
    .local v16, "bookmarkName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "URL"

    move-object/from16 v0, v34

    move-object/from16 v1, v23

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v18

    .line 536
    .local v18, "bookmarkUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "Editable"

    move-object/from16 v0, v34

    move-object/from16 v1, v23

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v14

    .line 538
    .local v14, "bookmarkEditable":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v17

    .line 539
    .local v17, "bookmarkNames":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    .line 540
    .local v19, "bookmarkUrls":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v0, v14}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v15

    .line 542
    .local v15, "bookmarkEditables":Ljava/lang/String;
    if-eqz v19, :cond_0

    if-eqz v17, :cond_0

    .line 543
    new-instance v32, Landroid/content/ContentValues;

    invoke-direct/range {v32 .. v32}, Landroid/content/ContentValues;-><init>()V

    .line 544
    .local v32, "opBookmark":Landroid/content/ContentValues;
    const-string v34, "_id"

    const-wide/16 v36, 0x1

    add-long v12, v10, v36

    .end local v10    # "_id":J
    .local v12, "_id":J
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 545
    const-string v34, "title"

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    const-string v34, "url"

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    if-eqz v15, :cond_1

    const-string v34, "no"

    move-object/from16 v0, v34

    invoke-virtual {v15, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v34

    if-nez v34, :cond_1

    .line 548
    const-string v34, "editable"

    const/16 v35, 0x0

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 551
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    if-eqz v34, :cond_9

    .line 553
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    sget-object v35, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v31

    .local v31, "inserted_uri":Landroid/net/Uri;
    move-wide v10, v12

    .line 531
    .end local v12    # "_id":J
    .end local v31    # "inserted_uri":Landroid/net/Uri;
    .end local v32    # "opBookmark":Landroid/content/ContentValues;
    .restart local v10    # "_id":J
    :cond_0
    :goto_2
    add-int/lit8 v30, v30, 0x1

    goto/16 :goto_0

    .line 550
    .end local v10    # "_id":J
    .restart local v12    # "_id":J
    .restart local v32    # "opBookmark":Landroid/content/ContentValues;
    :cond_1
    const-string v34, "editable"

    const/16 v35, 0x1

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 555
    :catch_0
    move-exception v24

    .line 556
    .local v24, "e":Ljava/lang/SecurityException;
    const-string v34, "CscSBrowser"

    const-string v35, " updateBookmarks : SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE SecurityException"

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v10, v12

    .line 558
    .end local v12    # "_id":J
    .restart local v10    # "_id":J
    goto :goto_2

    .line 565
    .end local v14    # "bookmarkEditable":Lorg/w3c/dom/Node;
    .end local v15    # "bookmarkEditables":Ljava/lang/String;
    .end local v16    # "bookmarkName":Lorg/w3c/dom/Node;
    .end local v17    # "bookmarkNames":Ljava/lang/String;
    .end local v18    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .end local v19    # "bookmarkUrls":Ljava/lang/String;
    .end local v23    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .end local v24    # "e":Ljava/lang/SecurityException;
    .end local v30    # "i":I
    .end local v32    # "opBookmark":Landroid/content/ContentValues;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "eManual"

    move-object/from16 v0, v34

    move-object/from16 v1, v21

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v27

    .line 566
    .local v27, "eManualBookmarkNode":Lorg/w3c/dom/Node;
    if-eqz v27, :cond_4

    .line 567
    new-instance v25, Landroid/content/ContentValues;

    invoke-direct/range {v25 .. v25}, Landroid/content/ContentValues;-><init>()V

    .line 568
    .local v25, "eManual":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "URL"

    move-object/from16 v0, v34

    move-object/from16 v1, v27

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v29

    .line 570
    .local v29, "eManualUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    const v35, 0x7f04000e

    invoke-virtual/range {v34 .. v35}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 571
    .local v26, "eManualBookmarkName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v28

    .line 572
    .local v28, "eManualBookmarkUrl":Ljava/lang/String;
    if-eqz v28, :cond_6

    .line 573
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 574
    .local v20, "bookmarkmap":Landroid/content/ContentValues;
    const-string v34, "_id"

    const-wide/16 v36, 0x1

    add-long v12, v10, v36

    .end local v10    # "_id":J
    .restart local v12    # "_id":J
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 575
    const-string v34, "title"

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    const-string v34, "url"

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v34, "editable"

    const/16 v35, 0x1

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    if-eqz v34, :cond_3

    .line 582
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    sget-object v35, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v31

    :cond_3
    :goto_3
    move-wide v10, v12

    .line 595
    .end local v12    # "_id":J
    .end local v20    # "bookmarkmap":Landroid/content/ContentValues;
    .end local v25    # "eManual":Landroid/content/ContentValues;
    .end local v26    # "eManualBookmarkName":Ljava/lang/String;
    .end local v28    # "eManualBookmarkUrl":Ljava/lang/String;
    .end local v29    # "eManualUrl":Lorg/w3c/dom/Node;
    .restart local v10    # "_id":J
    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "QuickAccess"

    move-object/from16 v0, v34

    move-object/from16 v1, v21

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 596
    .local v6, "QuickAccessNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v6, :cond_7

    .line 597
    const/16 v30, 0x0

    .restart local v30    # "i":I
    :goto_5
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v34

    move/from16 v0, v30

    move/from16 v1, v34

    if-ge v0, v1, :cond_7

    .line 598
    move/from16 v0, v30

    invoke-interface {v6, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 599
    .local v7, "QuickAccessNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "QATitle"

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-virtual {v0, v7, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 600
    .local v4, "QuickAccessName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "QAURL"

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-virtual {v0, v7, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 601
    .local v8, "QuickAccessUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    .line 602
    .local v5, "QuickAccessNames":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v0, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    .line 603
    .local v9, "QuickAccessUrls":Ljava/lang/String;
    if-eqz v9, :cond_5

    if-eqz v5, :cond_5

    .line 604
    new-instance v33, Landroid/content/ContentValues;

    invoke-direct/range {v33 .. v33}, Landroid/content/ContentValues;-><init>()V

    .line 605
    .local v33, "preloadQuicAccess":Landroid/content/ContentValues;
    const-string v34, "_id"

    const-wide/16 v36, 0x1

    add-long v12, v10, v36

    .end local v10    # "_id":J
    .restart local v12    # "_id":J
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    invoke-virtual/range {v33 .. v35}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 606
    const-string v34, "title"

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    const-string v34, "url"

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    const-string v34, "parent"

    const/16 v35, 0x0

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    invoke-virtual/range {v33 .. v35}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 609
    const-string v34, "dominant"

    const/16 v35, -0x1

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    invoke-virtual/range {v33 .. v35}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 610
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    if-eqz v34, :cond_8

    .line 612
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    sget-object v35, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_QUICKACCESS_CONTENT_URI_TABLE:Landroid/net/Uri;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v31

    .restart local v31    # "inserted_uri":Landroid/net/Uri;
    move-wide v10, v12

    .line 597
    .end local v12    # "_id":J
    .end local v31    # "inserted_uri":Landroid/net/Uri;
    .end local v33    # "preloadQuicAccess":Landroid/content/ContentValues;
    .restart local v10    # "_id":J
    :cond_5
    :goto_6
    add-int/lit8 v30, v30, 0x1

    goto/16 :goto_5

    .line 584
    .end local v4    # "QuickAccessName":Lorg/w3c/dom/Node;
    .end local v5    # "QuickAccessNames":Ljava/lang/String;
    .end local v6    # "QuickAccessNodeList":Lorg/w3c/dom/NodeList;
    .end local v7    # "QuickAccessNodeListChild":Lorg/w3c/dom/Node;
    .end local v8    # "QuickAccessUrl":Lorg/w3c/dom/Node;
    .end local v9    # "QuickAccessUrls":Ljava/lang/String;
    .end local v10    # "_id":J
    .end local v30    # "i":I
    .restart local v12    # "_id":J
    .restart local v20    # "bookmarkmap":Landroid/content/ContentValues;
    .restart local v25    # "eManual":Landroid/content/ContentValues;
    .restart local v26    # "eManualBookmarkName":Ljava/lang/String;
    .restart local v28    # "eManualBookmarkUrl":Ljava/lang/String;
    .restart local v29    # "eManualUrl":Lorg/w3c/dom/Node;
    :catch_1
    move-exception v24

    .line 585
    .restart local v24    # "e":Ljava/lang/SecurityException;
    const-string v34, "CscSBrowser"

    const-string v35, " updateBookmarks : SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE SecurityException"

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 591
    .end local v12    # "_id":J
    .end local v20    # "bookmarkmap":Landroid/content/ContentValues;
    .end local v24    # "e":Ljava/lang/SecurityException;
    .restart local v10    # "_id":J
    :cond_6
    const-string v34, "CscSBrowser"

    const-string v35, " ** updateBookmarks : eManual bookmark url is null. Skip.."

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 613
    .end local v10    # "_id":J
    .end local v25    # "eManual":Landroid/content/ContentValues;
    .end local v26    # "eManualBookmarkName":Ljava/lang/String;
    .end local v28    # "eManualBookmarkUrl":Ljava/lang/String;
    .end local v29    # "eManualUrl":Lorg/w3c/dom/Node;
    .restart local v4    # "QuickAccessName":Lorg/w3c/dom/Node;
    .restart local v5    # "QuickAccessNames":Ljava/lang/String;
    .restart local v6    # "QuickAccessNodeList":Lorg/w3c/dom/NodeList;
    .restart local v7    # "QuickAccessNodeListChild":Lorg/w3c/dom/Node;
    .restart local v8    # "QuickAccessUrl":Lorg/w3c/dom/Node;
    .restart local v9    # "QuickAccessUrls":Ljava/lang/String;
    .restart local v12    # "_id":J
    .restart local v30    # "i":I
    .restart local v33    # "preloadQuicAccess":Landroid/content/ContentValues;
    :catch_2
    move-exception v24

    .line 614
    .restart local v24    # "e":Ljava/lang/SecurityException;
    const-string v34, "CscSBrowser"

    const-string v35, " updateBookmarks : SBROWSER_QUICKACCESS_CONTENT_URI_TABLE SecurityException"

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v10, v12

    .line 615
    .end local v12    # "_id":J
    .restart local v10    # "_id":J
    goto :goto_6

    .line 622
    .end local v4    # "QuickAccessName":Lorg/w3c/dom/Node;
    .end local v5    # "QuickAccessNames":Ljava/lang/String;
    .end local v7    # "QuickAccessNodeListChild":Lorg/w3c/dom/Node;
    .end local v8    # "QuickAccessUrl":Lorg/w3c/dom/Node;
    .end local v9    # "QuickAccessUrls":Ljava/lang/String;
    .end local v24    # "e":Ljava/lang/SecurityException;
    .end local v30    # "i":I
    .end local v33    # "preloadQuicAccess":Landroid/content/ContentValues;
    :cond_7
    return-void

    .end local v10    # "_id":J
    .restart local v4    # "QuickAccessName":Lorg/w3c/dom/Node;
    .restart local v5    # "QuickAccessNames":Ljava/lang/String;
    .restart local v7    # "QuickAccessNodeListChild":Lorg/w3c/dom/Node;
    .restart local v8    # "QuickAccessUrl":Lorg/w3c/dom/Node;
    .restart local v9    # "QuickAccessUrls":Ljava/lang/String;
    .restart local v12    # "_id":J
    .restart local v30    # "i":I
    .restart local v33    # "preloadQuicAccess":Landroid/content/ContentValues;
    :cond_8
    move-wide v10, v12

    .end local v12    # "_id":J
    .restart local v10    # "_id":J
    goto :goto_6

    .end local v4    # "QuickAccessName":Lorg/w3c/dom/Node;
    .end local v5    # "QuickAccessNames":Ljava/lang/String;
    .end local v6    # "QuickAccessNodeList":Lorg/w3c/dom/NodeList;
    .end local v7    # "QuickAccessNodeListChild":Lorg/w3c/dom/Node;
    .end local v8    # "QuickAccessUrl":Lorg/w3c/dom/Node;
    .end local v9    # "QuickAccessUrls":Ljava/lang/String;
    .end local v10    # "_id":J
    .end local v27    # "eManualBookmarkNode":Lorg/w3c/dom/Node;
    .end local v33    # "preloadQuicAccess":Landroid/content/ContentValues;
    .restart local v12    # "_id":J
    .restart local v14    # "bookmarkEditable":Lorg/w3c/dom/Node;
    .restart local v15    # "bookmarkEditables":Ljava/lang/String;
    .restart local v16    # "bookmarkName":Lorg/w3c/dom/Node;
    .restart local v17    # "bookmarkNames":Ljava/lang/String;
    .restart local v18    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .restart local v19    # "bookmarkUrls":Ljava/lang/String;
    .restart local v23    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .restart local v32    # "opBookmark":Landroid/content/ContentValues;
    :cond_9
    move-wide v10, v12

    .end local v12    # "_id":J
    .restart local v10    # "_id":J
    goto/16 :goto_2
.end method

.method public updateBookmarksEx()V
    .locals 34

    .prologue
    .line 627
    const-wide/16 v4, 0x1

    .line 629
    .local v4, "_id":J
    const-string v31, "CscSBrowser"

    const-string v32, " updateBookmarksEx: called."

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v31, v0

    if-nez v31, :cond_0

    .line 632
    const-string v31, "CscSBrowser"

    const-string v32, " updateBookmarksEx: mResolver is null. Skip.."

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    :goto_0
    return-void

    .line 637
    :cond_0
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 638
    .local v29, "title":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 639
    .local v30, "url":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 641
    .local v24, "editable":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v31, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-direct/range {v31 .. v32}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 642
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v31, v0

    const-string v32, "Settings.Browser."

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    .line 643
    .local v15, "browserNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v31, v0

    const-string v32, "Bookmark"

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v0, v15, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v16

    .line 644
    .local v16, "browserNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v16, :cond_3

    .line 645
    const/16 v25, 0x0

    .local v25, "i":I
    :goto_1
    invoke-interface/range {v16 .. v16}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v31

    move/from16 v0, v25

    move/from16 v1, v31

    if-ge v0, v1, :cond_4

    .line 646
    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v17

    .line 647
    .local v17, "browserNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v31, v0

    const-string v32, "BookmarkName"

    move-object/from16 v0, v31

    move-object/from16 v1, v17

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 648
    .local v10, "bookmarkName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v31, v0

    const-string v32, "URL"

    move-object/from16 v0, v31

    move-object/from16 v1, v17

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v12

    .line 649
    .local v12, "bookmarkUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v31, v0

    const-string v32, "Editable"

    move-object/from16 v0, v31

    move-object/from16 v1, v17

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 651
    .local v8, "bookmarkEditable":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v11

    .line 652
    .local v11, "bookmarkNames":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v13

    .line 653
    .local v13, "bookmarkUrls":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    .line 655
    .local v9, "bookmarkEditables":Ljava/lang/String;
    if-eqz v13, :cond_1

    if-eqz v11, :cond_1

    .line 656
    move-object/from16 v0, v29

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 657
    move-object/from16 v0, v30

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 662
    :goto_2
    if-eqz v9, :cond_2

    const-string v31, "no"

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v31

    if-nez v31, :cond_2

    .line 663
    const/16 v31, 0x0

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 645
    :goto_3
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_1

    .line 659
    :cond_1
    const-string v31, "CscSBrowser"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, " updateBookmarksEx: getBookmarks ("

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, ") : bookmarkUrls or bookmarkNames is null. Skip.."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 665
    :cond_2
    const/16 v31, 0x1

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 668
    .end local v8    # "bookmarkEditable":Lorg/w3c/dom/Node;
    .end local v9    # "bookmarkEditables":Ljava/lang/String;
    .end local v10    # "bookmarkName":Lorg/w3c/dom/Node;
    .end local v11    # "bookmarkNames":Ljava/lang/String;
    .end local v12    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .end local v13    # "bookmarkUrls":Ljava/lang/String;
    .end local v17    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .end local v25    # "i":I
    :cond_3
    const-string v31, "CscSBrowser"

    const-string v32, " ** updateBookmarksEx : bookmarkNodeList is null. Skip.."

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v31, v0

    const-string v32, "eManual"

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v0, v15, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v21

    .line 672
    .local v21, "eManualBookmarkNode":Lorg/w3c/dom/Node;
    if-eqz v21, :cond_5

    .line 673
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 674
    .local v19, "eManual":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v31, v0

    const-string v32, "URL"

    move-object/from16 v0, v31

    move-object/from16 v1, v21

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v23

    .line 676
    .local v23, "eManualUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    const v32, 0x7f04000e

    invoke-virtual/range {v31 .. v32}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 677
    .local v20, "eManualBookmarkName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v22

    .line 678
    .local v22, "eManualBookmarkUrl":Ljava/lang/String;
    if-eqz v22, :cond_6

    .line 679
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 680
    .local v14, "bookmarkmap":Landroid/content/ContentValues;
    const-string v31, "_id"

    const-wide/16 v32, 0x1

    add-long v6, v4, v32

    .end local v4    # "_id":J
    .local v6, "_id":J
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 681
    const-string v31, "title"

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    const-string v31, "url"

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    const-string v31, "editable"

    const/16 v32, 0x1

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 687
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v31, v0

    sget-object v32, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v26

    .local v26, "inserted_uri":Landroid/net/Uri;
    :goto_4
    move-wide v4, v6

    .line 698
    .end local v6    # "_id":J
    .end local v14    # "bookmarkmap":Landroid/content/ContentValues;
    .end local v19    # "eManual":Landroid/content/ContentValues;
    .end local v20    # "eManualBookmarkName":Ljava/lang/String;
    .end local v22    # "eManualBookmarkUrl":Ljava/lang/String;
    .end local v23    # "eManualUrl":Lorg/w3c/dom/Node;
    .end local v26    # "inserted_uri":Landroid/net/Uri;
    .restart local v4    # "_id":J
    :cond_5
    :goto_5
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v31

    if-lez v31, :cond_8

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v31

    if-lez v31, :cond_8

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v31

    if-lez v31, :cond_8

    .line 699
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v31

    add-int/lit8 v27, v31, -0x1

    .local v27, "j":I
    move-wide v6, v4

    .end local v4    # "_id":J
    .restart local v6    # "_id":J
    :goto_6
    if-ltz v27, :cond_7

    .line 700
    new-instance v28, Landroid/content/ContentValues;

    invoke-direct/range {v28 .. v28}, Landroid/content/ContentValues;-><init>()V

    .line 701
    .local v28, "opBookmark":Landroid/content/ContentValues;
    const-string v31, "_id"

    const-wide/16 v32, 0x1

    add-long v4, v6, v32

    .end local v6    # "_id":J
    .restart local v4    # "_id":J
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v28

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 702
    const-string v32, "title"

    move-object/from16 v0, v29

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    move-object/from16 v0, v28

    move-object/from16 v1, v32

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    const-string v32, "url"

    move-object/from16 v0, v30

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    move-object/from16 v0, v28

    move-object/from16 v1, v32

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    const-string v32, "editable"

    move-object/from16 v0, v24

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/Integer;

    move-object/from16 v0, v28

    move-object/from16 v1, v32

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 706
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v31, v0

    sget-object v32, Lcom/samsung/sec/android/application/csc/CscSBrowser;->SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v26

    .line 709
    .restart local v26    # "inserted_uri":Landroid/net/Uri;
    const-wide/16 v32, 0x14

    :try_start_2
    invoke-static/range {v32 .. v33}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_1

    .line 699
    :goto_7
    add-int/lit8 v27, v27, -0x1

    move-wide v6, v4

    .end local v4    # "_id":J
    .restart local v6    # "_id":J
    goto :goto_6

    .line 688
    .end local v26    # "inserted_uri":Landroid/net/Uri;
    .end local v27    # "j":I
    .end local v28    # "opBookmark":Landroid/content/ContentValues;
    .restart local v14    # "bookmarkmap":Landroid/content/ContentValues;
    .restart local v19    # "eManual":Landroid/content/ContentValues;
    .restart local v20    # "eManualBookmarkName":Ljava/lang/String;
    .restart local v22    # "eManualBookmarkUrl":Ljava/lang/String;
    .restart local v23    # "eManualUrl":Lorg/w3c/dom/Node;
    :catch_0
    move-exception v18

    .line 689
    .local v18, "e":Ljava/lang/SecurityException;
    const-string v31, "CscSBrowser"

    const-string v32, " updateBookmarksEx : insert(eManual) SecurityException"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    const/16 v26, 0x0

    .restart local v26    # "inserted_uri":Landroid/net/Uri;
    goto/16 :goto_4

    .line 693
    .end local v6    # "_id":J
    .end local v14    # "bookmarkmap":Landroid/content/ContentValues;
    .end local v18    # "e":Ljava/lang/SecurityException;
    .end local v26    # "inserted_uri":Landroid/net/Uri;
    .restart local v4    # "_id":J
    :cond_6
    const-string v31, "CscSBrowser"

    const-string v32, " ** updateBookmarksEx : eManual bookmark url is null. Skip.."

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 712
    .end local v19    # "eManual":Landroid/content/ContentValues;
    .end local v20    # "eManualBookmarkName":Ljava/lang/String;
    .end local v22    # "eManualBookmarkUrl":Ljava/lang/String;
    .end local v23    # "eManualUrl":Lorg/w3c/dom/Node;
    .restart local v27    # "j":I
    .restart local v28    # "opBookmark":Landroid/content/ContentValues;
    :catch_1
    move-exception v18

    .line 713
    .restart local v18    # "e":Ljava/lang/SecurityException;
    const-string v31, "CscSBrowser"

    const-string v32, " updateBookmarksEx : insert() SecurityException"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    const/16 v26, 0x0

    .restart local v26    # "inserted_uri":Landroid/net/Uri;
    goto :goto_7

    .end local v4    # "_id":J
    .end local v18    # "e":Ljava/lang/SecurityException;
    .end local v26    # "inserted_uri":Landroid/net/Uri;
    .end local v28    # "opBookmark":Landroid/content/ContentValues;
    .restart local v6    # "_id":J
    :cond_7
    move-wide v4, v6

    .line 699
    .end local v6    # "_id":J
    .restart local v4    # "_id":J
    goto/16 :goto_0

    .line 718
    .end local v27    # "j":I
    :cond_8
    const-string v31, "CscSBrowser"

    const-string v32, " updateBookmarksEx : arrarList size error"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 710
    .restart local v26    # "inserted_uri":Landroid/net/Uri;
    .restart local v27    # "j":I
    .restart local v28    # "opBookmark":Landroid/content/ContentValues;
    :catch_2
    move-exception v31

    goto :goto_7
.end method
