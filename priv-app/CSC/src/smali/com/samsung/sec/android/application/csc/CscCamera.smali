.class public Lcom/samsung/sec/android/application/csc/CscCamera;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscCamera.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    }
.end annotation


# static fields
.field private static final CSC_KEY_PRFS:[Ljava/lang/String;


# instance fields
.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final NOERROR:Ljava/lang/String;

.field private final OTHERS_CSC_FILE:Ljava/lang/String;

.field private final TRACE_TAG:Ljava/lang/String;

.field private final VALUE_BURSTSHOT_OFF:Ljava/lang/String;

.field private final VALUE_BURSTSHOT_ON:Ljava/lang/String;

.field private final VALUE_CAMERA_RESOLUTION:[Ljava/lang/String;

.field private final VALUE_FINE:Ljava/lang/String;

.field private final VALUE_FLASH_AUTO:Ljava/lang/String;

.field private final VALUE_FLASH_OFF:Ljava/lang/String;

.field private final VALUE_FLASH_ON:Ljava/lang/String;

.field private final VALUE_MMC:Ljava/lang/String;

.field private final VALUE_NORMAL:Ljava/lang/String;

.field private final VALUE_OFF:Ljava/lang/String;

.field private final VALUE_ON:Ljava/lang/String;

.field private final VALUE_PHONE:Ljava/lang/String;

.field private final VALUE_SUPERFINE:Ljava/lang/String;

.field private final VALUE_VIDEO_RESOLUTION:[Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private sPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 119
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "csc_pref_camera_quality_key"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "csc_pref_camera_resolution_key"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "csc_pref_camera_videoquality_key"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "csc_pref_camcorder_resolution_key"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "csc_pref_setup_storage_key"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "csc_pref_camera_flash_key"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "csc_pref_camera_forced_shuttersound_key"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "csc_pref_camera_burstshot_key"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "csc_pref_camera_autonightdetection_key"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 219
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 38
    const-string v0, "NOERROR"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->NOERROR:Ljava/lang/String;

    .line 42
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 46
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->OTHERS_CSC_FILE:Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->sPrefs:Landroid/content/SharedPreferences;

    .line 55
    const-string v0, "CscCamera"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->TRACE_TAG:Ljava/lang/String;

    .line 82
    const-string v0, "off"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_FLASH_OFF:Ljava/lang/String;

    .line 84
    const-string v0, "on"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_FLASH_ON:Ljava/lang/String;

    .line 86
    const-string v0, "auto"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_FLASH_AUTO:Ljava/lang/String;

    .line 88
    const-string v0, "high"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_SUPERFINE:Ljava/lang/String;

    .line 90
    const-string v0, "normal"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_FINE:Ljava/lang/String;

    .line 92
    const-string v0, "low"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_NORMAL:Ljava/lang/String;

    .line 96
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "qxga"

    aput-object v1, v0, v3

    const-string v1, "hd1080"

    aput-object v1, v0, v4

    const-string v1, "uxga"

    aput-object v1, v0, v5

    const-string v1, "4vga"

    aput-object v1, v0, v6

    const-string v1, "hd720"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "wvga"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "vga"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "qvga"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "qcif"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_CAMERA_RESOLUTION:[Ljava/lang/String;

    .line 102
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "hd1080"

    aput-object v1, v0, v3

    const-string v1, "hd720"

    aput-object v1, v0, v4

    const-string v1, "wvga"

    aput-object v1, v0, v5

    const-string v1, "vga"

    aput-object v1, v0, v6

    const-string v1, "qvga"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "qcif"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_VIDEO_RESOLUTION:[Ljava/lang/String;

    .line 106
    const-string v0, "phone"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_PHONE:Ljava/lang/String;

    .line 108
    const-string v0, "external"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_MMC:Ljava/lang/String;

    .line 110
    const-string v0, "on"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_ON:Ljava/lang/String;

    .line 112
    const-string v0, "off"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_OFF:Ljava/lang/String;

    .line 114
    const-string v0, "off"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_BURSTSHOT_OFF:Ljava/lang/String;

    .line 116
    const-string v0, "on"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_BURSTSHOT_ON:Ljava/lang/String;

    .line 220
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    .line 221
    return-void
.end method

.method private compareAutoNightDetection(Ljava/lang/String;)Z
    .locals 7
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
        }
    .end annotation

    .prologue
    .line 856
    const-string v4, "CscCamera"

    const-string v5, "compareAutoNightDetection start"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    if-nez p1, :cond_0

    .line 859
    const-string v4, "CscCamera"

    const-string v5, "compareAutoNightDetection strValue is NULL"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    const/4 v4, 0x0

    .line 895
    :goto_0
    return v4

    .line 863
    :cond_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 864
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0x8

    aget-object v4, v4, v5

    const/4 v5, -0x1

    invoke-static {v0, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 866
    .local v1, "phoneIntValue":I
    packed-switch v1, :pswitch_data_0

    .line 879
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareAutoNightDetection : phoneIntValue is not supported.("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    const-string v2, "Invalid"

    .line 884
    .local v2, "phoneStringValue":Ljava/lang/String;
    :goto_1
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 885
    const-string v4, "Settings.Multimedia.Camera.AutoNightDetection"

    invoke-static {v4, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    const-string v4, "CscCamera"

    const-string v5, "compareAutoNightDetection success"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    const/4 v4, 0x1

    goto :goto_0

    .line 868
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_0
    const-string v2, "off"

    .line 869
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 871
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_1
    const-string v2, "on"

    .line 872
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 874
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_2
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareAutoNightDetection : xmlValue is not supported("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") or phoneValue is not initalized.(UnInitialized)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    const-string v2, "UnInitialized"

    .line 877
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 887
    :cond_1
    const-string v4, "Settings.Multimedia.Camera.AutoNightDetection"

    invoke-static {v4, p1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    const-string v3, "Settings.Multimedia.Camera.AutoNightDetection "

    .line 890
    .local v3, "strTrace":Ljava/lang/String;
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;

    invoke-direct {v4, p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;-><init>(Lcom/samsung/sec/android/application/csc/CscCamera;Ljava/lang/String;)V

    throw v4

    .line 866
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private compareBurstShot(Ljava/lang/String;)Z
    .locals 7
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
        }
    .end annotation

    .prologue
    .line 809
    const-string v4, "CscCamera"

    const-string v5, "compareBurstShot start"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    if-nez p1, :cond_0

    .line 812
    const-string v4, "CscCamera"

    const-string v5, "compareBurstShot strValue is NULL"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    const/4 v4, 0x0

    .line 848
    :goto_0
    return v4

    .line 816
    :cond_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 817
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x7

    aget-object v4, v4, v5

    const/4 v5, -0x1

    invoke-static {v0, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 819
    .local v1, "phoneIntValue":I
    packed-switch v1, :pswitch_data_0

    .line 832
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareBurstShot : phoneIntValue is not supported.("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    const-string v2, "Invalid"

    .line 837
    .local v2, "phoneStringValue":Ljava/lang/String;
    :goto_1
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 838
    const-string v4, "Settings.Multimedia.Camera.BurstShot"

    invoke-static {v4, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    const-string v4, "CscCamera"

    const-string v5, "compareBurstShot success"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    const/4 v4, 0x1

    goto :goto_0

    .line 821
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_0
    const-string v2, "off"

    .line 822
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 824
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_1
    const-string v2, "on"

    .line 825
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 827
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_2
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareBurstShot : xmlValue is not supported("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") or phoneValue is not initalized.(UnInitialized)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    const-string v2, "UnInitialized"

    .line 830
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 840
    :cond_1
    const-string v4, "Settings.Multimedia.Camera.BurstShot"

    invoke-static {v4, p1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    const-string v3, "Settings.Multimedia.Camera.BurstShot "

    .line 843
    .local v3, "strTrace":Ljava/lang/String;
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;

    invoke-direct {v4, p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;-><init>(Lcom/samsung/sec/android/application/csc/CscCamera;Ljava/lang/String;)V

    throw v4

    .line 819
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private compareCameraFlash(Ljava/lang/String;)Z
    .locals 7
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
        }
    .end annotation

    .prologue
    .line 499
    const-string v4, "CscCamera"

    const-string v5, "compareCameraFlash start"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    if-nez p1, :cond_0

    .line 502
    const-string v4, "CscCamera"

    const-string v5, "compareCameraFlash strValue is NULL"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const/4 v4, 0x0

    .line 538
    :goto_0
    return v4

    .line 505
    :cond_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 506
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x5

    aget-object v4, v4, v5

    const/4 v5, -0x1

    invoke-static {v0, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 507
    .local v1, "phoneIntValue":I
    packed-switch v1, :pswitch_data_0

    .line 523
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareCameraFlash : phoneIntValue is not supported.("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const-string v2, "Invalid"

    .line 528
    .local v2, "phoneStringValue":Ljava/lang/String;
    :goto_1
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 529
    const-string v4, "Settings.Multimedia.Camera.FlashSetting"

    invoke-static {v4, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    const-string v4, "CscCamera"

    const-string v5, "compareCameraFlash success"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const/4 v4, 0x1

    goto :goto_0

    .line 509
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_0
    const-string v2, "off"

    .line 510
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 512
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_1
    const-string v2, "on"

    .line 513
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 515
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_2
    const-string v2, "auto"

    .line 516
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 518
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_3
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareCameraFlash : xmlValue is not supported("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") or phoneValue is not initalized.(UnInitialized)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    const-string v2, "UnInitialized"

    .line 521
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 531
    :cond_1
    const-string v4, "Settings.Multimedia.Camera.FlashSetting"

    invoke-static {v4, p1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    const-string v3, "Settings.Multimedia.Camera.FlashSetting "

    .line 533
    .local v3, "strTrace":Ljava/lang/String;
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;

    invoke-direct {v4, p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;-><init>(Lcom/samsung/sec/android/application/csc/CscCamera;Ljava/lang/String;)V

    throw v4

    .line 507
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private compareCameraQuality(Ljava/lang/String;)Z
    .locals 7
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 546
    const-string v5, "CscCamera"

    const-string v6, "compareCameraQuality start"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    if-nez p1, :cond_0

    .line 549
    const-string v5, "CscCamera"

    const-string v6, "compareCameraQuality strValue is NULL"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    :goto_0
    return v4

    .line 553
    :cond_0
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 554
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v5, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    aget-object v4, v5, v4

    const/4 v5, -0x1

    invoke-static {v0, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 556
    .local v1, "phoneIntValue":I
    packed-switch v1, :pswitch_data_0

    .line 572
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareCameraQuality : phoneIntValue is not supported.("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    const-string v2, "Invalid"

    .line 577
    .local v2, "phoneStringValue":Ljava/lang/String;
    :goto_1
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 578
    const-string v4, "Settings.Multimedia.Camera.Quality"

    invoke-static {v4, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    const-string v4, "CscCamera"

    const-string v5, "compareCameraQuality success"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    const/4 v4, 0x1

    goto :goto_0

    .line 558
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_0
    const-string v2, "high"

    .line 559
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 561
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_1
    const-string v2, "normal"

    .line 562
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 564
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_2
    const-string v2, "low"

    .line 565
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 567
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_3
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareCameraQuality : xmlValue is not supported("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") or phoneValue is not initalized.(UnInitialized)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    const-string v2, "UnInitialized"

    .line 570
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 580
    :cond_1
    const-string v4, "Settings.Multimedia.Camera.Quality"

    invoke-static {v4, p1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    const-string v3, "Settings.Multimedia.Camera.Quality "

    .line 582
    .local v3, "strTrace":Ljava/lang/String;
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;

    invoke-direct {v4, p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;-><init>(Lcom/samsung/sec/android/application/csc/CscCamera;Ljava/lang/String;)V

    throw v4

    .line 556
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private compareCameraShutterSound(Ljava/lang/String;)Z
    .locals 7
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
        }
    .end annotation

    .prologue
    .line 595
    const-string v4, "CscCamera"

    const-string v5, "compareCameraShutterSound start"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    if-nez p1, :cond_0

    .line 598
    const-string v4, "CscCamera"

    const-string v5, "compareCameraShutterSound strValue is NULL"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    const/4 v4, 0x0

    .line 634
    :goto_0
    return v4

    .line 602
    :cond_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 603
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v4, v4, v5

    const/4 v5, -0x1

    invoke-static {v0, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 605
    .local v1, "phoneIntValue":I
    packed-switch v1, :pswitch_data_0

    .line 618
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareCameraShutterSound : phoneIntValue is not supported.("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    const-string v2, "Invalid"

    .line 623
    .local v2, "phoneStringValue":Ljava/lang/String;
    :goto_1
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 624
    const-string v4, "Settings.Multimedia.Camera.ShutterSound"

    invoke-static {v4, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    const-string v4, "CscCamera"

    const-string v5, "compareCameraShutterSound success"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const/4 v4, 0x1

    goto :goto_0

    .line 607
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_0
    const-string v2, "off"

    .line 608
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 610
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_1
    const-string v2, "on"

    .line 611
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 613
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_2
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareCameraShutterSound : xmlValue is not supported("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") or phoneValue is not initalized.(UnInitialized)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    const-string v2, "UnInitialized"

    .line 616
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 626
    :cond_1
    const-string v4, "Settings.Multimedia.Camera.ShutterSound"

    invoke-static {v4, p1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    const-string v3, "Settings.Multimedia.Camera.ShutterSound "

    .line 629
    .local v3, "strTrace":Ljava/lang/String;
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;

    invoke-direct {v4, p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;-><init>(Lcom/samsung/sec/android/application/csc/CscCamera;Ljava/lang/String;)V

    throw v4

    .line 605
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private compareMultimediaStorage(Ljava/lang/String;)Z
    .locals 7
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
        }
    .end annotation

    .prologue
    .line 763
    const-string v4, "CscCamera"

    const-string v5, "compareMultimediaStorage start"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    if-nez p1, :cond_0

    .line 766
    const-string v4, "CscCamera"

    const-string v5, "compareMultimediaStorage strValue is NULL"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    const/4 v4, 0x0

    .line 801
    :goto_0
    return v4

    .line 770
    :cond_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 771
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    const/4 v5, -0x1

    invoke-static {v0, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 773
    .local v1, "phoneIntValue":I
    packed-switch v1, :pswitch_data_0

    .line 786
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareMultimediaStorage : phoneIntValue is not supported.("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    const-string v2, "Invalid"

    .line 791
    .local v2, "phoneStringValue":Ljava/lang/String;
    :goto_1
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 792
    const-string v4, "Settings.Multimedia.DefMMStorage"

    invoke-static {v4, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    const-string v4, "CscCamera"

    const-string v5, "compareMultimediaStorage success"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    const/4 v4, 0x1

    goto :goto_0

    .line 775
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_0
    const-string v2, "phone"

    .line 776
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 778
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_1
    const-string v2, "external"

    .line 779
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 781
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_2
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareMultimediaStorage : xmlValue is not supported("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") or phoneValue is not initalized.(UnInitialized)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    const-string v2, "UnInitialized"

    .line 784
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 794
    :cond_1
    const-string v4, "Settings.Multimedia.DefMMStorage"

    invoke-static {v4, p1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    const-string v3, "Settings.Multimedia.DefMMStorage "

    .line 796
    .local v3, "strTrace":Ljava/lang/String;
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;

    invoke-direct {v4, p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;-><init>(Lcom/samsung/sec/android/application/csc/CscCamera;Ljava/lang/String;)V

    throw v4

    .line 773
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private compareVideoQuality(Ljava/lang/String;)Z
    .locals 7
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
        }
    .end annotation

    .prologue
    .line 678
    const-string v4, "CscCamera"

    const-string v5, "compareVideoQuality start"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    if-nez p1, :cond_0

    .line 681
    const-string v4, "CscCamera"

    const-string v5, "compareVideoQuality strValue is NULL"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    const/4 v4, 0x0

    .line 719
    :goto_0
    return v4

    .line 685
    :cond_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 686
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const/4 v5, -0x1

    invoke-static {v0, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 688
    .local v1, "phoneIntValue":I
    packed-switch v1, :pswitch_data_0

    .line 704
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareVideoQuality : phoneIntValue is not supported.("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    const-string v2, "Invalid"

    .line 709
    .local v2, "phoneStringValue":Ljava/lang/String;
    :goto_1
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 710
    const-string v4, "Settings.Multimedia.Video.Quality"

    invoke-static {v4, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    const-string v4, "CscCamera"

    const-string v5, "compareVideoQuality success"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    const/4 v4, 0x1

    goto :goto_0

    .line 690
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_0
    const-string v2, "high"

    .line 691
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 693
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_1
    const-string v2, "normal"

    .line 694
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 696
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_2
    const-string v2, "low"

    .line 697
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 699
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :pswitch_3
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareVideoQuality : xmlValue is not supported("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") or phoneValue is not initalized.(UnInitialized)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    const-string v2, "UnInitialized"

    .line 702
    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 712
    :cond_1
    const-string v4, "Settings.Multimedia.Video.Quality"

    invoke-static {v4, p1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    const-string v3, "Settings.Multimedia.Video.Quality "

    .line 714
    .local v3, "strTrace":Ljava/lang/String;
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;

    invoke-direct {v4, p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;-><init>(Lcom/samsung/sec/android/application/csc/CscCamera;Ljava/lang/String;)V

    throw v4

    .line 688
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private compareVideoResolution(Ljava/lang/String;)Z
    .locals 7
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    .line 727
    const-string v4, "CscCamera"

    const-string v5, "compareVideoResolution start"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    if-nez p1, :cond_0

    .line 730
    const-string v4, "CscCamera"

    const-string v5, "compareVideoResolution strValue is NULL"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    const/4 v4, 0x0

    .line 755
    :goto_0
    return v4

    .line 734
    :cond_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 735
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-static {v0, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 737
    .local v1, "phoneIntValue":I
    if-ne v1, v6, :cond_1

    .line 738
    const-string v4, "CscCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compareVideoResolution : xmlValue is not supported("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") or phoneValue is not initalized.(UnInitialized)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    const-string v2, "UnInitialized"

    .line 745
    .local v2, "phoneStringValue":Ljava/lang/String;
    :goto_1
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 746
    const-string v4, "Settings.Multimedia.Video.VideoResolution"

    invoke-static {v4, p1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    const-string v4, "CscCamera"

    const-string v5, "compareVideoResolution success"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    const/4 v4, 0x1

    goto :goto_0

    .line 742
    .end local v2    # "phoneStringValue":Ljava/lang/String;
    :cond_1
    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getResolutionString(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "phoneStringValue":Ljava/lang/String;
    goto :goto_1

    .line 748
    :cond_2
    const-string v4, "Settings.Multimedia.Video.VideoResolution"

    invoke-static {v4, p1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const-string v3, "Settings.Multimedia.Video.VideoResolution "

    .line 750
    .local v3, "strTrace":Ljava/lang/String;
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;

    invoke-direct {v4, p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;-><init>(Lcom/samsung/sec/android/application/csc/CscCamera;Ljava/lang/String;)V

    throw v4
.end method

.method private getCameraAutoNightDetection(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 1089
    const-string v0, ""

    .line 1091
    .local v0, "strValue":Ljava/lang/String;
    const-string v1, "Settings.Multimedia.Camera.AutoNightDetection"

    invoke-virtual {p1, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1092
    const-string v1, "CscCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCameraAutoNightDetectionValue="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    return-object v0
.end method

.method private getCameraBurstShot(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 1081
    const-string v0, ""

    .line 1083
    .local v0, "strValue":Ljava/lang/String;
    const-string v1, "Settings.Multimedia.Camera.BurstShot"

    invoke-virtual {p1, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1084
    const-string v1, "CscCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCameraBurstShotValue="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    return-object v0
.end method

.method private getCameraFlash(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 900
    const-string v0, ""

    .line 902
    .local v0, "strValue":Ljava/lang/String;
    const-string v1, "Settings.Multimedia.Camera.FlashSetting"

    invoke-virtual {p1, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 903
    const-string v1, "CscCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCameraFlashValue="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    return-object v0
.end method

.method private getCameraQuality(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 908
    const-string v0, ""

    .line 910
    .local v0, "strValue":Ljava/lang/String;
    const-string v1, "Settings.Multimedia.Camera.Quality"

    invoke-virtual {p1, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 911
    const-string v1, "CscCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCameraQualityValue="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    return-object v0
.end method

.method private getCameraShutterSound(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 1073
    const-string v0, ""

    .line 1075
    .local v0, "strValue":Ljava/lang/String;
    const-string v1, "Settings.Multimedia.Camera.ShutterSound"

    invoke-virtual {p1, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1076
    const-string v1, "CscCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCameraShutterSoundValue="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1077
    return-object v0
.end method

.method private getMultimediaStorage(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 940
    const-string v0, ""

    .line 942
    .local v0, "strValue":Ljava/lang/String;
    const-string v1, "Settings.Multimedia.DefMMStorage"

    invoke-virtual {p1, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 943
    const-string v1, "CscCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMultimediaStorageValue="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    return-object v0
.end method

.method public static getResolutionID(Ljava/lang/String;)I
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 1011
    const-string v0, "3264x2448"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1012
    const/4 v0, 0x0

    .line 1068
    :goto_0
    return v0

    .line 1013
    :cond_0
    const-string v0, "3264x1968"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1014
    const/4 v0, 0x1

    goto :goto_0

    .line 1015
    :cond_1
    const-string v0, "3264x1836"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1016
    const/16 v0, 0x14

    goto :goto_0

    .line 1017
    :cond_2
    const-string v0, "3072x2304"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1018
    const/4 v0, 0x2

    goto :goto_0

    .line 1019
    :cond_3
    const-string v0, "3072x1856"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1020
    const/4 v0, 0x3

    goto :goto_0

    .line 1021
    :cond_4
    const-string v0, "2592x1944"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1022
    const/16 v0, 0x19

    goto :goto_0

    .line 1023
    :cond_5
    const-string v0, "2560x1920"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1024
    const/4 v0, 0x4

    goto :goto_0

    .line 1025
    :cond_6
    const-string v0, "2560x1536"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1026
    const/4 v0, 0x5

    goto :goto_0

    .line 1027
    :cond_7
    const-string v0, "2560x1440"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1028
    const/16 v0, 0x15

    goto :goto_0

    .line 1029
    :cond_8
    const-string v0, "qxga"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1030
    const/4 v0, 0x6

    goto :goto_0

    .line 1031
    :cond_9
    const-string v0, "2048x1232"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1032
    const/4 v0, 0x7

    goto :goto_0

    .line 1033
    :cond_a
    const-string v0, "2048x1152"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1034
    const/16 v0, 0x17

    goto :goto_0

    .line 1035
    :cond_b
    const-string v0, "2048x1104"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1036
    const/16 v0, 0x1a

    goto :goto_0

    .line 1037
    :cond_c
    const-string v0, "hd1080"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1038
    const/16 v0, 0xd

    goto/16 :goto_0

    .line 1039
    :cond_d
    const-string v0, "uxga"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1040
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 1041
    :cond_e
    const-string v0, "1600x960"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1042
    const/16 v0, 0x9

    goto/16 :goto_0

    .line 1043
    :cond_f
    const-string v0, "1536x864"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1044
    const/16 v0, 0x1c

    goto/16 :goto_0

    .line 1045
    :cond_10
    const-string v0, "1392x1392"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1046
    const/16 v0, 0x18

    goto/16 :goto_0

    .line 1047
    :cond_11
    const-string v0, "4vga"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1048
    const/16 v0, 0xa

    goto/16 :goto_0

    .line 1049
    :cond_12
    const-string v0, "wvga"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1050
    const/16 v0, 0xc

    goto/16 :goto_0

    .line 1051
    :cond_13
    const-string v0, "vga"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1052
    const/16 v0, 0xb

    goto/16 :goto_0

    .line 1053
    :cond_14
    const-string v0, "1280x800"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1054
    const/16 v0, 0x16

    goto/16 :goto_0

    .line 1055
    :cond_15
    const-string v0, "hd720"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1056
    const/16 v0, 0xe

    goto/16 :goto_0

    .line 1057
    :cond_16
    const-string v0, "1248x672"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1058
    const/16 v0, 0x1d

    goto/16 :goto_0

    .line 1059
    :cond_17
    const-string v0, "720x480"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1060
    const/16 v0, 0xf

    goto/16 :goto_0

    .line 1061
    :cond_18
    const-string v0, "400x240"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1062
    const/16 v0, 0x10

    goto/16 :goto_0

    .line 1063
    :cond_19
    const-string v0, "qvga"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1064
    const/16 v0, 0x12

    goto/16 :goto_0

    .line 1065
    :cond_1a
    const-string v0, "qcif"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1066
    const/16 v0, 0x13

    goto/16 :goto_0

    .line 1068
    :cond_1b
    const/4 v0, -0x1

    goto/16 :goto_0
.end method

.method public static getResolutionString(I)Ljava/lang/String;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 948
    packed-switch p0, :pswitch_data_0

    .line 1006
    :pswitch_0
    const-string v0, "Invalid"

    :goto_0
    return-object v0

    .line 950
    :pswitch_1
    const-string v0, "3264x2448"

    goto :goto_0

    .line 952
    :pswitch_2
    const-string v0, "3264x1968"

    goto :goto_0

    .line 954
    :pswitch_3
    const-string v0, "3264x1836"

    goto :goto_0

    .line 956
    :pswitch_4
    const-string v0, "3072x2304"

    goto :goto_0

    .line 958
    :pswitch_5
    const-string v0, "3072x1856"

    goto :goto_0

    .line 960
    :pswitch_6
    const-string v0, "2592x1944"

    goto :goto_0

    .line 962
    :pswitch_7
    const-string v0, "2560x1920"

    goto :goto_0

    .line 964
    :pswitch_8
    const-string v0, "2560x1536"

    goto :goto_0

    .line 966
    :pswitch_9
    const-string v0, "2560x1440"

    goto :goto_0

    .line 968
    :pswitch_a
    const-string v0, "qxga"

    goto :goto_0

    .line 970
    :pswitch_b
    const-string v0, "2048x1232"

    goto :goto_0

    .line 972
    :pswitch_c
    const-string v0, "2048x1152"

    goto :goto_0

    .line 974
    :pswitch_d
    const-string v0, "2048x1104"

    goto :goto_0

    .line 976
    :pswitch_e
    const-string v0, "hd1080"

    goto :goto_0

    .line 978
    :pswitch_f
    const-string v0, "uxga"

    goto :goto_0

    .line 980
    :pswitch_10
    const-string v0, "1600x960"

    goto :goto_0

    .line 982
    :pswitch_11
    const-string v0, "1536x864"

    goto :goto_0

    .line 984
    :pswitch_12
    const-string v0, "1392x1392"

    goto :goto_0

    .line 986
    :pswitch_13
    const-string v0, "4vga"

    goto :goto_0

    .line 988
    :pswitch_14
    const-string v0, "wvga"

    goto :goto_0

    .line 990
    :pswitch_15
    const-string v0, "vga"

    goto :goto_0

    .line 992
    :pswitch_16
    const-string v0, "1280x800"

    goto :goto_0

    .line 994
    :pswitch_17
    const-string v0, "hd720"

    goto :goto_0

    .line 996
    :pswitch_18
    const-string v0, "1248x672"

    goto :goto_0

    .line 998
    :pswitch_19
    const-string v0, "720x480"

    goto :goto_0

    .line 1000
    :pswitch_1a
    const-string v0, "400x240"

    goto :goto_0

    .line 1002
    :pswitch_1b
    const-string v0, "qvga"

    goto :goto_0

    .line 1004
    :pswitch_1c
    const-string v0, "qcif"

    goto :goto_0

    .line 948
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_f
        :pswitch_10
        :pswitch_13
        :pswitch_15
        :pswitch_14
        :pswitch_e
        :pswitch_17
        :pswitch_19
        :pswitch_1a
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
        :pswitch_3
        :pswitch_9
        :pswitch_16
        :pswitch_c
        :pswitch_12
        :pswitch_6
        :pswitch_d
        :pswitch_0
        :pswitch_11
        :pswitch_18
    .end packed-switch
.end method

.method private getVideoQuality(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 924
    const-string v0, ""

    .line 926
    .local v0, "strValue":Ljava/lang/String;
    const-string v1, "Settings.Multimedia.Video.Quality"

    invoke-virtual {p1, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 927
    const-string v1, "CscCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVideoQualityValue="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    return-object v0
.end method

.method private getVideoResolution(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 932
    const-string v0, ""

    .line 934
    .local v0, "strValue":Ljava/lang/String;
    const-string v1, "Settings.Multimedia.Video.VideoResolution"

    invoke-virtual {p1, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 935
    const-string v1, "CscCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVideoResolutionValue="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    return-object v0
.end method

.method private updateCameraAutoNightDetection(Ljava/lang/String;)Z
    .locals 5
    .param p1, "strValue"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 472
    const-string v3, "CscCamera"

    const-string v4, "updateCameraAutoNightDetection start"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    if-nez p1, :cond_0

    .line 475
    const-string v3, "CscCamera"

    const-string v4, "updateCameraAutoNightDetection strValue is NULL"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    :goto_0
    return v2

    .line 478
    :cond_0
    const-string v3, "off"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 479
    const/4 v1, 0x0

    .line 487
    .local v1, "value":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 488
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 490
    const/4 v2, 0x1

    goto :goto_0

    .line 480
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "value":I
    :cond_1
    const-string v3, "on"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 481
    const/4 v1, 0x1

    .restart local v1    # "value":I
    goto :goto_1

    .line 483
    .end local v1    # "value":I
    :cond_2
    const-string v3, "CscCamera"

    const-string v4, "updateCameraAutoNightDetection strValue is not supported"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateCameraBurstShot(Ljava/lang/String;)Z
    .locals 5
    .param p1, "strValue"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 448
    const-string v3, "CscCamera"

    const-string v4, "updateCameraBurstShot start"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    if-nez p1, :cond_0

    .line 451
    const-string v3, "CscCamera"

    const-string v4, "updateCameraBurstShot strValue is NULL"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    :goto_0
    return v2

    .line 454
    :cond_0
    const-string v3, "off"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 455
    const/4 v1, 0x0

    .line 463
    .local v1, "value":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 464
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 466
    const/4 v2, 0x1

    goto :goto_0

    .line 456
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "value":I
    :cond_1
    const-string v3, "on"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 457
    const/4 v1, 0x1

    .restart local v1    # "value":I
    goto :goto_1

    .line 459
    .end local v1    # "value":I
    :cond_2
    const-string v3, "CscCamera"

    const-string v4, "updateCameraBurstShot strValue is not supported"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateCameraFlash(Ljava/lang/String;)Z
    .locals 5
    .param p1, "strValue"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 261
    const-string v3, "CscCamera"

    const-string v4, "updateCameraFlash start"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    if-nez p1, :cond_0

    .line 264
    const-string v3, "CscCamera"

    const-string v4, "updateCameraFlash strValue is NULL"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :goto_0
    return v2

    .line 267
    :cond_0
    const-string v3, "off"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 268
    const/4 v1, 0x0

    .line 278
    .local v1, "value":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 279
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 281
    const/4 v2, 0x1

    goto :goto_0

    .line 269
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "value":I
    :cond_1
    const-string v3, "on"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 270
    const/4 v1, 0x1

    .restart local v1    # "value":I
    goto :goto_1

    .line 271
    .end local v1    # "value":I
    :cond_2
    const-string v3, "auto"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 272
    const/4 v1, 0x2

    .restart local v1    # "value":I
    goto :goto_1

    .line 274
    .end local v1    # "value":I
    :cond_3
    const-string v3, "CscCamera"

    const-string v4, "updateCameraFlash strValue is not supported"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateCameraQuality(Ljava/lang/String;)Z
    .locals 5
    .param p1, "strValue"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 288
    const-string v3, "CscCamera"

    const-string v4, "updateCameraQuality start"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    if-nez p1, :cond_0

    .line 291
    const-string v3, "CscCamera"

    const-string v4, "updateCameraQuality strValue is NULL"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    :goto_0
    return v2

    .line 294
    :cond_0
    const-string v3, "high"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 295
    const/4 v1, 0x0

    .line 305
    .local v1, "value":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 306
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    aget-object v2, v3, v2

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 308
    const/4 v2, 0x1

    goto :goto_0

    .line 296
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "value":I
    :cond_1
    const-string v3, "normal"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 297
    const/4 v1, 0x1

    .restart local v1    # "value":I
    goto :goto_1

    .line 298
    .end local v1    # "value":I
    :cond_2
    const-string v3, "low"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 299
    const/4 v1, 0x2

    .restart local v1    # "value":I
    goto :goto_1

    .line 301
    .end local v1    # "value":I
    :cond_3
    const-string v3, "CscCamera"

    const-string v4, "updateCameraQuality strValue is not supported"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateCameraShutterSound(Ljava/lang/String;)Z
    .locals 5
    .param p1, "strValue"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 424
    const-string v3, "CscCamera"

    const-string v4, "updateCameraShutterSound start"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    if-nez p1, :cond_0

    .line 427
    const-string v3, "CscCamera"

    const-string v4, "updateCameraShutterSound strValue is NULL"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :goto_0
    return v2

    .line 430
    :cond_0
    const-string v3, "off"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 431
    const/4 v1, 0x0

    .line 439
    .local v1, "value":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 440
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 442
    const/4 v2, 0x1

    goto :goto_0

    .line 432
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "value":I
    :cond_1
    const-string v3, "on"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 433
    const/4 v1, 0x1

    .restart local v1    # "value":I
    goto :goto_1

    .line 435
    .end local v1    # "value":I
    :cond_2
    const-string v3, "CscCamera"

    const-string v4, "updateCameraShutterSound strValue is not supported"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateMultimediaStorage(Ljava/lang/String;)Z
    .locals 5
    .param p1, "strValue"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 397
    const/4 v1, -0x1

    .line 399
    .local v1, "value":I
    const-string v3, "CscCamera"

    const-string v4, "updateMultimediaStorage start"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    if-nez p1, :cond_0

    .line 402
    const-string v3, "CscCamera"

    const-string v4, "updateMultimediaStorage strValue is NULL"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :goto_0
    return v2

    .line 406
    :cond_0
    const-string v3, "phone"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 407
    const/4 v1, 0x0

    .line 415
    :goto_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 416
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 418
    const/4 v2, 0x1

    goto :goto_0

    .line 408
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    const-string v3, "external"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 409
    const/4 v1, 0x1

    goto :goto_1

    .line 411
    :cond_2
    const-string v3, "CscCamera"

    const-string v4, "updateMultimediaStorage strValue is not supported"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateVideoQuality(Ljava/lang/String;)Z
    .locals 5
    .param p1, "strValue"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 342
    const/4 v1, -0x1

    .line 344
    .local v1, "value":I
    const-string v3, "CscCamera"

    const-string v4, "updateVideoQuality start"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    if-nez p1, :cond_0

    .line 347
    const-string v3, "CscCamera"

    const-string v4, "updateVideoQuality strValue is NULL"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :goto_0
    return v2

    .line 350
    :cond_0
    const-string v3, "high"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 351
    const/4 v1, 0x0

    .line 361
    :goto_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 362
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 364
    const/4 v2, 0x1

    goto :goto_0

    .line 352
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    const-string v3, "normal"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 353
    const/4 v1, 0x1

    goto :goto_1

    .line 354
    :cond_2
    const-string v3, "low"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 355
    const/4 v1, 0x2

    goto :goto_1

    .line 357
    :cond_3
    const-string v3, "CscCamera"

    const-string v4, "updateVideoQuality strValue is not supported"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateVideoResolution(Ljava/lang/String;)Z
    .locals 9
    .param p1, "strValue"    # Ljava/lang/String;

    .prologue
    .line 369
    const-string v7, "CscCamera"

    const-string v8, "updateVideoResolution start"

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    if-nez p1, :cond_0

    .line 372
    const-string v7, "CscCamera"

    const-string v8, "updateVideoResolution strValue is NULL"

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const/4 v7, 0x0

    .line 392
    :goto_0
    return v7

    .line 376
    :cond_0
    const/4 v6, 0x0

    .line 377
    .local v6, "ret":Z
    const/4 v3, -0x1

    .line 378
    .local v3, "intValue":I
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->VALUE_VIDEO_RESOLUTION:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v0, v2

    .line 379
    .local v5, "resolution":Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 380
    invoke-static {p1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getResolutionID(Ljava/lang/String;)I

    move-result v3

    .line 381
    const/4 v6, 0x1

    .line 385
    .end local v5    # "resolution":Ljava/lang/String;
    :cond_1
    if-eqz v6, :cond_2

    if-gez v3, :cond_3

    .line 386
    :cond_2
    const-string v7, "CscCamera"

    const-string v8, "updateVideoResolution strValue is not supported"

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :cond_3
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 390
    .local v1, "cr":Landroid/content/ContentResolver;
    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCamera;->CSC_KEY_PRFS:[Ljava/lang/String;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    invoke-static {v1, v7, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 392
    const/4 v7, 0x1

    goto :goto_0

    .line 378
    .end local v1    # "cr":Landroid/content/ContentResolver;
    .restart local v5    # "resolution":Ljava/lang/String;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1175
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Multimedia.Camera.Resolution"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1176
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1097
    const-string v0, "NOERROR"

    .line 1098
    .local v0, "answer":Ljava/lang/String;
    const-string v3, "CscCamera"

    const-string v4, "CscCamera compare()"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1102
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 1104
    .local v1, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    if-nez v1, :cond_0

    .line 1105
    const-string v3, "CscCamera"

    const-string v4, "CscCamera::compare memory alloc fail"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    const-string v3, "CscCamera : CscCamera::compare memory alloc fail"

    invoke-static {v3}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 1108
    const-string v3, "CAMERA"

    .line 1168
    :goto_0
    return-object v3

    .line 1111
    :cond_0
    const-string v3, "Settings.Multimedia"

    invoke-virtual {v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 1112
    const-string v3, "CscCamera"

    const-string v4, "<Multimedia> is not exist."

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1113
    const-string v3, "NOERROR"

    goto :goto_0

    .line 1118
    :cond_1
    :try_start_0
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getCameraFlash(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera;->compareCameraFlash(Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1124
    :goto_1
    :try_start_1
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getCameraQuality(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera;->compareCameraQuality(Ljava/lang/String;)Z
    :try_end_1
    .catch Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1129
    :goto_2
    :try_start_2
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getCameraShutterSound(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera;->compareCameraShutterSound(Ljava/lang/String;)Z
    :try_end_2
    .catch Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1134
    :goto_3
    :try_start_3
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getVideoQuality(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera;->compareVideoQuality(Ljava/lang/String;)Z
    :try_end_3
    .catch Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1140
    :goto_4
    :try_start_4
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getVideoResolution(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera;->compareVideoResolution(Ljava/lang/String;)Z
    :try_end_4
    .catch Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException; {:try_start_4 .. :try_end_4} :catch_4

    .line 1146
    :goto_5
    :try_start_5
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getMultimediaStorage(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera;->compareMultimediaStorage(Ljava/lang/String;)Z
    :try_end_5
    .catch Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException; {:try_start_5 .. :try_end_5} :catch_5

    .line 1152
    :goto_6
    :try_start_6
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getCameraBurstShot(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera;->compareBurstShot(Ljava/lang/String;)Z
    :try_end_6
    .catch Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException; {:try_start_6 .. :try_end_6} :catch_6

    .line 1157
    :goto_7
    :try_start_7
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getCameraAutoNightDetection(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/sec/android/application/csc/CscCamera;->compareAutoNightDetection(Ljava/lang/String;)Z
    :try_end_7
    .catch Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException; {:try_start_7 .. :try_end_7} :catch_7

    .line 1161
    :goto_8
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 1162
    const-string v0, "NOERROR"

    .line 1167
    :goto_9
    const-string v3, "CscCamera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Camera compare result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v0

    .line 1168
    goto :goto_0

    .line 1119
    :catch_0
    move-exception v2

    .line 1120
    .local v2, "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->getTrace()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1125
    .end local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    :catch_1
    move-exception v2

    .line 1126
    .restart local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->getTrace()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1130
    .end local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    :catch_2
    move-exception v2

    .line 1131
    .restart local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->getTrace()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1135
    .end local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    :catch_3
    move-exception v2

    .line 1136
    .restart local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->getTrace()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 1141
    .end local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    :catch_4
    move-exception v2

    .line 1142
    .restart local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->getTrace()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1147
    .end local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    :catch_5
    move-exception v2

    .line 1148
    .restart local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->getTrace()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    .line 1153
    .end local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    :catch_6
    move-exception v2

    .line 1154
    .restart local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->getTrace()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 1158
    .end local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    :catch_7
    move-exception v2

    .line 1159
    .restart local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->getTrace()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    .line 1164
    .end local v2    # "e":Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CscCamera : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    goto/16 :goto_9
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 0
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 1184
    return-void
.end method

.method public update()V
    .locals 4

    .prologue
    .line 225
    const-string v0, ""

    .line 227
    .local v0, "answer":Ljava/lang/String;
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscCamera;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 229
    .local v1, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    if-nez v1, :cond_0

    .line 230
    const-string v2, "CscCamera"

    const-string v3, "CscCamera::update memory alloc fail"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :goto_0
    return-void

    .line 234
    :cond_0
    const-string v2, "CscCamera"

    const-string v3, "update start"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v2, "Settings.Multimedia"

    invoke-virtual {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 237
    const-string v2, "CscCamera"

    const-string v3, "<Multimedia> is not exist."

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 242
    :cond_1
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getCameraFlash(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCamera;->updateCameraFlash(Ljava/lang/String;)Z

    .line 243
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getCameraQuality(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCamera;->updateCameraQuality(Ljava/lang/String;)Z

    .line 244
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getVideoQuality(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCamera;->updateVideoQuality(Ljava/lang/String;)Z

    .line 245
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getVideoResolution(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCamera;->updateVideoResolution(Ljava/lang/String;)Z

    .line 246
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getMultimediaStorage(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCamera;->updateMultimediaStorage(Ljava/lang/String;)Z

    .line 247
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getCameraShutterSound(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCamera;->updateCameraShutterSound(Ljava/lang/String;)Z

    .line 248
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getCameraBurstShot(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCamera;->updateCameraBurstShot(Ljava/lang/String;)Z

    .line 249
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscCamera;->getCameraAutoNightDetection(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCamera;->updateCameraAutoNightDetection(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public updateForSubUser()V
    .locals 0

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscCamera;->update()V

    .line 254
    return-void
.end method
