.class public Lcom/samsung/sec/android/application/csc/CscCompareService;
.super Landroid/app/Service;
.source "CscCompareService.java"


# instance fields
.field failCount:I

.field mComparables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscTgBase;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

.field mResultByte:Ljava/nio/ByteBuffer;

.field mResultByteSize:I

.field mResultString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 49
    const/16 v0, 0x1c2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByte:Ljava/nio/ByteBuffer;

    .line 51
    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByteSize:I

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    .line 55
    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->failCount:I

    .line 382
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscCompareService$2;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscCompareService$2;-><init>(Lcom/samsung/sec/android/application/csc/CscCompareService;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mHandler:Landroid/os/Handler;

    .line 402
    return-void
.end method

.method private MakeRespData()V
    .locals 3

    .prologue
    .line 142
    const-string v1, "CscCompareService"

    const-string v2, "MakeRespData"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const/4 v0, 0x1

    .line 145
    .local v0, "result":C
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 146
    const/4 v0, 0x0

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByte:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 152
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByte:Ljava/nio/ByteBuffer;

    int-to-byte v2, v0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 153
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByte:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->failCount:I

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 154
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByte:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 155
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByteSize:I

    .line 156
    return-void
.end method

.method private convertResultToItemID(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x80

    .line 163
    const/16 v0, 0x80

    .line 164
    .local v0, "MAX_RESULT_LENGTH":I
    const-string v2, "CscCompareService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Compare Result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v2, "NOERROR"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 304
    .end local p1    # "result":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 170
    .restart local p1    # "result":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/samsung/sec/android/application/csc/Log;->endLog()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->addFailLogLine(Ljava/lang/String;)V

    .line 172
    const-string v2, "SettingsData.ForceLiveWallpaper"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 173
    const-string p1, "WALLPAPER"

    goto :goto_0

    .line 174
    :cond_2
    const-string v2, "Settings.Messages.Voicemail.TelNum"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 175
    const-string p1, "VOICEMAIL"

    goto :goto_0

    .line 178
    :cond_3
    const-string v2, "Settings.Main.Phone.DefLanguage"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 179
    const-string p1, "LANGUAGE"

    goto :goto_0

    .line 180
    :cond_4
    const-string v2, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 181
    const-string p1, "DATEFORM"

    goto :goto_0

    .line 182
    :cond_5
    const-string v2, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 183
    const-string p1, "TIMEFORM"

    goto :goto_0

    .line 184
    :cond_6
    const-string v2, "Settings.Main.Sound.RingTone.src"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 185
    const-string p1, "RINGTONE"

    goto :goto_0

    .line 186
    :cond_7
    const-string v2, "Settings.Main.Sound.MessageTone.src"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 187
    const-string p1, "MSGTONE"

    goto :goto_0

    .line 188
    :cond_8
    const-string v2, "SettingsData.SDNotification"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 189
    const-string p1, "SDNOTI"

    goto :goto_0

    .line 190
    :cond_9
    const-string v2, "Settings.Main.Sound.ExtraSound.CallEndTone"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 191
    const-string p1, "CALLENDT"

    goto/16 :goto_0

    .line 192
    :cond_a
    const-string v2, "Settings.Main.Phone.AlertOnCall"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 193
    const-string p1, "ALERTCALL"

    goto/16 :goto_0

    .line 194
    :cond_b
    const-string v2, "Settings.Main.Phone.AutoRedial"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 195
    const-string p1, "AUTOREDIAL"

    goto/16 :goto_0

    .line 196
    :cond_c
    const-string v2, "Settings.Main.Sound.ExtraSound.ConnectTone"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 197
    const-string p1, "CONNTONE"

    goto/16 :goto_0

    .line 198
    :cond_d
    const-string v2, "Settings.Main.Sound.ExtraSound.MinuteMind"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 199
    const-string p1, "MINMIND"

    goto/16 :goto_0

    .line 200
    :cond_e
    const-string v2, "Settings.Main.Phone.CallNoiseReduction"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 201
    const-string p1, "NOISERED"

    goto/16 :goto_0

    .line 202
    :cond_f
    const-string v2, "Settings.Main.Phone.UseExtraVolume"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 203
    const-string p1, "EXTRAVOL"

    goto/16 :goto_0

    .line 204
    :cond_10
    const-string v2, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 205
    const-string p1, "BACKLIGHT"

    goto/16 :goto_0

    .line 208
    :cond_11
    const-string v2, "Settings.Browser."

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 209
    const-string p1, "BOOKMARK"

    goto/16 :goto_0

    .line 210
    :cond_12
    const-string v2, "BrowserData.HomePage.URL"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 211
    const-string p1, "HOMEPAGE"

    goto/16 :goto_0

    .line 215
    :cond_13
    const-string v2, "GeneralInfo"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 216
    const-string p1, "CONNECTION"

    goto/16 :goto_0

    .line 218
    :cond_14
    const-string v2, "Settings.Connections.ProfileHandle"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 219
    const-string p1, "APNHANDLE"

    goto/16 :goto_0

    .line 221
    :cond_15
    const-string v2, "Settings.Connections.Profile"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 222
    const-string p1, "APNPROFILE"

    goto/16 :goto_0

    .line 224
    :cond_16
    const-string v2, "Connection."

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 225
    const-string p1, "APNSETTING"

    goto/16 :goto_0

    .line 229
    :cond_17
    const-string v2, "CONTACTS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 230
    const-string p1, "CONTACTS"

    goto/16 :goto_0

    .line 233
    :cond_18
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1a

    .line 234
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 235
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MESSAGE_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 233
    :cond_19
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 240
    :cond_1a
    const/4 v1, 0x0

    :goto_2
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_CSC:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1c

    .line 241
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_CSC:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 242
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EmailEas_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 240
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 246
    :cond_1c
    const-string v2, "Network.gps_xtra"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 247
    const-string p1, "GPSXTRA"

    goto/16 :goto_0

    .line 249
    :cond_1d
    const-string v2, "Network.cb_enable"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 250
    const-string p1, "CBENABLE"

    goto/16 :goto_0

    .line 252
    :cond_1e
    const-string v2, "Settings.Main.Network."

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 253
    const-string p1, "ECALL"

    goto/16 :goto_0

    .line 257
    :cond_1f
    const-string v2, "OPERATOR_CONTENTS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 258
    const-string p1, "OPCONTENTS"

    goto/16 :goto_0

    .line 260
    :cond_20
    const-string v2, "SDCARD_CONTENTS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 261
    const-string p1, "SDCONTENTS"

    goto/16 :goto_0

    .line 278
    :cond_21
    const-string v2, "WbAmrCodec"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 279
    const-string p1, "WBAMR_1"

    goto/16 :goto_0

    .line 281
    :cond_22
    const-string v2, "WBAMR_SETTINGS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 282
    const-string p1, "WBAMR_2"

    goto/16 :goto_0

    .line 285
    :cond_23
    const-string v2, "Settings.Multimedia.Camera.Quality"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 286
    const-string p1, "CAMERAQUALITY"

    goto/16 :goto_0

    .line 287
    :cond_24
    const-string v2, "Settings.Multimedia.Camera.Resolution"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 288
    const-string p1, "CAMERARESOLUTION"

    goto/16 :goto_0

    .line 289
    :cond_25
    const-string v2, "Settings.Multimedia.Video.Quality"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 290
    const-string p1, "VIDEOQUALITY"

    goto/16 :goto_0

    .line 291
    :cond_26
    const-string v2, "Settings.Multimedia.Video.VideoResolution"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 292
    const-string p1, "VIDEORESOLUTION"

    goto/16 :goto_0

    .line 293
    :cond_27
    const-string v2, "Settings.Multimedia.DefMMStorage"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 294
    const-string p1, "CAMSTORAGE"

    goto/16 :goto_0

    .line 297
    :cond_28
    const-string v2, "CALENDAR"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 298
    const-string p1, "CALENDAR"

    goto/16 :goto_0

    .line 301
    :cond_29
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v5, :cond_0

    .line 304
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0
.end method


# virtual methods
.method doCompare()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 96
    const-string v5, ""

    iput-object v5, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    .line 97
    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->failCount:I

    .line 98
    const/4 v0, 0x0

    .line 100
    .local v0, "arrayIndex":I
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->isEnableLog()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 101
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->addUnusedItems(Landroid/content/Context;)V

    .line 104
    :cond_0
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mComparables:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/sec/android/application/csc/Comparable;

    .line 105
    .local v3, "u":Lcom/samsung/sec/android/application/csc/Comparable;
    const-string v5, "CscCompareService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mComparables start. ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->isEnableLog()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 108
    invoke-static {}, Lcom/samsung/sec/android/application/csc/Log;->startLog()V

    .line 111
    :cond_1
    invoke-interface {v3}, Lcom/samsung/sec/android/application/csc/Comparable;->compare()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/sec/android/application/csc/CscCompareService;->convertResultToItemID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 113
    .local v2, "resultString":Ljava/lang/String;
    const-string v5, "NOERROR"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 114
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 115
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    .line 116
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    .line 117
    iget v5, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->failCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->failCount:I

    .line 120
    :cond_3
    const-string v5, "CscCompareService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mComparables done. ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    add-int/lit8 v0, v0, 0x1

    .line 122
    goto/16 :goto_0

    .line 125
    .end local v2    # "resultString":Ljava/lang/String;
    .end local v3    # "u":Lcom/samsung/sec/android/application/csc/Comparable;
    :cond_4
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscCompareService;->MakeRespData()V

    .line 128
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscCompareService;->sendCompareResult()V

    .line 131
    iget v5, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->failCount:I

    if-nez v5, :cond_5

    const/4 v4, 0x1

    :cond_5
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->endLog(ZLjava/lang/String;)V

    .line 134
    const-string v4, "CscCompareService"

    const-string v5, "End of compare"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-direct {v0, p0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 68
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 73
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->disconnectFromRilService()V

    .line 74
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 80
    const-string v0, "CscCompareService"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-static {p0}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscTgManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mComparables:Ljava/util/ArrayList;

    .line 84
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscCompareService;->startCompareThread()V

    .line 85
    return-void
.end method

.method public sendCompareResult()V
    .locals 5

    .prologue
    const/4 v3, 0x6

    .line 366
    const-string v1, "CscCompareService"

    const-string v2, "sendCompareResult"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    invoke-static {p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->isWifiOnly(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 371
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscCompareService;->sendCompareResultWifi()V

    .line 379
    :goto_0
    return-void

    .line 373
    :cond_0
    new-instance v0, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    invoke-direct {v0, v3, v3}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 375
    .local v0, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByte:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByteSize:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData([BII)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 376
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x7ce

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0
.end method

.method public sendCompareResultWifi()V
    .locals 10

    .prologue
    .line 313
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 314
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 316
    .local v1, "dos":Ljava/io/DataOutputStream;
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6}, Ljava/lang/String;-><init>()V

    .line 317
    .local v6, "tfg_prodcode":Ljava/lang/String;
    const/4 v5, 0x0

    .line 319
    .local v5, "tfg_len":I
    const-string v7, "CscCompareService"

    const-string v8, "sendCompareResult"

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :try_start_0
    iget v7, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByteSize:I

    add-int/lit8 v3, v7, 0x4

    .line 323
    .local v3, "fileSize":I
    const/4 v7, 0x6

    invoke-virtual {v1, v7}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 324
    const/4 v7, 0x6

    invoke-virtual {v1, v7}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 325
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 326
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByte:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    const/4 v8, 0x0

    iget v9, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultByteSize:I

    invoke-virtual {v1, v7, v8, v9}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 338
    :goto_0
    const-string v7, "CscCompareService"

    const-string v8, "start sendcompare result for WIFI  "

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    new-instance v4, Landroid/content/Intent;

    const-string v7, "com.sec.factory.aporiented.athandler.atpreconfg"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 342
    .local v4, "i":Landroid/content/Intent;
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscCompareService;->mResultString:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 343
    const-string v7, "code"

    const-string v8, "NG"

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    const-string v7, "CscCompareService"

    const-string v8, "check result : fail ->  send NG "

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :goto_1
    invoke-virtual {p0, v4}, Lcom/samsung/sec/android/application/csc/CscCompareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 363
    .end local v3    # "fileSize":I
    .end local v4    # "i":Landroid/content/Intent;
    :goto_2
    return-void

    .line 332
    .restart local v3    # "fileSize":I
    :catch_0
    move-exception v2

    .line 334
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 327
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fileSize":I
    :catch_1
    move-exception v2

    .line 331
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 332
    :catch_2
    move-exception v2

    .line 334
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 330
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 331
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 335
    :goto_3
    throw v7

    .line 332
    :catch_3
    move-exception v2

    .line 334
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 347
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v3    # "fileSize":I
    .restart local v4    # "i":Landroid/content/Intent;
    :cond_0
    const-string v7, "TFG"

    const-string v8, "ro.csc.sales_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 348
    const-string v7, "ril.product_code"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 349
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v5

    .line 350
    const-string v7, "CscCompareService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Prodcode = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    if-lez v5, :cond_1

    .line 352
    const-string v7, "code"

    add-int/lit8 v8, v5, -0x3

    invoke-virtual {v6, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    :goto_4
    const-string v7, "CscCompareService"

    const-string v8, "check result : success -> send sales code "

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 354
    :cond_1
    const-string v7, "code"

    const-string v8, "NG"

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_4

    .line 357
    :cond_2
    const-string v7, "code"

    const-string v8, "ro.csc.sales_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_4
.end method

.method startCompareThread()V
    .locals 2

    .prologue
    .line 88
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscCompareService$1;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscCompareService$1;-><init>(Lcom/samsung/sec/android/application/csc/CscCompareService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 93
    return-void
.end method
