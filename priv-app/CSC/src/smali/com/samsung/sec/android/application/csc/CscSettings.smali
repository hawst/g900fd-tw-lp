.class public Lcom/samsung/sec/android/application/csc/CscSettings;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscSettings.java"


# static fields
.field private static GeneralMCCMNCs:[Ljava/lang/String;

.field private static GeneralNWNames:[Ljava/lang/String;

.field private static WifiEAPMethods:[Ljava/lang/String;

.field private static WifiKeyMgmts:[Ljava/lang/String;

.field private static WifiNWNames:[Ljava/lang/String;

.field private static WifiPrioritys:[Ljava/lang/String;

.field private static WifiSSIDs:[Ljava/lang/String;


# instance fields
.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final PATH_OPERATORS_DEFAULTRINGER:Ljava/lang/String;

.field private final SW_CONFIG_FILE:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private final USBMENUSEL_PATH:Ljava/lang/String;

.field private final askon:[B

.field editor:Landroid/content/SharedPreferences$Editor;

.field private final kies:[B

.field private mContext:Landroid/content/Context;

.field private mMountService:Landroid/os/storage/IMountService;

.field private mParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mResolver:Landroid/content/ContentResolver;

.field private final mtp:[B

.field prefs:Landroid/content/SharedPreferences;

.field private final ums:[B

.field private final vtp:[B


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 276
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 66
    const-string v0, "CSCSettings"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->TAG:Ljava/lang/String;

    .line 68
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 221
    const-string v0, "/system/SW_Configuration.xml"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->SW_CONFIG_FILE:Ljava/lang/String;

    .line 242
    const-string v0, "Operators.DefaultRinger"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->PATH_OPERATORS_DEFAULTRINGER:Ljava/lang/String;

    .line 246
    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 250
    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mMountService:Landroid/os/storage/IMountService;

    .line 3073
    const-string v0, "/sys/class/sec/switch/UsbMenuSel"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->USBMENUSEL_PATH:Ljava/lang/String;

    .line 3075
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->kies:[B

    .line 3079
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mtp:[B

    .line 3083
    new-array v0, v1, [B

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->ums:[B

    .line 3087
    new-array v0, v1, [B

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->vtp:[B

    .line 3091
    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->askon:[B

    .line 277
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    .line 278
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    .line 279
    const-string v0, "USER_PREFERENCES"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    .line 280
    return-void

    .line 3075
    :array_0
    .array-data 1
        0x4bt
        0x49t
        0x45t
        0x53t
        0x0t
    .end array-data

    .line 3079
    nop

    :array_1
    .array-data 1
        0x4dt
        0x54t
        0x50t
        0x0t
    .end array-data

    .line 3083
    :array_2
    .array-data 1
        0x55t
        0x4dt
        0x53t
        0x0t
    .end array-data

    .line 3087
    :array_3
    .array-data 1
        0x56t
        0x54t
        0x50t
        0x0t
    .end array-data

    .line 3091
    :array_4
    .array-data 1
        0x41t
        0x53t
        0x4bt
        0x4ft
        0x4et
        0x0t
    .end array-data
.end method

.method private updateCountryCertInfo()V
    .locals 17

    .prologue
    .line 1372
    const-string v14, "CSCSettings"

    const-string v15, "Setting Country Certification info"

    invoke-static {v14, v15}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1374
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v14, :cond_0

    .line 1375
    new-instance v14, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v14, v15}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 1377
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "Settings.Main.CountryCert.CertDisplayStatys"

    invoke-virtual {v14, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1378
    .local v2, "certDisplay":Ljava/lang/String;
    if-eqz v2, :cond_8

    const-string v14, "On"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1379
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 1380
    .local v3, "certInfo":Ljava/lang/StringBuffer;
    const/4 v12, 0x0

    .line 1381
    .local v12, "certValues":Ljava/lang/String;
    const/4 v10, 0x0

    .line 1382
    .local v10, "certTRA":Ljava/lang/String;
    const/4 v9, 0x0

    .line 1383
    .local v9, "certTA":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1385
    .local v1, "certCountryName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "Settings.Main.CountryCert.CertCountryName"

    invoke-virtual {v14, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1387
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "Settings.Main.CountryCert"

    invoke-virtual {v14, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 1388
    .local v6, "certNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "DetailDesc"

    invoke-virtual {v14, v6, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    .line 1389
    .local v7, "certNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v6, :cond_4

    .line 1391
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v14

    if-ge v13, v14, :cond_4

    .line 1392
    invoke-interface {v7, v13}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 1394
    .local v8, "certNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "CertTag"

    invoke-virtual {v14, v8, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 1395
    .local v4, "certName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v14, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    .line 1396
    .local v5, "certNames":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 1397
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "CertValue"

    invoke-virtual {v14, v8, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v11

    .line 1398
    .local v11, "certValue":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v14, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v12

    .line 1399
    if-eqz v12, :cond_1

    .line 1400
    const-string v14, "CSCSettings"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " ** getCountryCert ("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ") : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1402
    const-string v14, "TRA ID"

    invoke-virtual {v5, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_2

    .line 1403
    move-object v10, v12

    .line 1391
    .end local v11    # "certValue":Lorg/w3c/dom/Node;
    :cond_1
    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 1404
    .restart local v11    # "certValue":Lorg/w3c/dom/Node;
    :cond_2
    const-string v14, "TA"

    invoke-virtual {v5, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_3

    .line 1405
    move-object v9, v12

    goto :goto_1

    .line 1406
    :cond_3
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v14

    if-lez v14, :cond_1

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v14

    if-lez v14, :cond_1

    .line 1407
    const-string v14, "CSCSettings"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "add certInfo : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ","

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1408
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ";"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1415
    .end local v4    # "certName":Lorg/w3c/dom/Node;
    .end local v5    # "certNames":Ljava/lang/String;
    .end local v8    # "certNodeListChild":Lorg/w3c/dom/Node;
    .end local v11    # "certValue":Lorg/w3c/dom/Node;
    .end local v13    # "i":I
    :cond_4
    if-eqz v10, :cond_6

    if-eqz v9, :cond_6

    if-eqz v1, :cond_6

    .line 1416
    const-string v14, "000000000/00"

    invoke-virtual {v9, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_5

    .line 1417
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v15, "country_cert_info_enable"

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1434
    .end local v1    # "certCountryName":Ljava/lang/String;
    .end local v3    # "certInfo":Ljava/lang/StringBuffer;
    .end local v6    # "certNode":Lorg/w3c/dom/Node;
    .end local v7    # "certNodeList":Lorg/w3c/dom/NodeList;
    .end local v9    # "certTA":Ljava/lang/String;
    .end local v10    # "certTRA":Ljava/lang/String;
    .end local v12    # "certValues":Ljava/lang/String;
    :goto_2
    return-void

    .line 1419
    .restart local v1    # "certCountryName":Ljava/lang/String;
    .restart local v3    # "certInfo":Ljava/lang/StringBuffer;
    .restart local v6    # "certNode":Lorg/w3c/dom/Node;
    .restart local v7    # "certNodeList":Lorg/w3c/dom/NodeList;
    .restart local v9    # "certTA":Ljava/lang/String;
    .restart local v10    # "certTRA":Ljava/lang/String;
    .restart local v12    # "certValues":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v15, "country_cert_info_traid"

    invoke-static {v14, v15, v10}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1420
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v15, "country_cert_info_ta"

    invoke-static {v14, v15, v9}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1421
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v15, "country_cert_info_name"

    invoke-static {v14, v15, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1422
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v15, "country_cert_info_enable"

    const/16 v16, 0x1

    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_2

    .line 1424
    :cond_6
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v14

    if-lez v14, :cond_7

    if-eqz v1, :cond_7

    .line 1425
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v15, "country_cert_info"

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1426
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v15, "country_cert_info_name"

    invoke-static {v14, v15, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1427
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v15, "country_cert_info_enable"

    const/16 v16, 0x1

    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_2

    .line 1429
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v15, "country_cert_info_enable"

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_2

    .line 1432
    .end local v1    # "certCountryName":Ljava/lang/String;
    .end local v3    # "certInfo":Ljava/lang/StringBuffer;
    .end local v6    # "certNode":Lorg/w3c/dom/Node;
    .end local v7    # "certNodeList":Lorg/w3c/dom/NodeList;
    .end local v9    # "certTA":Ljava/lang/String;
    .end local v10    # "certTRA":Ljava/lang/String;
    .end local v12    # "certValues":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v15, "country_cert_info_enable"

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_2
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3525
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Main.Phone.WorldTime.City"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3526
    const-string v0, "Settings.Main.Phone.WorldTime.Sign"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3527
    const-string v0, "Settings.Main.Phone.WorldTime.TimeDiff"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3528
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 53

    .prologue
    .line 1927
    const-string v5, "NOERROR"

    .line 1929
    .local v5, "answer":Ljava/lang/String;
    const/16 v48, 0x0

    .local v48, "xmlValue":Ljava/lang/String;
    const/16 v36, 0x0

    .line 1933
    .local v36, "phoneValue":Ljava/lang/String;
    new-instance v49, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v50, v0

    invoke-direct/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v49

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 1936
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.VisiblePassword"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "show_password"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_1

    .line 1939
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_26

    const-string v36, "off"

    .line 1940
    :goto_0
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_0

    .line 1941
    const-string v5, "Settings.Main.Phone.VisiblePassword"

    .line 1944
    :cond_0
    const-string v49, "Settings.Main.Phone.VisiblePassword"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_27

    .line 1945
    const-string v49, "Settings.Main.Phone.VisiblePassword"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 1951
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.UnknownSource"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "install_non_market_apps"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_3

    .line 1954
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_28

    const-string v36, "off"

    .line 1955
    :goto_2
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_2

    .line 1956
    const-string v5, "Settings.Main.Phone.UnknownSource"

    .line 1959
    :cond_2
    const-string v49, "Settings.Main.Phone.UnknownSource"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_29

    .line 1960
    const-string v49, "Settings.Main.Phone.UnknownSource"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 1966
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.DateTimeFormat.TimezoneUpdate"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_5

    .line 1967
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "auto_time"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    const-string v50, "1"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_2a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "auto_time_zone"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    const-string v50, "1"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_2a

    .line 1971
    const-string v36, "on"

    .line 1976
    :goto_4
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_4

    .line 1977
    const-string v5, "Settings.Main.Phone.DateTimeFormat.TimezoneUpdate"

    .line 1980
    :cond_4
    const-string v49, "Settings.Main.Phone.DateTimeFormat.TimezoneUpdate"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_2b

    .line 1981
    const-string v49, "Settings.Main.Phone.DateTimeFormat.TimezoneUpdate"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 1987
    :cond_5
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Sound.MediaVolume"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_7

    .line 1988
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v49, v0

    const-string v50, "audio"

    invoke-virtual/range {v49 .. v50}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    .line 1989
    .local v4, "am":Landroid/media/AudioManager;
    const/16 v49, 0x3

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v26

    .line 1990
    .local v26, "muted":Z
    if-eqz v26, :cond_2c

    const/16 v49, 0x3

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v41

    .line 1993
    .local v41, "volume":I
    :goto_6
    const-string v49, "CSCSettings"

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, " MEDIA_VOLUME muted : "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, " AudibleStreamvolume: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const/16 v51, 0x3

    move/from16 v0, v51

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, " StreamVolume: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const/16 v51, 0x3

    move/from16 v0, v51

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1998
    invoke-static/range {v41 .. v41}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_6

    .line 1999
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_6

    .line 2000
    const-string v5, "Settings.Main.Sound.MediaVolume"

    .line 2004
    :cond_6
    const-string v49, "Settings.Main.Sound.MediaVolume"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_2d

    .line 2005
    const-string v49, "Settings.Main.Sound.MediaVolume"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2011
    .end local v4    # "am":Landroid/media/AudioManager;
    .end local v26    # "muted":Z
    .end local v41    # "volume":I
    :cond_7
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Sound.MsgToneVolume"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_9

    .line 2012
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v49, v0

    const-string v50, "audio"

    invoke-virtual/range {v49 .. v50}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    .line 2013
    .restart local v4    # "am":Landroid/media/AudioManager;
    const/16 v49, 0x5

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v26

    .line 2014
    .restart local v26    # "muted":Z
    if-eqz v26, :cond_2e

    const/16 v49, 0x5

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v41

    .line 2017
    .restart local v41    # "volume":I
    :goto_8
    const-string v49, "CSCSettings"

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, " NOTIFICATION_VOLUME muted : "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, " AudibleStreamvolume: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const/16 v51, 0x5

    move/from16 v0, v51

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, " StreamVolume: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const/16 v51, 0x5

    move/from16 v0, v51

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2023
    invoke-static/range {v41 .. v41}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_8

    .line 2024
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_8

    .line 2025
    const-string v5, "Settings.Main.Sound.MsgToneVolume"

    .line 2029
    :cond_8
    const-string v49, "Settings.Main.Sound.MsgToneVolume"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_2f

    .line 2030
    const-string v49, "Settings.Main.Sound.MsgToneVolume"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2036
    .end local v4    # "am":Landroid/media/AudioManager;
    .end local v26    # "muted":Z
    .end local v41    # "volume":I
    :cond_9
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Sound.RngVolume"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_b

    .line 2037
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v49, v0

    const-string v50, "audio"

    invoke-virtual/range {v49 .. v50}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    .line 2038
    .restart local v4    # "am":Landroid/media/AudioManager;
    const/16 v49, 0x2

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v26

    .line 2039
    .restart local v26    # "muted":Z
    if-eqz v26, :cond_30

    const/16 v49, 0x2

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v41

    .line 2042
    .restart local v41    # "volume":I
    :goto_a
    const-string v49, "CSCSettings"

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, " RING_VOLUME muted : "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, " AudibleStreamvolume: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const/16 v51, 0x2

    move/from16 v0, v51

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, " StreamVolume: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const/16 v51, 0x2

    move/from16 v0, v51

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2047
    invoke-static/range {v41 .. v41}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_a

    .line 2048
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_a

    .line 2049
    const-string v5, "Settings.Main.Sound.RngVolume"

    .line 2053
    :cond_a
    const-string v49, "Settings.Main.Sound.RngVolume"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_31

    .line 2054
    const-string v49, "Settings.Main.Sound.RngVolume"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2060
    .end local v4    # "am":Landroid/media/AudioManager;
    .end local v26    # "muted":Z
    .end local v41    # "volume":I
    :cond_b
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Sound.ALARMVolume"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_d

    .line 2061
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v49, v0

    const-string v50, "audio"

    invoke-virtual/range {v49 .. v50}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    .line 2062
    .restart local v4    # "am":Landroid/media/AudioManager;
    const/16 v49, 0x4

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v41

    .line 2064
    .restart local v41    # "volume":I
    invoke-static/range {v41 .. v41}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_c

    .line 2065
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_c

    .line 2066
    const-string v5, "Settings.Main.Sound.ALARMVolume"

    .line 2070
    :cond_c
    const-string v49, "Settings.Main.Sound.ALARMVolume"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_32

    .line 2071
    const-string v49, "Settings.Main.Sound.ALARMVolume"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2077
    .end local v4    # "am":Landroid/media/AudioManager;
    .end local v41    # "volume":I
    :cond_d
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Sound.RngToneAlertType"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_10

    .line 2078
    const/16 v40, 0x0

    .line 2081
    .local v40, "vibMelodymode":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "vibrate_when_ringing"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v40

    .line 2086
    :goto_d
    const/16 v49, 0x1

    move/from16 v0, v40

    move/from16 v1, v49

    if-ne v0, v1, :cond_33

    .line 2087
    const-string v36, "vibmelody"

    .line 2099
    :cond_e
    :goto_e
    if-eqz v36, :cond_f

    .line 2100
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_f

    .line 2101
    const-string v5, "Settings.Main.Sound.RngToneAlertType"

    .line 2104
    :cond_f
    const-string v49, "Settings.Main.Sound.RngToneAlertType"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_36

    .line 2105
    const-string v49, "Settings.Main.Sound.RngToneAlertType"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2110
    .end local v40    # "vibMelodymode":I
    :cond_10
    :goto_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.BackLightTime"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "screen_off_timeout"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_13

    .line 2114
    const-string v49, "15000"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_37

    .line 2115
    const-string v36, "15sec"

    .line 2129
    :cond_11
    :goto_10
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_12

    .line 2130
    const-string v5, "Settings.Main.Phone.BackLightTime"

    .line 2133
    :cond_12
    const-string v49, "Settings.Main.Phone.BackLightTime"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_3d

    .line 2134
    const-string v49, "Settings.Main.Phone.BackLightTime"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2140
    :cond_13
    :goto_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Sound.KeyTone.KeyVolume"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_15

    .line 2142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v49, v0

    const-string v50, "audio"

    invoke-virtual/range {v49 .. v50}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    .line 2143
    .restart local v4    # "am":Landroid/media/AudioManager;
    const/16 v49, 0x1

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v26

    .line 2144
    .restart local v26    # "muted":Z
    if-eqz v26, :cond_3e

    const/16 v49, 0x1

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v41

    .line 2147
    .restart local v41    # "volume":I
    :goto_12
    const-string v49, "CSCSettings"

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, " KEYTONES muted : "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, " AudibleStreamvolume: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const/16 v51, 0x1

    move/from16 v0, v51

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, " StreamVolume: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const/16 v51, 0x1

    move/from16 v0, v51

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2152
    invoke-static/range {v41 .. v41}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v49

    const-string v50, "0"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_3f

    .line 2153
    const-string v36, "0"

    .line 2157
    :goto_13
    if-eqz v36, :cond_14

    .line 2158
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_14

    .line 2159
    const-string v5, "Settings.Main.Sound.KeyTone.KeyVolume"

    .line 2163
    :cond_14
    const-string v49, "Settings.Main.Sound.KeyTone.KeyVolume"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_40

    .line 2164
    const-string v49, "Settings.Main.Sound.KeyTone.KeyVolume"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2170
    .end local v4    # "am":Landroid/media/AudioManager;
    .end local v26    # "muted":Z
    .end local v41    # "volume":I
    :cond_15
    :goto_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Sound.TouchTone"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "sound_effects_enabled"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_17

    .line 2173
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_41

    const-string v36, "off"

    .line 2174
    :goto_15
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_16

    .line 2175
    const-string v5, "Settings.Main.Sound.TouchTone"

    .line 2178
    :cond_16
    const-string v49, "Settings.Main.Sound.TouchTone"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_42

    .line 2179
    const-string v49, "Settings.Main.Sound.TouchTone"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2185
    :cond_17
    :goto_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.UseSensorAiding"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "location_pdr_enabled"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_19

    .line 2188
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_43

    const-string v36, "off"

    .line 2189
    :goto_17
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_18

    .line 2190
    const-string v5, "Settings.Main.Phone.UseSensorAiding"

    .line 2193
    :cond_18
    const-string v49, "Settings.Main.Phone.UseSensorAiding"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_44

    .line 2194
    const-string v49, "Settings.Main.Phone.UseSensorAiding"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200
    :cond_19
    :goto_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.GPS.GPSActivation"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "location_providers_allowed"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_1b

    .line 2203
    const-string v49, "gps"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v49

    if-eqz v49, :cond_45

    const-string v36, "on"

    .line 2204
    :goto_19
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_1a

    .line 2205
    const-string v5, "Settings.GPS.GPSActivation"

    .line 2208
    :cond_1a
    const-string v49, "Settings.GPS.GPSActivation"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_46

    .line 2209
    const-string v49, "Settings.GPS.GPSActivation"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214
    :cond_1b
    :goto_1a
    const-string v49, "CSCSettings"

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "PATH_DATE_FORMAT : "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v51, v0

    const-string v52, "date_format"

    invoke-static/range {v51 .. v52}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2217
    const-string v49, "CSCSettings"

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "PATH_TIME_FORMAT : "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v51, v0

    const-string v52, "time_12_24"

    invoke-static/range {v51 .. v52}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_1e

    .line 2223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "date_format"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 2224
    if-eqz v36, :cond_1c

    .line 2225
    const-string v49, "-"

    const-string v50, ""

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 2227
    :cond_1c
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_1d

    .line 2228
    const-string v5, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    .line 2231
    :cond_1d
    const-string v49, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_47

    .line 2232
    const-string v49, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2238
    :cond_1e
    :goto_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "time_12_24"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_20

    .line 2240
    const-string v49, "24"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_48

    const-string v36, "24h"

    .line 2241
    :goto_1c
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_1f

    .line 2242
    const-string v5, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    .line 2245
    :cond_1f
    const-string v49, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_49

    .line 2246
    const-string v49, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2252
    :cond_20
    :goto_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.AutoSyncCommon"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_22

    .line 2253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v49

    invoke-static/range {v49 .. v49}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v36

    .line 2254
    const-string v49, "false"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_4a

    const-string v36, "off"

    .line 2255
    :goto_1e
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_21

    .line 2256
    const-string v5, "Settings.Main.Phone.AutoSyncCommon"

    .line 2259
    :cond_21
    const-string v49, "Settings.Main.Phone.AutoSyncCommon"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_4b

    .line 2260
    const-string v49, "Settings.Main.Phone.AutoSyncCommon"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2266
    :cond_22
    :goto_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.SmartStay"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "intelligent_sleep_mode"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_24

    .line 2269
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_4c

    const-string v36, "off"

    .line 2270
    :goto_20
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_23

    .line 2271
    const-string v5, "Settings.Main.Phone.SmartStay"

    .line 2274
    :cond_23
    const-string v49, "Settings.Main.Phone.SmartStay"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_4d

    .line 2275
    const-string v49, "Settings.Main.Phone.SmartStay"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2281
    :cond_24
    :goto_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.CountryCert.CertDisplayStatys"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_61

    .line 2282
    const/4 v7, 0x1

    .line 2285
    .local v7, "certCmpPass":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.CountryCert.CertCountryName"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    .line 2286
    .local v42, "xmlCountryName":Ljava/lang/String;
    new-instance v43, Ljava/lang/StringBuffer;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuffer;-><init>()V

    .line 2287
    .local v43, "xmlInfo":Ljava/lang/StringBuffer;
    const/16 v47, 0x0

    .line 2288
    .local v47, "xmlTRA":Ljava/lang/String;
    const/16 v46, 0x0

    .line 2290
    .local v46, "xmlTA":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.CountryCert"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v12

    .line 2291
    .local v12, "certNode":Lorg/w3c/dom/Node;
    if-eqz v12, :cond_50

    .line 2292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "DetailDesc"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    invoke-virtual {v0, v12, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v13

    .line 2294
    .local v13, "certNodeList":Lorg/w3c/dom/NodeList;
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_22
    invoke-interface {v13}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v49

    move/from16 v0, v24

    move/from16 v1, v49

    if-ge v0, v1, :cond_50

    .line 2295
    move/from16 v0, v24

    invoke-interface {v13, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    .line 2296
    .local v14, "certNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "CertTag"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    invoke-virtual {v0, v14, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v16

    .line 2297
    .local v16, "certTagNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "CertValue"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    invoke-virtual {v0, v14, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v18

    .line 2299
    .local v18, "certValueNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v15

    .line 2300
    .local v15, "certTag":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v17

    .line 2302
    .local v17, "certValue":Ljava/lang/String;
    const-string v49, "TRA ID"

    move-object/from16 v0, v49

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_4e

    .line 2303
    move-object/from16 v47, v17

    .line 2294
    :cond_25
    :goto_23
    add-int/lit8 v24, v24, 0x1

    goto :goto_22

    .line 1939
    .end local v7    # "certCmpPass":Z
    .end local v12    # "certNode":Lorg/w3c/dom/Node;
    .end local v13    # "certNodeList":Lorg/w3c/dom/NodeList;
    .end local v14    # "certNodeListChild":Lorg/w3c/dom/Node;
    .end local v15    # "certTag":Ljava/lang/String;
    .end local v16    # "certTagNode":Lorg/w3c/dom/Node;
    .end local v17    # "certValue":Ljava/lang/String;
    .end local v18    # "certValueNode":Lorg/w3c/dom/Node;
    .end local v24    # "i":I
    .end local v42    # "xmlCountryName":Ljava/lang/String;
    .end local v43    # "xmlInfo":Ljava/lang/StringBuffer;
    .end local v46    # "xmlTA":Ljava/lang/String;
    .end local v47    # "xmlTRA":Ljava/lang/String;
    :cond_26
    const-string v36, "on"

    goto/16 :goto_0

    .line 1947
    :cond_27
    const-string v49, "Settings.Main.Phone.VisiblePassword"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1954
    :cond_28
    const-string v36, "on"

    goto/16 :goto_2

    .line 1962
    :cond_29
    const-string v49, "Settings.Main.Phone.UnknownSource"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1973
    :cond_2a
    const-string v36, "off"

    goto/16 :goto_4

    .line 1983
    :cond_2b
    const-string v49, "Settings.Main.Phone.DateTimeFormat.TimezoneUpdate"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1990
    .restart local v4    # "am":Landroid/media/AudioManager;
    .restart local v26    # "muted":Z
    :cond_2c
    const/16 v49, 0x3

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v41

    goto/16 :goto_6

    .line 2007
    .restart local v41    # "volume":I
    :cond_2d
    const-string v49, "Settings.Main.Sound.MediaVolume"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 2014
    .end local v41    # "volume":I
    :cond_2e
    const/16 v49, 0x5

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v41

    goto/16 :goto_8

    .line 2032
    .restart local v41    # "volume":I
    :cond_2f
    const-string v49, "Settings.Main.Sound.MsgToneVolume"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 2039
    .end local v41    # "volume":I
    :cond_30
    const/16 v49, 0x2

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v41

    goto/16 :goto_a

    .line 2056
    .restart local v41    # "volume":I
    :cond_31
    const-string v49, "Settings.Main.Sound.RngVolume"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 2073
    .end local v26    # "muted":Z
    :cond_32
    const-string v49, "Settings.Main.Sound.ALARMVolume"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 2083
    .end local v4    # "am":Landroid/media/AudioManager;
    .end local v41    # "volume":I
    .restart local v40    # "vibMelodymode":I
    :catch_0
    move-exception v23

    .line 2084
    .local v23, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual/range {v23 .. v23}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_d

    .line 2089
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v49, v0

    const-string v50, "audio"

    invoke-virtual/range {v49 .. v50}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    .line 2090
    .restart local v4    # "am":Landroid/media/AudioManager;
    invoke-virtual {v4}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v37

    .line 2091
    .local v37, "ringmode":I
    const/16 v49, 0x2

    move/from16 v0, v37

    move/from16 v1, v49

    if-ne v0, v1, :cond_34

    .line 2092
    const-string v36, "melody"

    goto/16 :goto_e

    .line 2093
    :cond_34
    const/16 v49, 0x1

    move/from16 v0, v37

    move/from16 v1, v49

    if-ne v0, v1, :cond_35

    .line 2094
    const-string v36, "vib"

    goto/16 :goto_e

    .line 2095
    :cond_35
    if-nez v37, :cond_e

    .line 2096
    const-string v36, "mute"

    goto/16 :goto_e

    .line 2107
    .end local v4    # "am":Landroid/media/AudioManager;
    .end local v37    # "ringmode":I
    :cond_36
    const-string v49, "Settings.Main.Sound.RngToneAlertType"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_f

    .line 2116
    .end local v40    # "vibMelodymode":I
    :cond_37
    const-string v49, "30000"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_38

    .line 2117
    const-string v36, "30sec"

    goto/16 :goto_10

    .line 2118
    :cond_38
    const-string v49, "60000"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_39

    .line 2119
    const-string v36, "1min"

    goto/16 :goto_10

    .line 2120
    :cond_39
    const-string v49, "120000"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_3a

    .line 2121
    const-string v36, "2min"

    goto/16 :goto_10

    .line 2122
    :cond_3a
    const-string v49, "300000"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_3b

    .line 2123
    const-string v36, "5min"

    goto/16 :goto_10

    .line 2124
    :cond_3b
    const-string v49, "600000"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_3c

    .line 2125
    const-string v36, "10min"

    goto/16 :goto_10

    .line 2126
    :cond_3c
    const-string v49, "1800000"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_11

    .line 2127
    const-string v36, "30min"

    goto/16 :goto_10

    .line 2136
    :cond_3d
    const-string v49, "Settings.Main.Phone.BackLightTime"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_11

    .line 2144
    .restart local v4    # "am":Landroid/media/AudioManager;
    .restart local v26    # "muted":Z
    :cond_3e
    const/16 v49, 0x1

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v41

    goto/16 :goto_12

    .line 2155
    .restart local v41    # "volume":I
    :cond_3f
    invoke-static/range {v41 .. v41}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v36

    goto/16 :goto_13

    .line 2166
    :cond_40
    const-string v49, "Settings.Main.Sound.KeyTone.KeyVolume"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_14

    .line 2173
    .end local v4    # "am":Landroid/media/AudioManager;
    .end local v26    # "muted":Z
    .end local v41    # "volume":I
    :cond_41
    const-string v36, "on"

    goto/16 :goto_15

    .line 2181
    :cond_42
    const-string v49, "Settings.Main.Sound.TouchTone"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_16

    .line 2188
    :cond_43
    const-string v36, "on"

    goto/16 :goto_17

    .line 2196
    :cond_44
    const-string v49, "Settings.Main.Phone.UseSensorAiding"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_18

    .line 2203
    :cond_45
    const-string v36, "off"

    goto/16 :goto_19

    .line 2211
    :cond_46
    const-string v49, "Settings.GPS.GPSActivation"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1a

    .line 2234
    :cond_47
    const-string v49, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1b

    .line 2240
    :cond_48
    const-string v36, "12h"

    goto/16 :goto_1c

    .line 2248
    :cond_49
    const-string v49, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1d

    .line 2254
    :cond_4a
    const-string v36, "on"

    goto/16 :goto_1e

    .line 2262
    :cond_4b
    const-string v49, "Settings.Main.Phone.AutoSyncCommon"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1f

    .line 2269
    :cond_4c
    const-string v36, "on"

    goto/16 :goto_20

    .line 2277
    :cond_4d
    const-string v49, "Settings.Main.Phone.SmartStay"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_21

    .line 2304
    .restart local v7    # "certCmpPass":Z
    .restart local v12    # "certNode":Lorg/w3c/dom/Node;
    .restart local v13    # "certNodeList":Lorg/w3c/dom/NodeList;
    .restart local v14    # "certNodeListChild":Lorg/w3c/dom/Node;
    .restart local v15    # "certTag":Ljava/lang/String;
    .restart local v16    # "certTagNode":Lorg/w3c/dom/Node;
    .restart local v17    # "certValue":Ljava/lang/String;
    .restart local v18    # "certValueNode":Lorg/w3c/dom/Node;
    .restart local v24    # "i":I
    .restart local v42    # "xmlCountryName":Ljava/lang/String;
    .restart local v43    # "xmlInfo":Ljava/lang/StringBuffer;
    .restart local v46    # "xmlTA":Ljava/lang/String;
    .restart local v47    # "xmlTRA":Ljava/lang/String;
    :cond_4e
    const-string v49, "TA"

    move-object/from16 v0, v49

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_4f

    .line 2305
    move-object/from16 v46, v17

    goto/16 :goto_23

    .line 2306
    :cond_4f
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v49

    if-lez v49, :cond_25

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v49

    if-lez v49, :cond_25

    .line 2307
    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v49

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ";"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v43

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_23

    .line 2313
    .end local v13    # "certNodeList":Lorg/w3c/dom/NodeList;
    .end local v14    # "certNodeListChild":Lorg/w3c/dom/Node;
    .end local v15    # "certTag":Ljava/lang/String;
    .end local v16    # "certTagNode":Lorg/w3c/dom/Node;
    .end local v17    # "certValue":Ljava/lang/String;
    .end local v18    # "certValueNode":Lorg/w3c/dom/Node;
    .end local v24    # "i":I
    :cond_50
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "country_cert_info_enable"

    const/16 v51, 0x0

    invoke-static/range {v49 .. v51}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v30

    .line 2314
    .local v30, "phoneEnable":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "country_cert_info_name"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 2316
    .local v29, "phoneCountryName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "country_cert_info"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 2317
    .local v31, "phoneInfo":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "country_cert_info_traid"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 2318
    .local v35, "phoneTRA":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "country_cert_info_ta"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 2321
    .local v34, "phoneTA":Ljava/lang/String;
    const/16 v49, 0x1

    move/from16 v0, v30

    move/from16 v1, v49

    if-ne v0, v1, :cond_56

    const/16 v49, 0x1

    :goto_24
    const-string v50, "on"

    move-object/from16 v0, v50

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v50

    move/from16 v0, v49

    move/from16 v1, v50

    if-ne v0, v1, :cond_57

    .line 2323
    :goto_25
    const-string v50, "Settings.Main.CountryCert.CertDisplayStatys"

    const/16 v49, 0x1

    move/from16 v0, v30

    move/from16 v1, v49

    if-ne v0, v1, :cond_58

    const-string v49, "on"

    :goto_26
    move-object/from16 v0, v50

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-static {v7, v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2326
    if-eqz v42, :cond_51

    .line 2327
    move-object/from16 v0, v42

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_59

    .line 2328
    :goto_27
    move-object/from16 v0, v42

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    const-string v50, "Settings.Main.CountryCert.CertCountryName"

    move/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v42

    move-object/from16 v3, v29

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2332
    :cond_51
    const-string v27, "Settings.Main.CountryCert.DetailDesc.CertTag"

    .line 2333
    .local v27, "pathCertTag":Ljava/lang/String;
    const-string v28, "Settings.Main.CountryCert.DetailDesc.CertValue"

    .line 2334
    .local v28, "pathCertValue":Ljava/lang/String;
    if-eqz v47, :cond_52

    .line 2335
    if-eqz v35, :cond_5a

    move-object/from16 v0, v47

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_5a

    .line 2336
    :goto_28
    if-eqz v35, :cond_5b

    const/16 v49, 0x1

    :goto_29
    const-string v50, "TRA ID"

    const/16 v51, 0x0

    move/from16 v0, v49

    move-object/from16 v1, v27

    move-object/from16 v2, v50

    move-object/from16 v3, v51

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2337
    move-object/from16 v0, v47

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    move/from16 v0, v49

    move-object/from16 v1, v28

    move-object/from16 v2, v47

    move-object/from16 v3, v35

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2339
    :cond_52
    if-eqz v46, :cond_53

    .line 2340
    if-eqz v34, :cond_5c

    move-object/from16 v0, v46

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_5c

    .line 2341
    :goto_2a
    if-eqz v34, :cond_5d

    const/16 v49, 0x1

    :goto_2b
    const-string v50, "TA"

    const/16 v51, 0x0

    move/from16 v0, v49

    move-object/from16 v1, v27

    move-object/from16 v2, v50

    move-object/from16 v3, v51

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2342
    move-object/from16 v0, v46

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    move/from16 v0, v49

    move-object/from16 v1, v28

    move-object/from16 v2, v46

    move-object/from16 v3, v34

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2344
    :cond_53
    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuffer;->length()I

    move-result v49

    if-lez v49, :cond_60

    .line 2345
    new-instance v45, Ljava/util/StringTokenizer;

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v49

    const-string v50, ";"

    move-object/from16 v0, v45

    move-object/from16 v1, v49

    move-object/from16 v2, v50

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346
    .local v45, "xmlInfoStrToken":Ljava/util/StringTokenizer;
    if-eqz v31, :cond_5e

    new-instance v33, Ljava/util/StringTokenizer;

    const-string v49, ";"

    move-object/from16 v0, v33

    move-object/from16 v1, v31

    move-object/from16 v2, v49

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2349
    .local v33, "phoneInfoStrToken":Ljava/util/StringTokenizer;
    :cond_54
    :goto_2c
    invoke-virtual/range {v45 .. v45}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v49

    if-eqz v49, :cond_60

    .line 2350
    invoke-virtual/range {v45 .. v45}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v49

    const-string v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v44

    .line 2351
    .local v44, "xmlInfoArray":[Ljava/lang/String;
    const/16 v49, 0x2

    move/from16 v0, v49

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v32, v0

    const/16 v49, 0x0

    const/16 v50, 0x0

    aput-object v50, v32, v49

    const/16 v49, 0x1

    const/16 v50, 0x0

    aput-object v50, v32, v49

    .line 2354
    .local v32, "phoneInfoArray":[Ljava/lang/String;
    if-eqz v33, :cond_55

    invoke-virtual/range {v33 .. v33}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v49

    if-eqz v49, :cond_55

    .line 2355
    invoke-virtual/range {v33 .. v33}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v49

    const-string v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v39

    .line 2356
    .local v39, "strs":[Ljava/lang/String;
    if-eqz v39, :cond_55

    move-object/from16 v0, v39

    array-length v0, v0

    move/from16 v49, v0

    const/16 v50, 0x2

    move/from16 v0, v49

    move/from16 v1, v50

    if-ne v0, v1, :cond_55

    .line 2357
    move-object/from16 v32, v39

    .line 2360
    .end local v39    # "strs":[Ljava/lang/String;
    :cond_55
    if-eqz v44, :cond_54

    move-object/from16 v0, v44

    array-length v0, v0

    move/from16 v49, v0

    const/16 v50, 0x2

    move/from16 v0, v49

    move/from16 v1, v50

    if-ne v0, v1, :cond_54

    .line 2361
    const/16 v49, 0x0

    aget-object v49, v44, v49

    const/16 v50, 0x0

    aget-object v50, v32, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_5f

    const/16 v49, 0x1

    aget-object v49, v44, v49

    const/16 v50, 0x1

    aget-object v50, v32, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_5f

    .line 2363
    :goto_2d
    const/16 v49, 0x0

    aget-object v49, v44, v49

    const/16 v50, 0x0

    aget-object v50, v32, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    const/16 v50, 0x0

    aget-object v50, v44, v50

    const/16 v51, 0x0

    aget-object v51, v32, v51

    move/from16 v0, v49

    move-object/from16 v1, v27

    move-object/from16 v2, v50

    move-object/from16 v3, v51

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2365
    const/16 v49, 0x1

    aget-object v49, v44, v49

    const/16 v50, 0x1

    aget-object v50, v32, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    const/16 v50, 0x1

    aget-object v50, v44, v50

    const/16 v51, 0x1

    aget-object v51, v32, v51

    move/from16 v0, v49

    move-object/from16 v1, v28

    move-object/from16 v2, v50

    move-object/from16 v3, v51

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2c

    .line 2321
    .end local v27    # "pathCertTag":Ljava/lang/String;
    .end local v28    # "pathCertValue":Ljava/lang/String;
    .end local v32    # "phoneInfoArray":[Ljava/lang/String;
    .end local v33    # "phoneInfoStrToken":Ljava/util/StringTokenizer;
    .end local v44    # "xmlInfoArray":[Ljava/lang/String;
    .end local v45    # "xmlInfoStrToken":Ljava/util/StringTokenizer;
    :cond_56
    const/16 v49, 0x0

    goto/16 :goto_24

    :cond_57
    const/4 v7, 0x0

    goto/16 :goto_25

    .line 2323
    :cond_58
    const-string v49, "off"

    goto/16 :goto_26

    .line 2327
    :cond_59
    const/4 v7, 0x0

    goto/16 :goto_27

    .line 2335
    .restart local v27    # "pathCertTag":Ljava/lang/String;
    .restart local v28    # "pathCertValue":Ljava/lang/String;
    :cond_5a
    const/4 v7, 0x0

    goto/16 :goto_28

    .line 2336
    :cond_5b
    const/16 v49, 0x0

    goto/16 :goto_29

    .line 2340
    :cond_5c
    const/4 v7, 0x0

    goto/16 :goto_2a

    .line 2341
    :cond_5d
    const/16 v49, 0x0

    goto/16 :goto_2b

    .line 2346
    .restart local v45    # "xmlInfoStrToken":Ljava/util/StringTokenizer;
    :cond_5e
    const/16 v33, 0x0

    goto/16 :goto_2c

    .line 2361
    .restart local v32    # "phoneInfoArray":[Ljava/lang/String;
    .restart local v33    # "phoneInfoStrToken":Ljava/util/StringTokenizer;
    .restart local v44    # "xmlInfoArray":[Ljava/lang/String;
    :cond_5f
    const/4 v7, 0x0

    goto :goto_2d

    .line 2372
    .end local v32    # "phoneInfoArray":[Ljava/lang/String;
    .end local v33    # "phoneInfoStrToken":Ljava/util/StringTokenizer;
    .end local v44    # "xmlInfoArray":[Ljava/lang/String;
    .end local v45    # "xmlInfoStrToken":Ljava/util/StringTokenizer;
    :cond_60
    if-nez v7, :cond_61

    .line 2373
    const-string v5, "Settings.Main.CountryCert"

    .line 2378
    .end local v7    # "certCmpPass":Z
    .end local v12    # "certNode":Lorg/w3c/dom/Node;
    .end local v27    # "pathCertTag":Ljava/lang/String;
    .end local v28    # "pathCertValue":Ljava/lang/String;
    .end local v29    # "phoneCountryName":Ljava/lang/String;
    .end local v30    # "phoneEnable":I
    .end local v31    # "phoneInfo":Ljava/lang/String;
    .end local v34    # "phoneTA":Ljava/lang/String;
    .end local v35    # "phoneTRA":Ljava/lang/String;
    .end local v42    # "xmlCountryName":Ljava/lang/String;
    .end local v43    # "xmlInfo":Ljava/lang/StringBuffer;
    .end local v46    # "xmlTA":Ljava/lang/String;
    .end local v47    # "xmlTRA":Ljava/lang/String;
    :cond_61
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_79

    .line 2380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.Activation"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_63

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "motion_engine"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_63

    .line 2383
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_88

    const-string v36, "off"

    .line 2384
    :goto_2e
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_62

    .line 2385
    const-string v5, "Settings.Main.Phone.Motion.Activation"

    .line 2388
    :cond_62
    const-string v49, "Settings.Main.Phone.Motion.Activation"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_89

    .line 2389
    const-string v49, "Settings.Main.Phone.Motion.Activation"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2394
    :cond_63
    :goto_2f
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_GLANCE_VIEW"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_65

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.GlanceView"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_65

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "air_motion_glance_view"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_65

    .line 2399
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_8a

    const-string v36, "off"

    .line 2400
    :goto_30
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_64

    .line 2401
    const-string v5, "Settings.Main.Phone.Motion.GlanceView"

    .line 2404
    :cond_64
    const-string v49, "Settings.Main.Phone.Motion.GlanceView"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_8b

    .line 2405
    const-string v49, "Settings.Main.Phone.Motion.GlanceView"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2410
    :cond_65
    :goto_31
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PICK_UP_TO_CALL_OUT"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_67

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.DirectCall"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_67

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "motion_pick_up_to_call_out"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_67

    .line 2415
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_8c

    const-string v36, "off"

    .line 2416
    :goto_32
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_66

    .line 2417
    const-string v5, "Settings.Main.Phone.Motion.DirectCall"

    .line 2420
    :cond_66
    const-string v49, "Settings.Main.Phone.Motion.DirectCall"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_8d

    .line 2421
    const-string v49, "Settings.Main.Phone.Motion.DirectCall"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426
    :cond_67
    :goto_33
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PICK_UP"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_69

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.SmartAlert"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_69

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "motion_pick_up"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_69

    .line 2431
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_8e

    const-string v36, "off"

    .line 2432
    :goto_34
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_68

    .line 2433
    const-string v5, "Settings.Main.Phone.Motion.SmartAlert"

    .line 2436
    :cond_68
    const-string v49, "Settings.Main.Phone.Motion.SmartAlert"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_8f

    .line 2437
    const-string v49, "Settings.Main.Phone.Motion.SmartAlert"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2442
    :cond_69
    :goto_35
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_DOUBLE_TAP"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_6b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.DoubleTapToTop"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_6b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "motion_double_tap"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_6b

    .line 2447
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_90

    const-string v36, "off"

    .line 2448
    :goto_36
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_6a

    .line 2449
    const-string v5, "Settings.Main.Phone.Motion.DoubleTapToTop"

    .line 2452
    :cond_6a
    const-string v49, "Settings.Main.Phone.Motion.DoubleTapToTop"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_91

    .line 2453
    const-string v49, "Settings.Main.Phone.Motion.DoubleTapToTop"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458
    :cond_6b
    :goto_37
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_TILT"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_6d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.Tilt"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_6d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "motion_zooming"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_6d

    .line 2463
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_92

    const-string v36, "off"

    .line 2464
    :goto_38
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_6c

    .line 2465
    const-string v5, "Settings.Main.Phone.Motion.Tilt"

    .line 2468
    :cond_6c
    const-string v49, "Settings.Main.Phone.Motion.Tilt"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_93

    .line 2469
    const-string v49, "Settings.Main.Phone.Motion.Tilt"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2474
    :cond_6d
    :goto_39
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PAN"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_6f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.PanToMove"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_6f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "motion_panning"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_6f

    .line 2479
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_94

    const-string v36, "off"

    .line 2480
    :goto_3a
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_6e

    .line 2481
    const-string v5, "Settings.Main.Phone.Motion.PanToMove"

    .line 2484
    :cond_6e
    const-string v49, "Settings.Main.Phone.Motion.PanToMove"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_95

    .line 2485
    const-string v49, "Settings.Main.Phone.Motion.PanToMove"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2490
    :cond_6f
    :goto_3b
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PAN_TO_BROWSE_IMAGE"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_71

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.PanToBrowse"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_71

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "motion_pan_to_browse_image"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_71

    .line 2495
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_96

    const-string v36, "off"

    .line 2496
    :goto_3c
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_70

    .line 2497
    const-string v5, "Settings.Main.Phone.Motion.PanToBrowse"

    .line 2500
    :cond_70
    const-string v49, "Settings.Main.Phone.Motion.PanToBrowse"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_97

    .line 2501
    const-string v49, "Settings.Main.Phone.Motion.PanToBrowse"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2506
    :cond_71
    :goto_3d
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_SHAKE"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_73

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.PanToShake"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_73

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "motion_shake"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_73

    .line 2511
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_98

    const-string v36, "off"

    .line 2512
    :goto_3e
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_72

    .line 2513
    const-string v5, "Settings.Main.Phone.Motion.PanToShake"

    .line 2516
    :cond_72
    const-string v49, "Settings.Main.Phone.Motion.PanToShake"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_99

    .line 2517
    const-string v49, "Settings.Main.Phone.Motion.PanToShake"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2522
    :cond_73
    :goto_3f
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_TRUN_OVER"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_75

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.TurnOver"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_75

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "motion_overturn"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_75

    .line 2527
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_9a

    const-string v36, "off"

    .line 2528
    :goto_40
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_74

    .line 2529
    const-string v5, "Settings.Main.Phone.Motion.TurnOver"

    .line 2532
    :cond_74
    const-string v49, "Settings.Main.Phone.Motion.TurnOver"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_9b

    .line 2533
    const-string v49, "Settings.Main.Phone.Motion.TurnOver"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2538
    :cond_75
    :goto_41
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PALM_SWIPE"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_77

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.PalmSwipe"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_77

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "surface_palm_swipe"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_77

    .line 2543
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_9c

    const-string v36, "off"

    .line 2544
    :goto_42
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_76

    .line 2545
    const-string v5, "Settings.Main.Phone.Motion.PalmSwipe"

    .line 2548
    :cond_76
    const-string v49, "Settings.Main.Phone.Motion.PalmSwipe"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_9d

    .line 2549
    const-string v49, "Settings.Main.Phone.Motion.PalmSwipe"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2554
    :cond_77
    :goto_43
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v49

    const-string v50, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PALM_TOUCH"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_79

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.Motion.PalmTouch"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    if-eqz v48, :cond_79

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "surface_palm_touch"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    if-eqz v36, :cond_79

    .line 2559
    const-string v49, "0"

    move-object/from16 v0, v36

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_9e

    const-string v36, "off"

    .line 2560
    :goto_44
    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_78

    .line 2561
    const-string v5, "Settings.Main.Phone.Motion.PalmTouch"

    .line 2564
    :cond_78
    const-string v49, "Settings.Main.Phone.Motion.PalmTouch"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_9f

    .line 2565
    const-string v49, "Settings.Main.Phone.Motion.PalmTouch"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2574
    :cond_79
    :goto_45
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Sound.ExtraSound.ConnectTone"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 2575
    .local v25, "mPaserString":Ljava/lang/String;
    if-eqz v25, :cond_7b

    .line 2577
    :try_start_1
    const-string v49, "Off"

    move-object/from16 v0, v25

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_a0

    .line 2578
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "call_conn_tone"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_7a

    .line 2579
    const-string v5, "Settings.Main.Sound.ExtraSound.ConnectTone"
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2590
    :cond_7a
    :goto_46
    const-string v49, "Settings.Main.Sound.ExtraSound.ConnectTone"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_a1

    .line 2591
    const-string v49, "Settings.Main.Sound.ExtraSound.ConnectTone"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Sound.ExtraSound.ConnectTone"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2603
    :cond_7b
    :goto_47
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Sound.ExtraSound.CallEndTone"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 2604
    if-eqz v25, :cond_7d

    .line 2606
    :try_start_2
    const-string v49, "Off"

    move-object/from16 v0, v25

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_a2

    .line 2607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "call_end_tone"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_7c

    .line 2608
    const-string v5, "Settings.Main.Sound.ExtraSound.CallEndTone"
    :try_end_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_3

    .line 2619
    :cond_7c
    :goto_48
    const-string v49, "Settings.Main.Sound.ExtraSound.CallEndTone"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_a3

    .line 2620
    const-string v49, "Settings.Main.Sound.ExtraSound.CallEndTone"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Sound.ExtraSound.CallEndTone"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2631
    :cond_7d
    :goto_49
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Sound.ExtraSound.MinuteMind"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 2632
    if-eqz v25, :cond_7f

    .line 2634
    :try_start_3
    const-string v49, "Off"

    move-object/from16 v0, v25

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_a4

    .line 2635
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "min_minder"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_7e

    .line 2636
    const-string v5, "Settings.Main.Sound.ExtraSound.MinuteMind"
    :try_end_3
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_3 .. :try_end_3} :catch_5

    .line 2647
    :cond_7e
    :goto_4a
    const-string v49, "Settings.Main.Sound.ExtraSound.MinuteMind"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_a5

    .line 2648
    const-string v49, "Settings.Main.Sound.ExtraSound.MinuteMind"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Sound.ExtraSound.MinuteMind"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2659
    :cond_7f
    :goto_4b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.AlertOnCall"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 2660
    if-eqz v25, :cond_81

    .line 2662
    :try_start_4
    const-string v49, "Off"

    move-object/from16 v0, v25

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_a6

    .line 2663
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "alertoncall_mode"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_80

    .line 2664
    const-string v5, "Settings.Main.Phone.AlertOnCall"
    :try_end_4
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_4 .. :try_end_4} :catch_7

    .line 2676
    :cond_80
    :goto_4c
    const-string v49, "Settings.Main.Phone.AlertOnCall"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_a7

    .line 2677
    const-string v49, "Settings.Main.Phone.AlertOnCall"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Phone.AlertOnCall"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2690
    :cond_81
    :goto_4d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.AutoRedial"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 2691
    if-eqz v25, :cond_83

    .line 2693
    :try_start_5
    const-string v49, "Off"

    move-object/from16 v0, v25

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_a8

    .line 2694
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "autoredial_mode"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_82

    .line 2695
    const-string v5, "Settings.Main.Phone.AutoRedial"
    :try_end_5
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_5 .. :try_end_5} :catch_9

    .line 2706
    :cond_82
    :goto_4e
    const-string v49, "Settings.Main.Phone.AutoRedial"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_a9

    .line 2707
    const-string v49, "Settings.Main.Phone.AutoRedial"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Phone.AutoRedial"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2718
    :cond_83
    :goto_4f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.CallNoiseReduction"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 2719
    if-eqz v25, :cond_85

    .line 2721
    :try_start_6
    const-string v49, "Off"

    move-object/from16 v0, v25

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_aa

    .line 2722
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "call_noise_reduction"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_84

    .line 2723
    const-string v5, "Settings.Main.Phone.CallNoiseReduction"
    :try_end_6
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_6 .. :try_end_6} :catch_b

    .line 2735
    :cond_84
    :goto_50
    const-string v49, "Settings.Main.Phone.CallNoiseReduction"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_ab

    .line 2736
    const-string v49, "Settings.Main.Phone.CallNoiseReduction"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Phone.CallNoiseReduction"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2749
    :cond_85
    :goto_51
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.UseExtraVolume"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 2750
    if-eqz v25, :cond_87

    .line 2752
    :try_start_7
    const-string v49, "Off"

    move-object/from16 v0, v25

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_ac

    .line 2753
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "call_extra_volume"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_86

    .line 2754
    const-string v5, "Settings.Main.Phone.UseExtraVolume"
    :try_end_7
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_7 .. :try_end_7} :catch_d

    .line 2766
    :cond_86
    :goto_52
    const-string v49, "Settings.Main.Phone.UseExtraVolume"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_ad

    .line 2767
    const-string v49, "Settings.Main.Phone.UseExtraVolume"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Phone.UseExtraVolume"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2779
    :cond_87
    :goto_53
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    move-object/from16 v49, v0

    const-string v50, "PREF_RINGTONE_SET"

    const/16 v51, 0x0

    invoke-interface/range {v49 .. v51}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v49

    const/16 v50, 0x2

    move/from16 v0, v49

    move/from16 v1, v50

    if-ne v0, v1, :cond_ae

    .line 2780
    const-string v49, "Settings.Main.Sound.RingTone.src"

    .line 3068
    :goto_54
    return-object v49

    .line 2383
    .end local v25    # "mPaserString":Ljava/lang/String;
    :cond_88
    const-string v36, "on"

    goto/16 :goto_2e

    .line 2391
    :cond_89
    const-string v49, "Settings.Main.Phone.Motion.Activation"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2f

    .line 2399
    :cond_8a
    const-string v36, "on"

    goto/16 :goto_30

    .line 2407
    :cond_8b
    const-string v49, "Settings.Main.Phone.Motion.GlanceView"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_31

    .line 2415
    :cond_8c
    const-string v36, "on"

    goto/16 :goto_32

    .line 2423
    :cond_8d
    const-string v49, "Settings.Main.Phone.Motion.DirectCall"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_33

    .line 2431
    :cond_8e
    const-string v36, "on"

    goto/16 :goto_34

    .line 2439
    :cond_8f
    const-string v49, "Settings.Main.Phone.Motion.SmartAlert"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_35

    .line 2447
    :cond_90
    const-string v36, "on"

    goto/16 :goto_36

    .line 2455
    :cond_91
    const-string v49, "Settings.Main.Phone.Motion.DoubleTapToTop"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_37

    .line 2463
    :cond_92
    const-string v36, "on"

    goto/16 :goto_38

    .line 2471
    :cond_93
    const-string v49, "Settings.Main.Phone.Motion.Tilt"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_39

    .line 2479
    :cond_94
    const-string v36, "on"

    goto/16 :goto_3a

    .line 2487
    :cond_95
    const-string v49, "Settings.Main.Phone.Motion.PanToMove"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3b

    .line 2495
    :cond_96
    const-string v36, "on"

    goto/16 :goto_3c

    .line 2503
    :cond_97
    const-string v49, "Settings.Main.Phone.Motion.PanToBrowse"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3d

    .line 2511
    :cond_98
    const-string v36, "on"

    goto/16 :goto_3e

    .line 2519
    :cond_99
    const-string v49, "Settings.Main.Phone.Motion.PanToShake"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 2527
    :cond_9a
    const-string v36, "on"

    goto/16 :goto_40

    .line 2535
    :cond_9b
    const-string v49, "Settings.Main.Phone.Motion.TurnOver"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_41

    .line 2543
    :cond_9c
    const-string v36, "on"

    goto/16 :goto_42

    .line 2551
    :cond_9d
    const-string v49, "Settings.Main.Phone.Motion.PalmSwipe"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_43

    .line 2559
    :cond_9e
    const-string v36, "on"

    goto/16 :goto_44

    .line 2567
    :cond_9f
    const-string v49, "Settings.Main.Phone.Motion.PalmTouch"

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_45

    .line 2581
    .restart local v25    # "mPaserString":Ljava/lang/String;
    :cond_a0
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "call_conn_tone"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    const/16 v50, 0x1

    move/from16 v0, v49

    move/from16 v1, v50

    if-eq v0, v1, :cond_7a

    .line 2582
    const-string v5, "Settings.Main.Sound.ExtraSound.ConnectTone"
    :try_end_8
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_46

    .line 2584
    :catch_1
    move-exception v23

    .line 2585
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v49, "CSCSettings"

    const-string v50, "to get (call_conn_tone) failed"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2586
    const-string v49, "CSCSettings : to get (call_conn_tone) failed"

    invoke-static/range {v49 .. v49}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 2587
    const-string v5, "Settings.Main.Sound.ExtraSound.ConnectTone"

    goto/16 :goto_46

    .line 2594
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_a1
    :try_start_9
    const-string v49, "Settings.Main.Sound.ExtraSound.ConnectTone"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Sound.ExtraSound.ConnectTone"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v51, v0

    const-string v52, "call_conn_tone"

    invoke-static/range {v51 .. v52}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v51

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v49 .. v51}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_9 .. :try_end_9} :catch_2

    goto/16 :goto_47

    .line 2597
    :catch_2
    move-exception v23

    .line 2598
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual/range {v23 .. v23}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_47

    .line 2610
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_a2
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "call_end_tone"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    const/16 v50, 0x1

    move/from16 v0, v49

    move/from16 v1, v50

    if-eq v0, v1, :cond_7c

    .line 2611
    const-string v5, "Settings.Main.Sound.ExtraSound.CallEndTone"
    :try_end_a
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_a .. :try_end_a} :catch_3

    goto/16 :goto_48

    .line 2613
    :catch_3
    move-exception v23

    .line 2614
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v49, "CSCSettings"

    const-string v50, "to get (call_end_tone) failed"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2615
    const-string v49, "CSCSettings : to get (call_end_tone) failed"

    invoke-static/range {v49 .. v49}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 2616
    const-string v5, "Settings.Main.Sound.ExtraSound.CallEndTone"

    goto/16 :goto_48

    .line 2623
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_a3
    :try_start_b
    const-string v49, "Settings.Main.Sound.ExtraSound.CallEndTone"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Sound.ExtraSound.CallEndTone"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v51, v0

    const-string v52, "call_end_tone"

    invoke-static/range {v51 .. v52}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v51

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v49 .. v51}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_b .. :try_end_b} :catch_4

    goto/16 :goto_49

    .line 2625
    :catch_4
    move-exception v23

    .line 2626
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual/range {v23 .. v23}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_49

    .line 2638
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_a4
    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "min_minder"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    const/16 v50, 0x1

    move/from16 v0, v49

    move/from16 v1, v50

    if-eq v0, v1, :cond_7e

    .line 2639
    const-string v5, "Settings.Main.Sound.ExtraSound.MinuteMind"
    :try_end_c
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_c .. :try_end_c} :catch_5

    goto/16 :goto_4a

    .line 2641
    :catch_5
    move-exception v23

    .line 2642
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v49, "CSCSettings"

    const-string v50, "to get (min_minder) failed"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2643
    const-string v49, "CSCSettings : to get (min_minder) failed"

    invoke-static/range {v49 .. v49}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 2644
    const-string v5, "Settings.Main.Sound.ExtraSound.MinuteMind"

    goto/16 :goto_4a

    .line 2651
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_a5
    :try_start_d
    const-string v49, "Settings.Main.Sound.ExtraSound.MinuteMind"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Sound.ExtraSound.MinuteMind"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v51, v0

    const-string v52, "min_minder"

    invoke-static/range {v51 .. v52}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v51

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v49 .. v51}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_d .. :try_end_d} :catch_6

    goto/16 :goto_4b

    .line 2653
    :catch_6
    move-exception v23

    .line 2654
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual/range {v23 .. v23}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_4b

    .line 2666
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_a6
    :try_start_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "alertoncall_mode"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    const/16 v50, 0x1

    move/from16 v0, v49

    move/from16 v1, v50

    if-eq v0, v1, :cond_80

    .line 2667
    const-string v5, "Settings.Main.Phone.AlertOnCall"
    :try_end_e
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_e .. :try_end_e} :catch_7

    goto/16 :goto_4c

    .line 2669
    :catch_7
    move-exception v23

    .line 2670
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v49, "CSCSettings"

    const-string v50, "to get (alertoncall_mode) failed"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2671
    const-string v49, "CSCSettings : to get (alertoncall_mode) failed"

    invoke-static/range {v49 .. v49}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 2673
    const-string v5, "Settings.Main.Phone.AlertOnCall"

    goto/16 :goto_4c

    .line 2680
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_a7
    :try_start_f
    const-string v49, "Settings.Main.Phone.AlertOnCall"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Phone.AlertOnCall"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v51, v0

    const-string v52, "alertoncall_mode"

    invoke-static/range {v51 .. v52}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v51

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v49 .. v51}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_f .. :try_end_f} :catch_8

    goto/16 :goto_4d

    .line 2684
    :catch_8
    move-exception v23

    .line 2685
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual/range {v23 .. v23}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_4d

    .line 2697
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_a8
    :try_start_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "autoredial_mode"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    const/16 v50, 0x1

    move/from16 v0, v49

    move/from16 v1, v50

    if-eq v0, v1, :cond_82

    .line 2698
    const-string v5, "Settings.Main.Phone.AutoRedial"
    :try_end_10
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_10 .. :try_end_10} :catch_9

    goto/16 :goto_4e

    .line 2700
    :catch_9
    move-exception v23

    .line 2701
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v49, "CSCSettings"

    const-string v50, "to get (autoredial_mode) failed"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2702
    const-string v49, "CSCSettings : to get (autoredial_mode) failed"

    invoke-static/range {v49 .. v49}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 2703
    const-string v5, "Settings.Main.Phone.AutoRedial"

    goto/16 :goto_4e

    .line 2710
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_a9
    :try_start_11
    const-string v49, "Settings.Main.Phone.AutoRedial"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Phone.AutoRedial"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v51, v0

    const-string v52, "autoredial_mode"

    invoke-static/range {v51 .. v52}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v51

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v49 .. v51}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_11
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_11 .. :try_end_11} :catch_a

    goto/16 :goto_4f

    .line 2712
    :catch_a
    move-exception v23

    .line 2713
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual/range {v23 .. v23}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_4f

    .line 2725
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_aa
    :try_start_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "call_noise_reduction"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    const/16 v50, 0x1

    move/from16 v0, v49

    move/from16 v1, v50

    if-eq v0, v1, :cond_84

    .line 2726
    const-string v5, "Settings.Main.Phone.CallNoiseReduction"
    :try_end_12
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_12 .. :try_end_12} :catch_b

    goto/16 :goto_50

    .line 2728
    :catch_b
    move-exception v23

    .line 2729
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v49, "CSCSettings"

    const-string v50, "to get (call_noise_reduction) failed"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2730
    const-string v49, "CSCSettings : to get (call_noise_reduction) failed"

    invoke-static/range {v49 .. v49}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 2732
    const-string v5, "Settings.Main.Phone.CallNoiseReduction"

    goto/16 :goto_50

    .line 2740
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_ab
    :try_start_13
    const-string v49, "Settings.Main.Phone.CallNoiseReduction"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Phone.CallNoiseReduction"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v51, v0

    const-string v52, "call_noise_reduction"

    invoke-static/range {v51 .. v52}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v51

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v49 .. v51}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_13
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_13 .. :try_end_13} :catch_c

    goto/16 :goto_51

    .line 2743
    :catch_c
    move-exception v23

    .line 2744
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual/range {v23 .. v23}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_51

    .line 2756
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_ac
    :try_start_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "call_extra_volume"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    const/16 v50, 0x1

    move/from16 v0, v49

    move/from16 v1, v50

    if-eq v0, v1, :cond_86

    .line 2757
    const-string v5, "Settings.Main.Phone.UseExtraVolume"
    :try_end_14
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_14 .. :try_end_14} :catch_d

    goto/16 :goto_52

    .line 2759
    :catch_d
    move-exception v23

    .line 2760
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v49, "CSCSettings"

    const-string v50, "to get (call_extra_volume) failed"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2761
    const-string v49, "CSCSettings : to get (call_extra_volume) failed"

    invoke-static/range {v49 .. v49}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 2763
    const-string v5, "Settings.Main.Phone.UseExtraVolume"

    goto/16 :goto_52

    .line 2770
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_ad
    :try_start_15
    const-string v49, "Settings.Main.Phone.UseExtraVolume"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Main.Phone.UseExtraVolume"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v51, v0

    const-string v52, "call_extra_volume"

    invoke-static/range {v51 .. v52}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v51

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v49 .. v51}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_15 .. :try_end_15} :catch_e

    goto/16 :goto_53

    .line 2773
    :catch_e
    move-exception v23

    .line 2774
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual/range {v23 .. v23}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_53

    .line 2783
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_ae
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    move-object/from16 v49, v0

    const-string v50, "PREF_NOTIFICATION_SET"

    const/16 v51, 0x0

    invoke-interface/range {v49 .. v51}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v49

    const/16 v50, 0x2

    move/from16 v0, v49

    move/from16 v1, v50

    if-ne v0, v1, :cond_af

    .line 2784
    const-string v49, "Settings.Main.Sound.MessageTone.src"

    goto/16 :goto_54

    .line 2787
    :cond_af
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Sound.AlarmTone.src"

    const/16 v51, 0x0

    const/16 v52, 0x1

    invoke-virtual/range {v49 .. v52}, Lcom/samsung/sec/android/application/csc/CscParser;->getAttrbute(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v25

    .line 2788
    if-eqz v25, :cond_b0

    .line 2789
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    move-object/from16 v49, v0

    const-string v50, "PREF_ALARMTONE_SET"

    const/16 v51, 0x0

    invoke-interface/range {v49 .. v51}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v49

    const/16 v50, 0x2

    move/from16 v0, v49

    move/from16 v1, v50

    if-ne v0, v1, :cond_b7

    .line 2790
    const-string v49, "Settings.Main.Sound.AlarmTone.src"

    const/16 v50, 0x0

    move-object/from16 v0, v49

    move-object/from16 v1, v25

    move-object/from16 v2, v50

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2791
    const-string v5, "Settings.Main.Sound.AlarmTone.src"

    .line 2798
    :cond_b0
    :goto_55
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Wifi.SleepPolicy"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 2799
    if-eqz v25, :cond_b2

    .line 2801
    :try_start_16
    const-string v49, "always"

    move-object/from16 v0, v25

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_b8

    .line 2802
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "wifi_sleep_policy"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    const/16 v50, 0x2

    move/from16 v0, v49

    move/from16 v1, v50

    if-eq v0, v1, :cond_b1

    .line 2803
    const-string v5, "Settings.Wifi.SleepPolicy"
    :try_end_16
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_16 .. :try_end_16} :catch_f

    .line 2817
    :cond_b1
    :goto_56
    const-string v49, "Settings.Wifi.SleepPolicy"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_ba

    .line 2818
    const-string v49, "Settings.Wifi.SleepPolicy"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Wifi.SleepPolicy"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2830
    :cond_b2
    :goto_57
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Wifi.WifiEnableStatus"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 2831
    if-eqz v25, :cond_b4

    .line 2833
    :try_start_17
    const-string v49, "Off"

    move-object/from16 v0, v25

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_bb

    .line 2834
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "wifi_on"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_b3

    .line 2835
    const-string v5, "Settings.Wifi.WifiEnableStatus"
    :try_end_17
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_17 .. :try_end_17} :catch_11

    .line 2846
    :cond_b3
    :goto_58
    const-string v49, "Settings.Wifi.WifiEnableStatus"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_bc

    .line 2847
    const-string v49, "Settings.Wifi.WifiEnableStatus"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Wifi.WifiEnableStatus"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 2859
    :cond_b4
    :goto_59
    const-string v49, "ro.csc.sales_code"

    invoke-static/range {v49 .. v49}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    .line 2860
    .local v38, "salesCode":Ljava/lang/String;
    const-string v49, "XSG"

    move-object/from16 v0, v49

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_be

    .line 2861
    const-string v49, "CSCSettings"

    const-string v50, "Setting Country Certification info"

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2862
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.CountryCert"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v12

    .line 2863
    .restart local v12    # "certNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "DetailDesc"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    invoke-virtual {v0, v12, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v13

    .line 2864
    .restart local v13    # "certNodeList":Lorg/w3c/dom/NodeList;
    const/4 v8, 0x0

    .line 2866
    .local v8, "certEnable":I
    :try_start_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "country_cert_info_enable"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_18
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_18 .. :try_end_18} :catch_13

    move-result v8

    .line 2870
    :goto_5a
    if-eqz v12, :cond_be

    if-eqz v8, :cond_be

    .line 2871
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.CountryCert.CertCountryName"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2872
    .local v9, "certInfoName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "country_cert_info_name"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    invoke-static {v0, v1, v9}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2873
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "country_cert_info_name"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-virtual {v0, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_b5

    .line 2875
    const-string v49, "Settings.Main.CountryCert.CertCountryName"

    move-object/from16 v0, v49

    move-object/from16 v1, v25

    invoke-static {v0, v1, v9}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2878
    :cond_b5
    const/16 v24, 0x0

    .restart local v24    # "i":I
    :goto_5b
    invoke-interface {v13}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v49

    move/from16 v0, v24

    move/from16 v1, v49

    if-ge v0, v1, :cond_be

    .line 2879
    move/from16 v0, v24

    invoke-interface {v13, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    .line 2881
    .restart local v14    # "certNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "CertTag"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    invoke-virtual {v0, v14, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 2882
    .local v10, "certName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    invoke-virtual {v0, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v11

    .line 2883
    .local v11, "certNames":Ljava/lang/String;
    if-eqz v11, :cond_b6

    .line 2884
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "CertValue"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    invoke-virtual {v0, v14, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v17

    .line 2885
    .local v17, "certValue":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v19

    .line 2886
    .local v19, "certValues":Ljava/lang/String;
    if-eqz v19, :cond_b6

    .line 2887
    const-string v49, "CSCSettings"

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, " ** getCountryCert ("

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, ") : "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, ", "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2889
    const-string v49, "TRA ID"

    move-object/from16 v0, v49

    invoke-virtual {v11, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_bd

    .line 2890
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "country_cert_info_traid"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_b6

    .line 2893
    const-string v49, "Settings.Main.CountryCert"

    move-object/from16 v0, v49

    move-object/from16 v1, v25

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2878
    .end local v17    # "certValue":Lorg/w3c/dom/Node;
    .end local v19    # "certValues":Ljava/lang/String;
    :cond_b6
    :goto_5c
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_5b

    .line 2793
    .end local v8    # "certEnable":I
    .end local v9    # "certInfoName":Ljava/lang/String;
    .end local v10    # "certName":Lorg/w3c/dom/Node;
    .end local v11    # "certNames":Ljava/lang/String;
    .end local v12    # "certNode":Lorg/w3c/dom/Node;
    .end local v13    # "certNodeList":Lorg/w3c/dom/NodeList;
    .end local v14    # "certNodeListChild":Lorg/w3c/dom/Node;
    .end local v24    # "i":I
    .end local v38    # "salesCode":Ljava/lang/String;
    :cond_b7
    const-string v49, "Settings.Main.Sound.AlarmTone.src"

    move-object/from16 v0, v49

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_55

    .line 2804
    :cond_b8
    :try_start_19
    const-string v49, "plugged"

    move-object/from16 v0, v25

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_b9

    .line 2805
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "wifi_sleep_policy"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    const/16 v50, 0x1

    move/from16 v0, v49

    move/from16 v1, v50

    if-eq v0, v1, :cond_b1

    .line 2806
    const-string v5, "Settings.Wifi.SleepPolicy"

    goto/16 :goto_56

    .line 2807
    :cond_b9
    const-string v49, "never"

    move-object/from16 v0, v25

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_b1

    .line 2808
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "wifi_sleep_policy"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_b1

    .line 2809
    const-string v5, "Settings.Wifi.SleepPolicy"
    :try_end_19
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_19 .. :try_end_19} :catch_f

    goto/16 :goto_56

    .line 2811
    :catch_f
    move-exception v23

    .line 2812
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v49, "CSCSettings"

    const-string v50, "to get (Settings.Global.WIFI_SLEEP_POLICY) failed"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2813
    const-string v49, "CSCSettings : to get (Settings.Global.WIFI_SLEEP_POLICY) failed"

    invoke-static/range {v49 .. v49}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 2815
    const-string v5, "Settings.Wifi.SleepPolicy"

    goto/16 :goto_56

    .line 2821
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_ba
    :try_start_1a
    const-string v49, "Settings.Wifi.SleepPolicy"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Wifi.SleepPolicy"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v51, v0

    const-string v52, "wifi_sleep_policy"

    invoke-static/range {v51 .. v52}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v51

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v49 .. v51}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1a
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1a .. :try_end_1a} :catch_10

    goto/16 :goto_57

    .line 2824
    :catch_10
    move-exception v23

    .line 2825
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual/range {v23 .. v23}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_57

    .line 2837
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_bb
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "wifi_on"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v49

    const/16 v50, 0x1

    move/from16 v0, v49

    move/from16 v1, v50

    if-eq v0, v1, :cond_b3

    .line 2838
    const-string v5, "Settings.Wifi.WifiEnableStatus"
    :try_end_1b
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1b .. :try_end_1b} :catch_11

    goto/16 :goto_58

    .line 2840
    :catch_11
    move-exception v23

    .line 2841
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v49, "CSCSettings"

    const-string v50, "to get (Settings.Secure.WIFI_ON) failed"

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2842
    const-string v49, "CSCSettings : to get (Settings.Secure.WIFI_ON) failed"

    invoke-static/range {v49 .. v49}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 2844
    const-string v5, "Settings.Wifi.WifiEnableStatus"

    goto/16 :goto_58

    .line 2850
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_bc
    :try_start_1c
    const-string v49, "Settings.Wifi.WifiEnableStatus"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v50, v0

    const-string v51, "Settings.Wifi.WifiEnableStatus"

    invoke-virtual/range {v50 .. v51}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v51, v0

    const-string v52, "wifi_on"

    invoke-static/range {v51 .. v52}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v51

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v49 .. v51}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1c
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1c .. :try_end_1c} :catch_12

    goto/16 :goto_59

    .line 2853
    :catch_12
    move-exception v23

    .line 2854
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual/range {v23 .. v23}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_59

    .line 2867
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    .restart local v8    # "certEnable":I
    .restart local v12    # "certNode":Lorg/w3c/dom/Node;
    .restart local v13    # "certNodeList":Lorg/w3c/dom/NodeList;
    .restart local v38    # "salesCode":Ljava/lang/String;
    :catch_13
    move-exception v23

    .line 2868
    .restart local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual/range {v23 .. v23}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_5a

    .line 2897
    .end local v23    # "e":Landroid/provider/Settings$SettingNotFoundException;
    .restart local v9    # "certInfoName":Ljava/lang/String;
    .restart local v10    # "certName":Lorg/w3c/dom/Node;
    .restart local v11    # "certNames":Ljava/lang/String;
    .restart local v14    # "certNodeListChild":Lorg/w3c/dom/Node;
    .restart local v17    # "certValue":Lorg/w3c/dom/Node;
    .restart local v19    # "certValues":Ljava/lang/String;
    .restart local v24    # "i":I
    :cond_bd
    const-string v49, "TA"

    move-object/from16 v0, v49

    invoke-virtual {v11, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_b6

    .line 2898
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v49, v0

    const-string v50, "country_cert_info_ta"

    invoke-static/range {v49 .. v50}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v49

    if-eqz v49, :cond_b6

    .line 2900
    const-string v49, "Settings.Main.CountryCert"

    move-object/from16 v0, v49

    move-object/from16 v1, v25

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5c

    .line 2911
    .end local v8    # "certEnable":I
    .end local v9    # "certInfoName":Ljava/lang/String;
    .end local v10    # "certName":Lorg/w3c/dom/Node;
    .end local v11    # "certNames":Ljava/lang/String;
    .end local v12    # "certNode":Lorg/w3c/dom/Node;
    .end local v13    # "certNodeList":Lorg/w3c/dom/NodeList;
    .end local v14    # "certNodeListChild":Lorg/w3c/dom/Node;
    .end local v17    # "certValue":Lorg/w3c/dom/Node;
    .end local v19    # "certValues":Ljava/lang/String;
    .end local v24    # "i":I
    :cond_be
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v49, v0

    const-string v50, "Settings.Main.Phone.DefLanguage"

    invoke-virtual/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 2912
    const-string v49, "automatic"

    invoke-static/range {v49 .. v49}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2913
    .local v6, "automaticCompare":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v49, v0

    invoke-virtual/range {v49 .. v49}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v49

    move-object/from16 v0, v49

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v49, v0

    invoke-virtual/range {v49 .. v49}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v21

    .line 2915
    .local v21, "currentLang":Ljava/lang/String;
    if-eqz v25, :cond_bf

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_bf

    .line 2916
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v49, v0

    invoke-virtual/range {v49 .. v49}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v49

    move-object/from16 v0, v49

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v49, v0

    invoke-virtual/range {v49 .. v49}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v20

    .line 2917
    .local v20, "currentCountry":Ljava/lang/String;
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v49

    const/16 v50, 0x5

    move/from16 v0, v49

    move/from16 v1, v50

    if-ne v0, v1, :cond_c4

    const/16 v49, 0x5f

    move-object/from16 v0, v25

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v49

    const/16 v50, 0x2

    move/from16 v0, v49

    move/from16 v1, v50

    if-ne v0, v1, :cond_c4

    .line 2918
    const/16 v22, 0x0

    .line 2919
    .local v22, "currentLocale":Ljava/lang/String;
    if-eqz v21, :cond_c2

    if-eqz v20, :cond_c2

    .line 2920
    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v49

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "_"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 2921
    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_c1

    .line 2922
    const-string v49, "CSCSettings"

    const-string v50, "currentLocale is combined."

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2934
    :goto_5d
    const-string v49, "Settings.Main.Phone.DefLanguage"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_c3

    .line 2935
    const-string v49, "Settings.Main.Phone.DefLanguage"

    move-object/from16 v0, v49

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 3065
    .end local v20    # "currentCountry":Ljava/lang/String;
    .end local v22    # "currentLocale":Ljava/lang/String;
    :cond_bf
    :goto_5e
    const-string v49, "NOERROR"

    move-object/from16 v0, v49

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_c0

    .line 3066
    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "CSCSettings : "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v49 .. v49}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    :cond_c0
    move-object/from16 v49, v5

    .line 3068
    goto/16 :goto_54

    .line 2924
    .restart local v20    # "currentCountry":Ljava/lang/String;
    .restart local v22    # "currentLocale":Ljava/lang/String;
    :cond_c1
    const-string v49, "CSCSettings"

    const-string v50, "language value in customer.xml and combined value is different"

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2925
    const-string v49, "CSCSettings : language value in customer.xml and combined value is different"

    invoke-static/range {v49 .. v49}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 2928
    const-string v5, "Settings.Main.Phone.DefLanguage"

    goto :goto_5d

    .line 2931
    :cond_c2
    const-string v49, "CSCSettings"

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "currentLocale isn\'t combined. currentLang is"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, "currentCountry is "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5d

    .line 2937
    :cond_c3
    const-string v49, "Settings.Main.Phone.DefLanguage"

    move-object/from16 v0, v49

    move-object/from16 v1, v25

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5e

    .line 2941
    .end local v22    # "currentLocale":Ljava/lang/String;
    :cond_c4
    const-string v49, "en"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_c8

    .line 2942
    const-string v49, "ph"

    move-object/from16 v0, v20

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_c6

    .line 2943
    const-string v21, "english_philippines"

    .line 3052
    :cond_c5
    :goto_5f
    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_f9

    .line 3057
    :goto_60
    const-string v49, "Settings.Main.Phone.DefLanguage"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_fa

    .line 3058
    const-string v49, "Settings.Main.Phone.DefLanguage"

    move-object/from16 v0, v49

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5e

    .line 2944
    :cond_c6
    const-string v49, "us"

    move-object/from16 v0, v20

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_c7

    .line 2945
    const-string v21, "english_us"

    goto :goto_5f

    .line 2947
    :cond_c7
    const-string v21, "english"

    goto :goto_5f

    .line 2948
    :cond_c8
    const-string v49, "de"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_c9

    .line 2949
    const-string v21, "german"

    goto :goto_5f

    .line 2950
    :cond_c9
    const-string v49, "es"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_cb

    .line 2951
    const-string v49, "us"

    move-object/from16 v0, v20

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_ca

    .line 2952
    const-string v21, "spanish_latin"

    goto :goto_5f

    .line 2954
    :cond_ca
    const-string v21, "spanish"

    goto :goto_5f

    .line 2955
    :cond_cb
    const-string v49, "fr"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_cc

    .line 2956
    const-string v21, "french"

    goto :goto_5f

    .line 2957
    :cond_cc
    const-string v49, "it"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_cd

    .line 2958
    const-string v21, "italian"

    goto :goto_5f

    .line 2959
    :cond_cd
    const-string v49, "nl"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_ce

    .line 2960
    const-string v21, "dutch"

    goto/16 :goto_5f

    .line 2961
    :cond_ce
    const-string v49, "pt"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_d0

    .line 2962
    const-string v49, "br"

    move-object/from16 v0, v20

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_cf

    .line 2963
    const-string v21, "portuguese_latin"

    goto/16 :goto_5f

    .line 2965
    :cond_cf
    const-string v21, "portuguese"

    goto/16 :goto_5f

    .line 2966
    :cond_d0
    const-string v49, "pl"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_d1

    .line 2967
    const-string v21, "polish"

    goto/16 :goto_5f

    .line 2968
    :cond_d1
    const-string v49, "cs"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_d2

    .line 2969
    const-string v21, "czech"

    goto/16 :goto_5f

    .line 2970
    :cond_d2
    const-string v49, "da"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_d3

    .line 2971
    const-string v21, "danish"

    goto/16 :goto_5f

    .line 2972
    :cond_d3
    const-string v49, "fi"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_d4

    .line 2973
    const-string v21, "finnish"

    goto/16 :goto_5f

    .line 2974
    :cond_d4
    const-string v49, "no"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_d5

    .line 2975
    const-string v21, "norwegian"

    goto/16 :goto_5f

    .line 2976
    :cond_d5
    const-string v49, "sv"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_d6

    .line 2977
    const-string v21, "swedish"

    goto/16 :goto_5f

    .line 2978
    :cond_d6
    const-string v49, "ro"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_d7

    .line 2979
    const-string v21, "romanian"

    goto/16 :goto_5f

    .line 2980
    :cond_d7
    const-string v49, "sk"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_d8

    .line 2981
    const-string v21, "slovakian"

    goto/16 :goto_5f

    .line 2982
    :cond_d8
    const-string v49, "hu"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_d9

    .line 2983
    const-string v21, "hungarian"

    goto/16 :goto_5f

    .line 2984
    :cond_d9
    const-string v49, "sr"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_da

    .line 2985
    const-string v21, "serbian"

    goto/16 :goto_5f

    .line 2986
    :cond_da
    const-string v49, "el"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_db

    .line 2987
    const-string v21, "greek"

    goto/16 :goto_5f

    .line 2988
    :cond_db
    const-string v49, "sq"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_dc

    .line 2989
    const-string v21, "albanian"

    goto/16 :goto_5f

    .line 2990
    :cond_dc
    const-string v49, "ga"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_dd

    .line 2991
    const-string v21, "irish"

    goto/16 :goto_5f

    .line 2992
    :cond_dd
    const-string v49, "mk"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_de

    .line 2993
    const-string v21, "macedonian"

    goto/16 :goto_5f

    .line 2994
    :cond_de
    const-string v49, "mt"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_df

    .line 2995
    const-string v21, "maltese"

    goto/16 :goto_5f

    .line 2996
    :cond_df
    const-string v49, "tr"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_e0

    .line 2997
    const-string v21, "turkish"

    goto/16 :goto_5f

    .line 2998
    :cond_e0
    const-string v49, "sl"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_e1

    .line 2999
    const-string v21, "slovenian"

    goto/16 :goto_5f

    .line 3000
    :cond_e1
    const-string v49, "is"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_e2

    .line 3001
    const-string v21, "icelandic"

    goto/16 :goto_5f

    .line 3002
    :cond_e2
    const-string v49, "hr"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_e3

    .line 3003
    const-string v21, "croatian"

    goto/16 :goto_5f

    .line 3004
    :cond_e3
    const-string v49, "ru"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_e4

    .line 3005
    const-string v21, "russian"

    goto/16 :goto_5f

    .line 3006
    :cond_e4
    const-string v49, "bg"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_e5

    .line 3007
    const-string v21, "bulgarian"

    goto/16 :goto_5f

    .line 3008
    :cond_e5
    const-string v49, "et"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_e6

    .line 3009
    const-string v21, "estonian"

    goto/16 :goto_5f

    .line 3010
    :cond_e6
    const-string v49, "lv"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_e7

    .line 3011
    const-string v21, "latvian"

    goto/16 :goto_5f

    .line 3012
    :cond_e7
    const-string v49, "lt"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_e8

    .line 3013
    const-string v21, "lithuanian"

    goto/16 :goto_5f

    .line 3014
    :cond_e8
    const-string v49, "uk"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_e9

    .line 3015
    const-string v21, "ukrainian"

    goto/16 :goto_5f

    .line 3016
    :cond_e9
    const-string v49, "ko"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_ea

    .line 3017
    const-string v21, "korean"

    goto/16 :goto_5f

    .line 3018
    :cond_ea
    const-string v49, "ja"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_eb

    .line 3019
    const-string v21, "Japanese"

    goto/16 :goto_5f

    .line 3020
    :cond_eb
    const-string v49, "kk"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_ec

    .line 3021
    const-string v21, "kazakh"

    goto/16 :goto_5f

    .line 3022
    :cond_ec
    const-string v49, "ms"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_ed

    .line 3023
    const-string v21, "bahasa malaysia"

    goto/16 :goto_5f

    .line 3024
    :cond_ed
    const-string v49, "in"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_ee

    .line 3025
    const-string v21, "indonesia"

    goto/16 :goto_5f

    .line 3026
    :cond_ee
    const-string v49, "vi"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_ef

    .line 3027
    const-string v21, "vietnamese"

    goto/16 :goto_5f

    .line 3028
    :cond_ef
    const-string v49, "az"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_f0

    .line 3029
    const-string v21, "azerbaijani"

    goto/16 :goto_5f

    .line 3030
    :cond_f0
    const-string v49, "hy"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_f1

    .line 3031
    const-string v21, "armenian"

    goto/16 :goto_5f

    .line 3032
    :cond_f1
    const-string v49, "ka"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_f2

    .line 3033
    const-string v21, "geogian"

    goto/16 :goto_5f

    .line 3034
    :cond_f2
    const-string v49, "th"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_f3

    .line 3035
    const-string v21, "thai"

    goto/16 :goto_5f

    .line 3036
    :cond_f3
    const-string v49, "ar"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_f4

    .line 3037
    const-string v21, "arabic"

    goto/16 :goto_5f

    .line 3038
    :cond_f4
    const-string v49, "iw"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_f5

    .line 3039
    const-string v21, "hebrew"

    goto/16 :goto_5f

    .line 3040
    :cond_f5
    const-string v49, "zh"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_f8

    .line 3041
    const-string v49, "tw"

    move-object/from16 v0, v20

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_f6

    .line 3042
    const-string v21, "chinese_traditional_taiwan"

    goto/16 :goto_5f

    .line 3043
    :cond_f6
    const-string v49, "hk"

    move-object/from16 v0, v20

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_f7

    .line 3044
    const-string v21, "chinese_traditional_hongkong"

    goto/16 :goto_5f

    .line 3045
    :cond_f7
    const-string v49, "cn"

    move-object/from16 v0, v20

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v49

    if-eqz v49, :cond_c5

    .line 3046
    const-string v21, "chinese_simplified"

    goto/16 :goto_5f

    .line 3049
    :cond_f8
    const-string v49, "CSCSettings"

    const-string v50, "Un-supported Language. Do nothing."

    invoke-static/range {v49 .. v50}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5f

    .line 3054
    :cond_f9
    const-string v5, "Settings.Main.Phone.DefLanguage"

    goto/16 :goto_60

    .line 3060
    :cond_fa
    const-string v49, "Settings.Main.Phone.DefLanguage"

    move-object/from16 v0, v49

    move-object/from16 v1, v25

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5e
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 32
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 3158
    const-string v20, ""

    .line 3163
    .local v20, "mStringValue":Ljava/lang/String;
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "PATH_DATE_FORMAT : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v30, v0

    const-string v31, "date_format"

    invoke-static/range {v30 .. v31}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v28, v0

    const-string v29, "date_format"

    invoke-static/range {v28 .. v29}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 3167
    const-string v28, "dd-MM-yyyy"

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 3168
    const-string v28, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    const-string v29, "ddmmyyyy"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 3178
    :goto_0
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "PATH_TIME_FORMAT : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v30, v0

    const-string v31, "time_12_24"

    invoke-static/range {v30 .. v31}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v28, v0

    const-string v29, "time_12_24"

    invoke-static/range {v28 .. v29}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 3182
    const-string v28, "24"

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 3183
    const-string v28, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    const-string v29, "24h"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 3192
    :goto_1
    const/4 v13, 0x0

    .line 3193
    .local v13, "mCallConnTone":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v28, v0

    const-string v29, "call_conn_tone"

    const/16 v30, 0x0

    invoke-static/range {v28 .. v30}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v13

    .line 3194
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "PATH_CALL_CONNECT_TONE : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3195
    if-nez v13, :cond_d

    .line 3196
    const-string v28, "Settings.Main.Sound.ExtraSound.ConnectTone"

    const-string v29, "off"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 3204
    :cond_0
    :goto_2
    const/4 v14, 0x0

    .line 3205
    .local v14, "mCallEndTone":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v28, v0

    const-string v29, "call_end_tone"

    const/16 v30, 0x0

    invoke-static/range {v28 .. v30}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v14

    .line 3206
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "PATH_CALL_END_TONE : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3207
    if-nez v14, :cond_e

    .line 3208
    const-string v28, "Settings.Main.Sound.ExtraSound.CallEndTone"

    const-string v29, "off"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 3216
    :cond_1
    :goto_3
    const/16 v17, 0x0

    .line 3217
    .local v17, "mMinMinder":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v28, v0

    const-string v29, "min_minder"

    const/16 v30, 0x0

    invoke-static/range {v28 .. v30}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v17

    .line 3218
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "PATH_CALL_MINUTEMIND : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3219
    if-nez v17, :cond_f

    .line 3220
    const-string v28, "Settings.Main.Sound.ExtraSound.MinuteMind"

    const-string v29, "off"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 3228
    :cond_2
    :goto_4
    const/4 v11, 0x0

    .line 3229
    .local v11, "mAlertOnCall":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v28, v0

    const-string v29, "alertoncall_mode"

    const/16 v30, 0x0

    invoke-static/range {v28 .. v30}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v11

    .line 3230
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "PATH_ALERT_ON_CALL : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3231
    if-nez v11, :cond_10

    .line 3232
    const-string v28, "Settings.Main.Phone.AlertOnCall"

    const-string v29, "off"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 3240
    :cond_3
    :goto_5
    const/4 v12, 0x0

    .line 3241
    .local v12, "mAutoRedial":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v28, v0

    const-string v29, "autoredial_mode"

    const/16 v30, 0x0

    invoke-static/range {v28 .. v30}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v12

    .line 3242
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "PATH_AUTO_REDIAL : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3243
    if-nez v12, :cond_11

    .line 3244
    const-string v28, "Settings.Main.Phone.AutoRedial"

    const-string v29, "off"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 3252
    :cond_4
    :goto_6
    const/16 v16, 0x0

    .line 3253
    .local v16, "mCallNoiseReduction":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v28, v0

    const-string v29, "call_noise_reduction"

    const/16 v30, 0x0

    invoke-static/range {v28 .. v30}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v16

    .line 3254
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "PATH_CALL_NOISE_REDUCTION : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3255
    if-nez v16, :cond_12

    .line 3256
    const-string v28, "Settings.Main.Phone.CallNoiseReduction"

    const-string v29, "off"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 3264
    :cond_5
    :goto_7
    const/4 v15, 0x0

    .line 3265
    .local v15, "mCallExtraVolume":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v28, v0

    const-string v29, "call_extra_volume"

    const/16 v30, 0x0

    invoke-static/range {v28 .. v30}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v15

    .line 3266
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "PATH_CALL_EXTRA_VOLUME : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3267
    if-nez v15, :cond_13

    .line 3268
    const-string v28, "Settings.Main.Phone.UseExtraVolume"

    const-string v29, "off"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 3276
    :cond_6
    :goto_8
    sget-object v27, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    .line 3277
    .local v27, "uriDefaultRingtone":Landroid/net/Uri;
    const/16 v19, 0x0

    .line 3278
    .local v19, "mRingtoneName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 3280
    .local v5, "RingtoneFileName":Ljava/lang/String;
    const-string v28, "null"

    invoke-virtual/range {v27 .. v28}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v28

    if-eqz v28, :cond_15

    .line 3282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    .line 3285
    const-string v28, "("

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v28

    if-eqz v28, :cond_14

    .line 3286
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "BeforeSubString : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3287
    const-string v28, "\\("

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    .line 3288
    .local v24, "strings":[Ljava/lang/String;
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "strings : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3289
    const/16 v28, 0x1

    aget-object v19, v24, v28

    .line 3291
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "AfterSubString : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3303
    .end local v24    # "strings":[Ljava/lang/String;
    :goto_9
    const/16 v28, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v29

    add-int/lit8 v29, v29, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    .line 3304
    .local v23, "r_mRingtoneName":Ljava/lang/String;
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "r_mRingtoneName : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3305
    move-object/from16 v5, v23

    .line 3307
    const-string v28, "Settings.Main.Sound.RingTone.src"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v5}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 3310
    sget-object v26, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    .line 3311
    .local v26, "uriDefaultNotification":Landroid/net/Uri;
    const/16 v18, 0x0

    .line 3312
    .local v18, "mNotificationName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 3314
    .local v4, "NotificationFileName":Ljava/lang/String;
    const-string v28, "null"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v28

    if-eqz v28, :cond_17

    .line 3316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    .line 3319
    const-string v28, "("

    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v28

    if-eqz v28, :cond_16

    .line 3320
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "BeforeSubString : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3321
    const-string v28, "\\("

    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    .line 3322
    .restart local v24    # "strings":[Ljava/lang/String;
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "strings : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3323
    const/16 v28, 0x1

    aget-object v18, v24, v28

    .line 3325
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "AfterSubString : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3338
    .end local v24    # "strings":[Ljava/lang/String;
    :goto_a
    const/16 v28, 0x0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v29

    add-int/lit8 v29, v29, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    .line 3339
    .local v22, "r_mNotificationName":Ljava/lang/String;
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "r_mNotificationName : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3340
    move-object/from16 v4, v22

    .line 3342
    const-string v28, "Settings.Main.Sound.MessageTone.src"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v4}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 3345
    sget-object v25, Landroid/provider/Settings$System;->DEFAULT_ALARM_ALERT_URI:Landroid/net/Uri;

    .line 3346
    .local v25, "uriDefaultAlarmtone":Landroid/net/Uri;
    const/4 v10, 0x0

    .line 3347
    .local v10, "mAlarmtoneName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 3349
    .local v3, "AlarmtoneFileName":Ljava/lang/String;
    const-string v28, "null"

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v28

    if-eqz v28, :cond_19

    .line 3351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 3354
    const-string v28, "("

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v28

    if-eqz v28, :cond_18

    .line 3355
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "BeforeSubString : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3356
    const-string v28, "\\("

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    .line 3357
    .restart local v24    # "strings":[Ljava/lang/String;
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "strings : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3358
    const/16 v28, 0x1

    aget-object v10, v24, v28

    .line 3360
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "AfterSubString : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3373
    .end local v24    # "strings":[Ljava/lang/String;
    :goto_b
    const/16 v28, 0x0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v29

    add-int/lit8 v29, v29, -0x1

    move/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    .line 3374
    .local v21, "r_mAlarmtoneName":Ljava/lang/String;
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "r_mAlarmtoneName : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3375
    move-object/from16 v3, v21

    .line 3377
    const-string v28, "Settings.Main.Sound.AlarmTone.src"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 3380
    const-string v28, "automatic"

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 3381
    .local v6, "automaticCompare":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v28

    move-object/from16 v0, v28

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    .line 3382
    .local v8, "currentLang":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v28

    move-object/from16 v0, v28

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v7

    .line 3383
    .local v7, "currentCountry":Ljava/lang/String;
    const/4 v9, 0x0

    .line 3385
    .local v9, "currentLocale":Ljava/lang/String;
    if-eqz v8, :cond_1a

    if-eqz v7, :cond_1a

    .line 3386
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "_"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 3387
    const-string v28, "CSCSettings"

    const-string v29, "ISO type is used in encode()."

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3500
    :cond_7
    :goto_c
    if-eqz v9, :cond_4d

    .line 3501
    const-string v28, "Settings.Main.Phone.DefLanguage"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v9}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 3511
    :goto_d
    return-void

    .line 3169
    .end local v3    # "AlarmtoneFileName":Ljava/lang/String;
    .end local v4    # "NotificationFileName":Ljava/lang/String;
    .end local v5    # "RingtoneFileName":Ljava/lang/String;
    .end local v6    # "automaticCompare":Ljava/lang/String;
    .end local v7    # "currentCountry":Ljava/lang/String;
    .end local v8    # "currentLang":Ljava/lang/String;
    .end local v9    # "currentLocale":Ljava/lang/String;
    .end local v10    # "mAlarmtoneName":Ljava/lang/String;
    .end local v11    # "mAlertOnCall":I
    .end local v12    # "mAutoRedial":I
    .end local v13    # "mCallConnTone":I
    .end local v14    # "mCallEndTone":I
    .end local v15    # "mCallExtraVolume":I
    .end local v16    # "mCallNoiseReduction":I
    .end local v17    # "mMinMinder":I
    .end local v18    # "mNotificationName":Ljava/lang/String;
    .end local v19    # "mRingtoneName":Ljava/lang/String;
    .end local v21    # "r_mAlarmtoneName":Ljava/lang/String;
    .end local v22    # "r_mNotificationName":Ljava/lang/String;
    .end local v23    # "r_mRingtoneName":Ljava/lang/String;
    .end local v25    # "uriDefaultAlarmtone":Landroid/net/Uri;
    .end local v26    # "uriDefaultNotification":Landroid/net/Uri;
    .end local v27    # "uriDefaultRingtone":Landroid/net/Uri;
    :cond_8
    const-string v28, "MM-dd-yyyy"

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 3170
    const-string v28, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    const-string v29, "mmddyyyy"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3171
    :cond_9
    const-string v28, "yyyy-MM-dd"

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 3172
    const-string v28, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    const-string v29, "yyyymmdd"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3174
    :cond_a
    const-string v28, "CSCSettings"

    const-string v29, "PATH_DATE_FORMAT Not set"

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3184
    :cond_b
    const-string v28, "12"

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 3185
    const-string v28, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    const-string v29, "12h"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3187
    :cond_c
    const-string v28, "CSCSettings"

    const-string v29, "PATH_TIME_FORMAT Not set"

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3197
    .restart local v13    # "mCallConnTone":I
    :cond_d
    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v13, v0, :cond_0

    .line 3198
    const-string v28, "Settings.Main.Sound.ExtraSound.ConnectTone"

    const-string v29, "on"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 3209
    .restart local v14    # "mCallEndTone":I
    :cond_e
    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v14, v0, :cond_1

    .line 3210
    const-string v28, "Settings.Main.Sound.ExtraSound.CallEndTone"

    const-string v29, "on"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 3221
    .restart local v17    # "mMinMinder":I
    :cond_f
    const/16 v28, 0x1

    move/from16 v0, v17

    move/from16 v1, v28

    if-ne v0, v1, :cond_2

    .line 3222
    const-string v28, "Settings.Main.Sound.ExtraSound.MinuteMind"

    const-string v29, "on"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 3233
    .restart local v11    # "mAlertOnCall":I
    :cond_10
    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v11, v0, :cond_3

    .line 3234
    const-string v28, "Settings.Main.Phone.AlertOnCall"

    const-string v29, "on"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 3245
    .restart local v12    # "mAutoRedial":I
    :cond_11
    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v12, v0, :cond_4

    .line 3246
    const-string v28, "Settings.Main.Phone.AutoRedial"

    const-string v29, "on"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 3257
    .restart local v16    # "mCallNoiseReduction":I
    :cond_12
    const/16 v28, 0x1

    move/from16 v0, v16

    move/from16 v1, v28

    if-ne v0, v1, :cond_5

    .line 3258
    const-string v28, "Settings.Main.Phone.CallNoiseReduction"

    const-string v29, "on"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 3269
    .restart local v15    # "mCallExtraVolume":I
    :cond_13
    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v15, v0, :cond_6

    .line 3270
    const-string v28, "Settings.Main.Phone.UseExtraVolume"

    const-string v29, "on"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 3293
    .restart local v5    # "RingtoneFileName":Ljava/lang/String;
    .restart local v19    # "mRingtoneName":Ljava/lang/String;
    .restart local v27    # "uriDefaultRingtone":Landroid/net/Uri;
    :cond_14
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Ringtone is abnormal : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3297
    const-string v19, "Not Set"

    goto/16 :goto_9

    .line 3300
    :cond_15
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "uriDefaultRingtone is null : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3301
    const-string v19, "Not Set"

    goto/16 :goto_9

    .line 3327
    .restart local v4    # "NotificationFileName":Ljava/lang/String;
    .restart local v18    # "mNotificationName":Ljava/lang/String;
    .restart local v23    # "r_mRingtoneName":Ljava/lang/String;
    .restart local v26    # "uriDefaultNotification":Landroid/net/Uri;
    :cond_16
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Notification is abnormal : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3331
    const-string v18, "Not Set"

    goto/16 :goto_a

    .line 3334
    :cond_17
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "uriDefaultRingtone is null : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3335
    const-string v18, "Not Set"

    goto/16 :goto_a

    .line 3362
    .restart local v3    # "AlarmtoneFileName":Ljava/lang/String;
    .restart local v10    # "mAlarmtoneName":Ljava/lang/String;
    .restart local v22    # "r_mNotificationName":Ljava/lang/String;
    .restart local v25    # "uriDefaultAlarmtone":Landroid/net/Uri;
    :cond_18
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Alarmtone is abnormal : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3366
    const-string v10, "Not Set"

    goto/16 :goto_b

    .line 3369
    :cond_19
    const-string v28, "CSCSettings"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "uriDefaultAlarmtone is null : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3370
    const-string v10, "Not Set"

    goto/16 :goto_b

    .line 3388
    .restart local v6    # "automaticCompare":Ljava/lang/String;
    .restart local v7    # "currentCountry":Ljava/lang/String;
    .restart local v8    # "currentLang":Ljava/lang/String;
    .restart local v9    # "currentLocale":Ljava/lang/String;
    .restart local v21    # "r_mAlarmtoneName":Ljava/lang/String;
    :cond_1a
    const-string v28, "en"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_1d

    .line 3389
    const-string v28, "ph"

    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_1b

    .line 3390
    const-string v8, "english_philippines"

    goto/16 :goto_c

    .line 3391
    :cond_1b
    const-string v28, "us"

    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_1c

    .line 3392
    const-string v8, "english_us"

    goto/16 :goto_c

    .line 3394
    :cond_1c
    const-string v8, "english"

    goto/16 :goto_c

    .line 3396
    :cond_1d
    const-string v28, "de"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_1e

    .line 3397
    const-string v8, "german"

    goto/16 :goto_c

    .line 3398
    :cond_1e
    const-string v28, "es"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_20

    .line 3399
    const-string v28, "us"

    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_1f

    .line 3400
    const-string v8, "spanish_latin"

    goto/16 :goto_c

    .line 3402
    :cond_1f
    const-string v8, "spanish"

    goto/16 :goto_c

    .line 3404
    :cond_20
    const-string v28, "fr"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_21

    .line 3405
    const-string v8, "french"

    goto/16 :goto_c

    .line 3406
    :cond_21
    const-string v28, "it"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_22

    .line 3407
    const-string v8, "italian"

    goto/16 :goto_c

    .line 3408
    :cond_22
    const-string v28, "nl"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_23

    .line 3409
    const-string v8, "dutch"

    goto/16 :goto_c

    .line 3410
    :cond_23
    const-string v28, "pt"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_25

    .line 3411
    const-string v28, "br"

    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_24

    .line 3412
    const-string v8, "portuguese_latin"

    goto/16 :goto_c

    .line 3414
    :cond_24
    const-string v8, "portuguese"

    goto/16 :goto_c

    .line 3416
    :cond_25
    const-string v28, "pl"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_26

    .line 3417
    const-string v8, "polish"

    goto/16 :goto_c

    .line 3418
    :cond_26
    const-string v28, "cs"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_27

    .line 3419
    const-string v8, "czech"

    goto/16 :goto_c

    .line 3420
    :cond_27
    const-string v28, "da"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_28

    .line 3421
    const-string v8, "danish"

    goto/16 :goto_c

    .line 3422
    :cond_28
    const-string v28, "fi"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_29

    .line 3423
    const-string v8, "finnish"

    goto/16 :goto_c

    .line 3424
    :cond_29
    const-string v28, "no"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_2a

    .line 3425
    const-string v8, "norwegian"

    goto/16 :goto_c

    .line 3426
    :cond_2a
    const-string v28, "sv"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_2b

    .line 3427
    const-string v8, "swedish"

    goto/16 :goto_c

    .line 3428
    :cond_2b
    const-string v28, "ro"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_2c

    .line 3429
    const-string v8, "romanian"

    goto/16 :goto_c

    .line 3430
    :cond_2c
    const-string v28, "sk"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_2d

    .line 3431
    const-string v8, "slovakian"

    goto/16 :goto_c

    .line 3432
    :cond_2d
    const-string v28, "hu"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_2e

    .line 3433
    const-string v8, "hungarian"

    goto/16 :goto_c

    .line 3434
    :cond_2e
    const-string v28, "sr"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_2f

    .line 3435
    const-string v8, "serbian"

    goto/16 :goto_c

    .line 3436
    :cond_2f
    const-string v28, "el"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_30

    .line 3437
    const-string v8, "greek"

    goto/16 :goto_c

    .line 3438
    :cond_30
    const-string v28, "sq"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_31

    .line 3439
    const-string v8, "albanian"

    goto/16 :goto_c

    .line 3440
    :cond_31
    const-string v28, "ga"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_32

    .line 3441
    const-string v8, "irish"

    goto/16 :goto_c

    .line 3442
    :cond_32
    const-string v28, "mk"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_33

    .line 3443
    const-string v8, "macedonian"

    goto/16 :goto_c

    .line 3444
    :cond_33
    const-string v28, "mt"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_34

    .line 3445
    const-string v8, "maltese"

    goto/16 :goto_c

    .line 3446
    :cond_34
    const-string v28, "tr"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_35

    .line 3447
    const-string v8, "turkish"

    goto/16 :goto_c

    .line 3448
    :cond_35
    const-string v28, "sl"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_36

    .line 3449
    const-string v8, "slovenian"

    goto/16 :goto_c

    .line 3450
    :cond_36
    const-string v28, "is"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_37

    .line 3451
    const-string v8, "icelandic"

    goto/16 :goto_c

    .line 3452
    :cond_37
    const-string v28, "hr"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_38

    .line 3453
    const-string v8, "croatian"

    goto/16 :goto_c

    .line 3454
    :cond_38
    const-string v28, "ru"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_39

    .line 3455
    const-string v8, "russian"

    goto/16 :goto_c

    .line 3456
    :cond_39
    const-string v28, "bg"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_3a

    .line 3457
    const-string v8, "bulgarian"

    goto/16 :goto_c

    .line 3458
    :cond_3a
    const-string v28, "et"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_3b

    .line 3459
    const-string v8, "estonian"

    goto/16 :goto_c

    .line 3460
    :cond_3b
    const-string v28, "lv"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_3c

    .line 3461
    const-string v8, "latvian"

    goto/16 :goto_c

    .line 3462
    :cond_3c
    const-string v28, "lt"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_3d

    .line 3463
    const-string v8, "lithuanian"

    goto/16 :goto_c

    .line 3464
    :cond_3d
    const-string v28, "uk"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_3e

    .line 3465
    const-string v8, "ukrainian"

    goto/16 :goto_c

    .line 3466
    :cond_3e
    const-string v28, "ko"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_3f

    .line 3467
    const-string v8, "korean"

    goto/16 :goto_c

    .line 3468
    :cond_3f
    const-string v28, "ja"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_40

    .line 3469
    const-string v8, "Japanese"

    goto/16 :goto_c

    .line 3470
    :cond_40
    const-string v28, "kk"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_41

    .line 3471
    const-string v8, "kazakh"

    goto/16 :goto_c

    .line 3472
    :cond_41
    const-string v28, "ms"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_42

    .line 3473
    const-string v8, "bahasa malaysia"

    goto/16 :goto_c

    .line 3474
    :cond_42
    const-string v28, "in"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_43

    .line 3475
    const-string v8, "indonesia"

    goto/16 :goto_c

    .line 3476
    :cond_43
    const-string v28, "vi"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_44

    .line 3477
    const-string v8, "vietnamese"

    goto/16 :goto_c

    .line 3478
    :cond_44
    const-string v28, "az"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_45

    .line 3479
    const-string v8, "azerbaijani"

    goto/16 :goto_c

    .line 3480
    :cond_45
    const-string v28, "hy"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_46

    .line 3481
    const-string v8, "armenian"

    goto/16 :goto_c

    .line 3482
    :cond_46
    const-string v28, "ka"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_47

    .line 3483
    const-string v8, "geogian"

    goto/16 :goto_c

    .line 3484
    :cond_47
    const-string v28, "th"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_48

    .line 3485
    const-string v8, "thai"

    goto/16 :goto_c

    .line 3486
    :cond_48
    const-string v28, "ar"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_49

    .line 3487
    const-string v8, "arabic"

    goto/16 :goto_c

    .line 3488
    :cond_49
    const-string v28, "zh"

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_4c

    .line 3489
    const-string v28, "tw"

    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_4a

    .line 3490
    const-string v8, "chinese_traditional_taiwan"

    goto/16 :goto_c

    .line 3491
    :cond_4a
    const-string v28, "hk"

    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_4b

    .line 3492
    const-string v8, "chinese_traditional_hongkong"

    goto/16 :goto_c

    .line 3493
    :cond_4b
    const-string v28, "cn"

    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 3494
    const-string v8, "chinese_simplified"

    goto/16 :goto_c

    .line 3497
    :cond_4c
    const-string v28, "CSCSettings"

    const-string v29, "Un-supported Language. Do nothing."

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 3502
    :cond_4d
    if-eqz v8, :cond_4e

    .line 3503
    const-string v28, "Settings.Main.Phone.DefLanguage"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v8}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 3505
    :cond_4e
    const-string v28, "CSCSettings"

    const-string v29, "Un-supported Language : "

    invoke-static/range {v28 .. v29}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d
.end method

.method public update()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 568
    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 570
    .local v2, "salesCode":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v3, :cond_0

    .line 571
    new-instance v3, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 576
    :cond_0
    const-string v3, "CSCSettings"

    const-string v4, "Setting Data roaming mode"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    const-string v3, "national"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Setting_DataRoamingOption"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 580
    const-string v3, "CSCSettings"

    const-string v4, "Setting Data Roaming mode : National roaming only"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "data_national_roaming_mode"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 594
    :goto_0
    const-string v3, "TFG"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 595
    const-string v3, "ril.product_code"

    const-string v4, "none"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 596
    .local v1, "productCode":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v3, "EBE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 597
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "data_roaming"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 604
    .end local v1    # "productCode":Ljava/lang/String;
    :cond_1
    :goto_1
    const-string v3, "CSCSettings"

    const-string v4, "Setting Call end tone"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.ExtraSound.CallEndTone"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_29

    .line 607
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.ExtraSound.CallEndTone"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "On"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 608
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_end_tone"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 616
    :cond_2
    :goto_2
    const-string v3, "CSCSettings"

    const-string v4, "Setting Call connect tone"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.ExtraSound.ConnectTone"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2b

    .line 619
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.ExtraSound.ConnectTone"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "On"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2a

    .line 620
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_conn_tone"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 628
    :cond_3
    :goto_3
    const-string v3, "CSCSettings"

    const-string v4, "Setting Call MinuteMind"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.ExtraSound.MinuteMind"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2d

    .line 631
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.ExtraSound.MinuteMind"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "On"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2c

    .line 632
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "min_minder"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 641
    :cond_4
    :goto_4
    const-string v3, "CSCSettings"

    const-string v4, "Setting Alert on call"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.AlertOnCall"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2f

    .line 643
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.AlertOnCall"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "On"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 644
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "alertoncall_mode"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 653
    :cond_5
    :goto_5
    const-string v3, "CSCSettings"

    const-string v4, "Setting Auto redial"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.AutoRedial"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_31

    .line 655
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.AutoRedial"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "On"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_30

    .line 656
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "autoredial_mode"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 664
    :cond_6
    :goto_6
    const-string v3, "CSCSettings"

    const-string v4, "Setting Call noise reduction"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.CallNoiseReduction"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_33

    .line 667
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.CallNoiseReduction"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "On"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 668
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_noise_reduction"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 676
    :cond_7
    :goto_7
    const-string v3, "CSCSettings"

    const-string v4, "Setting Call extra volume"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.UseExtraVolume"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_35

    .line 679
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.UseExtraVolume"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "On"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_34

    .line 680
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_extra_volume"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 706
    :cond_8
    :goto_8
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.VisiblePassword"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_37

    .line 707
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.VisiblePassword"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.VisiblePassword"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_36

    .line 709
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "show_password"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 717
    :cond_9
    :goto_9
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.UnknownSource"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_39

    .line 718
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.UnknownSource"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.UnknownSource"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_38

    .line 720
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "install_non_market_apps"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 728
    :cond_a
    :goto_a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.DateTimeFormat.TimezoneUpdate"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3b

    .line 729
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.DateTimeFormat.TimezoneUpdate"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.DateTimeFormat.TimezoneUpdate"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3a

    .line 731
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "auto_time"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 732
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "auto_time_zone"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 756
    :cond_b
    :goto_b
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.MediaVolume"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3c

    .line 757
    const-string v3, "CSCSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Settings.Main.Sound.MediaVolume"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v6, "Settings.Main.Sound.MediaVolume"

    invoke-virtual {v5, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    const/4 v3, 0x3

    const-string v4, "Settings.Main.Sound.MediaVolume"

    invoke-virtual {p0, v3, v4}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateVolume(ILjava/lang/String;)V

    .line 764
    :goto_c
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.MsgToneVolume"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3d

    .line 765
    const-string v3, "CSCSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Settings.Main.Sound.NotificationVolume"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v6, "Settings.Main.Sound.MsgToneVolume"

    invoke-virtual {v5, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    const/4 v3, 0x5

    const-string v4, "Settings.Main.Sound.MsgToneVolume"

    invoke-virtual {p0, v3, v4}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateVolume(ILjava/lang/String;)V

    .line 774
    :goto_d
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.RngVolume"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3e

    .line 775
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Sound.RingVolume"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    const-string v3, "Settings.Main.Sound.RngVolume"

    invoke-virtual {p0, v9, v3}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateVolume(ILjava/lang/String;)V

    .line 782
    :goto_e
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.ALARMVolume"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3f

    .line 783
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Sound.AlarmVolume"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    const-string v3, "Settings.Main.Sound.ALARMVolume"

    invoke-virtual {p0, v10, v3}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateVolume(ILjava/lang/String;)V

    .line 790
    :goto_f
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.RngToneAlertType"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_40

    .line 791
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Sound.RngToneAlertType"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    const-string v3, "Settings.Main.Sound.RngToneAlertType"

    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateSilentMode(Ljava/lang/String;)V

    .line 797
    :goto_10
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_47

    .line 798
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.BackLightTime"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "15sec"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_41

    .line 800
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "screen_off_timeout"

    const/16 v5, 0x3a98

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 826
    :cond_c
    :goto_11
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.TouchTone"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_49

    .line 827
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Sound.TouchTone"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.TouchTone"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_48

    .line 829
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "sound_effects_enabled"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 837
    :cond_d
    :goto_12
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.KeyTone.KeyVolume"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4a

    .line 838
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Sound.KeyTone.KeyVolume"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    const-string v3, "Settings.Main.Sound.KeyTone.KeyVolume"

    invoke-virtual {p0, v7, v3}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateVolume(ILjava/lang/String;)V

    .line 845
    :goto_13
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.WorldTime.TimeZone"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4b

    .line 846
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.WorldTime.TimeZone"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 848
    .local v0, "alarm":Landroid/app/AlarmManager;
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.WorldTime.TimeZone"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    .line 853
    .end local v0    # "alarm":Landroid/app/AlarmManager;
    :goto_14
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4e

    .line 854
    const-string v3, "CSCSettings"

    const-string v4, "Setting Date format"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ddmmyyyy"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4c

    .line 856
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "date_format"

    const-string v5, "dd-MM-yyyy"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 861
    :cond_e
    :goto_15
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "regional"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 867
    :goto_16
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_50

    .line 868
    const-string v3, "CSCSettings"

    const-string v4, "Setting time format"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "24h"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4f

    .line 870
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "time_12_24"

    const-string v5, "24"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 878
    :cond_f
    :goto_17
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.PowerSavingMode"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_52

    .line 879
    const-string v3, "CSCSettings"

    const-string v4, "Setting Power Saving Mode"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.PowerSavingMode"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_51

    .line 881
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "psm_switch"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 889
    :cond_10
    :goto_18
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.UseSensorAiding"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_54

    .line 890
    const-string v3, "CSCSettings"

    const-string v4, "Setting PDR select"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.UseSensorAiding"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_53

    .line 892
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "location_pdr_enabled"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 900
    :cond_11
    :goto_19
    const-string v3, "CSCSettings"

    const-string v4, "Setting WifiProfile Info Called"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateWifiProfileInfo()V

    .line 904
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Wifi.SleepPolicy"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_57

    .line 905
    const-string v3, "CSCSettings"

    const-string v4, "Settings Wifi SleepPolicy"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Wifi.SleepPolicy"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "always"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_55

    .line 907
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "wifi_sleep_policy"

    invoke-static {v3, v4, v9}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 920
    :cond_12
    :goto_1a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Wifi.WifiEnableStatus"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_59

    .line 921
    const-string v3, "CSCSettings"

    const-string v4, "Setting Wifi On/Off"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Wifi.WifiEnableStatus"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_58

    .line 923
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "wifi_on"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 924
    const-string v3, "CSCSettings"

    const-string v4, "wifion csc is on "

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    :cond_13
    :goto_1b
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.CSC_WIFI_UPDATE_DONE"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 936
    :goto_1c
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Wifi.CheckInternet"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5b

    .line 937
    const-string v3, "CSCSettings"

    const-string v4, "Setting Wifi CheckInternet On/Off"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Wifi.CheckInternet"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5a

    .line 939
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "wifi_watchdog_poor_network_test_enabled"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 941
    const-string v3, "CSCSettings"

    const-string v4, "Setting Wifi CheckInternet On"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    :cond_14
    :goto_1d
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.GPS.GPSActivation"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5f

    .line 953
    const-string v3, "CSCSettings"

    const-string v4, "Setting GPS Satelites"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "location_providers_allowed"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "network"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5d

    .line 956
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.GPS.GPSActivation"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5c

    .line 957
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "location_providers_allowed"

    const-string v5, "gps,network"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 974
    :cond_15
    :goto_1e
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.AutoSyncCommon"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_61

    .line 975
    const-string v3, "CSCSettings"

    const-string v4, "Setting.Main.Phone.AutoSyncCommon"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 976
    const-string v3, "On"

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "Settings.Main.Phone.AutoSyncCommon"

    invoke-virtual {v4, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_60

    .line 977
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v7}, Landroid/content/ContentResolver;->setMasterSyncAutomatically(Z)V

    .line 984
    :cond_16
    :goto_1f
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.SmartStay"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_63

    .line 985
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.SmartStay"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.SmartStay"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_62

    .line 987
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "intelligent_sleep_mode"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 996
    :cond_17
    :goto_20
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_VoiceCall_EnablePrefix4LongDistanceCallAs"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_18

    .line 1000
    const-string v3, "CSCSettings"

    const-string v4, "Setting Default AutoCSP Settings"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "autocsp_enabled"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1002
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "data_operator_code"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v5

    const-string v6, "CscFeature_VoiceCall_EnablePrefix4LongDistanceCallAs"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v5

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1025
    :cond_18
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateCountryCertInfo()V

    .line 1027
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_24

    .line 1030
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.Activation"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_65

    .line 1031
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.Motion.MotionActivation"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.Activation"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_64

    .line 1033
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_engine"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1040
    :cond_19
    :goto_21
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.GlanceView"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_67

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_GLANCE_VIEW"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_67

    .line 1043
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.Motion.GlanceView"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.GlanceView"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_66

    .line 1045
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "air_motion_glance_view"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1052
    :cond_1a
    :goto_22
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.DirectCall"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_69

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PICK_UP_TO_CALL_OUT"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_69

    .line 1055
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Motion.DirctCall"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.DirectCall"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_68

    .line 1057
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_pick_up_to_call_out"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1066
    :cond_1b
    :goto_23
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.SmartAlert"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6b

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PICK_UP"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6b

    .line 1069
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.Motion.SmartAlert"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.SmartAlert"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6a

    .line 1071
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_pick_up"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1078
    :cond_1c
    :goto_24
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.DoubleTapToTop"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6d

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_DOUBLE_TAP"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6d

    .line 1081
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.Motion.DoulbeTap"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.DoubleTapToTop"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6c

    .line 1083
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_double_tap"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1090
    :cond_1d
    :goto_25
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.Tilt"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6f

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_TILT"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6f

    .line 1093
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.Motion.Tilt"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1094
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.Tilt"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6e

    .line 1095
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_zooming"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1099
    :cond_1e
    :goto_26
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_zooming_guide_show_again"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1105
    :goto_27
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PanToMove"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_71

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PAN"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_71

    .line 1108
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.Motion.PanToMove"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PanToMove"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_70

    .line 1110
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_panning"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1117
    :cond_1f
    :goto_28
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PanToBrowse"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_73

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PAN_TO_BROWSE_IMAGE"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_73

    .line 1120
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.Motion.PanToBrowse"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PanToBrowse"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_72

    .line 1122
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_pan_to_browse_image"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1131
    :cond_20
    :goto_29
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PanToShake"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_75

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_SHAKE"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_75

    .line 1134
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.Motion.Shake"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1135
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PanToShake"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_74

    .line 1136
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_shake"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1140
    :cond_21
    :goto_2a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_shake_scan_guide_show_again"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1142
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_shake_refresh_guide_show_again"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1148
    :goto_2b
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.TurnOver"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_77

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_TRUN_OVER"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_77

    .line 1151
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.Motion.TurnOver"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.TurnOver"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_76

    .line 1153
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_overturn"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1159
    :cond_22
    :goto_2c
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PalmSwipe"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_79

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PALM_SWIPE"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_79

    .line 1162
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.Motion.PalmSwipe"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PalmSwipe"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_78

    .line 1164
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "surface_palm_swipe"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1171
    :cond_23
    :goto_2d
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PalmTouch"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7b

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PALM_TOUCH"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7b

    .line 1174
    const-string v3, "CSCSettings"

    const-string v4, "Settings.Main.Phone.Motion.PalmTouch"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1175
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PalmTouch"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7a

    .line 1176
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "surface_palm_touch"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1185
    :cond_24
    :goto_2e
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v4, "PREF_RINGTONE_SET"

    invoke-interface {v3, v4, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    .line 1186
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v4, "PREF_NOTIFICATION_SET"

    invoke-interface {v3, v4, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    .line 1187
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v4, "PREF_ALARMTONE_SET"

    invoke-interface {v3, v4, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    .line 1188
    invoke-virtual {p0, v7}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateRingtones(I)V

    .line 1189
    invoke-virtual {p0, v9}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateRingtones(I)V

    .line 1190
    invoke-virtual {p0, v10}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateRingtones(I)V

    .line 1192
    return-void

    .line 583
    :cond_25
    const-string v3, "all"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Setting_DataRoamingOption"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 585
    const-string v3, "CSCSettings"

    const-string v4, "Setting Data Roaming mode : All networks"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "data_national_roaming_mode"

    invoke-static {v3, v4, v9}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 589
    :cond_26
    const-string v3, "CSCSettings"

    const-string v4, "Setting Data Roaming mode : Disabled"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "data_national_roaming_mode"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 599
    :cond_27
    const-string v3, "EBE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 600
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "data_roaming"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 609
    :cond_28
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.ExtraSound.CallEndTone"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 610
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_end_tone"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_2

    .line 612
    :cond_29
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_end_tone"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 613
    const-string v3, "CSCSettings"

    const-string v4, "Call end tone not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 621
    :cond_2a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.ExtraSound.ConnectTone"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 622
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_conn_tone"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_3

    .line 624
    :cond_2b
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_conn_tone"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 625
    const-string v3, "CSCSettings"

    const-string v4, "Call connect tone not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 633
    :cond_2c
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.ExtraSound.MinuteMind"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 634
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "min_minder"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_4

    .line 636
    :cond_2d
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "min_minder"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 637
    const-string v3, "CSCSettings"

    const-string v4, "Call  MinuteMind not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 645
    :cond_2e
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.AlertOnCall"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 646
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "alertoncall_mode"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_5

    .line 648
    :cond_2f
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "alertoncall_mode"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 649
    const-string v3, "CSCSettings"

    const-string v4, "Alert on call not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 657
    :cond_30
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.AutoRedial"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 658
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "autoredial_mode"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_6

    .line 660
    :cond_31
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "autoredial_mode"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 661
    const-string v3, "CSCSettings"

    const-string v4, "Auto redial not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 669
    :cond_32
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.CallNoiseReduction"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 670
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_noise_reduction"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_7

    .line 672
    :cond_33
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_noise_reduction"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 673
    const-string v3, "CSCSettings"

    const-string v4, "Call noise reduction not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 681
    :cond_34
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.UseExtraVolume"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 682
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_extra_volume"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_8

    .line 684
    :cond_35
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "call_extra_volume"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 685
    const-string v3, "CSCSettings"

    const-string v4, "Call extra volume not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 710
    :cond_36
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.VisiblePassword"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 711
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "show_password"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_9

    .line 713
    :cond_37
    const-string v3, "CSCSettings"

    const-string v4, "VisiblePassword is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 721
    :cond_38
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.UnknownSource"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 722
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "install_non_market_apps"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_a

    .line 724
    :cond_39
    const-string v3, "CSCSettings"

    const-string v4, "UnknownSource is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 733
    :cond_3a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.DateTimeFormat.TimezoneUpdate"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 734
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "auto_time"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 735
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "auto_time_zone"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_b

    .line 738
    :cond_3b
    const-string v3, "CSCSettings"

    const-string v4, "Timezone Update is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 760
    :cond_3c
    const-string v3, "CSCSettings"

    const-string v4, "Media volume is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 770
    :cond_3d
    const-string v3, "CSCSettings"

    const-string v4, "Notification volume is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 778
    :cond_3e
    const-string v3, "CSCSettings"

    const-string v4, "Ring Volume is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 786
    :cond_3f
    const-string v3, "CSCSettings"

    const-string v4, "ALARM Volume is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_f

    .line 794
    :cond_40
    const-string v3, "CSCSettings"

    const-string v4, "RngToneAlertType is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    .line 801
    :cond_41
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "30sec"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_42

    .line 802
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "screen_off_timeout"

    const/16 v5, 0x7530

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_11

    .line 803
    :cond_42
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "1min"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_43

    .line 804
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "screen_off_timeout"

    const v5, 0xea60

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_11

    .line 805
    :cond_43
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "2min"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_44

    .line 806
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "screen_off_timeout"

    const v5, 0x1d4c0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_11

    .line 807
    :cond_44
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "5min"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_45

    .line 808
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "screen_off_timeout"

    const v5, 0x493e0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_11

    .line 809
    :cond_45
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "10min"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_46

    .line 810
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "screen_off_timeout"

    const v5, 0x927c0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_11

    .line 811
    :cond_46
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "30min"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    goto/16 :goto_11

    .line 822
    :cond_47
    const-string v3, "CSCSettings"

    const-string v4, "BackLight Time is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_11

    .line 830
    :cond_48
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Sound.TouchTone"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 831
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "sound_effects_enabled"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_12

    .line 833
    :cond_49
    const-string v3, "CSCSettings"

    const-string v4, "Touch Sounds is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    .line 841
    :cond_4a
    const-string v3, "CSCSettings"

    const-string v4, "KeyTones is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_13

    .line 850
    :cond_4b
    const-string v3, "CSCSettings"

    const-string v4, "Time Zone format not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_14

    .line 857
    :cond_4c
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "mmddyyyy"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4d

    .line 858
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "date_format"

    const-string v5, "MM-dd-yyyy"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_15

    .line 859
    :cond_4d
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "yyyymmdd"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 860
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "date_format"

    const-string v5, "yyyy-MM-dd"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_15

    .line 863
    :cond_4e
    const-string v3, "CSCSettings"

    const-string v4, "Date format not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_16

    .line 871
    :cond_4f
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "12h"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 872
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "time_12_24"

    const-string v5, "12"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_17

    .line 874
    :cond_50
    const-string v3, "CSCSettings"

    const-string v4, "Time format not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_17

    .line 882
    :cond_51
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.PowerSavingMode"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 883
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "psm_switch"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_18

    .line 885
    :cond_52
    const-string v3, "CSCSettings"

    const-string v4, "Setting Power Saving Mode is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_18

    .line 893
    :cond_53
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.UseSensorAiding"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 894
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "location_pdr_enabled"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_19

    .line 896
    :cond_54
    const-string v3, "CSCSettings"

    const-string v4, "Setting PDR is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_19

    .line 909
    :cond_55
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Wifi.SleepPolicy"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "plugged"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_56

    .line 910
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "wifi_sleep_policy"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1a

    .line 912
    :cond_56
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Wifi.SleepPolicy"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "never"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 913
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "wifi_sleep_policy"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1a

    .line 916
    :cond_57
    const-string v3, "CSCSettings"

    const-string v4, "Wifi SleepPolicy is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1a

    .line 925
    :cond_58
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Wifi.WifiEnableStatus"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 926
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "wifi_on"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 927
    const-string v3, "CSCSettings"

    const-string v4, "wifion csc is off "

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1b

    .line 932
    :cond_59
    const-string v3, "CSCSettings"

    const-string v4, "Setting Wifi On/Off is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1c

    .line 942
    :cond_5a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Wifi.CheckInternet"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 943
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "wifi_watchdog_poor_network_test_enabled"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 945
    const-string v3, "CSCSettings"

    const-string v4, "Setting Wifi CheckInternet Off"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1d

    .line 948
    :cond_5b
    const-string v3, "CSCSettings"

    const-string v4, "Setting Wifi CheckInternet On/Off is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1d

    .line 959
    :cond_5c
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.GPS.GPSActivation"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 960
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "location_providers_allowed"

    const-string v5, "network"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_1e

    .line 963
    :cond_5d
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.GPS.GPSActivation"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5e

    .line 964
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "location_providers_allowed"

    const-string v5, "gps"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_1e

    .line 966
    :cond_5e
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.GPS.GPSActivation"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 967
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "location_providers_allowed"

    const-string v5, "0"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_1e

    .line 971
    :cond_5f
    const-string v3, "CSCSettings"

    const-string v4, "GPS Satelites is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1e

    .line 978
    :cond_60
    const-string v3, "Off"

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "Settings.Main.Phone.AutoSyncCommon"

    invoke-virtual {v4, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 979
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v8}, Landroid/content/ContentResolver;->setMasterSyncAutomatically(Z)V

    goto/16 :goto_1f

    .line 981
    :cond_61
    const-string v3, "CSCSettings"

    const-string v4, "AutoSyncCommon not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1f

    .line 988
    :cond_62
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.SmartStay"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 989
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "intelligent_sleep_mode"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_20

    .line 991
    :cond_63
    const-string v3, "CSCSettings"

    const-string v4, "SmartStay is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_20

    .line 1034
    :cond_64
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.Activation"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 1035
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_engine"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_21

    .line 1037
    :cond_65
    const-string v3, "CSCSettings"

    const-string v4, "MotionActivation is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_21

    .line 1046
    :cond_66
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.GlanceView"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 1047
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "air_motion_glance_view"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_22

    .line 1049
    :cond_67
    const-string v3, "CSCSettings"

    const-string v4, "GlanceView is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_22

    .line 1059
    :cond_68
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.DirectCall"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 1060
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_pick_up_to_call_out"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_23

    .line 1063
    :cond_69
    const-string v3, "CSCSettings"

    const-string v4, "DirctCall is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_23

    .line 1072
    :cond_6a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.SmartAlert"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 1073
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_pick_up"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_24

    .line 1075
    :cond_6b
    const-string v3, "CSCSettings"

    const-string v4, "SmartAlert is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_24

    .line 1084
    :cond_6c
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.DoubleTapToTop"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 1085
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_double_tap"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_25

    .line 1087
    :cond_6d
    const-string v3, "CSCSettings"

    const-string v4, "DoulbeTap is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_25

    .line 1096
    :cond_6e
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.Tilt"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 1097
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_zooming"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_26

    .line 1102
    :cond_6f
    const-string v3, "CSCSettings"

    const-string v4, "Tilt is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_27

    .line 1111
    :cond_70
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PanToMove"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 1112
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_panning"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_28

    .line 1114
    :cond_71
    const-string v3, "CSCSettings"

    const-string v4, "PanToMove is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_28

    .line 1124
    :cond_72
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PanToBrowse"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 1125
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_pan_to_browse_image"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_29

    .line 1128
    :cond_73
    const-string v3, "CSCSettings"

    const-string v4, "PanToBrowse is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_29

    .line 1137
    :cond_74
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PanToShake"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 1138
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_shake"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_2a

    .line 1145
    :cond_75
    const-string v3, "CSCSettings"

    const-string v4, "Shake is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2b

    .line 1154
    :cond_76
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.TurnOver"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 1155
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "motion_overturn"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_2c

    .line 1157
    :cond_77
    const-string v3, "CSCSettings"

    const-string v4, "TurnOver is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2c

    .line 1165
    :cond_78
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PalmSwipe"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 1166
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "surface_palm_swipe"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_2d

    .line 1168
    :cond_79
    const-string v3, "CSCSettings"

    const-string v4, "PalmSwipe is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2d

    .line 1177
    :cond_7a
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Settings.Main.Phone.Motion.PalmTouch"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_24

    .line 1178
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "surface_palm_touch"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_2e

    .line 1180
    :cond_7b
    const-string v3, "CSCSettings"

    const-string v4, "PalmTouch is not found"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2e
.end method

.method public updateForHomeFOTA(IZ)V
    .locals 2
    .param p1, "buildType"    # I
    .param p2, "isNewCscEdtion"    # Z

    .prologue
    .line 3514
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3515
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 3516
    const-string v1, "INU"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "INS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3517
    :cond_0
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateCountryCertInfo()V

    .line 3518
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateWifiProfileInfo()V

    .line 3520
    :cond_2
    return-void
.end method

.method public updateForSubUser()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 283
    const-string v0, "CSCSettings"

    const-string v1, "updateForSubUser in CSCSettings"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.VisiblePassword"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 290
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.VisiblePassword"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.VisiblePassword"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 292
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "show_password"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 300
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 301
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.BackLightTime"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "15sec"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 303
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_off_timeout"

    const/16 v2, 0x3a98

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 329
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Sound.TouchTone"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 330
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Sound.TouchTone"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Sound.TouchTone"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 332
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "sound_effects_enabled"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 340
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 341
    const-string v0, "CSCSettings"

    const-string v1, "Setting Date format"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "regional"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 343
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ddmmyyyy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 344
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "date_format"

    const-string v2, "dd-MM-yyyy"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 354
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 355
    const-string v0, "CSCSettings"

    const-string v1, "Setting time format"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "24h"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 357
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "time_12_24"

    const-string v2, "24"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 365
    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.UseSensorAiding"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 366
    const-string v0, "CSCSettings"

    const-string v1, "Setting PDR select"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.UseSensorAiding"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 368
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "location_pdr_enabled"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 376
    :cond_6
    :goto_5
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.GPS.GPSActivation"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 377
    const-string v0, "CSCSettings"

    const-string v1, "Setting GPS Satelites"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "location_providers_allowed"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "network"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 380
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.GPS.GPSActivation"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 381
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "location_providers_allowed"

    const-string v2, "gps,network"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 398
    :cond_7
    :goto_6
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.SmartStay"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 399
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.SmartStay"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.SmartStay"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 401
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "intelligent_sleep_mode"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 409
    :cond_8
    :goto_7
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 412
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.Activation"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 413
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.Motion.MotionActivation"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.Activation"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 415
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_engine"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 422
    :cond_9
    :goto_8
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.GlanceView"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_30

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_GLANCE_VIEW"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 425
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.Motion.GlanceView"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.GlanceView"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 427
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "air_motion_glance_view"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 434
    :cond_a
    :goto_9
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.DirectCall"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_32

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PICK_UP_TO_CALL_OUT"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 437
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Motion.DirctCall"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.DirectCall"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 439
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_pick_up_to_call_out"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 448
    :cond_b
    :goto_a
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.SmartAlert"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_34

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PICK_UP"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 451
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.Motion.SmartAlert"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.SmartAlert"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 453
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_pick_up"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 460
    :cond_c
    :goto_b
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.DoubleTapToTop"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_36

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_DOUBLE_TAP"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 463
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.Motion.DoulbeTap"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.DoubleTapToTop"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 465
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_double_tap"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 472
    :cond_d
    :goto_c
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.Tilt"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_38

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_TILT"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 475
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.Motion.Tilt"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.Tilt"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 477
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_zooming"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 481
    :cond_e
    :goto_d
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_zooming_guide_show_again"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 487
    :goto_e
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PanToMove"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3a

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PAN"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 490
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.Motion.PanToMove"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PanToMove"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 492
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_panning"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 499
    :cond_f
    :goto_f
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PanToBrowse"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3c

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PAN_TO_BROWSE_IMAGE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 502
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.Motion.PanToBrowse"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PanToBrowse"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 504
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_pan_to_browse_image"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 513
    :cond_10
    :goto_10
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PanToShake"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3e

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_SHAKE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 516
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.Motion.Shake"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PanToShake"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 518
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_shake"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 522
    :cond_11
    :goto_11
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_shake_scan_guide_show_again"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 524
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_shake_refresh_guide_show_again"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 530
    :goto_12
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.TurnOver"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_40

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_TRUN_OVER"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 533
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.Motion.TurnOver"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.TurnOver"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 535
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_overturn"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 541
    :cond_12
    :goto_13
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PalmSwipe"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_42

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PALM_SWIPE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 544
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.Motion.PalmSwipe"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PalmSwipe"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 546
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "surface_palm_swipe"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 553
    :cond_13
    :goto_14
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PalmTouch"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_44

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_PALM_TOUCH"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 556
    const-string v0, "CSCSettings"

    const-string v1, "Settings.Main.Phone.Motion.PalmTouch"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PalmTouch"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 558
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "surface_palm_touch"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 565
    :cond_14
    :goto_15
    return-void

    .line 293
    :cond_15
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.VisiblePassword"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "show_password"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 296
    :cond_16
    const-string v0, "CSCSettings"

    const-string v1, "VisiblePassword is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 304
    :cond_17
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "30sec"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 305
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_off_timeout"

    const/16 v2, 0x7530

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 306
    :cond_18
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1min"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 307
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_off_timeout"

    const v2, 0xea60

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 308
    :cond_19
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "2min"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 309
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_off_timeout"

    const v2, 0x1d4c0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 310
    :cond_1a
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "5min"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 311
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_off_timeout"

    const v2, 0x493e0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 312
    :cond_1b
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "10min"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 313
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_off_timeout"

    const v2, 0x927c0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 314
    :cond_1c
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.BackLightTime"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "30min"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto/16 :goto_1

    .line 325
    :cond_1d
    const-string v0, "CSCSettings"

    const-string v1, "BackLight Time is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 333
    :cond_1e
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Sound.TouchTone"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 334
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "sound_effects_enabled"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_2

    .line 336
    :cond_1f
    const-string v0, "CSCSettings"

    const-string v1, "Touch Sounds is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 345
    :cond_20
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mmddyyyy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 346
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "date_format"

    const-string v2, "MM-dd-yyyy"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 347
    :cond_21
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.DateFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "yyyymmdd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 348
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "date_format"

    const-string v2, "yyyy-MM-dd"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 350
    :cond_22
    const-string v0, "CSCSettings"

    const-string v1, "Date format not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 358
    :cond_23
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.DateTimeFormat.TimeFormat"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "12h"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 359
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "time_12_24"

    const-string v2, "12"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_4

    .line 361
    :cond_24
    const-string v0, "CSCSettings"

    const-string v1, "Time format not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 369
    :cond_25
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.UseSensorAiding"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 370
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "location_pdr_enabled"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_5

    .line 372
    :cond_26
    const-string v0, "CSCSettings"

    const-string v1, "Setting PDR is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 383
    :cond_27
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.GPS.GPSActivation"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 384
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "location_providers_allowed"

    const-string v2, "network"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_6

    .line 387
    :cond_28
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.GPS.GPSActivation"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 388
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "location_providers_allowed"

    const-string v2, "gps"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_6

    .line 390
    :cond_29
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.GPS.GPSActivation"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 391
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "location_providers_allowed"

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_6

    .line 395
    :cond_2a
    const-string v0, "CSCSettings"

    const-string v1, "GPS Satelites is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 402
    :cond_2b
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.SmartStay"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 403
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "intelligent_sleep_mode"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_7

    .line 405
    :cond_2c
    const-string v0, "CSCSettings"

    const-string v1, "SmartStay is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 416
    :cond_2d
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.Activation"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 417
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_engine"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_8

    .line 419
    :cond_2e
    const-string v0, "CSCSettings"

    const-string v1, "MotionActivation is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 428
    :cond_2f
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.GlanceView"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 429
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "air_motion_glance_view"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_9

    .line 431
    :cond_30
    const-string v0, "CSCSettings"

    const-string v1, "GlanceView is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 441
    :cond_31
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.DirectCall"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 442
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_pick_up_to_call_out"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_a

    .line 445
    :cond_32
    const-string v0, "CSCSettings"

    const-string v1, "DirctCall is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 454
    :cond_33
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.SmartAlert"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 455
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_pick_up"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_b

    .line 457
    :cond_34
    const-string v0, "CSCSettings"

    const-string v1, "SmartAlert is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 466
    :cond_35
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.DoubleTapToTop"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 467
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_double_tap"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_c

    .line 469
    :cond_36
    const-string v0, "CSCSettings"

    const-string v1, "DoulbeTap is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 478
    :cond_37
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.Tilt"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 479
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_zooming"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_d

    .line 484
    :cond_38
    const-string v0, "CSCSettings"

    const-string v1, "Tilt is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 493
    :cond_39
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PanToMove"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 494
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_panning"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_f

    .line 496
    :cond_3a
    const-string v0, "CSCSettings"

    const-string v1, "PanToMove is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_f

    .line 506
    :cond_3b
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PanToBrowse"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 507
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_pan_to_browse_image"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_10

    .line 510
    :cond_3c
    const-string v0, "CSCSettings"

    const-string v1, "PanToBrowse is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    .line 519
    :cond_3d
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PanToShake"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 520
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_shake"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_11

    .line 527
    :cond_3e
    const-string v0, "CSCSettings"

    const-string v1, "Shake is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    .line 536
    :cond_3f
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.TurnOver"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 537
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_overturn"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_13

    .line 539
    :cond_40
    const-string v0, "CSCSettings"

    const-string v1, "TurnOver is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_13

    .line 547
    :cond_41
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PalmSwipe"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 548
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "surface_palm_swipe"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_14

    .line 550
    :cond_42
    const-string v0, "CSCSettings"

    const-string v1, "PalmSwipe is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_14

    .line 559
    :cond_43
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Settings.Main.Phone.Motion.PalmTouch"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 560
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "surface_palm_touch"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_15

    .line 562
    :cond_44
    const-string v0, "CSCSettings"

    const-string v1, "PalmTouch is not found"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_15
.end method

.method public updateRingtones(I)V
    .locals 17
    .param p1, "type"    # I

    .prologue
    .line 1628
    new-instance v7, Landroid/media/RingtoneManager;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    invoke-direct {v7, v13}, Landroid/media/RingtoneManager;-><init>(Landroid/content/Context;)V

    .line 1629
    .local v7, "rm":Landroid/media/RingtoneManager;
    const/4 v2, 0x0

    .line 1632
    .local v2, "Tag_Str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v13, :cond_0

    .line 1633
    new-instance v13, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v13, v14}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 1635
    :cond_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v13

    const-string v14, "CscFeature_Common_UseChameleon"

    invoke-virtual {v13, v14}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1637
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getChameleonPath()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v4, v13}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 1638
    .local v4, "chameleonParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const-string v13, "Operators.DefaultRinger"

    invoke-virtual {v4, v13}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1639
    if-nez v2, :cond_2

    .line 1817
    .end local v4    # "chameleonParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_1
    :goto_0
    return-void

    .line 1643
    .restart local v4    # "chameleonParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_2
    const/4 v13, 0x1

    invoke-virtual {v7, v13}, Landroid/media/RingtoneManager;->setType(I)V

    .line 1645
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v14, "Settings.Main.Sound.RingTone.src"

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-virtual/range {v13 .. v16}, Lcom/samsung/sec/android/application/csc/CscParser;->getAttrbute(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v2

    .line 1646
    const-string v13, "CSCSettings"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Chameleon Tag_Str: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1647
    const/16 p1, -0x1

    .line 1650
    .end local v4    # "chameleonParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_3
    const-string v13, "CSCSettings"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Setting Ringtones (type) : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1651
    const/4 v13, 0x1

    move/from16 v0, p1

    if-ne v0, v13, :cond_8

    .line 1652
    const/4 v13, 0x1

    invoke-virtual {v7, v13}, Landroid/media/RingtoneManager;->setType(I)V

    .line 1654
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v14, "Settings.Main.Sound.RingTone.src"

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-virtual/range {v13 .. v16}, Lcom/samsung/sec/android/application/csc/CscParser;->getAttrbute(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v2

    .line 1655
    const-string v13, "CSCSettings"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "1. Tag_Str: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1671
    :cond_4
    :goto_1
    if-eqz v2, :cond_1

    .line 1673
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v13}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    .line 1675
    const/4 v13, 0x1

    move/from16 v0, p1

    if-ne v0, v13, :cond_a

    .line 1676
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v14, "PREF_RINGTONE_SET"

    const/4 v15, 0x0

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v13

    if-nez v13, :cond_5

    .line 1677
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v14, "PREF_RINGTONE_SET"

    const/4 v15, 0x1

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1679
    const-string v13, "CSCSettings"

    const-string v14, "CSC Ringtone found: PREF_RINGTONE_SET(1)"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1695
    :cond_5
    :goto_2
    invoke-virtual {v7}, Landroid/media/RingtoneManager;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    .line 1696
    .local v3, "c":Landroid/database/Cursor;
    const-string v13, "title"

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 1697
    .local v11, "titleIndex":I
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1698
    const/4 v6, 0x0

    .line 1700
    .local v6, "position":I
    :goto_3
    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v13

    if-nez v13, :cond_6

    .line 1701
    invoke-interface {v3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1702
    .local v10, "title":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    .line 1703
    .local v9, "sbText":Ljava/lang/StringBuffer;
    const-string v13, "Ringtone title : "

    invoke-virtual {v9, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1704
    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1705
    const-string v13, "CSCSettings"

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1706
    invoke-virtual {v2, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 1707
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    .line 1708
    .local v8, "sbLog":Ljava/lang/StringBuffer;
    const-string v13, "Ringtone equalsIgnoreCase (position): "

    invoke-virtual {v8, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1709
    invoke-virtual {v8, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1710
    const-string v13, "CSCSettings"

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1716
    .end local v8    # "sbLog":Ljava/lang/StringBuffer;
    .end local v9    # "sbText":Ljava/lang/StringBuffer;
    .end local v10    # "title":Ljava/lang/String;
    :cond_6
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v13

    if-lt v6, v13, :cond_13

    .line 1718
    :cond_7
    const-string v13, "CSCSettings"

    const-string v14, "No matched ringtones"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1719
    const/4 v13, 0x1

    move/from16 v0, p1

    if-ne v0, v13, :cond_f

    .line 1720
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v14, "PREF_RINGTONE_SET"

    const/4 v15, 0x0

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_d

    .line 1726
    const-string v13, "CSCSettings"

    const-string v14, "Aleady failed. The Ringtone is not exist"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1727
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v13}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1728
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1657
    .end local v3    # "c":Landroid/database/Cursor;
    .end local v6    # "position":I
    .end local v11    # "titleIndex":I
    :cond_8
    const/4 v13, 0x2

    move/from16 v0, p1

    if-ne v0, v13, :cond_9

    .line 1658
    const/4 v13, 0x2

    invoke-virtual {v7, v13}, Landroid/media/RingtoneManager;->setType(I)V

    .line 1660
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v14, "Settings.Main.Sound.MessageTone.src"

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-virtual/range {v13 .. v16}, Lcom/samsung/sec/android/application/csc/CscParser;->getAttrbute(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v2

    .line 1661
    const-string v13, "CSCSettings"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "1. Tag_Str: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1663
    :cond_9
    const/4 v13, 0x4

    move/from16 v0, p1

    if-ne v0, v13, :cond_4

    .line 1664
    const/4 v13, 0x4

    invoke-virtual {v7, v13}, Landroid/media/RingtoneManager;->setType(I)V

    .line 1666
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v14, "Settings.Main.Sound.AlarmTone.src"

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-virtual/range {v13 .. v16}, Lcom/samsung/sec/android/application/csc/CscParser;->getAttrbute(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v2

    .line 1667
    const-string v13, "CSCSettings"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "1. Tag_Str: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1681
    :cond_a
    const/4 v13, 0x2

    move/from16 v0, p1

    if-ne v0, v13, :cond_b

    .line 1682
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v14, "PREF_NOTIFICATION_SET"

    const/4 v15, 0x0

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v13

    if-nez v13, :cond_5

    .line 1683
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v14, "PREF_NOTIFICATION_SET"

    const/4 v15, 0x1

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1685
    const-string v13, "CSCSettings"

    const-string v14, "CSC Notification found: PREF_NOTIFICATION_SET(1)"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1687
    :cond_b
    const/4 v13, 0x4

    move/from16 v0, p1

    if-ne v0, v13, :cond_5

    .line 1688
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v14, "PREF_ALARMTONE_SET"

    const/4 v15, 0x0

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v13

    if-nez v13, :cond_5

    .line 1689
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v14, "PREF_ALARMTONE_SET"

    const/4 v15, 0x1

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1692
    const-string v13, "CSCSettings"

    const-string v14, "CSC Alarmtone found: PREF_ALARMTONE_SET(1)"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1713
    .restart local v3    # "c":Landroid/database/Cursor;
    .restart local v6    # "position":I
    .restart local v9    # "sbText":Ljava/lang/StringBuffer;
    .restart local v10    # "title":Ljava/lang/String;
    .restart local v11    # "titleIndex":I
    :cond_c
    add-int/lit8 v6, v6, 0x1

    .line 1714
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_3

    .line 1731
    .end local v9    # "sbText":Ljava/lang/StringBuffer;
    .end local v10    # "title":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v14, "PREF_RINGTONE_SET"

    const/4 v15, 0x2

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1732
    const-string v13, "CSCSettings"

    const-string v14, "Ringtone not found. Media DB was not prepared: PREF_RINGTONE_SET(2)"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1765
    :cond_e
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v13}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1766
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1734
    :cond_f
    const/4 v13, 0x2

    move/from16 v0, p1

    if-ne v0, v13, :cond_11

    .line 1735
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v14, "PREF_NOTIFICATION_SET"

    const/4 v15, 0x0

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_10

    .line 1741
    const-string v13, "CSCSettings"

    const-string v14, "Aleady failed. Notification is not exist"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1742
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v13}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1743
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1746
    :cond_10
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v14, "PREF_NOTIFICATION_SET"

    const/4 v15, 0x2

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1747
    const-string v13, "CSCSettings"

    const-string v14, "Notification not found. Media DB was not prepared: PREF_NOTIFICATION_SET(2)"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1749
    :cond_11
    const/4 v13, 0x4

    move/from16 v0, p1

    if-ne v0, v13, :cond_e

    .line 1750
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->prefs:Landroid/content/SharedPreferences;

    const-string v14, "PREF_ALARMTONE_SET"

    const/4 v15, 0x0

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_12

    .line 1756
    const-string v13, "CSCSettings"

    const-string v14, "Aleady failed. Alarmtone is not exist"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1757
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v13}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1758
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1761
    :cond_12
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v14, "PREF_ALARMTONE_SET"

    const/4 v15, 0x2

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1762
    const-string v13, "CSCSettings"

    const-string v14, "Alarmtone not found. Media DB was not prepared: PREF_ALARMTONE_SET(2)"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1769
    :cond_13
    invoke-virtual {v7, v6}, Landroid/media/RingtoneManager;->getRingtoneUri(I)Landroid/net/Uri;

    move-result-object v12

    .line 1770
    .local v12, "uri":Landroid/net/Uri;
    const-string v13, "CSCSettings"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Ringtone uri : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1771
    if-eqz v12, :cond_15

    .line 1775
    const/4 v13, 0x1

    move/from16 v0, p1

    if-ne v0, v13, :cond_16

    .line 1776
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    const/4 v14, 0x1

    invoke-static {v13, v14, v12}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 1777
    if-eqz v12, :cond_14

    .line 1778
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "DEBUG_RINGTONE"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "CscSettings : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1781
    :cond_14
    const-string v13, "CSCSettings"

    const-string v14, "CSC Ringtone was set : Before PREF_RINGTONE_SET(3)"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783
    const-wide/16 v14, 0x2710

    :try_start_0
    invoke-static {v14, v15}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1787
    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v14, "PREF_RINGTONE_SET"

    const/4 v15, 0x3

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1790
    const-string v13, "CSCSettings"

    const-string v14, "CSC Ringtone was set : PREF_RINGTONE_SET(3)"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1810
    :cond_15
    :goto_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v13}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1811
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1784
    :catch_0
    move-exception v5

    .line 1785
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 1791
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_16
    const/4 v13, 0x2

    move/from16 v0, p1

    if-ne v0, v13, :cond_18

    .line 1792
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    const/4 v14, 0x2

    invoke-static {v13, v14, v12}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 1793
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "DEBUG_RINGTONE"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "CscSettings : "

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_17

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    :goto_7
    invoke-static {v14, v15, v13}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1795
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v14, "PREF_NOTIFICATION_SET"

    const/4 v15, 0x3

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1798
    const-string v13, "CSCSettings"

    const-string v14, "CSC Notification was set : PREF_NOTIFICATION_SET(3)"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 1793
    :cond_17
    const-string v13, ""

    goto :goto_7

    .line 1799
    :cond_18
    const/4 v13, 0x4

    move/from16 v0, p1

    if-ne v0, v13, :cond_15

    .line 1800
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    const/4 v14, 0x4

    invoke-static {v13, v14, v12}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 1801
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "DEBUG_RINGTONE"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "CscSettings : "

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_19

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    :goto_8
    invoke-static {v14, v15, v13}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1803
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v14, "PREF_ALARMTONE_SET"

    const/4 v15, 0x3

    invoke-interface {v13, v14, v15}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1807
    const-string v13, "CSCSettings"

    const-string v14, "CSC Alarmtone was set : PREF_ALARMTONE_SET(3)"

    invoke-static {v13, v14}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1801
    :cond_19
    const-string v13, ""

    goto :goto_8
.end method

.method public updateSilentMode(Ljava/lang/String;)V
    .locals 6
    .param p1, "_dataPath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 1836
    const/4 v1, 0x0

    .line 1838
    .local v1, "cscData":Ljava/lang/String;
    const-string v2, "CSCSettings"

    invoke-static {v2, p1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1840
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v2, :cond_0

    .line 1841
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 1843
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v2, p1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1844
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1846
    .local v0, "am":Landroid/media/AudioManager;
    const-string v2, "melody"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1847
    const-string v2, "CSCSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " melody"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1848
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 1862
    .end local v0    # "am":Landroid/media/AudioManager;
    :goto_0
    return-void

    .line 1849
    .restart local v0    # "am":Landroid/media/AudioManager;
    :cond_1
    const-string v2, "mute"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1850
    const-string v2, "CSCSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : mute"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1851
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setRingerMode(I)V

    goto :goto_0

    .line 1852
    :cond_2
    const-string v2, "vib"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1853
    const-string v2, "CSCSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : vib"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1854
    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->setRingerMode(I)V

    goto :goto_0

    .line 1856
    :cond_3
    const-string v2, "CSCSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : vibmelody"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1857
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "vibrate_when_ringing"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 1861
    .end local v0    # "am":Landroid/media/AudioManager;
    :cond_4
    const-string v2, "CSCSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public updateVolume(ILjava/lang/String;)V
    .locals 5
    .param p1, "_volumeType"    # I
    .param p2, "_dataPath"    # Ljava/lang/String;

    .prologue
    .line 1820
    const/4 v1, 0x0

    .line 1822
    .local v1, "cscData":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v2, :cond_0

    .line 1823
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 1825
    :cond_0
    const-string v2, "CSCSettings"

    invoke-static {v2, p2}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1827
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v2, p2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1828
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1829
    .local v0, "am":Landroid/media/AudioManager;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1833
    .end local v0    # "am":Landroid/media/AudioManager;
    :goto_0
    return-void

    .line 1831
    :cond_1
    const-string v2, "CSCSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateWifiProfileInfo()V
    .locals 26

    .prologue
    .line 1195
    const/16 v22, 0x0

    .line 1196
    .local v22, "wifiprofilecnt":I
    const/16 v16, 0x0

    .line 1197
    .local v16, "generalinfocnt":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    if-nez v23, :cond_0

    .line 1198
    new-instance v23, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-direct/range {v23 .. v24}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 1200
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    const-string v24, "Settings."

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v11

    .line 1201
    .local v11, "WifiProfileNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    const-string v24, "WifiProfile"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v11, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v12

    .line 1203
    .local v12, "WifiProfileNodeList":Lorg/w3c/dom/NodeList;
    if-nez v12, :cond_2

    .line 1204
    const-string v23, "CSCSettings"

    const-string v24, "No WifiProfileNodeList to setup"

    invoke-static/range {v23 .. v24}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1369
    :cond_1
    :goto_0
    return-void

    .line 1208
    :cond_2
    if-eqz v12, :cond_10

    .line 1209
    const-string v23, "CSCSettings"

    const-string v24, "Settings Wifi WifiProfile"

    invoke-static/range {v23 .. v24}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v23

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    sput-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiSSIDs:[Ljava/lang/String;

    .line 1211
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v23

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    sput-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiKeyMgmts:[Ljava/lang/String;

    .line 1212
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v23

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    sput-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiEAPMethods:[Ljava/lang/String;

    .line 1213
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v23

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    sput-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiPrioritys:[Ljava/lang/String;

    .line 1214
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v23

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    sput-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiNWNames:[Ljava/lang/String;

    .line 1216
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v23

    move/from16 v0, v18

    move/from16 v1, v23

    if-ge v0, v1, :cond_8

    .line 1218
    move/from16 v0, v18

    invoke-interface {v12, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v13

    .line 1220
    .local v13, "WifiProfileNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    const-string v24, "WifiSSID"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v13, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v14

    .line 1221
    .local v14, "WifiSSID":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    const-string v24, "WifiKeyMgmt"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v13, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 1222
    .local v8, "WifiKeyMgmt":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    const-string v24, "WifiEAPMethod"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v13, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 1223
    .local v7, "WifiEAPMethod":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    const-string v24, "WifiPriority"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v13, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 1224
    .local v10, "WifiPriority":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    const-string v24, "NetworkName"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v13, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v9

    .line 1226
    .local v9, "WifiNWName":Lorg/w3c/dom/Node;
    if-eqz v14, :cond_3

    .line 1227
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiSSIDs:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v23, v18

    .line 1229
    :cond_3
    if-eqz v8, :cond_4

    .line 1230
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiKeyMgmts:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v23, v18

    .line 1232
    :cond_4
    if-eqz v7, :cond_5

    .line 1233
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiEAPMethods:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v23, v18

    .line 1235
    :cond_5
    if-eqz v10, :cond_6

    .line 1236
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiPrioritys:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v23, v18

    .line 1238
    :cond_6
    if-eqz v9, :cond_7

    .line 1239
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiNWNames:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v23, v18

    .line 1241
    :cond_7
    add-int/lit8 v22, v22, 0x1

    .line 1216
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1

    .line 1245
    .end local v7    # "WifiEAPMethod":Lorg/w3c/dom/Node;
    .end local v8    # "WifiKeyMgmt":Lorg/w3c/dom/Node;
    .end local v9    # "WifiNWName":Lorg/w3c/dom/Node;
    .end local v10    # "WifiPriority":Lorg/w3c/dom/Node;
    .end local v13    # "WifiProfileNodeListChild":Lorg/w3c/dom/Node;
    .end local v14    # "WifiSSID":Lorg/w3c/dom/Node;
    :cond_8
    :try_start_0
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    .line 1246
    .local v21, "sb":Ljava/lang/StringBuilder;
    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1247
    const-string v23, "CSCSettings"

    const-string v24, "Settings Wifi default ap conf file is created "

    invoke-static/range {v23 .. v24}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1249
    const/16 v19, 0x0

    .local v19, "j":I
    :goto_2
    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_14

    .line 1250
    const-string v23, "network={\n"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1251
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiSSIDs:[Ljava/lang/String;

    aget-object v23, v23, v19

    if-eqz v23, :cond_9

    .line 1252
    const-string v23, "    ssid="

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1253
    const-string v23, "\""

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1254
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiSSIDs:[Ljava/lang/String;

    aget-object v23, v23, v19

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1255
    const-string v23, "\""

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1256
    const-string v23, "\n"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1258
    :cond_9
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiKeyMgmts:[Ljava/lang/String;

    aget-object v23, v23, v19

    if-eqz v23, :cond_a

    .line 1259
    const-string v23, "    key_mgmt="

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1260
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiKeyMgmts:[Ljava/lang/String;

    aget-object v23, v23, v19

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1261
    const-string v23, "\n"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1263
    :cond_a
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiEAPMethods:[Ljava/lang/String;

    aget-object v23, v23, v19

    if-eqz v23, :cond_c

    .line 1264
    const-string v23, "    eap="

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1265
    const-string v23, "sim"

    sget-object v24, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiEAPMethods:[Ljava/lang/String;

    aget-object v24, v24, v19

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_f

    .line 1266
    const-string v23, "SIM"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1270
    :cond_b
    :goto_3
    const-string v23, "\n"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1272
    :cond_c
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiPrioritys:[Ljava/lang/String;

    aget-object v23, v23, v19

    if-eqz v23, :cond_d

    .line 1273
    const-string v23, "    priority="

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1274
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiPrioritys:[Ljava/lang/String;

    aget-object v23, v23, v19

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1275
    const-string v23, "\n"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1277
    :cond_d
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiNWNames:[Ljava/lang/String;

    aget-object v23, v23, v19

    if-eqz v23, :cond_e

    .line 1278
    const-string v23, "    networkname="

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1279
    const-string v23, "\""

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1280
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiNWNames:[Ljava/lang/String;

    aget-object v23, v23, v19

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1281
    const-string v23, "\""

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1282
    const-string v23, "\n"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1284
    :cond_e
    const-string v23, "    vendor_spec_ssid=1"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1285
    const-string v23, "\n"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1286
    const-string v23, "}\n"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1249
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_2

    .line 1267
    :cond_f
    const-string v23, "aka"

    sget-object v24, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiEAPMethods:[Ljava/lang/String;

    aget-object v24, v24, v19

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 1268
    const-string v23, "AKA"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    .line 1297
    .end local v19    # "j":I
    .end local v21    # "sb":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v15

    .line 1298
    .local v15, "e":Ljava/lang/NullPointerException;
    const-string v23, "CSCSettings"

    const-string v24, "WIFI Profile -NullPointerException"

    invoke-static/range {v23 .. v24}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1303
    .end local v15    # "e":Ljava/lang/NullPointerException;
    .end local v18    # "i":I
    :cond_10
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    const-string v24, "GeneralInfo."

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 1304
    .local v2, "GeneralInfoNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    const-string v24, "NetworkInfo"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 1306
    .local v3, "GeneralInfoNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v3, :cond_1

    if-eqz v12, :cond_1

    .line 1307
    const-string v23, "CSCSettings"

    const-string v24, "GeneralInfo NetworkInfo"

    invoke-static/range {v23 .. v24}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1308
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v23

    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v24

    mul-int v23, v23, v24

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    sput-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->GeneralMCCMNCs:[Ljava/lang/String;

    .line 1310
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v23

    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v24

    mul-int v23, v23, v24

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    sput-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->GeneralNWNames:[Ljava/lang/String;

    .line 1313
    const/16 v18, 0x0

    .restart local v18    # "i":I
    :goto_5
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v23

    move/from16 v0, v18

    move/from16 v1, v23

    if-ge v0, v1, :cond_16

    .line 1315
    move/from16 v0, v18

    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 1317
    .local v4, "GeneralInfoNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    const-string v24, "MCCMNC"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 1318
    .local v5, "GeneralMccMnc":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v23, v0

    const-string v24, "NetworkName"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 1319
    .local v6, "GeneralNWName":Lorg/w3c/dom/Node;
    const/16 v20, 0x0

    .local v20, "k":I
    :goto_6
    move/from16 v0, v20

    move/from16 v1, v22

    if-ge v0, v1, :cond_15

    .line 1320
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiNWNames:[Ljava/lang/String;

    aget-object v23, v23, v20

    if-eqz v23, :cond_13

    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->WifiNWNames:[Ljava/lang/String;

    aget-object v23, v23, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_13

    .line 1322
    if-eqz v5, :cond_11

    .line 1323
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->GeneralMCCMNCs:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v23, v16

    .line 1325
    :cond_11
    if-eqz v6, :cond_12

    .line 1326
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->GeneralNWNames:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v23, v16

    .line 1328
    :cond_12
    add-int/lit8 v16, v16, 0x1

    .line 1319
    :cond_13
    add-int/lit8 v20, v20, 0x1

    goto :goto_6

    .line 1289
    .end local v2    # "GeneralInfoNode":Lorg/w3c/dom/Node;
    .end local v3    # "GeneralInfoNodeList":Lorg/w3c/dom/NodeList;
    .end local v4    # "GeneralInfoNodeListChild":Lorg/w3c/dom/Node;
    .end local v5    # "GeneralMccMnc":Lorg/w3c/dom/Node;
    .end local v6    # "GeneralNWName":Lorg/w3c/dom/Node;
    .end local v20    # "k":I
    .restart local v19    # "j":I
    .restart local v21    # "sb":Ljava/lang/StringBuilder;
    :cond_14
    if-lez v22, :cond_10

    .line 1290
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v23, v0

    const-string v24, "wifi_defaultap_profile"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    new-instance v24, Landroid/content/Intent;

    const-string v25, "android.intent.action.CSC_WIFI_DEFAULTAP_DONE"

    invoke-direct/range {v24 .. v25}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v24}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v23, v0

    const-string v24, "wifi_defaultap_profile"

    invoke-static/range {v23 .. v24}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    .line 1313
    .end local v19    # "j":I
    .end local v21    # "sb":Ljava/lang/StringBuilder;
    .restart local v2    # "GeneralInfoNode":Lorg/w3c/dom/Node;
    .restart local v3    # "GeneralInfoNodeList":Lorg/w3c/dom/NodeList;
    .restart local v4    # "GeneralInfoNodeListChild":Lorg/w3c/dom/Node;
    .restart local v5    # "GeneralMccMnc":Lorg/w3c/dom/Node;
    .restart local v6    # "GeneralNWName":Lorg/w3c/dom/Node;
    .restart local v20    # "k":I
    :cond_15
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_5

    .line 1334
    .end local v4    # "GeneralInfoNodeListChild":Lorg/w3c/dom/Node;
    .end local v5    # "GeneralMccMnc":Lorg/w3c/dom/Node;
    .end local v6    # "GeneralNWName":Lorg/w3c/dom/Node;
    .end local v20    # "k":I
    :cond_16
    :try_start_2
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 1335
    .local v17, "generalsb":Ljava/lang/StringBuilder;
    const/16 v23, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1336
    const-string v23, "CSCSettings"

    const-string v24, "GeneralInfo NetworkInfo file is created "

    invoke-static/range {v23 .. v24}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1338
    const/16 v19, 0x0

    .restart local v19    # "j":I
    :goto_7
    move/from16 v0, v19

    move/from16 v1, v16

    if-ge v0, v1, :cond_19

    .line 1339
    const-string v23, "network={\n"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1340
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->GeneralNWNames:[Ljava/lang/String;

    aget-object v23, v23, v19

    if-eqz v23, :cond_17

    .line 1341
    const-string v23, "    networkname="

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1342
    const-string v23, "\""

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1343
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->GeneralNWNames:[Ljava/lang/String;

    aget-object v23, v23, v19

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1344
    const-string v23, "\""

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1345
    const-string v23, "\n"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1347
    :cond_17
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->GeneralMCCMNCs:[Ljava/lang/String;

    aget-object v23, v23, v19

    if-eqz v23, :cond_18

    .line 1348
    const-string v23, "    mccmnc="

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1349
    const-string v23, "\""

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1350
    sget-object v23, Lcom/samsung/sec/android/application/csc/CscSettings;->GeneralMCCMNCs:[Ljava/lang/String;

    aget-object v23, v23, v19

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1351
    const-string v23, "\""

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1352
    const-string v23, "\n"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1354
    :cond_18
    const-string v23, "}\n"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1338
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_7

    .line 1357
    :cond_19
    if-lez v16, :cond_1

    .line 1358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v23, v0

    const-string v24, "wifi_generalinfo_nwinfo"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1360
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    new-instance v24, Landroid/content/Intent;

    const-string v25, "android.intent.action.CSC_WIFI_GENERALINFO_DONE"

    invoke-direct/range {v24 .. v25}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v24}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSettings;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v23, v0

    const-string v24, "wifi_generalinfo_nwinfo"

    invoke-static/range {v23 .. v24}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 1365
    .end local v17    # "generalsb":Ljava/lang/StringBuilder;
    .end local v19    # "j":I
    :catch_1
    move-exception v15

    .line 1366
    .restart local v15    # "e":Ljava/lang/NullPointerException;
    const-string v23, "CSCSettings"

    const-string v24, "GeneralInfo -NullPointerException"

    invoke-static/range {v23 .. v24}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
