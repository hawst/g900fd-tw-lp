.class public Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscSamsungKeypad.java"


# static fields
.field private static final CSC_TAG:[Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "T9Enabling"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "KeypadType"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ContinuousInput"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "AutoReplacement"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->CSC_TAG:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->mContext:Landroid/content/Context;

    .line 51
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->mContext:Landroid/content/Context;

    .line 52
    return-void
.end method

.method private getBooleanToTagValue(Z)Ljava/lang/String;
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 190
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 191
    const-string v0, "enable"

    .line 192
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "disable"

    goto :goto_0
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Main.Phone.T9Language"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 62
    const-string v7, "NOERROR"

    .line 75
    .local v7, "returnString":Ljava/lang/String;
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v1, v11}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 76
    .local v1, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const/4 v0, 0x0

    .line 79
    .local v0, "SamsungIME_context":Landroid/content/Context;
    :try_start_0
    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "com.sec.android.inputmethod"

    const/4 v13, 0x2

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 85
    const-string v11, "com.sec.android.inputmethod_preferences"

    const/4 v12, 0x5

    invoke-virtual {v0, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 87
    .local v5, "mPrefs":Landroid/content/SharedPreferences;
    const/4 v2, 0x0

    .line 89
    .local v2, "cscValue":Ljava/lang/String;
    const-string v11, "Settings.Main.Phone.T9Enabling"

    invoke-virtual {v1, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 91
    .local v6, "node":Lorg/w3c/dom/Node;
    if-eqz v6, :cond_0

    .line 92
    const-string v11, "Settings.Main.Phone.T9Enabling"

    invoke-virtual {v1, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 93
    const-string v11, "SETTINGS_DEFAULT_PREDICTION_ON"

    invoke-interface {v5, v11, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .line 95
    .local v10, "useXT9":Z
    invoke-direct {p0, v10}, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->getBooleanToTagValue(Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 96
    const-string v11, "Settings.Main.Phone.T9Enabling"

    invoke-static {v11, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    .end local v10    # "useXT9":Z
    :cond_0
    :goto_0
    const-string v11, "Settings.Main.Phone.KeypadType"

    invoke-virtual {v1, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 105
    if-eqz v6, :cond_1

    .line 106
    const-string v11, "Settings.Main.Phone.KeypadType"

    invoke-virtual {v1, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 108
    const-string v11, "SETTINGS_DEFAULT_KEYPAD_TYPE"

    const-string v12, "0"

    invoke-interface {v5, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 110
    .local v4, "keypadType":Ljava/lang/String;
    const-string v11, "0"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 111
    const-string v4, "QWERTY"

    .line 118
    :goto_1
    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 119
    const-string v11, "Settings.Main.Phone.KeypadType"

    invoke-static {v11, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    .end local v4    # "keypadType":Ljava/lang/String;
    :cond_1
    :goto_2
    const-string v11, "Settings.Main.Phone.ContinuousInput"

    invoke-virtual {v1, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 129
    if-eqz v6, :cond_2

    .line 130
    const-string v11, "Settings.Main.Phone.ContinuousInput"

    invoke-virtual {v1, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 132
    const-string v11, "SETTINGS_DEFAULT_TRACE"

    invoke-interface {v5, v11, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 134
    .local v9, "useTrace":Z
    invoke-direct {p0, v9}, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->getBooleanToTagValue(Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 135
    const-string v11, "Settings.Main.Phone.ContinuousInput"

    invoke-static {v11, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    .end local v9    # "useTrace":Z
    :cond_2
    :goto_3
    const-string v11, "Settings.Main.Phone.AutoReplacement"

    invoke-virtual {v1, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 145
    if-eqz v6, :cond_3

    .line 146
    const-string v11, "Settings.Main.Phone.AutoReplacement"

    invoke-virtual {v1, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 148
    const-string v11, "SETTINGS_DEFAULT_AUTO_CORRECTION"

    invoke-interface {v5, v11, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    .line 150
    .local v8, "useAutoReplacement":Z
    invoke-direct {p0, v8}, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->getBooleanToTagValue(Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 151
    const-string v11, "Settings.Main.Phone.AutoReplacement"

    invoke-static {v11, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .end local v8    # "useAutoReplacement":Z
    :cond_3
    :goto_4
    move-object v11, v7

    .line 160
    .end local v2    # "cscValue":Ljava/lang/String;
    .end local v5    # "mPrefs":Landroid/content/SharedPreferences;
    .end local v6    # "node":Lorg/w3c/dom/Node;
    :goto_5
    return-object v11

    .line 81
    :catch_0
    move-exception v3

    .line 82
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v11, "SamsungKeypad not found"

    goto :goto_5

    .line 98
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "cscValue":Ljava/lang/String;
    .restart local v5    # "mPrefs":Landroid/content/SharedPreferences;
    .restart local v6    # "node":Lorg/w3c/dom/Node;
    .restart local v10    # "useXT9":Z
    :cond_4
    const-string v11, "Settings.Main.Phone.T9Enabling"

    invoke-direct {p0, v10}, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->getBooleanToTagValue(Z)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v2, v12}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v7, "[ERROR] SamsungKeypad setting"

    .line 100
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "CscSamsungKeypad : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 112
    .end local v10    # "useXT9":Z
    .restart local v4    # "keypadType":Ljava/lang/String;
    :cond_5
    const-string v11, "1"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 113
    const-string v4, "3X4"

    goto/16 :goto_1

    .line 115
    :cond_6
    const-string v4, "QWERTY"

    goto/16 :goto_1

    .line 121
    :cond_7
    const-string v11, "Settings.Main.Phone.KeypadType"

    invoke-static {v11, v2, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v7, "[ERROR] SamsungKeypad setting"

    .line 123
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "CscSamsungKeypad : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 137
    .end local v4    # "keypadType":Ljava/lang/String;
    .restart local v9    # "useTrace":Z
    :cond_8
    const-string v11, "Settings.Main.Phone.ContinuousInput"

    invoke-direct {p0, v9}, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->getBooleanToTagValue(Z)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v2, v12}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v7, "[ERROR] SamsungKeypad setting"

    .line 140
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "CscSamsungKeypad : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 153
    .end local v9    # "useTrace":Z
    .restart local v8    # "useAutoReplacement":Z
    :cond_9
    const-string v11, "Settings.Main.Phone.AutoReplacement"

    invoke-direct {p0, v8}, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->getBooleanToTagValue(Z)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v2, v12}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v7, "[ERROR] SamsungKeypad setting"

    .line 156
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "CscSamsungKeypad : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    goto/16 :goto_4
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 0
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 58
    return-void
.end method

.method public update()V
    .locals 9

    .prologue
    .line 171
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.sec.android.inputmethod.UpdatePreferences"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 174
    .local v3, "intent":Landroid/content/Intent;
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 176
    .local v0, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const/4 v4, 0x0

    .line 177
    .local v4, "node":Lorg/w3c/dom/Node;
    const/4 v1, 0x0

    .line 179
    .local v1, "cscValue":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->CSC_TAG:[Ljava/lang/String;

    array-length v5, v5

    if-ge v2, v5, :cond_1

    .line 180
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Main.Phone."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->CSC_TAG:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 181
    if-eqz v4, :cond_0

    .line 182
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings.Main.Phone."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->CSC_TAG:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 183
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->mContext:Landroid/content/Context;

    const-string v6, "TYPE"

    const-string v7, "STRING"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "KEY"

    sget-object v8, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;->CSC_TAG:[Ljava/lang/String;

    aget-object v8, v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "VALUE"

    invoke-virtual {v6, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 179
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 187
    :cond_1
    return-void
.end method
