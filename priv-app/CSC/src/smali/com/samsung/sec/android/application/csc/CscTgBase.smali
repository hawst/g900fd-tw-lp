.class public Lcom/samsung/sec/android/application/csc/CscTgBase;
.super Ljava/lang/Object;
.source "CscTgBase.java"

# interfaces
.implements Lcom/samsung/sec/android/application/csc/Comparable;
.implements Lcom/samsung/sec/android/application/csc/Encodable;
.implements Lcom/samsung/sec/android/application/csc/Updatable;
.implements Lcom/samsung/sec/android/application/csc/UpdatableForHomeFOTA;
.implements Lcom/samsung/sec/android/application/csc/UpdatableForSubUser;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    const-string v0, "NOERROR"

    return-object v0
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 0
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 32
    return-void
.end method

.method public update()V
    .locals 0

    .prologue
    .line 19
    return-void
.end method

.method public updateForHomeFOTA(IZ)V
    .locals 0
    .param p1, "buildType"    # I
    .param p2, "isNewCscEdtion"    # Z

    .prologue
    .line 15
    return-void
.end method

.method public updateForSubUser()V
    .locals 0

    .prologue
    .line 11
    return-void
.end method
