.class Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;
.super Ljava/lang/Object;
.source "CscTgManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscTgManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TgTable"
.end annotation


# instance fields
.field mFullList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;",
            ">;"
        }
    .end annotation
.end field

.field mWorkingList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscTgBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mWorkingList:Ljava/util/ArrayList;

    .line 68
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscPreferredPackage;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscPreferredPackage;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscHomescreen;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscHomescreen;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscWidget;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscWidget;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscContacts;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscContacts;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscSettings;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscSettings;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscDRMVerifier;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscDRMVerifier;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscCamera;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscCamera;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscCloud;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscCloud;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscContents;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscContents;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscIms;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscIms;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscBrowser;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscBrowser;-><init>(Landroid/content/Context;)V

    const-string v3, "com.android.browser"

    invoke-direct {p0, v2, v3}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscSBrowser;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscSBrowser;-><init>(Landroid/content/Context;)V

    const-string v3, "com.sec.android.app.sbrowser"

    invoke-direct {p0, v2, v3}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;-><init>(Landroid/content/Context;)V

    const-string v3, "com.android.providers.partnerbookmarks"

    invoke-direct {p0, v2, v3}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscCalendar;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscCalendar;-><init>(Landroid/content/Context;)V

    const-string v3, "com.android.calendar"

    invoke-direct {p0, v2, v3}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscEmailEas;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscEmailEas;-><init>(Landroid/content/Context;)V

    const-string v3, "com.android.email"

    invoke-direct {p0, v2, v3}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscSamsungKeypad;-><init>(Landroid/content/Context;)V

    const-string v3, "com.sec.android.inputmethod"

    invoke-direct {p0, v2, v3}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscRmpVerifier;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscRmpVerifier;-><init>(Landroid/content/Context;)V

    const-string v3, "com.samsung.sec.mtv"

    invoke-direct {p0, v2, v3}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscConnection;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscConnection;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2, v4}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Z)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscNetwork;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscNetwork;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2, v4}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Z)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscVoiceMail;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscVoiceMail;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2, v4}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Z)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscGPS;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscGPS;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2, v4}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Z)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscWbAmr;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscWbAmr;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2, v4}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Z)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscFota;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscFota;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2, v4}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Z)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscAudioManager;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscAudioManager;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2, v4}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Z)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscSmsMms;-><init>(Landroid/content/Context;)V

    const-string v3, "com.android.mms"

    invoke-direct {p0, v2, v4, v3}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;ZLjava/lang/String;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscEmailCustomer;-><init>(Landroid/content/Context;)V

    const-string v3, "com.android.email"

    invoke-direct {p0, v2, v4, v3}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;ZLjava/lang/String;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "salesCode":Ljava/lang/String;
    const-string v1, "ATT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscPackageManager;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscPackageManager;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->makeWorkingList(Landroid/content/Context;)V

    .line 120
    return-void

    .line 104
    :cond_1
    const-string v1, "SKC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "LUC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "KTC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 105
    :cond_2
    const-string v1, "ro.config.rm_preload_enabled"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 106
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscPackageManager;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscPackageManager;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    :cond_3
    const-string v1, "SPR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "BST"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "VMU"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "XAS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 110
    :cond_4
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscChameleon;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscChameleon;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    :cond_5
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUtil;->checkCarrier()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "VZW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "ACG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    :cond_6
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscChameleon;

    invoke-direct {v2, p1}, Lcom/samsung/sec/android/application/csc/CscChameleon;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;
    .locals 3
    .param p1, "cscTgObject"    # Lcom/samsung/sec/android/application/csc/CscTgBase;

    .prologue
    const/4 v2, 0x0

    .line 51
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, v2, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;-><init>(Lcom/samsung/sec/android/application/csc/CscTgBase;ZLjava/lang/String;Lcom/samsung/sec/android/application/csc/CscTgManager$1;)V

    return-object v0
.end method

.method private newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;
    .locals 3
    .param p1, "cscTgObject"    # Lcom/samsung/sec/android/application/csc/CscTgBase;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 59
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, p2, v2}, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;-><init>(Lcom/samsung/sec/android/application/csc/CscTgBase;ZLjava/lang/String;Lcom/samsung/sec/android/application/csc/CscTgManager$1;)V

    return-object v0
.end method

.method private newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;Z)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;
    .locals 2
    .param p1, "cscTgObject"    # Lcom/samsung/sec/android/application/csc/CscTgBase;
    .param p2, "modemOnly"    # Z

    .prologue
    const/4 v1, 0x0

    .line 55
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    invoke-direct {v0, p1, p2, v1, v1}, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;-><init>(Lcom/samsung/sec/android/application/csc/CscTgBase;ZLjava/lang/String;Lcom/samsung/sec/android/application/csc/CscTgManager$1;)V

    return-object v0
.end method

.method private newItem(Lcom/samsung/sec/android/application/csc/CscTgBase;ZLjava/lang/String;)Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;
    .locals 2
    .param p1, "cscTgObject"    # Lcom/samsung/sec/android/application/csc/CscTgBase;
    .param p2, "modemOnly"    # Z
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    .line 63
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;-><init>(Lcom/samsung/sec/android/application/csc/CscTgBase;ZLjava/lang/String;Lcom/samsung/sec/android/application/csc/CscTgManager$1;)V

    return-object v0
.end method


# virtual methods
.method getCscTgList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscTgBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mWorkingList:Ljava/util/ArrayList;

    return-object v0
.end method

.method getFullList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    return-object v0
.end method

.method makeWorkingList(Landroid/content/Context;)V
    .locals 6
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 123
    invoke-static {p1}, Lcom/samsung/sec/android/application/csc/CscUtil;->isWifiOnly(Landroid/content/Context;)Z

    move-result v2

    .line 124
    .local v2, "wifiModel":Z
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mFullList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;

    .line 125
    .local v1, "item":Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;
    if-eqz v2, :cond_1

    iget-boolean v3, v1, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;->modemOnly:Z

    if-nez v3, :cond_0

    .line 126
    :cond_1
    iget-object v3, v1, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;->pkgName:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;->pkgName:Ljava/lang/String;

    invoke-static {p1, v3}, Lcom/samsung/sec/android/application/csc/CscUtil;->isPackageLoaded(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 127
    :cond_2
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$TgTable;->mWorkingList:Ljava/util/ArrayList;

    iget-object v4, v1, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;->tgObject:Lcom/samsung/sec/android/application/csc/CscTgBase;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    const-string v3, "CscTgManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "makeWorkingList() +"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;->tgObject:Lcom/samsung/sec/android/application/csc/CscTgBase;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 132
    .end local v1    # "item":Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;
    :cond_3
    return-void
.end method
