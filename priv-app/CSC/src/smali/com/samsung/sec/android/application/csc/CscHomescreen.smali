.class public Lcom/samsung/sec/android/application/csc/CscHomescreen;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscHomescreen.java"


# instance fields
.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final LOG_TAG:Ljava/lang/String;

.field private final OTHERS_CSC_FILE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mWallpaperManager:Landroid/app/WallpaperManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 53
    const-string v0, "CSCHomescreen"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->LOG_TAG:Ljava/lang/String;

    .line 55
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 59
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->OTHERS_CSC_FILE:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 84
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 89
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mContext:Landroid/content/Context;

    .line 90
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mWallpaperManager:Landroid/app/WallpaperManager;

    .line 91
    return-void
.end method

.method private getWallpaperComponentFromFile(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 95
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "SettingsData.ForceLiveWallpaper.Package"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "wallpaperPackage":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "SettingsData.ForceLiveWallpaper.Class"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "wallpaperClass":Ljava/lang/String;
    const/4 v1, 0x0

    .line 98
    .local v1, "wallpaperComponent":Landroid/content/ComponentName;
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 99
    new-instance v1, Landroid/content/ComponentName;

    .end local v1    # "wallpaperComponent":Landroid/content/ComponentName;
    invoke-direct {v1, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .restart local v1    # "wallpaperComponent":Landroid/content/ComponentName;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public compare()Ljava/lang/String;
    .locals 22

    .prologue
    .line 128
    const-string v19, "CSCHomescreen"

    const-string v20, "Start of compare"

    invoke-static/range {v19 .. v20}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    if-nez v19, :cond_0

    .line 131
    new-instance v19, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 133
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    if-nez v19, :cond_1

    .line 134
    new-instance v19, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->OTHERS_CSC_FILE:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 136
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->OTHERS_CSC_FILE:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscHomescreen;->getWallpaperComponentFromFile(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v15

    .line 138
    .local v15, "name":Landroid/content/ComponentName;
    if-eqz v15, :cond_2

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mWallpaperManager:Landroid/app/WallpaperManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Landroid/content/ComponentName;->compareTo(Landroid/content/ComponentName;)I

    move-result v19

    if-eqz v19, :cond_2

    .line 140
    const-string v19, "[Error]CSCHomescreen: SettingsData.ForceLiveWallpaper tag(in others.xml) is not matched with wallpaperManager info!"

    .line 257
    :goto_0
    return-object v19

    .line 148
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "Settings.Main.Display.NbWallpaper"

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 149
    .local v17, "xmlCnt":Ljava/lang/String;
    new-instance v10, Ljava/io/File;

    const-string v19, "/system/wallpaper/drawable/"

    move-object/from16 v0, v19

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 151
    .local v10, "folder":Ljava/io/File;
    const-string v19, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_3

    if-nez v17, :cond_4

    .line 162
    :cond_3
    const-string v19, "NOERROR"

    goto :goto_0

    .line 164
    :cond_4
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 165
    .local v5, "bitmapCnt":I
    if-gtz v5, :cond_5

    .line 167
    const-string v19, "Settings.Main.Display.NbWallpaper"

    const-string v20, " <= 0"

    const/16 v21, 0x0

    invoke-static/range {v19 .. v21}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v19, "CSCHomescreen:Settings.Main.Display.NbWallpaper invalid.!"

    invoke-static/range {v19 .. v19}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 170
    const-string v19, "[Error]CSCHomescreen:Settings.Main.Display.NbWallpaper invalid.!"

    goto :goto_0

    .line 172
    :cond_5
    const-string v19, "Settings.Main.Display.NbWallpaper"

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-virtual {v10}, Ljava/io/File;->isDirectory()Z

    move-result v19

    if-nez v19, :cond_6

    .line 177
    const-string v19, "Settings.Main.Display.NbWallpaper"

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    const-string v21, "0"

    invoke-static/range {v19 .. v21}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v19, "CSCHomescreen:Settings.Main.Display.NbWallpaper valid, but (/system/wallpaper/drawable/) folder cannot find."

    invoke-static/range {v19 .. v19}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 181
    const-string v19, "[Error]CSCHomescreen:Settings.Main.Display.NbWallpaper valid, but (/system/wallpaper/drawable/) folder cannot find."

    goto :goto_0

    .line 184
    :cond_6
    const-string v19, "Settings.Main.Display.Wallpaper.src"

    const-string v20, "Valid"

    invoke-static/range {v19 .. v20}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-ge v11, v5, :cond_d

    .line 209
    new-instance v19, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v19, v0

    const-string v20, "Settings.Main.Display.Wallpaper.src"

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v11, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->getAttrbute(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 212
    .local v4, "attrSlash":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    aget-object v18, v4, v19

    .line 213
    .local v18, "xmlPath":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v20, 0x0

    const/16 v21, 0x2e

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v21

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_small"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x2e

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v20

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v21

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 217
    .local v16, "thumbnailPath":Ljava/lang/String;
    const/4 v9, 0x0

    .line 218
    .local v9, "findXmlPath":Z
    const/4 v8, 0x0

    .line 220
    .local v8, "findThunmbnailPath":Z
    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v14

    .line 221
    .local v14, "list":[Ljava/io/File;
    move-object v3, v14

    .local v3, "arr$":[Ljava/io/File;
    array-length v13, v3

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_2
    if-ge v12, v13, :cond_9

    aget-object v6, v3, v12

    .line 223
    .local v6, "f":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->isFile()Z

    move-result v19

    if-eqz v19, :cond_7

    .line 224
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 225
    array-length v0, v4

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    aget-object v7, v4, v19

    .line 227
    .local v7, "fileName":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 228
    const-string v19, "Settings.Main.Display.Wallpaper.src"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const/4 v9, 0x1

    .line 221
    .end local v7    # "fileName":Ljava/lang/String;
    :cond_7
    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 230
    .restart local v7    # "fileName":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 231
    const-string v19, "Settings.Main.Display.Wallpaper.src"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const/4 v8, 0x1

    goto :goto_3

    .line 237
    .end local v6    # "f":Ljava/io/File;
    .end local v7    # "fileName":Ljava/lang/String;
    :cond_9
    and-int v19, v9, v8

    if-nez v19, :cond_c

    .line 241
    if-nez v9, :cond_a

    .line 242
    const-string v19, "Settings.Main.Display.Wallpaper.src"

    const-string v20, "Not found"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :cond_a
    if-nez v8, :cond_b

    .line 244
    const-string v19, "Settings.Main.Display.Wallpaper.src"

    const-string v20, "Not found"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_b
    const-string v19, "CSCHomescreen:Settings.Main.Display.Wallpaper.src not matched with (/system/wallpaper/drawable/) folder files"

    invoke-static/range {v19 .. v19}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 249
    const-string v19, "[Error]CSCHomescreen:Settings.Main.Display.Wallpaper.src not matched with (/system/wallpaper/drawable/) folder files"

    goto/16 :goto_0

    .line 206
    :cond_c
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 257
    .end local v3    # "arr$":[Ljava/io/File;
    .end local v4    # "attrSlash":[Ljava/lang/String;
    .end local v8    # "findThunmbnailPath":Z
    .end local v9    # "findXmlPath":Z
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v14    # "list":[Ljava/io/File;
    .end local v16    # "thumbnailPath":Ljava/lang/String;
    .end local v18    # "xmlPath":Ljava/lang/String;
    :cond_d
    const-string v19, "NOERROR"

    goto/16 :goto_0
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 0
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 266
    return-void
.end method

.method public update()V
    .locals 5

    .prologue
    .line 105
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v2, :cond_0

    .line 106
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 108
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v2, :cond_1

    .line 109
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 111
    :cond_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscHomescreen;->getWallpaperComponentFromFile(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    .line 113
    .local v1, "name":Landroid/content/ComponentName;
    if-eqz v1, :cond_2

    .line 115
    :try_start_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {v2}, Landroid/app/WallpaperManager;->getIWallpaperManager()Landroid/app/IWallpaperManager;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/app/IWallpaperManager;->setWallpaperComponent(Landroid/content/ComponentName;)V

    .line 116
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscHomescreen;->mWallpaperManager:Landroid/app/WallpaperManager;

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/WallpaperManager;->setWallpaperOffsetSteps(FF)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 125
    :cond_2
    :goto_0
    return-void

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "CSCHomescreen"

    const-string v3, "Failure to connect to wallpaper service"

    invoke-static {v2, v3, v0}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 121
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 122
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v2, "CSCHomescreen"

    const-string v3, "Failure setting wallpaper"

    invoke-static {v2, v3, v0}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
