.class public Lcom/samsung/sec/android/application/csc/CscChromeBrowser;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscChromeBrowser.java"


# static fields
.field static final PARTNER_BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

.field static final PARTNER_BOOKMARKS_CONTENT_URI_TABLE:Landroid/net/Uri;

.field private static final SEND_PACKAGE_CHROME:Landroid/content/ComponentName;


# instance fields
.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final OTHERS_CSC_FILE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 87
    const-string v0, "content://com.android.partnerbookmarks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->PARTNER_BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

    .line 90
    const-string v0, "content://com.android.partnerbookmarks/bookmarks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->PARTNER_BOOKMARKS_CONTENT_URI_TABLE:Landroid/net/Uri;

    .line 93
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.chrome"

    const-string v2, "com.android.chrome.Main"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->SEND_PACKAGE_CHROME:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 60
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 78
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->OTHERS_CSC_FILE:Ljava/lang/String;

    .line 81
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 97
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mContext:Landroid/content/Context;

    .line 98
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mResolver:Landroid/content/ContentResolver;

    .line 101
    const-string v1, "com.android.providers.partnerbookmarks"

    .line 103
    .local v1, "packageName":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "CscChromeBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CscChromeBrowser: packageName"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not there"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isBrowserInstalled()Z
    .locals 7

    .prologue
    .line 460
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 461
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    .line 464
    .local v1, "mIsFound":Z
    :try_start_0
    const-string v4, "com.sec.android.app.sbrowser"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 473
    :goto_0
    const-string v4, "CscChromeBrowser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " ** isBrowserInstalled mIsFound="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    return v1

    .line 465
    :catch_0
    move-exception v0

    .line 467
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    const-string v4, "com.android.browser"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 468
    :catch_1
    move-exception v2

    .line 469
    .local v2, "ne":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 395
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Browser.Bookmark.Editable"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 396
    const-string v0, "Settings.Browser.Bookmark.Index"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 397
    const-string v0, "Settings.Browser.Bookmark.NetworkName"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 398
    const-string v0, "Settings.Browser.eManual.URL"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 399
    const-string v0, "Settings.Browser.CookieOption"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 400
    const-string v0, "Settings.Browser.HistoryList"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 401
    const-string v0, "Settings.Main.Network.AutoBookmark"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 402
    const-string v0, "BrowserData.HomePage.URL"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 403
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 18

    .prologue
    .line 291
    const-string v1, "CscChromeBrowser"

    const-string v2, " ** compare runs.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    new-instance v10, Ljava/lang/String;

    const-string v1, "NOERROR"

    invoke-direct {v10, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 295
    .local v10, "answer":Ljava/lang/String;
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 297
    .local v17, "salesCode":Ljava/lang/String;
    const/4 v8, 0x0

    .line 298
    .local v8, "FOUND_NO":I
    const/4 v9, 0x1

    .line 299
    .local v9, "FOUND_TITLE":I
    const/4 v7, 0x2

    .line 300
    .local v7, "FOUND_ALL":I
    const/4 v14, 0x0

    .line 312
    .local v14, "fnd":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->getBookmarks()Ljava/util/ArrayList;

    move-result-object v12

    .line 313
    .local v12, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_8

    .line 314
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->PARTNER_BOOKMARKS_CONTENT_URI_TABLE:Landroid/net/Uri;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "title"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "url"

    aput-object v5, v3, v4

    const-string v4, "parent = 1"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 318
    .local v13, "dbCur":Landroid/database/Cursor;
    if-nez v13, :cond_0

    .line 319
    const-string v1, "CscChromeBrowser"

    const-string v2, "dbCur is null. so could not check the bookmarks in DB"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v11, v10

    .line 389
    .end local v10    # "answer":Ljava/lang/String;
    .end local v13    # "dbCur":Landroid/database/Cursor;
    .local v11, "answer":Ljava/lang/Object;
    :goto_0
    return-object v11

    .line 323
    .end local v11    # "answer":Ljava/lang/Object;
    .restart local v10    # "answer":Ljava/lang/String;
    .restart local v13    # "dbCur":Landroid/database/Cursor;
    :cond_0
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    if-eq v1, v2, :cond_1

    .line 324
    const-string v1, "CscChromeBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "compare : dbCur.getCount() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bookmarks.size()/2 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    new-instance v10, Ljava/lang/String;

    .end local v10    # "answer":Ljava/lang/String;
    const-string v1, "PartnerBookmarksProvider Bookmarks count not matched"

    invoke-direct {v10, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 327
    .restart local v10    # "answer":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bookmarkFromDb: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BookmarkFromCsc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v10, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    move-object v11, v10

    .line 330
    .restart local v11    # "answer":Ljava/lang/Object;
    goto :goto_0

    .line 333
    .end local v11    # "answer":Ljava/lang/Object;
    :cond_1
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_7

    .line 334
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 335
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v15, v1, :cond_3

    .line 336
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_2
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v1

    move/from16 v0, v16

    if-ge v0, v1, :cond_2

    .line 337
    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 338
    const/4 v14, 0x1

    .line 340
    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 341
    const/4 v14, 0x2

    .line 342
    const-string v2, "Settings.Browser.Bookmark.BookmarkName"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v3, v15, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") BookmarkName: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    const-string v2, "Settings.Browser.Bookmark.URL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v3, v15, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") URL: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_2
    const/4 v1, 0x1

    if-ne v14, v1, :cond_5

    .line 355
    const-string v1, "CscChromeBrowser"

    const-string v2, " ** compare : Bookmark URL is missied.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const-string v1, "CscChromeBrowser : ** compare : Bookmark URL is missied.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 359
    const-string v10, "Settings.Browser.Bookmark.URL"

    .line 360
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-int/lit8 v2, v15, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v1, v15, 0x1

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v10, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    .end local v16    # "j":I
    :cond_3
    :goto_3
    const-string v1, "CscChromeBrowser"

    const-string v2, " ** compare : Bookmarks DONE.."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    .end local v15    # "i":I
    :goto_4
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .end local v13    # "dbCur":Landroid/database/Cursor;
    :goto_5
    move-object v11, v10

    .line 389
    .restart local v11    # "answer":Ljava/lang/Object;
    goto/16 :goto_0

    .line 351
    .end local v11    # "answer":Ljava/lang/Object;
    .restart local v13    # "dbCur":Landroid/database/Cursor;
    .restart local v15    # "i":I
    .restart local v16    # "j":I
    :cond_4
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    .line 336
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_2

    .line 363
    :cond_5
    if-nez v14, :cond_6

    .line 364
    const-string v1, "CscChromeBrowser"

    const-string v2, " ** compare : Bookmark Name is missed.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const-string v1, "CscChromeBrowser : ** compare : Bookmark Name is missed.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 368
    const-string v10, "Settings.Browser.Bookmark.BookmarkName"

    .line 369
    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v10, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 372
    :cond_6
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 373
    const/4 v14, 0x0

    .line 335
    add-int/lit8 v15, v15, 0x2

    goto/16 :goto_1

    .line 379
    .end local v15    # "i":I
    .end local v16    # "j":I
    :cond_7
    const-string v1, "CscChromeBrowser"

    const-string v2, " ** compare : Browser DB is empty.."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const-string v1, "CscChromeBrowser : ** compare : Browser DB is empty.."

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 382
    const-string v10, "Settings.Browser."

    goto :goto_4

    .line 386
    .end local v13    # "dbCur":Landroid/database/Cursor;
    :cond_8
    const-string v1, "CscChromeBrowser"

    const-string v2, " ** compare : No bookmarks were given. NOERROR.."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public getBookmarks()Ljava/util/ArrayList;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 406
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 407
    .local v5, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v14, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v14, v15}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 408
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "Settings.Browser."

    invoke-virtual {v14, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 409
    .local v6, "browserNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "Bookmark"

    invoke-virtual {v14, v6, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    .line 410
    .local v7, "browserNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v7, :cond_3

    .line 411
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v14

    if-ge v13, v14, :cond_4

    .line 412
    invoke-interface {v7, v13}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 413
    .local v8, "browserNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "BookmarkName"

    invoke-virtual {v14, v8, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 414
    .local v1, "bookmarkName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "URL"

    invoke-virtual {v14, v8, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 416
    .local v3, "bookmarkUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v14, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    .line 417
    .local v2, "bookmarkNames":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 418
    const-string v14, "CscChromeBrowser"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " getBookmarks: getBookmarks ("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ") : bookmark name is null. Skip.."

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v14, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 422
    .local v4, "bookmarkUrls":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 423
    const-string v14, "CscChromeBrowser"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " getBookmarks: getBookmarks ("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ") : bookmark url is null. Skip.."

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    :cond_1
    if-eqz v4, :cond_2

    if-eqz v2, :cond_2

    .line 427
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 430
    :cond_2
    const-string v14, "CscChromeBrowser"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " getBookmarks: getBookmarks ("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ") : bookmarkUrls or bookmarkNames is null. Skip.."

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 435
    .end local v1    # "bookmarkName":Lorg/w3c/dom/Node;
    .end local v2    # "bookmarkNames":Ljava/lang/String;
    .end local v3    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .end local v4    # "bookmarkUrls":Ljava/lang/String;
    .end local v8    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .end local v13    # "i":I
    :cond_3
    const-string v14, "CscChromeBrowser"

    const-string v15, " ** getBookmarks : bookmarkNodeList is null. Skip.."

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "eManual"

    invoke-virtual {v14, v6, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 439
    .local v10, "eManualBookmarkNode":Lorg/w3c/dom/Node;
    if-eqz v10, :cond_5

    .line 440
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->isBrowserInstalled()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 441
    const-string v14, "CscChromeBrowser"

    const-string v15, " ** getBookmarks : Chrome does not support eManual. Skip.."

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    :cond_5
    :goto_2
    return-object v5

    .line 443
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v15, "URL"

    invoke-virtual {v14, v10, v15}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v12

    .line 444
    .local v12, "eManualUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mContext:Landroid/content/Context;

    const v15, 0x7f04000e

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 445
    .local v9, "eManualBookmarkName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v14, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v11

    .line 446
    .local v11, "eManualBookmarkUrl":Ljava/lang/String;
    if-eqz v11, :cond_7

    .line 447
    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    const-string v14, "CscChromeBrowser"

    const-string v15, " ** getBookmarks : eManual bookmark added"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 451
    :cond_7
    const-string v14, "CscChromeBrowser"

    const-string v15, " ** getBookmarks : eManual bookmark url is null. Skip.."

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public update()V
    .locals 7

    .prologue
    .line 113
    const-string v3, "CscChromeBrowser"

    const-string v4, " ** update runs.."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 117
    .local v2, "salesCode":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_Web_BookmarkPreloadOnChrome"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "NONE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 120
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->isBrowserInstalled()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "TFG"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 121
    :cond_0
    const-string v3, "CscChromeBrowser"

    const-string v4, " update :  do not update customer bookmark"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :cond_1
    :goto_0
    return-void

    .line 127
    :cond_2
    const/4 v0, 0x0

    .line 128
    .local v0, "cpClient":Landroid/content/ContentProviderClient;
    new-instance v3, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 130
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mResolver:Landroid/content/ContentResolver;

    if-nez v3, :cond_3

    .line 131
    const-string v3, "CscChromeBrowser"

    const-string v4, " update :  mResolver is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 136
    :cond_3
    :try_start_0
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->PARTNER_BOOKMARKS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 142
    :goto_1
    if-nez v0, :cond_4

    .line 143
    const-string v3, "CscChromeBrowser"

    const-string v4, " update : Phone does not ready to acquire Provider"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 137
    :catch_0
    move-exception v1

    .line 138
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "CscChromeBrowser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " update : acquireContentProviderClient.Exception ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    const/4 v0, 0x0

    goto :goto_1

    .line 148
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_1
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->PARTNER_BOOKMARKS_CONTENT_URI_TABLE:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 152
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->updateBookmarks()V

    .line 154
    if-eqz v0, :cond_1

    .line 155
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_0

    .line 149
    :catch_1
    move-exception v1

    .line 150
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v3, "CscChromeBrowser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " update : delete.Exception ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public updateBookmarks()V
    .locals 38

    .prologue
    .line 161
    new-instance v34, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->DEFAULT_CSC_FILE:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-direct/range {v34 .. v35}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 163
    const-string v34, "CscChromeBrowser"

    const-string v35, " updateBookmarks : It is called."

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    const/4 v8, 0x1

    .line 167
    .local v8, "BOOKMARK_TYPE_BOOKMARK":I
    const/4 v9, 0x2

    .line 169
    .local v9, "BOOKMARK_TYPE_FOLDER":I
    const-wide/16 v6, 0x0

    .line 171
    .local v6, "BOOKMARK_PARENT_ROOT_ID":J
    const-wide/16 v4, 0x1

    .line 173
    .local v4, "BOOKMARK_PARENT_FOLDER_ID":J
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v34

    const-string v35, "CscFeature_Web_DefBookmarkFolderName"

    invoke-virtual/range {v34 .. v35}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 176
    .local v10, "BookmarkTitle":Ljava/lang/String;
    const-wide/16 v30, 0x2

    .line 181
    .local v30, "startID":J
    new-instance v29, Landroid/content/ContentValues;

    invoke-direct/range {v29 .. v29}, Landroid/content/ContentValues;-><init>()V

    .line 182
    .local v29, "values":Landroid/content/ContentValues;
    const-string v34, "_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 184
    const/4 v11, 0x0

    .line 185
    .local v11, "TempString":[Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-eq v0, v1, :cond_0

    const-string v34, ","

    move-object/from16 v0, v34

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    array-length v0, v11

    move/from16 v34, v0

    const/16 v35, 0x2

    move/from16 v0, v34

    move/from16 v1, v35

    if-eq v0, v1, :cond_5

    .line 187
    :cond_0
    const-string v10, "Samsung Mobile"

    .line 196
    :goto_0
    const-string v34, "title"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const-string v34, "parent"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 198
    const-string v34, "type"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    if-eqz v34, :cond_1

    .line 201
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    sget-object v35, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->PARTNER_BOOKMARKS_CONTENT_URI_TABLE:Landroid/net/Uri;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v27

    .line 211
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "Settings.Browser."

    invoke-virtual/range {v34 .. v35}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v17

    .line 212
    .local v17, "browserNode":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "Bookmark"

    move-object/from16 v0, v34

    move-object/from16 v1, v17

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v18

    .line 213
    .local v18, "browserNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v18, :cond_7

    .line 214
    const/16 v26, 0x0

    .local v26, "i":I
    :goto_2
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v34

    move/from16 v0, v26

    move/from16 v1, v34

    if-ge v0, v1, :cond_7

    .line 216
    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v19

    .line 217
    .local v19, "browserNodeListChild":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "BookmarkName"

    move-object/from16 v0, v34

    move-object/from16 v1, v19

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v12

    .line 218
    .local v12, "bookmarkName":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "URL"

    move-object/from16 v0, v34

    move-object/from16 v1, v19

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v14

    .line 220
    .local v14, "bookmarkUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v0, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v13

    .line 221
    .local v13, "bookmarkNames":Ljava/lang/String;
    if-nez v13, :cond_2

    .line 222
    const-string v34, "CscChromeBrowser"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, " updateBookmarks: getBookmarks ("

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, ") : bookmark name is null. Skip.."

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v0, v14}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v15

    .line 226
    .local v15, "bookmarkUrls":Ljava/lang/String;
    if-nez v15, :cond_3

    .line 227
    const-string v34, "CscChromeBrowser"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, " updateBookmarks: getBookmarks ("

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, ") : bookmark url is null. Skip.."

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :cond_3
    if-eqz v15, :cond_4

    if-eqz v13, :cond_4

    .line 231
    new-instance v28, Landroid/content/ContentValues;

    invoke-direct/range {v28 .. v28}, Landroid/content/ContentValues;-><init>()V

    .line 232
    .local v28, "opBookmark":Landroid/content/ContentValues;
    const-string v34, "_id"

    const-wide/16 v36, 0x1

    add-long v32, v30, v36

    .end local v30    # "startID":J
    .local v32, "startID":J
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 233
    const-string v34, "title"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v34, "url"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v34, "parent"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 236
    const-string v34, "type"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    if-eqz v34, :cond_c

    .line 239
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    sget-object v35, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->PARTNER_BOOKMARKS_CONTENT_URI_TABLE:Landroid/net/Uri;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v27

    .local v27, "inserted_uri":Landroid/net/Uri;
    move-wide/from16 v30, v32

    .line 214
    .end local v27    # "inserted_uri":Landroid/net/Uri;
    .end local v28    # "opBookmark":Landroid/content/ContentValues;
    .end local v32    # "startID":J
    .restart local v30    # "startID":J
    :cond_4
    :goto_3
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_2

    .line 189
    .end local v12    # "bookmarkName":Lorg/w3c/dom/Node;
    .end local v13    # "bookmarkNames":Ljava/lang/String;
    .end local v14    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .end local v15    # "bookmarkUrls":Ljava/lang/String;
    .end local v17    # "browserNode":Lorg/w3c/dom/Node;
    .end local v18    # "browserNodeList":Lorg/w3c/dom/NodeList;
    .end local v19    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .end local v26    # "i":I
    :cond_5
    const/16 v34, 0x0

    aget-object v34, v11, v34

    const-string v35, "Chrome"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_6

    .line 190
    const/16 v34, 0x1

    aget-object v10, v11, v34

    goto/16 :goto_0

    .line 192
    :cond_6
    const-string v10, "Samsung Mobile"

    goto/16 :goto_0

    .line 202
    :catch_0
    move-exception v20

    .line 203
    .local v20, "e":Ljava/lang/Exception;
    const-string v34, "CscChromeBrowser"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, " updateBookmarks : insert parent folder to db. Exception - "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 241
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v30    # "startID":J
    .restart local v12    # "bookmarkName":Lorg/w3c/dom/Node;
    .restart local v13    # "bookmarkNames":Ljava/lang/String;
    .restart local v14    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .restart local v15    # "bookmarkUrls":Ljava/lang/String;
    .restart local v17    # "browserNode":Lorg/w3c/dom/Node;
    .restart local v18    # "browserNodeList":Lorg/w3c/dom/NodeList;
    .restart local v19    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .restart local v26    # "i":I
    .restart local v28    # "opBookmark":Landroid/content/ContentValues;
    .restart local v32    # "startID":J
    :catch_1
    move-exception v20

    .line 242
    .restart local v20    # "e":Ljava/lang/Exception;
    const-string v34, "CscChromeBrowser"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, " updateBookmarks : insert bookmark items to db. Exception - "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide/from16 v30, v32

    .line 245
    .end local v32    # "startID":J
    .restart local v30    # "startID":J
    goto :goto_3

    .line 252
    .end local v12    # "bookmarkName":Lorg/w3c/dom/Node;
    .end local v13    # "bookmarkNames":Ljava/lang/String;
    .end local v14    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .end local v15    # "bookmarkUrls":Ljava/lang/String;
    .end local v19    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v26    # "i":I
    .end local v28    # "opBookmark":Landroid/content/ContentValues;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "eManual"

    move-object/from16 v0, v34

    move-object/from16 v1, v17

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v23

    .line 253
    .local v23, "eManualBookmarkNode":Lorg/w3c/dom/Node;
    if-eqz v23, :cond_8

    .line 254
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->isBrowserInstalled()Z

    move-result v34

    if-eqz v34, :cond_9

    .line 255
    const-string v34, "CscChromeBrowser"

    const-string v35, " ** updateBookmarks : Chrome does not support eManual. Skip.."

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :cond_8
    :goto_4
    const-string v34, "CscChromeBrowser"

    const-string v35, " ** updateBookmarks : Done..."

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    return-void

    .line 257
    :cond_9
    new-instance v21, Landroid/content/ContentValues;

    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    .line 258
    .local v21, "eManual":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    const-string v35, "URL"

    move-object/from16 v0, v34

    move-object/from16 v1, v23

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v25

    .line 260
    .local v25, "eManualUrl":Lorg/w3c/dom/Node;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    const v35, 0x7f04000e

    invoke-virtual/range {v34 .. v35}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 261
    .local v22, "eManualBookmarkName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v24

    .line 262
    .local v24, "eManualBookmarkUrl":Ljava/lang/String;
    if-eqz v24, :cond_b

    .line 263
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 264
    .local v16, "bookmarkmap":Landroid/content/ContentValues;
    const-string v34, "_id"

    const-wide/16 v36, 0x1

    add-long v32, v30, v36

    .end local v30    # "startID":J
    .restart local v32    # "startID":J
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 265
    const-string v34, "title"

    move-object/from16 v0, v21

    move-object/from16 v1, v34

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-string v34, "url"

    move-object/from16 v0, v21

    move-object/from16 v1, v34

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v34, "parent"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 268
    const-string v34, "type"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    if-eqz v34, :cond_a

    .line 271
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v34, v0

    sget-object v35, Lcom/samsung/sec/android/application/csc/CscChromeBrowser;->PARTNER_BOOKMARKS_CONTENT_URI_TABLE:Landroid/net/Uri;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v27

    :cond_a
    :goto_5
    move-wide/from16 v30, v32

    .line 280
    .end local v32    # "startID":J
    .restart local v30    # "startID":J
    goto/16 :goto_4

    .line 273
    .end local v30    # "startID":J
    .restart local v32    # "startID":J
    :catch_2
    move-exception v20

    .line 274
    .restart local v20    # "e":Ljava/lang/Exception;
    const-string v34, "CscChromeBrowser"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, " updateBookmarks : insert eManual to db. Exception - "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 281
    .end local v16    # "bookmarkmap":Landroid/content/ContentValues;
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v32    # "startID":J
    .restart local v30    # "startID":J
    :cond_b
    const-string v34, "CscChromeBrowser"

    const-string v35, " ** updateBookmarks : eManual bookmark url is null. Skip.."

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .end local v21    # "eManual":Landroid/content/ContentValues;
    .end local v22    # "eManualBookmarkName":Ljava/lang/String;
    .end local v23    # "eManualBookmarkNode":Lorg/w3c/dom/Node;
    .end local v24    # "eManualBookmarkUrl":Ljava/lang/String;
    .end local v25    # "eManualUrl":Lorg/w3c/dom/Node;
    .end local v30    # "startID":J
    .restart local v12    # "bookmarkName":Lorg/w3c/dom/Node;
    .restart local v13    # "bookmarkNames":Ljava/lang/String;
    .restart local v14    # "bookmarkUrl":Lorg/w3c/dom/Node;
    .restart local v15    # "bookmarkUrls":Ljava/lang/String;
    .restart local v19    # "browserNodeListChild":Lorg/w3c/dom/Node;
    .restart local v26    # "i":I
    .restart local v28    # "opBookmark":Landroid/content/ContentValues;
    .restart local v32    # "startID":J
    :cond_c
    move-wide/from16 v30, v32

    .end local v32    # "startID":J
    .restart local v30    # "startID":J
    goto/16 :goto_3
.end method
