.class public Lcom/samsung/sec/android/application/csc/CscModemSettingService;
.super Landroid/app/Service;
.source "CscModemSettingService.java"


# static fields
.field private static final isMarvell:Z


# instance fields
.field private CPLockCode:Ljava/lang/String;

.field private DEFAULT_CSC_FILE:Ljava/lang/String;

.field private NSPLockCode:Ljava/lang/String;

.field private NetLockCode:Ljava/lang/String;

.field private OTHERS_CSC_FILE:Ljava/lang/String;

.field private SPLockCode:Ljava/lang/String;

.field private amrVoiceCodec:I

.field private generalLockInfo:I

.field private gprsAttachMode:I

.field mByteArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mIntent:Landroid/content/Intent;

.field private mItemCount:I

.field private mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

.field private mRetryCount:I

.field private networkMode:I

.field private ssms:I

.field private strNetworkMode:Ljava/lang/String;

.field private strSalesCode:Ljava/lang/String;

.field private unlockCnt:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 157
    const-string v0, "mrvl"

    const-string v1, "ro.board.platform"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SM-G3568V"

    const-string v1, "ro.product.model"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->isMarvell:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 47
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 49
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->OTHERS_CSC_FILE:Ljava/lang/String;

    .line 121
    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 123
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strSalesCode:Ljava/lang/String;

    .line 125
    const-string v0, "auto"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strNetworkMode:Ljava/lang/String;

    .line 127
    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->networkMode:I

    .line 129
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->gprsAttachMode:I

    .line 131
    iput v2, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->ssms:I

    .line 133
    iput v2, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->amrVoiceCodec:I

    .line 135
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->SPLockCode:Ljava/lang/String;

    .line 139
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NSPLockCode:Ljava/lang/String;

    .line 141
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->CPLockCode:Ljava/lang/String;

    .line 143
    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    .line 145
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->unlockCnt:I

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    .line 151
    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 153
    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I

    .line 155
    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mIntent:Landroid/content/Intent;

    .line 803
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;-><init>(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->updateLockInfo()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->updateModemInfo()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->compareLockAndModemInfo()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/sec/android/application/csc/CscModemSettingService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscModemSettingService;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I

    return p1
.end method

.method static synthetic access$408(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->readModemInfo()V

    return-void
.end method

.method static synthetic access$602(Lcom/samsung/sec/android/application/csc/CscModemSettingService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscModemSettingService;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    return p1
.end method

.method static synthetic access$700()Z
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->isMarvell:Z

    return v0
.end method

.method private compareLockAndModemInfo()V
    .locals 3

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->readModemInfo()V

    .line 227
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->readLockInfo()V

    .line 229
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 230
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    .line 231
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->addModemInfoToBuffer()V

    .line 232
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->addLockInfoToBuffer()V

    .line 234
    const-string v0, "CscModemSettingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compareLockAndModemInfo() : mByteArray = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    const/4 v0, 0x7

    const/4 v1, 0x1

    const/16 v2, 0x7d2

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->sendByteBuffer(III)V

    .line 236
    return-void
.end method

.method private readLockInfo()V
    .locals 13

    .prologue
    .line 427
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v10}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 428
    .local v1, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const/4 v5, 0x5

    .line 430
    .local v5, "plmnLength":I
    const-string v10, ""

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    .line 431
    const-string v10, ""

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->SPLockCode:Ljava/lang/String;

    .line 432
    const-string v10, ""

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NSPLockCode:Ljava/lang/String;

    .line 433
    const-string v10, ""

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->CPLockCode:Ljava/lang/String;

    .line 436
    const-string v10, "Settings.Main.Security.UnlockCnt"

    invoke-virtual {v1, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 437
    .local v9, "strlockCnt":Ljava/lang/String;
    if-nez v9, :cond_3

    .line 438
    const/4 v10, 0x3

    iput v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->unlockCnt:I

    .line 443
    :goto_0
    const-string v10, "Settings.Main.Security."

    invoke-virtual {v1, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 444
    .local v8, "securityNode":Lorg/w3c/dom/Node;
    const-string v10, "NetworkLock"

    invoke-virtual {v1, v8, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 447
    .local v4, "lockNodeList":Lorg/w3c/dom/NodeList;
    const-string v10, "ril.sales_code"

    const-string v11, "none"

    invoke-static {v10, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 448
    .local v7, "salesCode":Ljava/lang/String;
    const-string v10, "ril.product_code"

    const-string v11, "none"

    invoke-static {v10, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 450
    .local v6, "productCode":Ljava/lang/String;
    const-string v10, "CscModemSettingService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "salesCode : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    const-string v10, "CscModemSettingService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "productCode : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    const-string v10, "TFG"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 453
    if-eqz v6, :cond_1

    const-string v10, "COB"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "CRM"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "PBS"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 454
    :cond_0
    const/4 v4, 0x0

    .line 455
    const-string v10, "CscModemSettingService"

    const-string v11, "COB or CRM or PBS : Unlock"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :cond_1
    const-string v10, "CscModemSettingService"

    const-string v11, "Not COB or CRM or PBS"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    :cond_2
    if-eqz v4, :cond_6

    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    if-lez v10, :cond_6

    .line 461
    iget v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    or-int/lit8 v10, v10, 0x1

    iput v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    .line 462
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    if-ge v3, v10, :cond_5

    .line 463
    invoke-interface {v4, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 464
    .local v0, "codeNode":Lorg/w3c/dom/Node;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-virtual {v1, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 465
    .local v2, "currentCode":Ljava/lang/StringBuffer;
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x5

    if-ne v10, v11, :cond_4

    .line 466
    const/4 v5, 0x5

    .line 467
    const-string v10, "#"

    invoke-virtual {v2, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 471
    :goto_2
    const-string v10, "CscModemSettingService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "NetworkLock > code ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    .line 462
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 440
    .end local v0    # "codeNode":Lorg/w3c/dom/Node;
    .end local v2    # "currentCode":Ljava/lang/StringBuffer;
    .end local v3    # "i":I
    .end local v4    # "lockNodeList":Lorg/w3c/dom/NodeList;
    .end local v6    # "productCode":Ljava/lang/String;
    .end local v7    # "salesCode":Ljava/lang/String;
    .end local v8    # "securityNode":Lorg/w3c/dom/Node;
    :cond_3
    new-instance v10, Ljava/lang/Integer;

    invoke-direct {v10, v9}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    iput v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->unlockCnt:I

    goto/16 :goto_0

    .line 469
    .restart local v0    # "codeNode":Lorg/w3c/dom/Node;
    .restart local v2    # "currentCode":Ljava/lang/StringBuffer;
    .restart local v3    # "i":I
    .restart local v4    # "lockNodeList":Lorg/w3c/dom/NodeList;
    .restart local v6    # "productCode":Ljava/lang/String;
    .restart local v7    # "salesCode":Ljava/lang/String;
    .restart local v8    # "securityNode":Lorg/w3c/dom/Node;
    :cond_4
    const/4 v5, 0x6

    goto :goto_2

    .line 476
    .end local v0    # "codeNode":Lorg/w3c/dom/Node;
    .end local v2    # "currentCode":Ljava/lang/StringBuffer;
    :cond_5
    if-eqz v6, :cond_6

    .line 478
    const-string v10, "SAL"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 480
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "706FF#"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    .line 495
    .end local v3    # "i":I
    :cond_6
    :goto_3
    const-string v10, "CscModemSettingService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "NetworkLock > all code : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    const-string v10, "Settings.Main.Security."

    invoke-virtual {v1, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 499
    const-string v10, "SubsetLock"

    invoke-virtual {v1, v8, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 500
    if-eqz v4, :cond_b

    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    if-lez v10, :cond_b

    .line 501
    iget v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    or-int/lit8 v10, v10, 0x2

    iput v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    .line 502
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_4
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    if-ge v3, v10, :cond_b

    .line 503
    invoke-interface {v4, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 504
    .restart local v0    # "codeNode":Lorg/w3c/dom/Node;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-virtual {v1, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 505
    .restart local v2    # "currentCode":Ljava/lang/StringBuffer;
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_7

    .line 506
    const-string v10, "#"

    invoke-virtual {v2, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 507
    :cond_7
    const-string v10, "CscModemSettingService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SubsetLock > code ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NSPLockCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NSPLockCode:Ljava/lang/String;

    .line 502
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 482
    .end local v0    # "codeNode":Lorg/w3c/dom/Node;
    .end local v2    # "currentCode":Ljava/lang/StringBuffer;
    :cond_8
    const-string v10, "UFU"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 484
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "748FF#"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    goto/16 :goto_3

    .line 486
    :cond_9
    const-string v10, "CHD"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    const-string v10, "CHE"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    const-string v10, "CHF"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    const-string v10, "CHG"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    const-string v10, "CHL"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    const-string v10, "CHO"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    const-string v10, "CHT"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    const-string v10, "CHV"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    const-string v10, "CHX"

    invoke-virtual {v6, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 490
    :cond_a
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "30237#30272#730FF#"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    goto/16 :goto_3

    .line 514
    .end local v3    # "i":I
    :cond_b
    const-string v10, "CscModemSettingService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SubsetLock > all code : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NSPLockCode:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    const-string v10, "Settings.Main.Security."

    invoke-virtual {v1, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 520
    const-string v10, "SPLock"

    invoke-virtual {v1, v8, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 521
    if-eqz v4, :cond_c

    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    if-lez v10, :cond_c

    .line 522
    iget v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    or-int/lit8 v10, v10, 0x8

    iput v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    .line 523
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_5
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    if-ge v3, v10, :cond_c

    .line 524
    invoke-interface {v4, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 525
    .restart local v0    # "codeNode":Lorg/w3c/dom/Node;
    invoke-virtual {v1, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    .line 526
    .local v2, "currentCode":Ljava/lang/String;
    const-string v10, "CscModemSettingService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SPLock > code ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->SPLockCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->SPLockCode:Ljava/lang/String;

    .line 523
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 531
    .end local v0    # "codeNode":Lorg/w3c/dom/Node;
    .end local v2    # "currentCode":Ljava/lang/String;
    .end local v3    # "i":I
    :cond_c
    const-string v10, "CscModemSettingService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SPLock > all code : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->SPLockCode:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    const-string v10, "Settings.Main.Security."

    invoke-virtual {v1, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 535
    const-string v10, "CPLock"

    invoke-virtual {v1, v8, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 536
    if-eqz v4, :cond_d

    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    if-lez v10, :cond_d

    .line 537
    iget v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    or-int/lit8 v10, v10, 0x10

    iput v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    .line 538
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_6
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    if-ge v3, v10, :cond_d

    .line 539
    invoke-interface {v4, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 540
    .restart local v0    # "codeNode":Lorg/w3c/dom/Node;
    invoke-virtual {v1, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    .line 541
    .restart local v2    # "currentCode":Ljava/lang/String;
    const-string v10, "CscModemSettingService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "CPLock > code ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->CPLockCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->CPLockCode:Ljava/lang/String;

    .line 538
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 546
    .end local v0    # "codeNode":Lorg/w3c/dom/Node;
    .end local v2    # "currentCode":Ljava/lang/String;
    .end local v3    # "i":I
    :cond_d
    const-string v10, "CscModemSettingService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "CPLock > all code : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->CPLockCode:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    return-void
.end method

.method private readModemInfo()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 275
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 276
    .local v2, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 278
    .local v1, "cscOtherParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const-string v4, "Settings.Main.Network.NetworkMode"

    invoke-virtual {v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strNetworkMode:Ljava/lang/String;

    .line 280
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strNetworkMode:Ljava/lang/String;

    if-nez v4, :cond_6

    .line 281
    const-string v4, "ro.csc.sales_code"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 282
    .local v3, "scode":Ljava/lang/String;
    const-string v4, "ro.csc.countryiso_code"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 283
    .local v0, "countryisocode":Ljava/lang/String;
    const-string v4, "SKT"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "KTT"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "LGT"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "KR"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 285
    :cond_0
    iput v7, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->networkMode:I

    .line 305
    .end local v0    # "countryisocode":Ljava/lang/String;
    .end local v3    # "scode":Ljava/lang/String;
    :cond_1
    :goto_0
    const-string v4, "Settings.Main.Network.AutoAttach"

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_c

    .line 306
    iput v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->gprsAttachMode:I

    .line 314
    :cond_2
    :goto_1
    const-string v4, "Settings.Messages.SMS.SSMS"

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_e

    .line 315
    iput v7, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->ssms:I

    .line 323
    :cond_3
    :goto_2
    const-string v4, "Settings.Main.Sound.AMRVoiceCodec"

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_10

    .line 324
    const/4 v4, -0x1

    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->amrVoiceCodec:I

    .line 332
    :cond_4
    :goto_3
    return-void

    .line 287
    .restart local v0    # "countryisocode":Ljava/lang/String;
    .restart local v3    # "scode":Ljava/lang/String;
    :cond_5
    iput v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->networkMode:I

    goto :goto_0

    .line 290
    .end local v0    # "countryisocode":Ljava/lang/String;
    .end local v3    # "scode":Ljava/lang/String;
    :cond_6
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strNetworkMode:Ljava/lang/String;

    const-string v5, "auto"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 291
    iput v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->networkMode:I

    goto :goto_0

    .line 292
    :cond_7
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strNetworkMode:Ljava/lang/String;

    const-string v5, "UMTS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 293
    const/4 v4, 0x2

    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->networkMode:I

    goto :goto_0

    .line 294
    :cond_8
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strNetworkMode:Ljava/lang/String;

    const-string v5, "GSM"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 295
    const/4 v4, 0x3

    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->networkMode:I

    goto :goto_0

    .line 296
    :cond_9
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strNetworkMode:Ljava/lang/String;

    const-string v5, "900"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 297
    const/4 v4, 0x4

    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->networkMode:I

    goto :goto_0

    .line 298
    :cond_a
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strNetworkMode:Ljava/lang/String;

    const-string v5, "1800"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 299
    const/4 v4, 0x5

    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->networkMode:I

    goto :goto_0

    .line 300
    :cond_b
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strNetworkMode:Ljava/lang/String;

    const-string v5, "1900"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 301
    const/4 v4, 0x6

    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->networkMode:I

    goto :goto_0

    .line 307
    :cond_c
    const-string v4, "Settings.Main.Network.AutoAttach"

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "on"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 308
    iput v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->gprsAttachMode:I

    goto :goto_1

    .line 309
    :cond_d
    const-string v4, "Settings.Main.Network.AutoAttach"

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "off"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 310
    iput v7, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->gprsAttachMode:I

    goto/16 :goto_1

    .line 316
    :cond_e
    const-string v4, "Settings.Messages.SMS.SSMS"

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "on"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 317
    iput v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->ssms:I

    goto/16 :goto_2

    .line 318
    :cond_f
    const-string v4, "Settings.Messages.SMS.SSMS"

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "off"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 319
    iput v7, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->ssms:I

    goto/16 :goto_2

    .line 325
    :cond_10
    const-string v4, "Settings.Main.Sound.AMRVoiceCodec"

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "enable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_11

    const-string v4, "Settings.Main.Sound.AMRVoiceCodec"

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "on"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 327
    :cond_11
    iput v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->amrVoiceCodec:I

    goto/16 :goto_3

    .line 328
    :cond_12
    const-string v4, "Settings.Main.Sound.AMRVoiceCodec"

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "disable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_13

    const-string v4, "Settings.Main.Sound.AMRVoiceCodec"

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "off"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 330
    :cond_13
    iput v7, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->amrVoiceCodec:I

    goto/16 :goto_3
.end method

.method private readSalesCode()V
    .locals 4

    .prologue
    .line 259
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 261
    .local v0, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const-string v1, "GeneralInfo.SalesCode"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strSalesCode:Ljava/lang/String;

    .line 262
    const-string v1, "CscModemSettingService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "csc modem strSalesCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strSalesCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    return-void
.end method

.method private updateLockInfo()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 403
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getSalesCode()Ljava/lang/String;

    move-result-object v2

    .line 404
    .local v2, "salesCode":Ljava/lang/String;
    const-string v3, "null"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 405
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/system/csc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 406
    .local v1, "new_CSC_FILE":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 407
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 408
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 411
    .end local v0    # "mFile":Ljava/io/File;
    .end local v1    # "new_CSC_FILE":Ljava/lang/String;
    :cond_0
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->readLockInfo()V

    .line 413
    iput v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 414
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    .line 415
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->addLockInfoToBuffer()V

    .line 417
    const-string v3, "CscModemSettingService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateLockInfo() : mByteArray Size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    const/16 v3, 0x8

    const/16 v4, 0x7d2

    invoke-virtual {p0, v3, v6, v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->sendByteBuffer(III)V

    .line 420
    return-void
.end method

.method private updateModemInfo()V
    .locals 5

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->readSalesCode()V

    .line 241
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->sendSalesCode()V

    .line 243
    sget-boolean v1, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->isMarvell:Z

    if-eqz v1, :cond_0

    .line 244
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->readModemInfo()V

    .line 245
    new-instance v0, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 246
    .local v0, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    const/16 v1, -0x73

    invoke-virtual {v0, v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 247
    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->ssms:I

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 248
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    if-eqz v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x7d4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 252
    .end local v0    # "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    :cond_0
    return-void
.end method


# virtual methods
.method public addLockInfoToBuffer()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 554
    const-string v4, "CscModemSettingService"

    const-string v5, "add Lock Info"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    const/16 v6, -0x7f

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 563
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 564
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 565
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 566
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    const/4 v6, 0x6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 567
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 568
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 571
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 572
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    iget v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 575
    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 577
    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    and-int/lit8 v4, v4, 0x1

    if-lez v4, :cond_1

    .line 578
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 579
    .local v3, "lockSize":I
    add-int/lit8 v2, v3, 0xf

    .line 580
    .local v2, "itemSize":I
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    const/16 v6, -0x7e

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 581
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 582
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 584
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    and-int/lit16 v6, v2, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 585
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    shr-int/lit8 v6, v2, 0x8

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 586
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v9}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 589
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 590
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 591
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 594
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v10}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 595
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 596
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    iget v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->unlockCnt:I

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 601
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v9}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 602
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 603
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    and-int/lit16 v6, v3, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 605
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    shr-int/lit8 v6, v3, 0x8

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 608
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    new-array v0, v4, [B

    .line 609
    .local v0, "dummyByteCode":[B
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NetLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 610
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_0

    .line 611
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    aget-byte v6, v0, v1

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 610
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 614
    :cond_0
    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 617
    .end local v0    # "dummyByteCode":[B
    .end local v1    # "i":I
    .end local v2    # "itemSize":I
    .end local v3    # "lockSize":I
    :cond_1
    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    and-int/lit8 v4, v4, 0x2

    if-lez v4, :cond_3

    .line 618
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NSPLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 619
    .restart local v3    # "lockSize":I
    add-int/lit8 v2, v3, 0xf

    .line 620
    .restart local v2    # "itemSize":I
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    const/16 v6, -0x7d

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 621
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 622
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 623
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 624
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    and-int/lit16 v6, v2, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 625
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    shr-int/lit8 v6, v2, 0x8

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 626
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v9}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 628
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 629
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 630
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 631
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 632
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 634
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v10}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 635
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 636
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 638
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    iget v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->unlockCnt:I

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v9}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 642
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 643
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    and-int/lit16 v6, v3, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 645
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    shr-int/lit8 v6, v3, 0x8

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 648
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NSPLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    new-array v0, v4, [B

    .line 649
    .restart local v0    # "dummyByteCode":[B
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->NSPLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 650
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    array-length v4, v0

    if-ge v1, v4, :cond_2

    .line 651
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    aget-byte v6, v0, v1

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 650
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 654
    :cond_2
    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 657
    .end local v0    # "dummyByteCode":[B
    .end local v1    # "i":I
    .end local v2    # "itemSize":I
    .end local v3    # "lockSize":I
    :cond_3
    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    and-int/lit8 v4, v4, 0x8

    if-lez v4, :cond_5

    .line 658
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->SPLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 659
    .restart local v3    # "lockSize":I
    add-int/lit8 v2, v3, 0xf

    .line 660
    .restart local v2    # "itemSize":I
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    const/16 v6, -0x7c

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 661
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 662
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 663
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 664
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    and-int/lit16 v6, v2, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 665
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    shr-int/lit8 v6, v2, 0x8

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 666
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v9}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 668
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 669
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 670
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 671
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 672
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 674
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v10}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 675
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 676
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 677
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 678
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    iget v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->unlockCnt:I

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 681
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v9}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 682
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 683
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    and-int/lit16 v6, v3, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 685
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    shr-int/lit8 v6, v3, 0x8

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 688
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->SPLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    new-array v0, v4, [B

    .line 689
    .restart local v0    # "dummyByteCode":[B
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->SPLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 690
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    array-length v4, v0

    if-ge v1, v4, :cond_4

    .line 691
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    aget-byte v6, v0, v1

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 694
    :cond_4
    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 697
    .end local v0    # "dummyByteCode":[B
    .end local v1    # "i":I
    .end local v2    # "itemSize":I
    .end local v3    # "lockSize":I
    :cond_5
    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->generalLockInfo:I

    and-int/lit8 v4, v4, 0x10

    if-lez v4, :cond_7

    .line 698
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->CPLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 699
    .restart local v3    # "lockSize":I
    add-int/lit8 v2, v3, 0xf

    .line 700
    .restart local v2    # "itemSize":I
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    const/16 v6, -0x7b

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 701
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 702
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 703
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 704
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    and-int/lit16 v6, v2, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 705
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    shr-int/lit8 v6, v2, 0x8

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 706
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v9}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 708
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 709
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 710
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 711
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 712
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 714
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v10}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 715
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 716
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v8}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 717
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 718
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    iget v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->unlockCnt:I

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 721
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v9}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 722
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    invoke-direct {v5, v7}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 723
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    and-int/lit16 v6, v3, 0xff

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 725
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    shr-int/lit8 v6, v3, 0x8

    int-to-byte v6, v6

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 728
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->CPLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    new-array v0, v4, [B

    .line 729
    .restart local v0    # "dummyByteCode":[B
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->CPLockCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 730
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    array-length v4, v0

    if-ge v1, v4, :cond_6

    .line 731
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/Byte;

    aget-byte v6, v0, v1

    invoke-direct {v5, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 730
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 734
    :cond_6
    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 736
    .end local v0    # "dummyByteCode":[B
    .end local v1    # "i":I
    .end local v2    # "itemSize":I
    .end local v3    # "lockSize":I
    :cond_7
    return-void
.end method

.method public addModemInfoToBuffer()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 335
    const-string v1, "CscModemSettingService"

    const-string v2, "add Modem Info"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    const/4 v0, 0x0

    .line 339
    .local v0, "dataSize":I
    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->networkMode:I

    if-lez v1, :cond_0

    .line 340
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    const/16 v3, -0x79

    invoke-direct {v2, v3}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v5}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    iget v3, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->networkMode:I

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    invoke-direct {v2, v3}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 351
    :cond_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    const/16 v3, -0x77

    invoke-direct {v2, v3}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v5}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    iget v3, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->gprsAttachMode:I

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    invoke-direct {v2, v3}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 361
    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->amrVoiceCodec:I

    if-ltz v1, :cond_1

    .line 362
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    const/16 v3, -0x75

    invoke-direct {v2, v3}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v5}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 367
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 368
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    iget v3, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->amrVoiceCodec:I

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    invoke-direct {v2, v3}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 370
    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 374
    :cond_1
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    const/16 v3, -0x73

    invoke-direct {v2, v3}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 376
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    const/16 v3, 0xb

    invoke-direct {v2, v3}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 382
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v5}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v5}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    iget v3, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->ssms:I

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    invoke-direct {v2, v3}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v6}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v5}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, v4}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 394
    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    .line 395
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 162
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 167
    new-instance v0, Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-direct {v0, p0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 168
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 221
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 222
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->disconnectFromRilService()V

    .line 223
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 173
    const-string v1, "CscModemSettingService"

    const-string v2, "onStart"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mIntent:Landroid/content/Intent;

    .line 176
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscModemSettingService$1;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscModemSettingService$1;-><init>(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 214
    .local v0, "CscModemSettingThread":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 215
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 217
    return-void
.end method

.method public sendByteBuffer(III)V
    .locals 7
    .param p1, "SUB_TYPE"    # I
    .param p2, "setType"    # I
    .param p3, "MsgType"    # I

    .prologue
    .line 785
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 786
    .local v1, "byteSize":I
    new-array v0, v1, [B

    .line 787
    .local v0, "byteData":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 788
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    aput-byte v4, v0, v2

    .line 787
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 790
    :cond_0
    const-string v4, "CscModemSettingService"

    const-string v5, "sendByteBuffer"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    new-instance v3, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/4 v4, 0x6

    int-to-byte v5, p1

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 793
    .local v3, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    const/4 v4, 0x7

    if-ne p1, v4, :cond_1

    .line 794
    int-to-byte v4, p2

    invoke-virtual {v3, v4}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 795
    :cond_1
    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I

    int-to-byte v4, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 796
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData([BII)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 798
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v3}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, p3, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 800
    return-void
.end method

.method public sendResetMsg()V
    .locals 5

    .prologue
    .line 766
    const-string v1, "CscModemSettingService"

    const-string v2, "sendResetMsg"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    new-instance v0, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/4 v1, 0x6

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 770
    .local v0, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 771
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    if-eqz v1, :cond_0

    .line 772
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x7d3

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 777
    :goto_0
    return-void

    .line 775
    :cond_0
    const-string v1, "CscModemSettingService"

    const-string v2, "sendResetMsg fail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendSalesCode()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 743
    const-string v1, "CscModemSettingService"

    const-string v2, "sendSalesCode"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strSalesCode:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 759
    :goto_0
    return-void

    .line 748
    :cond_0
    new-instance v0, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/4 v1, 0x6

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 750
    .local v0, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    invoke-virtual {v0, v3}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 751
    invoke-virtual {v0, v3}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 752
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strSalesCode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->strSalesCode:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData([BII)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 757
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x7d0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0
.end method
