.class public Lcom/samsung/sec/android/application/csc/CscUpdateService;
.super Landroid/app/Service;
.source "CscUpdateService.java"


# static fields
.field private static checkDelay:I

.field private static checkRetry:I


# instance fields
.field private final CHECK_FILE_NAME:Ljava/lang/String;

.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final MPS_CODE_DIR:Ljava/lang/String;

.field private final MPS_CODE_FILE:Ljava/lang/String;

.field private final PATH_CSCNAME:Ljava/lang/String;

.field private final PATH_CSCVERSION:Ljava/lang/String;

.field private final PATH_HANDDATA:Ljava/lang/String;

.field private final PATH_SALESCODE:Ljava/lang/String;

.field private PendingItems:Ljava/lang/String;

.field private final SEC_CSC_ZIP_FLAG:Ljava/lang/String;

.field private final SW_CONFIG_FILE:Ljava/lang/String;

.field private final TAG_APPROVALVER:Ljava/lang/String;

.field private final TAG_CODEFULLVERSION:Ljava/lang/String;

.field private final TAG_CSCFULLVERSION:Ljava/lang/String;

.field private final TAG_CUSTOMER:Ljava/lang/String;

.field private final TAG_MODEMFULLVERSION:Ljava/lang/String;

.field private arrayIndex:I

.field mHandler:Landroid/os/Handler;

.field private mHandlerPhone:Landroid/os/Handler;

.field private mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

.field mUpdatables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscTgBase;",
            ">;"
        }
    .end annotation
.end field

.field private strSalesCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 143
    const/16 v0, 0x3e8

    sput v0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkDelay:I

    .line 145
    const/16 v0, 0x64

    sput v0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkRetry:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 83
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getIDPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CHECK_FILE_NAME:Ljava/lang/String;

    .line 85
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 87
    const-string v0, "CodeFullVersion"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->TAG_CODEFULLVERSION:Ljava/lang/String;

    .line 89
    const-string v0, "ModemFullVersion"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->TAG_MODEMFULLVERSION:Ljava/lang/String;

    .line 91
    const-string v0, "CSCFullVersion"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->TAG_CSCFULLVERSION:Ljava/lang/String;

    .line 93
    const-string v0, "Customer"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->TAG_CUSTOMER:Ljava/lang/String;

    .line 95
    const-string v0, "ApprovalVer"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->TAG_APPROVALVER:Ljava/lang/String;

    .line 97
    const-string v0, "CustomerDataSet.CSCName"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PATH_CSCNAME:Ljava/lang/String;

    .line 99
    const-string v0, "GeneralInfo.SalesCode"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PATH_SALESCODE:Ljava/lang/String;

    .line 101
    const-string v0, "CustomerDataSet.CSCVersion"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PATH_CSCVERSION:Ljava/lang/String;

    .line 103
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->strSalesCode:Ljava/lang/String;

    .line 105
    const-string v0, "/efs/imei"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->MPS_CODE_DIR:Ljava/lang/String;

    .line 107
    const-string v0, "/efs/imei/mps_code.dat"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->MPS_CODE_FILE:Ljava/lang/String;

    .line 109
    const-string v0, "/cache/recovery/sec_csc.zip"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->SEC_CSC_ZIP_FLAG:Ljava/lang/String;

    .line 111
    const-string v0, "/system/SW_Configuration.xml"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->SW_CONFIG_FILE:Ljava/lang/String;

    .line 113
    const-string v0, "HandData"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PATH_HANDDATA:Ljava/lang/String;

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    .line 131
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->mHandler:Landroid/os/Handler;

    .line 141
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->arrayIndex:I

    .line 147
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    .line 809
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscUpdateService$12;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService$12;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->mHandlerPhone:Landroid/os/Handler;

    return-void
.end method

.method private CscAlarmtoneUpdate()V
    .locals 2

    .prologue
    .line 211
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscUpdateService$5;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService$5;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 217
    .local v0, "thread":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 218
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 219
    return-void
.end method

.method private CscAttResetVerify()V
    .locals 6

    .prologue
    .line 383
    const-string v0, ""

    .line 384
    .local v0, "ANSWER":Ljava/lang/String;
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscContents;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscContents;-><init>(Landroid/content/Context;)V

    .line 386
    .local v1, "cscContents":Lcom/samsung/sec/android/application/csc/CscContents;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUtil;->isCscUpdated()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 387
    const-string v3, "CscUpdateService"

    const-string v4, "Set update status to cscContents"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/samsung/sec/android/application/csc/CscContents;->SetUpdateStatus(Z)V

    .line 391
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscContents;->verifyResetDone()Ljava/lang/String;

    move-result-object v0

    .line 392
    const-string v3, "CscUpdateService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "verify result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.BCS_RESPONSE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 394
    .local v2, "i":Landroid/content/Intent;
    const-string v3, "response"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    invoke-virtual {p0, v2}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 397
    return-void
.end method

.method private CscChameleonUpdate(Z)V
    .locals 3
    .param p1, "b"    # Z

    .prologue
    .line 242
    move v0, p1

    .line 243
    .local v0, "reboot":Z
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscUpdateService$8;

    invoke-direct {v2, p0, v0}, Lcom/samsung/sec/android/application/csc/CscUpdateService$8;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;Z)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 248
    .local v1, "thread":Ljava/lang/Thread;
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setPriority(I)V

    .line 249
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 250
    return-void
.end method

.method private CscConnectionUpdate()V
    .locals 2

    .prologue
    .line 161
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscUpdateService$1;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService$1;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 169
    .local v0, "thread":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 170
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 171
    return-void
.end method

.method private CscConnectionUpdate(I)V
    .locals 5
    .param p1, "simSlot"    # I

    .prologue
    .line 174
    const-string v2, "CscUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CscConnectionUpdate, simSlot : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    move v0, p1

    .line 176
    .local v0, "fSimSlot":I
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscUpdateService$2;

    invoke-direct {v2, p0, v0}, Lcom/samsung/sec/android/application/csc/CscUpdateService$2;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;I)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 184
    .local v1, "thread":Ljava/lang/Thread;
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setPriority(I)V

    .line 185
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 186
    return-void
.end method

.method private CscGpsUpdate()V
    .locals 2

    .prologue
    .line 232
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscUpdateService$7;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService$7;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 237
    .local v0, "thread":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 238
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 239
    return-void
.end method

.method private CscInitTest(ZZ)V
    .locals 8
    .param p1, "isApo"    # Z
    .param p2, "isNeedCheckResult"    # Z

    .prologue
    const/4 v7, 0x0

    .line 331
    new-instance v2, Landroid/content/Intent;

    const-string v5, "com.sec.factory.SEND_TO_RIL"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 332
    .local v2, "SendToRilIntent":Landroid/content/Intent;
    const-string v1, "NG"

    .line 333
    .local v1, "ResultString":Ljava/lang/String;
    const/4 v3, 0x0

    .line 335
    .local v3, "is_modem_setting_done":Z
    if-eqz p2, :cond_0

    .line 336
    invoke-static {p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->isWifiOnly(Landroid/content/Context;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 337
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->chekUpdateDoneCondition()Z

    move-result v3

    .line 343
    :goto_0
    if-eqz v3, :cond_0

    .line 344
    const-string v1, "OK"

    .line 347
    :cond_0
    const-string v5, "CscUpdateService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Inittest result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    if-eqz p1, :cond_2

    .line 349
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\r\n+INITTEST:1,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\r\n\r\nOK\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 350
    const-string v5, "message"

    invoke-virtual {v2, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 356
    :goto_1
    invoke-virtual {p0, v2}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 357
    return-void

    .line 339
    :cond_1
    const-string v5, "csc.preferences_name"

    invoke-virtual {p0, v5, v7}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 340
    .local v4, "pref":Landroid/content/SharedPreferences;
    const-string v5, "csc.key.csc_modem_setting_done"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    goto :goto_0

    .line 352
    .end local v4    # "pref":Landroid/content/SharedPreferences;
    :cond_2
    const-string v5, "8D"

    const-string v6, "01"

    invoke-direct {p0, v5, v6, v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->setResultEndModeData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 353
    .local v0, "ResultBytes":[B
    const-string v5, "message"

    invoke-virtual {v2, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    goto :goto_1
.end method

.method private CscNotificationUpdate()V
    .locals 2

    .prologue
    .line 200
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscUpdateService$4;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService$4;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 206
    .local v0, "thread":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 207
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 208
    return-void
.end method

.method private CscPreferredPackageUpdate()V
    .locals 2

    .prologue
    .line 222
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscUpdateService$6;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService$6;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 227
    .local v0, "thread":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 228
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 229
    return-void
.end method

.method private CscResetVerify(Z)V
    .locals 7
    .param p1, "isApo"    # Z

    .prologue
    .line 360
    const-string v0, ""

    .line 361
    .local v0, "ANSWER":Ljava/lang/String;
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscContents;

    invoke-direct {v2, p0}, Lcom/samsung/sec/android/application/csc/CscContents;-><init>(Landroid/content/Context;)V

    .line 363
    .local v2, "cscContents":Lcom/samsung/sec/android/application/csc/CscContents;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUtil;->isCscUpdated()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 364
    const-string v4, "CscUpdateService"

    const-string v5, "Set update status to cscContents"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/samsung/sec/android/application/csc/CscContents;->SetUpdateStatus(Z)V

    .line 368
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscContents;->verifyResetDone()Ljava/lang/String;

    move-result-object v0

    .line 369
    const-string v4, "CscUpdateService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "verify result : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.factory.SEND_TO_RIL"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 372
    .local v3, "i":Landroid/content/Intent;
    if-eqz p1, :cond_1

    .line 373
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\r\n+RSTVERIF:0,"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\r\n\r\nOK\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 374
    const-string v4, "message"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    :goto_0
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 380
    return-void

    .line 376
    :cond_1
    const-string v4, "3E"

    const-string v5, "00"

    invoke-direct {p0, v4, v5, v0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->setResultEndModeData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    .line 377
    .local v1, "ResultBytes":[B
    const-string v4, "message"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    goto :goto_0
.end method

.method private CscRingtoneUpdate()V
    .locals 2

    .prologue
    .line 189
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscUpdateService$3;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService$3;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 195
    .local v0, "thread":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 196
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 197
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/sec/android/application/csc/CscUpdateService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscUpdateService;

    .prologue
    .line 56
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->arrayIndex:I

    return v0
.end method

.method static synthetic access$008(Lcom/samsung/sec/android/application/csc/CscUpdateService;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscUpdateService;

    .prologue
    .line 56
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->arrayIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->arrayIndex:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscUpdateService;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->doUpdate()V

    return-void
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 56
    sget v0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkRetry:I

    return v0
.end method

.method static synthetic access$210()I
    .locals 2

    .prologue
    .line 56
    sget v0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkRetry:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkRetry:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/sec/android/application/csc/CscUpdateService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscUpdateService;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->mHandlerPhone:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/sec/android/application/csc/CscUpdateService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscUpdateService;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 56
    sget v0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkDelay:I

    return v0
.end method

.method public static charToNum(C)I
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 293
    const/16 v0, 0x30

    if-gt v0, p0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 294
    add-int/lit8 v0, p0, -0x30

    .line 301
    :goto_0
    return v0

    .line 295
    :cond_0
    const/16 v0, 0x61

    if-gt v0, p0, :cond_1

    const/16 v0, 0x66

    if-gt p0, v0, :cond_1

    .line 296
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 297
    :cond_1
    const/16 v0, 0x41

    if-gt v0, p0, :cond_2

    const/16 v0, 0x46

    if-gt p0, v0, :cond_2

    .line 298
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 301
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private doUpdate()V
    .locals 2

    .prologue
    .line 619
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->arrayIndex:I

    .line 620
    invoke-static {p0}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscTgManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->mUpdatables:Ljava/util/ArrayList;

    .line 622
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscUpdateService$9;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService$9;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 639
    .local v0, "thread":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 640
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 641
    return-void
.end method

.method private doUpdateForHomeFOTA(I)V
    .locals 3
    .param p1, "binary_type"    # I

    .prologue
    .line 671
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 673
    .local v0, "fotaUpdateList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/UpdatableForHomeFOTA;>;"
    invoke-static {p0}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscTgManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 674
    invoke-static {p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->isWifiOnly(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 675
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscSimProfile;

    invoke-direct {v2, p0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 678
    :cond_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsung/sec/android/application/csc/CscUpdateService$11;

    invoke-direct {v2, p0, v0, p1}, Lcom/samsung/sec/android/application/csc/CscUpdateService$11;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;Ljava/util/ArrayList;I)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 696
    .local v1, "thread":Ljava/lang/Thread;
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setPriority(I)V

    .line 697
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 698
    return-void
.end method

.method private doUpdateForSubUser()V
    .locals 2

    .prologue
    .line 644
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->arrayIndex:I

    .line 645
    invoke-static {p0}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscTgManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscTgManager;->getList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->mUpdatables:Ljava/util/ArrayList;

    .line 647
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/sec/android/application/csc/CscUpdateService$10;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService$10;-><init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 661
    .local v0, "thread":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 662
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 664
    invoke-static {p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->powerSavingOn(Landroid/content/Context;)V

    .line 667
    const-string v1, "android.intent.action.CSC_STOPPED"

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->sendIntentForLockScreen(Ljava/lang/String;)V

    .line 668
    return-void
.end method

.method private isModelAPO()Z
    .locals 8

    .prologue
    .line 305
    const/4 v4, 0x0

    .line 306
    .local v4, "isAPO":Z
    const/4 v2, 0x0

    .line 309
    .local v2, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    const-string v5, "system/bin/at_distributor"

    invoke-direct {v3, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    .end local v2    # "fr":Ljava/io/FileReader;
    .local v3, "fr":Ljava/io/FileReader;
    const/4 v4, 0x1

    .line 311
    :try_start_1
    const-string v5, "CscUpdateService"

    const-string v6, "Found the at_distributor. Decided APO mode."

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 318
    if-eqz v3, :cond_2

    .line 320
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 327
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    :cond_0
    :goto_0
    return v4

    .line 321
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v0

    .line 322
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "CscUpdateService"

    const-string v6, "File Close error"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    .line 323
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_0

    .line 312
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 313
    .local v1, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    const/4 v4, 0x0

    .line 314
    :try_start_3
    const-string v5, "CscUpdateService"

    const-string v6, "Can not find at_distributor. Decided CPO mode"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 318
    if-eqz v2, :cond_0

    .line 320
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 321
    :catch_2
    move-exception v0

    .line 322
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "CscUpdateService"

    const-string v6, "File Close error"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 315
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v0

    .line 316
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    const-string v5, "CscUpdateService"

    const-string v6, "Can not decide because of error"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 318
    if-eqz v2, :cond_0

    .line 320
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 321
    :catch_4
    move-exception v0

    .line 322
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "CscUpdateService"

    const-string v6, "File Close error"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 318
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    if-eqz v2, :cond_1

    .line 320
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 323
    :cond_1
    :goto_4
    throw v5

    .line 321
    :catch_5
    move-exception v0

    .line 322
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v6, "CscUpdateService"

    const-string v7, "File Close error"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 318
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .line 315
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v0

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .line 312
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_1

    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :cond_2
    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_0
.end method

.method private isPreconfiged()Z
    .locals 4

    .prologue
    .line 601
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 602
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 603
    const-string v2, "CscUpdateService"

    const-string v3, "costomer file is exists : it has been preconfiged."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    const/4 v2, 0x1

    .line 614
    :goto_0
    return v2

    .line 606
    :cond_0
    const-string v2, "CscUpdateService"

    const-string v3, "costomer file is not exists : search sec_csc zip file."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    new-instance v1, Ljava/io/File;

    const-string v2, "/cache/recovery/sec_csc.zip"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 609
    .local v1, "file_csc":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 610
    const-string v2, "CscUpdateService"

    const-string v3, "sec_csc zip file is exists"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 612
    :cond_1
    const-string v2, "CscUpdateService"

    const-string v3, "sec_csc zip file is not exists"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private sendIntentForLockScreen(Ljava/lang/String;)V
    .locals 4
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 155
    const-string v1, "CscUpdateService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Intent for Lock Screen : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 157
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 158
    return-void
.end method

.method private setResultEndModeData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 12
    .param p1, "mainCMD"    # Ljava/lang/String;
    .param p2, "attr"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/String;

    .prologue
    .line 253
    const/4 v8, 0x2

    .line 254
    .local v8, "nLength":I
    const/4 v3, 0x1

    .line 256
    .local v3, "eventType":I
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 257
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 258
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v7, -0x1

    .line 259
    .local v7, "nAttr":I
    const/4 v10, 0x0

    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v10}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->charToNum(C)I

    move-result v10

    mul-int/lit8 v10, v10, 0x10

    const/4 v11, 0x1

    invoke-virtual {p1, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->charToNum(C)I

    move-result v11

    add-int v9, v10, v11

    .line 260
    .local v9, "nMainCMD":I
    if-eqz p2, :cond_0

    .line 261
    const/4 v10, 0x0

    invoke-virtual {p2, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v10}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->charToNum(C)I

    move-result v10

    mul-int/lit8 v10, v10, 0x10

    const/4 v11, 0x1

    invoke-virtual {p2, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->charToNum(C)I

    move-result v11

    add-int v7, v10, v11

    .line 264
    :cond_0
    if-eqz p3, :cond_1

    .line 265
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v8, v10

    .line 267
    :cond_1
    int-to-byte v5, v8

    .line 268
    .local v5, "length2":B
    shr-int/lit8 v10, v8, 0x8

    int-to-byte v4, v10

    .line 270
    .local v4, "length1":B
    add-int/lit8 v6, v8, 0x6

    .line 273
    .local v6, "mFileSize":I
    const/16 v10, 0x12

    :try_start_0
    invoke-virtual {v1, v10}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 274
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 275
    invoke-virtual {v1, v6}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 276
    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 277
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 278
    invoke-virtual {v1, v9}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 279
    invoke-virtual {v1, v7}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 281
    if-eqz p3, :cond_2

    .line 282
    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    :cond_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    :goto_0
    return-object v10

    .line 284
    :catch_0
    move-exception v2

    .line 285
    .local v2, "e":Ljava/io/IOException;
    const-string v10, "CscUpdateService"

    const-string v11, "Can\'t set at handler data"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    const/4 v10, 0x0

    goto :goto_0
.end method

.method private set_CSC_version()V
    .locals 19

    .prologue
    .line 847
    const-string v16, "CscUpdateService"

    const-string v17, "set_CSC_version"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    new-instance v6, Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v16, "/system/SW_Configuration.xml"

    move-object/from16 v0, v16

    invoke-direct {v6, v0}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 849
    .local v6, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const-string v16, "HandData"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 855
    .local v10, "handDataNode":Lorg/w3c/dom/Node;
    const-string v16, "ril.approved_codever"

    const-string v17, "none"

    invoke-static/range {v16 .. v17}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    invoke-static/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->isWifiOnly(Landroid/content/Context;)Z

    move-result v16

    if-nez v16, :cond_0

    .line 858
    const-string v16, "ril.approved_modemver"

    const-string v17, "none"

    invoke-static/range {v16 .. v17}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    :cond_0
    const-string v16, "ril.approved_cscver"

    const-string v17, "none"

    invoke-static/range {v16 .. v17}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    if-nez v10, :cond_3

    .line 864
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v17, "HandDataNode = null"

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 902
    :cond_1
    const-string v16, "CustomerDataSet.CSCName"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_6

    .line 903
    const-string v16, "CustomerDataSet.CSCName"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 904
    .local v5, "cscName":Ljava/lang/String;
    const-string v16, "CustomerDataSet.CSCVersion"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 905
    .local v7, "cscVer":Ljava/lang/String;
    const-string v16, "CscUpdateService"

    const-string v17, "PATH_CSCNAME =CustomerDataSet.CSCName"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    const-string v16, "ril.official_cscver"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    .end local v5    # "cscName":Ljava/lang/String;
    .end local v7    # "cscVer":Ljava/lang/String;
    :goto_0
    const-string v16, "ro.csc.sales_code"

    invoke-static/range {v16 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    if-nez v16, :cond_2

    .line 913
    const-string v16, "CscUpdateService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "TAG_CUSTOMER ="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "ro.csc.sales_code"

    invoke-static/range {v18 .. v18}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    const-string v16, "ril.official_customer"

    const-string v17, "none"

    invoke-static/range {v16 .. v17}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    :cond_2
    return-void

    .line 866
    :cond_3
    const-string v16, "ApprovalVer"

    move-object/from16 v0, v16

    invoke-virtual {v6, v10, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v11

    .line 867
    .local v11, "handDataNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v11, :cond_1

    .line 868
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    invoke-interface {v11}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v16

    move/from16 v0, v16

    if-ge v13, v0, :cond_1

    .line 869
    invoke-interface {v11, v13}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    .line 870
    .local v12, "handDataNodeListChild":Lorg/w3c/dom/Node;
    const-string v16, "Customer"

    move-object/from16 v0, v16

    invoke-virtual {v6, v12, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 871
    .local v8, "customer":Lorg/w3c/dom/Node;
    invoke-virtual {v6, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    .line 873
    .local v9, "customerName":Ljava/lang/String;
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "customer = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 875
    const-string v16, "ro.csc.sales_code"

    invoke-static/range {v16 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 877
    const-string v16, "CodeFullVersion"

    move-object/from16 v0, v16

    invoke-virtual {v6, v12, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 879
    .local v1, "codeApproved":Lorg/w3c/dom/Node;
    invoke-virtual {v6, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    .line 880
    .local v2, "codeApprovedVer":Ljava/lang/String;
    const-string v16, "ril.approved_codever"

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    invoke-static/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->isWifiOnly(Landroid/content/Context;)Z

    move-result v16

    if-nez v16, :cond_4

    .line 886
    const-string v16, "ModemFullVersion"

    move-object/from16 v0, v16

    invoke-virtual {v6, v12, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v14

    .line 888
    .local v14, "modemApproved":Lorg/w3c/dom/Node;
    invoke-virtual {v6, v14}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v15

    .line 889
    .local v15, "modemApprovedVer":Ljava/lang/String;
    const-string v16, "ril.approved_modemver"

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    .end local v14    # "modemApproved":Lorg/w3c/dom/Node;
    .end local v15    # "modemApprovedVer":Ljava/lang/String;
    :cond_4
    const-string v16, "CSCFullVersion"

    move-object/from16 v0, v16

    invoke-virtual {v6, v12, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 894
    .local v3, "cscApproved":Lorg/w3c/dom/Node;
    invoke-virtual {v6, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 895
    .local v4, "cscApprovedVer":Ljava/lang/String;
    const-string v16, "ril.approved_cscver"

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    .end local v1    # "codeApproved":Lorg/w3c/dom/Node;
    .end local v2    # "codeApprovedVer":Ljava/lang/String;
    .end local v3    # "cscApproved":Lorg/w3c/dom/Node;
    .end local v4    # "cscApprovedVer":Ljava/lang/String;
    :cond_5
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 908
    .end local v8    # "customer":Lorg/w3c/dom/Node;
    .end local v9    # "customerName":Ljava/lang/String;
    .end local v11    # "handDataNodeList":Lorg/w3c/dom/NodeList;
    .end local v12    # "handDataNodeListChild":Lorg/w3c/dom/Node;
    .end local v13    # "i":I
    :cond_6
    const-string v16, "CscUpdateService"

    const-string v17, "PATH_CSCNAME =CustomerDataSet.CSCName"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 909
    const-string v16, "ril.official_cscver"

    const-string v17, "none"

    invoke-static/range {v16 .. v17}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public SendModemSettingIntent()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 781
    const-string v4, "csc.preferences_name"

    invoke-virtual {p0, v4, v7}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 782
    .local v3, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 783
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "csc.key.csc_modem_setting_done"

    invoke-interface {v1, v4, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 784
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 786
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.CSC_MODEM_SETTING"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 788
    .local v2, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Common_AutoConfigurationType"

    const-string v6, "DISABLE"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    aget-object v0, v4, v7

    .line 789
    .local v0, "cscCode":Ljava/lang/String;
    const-string v4, "DISABLE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 790
    const-string v4, "CscUpdateService"

    const-string v5, "SendModemSettingIntent : sendBroadcast"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    const v4, 0x10000020

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 793
    const-string v4, "MODE"

    const-string v5, "SET_MODEM_INFO"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4, v8}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 801
    :goto_0
    invoke-static {p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->powerSavingOn(Landroid/content/Context;)V

    .line 805
    const-string v4, "android.intent.action.CSC_STOPPED"

    invoke-direct {p0, v4}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->sendIntentForLockScreen(Ljava/lang/String;)V

    .line 806
    return-void

    .line 795
    :cond_0
    const-string v4, "CscUpdateService"

    const-string v5, "SendModemSettingIntent : sendOrderedBroadcast"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    const/16 v4, 0x20

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 797
    const-string v4, "MODE"

    const-string v5, "SET_MODEM_INFO"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4, v8}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public checkAndSendModemSettingIntent()V
    .locals 6

    .prologue
    .line 757
    const-string v2, "CscUpdateService"

    const-string v3, "checkAndSendModemSettingIntent"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    const-string v2, "ro.product.name"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 760
    .local v0, "prodName":Ljava/lang/String;
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 761
    .local v1, "salesCode":Ljava/lang/String;
    const-string v2, "t03gxxx"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "baffinxxx"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const-string v2, "PAP"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 762
    const/16 v2, 0x2710

    sput v2, Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkRetry:I

    .line 767
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->chekUpdateDoneCondition()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 768
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->SendModemSettingIntent()V

    .line 772
    :goto_1
    return-void

    .line 764
    :cond_1
    const/16 v2, 0x12c

    sput v2, Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkRetry:I

    goto :goto_0

    .line 770
    :cond_2
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->mHandlerPhone:Landroid/os/Handler;

    const/16 v3, 0x7c6

    sget v4, Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkDelay:I

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1
.end method

.method public chekUpdateDoneCondition()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x3

    .line 714
    const-string v4, "CscUpdateService"

    const-string v5, "checkUpdateDoneCondition"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    const-string v4, ""

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    .line 718
    const-string v4, "USER_PREFERENCES"

    invoke-virtual {p0, v4, v3}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 720
    .local v1, "preferences":Landroid/content/SharedPreferences;
    const-string v4, "PREF_RINGTONE_SET"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v6, :cond_0

    .line 721
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "PREF_RINGTONE_SET/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    .line 724
    :cond_0
    const-string v4, "PREF_NOTIFICATION_SET"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v6, :cond_1

    .line 725
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "PREF_NOTIFICATION_SET/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    .line 728
    :cond_1
    const-string v4, "PREF_ALARMTONE_SET"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v6, :cond_2

    .line 729
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "PREF_ALARMTONE_SET/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    .line 732
    :cond_2
    sget-boolean v4, Lcom/samsung/sec/android/application/csc/CscReceiver;->APPinstallingDone:Z

    if-nez v4, :cond_3

    const-string v4, "com.sec.android.preloadinstaller"

    invoke-static {p0, v4}, Lcom/samsung/sec/android/application/csc/CscUtil;->isPackageLoaded(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-ne v4, v2, :cond_3

    .line 735
    const-string v4, "persist.sys.storage_preload"

    const-string v5, "false"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 737
    .local v0, "StoragePreload":Ljava/lang/String;
    const-string v4, "2"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 738
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "APPinstallingDone/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    .line 743
    .end local v0    # "StoragePreload":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 747
    :goto_0
    return v2

    .line 746
    :cond_4
    const-string v2, "CscUpdateService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "check update done condition is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 747
    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 151
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 23
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 400
    const-string v20, "CscUpdateService"

    const-string v21, "onStart"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    const-string v20, "ro.csc.sales_code"

    invoke-static/range {v20 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 404
    .local v17, "salesCode":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->isPreconfiged()Z

    move-result v20

    if-nez v20, :cond_3

    .line 405
    if-eqz p1, :cond_0

    const-string v20, "MODE"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 406
    const-string v20, "MODE"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 407
    .local v12, "mode":Ljava/lang/String;
    const-string v20, "CSCUPDATECHECK"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 408
    const-string v20, "CscUpdateService"

    const-string v21, "only init test"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    const/4 v10, 0x0

    .line 411
    .local v10, "isApo":Z
    const-string v20, "ATTYPE"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 412
    const-string v20, "ATTYPE"

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 416
    :goto_0
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v10, v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscInitTest(ZZ)V

    .line 420
    .end local v10    # "isApo":Z
    .end local v12    # "mode":Ljava/lang/String;
    :cond_0
    const-string v20, "CscUpdateService"

    const-string v21, "CSC is not preconfiged yet. Do nothing."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    :cond_1
    :goto_1
    return-void

    .line 414
    .restart local v10    # "isApo":Z
    .restart local v12    # "mode":Ljava/lang/String;
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->isModelAPO()Z

    move-result v10

    goto :goto_0

    .line 425
    .end local v10    # "isApo":Z
    .end local v12    # "mode":Ljava/lang/String;
    :cond_3
    if-eqz p1, :cond_11

    const-string v20, "MODE"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_11

    .line 426
    const-string v20, "MODE"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 428
    .restart local v12    # "mode":Ljava/lang/String;
    if-eqz v12, :cond_11

    .line 429
    const-string v20, "CSCCONNECTION"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 430
    const-string v20, "CscUpdateService"

    const-string v21, "only CscConnection update"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    const-string v20, "simSlot"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 432
    const-string v20, "simSlot"

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscConnectionUpdate(I)V

    goto :goto_1

    .line 434
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscConnectionUpdate()V

    goto :goto_1

    .line 435
    :cond_5
    const-string v20, "CSCRINGTONE"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 436
    const-string v20, "CscUpdateService"

    const-string v21, "only ringtone update"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscRingtoneUpdate()V

    goto :goto_1

    .line 438
    :cond_6
    const-string v20, "CSCNOTIFITICATION"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 439
    const-string v20, "CscUpdateService"

    const-string v21, "only notification update"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscNotificationUpdate()V

    goto :goto_1

    .line 441
    :cond_7
    const-string v20, "CSCALARMTONE"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 442
    const-string v20, "CscUpdateService"

    const-string v21, "only alarmtone update"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscAlarmtoneUpdate()V

    goto/16 :goto_1

    .line 444
    :cond_8
    const-string v20, "CSCPREFERREDPACKAGE"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 445
    const-string v20, "CscUpdateService"

    const-string v21, "only preferred package update"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscPreferredPackageUpdate()V

    goto/16 :goto_1

    .line 447
    :cond_9
    const-string v20, "CSCCHAMELEON"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 448
    const-string v20, "REBOOT"

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v15

    .line 449
    .local v15, "reboot":Z
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUtil;->isChameleonModel()Z

    move-result v20

    if-eqz v20, :cond_1

    const-string v20, "SPR"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_a

    const-string v20, "BST"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_a

    const-string v20, "XAS"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_a

    const-string v20, "VMU"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 452
    :cond_a
    const-string v20, "CscUpdateService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "chameleon update | REBOOT: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscChameleonUpdate(Z)V

    goto/16 :goto_1

    .line 455
    .end local v15    # "reboot":Z
    :cond_b
    const-string v20, "CSCUPDATECHECK"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 456
    const-string v20, "CscUpdateService"

    const-string v21, "only init test"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    const/4 v10, 0x0

    .line 459
    .restart local v10    # "isApo":Z
    const-string v20, "ATTYPE"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 460
    const-string v20, "ATTYPE"

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 463
    :goto_2
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v10, v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscInitTest(ZZ)V

    goto/16 :goto_1

    .line 462
    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->isModelAPO()Z

    move-result v10

    goto :goto_2

    .line 464
    .end local v10    # "isApo":Z
    :cond_d
    const-string v20, "RESETVERIFY"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 465
    const-string v20, "CscUpdateService"

    const-string v21, "only reset verify"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    const/4 v10, 0x0

    .line 468
    .restart local v10    # "isApo":Z
    const-string v20, "ATTYPE"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 469
    const-string v20, "ATTYPE"

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 473
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscResetVerify(Z)V

    goto/16 :goto_1

    .line 471
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->isModelAPO()Z

    move-result v10

    goto :goto_3

    .line 474
    .end local v10    # "isApo":Z
    :cond_f
    const-string v20, "CTBCPS"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_10

    .line 475
    const-string v20, "CscUpdateService"

    const-string v21, "only att reset verify"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscAttResetVerify()V

    goto/16 :goto_1

    .line 478
    :cond_10
    const-string v20, "CscUpdateService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Unknown mode is request : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 488
    .end local v12    # "mode":Ljava/lang/String;
    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->set_CSC_version()V

    .line 491
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUtil;->triggeredByChameleon()Z

    move-result v20

    if-eqz v20, :cond_13

    .line 493
    const-string v20, "CscUpdateService"

    const-string v21, "This is chameleon boot : CSC updated"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    invoke-static/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->powerSavingOff(Landroid/content/Context;)V

    .line 495
    const-string v20, "android.intent.action.CSC_STARTED"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->sendIntentForLockScreen(Ljava/lang/String;)V

    .line 497
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->doUpdate()V

    .line 498
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUtil;->deleteCheckChameleon()V

    .line 499
    invoke-static/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->setCscVersionAndEdition(Landroid/content/Context;)V

    .line 597
    :cond_12
    :goto_4
    invoke-static/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->setNewBuild(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 502
    :cond_13
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUtil;->isCscUpdated()Z

    move-result v20

    if-nez v20, :cond_14

    const-string v20, "csc.preferences_name"

    const-string v21, "csc.key.already_executed"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscUtil;->getCscPreferenceValue(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_1f

    .line 505
    :cond_14
    const-string v20, "CscUpdateService"

    const-string v21, "This is normal boot : CSC not updated"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->CscGpsUpdate()V

    .line 508
    const-string v20, "csc.preferences_name"

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 509
    .local v13, "pref":Landroid/content/SharedPreferences;
    const-string v20, "csc.key.csc_modem_setting_done"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 510
    .local v11, "is_modem_setting_done":Z
    if-nez v11, :cond_17

    .line 512
    const/16 v18, 0x0

    .line 513
    .local v18, "skipModemSetting":Z
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v19

    .line 514
    .local v19, "userId":I
    if-eqz v19, :cond_15

    .line 515
    const-string v20, "CscUpdateService"

    const-string v21, "disable csc modem setting cuz this user isn\'t owner."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    const/16 v18, 0x1

    .line 519
    :cond_15
    invoke-static/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->isWifiOnly(Landroid/content/Context;)Z

    move-result v20

    if-nez v20, :cond_16

    if-eqz v18, :cond_1b

    .line 520
    :cond_16
    const-string v20, "CscUpdateService"

    const-string v21, "disable csc modem setting."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    const-string v20, "csc.preferences_name"

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 523
    .local v14, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 524
    .local v5, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v20, "csc.key.csc_modem_setting_done"

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 525
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 541
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v14    # "preferences":Landroid/content/SharedPreferences;
    .end local v18    # "skipModemSetting":Z
    .end local v19    # "userId":I
    :cond_17
    :goto_5
    invoke-static/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->isNewCscVersion(Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_1e

    .line 542
    const-string v20, "CscUpdateService"

    const-string v21, "CSC Version changed"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    const/4 v3, 0x0

    .line 544
    .local v3, "binary_type":I
    const/4 v8, 0x0

    .line 545
    .local v8, "in":Ljava/io/BufferedReader;
    new-instance v7, Ljava/io/File;

    const-string v20, "/efs/log/boot_cause"

    move-object/from16 v0, v20

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 546
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_1a

    .line 548
    :try_start_0
    new-instance v9, Ljava/io/BufferedReader;

    new-instance v20, Ljava/io/FileReader;

    const-string v21, "/efs/log/boot_cause"

    invoke-direct/range {v20 .. v21}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-direct {v9, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    .end local v8    # "in":Ljava/io/BufferedReader;
    .local v9, "in":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v16

    .line 551
    .local v16, "s":Ljava/lang/String;
    const-string v20, "home"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1c

    .line 552
    const/4 v3, 0x1

    .line 553
    const-string v20, "CscUpdateService"

    const-string v21, "HOME binary."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 562
    :cond_18
    :goto_6
    if-eqz v9, :cond_19

    .line 563
    :try_start_2
    invoke-virtual {v9}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_19
    move-object v8, v9

    .line 568
    .end local v9    # "in":Ljava/io/BufferedReader;
    .end local v16    # "s":Ljava/lang/String;
    .restart local v8    # "in":Ljava/io/BufferedReader;
    :cond_1a
    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->doUpdateForHomeFOTA(I)V

    .line 569
    const-string v20, "CscUpdateService"

    const-string v21, "HOME or FOTA UPDATE."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    .end local v3    # "binary_type":I
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "in":Ljava/io/BufferedReader;
    :goto_8
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUtil;->isCscUpdated()Z

    move-result v20

    if-nez v20, :cond_12

    .line 575
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUtil;->setCscUpdated()V

    goto/16 :goto_4

    .line 527
    .restart local v18    # "skipModemSetting":Z
    .restart local v19    # "userId":I
    :cond_1b
    const-string v20, "CscUpdateService"

    const-string v21, "retry csc modem setting."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkAndSendModemSettingIntent()V

    goto :goto_5

    .line 554
    .end local v18    # "skipModemSetting":Z
    .end local v19    # "userId":I
    .restart local v3    # "binary_type":I
    .restart local v7    # "file":Ljava/io/File;
    .restart local v9    # "in":Ljava/io/BufferedReader;
    .restart local v16    # "s":Ljava/lang/String;
    :cond_1c
    :try_start_3
    const-string v20, "fota"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_18

    .line 555
    const/4 v3, 0x2

    .line 556
    const-string v20, "CscUpdateService"

    const-string v21, "FOTA binary."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_6

    .line 558
    .end local v16    # "s":Ljava/lang/String;
    :catch_0
    move-exception v6

    move-object v8, v9

    .line 559
    .end local v9    # "in":Ljava/io/BufferedReader;
    .local v6, "ex":Ljava/lang/Exception;
    .restart local v8    # "in":Ljava/io/BufferedReader;
    :goto_9
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 562
    if-eqz v8, :cond_1a

    .line 563
    :try_start_5
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_7

    .line 564
    :catch_1
    move-exception v20

    goto :goto_7

    .end local v6    # "ex":Ljava/lang/Exception;
    .end local v8    # "in":Ljava/io/BufferedReader;
    .restart local v9    # "in":Ljava/io/BufferedReader;
    .restart local v16    # "s":Ljava/lang/String;
    :catch_2
    move-exception v20

    move-object v8, v9

    .line 566
    .end local v9    # "in":Ljava/io/BufferedReader;
    .restart local v8    # "in":Ljava/io/BufferedReader;
    goto :goto_7

    .line 561
    .end local v16    # "s":Ljava/lang/String;
    :catchall_0
    move-exception v20

    .line 562
    :goto_a
    if-eqz v8, :cond_1d

    .line 563
    :try_start_6
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 565
    :cond_1d
    :goto_b
    throw v20

    .line 571
    .end local v3    # "binary_type":I
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "in":Ljava/io/BufferedReader;
    :cond_1e
    const-string v20, "CscUpdateService"

    const-string v21, "same CSC Version"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 580
    .end local v11    # "is_modem_setting_done":Z
    .end local v13    # "pref":Landroid/content/SharedPreferences;
    :cond_1f
    invoke-static/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->powerSavingOff(Landroid/content/Context;)V

    .line 581
    const-string v20, "android.intent.action.CSC_STARTED"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->sendIntentForLockScreen(Ljava/lang/String;)V

    .line 582
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v4

    .line 584
    .local v4, "currentUserId":I
    if-nez v4, :cond_20

    .line 586
    const-string v20, "CscUpdateService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "This is first boot for Owner : CSC updated for user ("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->doUpdate()V

    .line 594
    :goto_c
    invoke-static/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUtil;->setCscVersionAndEdition(Landroid/content/Context;)V

    goto/16 :goto_4

    .line 590
    :cond_20
    const-string v20, "CscUpdateService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "This is first boot for Sub User : CSC updated for user ("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->doUpdateForSubUser()V

    goto :goto_c

    .line 564
    .end local v4    # "currentUserId":I
    .restart local v3    # "binary_type":I
    .restart local v7    # "file":Ljava/io/File;
    .restart local v8    # "in":Ljava/io/BufferedReader;
    .restart local v11    # "is_modem_setting_done":Z
    .restart local v13    # "pref":Landroid/content/SharedPreferences;
    :catch_3
    move-exception v21

    goto :goto_b

    .line 561
    .end local v8    # "in":Ljava/io/BufferedReader;
    .restart local v9    # "in":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v20

    move-object v8, v9

    .end local v9    # "in":Ljava/io/BufferedReader;
    .restart local v8    # "in":Ljava/io/BufferedReader;
    goto :goto_a

    .line 558
    :catch_4
    move-exception v6

    goto/16 :goto_9
.end method

.method public verifyResetDone()V
    .locals 3

    .prologue
    .line 707
    const-string v0, "NOERROR"

    .line 708
    .local v0, "ANSWER":Ljava/lang/String;
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscContents;

    invoke-direct {v1, p0}, Lcom/samsung/sec/android/application/csc/CscContents;-><init>(Landroid/content/Context;)V

    .line 709
    .local v1, "cscContents":Lcom/samsung/sec/android/application/csc/CscContents;
    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscContents;->verifyResetDone()Ljava/lang/String;

    move-result-object v0

    .line 710
    const-string v2, "ril.resetVerify"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    return-void
.end method
