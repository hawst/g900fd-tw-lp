.class public Lcom/samsung/sec/android/application/csc/CscCloud;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscCloud.java"


# static fields
.field static final KEY_CSC:[Ljava/lang/String;

.field static final KEY_PRFS:[Ljava/lang/String;

.field static final USER_KEY_PRFS:[Ljava/lang/String;


# instance fields
.field private final CLOUD_SETTINGS_PREF:Ljava/lang/String;

.field private final CSC_CLOUD_INTENT:Ljava/lang/String;

.field private final CSC_FILE:Ljava/lang/String;

.field private ct:Landroid/content/Context;

.field private sPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Settings.Main.Cloud.WifiOnly"

    aput-object v1, v0, v3

    const-string v1, "Settings.Main.Cloud.PictureSync"

    aput-object v1, v0, v4

    const-string v1, "Settings.Main.Cloud.VideoSync"

    aput-object v1, v0, v5

    const-string v1, "Settings.Main.Cloud.MusicSync"

    aput-object v1, v0, v6

    const-string v1, "Settings.Main.Cloud.DocSync"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Settings.Main.Cloud.DocUseMobileData"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Settings.Main.Cloud.EnableSyncRoaming"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    .line 34
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "csc_pref_key_wifi_only"

    aput-object v1, v0, v3

    const-string v1, "csc_pref_key_picture_sync"

    aput-object v1, v0, v4

    const-string v1, "csc_pref_key_video_sync"

    aput-object v1, v0, v5

    const-string v1, "csc_pref_key_music_sync"

    aput-object v1, v0, v6

    const-string v1, "csc_pref_key_doc_sync"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "csc_pref_key_doc_use_mobile_data"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "csc_pref_key_enable_sync_roaming"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_PRFS:[Ljava/lang/String;

    .line 41
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "photo"

    aput-object v1, v0, v3

    const-string v1, "video"

    aput-object v1, v0, v4

    const-string v1, "music"

    aput-object v1, v0, v5

    const-string v1, "doc"

    aput-object v1, v0, v6

    const-string v1, "photoSync"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "videoSync"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "musicSync"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "docSync"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "useMobileDataForDocSync"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "enableSyncRoaming"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscCloud;->USER_KEY_PRFS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 19
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->CSC_FILE:Ljava/lang/String;

    .line 21
    const-string v0, "com.sec.android.cloudagent.settings.ACTION_REQUEST_CSC_UPDATE"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->CSC_CLOUD_INTENT:Ljava/lang/String;

    .line 23
    const-string v0, "CloudSettings"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->CLOUD_SETTINGS_PREF:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;

    .line 48
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->ct:Landroid/content/Context;

    .line 49
    return-void
.end method


# virtual methods
.method public compare()Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x5

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 134
    const/4 v4, 0x0

    .line 135
    .local v4, "valueStr":Ljava/lang/String;
    const/4 v0, 0x0

    .line 136
    .local v0, "context":Landroid/content/Context;
    const/4 v2, 0x0

    .line 137
    .local v2, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const/4 v5, 0x0

    .line 138
    .local v5, "xml_boolean":Z
    const/4 v3, 0x0

    .line 141
    .local v3, "user_key_boolean":Z
    :try_start_0
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->ct:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "com.sec.android.cloudagent"

    const/4 v8, 0x2

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 143
    const-string v6, "CloudSettings"

    const/4 v7, 0x5

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    .end local v2    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 155
    .restart local v2    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v10

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 156
    if-eqz v4, :cond_4

    .line 157
    invoke-virtual {p0, v4}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 158
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->USER_KEY_PRFS:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 160
    if-eq v5, v3, :cond_0

    .line 161
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v10

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscCloud : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 164
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 299
    :goto_0
    return-object v6

    .line 145
    :catch_0
    move-exception v1

    .line 146
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "NOERROR"

    goto :goto_0

    .line 167
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->USER_KEY_PRFS:[Ljava/lang/String;

    aget-object v7, v7, v9

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 169
    if-eq v5, v3, :cond_1

    .line 170
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v10

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscCloud : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 173
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 176
    :cond_1
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->USER_KEY_PRFS:[Ljava/lang/String;

    aget-object v7, v7, v11

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 178
    if-eq v5, v3, :cond_2

    .line 179
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v10

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscCloud : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 182
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 185
    :cond_2
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->USER_KEY_PRFS:[Ljava/lang/String;

    aget-object v7, v7, v13

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 187
    if-eq v5, v3, :cond_3

    .line 188
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v10

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscCloud : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 191
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 194
    :cond_3
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v10

    invoke-static {v6, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :cond_4
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v9

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 200
    if-eqz v4, :cond_6

    .line 201
    invoke-virtual {p0, v4}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 202
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->USER_KEY_PRFS:[Ljava/lang/String;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 204
    if-eq v5, v3, :cond_5

    .line 205
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v9

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscCloud : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 208
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 211
    :cond_5
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v9

    invoke-static {v6, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :cond_6
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v11

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 217
    if-eqz v4, :cond_8

    .line 218
    invoke-virtual {p0, v4}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 219
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->USER_KEY_PRFS:[Ljava/lang/String;

    aget-object v7, v7, v12

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 221
    if-eq v5, v3, :cond_7

    .line 222
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v11

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscCloud : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 225
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 228
    :cond_7
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v11

    invoke-static {v6, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_8
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v13

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 234
    if-eqz v4, :cond_a

    .line 235
    invoke-virtual {p0, v4}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 236
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->USER_KEY_PRFS:[Ljava/lang/String;

    const/4 v8, 0x6

    aget-object v7, v7, v8

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 238
    if-eq v5, v3, :cond_9

    .line 239
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v13

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscCloud : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v13

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 242
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v13

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 245
    :cond_9
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v13

    invoke-static {v6, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_a
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v7, 0x4

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 251
    if-eqz v4, :cond_c

    .line 252
    invoke-virtual {p0, v4}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 253
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->USER_KEY_PRFS:[Ljava/lang/String;

    const/4 v8, 0x7

    aget-object v7, v7, v8

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 255
    if-eq v5, v3, :cond_b

    .line 256
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v7, 0x4

    aget-object v6, v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscCloud : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 259
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 262
    :cond_b
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v7, 0x4

    aget-object v6, v6, v7

    invoke-static {v6, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    :cond_c
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v12

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 268
    if-eqz v4, :cond_e

    .line 269
    invoke-virtual {p0, v4}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 270
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x8

    aget-object v7, v7, v8

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 272
    if-eq v5, v3, :cond_d

    .line 273
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v12

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscCloud : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v12

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 276
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v7, v7, v12

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 279
    :cond_d
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v12

    invoke-static {v6, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :cond_e
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v7, 0x6

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 285
    if-eqz v4, :cond_10

    .line 286
    invoke-virtual {p0, v4}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 287
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->sPrefs:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x9

    aget-object v7, v7, v8

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 289
    if-eq v5, v3, :cond_f

    .line 290
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v7, 0x6

    aget-object v6, v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscCloud : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v8, 0x6

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 293
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v8, 0x6

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 296
    :cond_f
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v7, 0x6

    aget-object v6, v6, v7

    invoke-static {v6, v4}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_10
    const-string v6, "NOERROR"

    goto/16 :goto_0
.end method

.method public cutomerUpdate()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 57
    const/4 v2, 0x0

    .line 60
    .local v2, "valueStr":Ljava/lang/String;
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 62
    .local v1, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.sec.android.cloudagent.settings.ACTION_REQUEST_CSC_UPDATE"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 64
    .local v0, "intent":Landroid/content/Intent;
    const/4 v3, 0x1

    .line 68
    .local v3, "xml":Z
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v4, v4, v5

    invoke-virtual {v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 70
    if-eqz v2, :cond_0

    .line 71
    invoke-virtual {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v3

    .line 72
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_PRFS:[Ljava/lang/String;

    aget-object v4, v4, v5

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 77
    :cond_0
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v4, v4, v6

    invoke-virtual {v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 79
    if-eqz v2, :cond_1

    .line 80
    invoke-virtual {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v3

    .line 81
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_PRFS:[Ljava/lang/String;

    aget-object v4, v4, v6

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 86
    :cond_1
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v4, v4, v7

    invoke-virtual {v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 88
    if-eqz v2, :cond_2

    .line 89
    invoke-virtual {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v3

    .line 90
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_PRFS:[Ljava/lang/String;

    aget-object v4, v4, v7

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 95
    :cond_2
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v4, v4, v8

    invoke-virtual {v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 97
    if-eqz v2, :cond_3

    .line 98
    invoke-virtual {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v3

    .line 99
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_PRFS:[Ljava/lang/String;

    aget-object v4, v4, v8

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 104
    :cond_3
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    aget-object v4, v4, v9

    invoke-virtual {v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 106
    if-eqz v2, :cond_4

    .line 107
    invoke-virtual {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v3

    .line 108
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_PRFS:[Ljava/lang/String;

    aget-object v4, v4, v9

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 113
    :cond_4
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x5

    aget-object v4, v4, v5

    invoke-virtual {v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 115
    if-eqz v2, :cond_5

    .line 116
    invoke-virtual {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v3

    .line 117
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x5

    aget-object v4, v4, v5

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 122
    :cond_5
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v4, v4, v5

    invoke-virtual {v1, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 124
    if-eqz v2, :cond_6

    .line 125
    invoke-virtual {p0, v2}, Lcom/samsung/sec/android/application/csc/CscCloud;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v3

    .line 126
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscCloud;->KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v4, v4, v5

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 129
    :cond_6
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscCloud;->ct:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 130
    return-void
.end method

.method getBooleanFromString(Ljava/lang/String;)Z
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 304
    const-string v2, "on"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 313
    :cond_0
    :goto_0
    return v0

    .line 306
    :cond_1
    const-string v2, "off"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 307
    goto :goto_0

    .line 308
    :cond_2
    const-string v2, "true"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 310
    const-string v0, "false"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 311
    goto :goto_0

    :cond_3
    move v0, v1

    .line 313
    goto :goto_0
.end method

.method public update()V
    .locals 0

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscCloud;->cutomerUpdate()V

    .line 54
    return-void
.end method
