.class public Lcom/samsung/sec/android/application/csc/CscVoiceMail;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscVoiceMail.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;
    }
.end annotation


# instance fields
.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final NOERROR:Ljava/lang/String;

.field private final OTHERS_CSC_FILE:Ljava/lang/String;

.field protected ct:Landroid/content/Context;

.field protected mDefaultNumber:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field protected mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

.field protected mResolver:Landroid/content/ContentResolver;

.field protected sPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->sPrefs:Landroid/content/SharedPreferences;

    .line 54
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 56
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->OTHERS_CSC_FILE:Ljava/lang/String;

    .line 92
    const-string v0, "NOERROR"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->NOERROR:Ljava/lang/String;

    .line 96
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mDefaultNumber:Ljava/lang/String;

    .line 507
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscVoiceMail$1;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscVoiceMail$1;-><init>(Lcom/samsung/sec/android/application/csc/CscVoiceMail;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mHandler:Landroid/os/Handler;

    .line 113
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mResolver:Landroid/content/ContentResolver;

    .line 115
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->ct:Landroid/content/Context;

    .line 116
    return-void
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 503
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Main.Network.AutoVoicemail"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 504
    return-void
.end method

.method public checkMarkupLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "inputString"    # Ljava/lang/String;

    .prologue
    .line 533
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 534
    :cond_0
    const-string v0, ""

    .line 542
    :goto_0
    return-object v0

    .line 536
    :cond_1
    move-object v0, p1

    .line 537
    .local v0, "output":Ljava/lang/String;
    const-string v1, "&"

    const-string v2, "&amp;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 538
    const-string v1, "<"

    const-string v2, "&lt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 539
    const-string v1, ">"

    const-string v2, "&gt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 540
    const-string v1, "\'"

    const-string v2, "&apos;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 541
    const-string v1, "\""

    const-string v2, "&quot;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 542
    goto :goto_0
.end method

.method checkSameFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 22
    .param p1, "fileName1"    # Ljava/lang/String;
    .param p2, "fileName2"    # Ljava/lang/String;

    .prologue
    .line 325
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 326
    .local v11, "file1":Ljava/io/File;
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 327
    .local v12, "file2":Ljava/io/File;
    const/4 v13, 0x0

    .line 328
    .local v13, "fileSize":I
    const/4 v14, 0x0

    .line 329
    .local v14, "readSize1":I
    const/4 v15, 0x0

    .line 330
    .local v15, "readSize2":I
    const/4 v2, 0x0

    .line 331
    .local v2, "bf1":Ljava/nio/ByteBuffer;
    const/4 v3, 0x0

    .line 332
    .local v3, "bf2":Ljava/nio/ByteBuffer;
    const/16 v16, 0x1

    .line 333
    .local v16, "result":Z
    const/4 v6, 0x0

    .line 334
    .local v6, "dis1":Ljava/io/DataInputStream;
    const/4 v8, 0x0

    .line 336
    .local v8, "dis2":Ljava/io/DataInputStream;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_0

    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_1

    .line 337
    :cond_0
    const/16 v17, 0x0

    .line 395
    :goto_0
    return v17

    .line 339
    :cond_1
    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v18

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v20

    cmp-long v17, v18, v20

    if-eqz v17, :cond_2

    .line 340
    const/16 v17, 0x0

    goto :goto_0

    .line 342
    :cond_2
    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v13, v0

    .line 344
    const-string v17, "CscVoiceMail"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "fileSize : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    :try_start_0
    new-instance v7, Ljava/io/DataInputStream;

    new-instance v17, Ljava/io/FileInputStream;

    move-object/from16 v0, v17

    invoke-direct {v0, v11}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    .end local v6    # "dis1":Ljava/io/DataInputStream;
    .local v7, "dis1":Ljava/io/DataInputStream;
    :try_start_1
    new-instance v9, Ljava/io/DataInputStream;

    new-instance v17, Ljava/io/FileInputStream;

    move-object/from16 v0, v17

    invoke-direct {v0, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v17

    invoke-direct {v9, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 352
    .end local v8    # "dis2":Ljava/io/DataInputStream;
    .local v9, "dis2":Ljava/io/DataInputStream;
    :try_start_2
    new-array v4, v13, [B

    .line 353
    .local v4, "buff1":[B
    new-array v5, v13, [B

    .line 355
    .local v5, "buff2":[B
    invoke-virtual {v7, v4}, Ljava/io/DataInputStream;->read([B)I

    move-result v14

    .line 356
    invoke-virtual {v9, v5}, Ljava/io/DataInputStream;->read([B)I

    move-result v15

    .line 358
    const-string v17, "CscVoiceMail"

    const-string v18, "read OK"

    invoke-static/range {v17 .. v18}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    if-eq v14, v15, :cond_7

    .line 361
    if-eqz v7, :cond_3

    .line 362
    invoke-virtual {v7}, Ljava/io/DataInputStream;->close()V

    .line 363
    :cond_3
    if-eqz v9, :cond_4

    .line 364
    invoke-virtual {v9}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 365
    :cond_4
    const/16 v17, 0x0

    .line 378
    if-eqz v7, :cond_5

    .line 379
    :try_start_3
    invoke-virtual {v7}, Ljava/io/DataInputStream;->close()V

    .line 380
    :cond_5
    if-eqz v9, :cond_6

    .line 381
    invoke-virtual {v9}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_6
    :goto_1
    move-object v8, v9

    .end local v9    # "dis2":Ljava/io/DataInputStream;
    .restart local v8    # "dis2":Ljava/io/DataInputStream;
    move-object v6, v7

    .line 384
    .end local v7    # "dis1":Ljava/io/DataInputStream;
    .restart local v6    # "dis1":Ljava/io/DataInputStream;
    goto :goto_0

    .line 382
    .end local v6    # "dis1":Ljava/io/DataInputStream;
    .end local v8    # "dis2":Ljava/io/DataInputStream;
    .restart local v7    # "dis1":Ljava/io/DataInputStream;
    .restart local v9    # "dis2":Ljava/io/DataInputStream;
    :catch_0
    move-exception v10

    .line 383
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 368
    .end local v10    # "e":Ljava/io/IOException;
    :cond_7
    :try_start_4
    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 369
    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 371
    const-string v17, "CscVoiceMail"

    const-string v18, "buff  OK"

    invoke-static/range {v17 .. v18}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 378
    if-eqz v7, :cond_8

    .line 379
    :try_start_5
    invoke-virtual {v7}, Ljava/io/DataInputStream;->close()V

    .line 380
    :cond_8
    if-eqz v9, :cond_9

    .line 381
    invoke-virtual {v9}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_9
    move-object v8, v9

    .end local v9    # "dis2":Ljava/io/DataInputStream;
    .restart local v8    # "dis2":Ljava/io/DataInputStream;
    move-object v6, v7

    .line 387
    .end local v4    # "buff1":[B
    .end local v5    # "buff2":[B
    .end local v7    # "dis1":Ljava/io/DataInputStream;
    .restart local v6    # "dis1":Ljava/io/DataInputStream;
    :cond_a
    :goto_2
    if-eqz v2, :cond_b

    if-nez v3, :cond_f

    .line 388
    :cond_b
    const/16 v16, 0x0

    :goto_3
    move/from16 v17, v16

    .line 395
    goto/16 :goto_0

    .line 382
    .end local v6    # "dis1":Ljava/io/DataInputStream;
    .end local v8    # "dis2":Ljava/io/DataInputStream;
    .restart local v4    # "buff1":[B
    .restart local v5    # "buff2":[B
    .restart local v7    # "dis1":Ljava/io/DataInputStream;
    .restart local v9    # "dis2":Ljava/io/DataInputStream;
    :catch_1
    move-exception v10

    .line 383
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    move-object v8, v9

    .end local v9    # "dis2":Ljava/io/DataInputStream;
    .restart local v8    # "dis2":Ljava/io/DataInputStream;
    move-object v6, v7

    .line 385
    .end local v7    # "dis1":Ljava/io/DataInputStream;
    .restart local v6    # "dis1":Ljava/io/DataInputStream;
    goto :goto_2

    .line 373
    .end local v4    # "buff1":[B
    .end local v5    # "buff2":[B
    .end local v10    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v10

    .line 374
    .local v10, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_6
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 375
    const/16 v16, 0x0

    .line 378
    if-eqz v6, :cond_c

    .line 379
    :try_start_7
    invoke-virtual {v6}, Ljava/io/DataInputStream;->close()V

    .line 380
    :cond_c
    if-eqz v8, :cond_a

    .line 381
    invoke-virtual {v8}, Ljava/io/DataInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_2

    .line 382
    :catch_3
    move-exception v10

    .line 383
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 377
    .end local v10    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v17

    .line 378
    :goto_5
    if-eqz v6, :cond_d

    .line 379
    :try_start_8
    invoke-virtual {v6}, Ljava/io/DataInputStream;->close()V

    .line 380
    :cond_d
    if-eqz v8, :cond_e

    .line 381
    invoke-virtual {v8}, Ljava/io/DataInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 384
    :cond_e
    :goto_6
    throw v17

    .line 382
    :catch_4
    move-exception v10

    .line 383
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 389
    .end local v10    # "e":Ljava/io/IOException;
    :cond_f
    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_10

    .line 390
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 391
    const/16 v16, 0x1

    goto :goto_3

    .line 393
    :cond_10
    const/16 v16, 0x0

    goto :goto_3

    .line 377
    .end local v6    # "dis1":Ljava/io/DataInputStream;
    .restart local v7    # "dis1":Ljava/io/DataInputStream;
    :catchall_1
    move-exception v17

    move-object v6, v7

    .end local v7    # "dis1":Ljava/io/DataInputStream;
    .restart local v6    # "dis1":Ljava/io/DataInputStream;
    goto :goto_5

    .end local v6    # "dis1":Ljava/io/DataInputStream;
    .end local v8    # "dis2":Ljava/io/DataInputStream;
    .restart local v7    # "dis1":Ljava/io/DataInputStream;
    .restart local v9    # "dis2":Ljava/io/DataInputStream;
    :catchall_2
    move-exception v17

    move-object v8, v9

    .end local v9    # "dis2":Ljava/io/DataInputStream;
    .restart local v8    # "dis2":Ljava/io/DataInputStream;
    move-object v6, v7

    .end local v7    # "dis1":Ljava/io/DataInputStream;
    .restart local v6    # "dis1":Ljava/io/DataInputStream;
    goto :goto_5

    .line 373
    .end local v6    # "dis1":Ljava/io/DataInputStream;
    .restart local v7    # "dis1":Ljava/io/DataInputStream;
    :catch_5
    move-exception v10

    move-object v6, v7

    .end local v7    # "dis1":Ljava/io/DataInputStream;
    .restart local v6    # "dis1":Ljava/io/DataInputStream;
    goto :goto_4

    .end local v6    # "dis1":Ljava/io/DataInputStream;
    .end local v8    # "dis2":Ljava/io/DataInputStream;
    .restart local v7    # "dis1":Ljava/io/DataInputStream;
    .restart local v9    # "dis2":Ljava/io/DataInputStream;
    :catch_6
    move-exception v10

    move-object v8, v9

    .end local v9    # "dis2":Ljava/io/DataInputStream;
    .restart local v8    # "dis2":Ljava/io/DataInputStream;
    move-object v6, v7

    .end local v7    # "dis1":Ljava/io/DataInputStream;
    .restart local v6    # "dis1":Ljava/io/DataInputStream;
    goto :goto_4
.end method

.method public compare()Ljava/lang/String;
    .locals 4

    .prologue
    .line 487
    const-string v0, "NOERROR"

    .line 489
    .local v0, "answer":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->loadNetworkInfo()I

    move-result v2

    if-eqz v2, :cond_0

    .line 490
    const-string v2, "CscVoiceMail"

    const-string v3, "no network / voicemail info"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 497
    .end local v0    # "answer":Ljava/lang/String;
    .local v1, "answer":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 495
    .end local v1    # "answer":Ljava/lang/String;
    .restart local v0    # "answer":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->compareVoiceMailNumber()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 497
    .end local v0    # "answer":Ljava/lang/String;
    .restart local v1    # "answer":Ljava/lang/String;
    goto :goto_0
.end method

.method public compareVoiceMailNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 399
    const-string v0, "/data/misc/radio/voicemail-compare.xml"

    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->makeVoiceMailConfXML(Ljava/lang/String;)V

    .line 401
    const-string v0, "/data/misc/radio/voicemail-conf.xml"

    const-string v1, "/data/misc/radio/voicemail-compare.xml"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->checkSameFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    const-string v0, "NOERROR"

    .line 405
    :goto_0
    return-object v0

    .line 404
    :cond_0
    const-string v0, "CscVoiceMail : Settings.Messages.Voicemail.TelNum"

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 405
    const-string v0, "Settings.Messages.Voicemail.TelNum"

    goto :goto_0
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 0
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 530
    return-void
.end method

.method protected loadNetworkInfo()I
    .locals 15

    .prologue
    .line 119
    const/4 v4, 0x0

    .line 120
    .local v4, "networkSize":I
    const/4 v9, 0x0

    .line 126
    .local v9, "voiceMailSize":I
    new-instance v3, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v3, v12}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 129
    .local v3, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const-string v12, "Settings.Messages.Voicemail.TelNum"

    invoke-virtual {v3, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "checkNumExist":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 131
    const/4 v12, -0x1

    .line 231
    :goto_0
    return v12

    .line 136
    :cond_0
    const-string v12, "GeneralInfo"

    invoke-virtual {v3, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 137
    .local v5, "node":Lorg/w3c/dom/Node;
    if-nez v5, :cond_1

    .line 138
    const/4 v12, -0x1

    goto :goto_0

    .line 140
    :cond_1
    const-string v12, "NetworkInfo"

    invoke-virtual {v3, v5, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 141
    .local v6, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v6, :cond_2

    .line 142
    const-string v12, "CscVoiceMail"

    const-string v13, "No network info"

    invoke-static {v12, v13}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const/4 v12, -0x1

    goto :goto_0

    .line 147
    :cond_2
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 148
    new-array v12, v4, [Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    iput-object v12, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    .line 149
    const-string v12, "CscVoiceMail"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Network numbers : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v4, :cond_5

    .line 153
    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    new-instance v13, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    invoke-direct {v13, p0}, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;-><init>(Lcom/samsung/sec/android/application/csc/CscVoiceMail;)V

    aput-object v13, v12, v1

    .line 156
    invoke-interface {v6, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    const-string v13, "NetworkName"

    invoke-virtual {v3, v12, v13}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 157
    .local v7, "temp":Lorg/w3c/dom/Node;
    if-eqz v7, :cond_3

    .line 158
    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v12, v12, v1

    invoke-virtual {v3, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v12, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mName:Ljava/lang/String;

    .line 162
    :cond_3
    invoke-interface {v6, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    const-string v13, "MCCMNC"

    invoke-virtual {v3, v12, v13}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 163
    if-eqz v7, :cond_4

    .line 164
    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v12, v12, v1

    invoke-virtual {v3, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v12, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mMccMnc:Ljava/lang/String;

    .line 166
    :cond_4
    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v12, v12, v1

    const/4 v13, 0x0

    iput-object v13, v12, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mVoiceMailName:Ljava/lang/String;

    .line 167
    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v12, v12, v1

    const/4 v13, 0x0

    iput-object v13, v12, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mVoiceMailNumber:Ljava/lang/String;

    .line 152
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 173
    .end local v7    # "temp":Lorg/w3c/dom/Node;
    :cond_5
    const-string v12, "Settings.Messages"

    invoke-virtual {v3, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 174
    if-nez v5, :cond_6

    .line 175
    const/4 v12, -0x1

    goto/16 :goto_0

    .line 177
    :cond_6
    const-string v12, "Settings.Messages"

    invoke-virtual {v3, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 178
    if-nez v5, :cond_7

    .line 179
    const/4 v12, -0x1

    goto/16 :goto_0

    .line 181
    :cond_7
    const-string v12, "Voicemail"

    invoke-virtual {v3, v5, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 182
    if-nez v6, :cond_8

    .line 183
    const-string v12, "CscVoiceMail"

    const-string v13, "No VoiceMail info"

    invoke-static {v12, v13}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const/4 v12, -0x1

    goto/16 :goto_0

    .line 188
    :cond_8
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    .line 189
    const-string v12, "CscVoiceMail"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "voiceMail num: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v9, :cond_f

    .line 193
    const-string v10, ""

    .line 194
    .local v10, "voiceNetName":Ljava/lang/String;
    const-string v11, ""

    .line 195
    .local v11, "voiceTelNum":Ljava/lang/String;
    const-string v8, ""

    .line 198
    .local v8, "voiceMailName":Ljava/lang/String;
    invoke-interface {v6, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    const-string v13, "NetworkName"

    invoke-virtual {v3, v12, v13}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 199
    .restart local v7    # "temp":Lorg/w3c/dom/Node;
    if-eqz v7, :cond_9

    .line 200
    invoke-virtual {v3, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v10

    .line 204
    :cond_9
    invoke-interface {v6, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    const-string v13, "TelNum"

    invoke-virtual {v3, v12, v13}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 205
    if-eqz v7, :cond_a

    .line 206
    invoke-virtual {v3, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v11

    .line 210
    :cond_a
    invoke-interface {v6, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    const-string v13, "VoicemailName"

    invoke-virtual {v3, v12, v13}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 211
    if-eqz v7, :cond_b

    .line 212
    invoke-virtual {v3, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    .line 215
    :cond_b
    const-string v12, "CscVoiceMail"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "voiceNetName : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " /  voiceTelNum : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " /  voiceMailName : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_3
    if-ge v2, v4, :cond_e

    .line 221
    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v12, v12, v2

    iget-object v12, v12, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mName:Ljava/lang/String;

    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 222
    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v12, v12, v2

    iput-object v8, v12, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mVoiceMailName:Ljava/lang/String;

    .line 223
    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v12, v12, v2

    iput-object v11, v12, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mVoiceMailNumber:Ljava/lang/String;

    .line 220
    :cond_c
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 224
    :cond_d
    const-string v12, "default"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 225
    iput-object v11, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mDefaultNumber:Ljava/lang/String;

    goto :goto_4

    .line 192
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 231
    .end local v2    # "k":I
    .end local v7    # "temp":Lorg/w3c/dom/Node;
    .end local v8    # "voiceMailName":Ljava/lang/String;
    .end local v10    # "voiceNetName":Ljava/lang/String;
    .end local v11    # "voiceTelNum":Ljava/lang/String;
    :cond_f
    const/4 v12, 0x0

    goto/16 :goto_0
.end method

.method public makeVoiceMailConfXML(Ljava/lang/String;)V
    .locals 17
    .param p1, "voiceMailXMLPath"    # Ljava/lang/String;

    .prologue
    .line 235
    const-string v14, "CscVoiceMail"

    const-string v15, "makeVoiceMailConfXML"

    invoke-static {v14, v15}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    new-instance v1, Ljava/io/File;

    const-string v14, "/data/misc/radio"

    invoke-direct {v1, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 238
    .local v1, "CscDirectory":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_0

    .line 239
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v14

    if-nez v14, :cond_0

    .line 240
    const-string v14, "CscVoiceMail"

    const-string v15, "Cannot mkdir : /data/misc/radio"

    invoke-static {v14, v15}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_0
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 243
    .local v5, "fileVoiceMailConf":Ljava/io/File;
    const/4 v9, 0x0

    .line 245
    .local v9, "out":Ljava/io/OutputStream;
    new-instance v13, Ljava/lang/StringBuffer;

    const-string v14, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"

    invoke-direct {v13, v14}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 247
    .local v13, "writeBuffer":Ljava/lang/StringBuffer;
    const-string v14, "<voicemail>\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 249
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    array-length v14, v14

    if-ge v6, v14, :cond_6

    .line 250
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v14, v14, v6

    iget-object v7, v14, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mMccMnc:Ljava/lang/String;

    .line 251
    .local v7, "mccmnc":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v14, v14, v6

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->checkMarkupLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 252
    .local v3, "carrier":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v14, v14, v6

    iget-object v12, v14, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mVoiceMailNumber:Ljava/lang/String;

    .line 253
    .local v12, "voiceMailNum":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v14, v14, v6

    iget-object v14, v14, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mVoiceMailName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->checkMarkupLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 255
    .local v11, "voiceMailName":Ljava/lang/String;
    if-eqz v12, :cond_1

    const-string v14, ""

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 256
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mDefaultNumber:Ljava/lang/String;

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mDefaultNumber:Ljava/lang/String;

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 249
    :cond_2
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 259
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mDefaultNumber:Ljava/lang/String;

    .line 263
    :cond_4
    if-nez v11, :cond_5

    .line 264
    const-string v11, ""

    .line 267
    :cond_5
    const-string v14, "<voicemail carrier=\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, "\"    numeric=\""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, "\"    vmnumber=\""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, "\"    vmtag=\""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, "\"    />\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 272
    .end local v3    # "carrier":Ljava/lang/String;
    .end local v7    # "mccmnc":Ljava/lang/String;
    .end local v11    # "voiceMailName":Ljava/lang/String;
    .end local v12    # "voiceMailNum":Ljava/lang/String;
    :cond_6
    const-string v14, "</voicemail>\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 279
    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v14

    array-length v2, v14

    .line 280
    .local v2, "bufferCapacity":I
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 282
    .local v8, "newByteBuffer":Ljava/nio/ByteBuffer;
    const-string v14, "CscVoiceMail"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Capa : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " / byte Size : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->getBytes()[B

    move-result-object v16

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v14

    invoke-virtual {v8, v14}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 289
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 290
    invoke-virtual {v5}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    :cond_7
    :try_start_1
    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 305
    :try_start_2
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .line 311
    .end local v9    # "out":Ljava/io/OutputStream;
    .local v10, "out":Ljava/io/OutputStream;
    :try_start_3
    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/io/OutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 317
    :try_start_4
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_2
    move-object v9, v10

    .line 322
    .end local v10    # "out":Ljava/io/OutputStream;
    .restart local v9    # "out":Ljava/io/OutputStream;
    :goto_3
    return-void

    .line 291
    :catch_0
    move-exception v4

    .line 292
    .local v4, "e":Ljava/lang/SecurityException;
    invoke-virtual {v4}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_3

    .line 298
    .end local v4    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v4

    .line 300
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 306
    .end local v4    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v4

    .line 307
    .local v4, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_3

    .line 318
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    .end local v9    # "out":Ljava/io/OutputStream;
    .restart local v10    # "out":Ljava/io/OutputStream;
    :catch_3
    move-exception v4

    .line 319
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 312
    .end local v4    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v4

    .line 314
    .restart local v4    # "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 317
    :try_start_6
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_2

    .line 318
    :catch_5
    move-exception v4

    .line 319
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 316
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v14

    .line 317
    :try_start_7
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 320
    :goto_4
    throw v14

    .line 318
    :catch_6
    move-exception v4

    .line 319
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4
.end method

.method public setVoicemailUI(Ljava/lang/String;)V
    .locals 5
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 469
    const/4 v0, 0x0

    .line 470
    .local v0, "context":Landroid/content/Context;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->ct:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.android.phone"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 472
    const-string v2, "com.android.phone_preferences"

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->sPrefs:Landroid/content/SharedPreferences;

    .line 474
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->sPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 475
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 476
    :cond_0
    const-string v2, "vm_number_key"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 480
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 484
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :goto_1
    return-void

    .line 478
    .restart local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    const-string v2, "vm_number_key"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 481
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public update()V
    .locals 7

    .prologue
    .line 419
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->loadNetworkInfo()I

    move-result v4

    if-eqz v4, :cond_1

    .line 420
    const-string v4, "CscVoiceMail"

    const-string v5, "no network / voicemail info"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string v4, ""

    invoke-virtual {p0, v4}, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->setVoicemailUI(Ljava/lang/String;)V

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 427
    :cond_1
    const-string v4, "/data/misc/radio/voicemail-conf.xml"

    invoke-virtual {p0, v4}, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->makeVoiceMailConfXML(Ljava/lang/String;)V

    .line 430
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mDefaultNumber:Ljava/lang/String;

    .line 433
    .local v3, "setVoiceMailNumber":Ljava/lang/String;
    const-string v4, "gsm.sim.operator.numeric"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 436
    .local v0, "currentSimImsi":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 437
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    .line 439
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 440
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mMccMnc:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 441
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mVoiceMailName:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 442
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "VoicemailName"

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v6, v6, v1

    iget-object v6, v6, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mVoiceMailName:Ljava/lang/String;

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 444
    :cond_2
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mVoiceMailNumber:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 445
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;

    aget-object v4, v4, v1

    iget-object v3, v4, Lcom/samsung/sec/android/application/csc/CscVoiceMail$NwkInfo;->mVoiceMailNumber:Ljava/lang/String;

    .line 452
    .end local v1    # "i":I
    :cond_3
    const-string v4, "CscVoiceMail"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "currentSimImsi : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / setVoiceMailNumber : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->setVoicemailUI(Ljava/lang/String;)V

    .line 457
    const-string v4, "CscVoiceMail"

    const-string v5, "VoiceMail setting DONE"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v4, "CscVoiceMail"

    const-string v5, "[Voicemail] send broadcast CSC_UPDATE_VOICEMAIL_DONE"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.CSC_UPDATE_VOICEMAIL_DONE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 462
    .local v2, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->ct:Landroid/content/Context;

    if-eqz v4, :cond_0

    .line 463
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVoiceMail;->ct:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 439
    .end local v2    # "intent":Landroid/content/Intent;
    .restart local v1    # "i":I
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
