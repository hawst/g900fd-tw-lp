.class Lcom/samsung/sec/android/application/csc/CscContacts$Contact;
.super Ljava/lang/Object;
.source "CscContacts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscContacts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Contact"
.end annotation


# instance fields
.field private mName:Ljava/lang/String;

.field private mPhoneData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscContacts;


# direct methods
.method public constructor <init>(Lcom/samsung/sec/android/application/csc/CscContacts;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1610
    .local p3, "phoneData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;>;"
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscContacts$Contact;->this$0:Lcom/samsung/sec/android/application/csc/CscContacts;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1611
    iput-object p2, p0, Lcom/samsung/sec/android/application/csc/CscContacts$Contact;->mName:Ljava/lang/String;

    .line 1612
    iput-object p3, p0, Lcom/samsung/sec/android/application/csc/CscContacts$Contact;->mPhoneData:Ljava/util/ArrayList;

    .line 1613
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1616
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts$Contact;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscContacts$PhoneData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1620
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts$Contact;->mPhoneData:Ljava/util/ArrayList;

    return-object v0
.end method
