.class public Lcom/samsung/sec/android/application/csc/CscWbAmr;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscWbAmr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;
    }
.end annotation


# static fields
.field private static final isMarvell:Z


# instance fields
.field private final DATA_RADIO_AMR_PATH:Ljava/lang/String;

.field private final DATA_RADIO_AMR_TEMP:Ljava/lang/String;

.field private DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final DEFAULT_OFF:Ljava/lang/String;

.field private final DEFAULT_ON:Ljava/lang/String;

.field private DEFAULT_VALUE:Ljava/lang/String;

.field private DEFAULT_VALUE_INTEGER:I

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mNwkInfoWithWbAmr:[Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;

.field private mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

.field private mResolver:Landroid/content/ContentResolver;

.field private mStrWbAmrNetwork:Ljava/lang/String;

.field private mWbAmrEnable:I

.field private mbWbAmrTagExist:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 113
    const-string v0, "mrvl"

    const-string v1, "ro.board.platform"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SM-G3568V"

    const-string v1, "ro.product.model"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->isMarvell:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 66
    const-string v0, "/data/misc/radio/wbamr"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DATA_RADIO_AMR_PATH:Ljava/lang/String;

    .line 68
    const-string v0, "/data/misc/radio/wbamr"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DATA_RADIO_AMR_TEMP:Ljava/lang/String;

    .line 70
    const-string v0, "defaultOFF"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_OFF:Ljava/lang/String;

    .line 72
    const-string v0, "defaultON"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_ON:Ljava/lang/String;

    .line 74
    const-string v0, "defaultOFF"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE:Ljava/lang/String;

    .line 76
    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    .line 80
    iput-boolean v1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mbWbAmrTagExist:Z

    .line 83
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 111
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 477
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscWbAmr$1;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscWbAmr$1;-><init>(Lcom/samsung/sec/android/application/csc/CscWbAmr;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mHandler:Landroid/os/Handler;

    .line 117
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mContext:Landroid/content/Context;

    .line 118
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    .line 123
    const-string v0, "defaultOFF"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE:Ljava/lang/String;

    .line 124
    iput v1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    .line 126
    const-string v0, "2G"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_RIL_ConfigWbamr"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    .line 131
    :cond_0
    new-instance v0, Lcom/samsung/android/sec_platform_library/FactoryPhone;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sec_platform_library/FactoryPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 132
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    .line 133
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mResolver:Landroid/content/ContentResolver;

    .line 134
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/sec/android/application/csc/CscWbAmr;)Lcom/samsung/android/sec_platform_library/FactoryPhone;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscWbAmr;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    return-object v0
.end method

.method private loadNetworkInfoWithWbAmr()I
    .locals 14

    .prologue
    .line 223
    const/4 v6, 0x0

    .line 224
    .local v6, "networkSize":I
    const/4 v10, 0x0

    .line 229
    .local v10, "wbAmrNum":I
    const-string v11, ""

    iput-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    .line 232
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v4, v11}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 235
    .local v4, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const-string v11, "Settings.Main.Network.WbAmr.WbAmrCodec"

    invoke-virtual {v4, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 236
    .local v1, "checkWbAmrExist":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 237
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mbWbAmrTagExist:Z

    .line 238
    const-string v11, "CscWbAmr"

    const-string v12, "Can\'t parse Settings.Main.Network.WbAmr.WbAmrCodec"

    invoke-static {v11, v12}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const/4 v11, -0x1

    .line 379
    :goto_0
    return v11

    .line 241
    :cond_0
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mbWbAmrTagExist:Z

    .line 244
    const-string v11, "GeneralInfo"

    invoke-virtual {v4, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 245
    .local v7, "node":Lorg/w3c/dom/Node;
    if-nez v7, :cond_1

    .line 246
    const-string v11, "CscWbAmr"

    const-string v12, "Can\'t search GeneralInfo"

    invoke-static {v11, v12}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const/4 v11, -0x1

    goto :goto_0

    .line 250
    :cond_1
    const-string v11, "NetworkInfo"

    invoke-virtual {v4, v7, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    .line 251
    .local v8, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v8, :cond_2

    .line 252
    const-string v11, "CscWbAmr"

    const-string v12, "No network info"

    invoke-static {v11, v12}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const/4 v11, -0x1

    goto :goto_0

    .line 257
    :cond_2
    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    .line 258
    new-array v11, v6, [Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;

    iput-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mNwkInfoWithWbAmr:[Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;

    .line 259
    const-string v11, "CscWbAmr"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Network numbers : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v6, :cond_5

    .line 263
    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mNwkInfoWithWbAmr:[Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;

    new-instance v12, Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;

    const/4 v13, 0x0

    invoke-direct {v12, p0, v13}, Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;-><init>(Lcom/samsung/sec/android/application/csc/CscWbAmr;Lcom/samsung/sec/android/application/csc/CscWbAmr$1;)V

    aput-object v12, v11, v2

    .line 266
    invoke-interface {v8, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    const-string v12, "NetworkName"

    invoke-virtual {v4, v11, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v9

    .line 267
    .local v9, "temp":Lorg/w3c/dom/Node;
    if-eqz v9, :cond_3

    .line 268
    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mNwkInfoWithWbAmr:[Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;

    aget-object v11, v11, v2

    invoke-virtual {v4, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;->mName:Ljava/lang/String;

    .line 272
    :cond_3
    invoke-interface {v8, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    const-string v12, "MCCMNC"

    invoke-virtual {v4, v11, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v9

    .line 273
    if-eqz v9, :cond_4

    .line 274
    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mNwkInfoWithWbAmr:[Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;

    aget-object v11, v11, v2

    invoke-virtual {v4, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;->mMccMnc:Ljava/lang/String;

    .line 262
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 279
    .end local v9    # "temp":Lorg/w3c/dom/Node;
    :cond_5
    const-string v11, "Settings.Main.Network"

    invoke-virtual {v4, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 280
    if-nez v7, :cond_6

    .line 281
    const/4 v11, -0x1

    goto/16 :goto_0

    .line 283
    :cond_6
    const-string v11, "WbAmr"

    invoke-virtual {v4, v7, v11}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    .line 284
    if-nez v8, :cond_7

    .line 285
    const-string v11, "CscWbAmr"

    const-string v12, "No WB-AMR info"

    invoke-static {v11, v12}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const/4 v11, -0x1

    goto/16 :goto_0

    .line 290
    :cond_7
    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    .line 291
    const-string v11, "CscWbAmr"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "WB-AMR num: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v10, :cond_14

    .line 295
    const-string v5, ""

    .line 296
    .local v5, "netName":Ljava/lang/String;
    const-string v0, ""

    .line 299
    .local v0, "amrOption":Ljava/lang/String;
    invoke-interface {v8, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    const-string v12, "NetworkName"

    invoke-virtual {v4, v11, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v9

    .line 300
    .restart local v9    # "temp":Lorg/w3c/dom/Node;
    if-eqz v9, :cond_8

    .line 301
    invoke-virtual {v4, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    .line 305
    :cond_8
    invoke-interface {v8, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    const-string v12, "WbAmrCodec"

    invoke-virtual {v4, v11, v12}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v9

    .line 306
    if-eqz v9, :cond_9

    .line 307
    invoke-virtual {v4, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    .line 310
    :cond_9
    const-string v11, "CscWbAmr"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "netName  : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " /  WB-AMR : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    const-string v11, "default"

    invoke-virtual {v11, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 314
    const-string v11, "on"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_a

    const-string v11, "enable"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 315
    :cond_a
    const-string v11, "defaultON"

    iput-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    .line 316
    const/4 v11, 0x1

    iput v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    .line 326
    :goto_3
    const-string v11, "2G"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v12

    const-string v13, "CscFeature_RIL_ConfigWbamr"

    invoke-virtual {v12, v13}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 327
    iget v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    or-int/lit8 v11, v11, 0x2

    iput v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    .line 330
    :cond_b
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 317
    :cond_c
    const-string v11, "off"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_d

    const-string v11, "disable"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 319
    :cond_d
    const-string v11, "defaultOFF"

    iput-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    .line 320
    const/4 v11, 0x0

    iput v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    goto :goto_3

    .line 322
    :cond_e
    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE:Ljava/lang/String;

    iput-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    .line 323
    iget v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    iput v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    goto :goto_3

    .line 335
    :cond_f
    const/4 v3, 0x0

    .local v3, "k":I
    :goto_4
    if-ge v3, v6, :cond_13

    .line 336
    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mNwkInfoWithWbAmr:[Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;

    aget-object v11, v11, v3

    iget-object v11, v11, Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;->mName:Ljava/lang/String;

    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 350
    const-string v11, "on"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_10

    const-string v11, "enable"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 354
    :cond_10
    const/4 v11, 0x1

    iput v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    .line 357
    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mNwkInfoWithWbAmr:[Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;

    aget-object v11, v11, v3

    const/4 v12, 0x1

    iput v12, v11, Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;->mWbAmrCodec:I

    .line 358
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mNwkInfoWithWbAmr:[Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;

    aget-object v12, v12, v3

    iget-object v12, v12, Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;->mMccMnc:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    .line 359
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "#"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    .line 335
    :cond_11
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 361
    :cond_12
    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mNwkInfoWithWbAmr:[Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;

    aget-object v11, v11, v3

    const/4 v12, 0x0

    iput v12, v11, Lcom/samsung/sec/android/application/csc/CscWbAmr$NwkInfo;->mWbAmrCodec:I

    goto :goto_5

    .line 294
    :cond_13
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 370
    .end local v0    # "amrOption":Ljava/lang/String;
    .end local v3    # "k":I
    .end local v5    # "netName":Ljava/lang/String;
    .end local v9    # "temp":Lorg/w3c/dom/Node;
    :cond_14
    const-string v11, ""

    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_15

    .line 371
    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE:Ljava/lang/String;

    iput-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    .line 372
    iget v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    iput v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    .line 375
    :cond_15
    const-string v11, "2G"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v12

    const-string v13, "CscFeature_RIL_ConfigWbamr"

    invoke-virtual {v12, v13}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_16

    .line 376
    iget v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    or-int/lit8 v11, v11, 0x2

    iput v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    .line 379
    :cond_16
    const/4 v11, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 219
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Main.Network.AutoWbAmr"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 13

    .prologue
    .line 151
    const-string v1, ""

    .line 153
    .local v1, "compareString":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscWbAmr;->loadNetworkInfoWithWbAmr()I

    move-result v10

    if-gez v10, :cond_0

    .line 154
    iget-object v10, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE:Ljava/lang/String;

    iput-object v10, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    .line 155
    iget v10, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    iput v10, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    .line 160
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscWbAmr;->getSettings()I

    move-result v10

    iget v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    if-eq v10, v11, :cond_1

    .line 161
    const-string v10, "Settings.Main.Network.WbAmr.WbAmrCodec"

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-static {v10, v11, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v10, "CscWbAmrWBAMR_SETTINGS"

    invoke-static {v10}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 163
    const-string v10, "WBAMR_SETTINGS"

    .line 212
    :goto_0
    return-object v10

    .line 166
    :cond_1
    new-instance v4, Ljava/io/File;

    const-string v10, "/data/misc/radio/wbamr"

    invoke-direct {v4, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 167
    .local v4, "file":Ljava/io/File;
    const/4 v6, 0x0

    .line 168
    .local v6, "fin":Ljava/io/FileInputStream;
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v10

    long-to-int v5, v10

    .line 170
    .local v5, "fileSize":I
    if-nez v5, :cond_2

    .line 171
    const-string v10, "Settings.Main.Network.WbAmr.WbAmrCodec"

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-static {v10, v11, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v10, "CscWbAmrWbAmrCodec"

    invoke-static {v10}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 173
    const-string v10, "WbAmrCodec"

    goto :goto_0

    .line 177
    :cond_2
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .local v7, "fin":Ljava/io/FileInputStream;
    :try_start_1
    new-array v0, v5, [B

    .line 183
    .local v0, "buffer":[B
    invoke-virtual {v7, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v9

    .line 184
    .local v9, "len":I
    if-lez v9, :cond_3

    .line 185
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v1    # "compareString":Ljava/lang/String;
    .local v2, "compareString":Ljava/lang/String;
    move-object v1, v2

    .line 193
    .end local v2    # "compareString":Ljava/lang/String;
    .restart local v1    # "compareString":Ljava/lang/String;
    :cond_3
    if-eqz v7, :cond_4

    .line 194
    :try_start_2
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_4
    move-object v6, v7

    .line 200
    .end local v0    # "buffer":[B
    .end local v7    # "fin":Ljava/io/FileInputStream;
    .end local v9    # "len":I
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    :cond_5
    :goto_1
    const-string v10, "CscWbAmr"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "xml = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " / data = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v10, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 203
    iget-boolean v10, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mbWbAmrTagExist:Z

    if-eqz v10, :cond_6

    .line 204
    const-string v10, "Settings.Main.Network.WbAmr.NetworkName"

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v10, "Settings.Main.Network.WbAmr.WbAmrCodec"

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_6
    const-string v10, "NOERROR"

    goto :goto_0

    .line 195
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v0    # "buffer":[B
    .restart local v7    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "len":I
    :catch_0
    move-exception v3

    .line 196
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move-object v6, v7

    .line 198
    .end local v7    # "fin":Ljava/io/FileInputStream;
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    goto :goto_1

    .line 187
    .end local v0    # "buffer":[B
    .end local v3    # "e":Ljava/io/IOException;
    .end local v9    # "len":I
    :catch_1
    move-exception v8

    .line 188
    .local v8, "fnfe":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_3
    sget-object v10, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v11, "FileNotFoundException"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 193
    if-eqz v6, :cond_5

    .line 194
    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 195
    :catch_2
    move-exception v3

    .line 196
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 189
    .end local v3    # "e":Ljava/io/IOException;
    .end local v8    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 190
    .local v3, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_5
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 193
    if-eqz v6, :cond_5

    .line 194
    :try_start_6
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    .line 195
    :catch_4
    move-exception v3

    .line 196
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 192
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 193
    :goto_4
    if-eqz v6, :cond_7

    .line 194
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 197
    :cond_7
    :goto_5
    throw v10

    .line 195
    :catch_5
    move-exception v3

    .line 196
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 209
    .end local v3    # "e":Ljava/io/IOException;
    :cond_8
    const-string v10, "Settings.Main.Network.WbAmr.NetworkName"

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-static {v10, v11, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v10, "Settings.Main.Network.WbAmr.WbAmrCodec"

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-static {v10, v11, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v10, "CscWbAmrWbAmrCodec"

    invoke-static {v10}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 212
    const-string v10, "WbAmrCodec"

    goto/16 :goto_0

    .line 192
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v7    # "fin":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v10

    move-object v6, v7

    .end local v7    # "fin":Ljava/io/FileInputStream;
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    goto :goto_4

    .line 189
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v7    # "fin":Ljava/io/FileInputStream;
    :catch_6
    move-exception v3

    move-object v6, v7

    .end local v7    # "fin":Ljava/io/FileInputStream;
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    goto :goto_3

    .line 187
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v7    # "fin":Ljava/io/FileInputStream;
    :catch_7
    move-exception v8

    move-object v6, v7

    .end local v7    # "fin":Ljava/io/FileInputStream;
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method getSettings()I
    .locals 3

    .prologue
    .line 440
    :try_start_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "wbamr_mode"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 443
    :goto_0
    return v1

    .line 441
    :catch_0
    move-exception v0

    .line 442
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v1, "CscWbAmr"

    const-string v2, "to get (wbamr_mode) failed"

    invoke-static {v1, v2, v0}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 443
    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    goto :goto_0
.end method

.method makeWbAmrConfigFile()V
    .locals 5

    .prologue
    .line 383
    new-instance v1, Ljava/io/File;

    const-string v4, "/data/misc/radio/wbamr"

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 384
    .local v1, "file":Ljava/io/File;
    const/4 v2, 0x0

    .line 387
    .local v2, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 388
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 403
    :try_start_2
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .line 410
    .end local v2    # "out":Ljava/io/OutputStream;
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_3
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/OutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 416
    :try_start_4
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_0
    move-object v2, v3

    .line 421
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    :goto_1
    return-void

    .line 389
    :catch_0
    move-exception v0

    .line 390
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_1

    .line 396
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 398
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 404
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 405
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 417
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_3
    move-exception v0

    .line 418
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 411
    .end local v0    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v0

    .line 413
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 416
    :try_start_6
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_0

    .line 417
    :catch_5
    move-exception v0

    .line 418
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 415
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 416
    :try_start_7
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 419
    :goto_2
    throw v4

    .line 417
    :catch_6
    move-exception v0

    .line 418
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method sendIMEIUpdateItem()V
    .locals 7

    .prologue
    const/16 v6, 0x7d0

    const/4 v5, 0x6

    const/4 v4, 0x1

    const/16 v3, -0x6c

    const/4 v2, 0x0

    .line 449
    sget-boolean v1, Lcom/samsung/sec/android/application/csc/CscWbAmr;->isMarvell:Z

    if-eqz v1, :cond_1

    .line 450
    new-instance v0, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/4 v1, -0x1

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 451
    .local v0, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    invoke-virtual {v0, v3}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 452
    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 453
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    if-eqz v1, :cond_0

    .line 454
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 458
    .end local v0    # "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    :cond_1
    new-instance v0, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/4 v1, 0x7

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 460
    .restart local v0    # "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    invoke-virtual {v0, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 461
    invoke-virtual {v0, v4}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 463
    invoke-virtual {v0, v3}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 464
    invoke-virtual {v0, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 465
    invoke-virtual {v0, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 466
    invoke-virtual {v0, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 467
    invoke-virtual {v0, v4}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 468
    invoke-virtual {v0, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 469
    iget v1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 471
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0
.end method

.method setSettings()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 425
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    if-ne v0, v4, :cond_0

    .line 426
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "wbamr_mode"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 436
    :goto_0
    return-void

    .line 427
    :cond_0
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    if-ne v0, v3, :cond_1

    .line 428
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "wbamr_mode"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 429
    :cond_1
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    if-ne v0, v2, :cond_2

    .line 430
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "wbamr_mode"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 431
    :cond_2
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    if-nez v0, :cond_3

    .line 432
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "wbamr_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 434
    :cond_3
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "wbamr_mode"

    iget v2, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public update()V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscWbAmr;->loadNetworkInfoWithWbAmr()I

    move-result v0

    if-gez v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mStrWbAmrNetwork:Ljava/lang/String;

    .line 139
    iget v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->DEFAULT_VALUE_INTEGER:I

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscWbAmr;->mWbAmrEnable:I

    .line 144
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscWbAmr;->makeWbAmrConfigFile()V

    .line 145
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscWbAmr;->setSettings()V

    .line 146
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscWbAmr;->sendIMEIUpdateItem()V

    .line 148
    return-void
.end method

.method public updateForHomeFOTA(IZ)V
    .locals 0
    .param p1, "buildType"    # I
    .param p2, "isNewCscEdtion"    # Z

    .prologue
    .line 513
    return-void
.end method
