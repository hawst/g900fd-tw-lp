.class public Lcom/samsung/sec/android/jni/hdcp/HdcpKey;
.super Ljava/lang/Object;
.source "HdcpKey.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method


# virtual methods
.method public installKey(Landroid/content/Context;)I
    .locals 24
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 21
    const/16 v20, 0x0

    .line 23
    .local v20, "ret":I
    new-instance v6, Ljava/io/File;

    const-string v21, "/data/misc/radio/hatp"

    move-object/from16 v0, v21

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 24
    .local v6, "dir":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    .line 26
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 31
    .local v2, "base_path":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "/system/bin/insthk "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v19

    .line 32
    .local v19, "proc":Ljava/lang/Process;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->close()V

    .line 33
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->close()V

    .line 34
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/io/OutputStream;->close()V

    .line 35
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Process;->waitFor()I

    .line 37
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Process;->exitValue()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v20

    .line 52
    const-string v16, "OK"

    .line 53
    .local v16, "pOKMsg":Ljava/lang/String;
    const-string v10, "NG 1"

    .line 54
    .local v10, "pFailMsg1":Ljava/lang/String;
    const-string v11, "NG 2"

    .line 55
    .local v11, "pFailMsg2":Ljava/lang/String;
    const-string v12, "NG 3"

    .line 56
    .local v12, "pFailMsg3":Ljava/lang/String;
    const-string v13, "NG 4"

    .line 57
    .local v13, "pFailMsg4":Ljava/lang/String;
    const-string v14, "NG 5"

    .line 59
    .local v14, "pFailMsg5":Ljava/lang/String;
    const/16 v17, 0x0

    .line 60
    .local v17, "pWd":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .line 62
    .local v4, "checkFile":Ljava/io/File;
    :try_start_1
    new-instance v5, Ljava/io/File;

    const-string v21, "check_result"

    move-object/from16 v0, v21

    invoke-direct {v5, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 63
    .end local v4    # "checkFile":Ljava/io/File;
    .local v5, "checkFile":Ljava/io/File;
    :try_start_2
    new-instance v18, Ljava/io/FileOutputStream;

    move-object/from16 v0, v18

    invoke-direct {v0, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 64
    .end local v17    # "pWd":Ljava/io/FileOutputStream;
    .local v18, "pWd":Ljava/io/FileOutputStream;
    const/4 v9, 0x0

    .line 66
    .local v9, "len":I
    if-nez v20, :cond_1

    .line 67
    move-object/from16 v15, v16

    .line 80
    .local v15, "pMsg":Ljava/lang/String;
    :goto_0
    :try_start_3
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v9

    .line 81
    invoke-virtual {v15}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 82
    .local v3, "buf":[B
    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1, v9}, Ljava/io/FileOutputStream;->write([BII)V

    .line 83
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/io/FileDescriptor;->sync()V

    .line 84
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V

    .line 86
    const-string v21, "HATP"

    const-string v22, "Change checkfile permission"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const/16 v21, 0x1

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v5, v0, v1}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v21

    if-nez v21, :cond_0

    .line 88
    const-string v21, "HATP"

    const-string v22, "Cannot change permission checkfile"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const/16 v20, 0x64

    .line 92
    :cond_0
    const-string v21, "HATP"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "installKey log : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 105
    const/16 v21, 0x0

    .end local v3    # "buf":[B
    .end local v5    # "checkFile":Ljava/io/File;
    .end local v9    # "len":I
    .end local v10    # "pFailMsg1":Ljava/lang/String;
    .end local v11    # "pFailMsg2":Ljava/lang/String;
    .end local v12    # "pFailMsg3":Ljava/lang/String;
    .end local v13    # "pFailMsg4":Ljava/lang/String;
    .end local v14    # "pFailMsg5":Ljava/lang/String;
    .end local v15    # "pMsg":Ljava/lang/String;
    .end local v16    # "pOKMsg":Ljava/lang/String;
    .end local v18    # "pWd":Ljava/io/FileOutputStream;
    .end local v19    # "proc":Ljava/lang/Process;
    :goto_1
    return v21

    .line 42
    :catch_0
    move-exception v7

    .line 43
    .local v7, "e":Ljava/io/IOException;
    const-string v21, "HATP"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "installkey Error : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v7}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    const/16 v21, -0x1

    goto :goto_1

    .line 45
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    .line 46
    .local v7, "e":Ljava/lang/InterruptedException;
    const-string v21, "HATP"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "installkey Error : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v7}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const/16 v21, -0x1

    goto :goto_1

    .line 68
    .end local v7    # "e":Ljava/lang/InterruptedException;
    .restart local v5    # "checkFile":Ljava/io/File;
    .restart local v9    # "len":I
    .restart local v10    # "pFailMsg1":Ljava/lang/String;
    .restart local v11    # "pFailMsg2":Ljava/lang/String;
    .restart local v12    # "pFailMsg3":Ljava/lang/String;
    .restart local v13    # "pFailMsg4":Ljava/lang/String;
    .restart local v14    # "pFailMsg5":Ljava/lang/String;
    .restart local v16    # "pOKMsg":Ljava/lang/String;
    .restart local v18    # "pWd":Ljava/io/FileOutputStream;
    .restart local v19    # "proc":Ljava/lang/Process;
    :cond_1
    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    .line 69
    move-object v15, v10

    .restart local v15    # "pMsg":Ljava/lang/String;
    goto/16 :goto_0

    .line 70
    .end local v15    # "pMsg":Ljava/lang/String;
    :cond_2
    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    .line 71
    move-object v15, v11

    .restart local v15    # "pMsg":Ljava/lang/String;
    goto/16 :goto_0

    .line 72
    .end local v15    # "pMsg":Ljava/lang/String;
    :cond_3
    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    .line 73
    move-object v15, v12

    .restart local v15    # "pMsg":Ljava/lang/String;
    goto/16 :goto_0

    .line 74
    .end local v15    # "pMsg":Ljava/lang/String;
    :cond_4
    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    .line 75
    move-object v15, v13

    .restart local v15    # "pMsg":Ljava/lang/String;
    goto/16 :goto_0

    .line 77
    .end local v15    # "pMsg":Ljava/lang/String;
    :cond_5
    move-object v15, v14

    .restart local v15    # "pMsg":Ljava/lang/String;
    goto/16 :goto_0

    .line 93
    .end local v5    # "checkFile":Ljava/io/File;
    .end local v9    # "len":I
    .end local v15    # "pMsg":Ljava/lang/String;
    .end local v18    # "pWd":Ljava/io/FileOutputStream;
    .restart local v4    # "checkFile":Ljava/io/File;
    .restart local v17    # "pWd":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v7

    .line 94
    .local v7, "e":Ljava/io/IOException;
    :goto_2
    const-string v21, "HATP"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "File I/O Error"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "check_result"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    if-eqz v17, :cond_6

    .line 97
    :try_start_4
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 102
    :cond_6
    :goto_3
    const/16 v21, -0x1

    goto/16 :goto_1

    .line 98
    :catch_3
    move-exception v8

    .line 99
    .local v8, "e2":Ljava/io/IOException;
    const-string v21, "HATP"

    const-string v22, "close error"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 93
    .end local v4    # "checkFile":Ljava/io/File;
    .end local v7    # "e":Ljava/io/IOException;
    .end local v8    # "e2":Ljava/io/IOException;
    .restart local v5    # "checkFile":Ljava/io/File;
    :catch_4
    move-exception v7

    move-object v4, v5

    .end local v5    # "checkFile":Ljava/io/File;
    .restart local v4    # "checkFile":Ljava/io/File;
    goto :goto_2

    .end local v4    # "checkFile":Ljava/io/File;
    .end local v17    # "pWd":Ljava/io/FileOutputStream;
    .restart local v5    # "checkFile":Ljava/io/File;
    .restart local v9    # "len":I
    .restart local v15    # "pMsg":Ljava/lang/String;
    .restart local v18    # "pWd":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v7

    move-object v4, v5

    .end local v5    # "checkFile":Ljava/io/File;
    .restart local v4    # "checkFile":Ljava/io/File;
    move-object/from16 v17, v18

    .end local v18    # "pWd":Ljava/io/FileOutputStream;
    .restart local v17    # "pWd":Ljava/io/FileOutputStream;
    goto :goto_2
.end method
