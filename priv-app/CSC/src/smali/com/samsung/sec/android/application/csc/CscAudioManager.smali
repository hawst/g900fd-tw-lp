.class public Lcom/samsung/sec/android/application/csc/CscAudioManager;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscAudioManager.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 57
    const-string v0, "CSCAudiomanager"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscAudioManager;->TAG:Ljava/lang/String;

    .line 67
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscAudioManager;->mContext:Landroid/content/Context;

    .line 68
    return-void
.end method


# virtual methods
.method public compare()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 75
    const-string v2, "NOERROR"

    .line 76
    .local v2, "answer":Ljava/lang/String;
    const/4 v0, 0x0

    .line 77
    .local v0, "CscRingtoneStr":Ljava/lang/String;
    new-instance v3, Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 79
    .local v3, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    if-nez v3, :cond_1

    .line 80
    const-string v6, "CSCAudiomanager"

    const-string v7, "CscAudioManager::compare memory alloc fail"

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v6, "CSCAudiomanager : CscAudioManager::compare memory alloc fail"

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 83
    const-string v2, "Settings.Main.Sound.RingTone.src"

    .line 105
    .end local v2    # "answer":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 86
    .restart local v2    # "answer":Ljava/lang/String;
    :cond_1
    const-string v6, "CSCAudiomanager"

    const-string v7, "CscAudioManager::compare"

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    new-instance v5, Landroid/media/RingtoneManager;

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscAudioManager;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/media/RingtoneManager;-><init>(Landroid/content/Context;)V

    .line 90
    .local v5, "rm":Landroid/media/RingtoneManager;
    const-string v6, "Settings.Main.Sound.RingTone.src"

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->getAttrbute(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    .line 92
    const-string v6, "CSCAudiomanager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CscAudioManager Ringtone in CscRingtone : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "!!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    if-eqz v0, :cond_0

    .line 94
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscAudioManager;->mContext:Landroid/content/Context;

    invoke-static {v6, v9}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v1

    .line 96
    .local v1, "RingtoneUri":Landroid/net/Uri;
    const-string v6, "CSCAudiomanager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CscAudioManager Ringtone in RingtoneUri : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "!!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscAudioManager;->mContext:Landroid/content/Context;

    invoke-static {v6, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscAudioManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 98
    .local v4, "ringtoneFile":Ljava/lang/String;
    const-string v6, "CSCAudiomanager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CscAudioManager Ringtone in ringtoneFile : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "!!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    if-nez v4, :cond_0

    .line 100
    const-string v6, "CSCAudiomanager : Settings.Main.Sound.RingTone.src"

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 101
    const-string v2, "Settings.Main.Sound.RingTone.src"

    goto/16 :goto_0
.end method

.method public update()V
    .locals 2

    .prologue
    .line 71
    const-string v0, "CSCAudiomanager"

    const-string v1, "CscAudioManager::update"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-void
.end method
