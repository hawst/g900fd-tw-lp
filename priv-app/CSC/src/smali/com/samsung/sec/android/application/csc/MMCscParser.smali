.class public Lcom/samsung/sec/android/application/csc/MMCscParser;
.super Ljava/lang/Object;
.source "MMCscParser.java"


# instance fields
.field private final CUSTOMER_CSC_FILE:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "MMCscParser"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/MMCscParser;->TAG:Ljava/lang/String;

    .line 24
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/MMCscParser;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    .line 37
    return-void
.end method

.method private getNetworkName(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 138
    const/4 v4, 0x0

    .line 139
    .local v4, "rtnSting":Ljava/lang/String;
    const/4 v1, 0x0

    .line 141
    .local v1, "mtempCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    if-nez v1, :cond_0

    .line 142
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    .end local v1    # "mtempCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/MMCscParser;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 147
    .restart local v1    # "mtempCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_0
    const-string v7, "GeneralInfo"

    invoke-virtual {v1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 148
    .local v2, "node":Lorg/w3c/dom/Node;
    if-nez v2, :cond_1

    .line 149
    const-string v7, "MMCscParser"

    const-string v8, "No network info"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v4

    .line 165
    .end local v4    # "rtnSting":Ljava/lang/String;
    .local v5, "rtnSting":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 153
    .end local v5    # "rtnSting":Ljava/lang/String;
    .restart local v4    # "rtnSting":Ljava/lang/String;
    :cond_1
    const-string v7, "NetworkInfo"

    invoke-virtual {v1, v2, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 154
    .local v3, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v3, :cond_2

    .line 155
    const-string v7, "MMCscParser"

    const-string v8, "No network info"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v4

    .line 156
    .end local v4    # "rtnSting":Ljava/lang/String;
    .restart local v5    # "rtnSting":Ljava/lang/String;
    goto :goto_0

    .line 158
    .end local v5    # "rtnSting":Ljava/lang/String;
    .restart local v4    # "rtnSting":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    if-ge v0, v7, :cond_4

    .line 159
    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "MCCMNC"

    invoke-virtual {v1, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 160
    .local v6, "tempNode":Lorg/w3c/dom/Node;
    invoke-virtual {v1, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 161
    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "NetworkName"

    invoke-virtual {v1, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v6

    .line 162
    invoke-virtual {v1, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 158
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v6    # "tempNode":Lorg/w3c/dom/Node;
    :cond_4
    move-object v5, v4

    .line 165
    .end local v4    # "rtnSting":Ljava/lang/String;
    .restart local v5    # "rtnSting":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public getEMBMSValueFromMPS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 91
    const/4 v2, 0x0

    .line 93
    .local v2, "mEMBMSSettingsValue":Ljava/lang/String;
    const/4 v1, 0x0

    .line 95
    .local v1, "mCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    if-nez v1, :cond_0

    .line 96
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    .end local v1    # "mCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/MMCscParser;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v8}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 98
    .restart local v1    # "mCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_0
    const-string v8, "gsm.sim.operator.numeric"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/samsung/sec/android/application/csc/MMCscParser;->getNetworkName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 100
    .local v4, "mNwName":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 101
    const-string v8, "MMCscParser"

    const-string v9, "can\'t get MCCMNC from sim"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 129
    .end local v2    # "mEMBMSSettingsValue":Ljava/lang/String;
    .local v3, "mEMBMSSettingsValue":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 105
    .end local v3    # "mEMBMSSettingsValue":Ljava/lang/String;
    .restart local v2    # "mEMBMSSettingsValue":Ljava/lang/String;
    :cond_1
    if-nez v1, :cond_2

    .line 106
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    .end local v1    # "mCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/MMCscParser;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v8}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 108
    .restart local v1    # "mCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_2
    const-string v8, "Settings.eMBMSSettings"

    invoke-virtual {v1, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 109
    .local v5, "node":Lorg/w3c/dom/Node;
    if-nez v5, :cond_3

    .line 110
    const-string v8, "MMCscParser"

    const-string v9, "no node in eMBMSsettings"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 111
    .end local v2    # "mEMBMSSettingsValue":Ljava/lang/String;
    .restart local v3    # "mEMBMSSettingsValue":Ljava/lang/String;
    goto :goto_0

    .line 114
    .end local v3    # "mEMBMSSettingsValue":Ljava/lang/String;
    .restart local v2    # "mEMBMSSettingsValue":Ljava/lang/String;
    :cond_3
    invoke-virtual {v1, v5, p1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 115
    .local v6, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v6, :cond_4

    .line 116
    const-string v8, "MMCscParser"

    const-string v9, "no nodeList in eMBMS"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 117
    .end local v2    # "mEMBMSSettingsValue":Ljava/lang/String;
    .restart local v3    # "mEMBMSSettingsValue":Ljava/lang/String;
    goto :goto_0

    .line 120
    .end local v3    # "mEMBMSSettingsValue":Ljava/lang/String;
    .restart local v2    # "mEMBMSSettingsValue":Ljava/lang/String;
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-ge v0, v8, :cond_6

    .line 121
    invoke-interface {v6, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "NetworkName"

    invoke-virtual {v1, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 122
    .local v7, "tempNode":Lorg/w3c/dom/Node;
    invoke-virtual {v1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 123
    invoke-interface {v6, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    invoke-virtual {v1, v8, p2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 124
    invoke-virtual {v1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    .line 120
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 127
    .end local v7    # "tempNode":Lorg/w3c/dom/Node;
    :cond_6
    const-string v8, "MMCscParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getValueFromMPS mEMBMSSettingsValue : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 129
    .end local v2    # "mEMBMSSettingsValue":Ljava/lang/String;
    .restart local v3    # "mEMBMSSettingsValue":Ljava/lang/String;
    goto :goto_0
.end method

.method public getIMSValueFromMPS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 44
    const/4 v2, 0x0

    .line 46
    .local v2, "mImsSettingsValue":Ljava/lang/String;
    const/4 v1, 0x0

    .line 48
    .local v1, "mCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    if-nez v1, :cond_0

    .line 49
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    .end local v1    # "mCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/MMCscParser;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v8}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 51
    .restart local v1    # "mCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_0
    const-string v8, "gsm.sim.operator.numeric"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/samsung/sec/android/application/csc/MMCscParser;->getNetworkName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 53
    .local v4, "mNwName":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 54
    const-string v8, "MMCscParser"

    const-string v9, "can\'t get MCCMNC from sim"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 82
    .end local v2    # "mImsSettingsValue":Ljava/lang/String;
    .local v3, "mImsSettingsValue":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 58
    .end local v3    # "mImsSettingsValue":Ljava/lang/String;
    .restart local v2    # "mImsSettingsValue":Ljava/lang/String;
    :cond_1
    if-nez v1, :cond_2

    .line 59
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    .end local v1    # "mCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/MMCscParser;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v8}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 61
    .restart local v1    # "mCscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_2
    const-string v8, "Settings.IMSSettings"

    invoke-virtual {v1, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 62
    .local v5, "node":Lorg/w3c/dom/Node;
    if-nez v5, :cond_3

    .line 63
    const-string v8, "MMCscParser"

    const-string v9, "no node in imssettings"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 64
    .end local v2    # "mImsSettingsValue":Ljava/lang/String;
    .restart local v3    # "mImsSettingsValue":Ljava/lang/String;
    goto :goto_0

    .line 67
    .end local v3    # "mImsSettingsValue":Ljava/lang/String;
    .restart local v2    # "mImsSettingsValue":Ljava/lang/String;
    :cond_3
    invoke-virtual {v1, v5, p1}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 68
    .local v6, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v6, :cond_4

    .line 69
    const-string v8, "MMCscParser"

    const-string v9, "no nodeList in imserttings"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 70
    .end local v2    # "mImsSettingsValue":Ljava/lang/String;
    .restart local v3    # "mImsSettingsValue":Ljava/lang/String;
    goto :goto_0

    .line 73
    .end local v3    # "mImsSettingsValue":Ljava/lang/String;
    .restart local v2    # "mImsSettingsValue":Ljava/lang/String;
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-ge v0, v8, :cond_6

    .line 74
    invoke-interface {v6, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "NetworkName"

    invoke-virtual {v1, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 75
    .local v7, "tempNode":Lorg/w3c/dom/Node;
    invoke-virtual {v1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 76
    invoke-interface {v6, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    invoke-virtual {v1, v8, p2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 77
    invoke-virtual {v1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    .line 73
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 80
    .end local v7    # "tempNode":Lorg/w3c/dom/Node;
    :cond_6
    const-string v8, "MMCscParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getValueFromMPS mImsSettingsValue : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 82
    .end local v2    # "mImsSettingsValue":Ljava/lang/String;
    .restart local v3    # "mImsSettingsValue":Ljava/lang/String;
    goto :goto_0
.end method
