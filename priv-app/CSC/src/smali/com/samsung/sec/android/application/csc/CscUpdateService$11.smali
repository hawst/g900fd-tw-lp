.class Lcom/samsung/sec/android/application/csc/CscUpdateService$11;
.super Ljava/lang/Object;
.source "CscUpdateService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/sec/android/application/csc/CscUpdateService;->doUpdateForHomeFOTA(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

.field final synthetic val$binary_type:I

.field final synthetic val$fotaUpdateList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;Ljava/util/ArrayList;I)V
    .locals 0

    .prologue
    .line 678
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$11;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    iput-object p2, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$11;->val$fotaUpdateList:Ljava/util/ArrayList;

    iput p3, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$11;->val$binary_type:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 680
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$11;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscUtil;->isNewCscEdition(Landroid/content/Context;)Z

    move-result v2

    .line 681
    .local v2, "isNewCscEdition":Z
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$11;->val$fotaUpdateList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/sec/android/application/csc/UpdatableForHomeFOTA;

    .line 682
    .local v3, "u":Lcom/samsung/sec/android/application/csc/UpdatableForHomeFOTA;
    const-string v4, "CscUpdateService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mUpdatablesHomeFOTA start. ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$11;->val$binary_type:I

    invoke-interface {v3, v4, v2}, Lcom/samsung/sec/android/application/csc/UpdatableForHomeFOTA;->updateForHomeFOTA(IZ)V

    .line 684
    const-string v4, "CscUpdateService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mUpdatablesHomeFOTA done. ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 687
    .end local v3    # "u":Lcom/samsung/sec/android/application/csc/UpdatableForHomeFOTA;
    :cond_0
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$11;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscUtil;->setCscVersionAndEdition(Landroid/content/Context;)V

    .line 688
    new-instance v0, Ljava/io/File;

    const-string v4, "/efs/log/boot_cause"

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 689
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 690
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 694
    :goto_1
    return-void

    .line 692
    :cond_1
    const-string v4, "CscUpdateService"

    const-string v5, "No file about home/fota define file"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
