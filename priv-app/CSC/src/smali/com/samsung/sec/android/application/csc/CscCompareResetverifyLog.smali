.class public Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;
.super Ljava/lang/Object;
.source "CscCompareResetverifyLog.java"


# static fields
.field private static sFailLog:Ljava/lang/String;

.field private static sFailLogCount:I

.field private static sStartLog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sFailLog:Ljava/lang/String;

    .line 23
    sput v1, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sFailLogCount:I

    .line 25
    sput-boolean v1, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sStartLog:Z

    return-void
.end method

.method public static addFailLog(Ljava/lang/String;)V
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 51
    sget-boolean v0, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sStartLog:Z

    if-nez v0, :cond_0

    .line 52
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->startLog()V

    .line 55
    :cond_0
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sFailLog:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sFailLog:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sFailLogCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sFailLogCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sFailLog:Ljava/lang/String;

    .line 58
    :cond_1
    return-void
.end method

.method public static addFailLogLine(Ljava/lang/String;)V
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 46
    const-string v0, "\n"

    const-string v1, "\r\n"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLog(Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public static endLog()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    const-string v0, "CscCompareResetverifyLog"

    const-string v1, "endLog"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sFailLog:Ljava/lang/String;

    .line 41
    sput v2, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sFailLogCount:I

    .line 42
    sput-boolean v2, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sStartLog:Z

    .line 43
    return-void
.end method

.method public static startLog()V
    .locals 2

    .prologue
    .line 29
    const-string v0, "CscCompareResetverifyLog"

    const-string v1, "startLog"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->endLog()V

    .line 32
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sFailLog:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    sput v0, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sFailLogCount:I

    .line 34
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->sStartLog:Z

    .line 35
    return-void
.end method
