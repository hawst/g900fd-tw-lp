.class Lcom/samsung/sec/android/application/csc/CscSBrowser$1;
.super Landroid/content/BroadcastReceiver;
.source "CscSBrowser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/sec/android/application/csc/CscSBrowser;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscSBrowser;


# direct methods
.method constructor <init>(Lcom/samsung/sec/android/application/csc/CscSBrowser;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser$1;->this$0:Lcom/samsung/sec/android/application/csc/CscSBrowser;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 192
    const-string v3, "CscSBrowser"

    const-string v4, " ** onReceive runs.."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.CSC_BROWSER_HOMEPAGE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 195
    const-string v3, "homepage"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 196
    .local v1, "rcvd":Ljava/lang/String;
    const-string v3, "CscSBrowser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ** onReceive : received url - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser$1;->this$0:Lcom/samsung/sec/android/application/csc/CscSBrowser;

    # getter for: Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->access$000(Lcom/samsung/sec/android/application/csc/CscSBrowser;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "csc.preferences_name"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 199
    .local v2, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 200
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v3, "homepage"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 201
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 204
    .end local v0    # "ed":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "rcvd":Ljava/lang/String;
    .end local v2    # "sp":Landroid/content/SharedPreferences;
    :cond_0
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser$1;->this$0:Lcom/samsung/sec/android/application/csc/CscSBrowser;

    # getter for: Lcom/samsung/sec/android/application/csc/CscSBrowser;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->access$000(Lcom/samsung/sec/android/application/csc/CscSBrowser;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSBrowser$1;->this$0:Lcom/samsung/sec/android/application/csc/CscSBrowser;

    # getter for: Lcom/samsung/sec/android/application/csc/CscSBrowser;->mHomepageUrlRcvr:Landroid/content/BroadcastReceiver;
    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscSBrowser;->access$100(Lcom/samsung/sec/android/application/csc/CscSBrowser;)Landroid/content/BroadcastReceiver;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 205
    return-void
.end method
