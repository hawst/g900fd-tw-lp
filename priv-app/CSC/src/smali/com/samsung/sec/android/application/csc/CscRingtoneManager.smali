.class public Lcom/samsung/sec/android/application/csc/CscRingtoneManager;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscRingtoneManager.java"


# instance fields
.field private final CSCRINGTONE_CNT_FILE_PREFIX:Ljava/lang/String;

.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final NOERROR:Ljava/lang/String;

.field private final RINGTONECOUNT_FAIL:Ljava/lang/String;

.field private final RINGTONE_CNT_FILE:Ljava/lang/String;

.field private final RINGTONE_CNT_FILE_SUFFIX:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private final TAG_CP:Ljava/lang/String;

.field private final TXT_SUFFIX:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 36
    const-string v0, "CscCompareService"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->TAG_CP:Ljava/lang/String;

    .line 38
    const-string v0, "CSCRingtoneManager"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->TAG:Ljava/lang/String;

    .line 40
    const-string v0, "/system/etc/ringtones_count_list.txt"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->RINGTONE_CNT_FILE:Ljava/lang/String;

    .line 42
    const-string v0, ".txt"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->RINGTONE_CNT_FILE_SUFFIX:Ljava/lang/String;

    .line 44
    const-string v0, "/system/etc/csc_ringtones_list.txt"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->CSCRINGTONE_CNT_FILE_PREFIX:Ljava/lang/String;

    .line 46
    const-string v0, ".txt"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->TXT_SUFFIX:Ljava/lang/String;

    .line 48
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 50
    const-string v0, "NOERROR"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->NOERROR:Ljava/lang/String;

    .line 52
    const-string v0, "RINGTONECOUNT FAIL"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->RINGTONECOUNT_FAIL:Ljava/lang/String;

    .line 57
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->mContext:Landroid/content/Context;

    .line 58
    return-void
.end method

.method private getAudioFilePath(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 196
    const/4 v8, 0x0

    .line 197
    .local v8, "str":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 198
    .local v7, "fileUri":Ljava/lang/String;
    const/4 v6, 0x0

    .line 200
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v9, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "_data"

    aput-object v4, v2, v1

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 204
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v9, :cond_0

    .line 205
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 206
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 209
    :cond_0
    if-eqz v6, :cond_1

    .line 210
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 213
    :cond_1
    return-object v8

    .line 209
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 210
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getRingtoneListFromRingtoneManager(Landroid/media/RingtoneManager;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "rm"    # Landroid/media/RingtoneManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/RingtoneManager;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .local v2, "ringtoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_2

    if-eqz v2, :cond_2

    .line 178
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Landroid/media/RingtoneManager;->setType(I)V

    .line 179
    invoke-virtual {p1}, Landroid/media/RingtoneManager;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 180
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 181
    .local v1, "position":I
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 182
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_1

    .line 183
    invoke-virtual {p1, v1}, Landroid/media/RingtoneManager;->getRingtoneUri(I)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v5}, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->getAudioFilePath(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 184
    .local v3, "strPath":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    const-string v4, "/system/media/audio/ringtones/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 186
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 188
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 190
    .end local v3    # "strPath":Ljava/lang/String;
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 192
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v1    # "position":I
    :cond_2
    return-object v2
.end method


# virtual methods
.method public compare()Ljava/lang/String;
    .locals 23

    .prologue
    .line 64
    const-string v3, ""

    .line 66
    .local v3, "CscRingtoneStr":Ljava/lang/String;
    const-string v20, "CSCRingtoneManager"

    const-string v21, "compare"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const-string v20, "ro.csc.sales_code"

    invoke-static/range {v20 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 69
    .local v19, "salesCode":Ljava/lang/String;
    if-nez v19, :cond_1

    .line 70
    const-string v20, "CscCompareService"

    const-string v21, "no sales_code"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    const-string v20, "CscRingtoneManager : no sales_code"

    invoke-static/range {v20 .. v20}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 72
    const-string v20, "RINGTONECOUNT FAIL"

    .line 172
    :cond_0
    :goto_0
    return-object v20

    .line 75
    :cond_1
    const-string v15, "/system/etc/ringtones_count_list.txt"

    .line 76
    .local v15, "ringtoneCountFileName":Ljava/lang/String;
    const-string v5, "/system/etc/csc_ringtones_list.txt"

    .line 77
    .local v5, "cscRingtoneFileName":Ljava/lang/String;
    const/4 v12, 0x0

    .line 78
    .local v12, "reader_pda":Ljava/io/BufferedReader;
    const/4 v10, 0x0

    .line 80
    .local v10, "reader_csc":Ljava/io/BufferedReader;
    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    .line 83
    .local v16, "ringtonesInBuildTime":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :try_start_0
    new-instance v4, Ljava/io/File;

    const-string v20, "/system/etc/ringtones_count_list.txt"

    move-object/from16 v0, v20

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 84
    .local v4, "RingtoneCountFile":Ljava/io/File;
    new-instance v13, Ljava/io/BufferedReader;

    new-instance v20, Ljava/io/FileReader;

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v20

    invoke-direct {v13, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    .end local v12    # "reader_pda":Ljava/io/BufferedReader;
    .local v13, "reader_pda":Ljava/io/BufferedReader;
    :cond_2
    :goto_1
    :try_start_1
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    .local v8, "line":Ljava/lang/String;
    if-eqz v8, :cond_3

    .line 87
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v20

    if-lez v20, :cond_2

    .line 88
    const-string v20, "CSCRingtoneManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "add "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " on pda"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_1

    .line 92
    .end local v8    # "line":Ljava/lang/String;
    :catch_0
    move-exception v6

    move-object v12, v13

    .line 93
    .end local v4    # "RingtoneCountFile":Ljava/io/File;
    .end local v13    # "reader_pda":Ljava/io/BufferedReader;
    .local v6, "ex":Ljava/lang/Exception;
    .restart local v12    # "reader_pda":Ljava/io/BufferedReader;
    :goto_2
    :try_start_2
    const-string v20, "CscCompareService"

    const-string v21, "file error on /system/etc/ringtones_count_list.txt"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const-string v20, "CscRingtoneManager : file error on /system/etc/ringtones_count_list.txt"

    invoke-static/range {v20 .. v20}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 96
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 97
    const-string v20, "RINGTONECOUNT FAIL"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 100
    if-eqz v12, :cond_0

    .line 101
    :try_start_3
    invoke-virtual {v12}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 103
    :catch_1
    move-exception v6

    .line 104
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 100
    .end local v6    # "ex":Ljava/lang/Exception;
    .end local v12    # "reader_pda":Ljava/io/BufferedReader;
    .restart local v4    # "RingtoneCountFile":Ljava/io/File;
    .restart local v8    # "line":Ljava/lang/String;
    .restart local v13    # "reader_pda":Ljava/io/BufferedReader;
    :cond_3
    if-eqz v13, :cond_4

    .line 101
    :try_start_4
    invoke-virtual {v13}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 110
    :cond_4
    :goto_3
    :try_start_5
    new-instance v2, Ljava/io/File;

    const-string v20, "/system/etc/csc_ringtones_list.txt"

    move-object/from16 v0, v20

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 111
    .local v2, "CscRingtoneCountFile":Ljava/io/File;
    new-instance v11, Ljava/io/BufferedReader;

    new-instance v20, Ljava/io/FileReader;

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v20

    invoke-direct {v11, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 113
    .end local v10    # "reader_csc":Ljava/io/BufferedReader;
    .local v11, "reader_csc":Ljava/io/BufferedReader;
    :cond_5
    :goto_4
    :try_start_6
    invoke-virtual {v11}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 114
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v20

    if-lez v20, :cond_5

    .line 115
    const-string v20, "CSCRingtoneManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "add "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " on csc"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_4

    .line 119
    :catch_2
    move-exception v6

    move-object v10, v11

    .line 120
    .end local v2    # "CscRingtoneCountFile":Ljava/io/File;
    .end local v11    # "reader_csc":Ljava/io/BufferedReader;
    .restart local v6    # "ex":Ljava/lang/Exception;
    .restart local v10    # "reader_csc":Ljava/io/BufferedReader;
    :goto_5
    :try_start_7
    const-string v20, "CscCompareService"

    const-string v21, "file error on /system/etc/ringtones_count_list.txt"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const-string v20, "CscRingtoneManager : file error on /system/etc/ringtones_count_list.txt"

    invoke-static/range {v20 .. v20}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 123
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 124
    const-string v20, "RINGTONECOUNT FAIL"
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 127
    if-eqz v10, :cond_0

    .line 128
    :try_start_8
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_0

    .line 130
    :catch_3
    move-exception v6

    .line 131
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 103
    .end local v6    # "ex":Ljava/lang/Exception;
    :catch_4
    move-exception v6

    .line 104
    .restart local v6    # "ex":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 99
    .end local v4    # "RingtoneCountFile":Ljava/io/File;
    .end local v6    # "ex":Ljava/lang/Exception;
    .end local v8    # "line":Ljava/lang/String;
    .end local v13    # "reader_pda":Ljava/io/BufferedReader;
    .restart local v12    # "reader_pda":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v20

    .line 100
    :goto_6
    if-eqz v12, :cond_6

    .line 101
    :try_start_9
    invoke-virtual {v12}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    .line 105
    :cond_6
    :goto_7
    throw v20

    .line 103
    :catch_5
    move-exception v6

    .line 104
    .restart local v6    # "ex":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_7

    .line 127
    .end local v6    # "ex":Ljava/lang/Exception;
    .end local v10    # "reader_csc":Ljava/io/BufferedReader;
    .end local v12    # "reader_pda":Ljava/io/BufferedReader;
    .restart local v2    # "CscRingtoneCountFile":Ljava/io/File;
    .restart local v4    # "RingtoneCountFile":Ljava/io/File;
    .restart local v8    # "line":Ljava/lang/String;
    .restart local v11    # "reader_csc":Ljava/io/BufferedReader;
    .restart local v13    # "reader_pda":Ljava/io/BufferedReader;
    :cond_7
    if-eqz v11, :cond_8

    .line 128
    :try_start_a
    invoke-virtual {v11}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    .line 135
    :cond_8
    :goto_8
    new-instance v18, Landroid/media/RingtoneManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Landroid/media/RingtoneManager;-><init>(Landroid/content/Context;)V

    .line 136
    .local v18, "rm":Landroid/media/RingtoneManager;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscRingtoneManager;->getRingtoneListFromRingtoneManager(Landroid/media/RingtoneManager;)Ljava/util/ArrayList;

    move-result-object v17

    .line 138
    .local v17, "ringtonesInRingtoneManager":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v17, :cond_a

    .line 139
    const-string v20, "CscCompareService"

    const-string v21, "RINGTONECOUNT FAIL : RingtoneManager is null"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const-string v20, "RINGTONECOUNT FAIL : RingtoneManager is null"

    goto/16 :goto_0

    .line 130
    .end local v17    # "ringtonesInRingtoneManager":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v18    # "rm":Landroid/media/RingtoneManager;
    :catch_6
    move-exception v6

    .line 131
    .restart local v6    # "ex":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8

    .line 126
    .end local v2    # "CscRingtoneCountFile":Ljava/io/File;
    .end local v6    # "ex":Ljava/lang/Exception;
    .end local v11    # "reader_csc":Ljava/io/BufferedReader;
    .restart local v10    # "reader_csc":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v20

    .line 127
    :goto_9
    if-eqz v10, :cond_9

    .line 128
    :try_start_b
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7

    .line 132
    :cond_9
    :goto_a
    throw v20

    .line 130
    :catch_7
    move-exception v6

    .line 131
    .restart local v6    # "ex":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_a

    .line 143
    .end local v6    # "ex":Ljava/lang/Exception;
    .end local v10    # "reader_csc":Ljava/io/BufferedReader;
    .restart local v2    # "CscRingtoneCountFile":Ljava/io/File;
    .restart local v11    # "reader_csc":Ljava/io/BufferedReader;
    .restart local v17    # "ringtonesInRingtoneManager":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v18    # "rm":Landroid/media/RingtoneManager;
    :cond_a
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 144
    .local v14, "ringtone":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    .line 145
    .local v9, "ok":Z
    if-nez v9, :cond_b

    .line 146
    const-string v20, "CscCompareService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "RINGTONECOUNT FAIL:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    const-string v20, ""

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 148
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "RINGTONECOUNT FAIL:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 149
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "CscRingtoneManager : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    goto :goto_b

    .line 156
    .end local v9    # "ok":Z
    .end local v14    # "ringtone":Ljava/lang/String;
    :cond_c
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_d
    :goto_c
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 157
    .restart local v14    # "ringtone":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    .line 158
    .restart local v9    # "ok":Z
    if-nez v9, :cond_d

    .line 159
    const-string v20, "CscCompareService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "RINGTONECOUNT FAIL:MORE:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const-string v20, ""

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 161
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "RINGTONECOUNT FAIL:MORE:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 162
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "CscRingtoneManager : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    goto :goto_c

    .line 169
    .end local v9    # "ok":Z
    .end local v14    # "ringtone":Ljava/lang/String;
    :cond_e
    const-string v20, ""

    move-object/from16 v0, v20

    if-eq v3, v0, :cond_f

    move-object/from16 v20, v3

    .line 170
    goto/16 :goto_0

    .line 172
    :cond_f
    const-string v20, "NOERROR"

    goto/16 :goto_0

    .line 126
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v17    # "ringtonesInRingtoneManager":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v18    # "rm":Landroid/media/RingtoneManager;
    :catchall_2
    move-exception v20

    move-object v10, v11

    .end local v11    # "reader_csc":Ljava/io/BufferedReader;
    .restart local v10    # "reader_csc":Ljava/io/BufferedReader;
    goto/16 :goto_9

    .line 119
    .end local v2    # "CscRingtoneCountFile":Ljava/io/File;
    :catch_8
    move-exception v6

    goto/16 :goto_5

    .line 99
    .end local v8    # "line":Ljava/lang/String;
    :catchall_3
    move-exception v20

    move-object v12, v13

    .end local v13    # "reader_pda":Ljava/io/BufferedReader;
    .restart local v12    # "reader_pda":Ljava/io/BufferedReader;
    goto/16 :goto_6

    .line 92
    .end local v4    # "RingtoneCountFile":Ljava/io/File;
    :catch_9
    move-exception v6

    goto/16 :goto_2
.end method

.method public update()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method
