.class Lcom/samsung/sec/android/application/csc/CscUpdateService$12;
.super Landroid/os/Handler;
.source "CscUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscUpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;


# direct methods
.method constructor <init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V
    .locals 0

    .prologue
    .line 809
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$12;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v4, 0x7c6

    .line 812
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 843
    :cond_0
    :goto_0
    return-void

    .line 814
    :sswitch_0
    const-string v1, "CscUpdateService"

    const-string v2, "CscUpdate response incoming!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 817
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    .line 818
    const-string v1, "CscUpdateService"

    const-string v2, "AsyncResult Exception Occur!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 824
    .end local v0    # "ar":Landroid/os/AsyncResult;
    :sswitch_1
    const-string v1, "CscUpdateService"

    const-string v2, "START_UPDATE_MSG!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 825
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$12;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    iget-object v1, v1, Lcom/samsung/sec/android/application/csc/CscUpdateService;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x7cf

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 826
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$12;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    # invokes: Lcom/samsung/sec/android/application/csc/CscUpdateService;->doUpdate()V
    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->access$100(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V

    goto :goto_0

    .line 830
    :sswitch_2
    const-string v1, "CscUpdateService"

    const-string v2, "SEND_MODEM_DELAYED!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$12;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->chekUpdateDoneCondition()Z

    move-result v1

    if-nez v1, :cond_1

    # getter for: Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkRetry:I
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->access$200()I

    move-result v1

    if-gez v1, :cond_2

    .line 832
    :cond_1
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$12;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscUpdateService;->mHandlerPhone:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->access$300(Lcom/samsung/sec/android/application/csc/CscUpdateService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 833
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$12;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    invoke-virtual {v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->SendModemSettingIntent()V

    goto :goto_0

    .line 836
    :cond_2
    # operator-- for: Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkRetry:I
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->access$210()I

    .line 837
    const-string v1, "CscUpdateService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SEND_MODEM_DELAYED!! (checkRetry("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkRetry:I
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->access$200()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$12;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscUpdateService;->PendingItems:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->access$400(Lcom/samsung/sec/android/application/csc/CscUpdateService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$12;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscUpdateService;->mHandlerPhone:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->access$300(Lcom/samsung/sec/android/application/csc/CscUpdateService;)Landroid/os/Handler;

    move-result-object v1

    # getter for: Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkDelay:I
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->access$500()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 812
    :sswitch_data_0
    .sparse-switch
        0x7c6 -> :sswitch_2
        0x7ce -> :sswitch_0
        0x7cf -> :sswitch_1
    .end sparse-switch
.end method
