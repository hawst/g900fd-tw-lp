.class public Lcom/samsung/sec/android/application/csc/CscXMLParser;
.super Ljava/lang/Object;
.source "CscXMLParser.java"


# instance fields
.field doc:Lorg/w3c/dom/Document;

.field outputFilePath:Ljava/lang/String;

.field rootElement:Lcom/samsung/sec/android/application/csc/CscXMLElement;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "CscXMLParser"

    const-string v1, "CscXMLParser()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->outputFilePath:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method addAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "atrrValue"    # Ljava/lang/String;

    .prologue
    .line 137
    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 138
    .local v1, "lastTokenIndex":I
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "attibuteName":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 141
    .local v2, "tagPath":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->rootElement:Lcom/samsung/sec/android/application/csc/CscXMLElement;

    const/4 v3, 0x0

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v2, v3, v0, p2}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    return-void
.end method

.method addElement(Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscXMLElement;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "cscElement"    # Lcom/samsung/sec/android/application/csc/CscXMLElement;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->rootElement:Lcom/samsung/sec/android/application/csc/CscXMLElement;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addElement(Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscXMLElement;)V

    .line 146
    return-void
.end method

.method addValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->rootElement:Lcom/samsung/sec/android/application/csc/CscXMLElement;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method public endXML()V
    .locals 12

    .prologue
    .line 57
    const-string v9, "CscXMLParser"

    const-string v10, "endXML()"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const/4 v3, 0x0

    .local v3, "logWriter":Ljava/io/OutputStreamWriter;
    const/4 v1, 0x0

    .line 60
    .local v1, "fileWriter":Ljava/io/OutputStreamWriter;
    :try_start_0
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v8

    .line 62
    .local v8, "transformerFactory":Ljavax/xml/transform/TransformerFactory;
    invoke-virtual {v8}, Ljavax/xml/transform/TransformerFactory;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v7

    .line 64
    .local v7, "transformer":Ljavax/xml/transform/Transformer;
    const-string v9, "encoding"

    const-string v10, "UTF-8"

    invoke-virtual {v7, v9, v10}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v9, "indent"

    const-string v10, "yes"

    invoke-virtual {v7, v9, v10}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v9, "{http://xml.apache.org/xslt}indent-amount"

    const-string v10, "2"

    invoke-virtual {v7, v9, v10}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v9, "omit-xml-declaration"

    const-string v10, "yes"

    invoke-virtual {v7, v9, v10}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    new-instance v6, Ljavax/xml/transform/dom/DOMSource;

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->doc:Lorg/w3c/dom/Document;

    invoke-direct {v6, v9}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 71
    .local v6, "source":Ljavax/xml/transform/dom/DOMSource;
    new-instance v4, Ljava/io/OutputStreamWriter;

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "utf-8"

    invoke-direct {v4, v9, v10}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    .end local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    .local v4, "logWriter":Ljava/io/OutputStreamWriter;
    :try_start_1
    const-string v9, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v4, v9}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 73
    new-instance v5, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v5, v4}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/Writer;)V

    .line 74
    .local v5, "result":Ljavax/xml/transform/stream/StreamResult;
    invoke-virtual {v7, v6, v5}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V

    .line 77
    new-instance v2, Ljava/io/OutputStreamWriter;

    new-instance v9, Ljava/io/FileOutputStream;

    new-instance v10, Ljava/io/File;

    iget-object v11, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->outputFilePath:Ljava/lang/String;

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v9, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const-string v10, "utf-8"

    invoke-direct {v2, v9, v10}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 79
    .end local v1    # "fileWriter":Ljava/io/OutputStreamWriter;
    .local v2, "fileWriter":Ljava/io/OutputStreamWriter;
    :try_start_2
    const-string v9, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v2, v9}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 80
    new-instance v5, Ljavax/xml/transform/stream/StreamResult;

    .end local v5    # "result":Ljavax/xml/transform/stream/StreamResult;
    invoke-direct {v5, v2}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/Writer;)V

    .line 81
    .restart local v5    # "result":Ljavax/xml/transform/stream/StreamResult;
    invoke-virtual {v7, v6, v5}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 86
    if-eqz v4, :cond_0

    .line 87
    :try_start_3
    invoke-virtual {v4}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 91
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 92
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :cond_1
    move-object v1, v2

    .end local v2    # "fileWriter":Ljava/io/OutputStreamWriter;
    .restart local v1    # "fileWriter":Ljava/io/OutputStreamWriter;
    move-object v3, v4

    .line 96
    .end local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    .end local v5    # "result":Ljavax/xml/transform/stream/StreamResult;
    .end local v6    # "source":Ljavax/xml/transform/dom/DOMSource;
    .end local v7    # "transformer":Ljavax/xml/transform/Transformer;
    .end local v8    # "transformerFactory":Ljavax/xml/transform/TransformerFactory;
    .restart local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    :cond_2
    :goto_1
    return-void

    .line 93
    .end local v1    # "fileWriter":Ljava/io/OutputStreamWriter;
    .end local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v2    # "fileWriter":Ljava/io/OutputStreamWriter;
    .restart local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v5    # "result":Ljavax/xml/transform/stream/StreamResult;
    .restart local v6    # "source":Ljavax/xml/transform/dom/DOMSource;
    .restart local v7    # "transformer":Ljavax/xml/transform/Transformer;
    .restart local v8    # "transformerFactory":Ljavax/xml/transform/TransformerFactory;
    :catch_0
    move-exception v9

    move-object v1, v2

    .end local v2    # "fileWriter":Ljava/io/OutputStreamWriter;
    .restart local v1    # "fileWriter":Ljava/io/OutputStreamWriter;
    move-object v3, v4

    .line 95
    .end local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    goto :goto_1

    .line 82
    .end local v5    # "result":Ljavax/xml/transform/stream/StreamResult;
    .end local v6    # "source":Ljavax/xml/transform/dom/DOMSource;
    .end local v7    # "transformer":Ljavax/xml/transform/Transformer;
    .end local v8    # "transformerFactory":Ljavax/xml/transform/TransformerFactory;
    :catch_1
    move-exception v0

    .line 83
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    const-string v9, "CscXMLParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "endXML() - Exception:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 86
    if-eqz v3, :cond_3

    .line 87
    :try_start_6
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 91
    :cond_3
    :goto_3
    if-eqz v1, :cond_2

    .line 92
    :try_start_7
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_1

    .line 93
    :catch_2
    move-exception v9

    goto :goto_1

    .line 85
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    .line 86
    :goto_4
    if-eqz v3, :cond_4

    .line 87
    :try_start_8
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    .line 91
    :cond_4
    :goto_5
    if-eqz v1, :cond_5

    .line 92
    :try_start_9
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    .line 94
    :cond_5
    :goto_6
    throw v9

    .line 88
    .end local v1    # "fileWriter":Ljava/io/OutputStreamWriter;
    .end local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v2    # "fileWriter":Ljava/io/OutputStreamWriter;
    .restart local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v5    # "result":Ljavax/xml/transform/stream/StreamResult;
    .restart local v6    # "source":Ljavax/xml/transform/dom/DOMSource;
    .restart local v7    # "transformer":Ljavax/xml/transform/Transformer;
    .restart local v8    # "transformerFactory":Ljavax/xml/transform/TransformerFactory;
    :catch_3
    move-exception v9

    goto :goto_0

    .end local v2    # "fileWriter":Ljava/io/OutputStreamWriter;
    .end local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    .end local v5    # "result":Ljavax/xml/transform/stream/StreamResult;
    .end local v6    # "source":Ljavax/xml/transform/dom/DOMSource;
    .end local v7    # "transformer":Ljavax/xml/transform/Transformer;
    .end local v8    # "transformerFactory":Ljavax/xml/transform/TransformerFactory;
    .restart local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "fileWriter":Ljava/io/OutputStreamWriter;
    .restart local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    :catch_4
    move-exception v9

    goto :goto_3

    .end local v0    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v10

    goto :goto_5

    .line 93
    :catch_6
    move-exception v10

    goto :goto_6

    .line 85
    .end local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v6    # "source":Ljavax/xml/transform/dom/DOMSource;
    .restart local v7    # "transformer":Ljavax/xml/transform/Transformer;
    .restart local v8    # "transformerFactory":Ljavax/xml/transform/TransformerFactory;
    :catchall_1
    move-exception v9

    move-object v3, v4

    .end local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    goto :goto_4

    .end local v1    # "fileWriter":Ljava/io/OutputStreamWriter;
    .end local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v2    # "fileWriter":Ljava/io/OutputStreamWriter;
    .restart local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    :catchall_2
    move-exception v9

    move-object v1, v2

    .end local v2    # "fileWriter":Ljava/io/OutputStreamWriter;
    .restart local v1    # "fileWriter":Ljava/io/OutputStreamWriter;
    move-object v3, v4

    .end local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    goto :goto_4

    .line 82
    .end local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    :catch_7
    move-exception v0

    move-object v3, v4

    .end local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    goto :goto_2

    .end local v1    # "fileWriter":Ljava/io/OutputStreamWriter;
    .end local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v2    # "fileWriter":Ljava/io/OutputStreamWriter;
    .restart local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    :catch_8
    move-exception v0

    move-object v1, v2

    .end local v2    # "fileWriter":Ljava/io/OutputStreamWriter;
    .restart local v1    # "fileWriter":Ljava/io/OutputStreamWriter;
    move-object v3, v4

    .end local v4    # "logWriter":Ljava/io/OutputStreamWriter;
    .restart local v3    # "logWriter":Ljava/io/OutputStreamWriter;
    goto :goto_2
.end method

.method getFullTagSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->rootElement:Lcom/samsung/sec/android/application/csc/CscXMLElement;

    invoke-virtual {v0}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->getFullTagSet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method getNode(Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->rootElement:Lcom/samsung/sec/android/application/csc/CscXMLElement;

    invoke-virtual {v0, p1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->searchTag(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    return-object v0
.end method

.method getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->rootElement:Lcom/samsung/sec/android/application/csc/CscXMLElement;

    invoke-virtual {v0, p1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public newElement(Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscXMLElement;
    .locals 3
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 116
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscXMLElement;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->doc:Lorg/w3c/dom/Document;

    invoke-interface {v1, p1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->doc:Lorg/w3c/dom/Document;

    invoke-direct {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLElement;-><init>(Lorg/w3c/dom/Element;Lorg/w3c/dom/Document;)V

    .line 117
    .local v0, "element":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    return-object v0
.end method

.method public openXML()V
    .locals 7

    .prologue
    .line 100
    const-string v4, "CscXMLParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "openXML()"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->outputFilePath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :try_start_0
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->outputFilePath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 104
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 105
    .local v1, "docFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 107
    .local v0, "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v0, v3}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->doc:Lorg/w3c/dom/Document;

    .line 108
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscXMLElement;

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->doc:Lorg/w3c/dom/Document;

    invoke-interface {v5}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->doc:Lorg/w3c/dom/Document;

    invoke-direct {v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscXMLElement;-><init>(Lorg/w3c/dom/Element;Lorg/w3c/dom/Document;)V

    iput-object v4, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->rootElement:Lcom/samsung/sec/android/application/csc/CscXMLElement;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    .end local v0    # "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v1    # "docFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v3    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v2

    .line 111
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "CscXMLParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startXML() - Exception"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method setValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->rootElement:Lcom/samsung/sec/android/application/csc/CscXMLElement;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    return-void
.end method

.method public startXML(Ljava/lang/String;)V
    .locals 6
    .param p1, "rootElementName"    # Ljava/lang/String;

    .prologue
    .line 41
    const-string v3, "CscXMLParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startXML()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 44
    .local v1, "docFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 45
    .local v0, "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->doc:Lorg/w3c/dom/Document;

    .line 48
    invoke-virtual {p0, p1}, Lcom/samsung/sec/android/application/csc/CscXMLParser;->newElement(Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscXMLElement;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->rootElement:Lcom/samsung/sec/android/application/csc/CscXMLElement;

    .line 49
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->doc:Lorg/w3c/dom/Document;

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscXMLParser;->rootElement:Lcom/samsung/sec/android/application/csc/CscXMLElement;

    invoke-virtual {v4}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->getElement()Lorg/w3c/dom/Element;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/w3c/dom/Document;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .end local v0    # "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v1    # "docFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v2

    .line 51
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "CscXMLParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startXML() - Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
