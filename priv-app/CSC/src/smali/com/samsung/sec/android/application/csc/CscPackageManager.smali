.class public Lcom/samsung/sec/android/application/csc/CscPackageManager;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscPackageManager.java"


# static fields
.field private static final DEFAULT_CSC_FILE:Ljava/lang/String;


# instance fields
.field private GET_PACKAGE_FLAG:I

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscPackageManager;->DEFAULT_CSC_FILE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscPackageManager;->GET_PACKAGE_FLAG:I

    .line 94
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscPackageManager;->mContext:Landroid/content/Context;

    .line 95
    return-void
.end method

.method private isSystemPackage(Landroid/content/pm/PackageInfo;)Z
    .locals 1
    .param p1, "pkgInfo"    # Landroid/content/pm/PackageInfo;

    .prologue
    .line 98
    iget-object v0, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public compare()Ljava/lang/String;
    .locals 46

    .prologue
    .line 105
    const-string v3, ""

    .line 107
    .local v3, "CscAppResult":Ljava/lang/String;
    const-string v43, "CscCompareService"

    const-string v44, "compare"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v43, "ro.csc.sales_code"

    invoke-static/range {v43 .. v43}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 112
    .local v36, "salesCode":Ljava/lang/String;
    if-nez v36, :cond_0

    .line 113
    const-string v43, "CscCompareService"

    const-string v44, "no sales_code"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v43, "CscCompareService : no sales_code"

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 115
    const-string v43, "APPCOUNT FAIL"

    .line 429
    :goto_0
    return-object v43

    .line 118
    :cond_0
    const-string v6, "/system/etc/apks_count_list.txt"

    .line 119
    .local v6, "appCountFileName":Ljava/lang/String;
    const-string v11, "/system/etc/csc_apks_list.txt"

    .line 120
    .local v11, "cscAddSystemFileName":Ljava/lang/String;
    const-string v16, "/system/etc/csc_remove_apks_list.txt"

    .line 121
    .local v16, "cscRemoveSystemFileName":Ljava/lang/String;
    const-string v8, "/system/etc/csc_user_apks_list.txt"

    .line 122
    .local v8, "cscAddDataFileName":Ljava/lang/String;
    const-string v13, "/system/etc/csc_user_remove_apks_list.txt"

    .line 123
    .local v13, "cscRemoveDataFileName":Ljava/lang/String;
    const-string v23, "/system/etc/hidden_apks_list.txt"

    .line 124
    .local v23, "hiddenAppCountFileName":Ljava/lang/String;
    const-string v41, "/system/etc/vpl_apks_count_list.txt"

    .line 125
    .local v41, "vplAppCountFileName":Ljava/lang/String;
    const-string v38, "/system/etc/userdata_apks_count_list.txt"

    .line 127
    .local v38, "userAppCountFileName":Ljava/lang/String;
    new-instance v29, Ljava/util/HashSet;

    invoke-direct/range {v29 .. v29}, Ljava/util/HashSet;-><init>()V

    .line 128
    .local v29, "listPackages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v17, "cscRemoveSystemPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v24, "hiddenPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v42, Ljava/util/ArrayList;

    invoke-direct/range {v42 .. v42}, Ljava/util/ArrayList;-><init>()V

    .line 131
    .local v42, "vplPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 132
    .local v9, "cscAddDataPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v14, "cscRemoveDataPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v39, Ljava/util/ArrayList;

    invoke-direct/range {v39 .. v39}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v39, "userPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v34, 0x0

    .line 137
    .local v34, "reader":Ljava/io/BufferedReader;
    new-instance v5, Ljava/io/File;

    const-string v43, "/system/etc/apks_count_list.txt"

    move-object/from16 v0, v43

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 138
    .local v5, "appCountFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v43

    if-eqz v43, :cond_2

    .line 141
    :try_start_0
    new-instance v35, Ljava/io/BufferedReader;

    new-instance v43, Ljava/io/InputStreamReader;

    new-instance v44, Ljava/io/FileInputStream;

    move-object/from16 v0, v44

    invoke-direct {v0, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v45, "UTF-8"

    invoke-direct/range {v43 .. v45}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_19

    .line 145
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .local v35, "reader":Ljava/io/BufferedReader;
    :goto_1
    :try_start_1
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v28

    .local v28, "line":Ljava/lang/String;
    if-eqz v28, :cond_1

    .line 147
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v29

    move-object/from16 v1, v43

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 150
    .end local v28    # "line":Ljava/lang/String;
    :catch_0
    move-exception v21

    move-object/from16 v34, v35

    .line 151
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .local v21, "ex":Ljava/lang/Exception;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :goto_2
    const-string v43, "CscCompareService"

    const-string v44, "file error on /system/etc/apks_count_list.txt"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v43, "CscCompareService : file error on /system/etc/apks_count_list.txt"

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 153
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    .line 155
    :try_start_2
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 159
    :goto_3
    const-string v43, "APPCOUNT FAIL"

    goto/16 :goto_0

    .line 149
    .end local v21    # "ex":Ljava/lang/Exception;
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v28    # "line":Ljava/lang/String;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :try_start_3
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-object/from16 v34, v35

    .line 164
    .end local v28    # "line":Ljava/lang/String;
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :cond_2
    new-instance v10, Ljava/io/File;

    const-string v43, "/system/etc/csc_apks_list.txt"

    move-object/from16 v0, v43

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 165
    .local v10, "cscAddSystemFile":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v43

    if-eqz v43, :cond_4

    .line 167
    :try_start_4
    new-instance v35, Ljava/io/BufferedReader;

    new-instance v43, Ljava/io/InputStreamReader;

    new-instance v44, Ljava/io/FileInputStream;

    move-object/from16 v0, v44

    invoke-direct {v0, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v45, "UTF-8"

    invoke-direct/range {v43 .. v45}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_18

    .line 170
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :goto_4
    :try_start_5
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v28

    .restart local v28    # "line":Ljava/lang/String;
    if-eqz v28, :cond_3

    .line 172
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v29

    move-object/from16 v1, v43

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_4

    .line 175
    .end local v28    # "line":Ljava/lang/String;
    :catch_1
    move-exception v21

    move-object/from16 v34, v35

    .line 176
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v21    # "ex":Ljava/lang/Exception;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :goto_5
    const-string v43, "CscCompareService"

    const-string v44, "file error on /system/etc/csc_apks_list.txt"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v43, "CscCompareService : file error on /system/etc/csc_apks_list.txt"

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 179
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    .line 181
    :try_start_6
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 185
    :goto_6
    const-string v43, "APPCOUNT FAIL"

    goto/16 :goto_0

    .line 156
    .end local v10    # "cscAddSystemFile":Ljava/io/File;
    :catch_2
    move-exception v20

    .line 157
    .local v20, "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 174
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v21    # "ex":Ljava/lang/Exception;
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v10    # "cscAddSystemFile":Ljava/io/File;
    .restart local v28    # "line":Ljava/lang/String;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :cond_3
    :try_start_7
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    move-object/from16 v34, v35

    .line 190
    .end local v28    # "line":Ljava/lang/String;
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :cond_4
    new-instance v15, Ljava/io/File;

    const-string v43, "/system/etc/csc_remove_apks_list.txt"

    move-object/from16 v0, v43

    invoke-direct {v15, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 191
    .local v15, "cscRemoveSystemFile":Ljava/io/File;
    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v43

    if-eqz v43, :cond_6

    .line 193
    :try_start_8
    new-instance v35, Ljava/io/BufferedReader;

    new-instance v43, Ljava/io/InputStreamReader;

    new-instance v44, Ljava/io/FileInputStream;

    move-object/from16 v0, v44

    invoke-direct {v0, v15}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v45, "UTF-8"

    invoke-direct/range {v43 .. v45}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_17

    .line 196
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :goto_7
    :try_start_9
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v28

    .restart local v28    # "line":Ljava/lang/String;
    if-eqz v28, :cond_5

    .line 198
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v17

    move-object/from16 v1, v43

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_7

    .line 201
    .end local v28    # "line":Ljava/lang/String;
    :catch_3
    move-exception v21

    move-object/from16 v34, v35

    .line 202
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v21    # "ex":Ljava/lang/Exception;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :goto_8
    const-string v43, "CscCompareService"

    const-string v44, "file error on /system/etc/csc_remove_apks_list.txt"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string v43, "CscCompareService : file error on /system/etc/csc_remove_apks_list.txt"

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 205
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    .line 207
    :try_start_a
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    .line 211
    :goto_9
    const-string v43, "APPCOUNT FAIL"

    goto/16 :goto_0

    .line 182
    .end local v15    # "cscRemoveSystemFile":Ljava/io/File;
    :catch_4
    move-exception v20

    .line 183
    .restart local v20    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 200
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v21    # "ex":Ljava/lang/Exception;
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v15    # "cscRemoveSystemFile":Ljava/io/File;
    .restart local v28    # "line":Ljava/lang/String;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :cond_5
    :try_start_b
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3

    move-object/from16 v34, v35

    .line 216
    .end local v28    # "line":Ljava/lang/String;
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :cond_6
    new-instance v7, Ljava/io/File;

    const-string v43, "/system/etc/csc_user_apks_list.txt"

    move-object/from16 v0, v43

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 217
    .local v7, "cscAddDataFile":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v43

    if-eqz v43, :cond_8

    .line 219
    :try_start_c
    new-instance v35, Ljava/io/BufferedReader;

    new-instance v43, Ljava/io/InputStreamReader;

    new-instance v44, Ljava/io/FileInputStream;

    move-object/from16 v0, v44

    invoke-direct {v0, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v45, "UTF-8"

    invoke-direct/range {v43 .. v45}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_16

    .line 222
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :goto_a
    :try_start_d
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v28

    .restart local v28    # "line":Ljava/lang/String;
    if-eqz v28, :cond_7

    .line 224
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5

    goto :goto_a

    .line 227
    .end local v28    # "line":Ljava/lang/String;
    :catch_5
    move-exception v21

    move-object/from16 v34, v35

    .line 228
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v21    # "ex":Ljava/lang/Exception;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :goto_b
    const-string v43, "CscCompareService"

    const-string v44, "file error on /system/etc/csc_user_apks_list.txt"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const-string v43, "CscCompareService : file error on /system/etc/csc_user_apks_list.txt"

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 231
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    .line 233
    :try_start_e
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedReader;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_8

    .line 237
    :goto_c
    const-string v43, "APPCOUNT FAIL"

    goto/16 :goto_0

    .line 208
    .end local v7    # "cscAddDataFile":Ljava/io/File;
    :catch_6
    move-exception v20

    .line 209
    .restart local v20    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_9

    .line 226
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v21    # "ex":Ljava/lang/Exception;
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "cscAddDataFile":Ljava/io/File;
    .restart local v28    # "line":Ljava/lang/String;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :cond_7
    :try_start_f
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_5

    move-object/from16 v34, v35

    .line 242
    .end local v28    # "line":Ljava/lang/String;
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :cond_8
    new-instance v12, Ljava/io/File;

    const-string v43, "/system/etc/csc_user_remove_apks_list.txt"

    move-object/from16 v0, v43

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 243
    .local v12, "cscRemoveDataFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v43

    if-eqz v43, :cond_a

    .line 245
    :try_start_10
    new-instance v35, Ljava/io/BufferedReader;

    new-instance v43, Ljava/io/InputStreamReader;

    new-instance v44, Ljava/io/FileInputStream;

    move-object/from16 v0, v44

    invoke-direct {v0, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v45, "UTF-8"

    invoke-direct/range {v43 .. v45}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_15

    .line 248
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :goto_d
    :try_start_11
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v28

    .restart local v28    # "line":Ljava/lang/String;
    if-eqz v28, :cond_9

    .line 250
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_7

    goto :goto_d

    .line 253
    .end local v28    # "line":Ljava/lang/String;
    :catch_7
    move-exception v21

    move-object/from16 v34, v35

    .line 254
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v21    # "ex":Ljava/lang/Exception;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :goto_e
    const-string v43, "CscCompareService"

    const-string v44, "file error on /system/etc/csc_user_remove_apks_list.txt"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v43, "CscCompareService : file error on /system/etc/csc_user_remove_apks_list.txt"

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 257
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    .line 259
    :try_start_12
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedReader;->close()V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_a

    .line 263
    :goto_f
    const-string v43, "APPCOUNT FAIL"

    goto/16 :goto_0

    .line 234
    .end local v12    # "cscRemoveDataFile":Ljava/io/File;
    :catch_8
    move-exception v20

    .line 235
    .restart local v20    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_c

    .line 252
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v21    # "ex":Ljava/lang/Exception;
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v12    # "cscRemoveDataFile":Ljava/io/File;
    .restart local v28    # "line":Ljava/lang/String;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :cond_9
    :try_start_13
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->close()V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_7

    move-object/from16 v34, v35

    .line 268
    .end local v28    # "line":Ljava/lang/String;
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :cond_a
    new-instance v22, Ljava/io/File;

    const-string v43, "/system/etc/hidden_apks_list.txt"

    move-object/from16 v0, v22

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 269
    .local v22, "hiddenAppCountFile":Ljava/io/File;
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v43

    if-eqz v43, :cond_c

    .line 271
    :try_start_14
    new-instance v35, Ljava/io/BufferedReader;

    new-instance v43, Ljava/io/InputStreamReader;

    new-instance v44, Ljava/io/FileInputStream;

    move-object/from16 v0, v44

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v45, "UTF-8"

    invoke-direct/range {v43 .. v45}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_14

    .line 274
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :goto_10
    :try_start_15
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v28

    .restart local v28    # "line":Ljava/lang/String;
    if-eqz v28, :cond_b

    .line 276
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v24

    move-object/from16 v1, v43

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_9

    goto :goto_10

    .line 279
    .end local v28    # "line":Ljava/lang/String;
    :catch_9
    move-exception v21

    move-object/from16 v34, v35

    .line 280
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v21    # "ex":Ljava/lang/Exception;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :goto_11
    const-string v43, "CscCompareService"

    const-string v44, "file error on /system/etc/hidden_apks_list.txt"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const-string v43, "CscCompareService : file error on /system/etc/hidden_apks_list.txt"

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 283
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    .line 285
    :try_start_16
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedReader;->close()V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_c

    .line 289
    :goto_12
    const-string v43, "APPCOUNT FAIL"

    goto/16 :goto_0

    .line 260
    .end local v22    # "hiddenAppCountFile":Ljava/io/File;
    :catch_a
    move-exception v20

    .line 261
    .restart local v20    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_f

    .line 278
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v21    # "ex":Ljava/lang/Exception;
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v22    # "hiddenAppCountFile":Ljava/io/File;
    .restart local v28    # "line":Ljava/lang/String;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :cond_b
    :try_start_17
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->close()V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_9

    move-object/from16 v34, v35

    .line 294
    .end local v28    # "line":Ljava/lang/String;
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :cond_c
    new-instance v40, Ljava/io/File;

    const-string v43, "/system/etc/vpl_apks_count_list.txt"

    move-object/from16 v0, v40

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 295
    .local v40, "vplAppCountFile":Ljava/io/File;
    invoke-virtual/range {v40 .. v40}, Ljava/io/File;->exists()Z

    move-result v43

    if-eqz v43, :cond_e

    .line 297
    :try_start_18
    new-instance v35, Ljava/io/BufferedReader;

    new-instance v43, Ljava/io/InputStreamReader;

    new-instance v44, Ljava/io/FileInputStream;

    move-object/from16 v0, v44

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v45, "UTF-8"

    invoke-direct/range {v43 .. v45}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_13

    .line 300
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :goto_13
    :try_start_19
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v28

    .restart local v28    # "line":Ljava/lang/String;
    if-eqz v28, :cond_d

    .line 302
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v43

    invoke-interface/range {v42 .. v43}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_b

    goto :goto_13

    .line 305
    .end local v28    # "line":Ljava/lang/String;
    :catch_b
    move-exception v21

    move-object/from16 v34, v35

    .line 306
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v21    # "ex":Ljava/lang/Exception;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :goto_14
    const-string v43, "CscCompareService"

    const-string v44, "file error on /system/etc/vpl_apks_count_list.txt"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string v43, "CscCompareService : file error on /system/etc/vpl_apks_count_list.txt"

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 309
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    .line 311
    :try_start_1a
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedReader;->close()V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_e

    .line 315
    :goto_15
    const-string v43, "APPCOUNT FAIL"

    goto/16 :goto_0

    .line 286
    .end local v40    # "vplAppCountFile":Ljava/io/File;
    :catch_c
    move-exception v20

    .line 287
    .restart local v20    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_12

    .line 304
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v21    # "ex":Ljava/lang/Exception;
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v28    # "line":Ljava/lang/String;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v40    # "vplAppCountFile":Ljava/io/File;
    :cond_d
    :try_start_1b
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->close()V
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_b

    move-object/from16 v34, v35

    .line 320
    .end local v28    # "line":Ljava/lang/String;
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :cond_e
    new-instance v37, Ljava/io/File;

    const-string v43, "/system/etc/userdata_apks_count_list.txt"

    move-object/from16 v0, v37

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 321
    .local v37, "userAppCountFile":Ljava/io/File;
    invoke-virtual/range {v37 .. v37}, Ljava/io/File;->exists()Z

    move-result v43

    if-eqz v43, :cond_10

    .line 323
    :try_start_1c
    new-instance v35, Ljava/io/BufferedReader;

    new-instance v43, Ljava/io/InputStreamReader;

    new-instance v44, Ljava/io/FileInputStream;

    move-object/from16 v0, v44

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v45, "UTF-8"

    invoke-direct/range {v43 .. v45}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_12

    .line 326
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    :goto_16
    :try_start_1d
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v28

    .restart local v28    # "line":Ljava/lang/String;
    if-eqz v28, :cond_f

    .line 327
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v39

    move-object/from16 v1, v43

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_d

    goto :goto_16

    .line 330
    .end local v28    # "line":Ljava/lang/String;
    :catch_d
    move-exception v21

    move-object/from16 v34, v35

    .line 331
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v21    # "ex":Ljava/lang/Exception;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :goto_17
    const-string v43, "CscCompareService"

    const-string v44, "file error on /system/etc/userdata_apks_count_list.txt"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v43, "CscCompareService : file error on /system/etc/userdata_apks_count_list.txt"

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 334
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    .line 336
    :try_start_1e
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedReader;->close()V
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_f

    .line 340
    :goto_18
    const-string v43, "APPCOUNT FAIL"

    goto/16 :goto_0

    .line 312
    .end local v37    # "userAppCountFile":Ljava/io/File;
    :catch_e
    move-exception v20

    .line 313
    .restart local v20    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_15

    .line 329
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v21    # "ex":Ljava/lang/Exception;
    .end local v34    # "reader":Ljava/io/BufferedReader;
    .restart local v28    # "line":Ljava/lang/String;
    .restart local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v37    # "userAppCountFile":Ljava/io/File;
    :cond_f
    :try_start_1f
    invoke-virtual/range {v35 .. v35}, Ljava/io/BufferedReader;->close()V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_d

    move-object/from16 v34, v35

    .line 351
    .end local v28    # "line":Ljava/lang/String;
    .end local v35    # "reader":Ljava/io/BufferedReader;
    .restart local v34    # "reader":Ljava/io/BufferedReader;
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscPackageManager;->mContext:Landroid/content/Context;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v33

    .line 352
    .local v33, "pm":Landroid/content/pm/PackageManager;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscPackageManager;->GET_PACKAGE_FLAG:I

    move/from16 v43, v0

    move-object/from16 v0, v33

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v27

    .line 353
    .local v27, "installedPackages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 354
    .local v19, "currentPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 356
    .local v18, "currentDataPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .local v26, "i$":Ljava/util/Iterator;
    :cond_11
    :goto_19
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v43

    if-eqz v43, :cond_13

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Landroid/content/pm/PackageInfo;

    .line 358
    .local v31, "packageInfo":Landroid/content/pm/PackageInfo;
    const-string v43, "android"

    move-object/from16 v0, v31

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v44, v0

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-nez v43, :cond_11

    .line 361
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscPackageManager;->isSystemPackage(Landroid/content/pm/PackageInfo;)Z

    move-result v43

    if-eqz v43, :cond_12

    .line 364
    move-object/from16 v0, v31

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v43, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v43

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_19

    .line 337
    .end local v18    # "currentDataPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v19    # "currentPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v26    # "i$":Ljava/util/Iterator;
    .end local v27    # "installedPackages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v31    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v33    # "pm":Landroid/content/pm/PackageManager;
    .restart local v21    # "ex":Ljava/lang/Exception;
    :catch_f
    move-exception v20

    .line 338
    .restart local v20    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_18

    .line 368
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v21    # "ex":Ljava/lang/Exception;
    .restart local v18    # "currentDataPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v19    # "currentPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v26    # "i$":Ljava/util/Iterator;
    .restart local v27    # "installedPackages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v31    # "packageInfo":Landroid/content/pm/PackageInfo;
    .restart local v33    # "pm":Landroid/content/pm/PackageManager;
    :cond_12
    move-object/from16 v0, v31

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v43, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v43

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_19

    .line 373
    .end local v31    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_13
    const/16 v32, 0x0

    .line 374
    .local v32, "phoneutil":Landroid/content/pm/ApplicationInfo;
    const/16 v25, 0x0

    .line 376
    .local v25, "hiddenmenu":Landroid/content/pm/ApplicationInfo;
    :try_start_20
    const-string v43, "com.sec.android.app.phoneutil"

    const/16 v44, 0x0

    move-object/from16 v0, v33

    move-object/from16 v1, v43

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_20
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_20 .. :try_end_20} :catch_10

    move-result-object v32

    .line 381
    :goto_1a
    :try_start_21
    const-string v43, "com.android.hiddenmenu"

    const/16 v44, 0x0

    move-object/from16 v0, v33

    move-object/from16 v1, v43

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_21
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_21 .. :try_end_21} :catch_11

    move-result-object v25

    .line 386
    :goto_1b
    invoke-interface/range {v29 .. v29}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :cond_14
    :goto_1c
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v43

    if-eqz v43, :cond_17

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 387
    .local v4, "app":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v30

    .line 388
    .local v30, "ok":Z
    if-nez v30, :cond_14

    .line 389
    if-eqz v32, :cond_15

    move-object/from16 v0, v32

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-nez v43, :cond_14

    .line 391
    :cond_15
    if-eqz v25, :cond_16

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-nez v43, :cond_14

    .line 393
    :cond_16
    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v43

    if-nez v43, :cond_14

    .line 395
    const-string v43, "CscCompareService"

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "APPCOUNT FAIL:"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    const-string v43, ""

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_14

    .line 397
    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    const-string v44, "APPCOUNT FAIL:"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 398
    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    const-string v44, "CscCompareService : "

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    goto/16 :goto_1c

    .line 377
    .end local v4    # "app":Ljava/lang/String;
    .end local v30    # "ok":Z
    :catch_10
    move-exception v20

    .line 378
    .local v20, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v43, "CscCompareService"

    const-string v44, "There is no phoneutil"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1a

    .line 382
    .end local v20    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_11
    move-exception v20

    .line 383
    .restart local v20    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v43, "CscCompareService"

    const-string v44, "There is no hiddenmenu"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1b

    .line 404
    .end local v20    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_17
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :cond_18
    :goto_1d
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v43

    if-eqz v43, :cond_19

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 405
    .restart local v4    # "app":Ljava/lang/String;
    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v30

    .line 406
    .restart local v30    # "ok":Z
    if-nez v30, :cond_18

    .line 407
    const-string v43, "CscCompareService"

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "APPCOUNT FAIL:MORE:"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const-string v43, ""

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_18

    .line 409
    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    const-string v44, "APPCOUNT FAIL:MORE:"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 410
    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    const-string v44, "CscCompareService : "

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    goto :goto_1d

    .line 416
    .end local v4    # "app":Ljava/lang/String;
    .end local v30    # "ok":Z
    :cond_19
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v43

    invoke-interface/range {v42 .. v42}, Ljava/util/List;->size()I

    move-result v44

    add-int v43, v43, v44

    invoke-interface/range {v39 .. v39}, Ljava/util/List;->size()I

    move-result v44

    add-int v43, v43, v44

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v44

    add-int v43, v43, v44

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v44

    sub-int v43, v43, v44

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v44

    move/from16 v0, v43

    move/from16 v1, v44

    if-eq v0, v1, :cond_1a

    .line 417
    const-string v43, "CscCompareService"

    const-string v44, "APPCOUNT FAIL:HIDDEN"

    invoke-static/range {v43 .. v44}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    const-string v43, ""

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_1a

    .line 419
    const-string v3, "APPCOUNT FAIL:HIDDEN"

    .line 420
    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    const-string v44, "CscCompareService : "

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v43 .. v43}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 425
    :cond_1a
    const-string v43, ""

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-nez v43, :cond_1b

    move-object/from16 v43, v3

    .line 426
    goto/16 :goto_0

    .line 429
    :cond_1b
    const-string v43, "NOERROR"

    goto/16 :goto_0

    .line 330
    .end local v18    # "currentDataPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v19    # "currentPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v25    # "hiddenmenu":Landroid/content/pm/ApplicationInfo;
    .end local v26    # "i$":Ljava/util/Iterator;
    .end local v27    # "installedPackages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v32    # "phoneutil":Landroid/content/pm/ApplicationInfo;
    .end local v33    # "pm":Landroid/content/pm/PackageManager;
    :catch_12
    move-exception v21

    goto/16 :goto_17

    .line 305
    .end local v37    # "userAppCountFile":Ljava/io/File;
    :catch_13
    move-exception v21

    goto/16 :goto_14

    .line 279
    .end local v40    # "vplAppCountFile":Ljava/io/File;
    :catch_14
    move-exception v21

    goto/16 :goto_11

    .line 253
    .end local v22    # "hiddenAppCountFile":Ljava/io/File;
    :catch_15
    move-exception v21

    goto/16 :goto_e

    .line 227
    .end local v12    # "cscRemoveDataFile":Ljava/io/File;
    :catch_16
    move-exception v21

    goto/16 :goto_b

    .line 201
    .end local v7    # "cscAddDataFile":Ljava/io/File;
    :catch_17
    move-exception v21

    goto/16 :goto_8

    .line 175
    .end local v15    # "cscRemoveSystemFile":Ljava/io/File;
    :catch_18
    move-exception v21

    goto/16 :goto_5

    .line 150
    .end local v10    # "cscAddSystemFile":Ljava/io/File;
    :catch_19
    move-exception v21

    goto/16 :goto_2
.end method

.method public update()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method
