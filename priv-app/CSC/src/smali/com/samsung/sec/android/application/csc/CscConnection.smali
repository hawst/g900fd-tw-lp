.class public Lcom/samsung/sec/android/application/csc/CscConnection;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscConnection$1;,
        Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;,
        Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;,
        Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;,
        Lcom/samsung/sec/android/application/csc/CscConnection$Profile;,
        Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;,
        Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    }
.end annotation


# static fields
.field static final URI_CARRIERS:Landroid/net/Uri;

.field static final URI_DORMPOLICY:Landroid/net/Uri;

.field static final URI_NWKINFO:Landroid/net/Uri;

.field static final URI_RESTORE:Landroid/net/Uri;

.field static mUserApn:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final CUSTOMER_CSC_FILE:Ljava/lang/String;

.field private final LOG_TAG:Ljava/lang/String;

.field private final OTHERS_CSC_FILE:Ljava/lang/String;

.field private mCarriers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mDormProfiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mDormProfiles_Rel8:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mLastFailCause:Ljava/lang/String;

.field private mNwkInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPreferredApnName:Ljava/lang/String;

.field private mProfileHandles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;",
            ">;"
        }
    .end annotation
.end field

.field private mProfiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscConnection$Profile;",
            ">;"
        }
    .end annotation
.end field

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 359
    const-string v0, "content://nwkinfo/nwkinfo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_NWKINFO:Landroid/net/Uri;

    .line 361
    const-string v0, "content://nwkinfo/nwkinfo/dormpolicy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_DORMPOLICY:Landroid/net/Uri;

    .line 363
    const-string v0, "content://nwkinfo/nwkinfo/carriers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_CARRIERS:Landroid/net/Uri;

    .line 365
    const-string v0, "content://nwkinfo/nwkinfo/restore"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_RESTORE:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 401
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 230
    const-string v0, "CscConnection"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->LOG_TAG:Ljava/lang/String;

    .line 232
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    .line 234
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->OTHERS_CSC_FILE:Ljava/lang/String;

    .line 378
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 380
    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 399
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mPreferredApnName:Ljava/lang/String;

    .line 402
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mContext:Landroid/content/Context;

    .line 403
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    .line 406
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 408
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mNwkInfos:Ljava/util/ArrayList;

    .line 409
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mProfiles:Ljava/util/ArrayList;

    .line 410
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mProfileHandles:Ljava/util/ArrayList;

    .line 411
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mDormProfiles:Ljava/util/ArrayList;

    .line 412
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mDormProfiles_Rel8:Ljava/util/ArrayList;

    .line 413
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCarriers:Ljava/util/ArrayList;

    .line 414
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscConnection;->mUserApn:Ljava/util/ArrayList;

    .line 415
    return-void
.end method

.method private ConvertFromCarrierToContentValues(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "carrier":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;>;"
    const/4 v7, 0x3

    .line 2155
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2157
    .local v3, "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 2158
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;

    .line 2159
    .local v0, "c":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2160
    .local v4, "values":Landroid/content/ContentValues;
    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    .line 2161
    .local v2, "numeric":Ljava/lang/String;
    const-string v5, "name"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162
    const-string v5, "apn"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2163
    const-string v5, "proxy"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->proxy:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2164
    const-string v5, "port"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->port:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2165
    const-string v5, "mmsproxy"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsProxy:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2166
    const-string v5, "mmsport"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsPort:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2167
    const-string v5, "user"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2168
    const-string v5, "password"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2169
    const-string v5, "mmsc"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsc:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2170
    const-string v5, "mcc"

    const/4 v6, 0x0

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2171
    const-string v5, "mnc"

    invoke-virtual {v2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2172
    const-string v5, "numeric"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2173
    const-string v5, "authtype"

    iget v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->authType:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2174
    const-string v5, "protocol"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->protocol:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2175
    const-string v5, "roaming_protocol"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->roamingProtocol:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2176
    const-string v5, "type"

    iget-object v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->types:[Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2177
    const-string v5, "carrier_enabled"

    iget-boolean v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrierEnabled:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2178
    const-string v5, "bearer"

    iget v6, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->bearer:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2179
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2182
    .end local v0    # "c":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "numeric":Ljava/lang/String;
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_0
    const-string v5, "CscConnection"

    const-string v6, "Carrier is empty!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2183
    const/4 v3, 0x0

    .line 2185
    .end local v3    # "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_1
    return-object v3
.end method

.method private addProfileToCarriers(Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;Lcom/samsung/sec/android/application/csc/CscConnection$Profile;)V
    .locals 13
    .param p1, "nwkInfo"    # Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    .param p2, "profile"    # Lcom/samsung/sec/android/application/csc/CscConnection$Profile;

    .prologue
    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 1483
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;

    invoke-direct {v1, p0, v10}, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;-><init>(Lcom/samsung/sec/android/application/csc/CscConnection;Lcom/samsung/sec/android/application/csc/CscConnection$1;)V

    .line 1484
    .local v1, "carrierTypes":Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;
    const/4 v5, 0x0

    .line 1485
    .local v5, "profSuplExisted":Z
    const-string v8, "ro.csc.sales_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1488
    .local v7, "salesCode":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mProfileHandles:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;

    .line 1489
    .local v6, "profileHandle":Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;
    iget-object v8, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profSupl:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1490
    const/4 v5, 0x1

    .line 1495
    .end local v6    # "profileHandle":Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;
    :cond_1
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mProfileHandles:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;

    .line 1496
    .restart local v6    # "profileHandle":Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->nwkname:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    iget-object v8, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->nwkname:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->nwkname:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->nwkname:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1498
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1499
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profBrowser:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1500
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableDefault:Z

    .line 1503
    if-nez v5, :cond_3

    const-string v8, "ATT activation"

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profBrowser:Ljava/lang/String;

    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 1504
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableSupl:Z

    .line 1506
    :cond_3
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profMms:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1507
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableMms:Z

    .line 1509
    :cond_4
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profDun:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1510
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableDun:Z

    .line 1512
    :cond_5
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profSupl:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1513
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableSupl:Z

    .line 1515
    :cond_6
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profActiveSync:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1516
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableHipri:Z

    .line 1518
    :cond_7
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profCbs:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1519
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableCbs:Z

    .line 1521
    :cond_8
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profFota:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1522
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableFota:Z

    .line 1524
    :cond_9
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profIms:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1525
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableIms:Z

    .line 1527
    :cond_a
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profXcap:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1528
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableXcap:Z

    .line 1530
    :cond_b
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profEmail:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 1531
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableEmail:Z

    .line 1533
    :cond_c
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profSyncDM:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 1534
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableSyncDm:Z

    .line 1537
    :cond_d
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profWap:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 1538
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableWap:Z

    .line 1540
    :cond_e
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iget-object v9, v6, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profEpdg:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1541
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableMms:Z

    goto/16 :goto_0

    .line 1547
    .end local v6    # "profileHandle":Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;
    :cond_f
    const/4 v3, 0x0

    .line 1548
    .local v3, "isAlreadyExist":Z
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;

    invoke-direct {v4, p0, v10}, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;-><init>(Lcom/samsung/sec/android/application/csc/CscConnection;Lcom/samsung/sec/android/application/csc/CscConnection$1;)V

    .line 1554
    .local v4, "newCarrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    const-string v8, "CHN"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_28

    const-string v8, "CHM"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_28

    const-string v8, "CHC"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_28

    const-string v8, "CHU"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_28

    .line 1557
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v8

    const-string v9, "CscFeature_RIL_AllowDuplicatedApnName"

    invoke-virtual {v8, v9}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_11

    .line 1559
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCarriers:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_11

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;

    .line 1563
    .local v0, "carrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    const-string v8, "BRI"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_21

    const-string v8, "46689"

    iget-object v9, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_21

    .line 1597
    .end local v0    # "carrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    :cond_11
    :goto_1
    if-eqz v3, :cond_12

    .line 1598
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCarriers:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1601
    :cond_12
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 1602
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->nwkname:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    .line 1604
    :cond_13
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_14

    iget-boolean v8, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableDefault:Z

    if-eqz v8, :cond_15

    .line 1605
    :cond_14
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    .line 1608
    :cond_15
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_16

    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mPreferredApnName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 1610
    const-string v8, "CscConnection"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " mark it as preferred apn "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1611
    iput-boolean v11, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->preferred:Z

    .line 1614
    :cond_16
    const-string v8, "CscConnection"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " [newCarrier.carrier]: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "[mPreferredApnName]:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mPreferredApnName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1616
    const-string v8, "CscConnection"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " [newCarrier.preferred]: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->preferred:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1618
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_17

    .line 1619
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->apn:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    .line 1621
    :cond_17
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->authtype:Ljava/lang/String;

    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertAuthType(Ljava/lang/String;)I

    move-result v8

    iput v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->authType:I

    .line 1623
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->ipVersion:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertProtocolType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->protocol:Ljava/lang/String;

    .line 1625
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->roamingIpVersion:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertProtocolType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->roamingProtocol:Ljava/lang/String;

    .line 1627
    iget v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->bearer:I

    iput v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->bearer:I

    .line 1629
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_18

    .line 1630
    iget-object v8, p1, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    .line 1632
    :cond_18
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_19

    .line 1633
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->userId:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    .line 1635
    :cond_19
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1a

    .line 1636
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->passwd:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    .line 1638
    :cond_1a
    iget-boolean v8, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableMms:Z

    if-eqz v8, :cond_29

    .line 1639
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsProxy:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1b

    .line 1640
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->proxy:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsProxy:Ljava/lang/String;

    .line 1642
    :cond_1b
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsPort:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1c

    .line 1643
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->port:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsPort:Ljava/lang/String;

    .line 1645
    :cond_1c
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsc:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1d

    .line 1646
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->url:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsc:Ljava/lang/String;

    .line 1660
    :cond_1d
    :goto_2
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->enableStatus:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2c

    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->enableStatus:Ljava/lang/String;

    const-string v9, "disable"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2c

    .line 1661
    iput-boolean v12, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrierEnabled:Z

    .line 1665
    :goto_3
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->roamingEnableStatus:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2d

    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->roamingEnableStatus:Ljava/lang/String;

    const-string v9, "enable"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2d

    .line 1667
    iput-boolean v11, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrierEnabledRoaming:Z

    .line 1672
    :goto_4
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->editable:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2e

    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->editable:Ljava/lang/String;

    const-string v9, "disable"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1e

    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->editable:Ljava/lang/String;

    const-string v9, "no"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2e

    .line 1674
    :cond_1e
    iput-boolean v12, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->editable:Z

    .line 1680
    :goto_5
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->types:[Ljava/lang/String;

    if-eqz v8, :cond_1f

    .line 1681
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->types:[Ljava/lang/String;

    invoke-virtual {p0, v1, v8}, Lcom/samsung/sec/android/application/csc/CscConnection;->markCarrierTypes(Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;[Ljava/lang/String;)V

    .line 1685
    :cond_1f
    invoke-virtual {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertTypes(Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;)[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscConnection;->hasType([Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_20

    .line 1686
    iput-boolean v11, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableDefault:Z

    .line 1689
    :cond_20
    invoke-virtual {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertTypes(Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;)[Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->types:[Ljava/lang/String;

    .line 1690
    iget v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->mtu:I

    iput v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mtu:I

    .line 1693
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCarriers:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1694
    return-void

    .line 1568
    .restart local v0    # "carrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    :cond_21
    iget-boolean v8, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableCbs:Z

    if-eqz v8, :cond_22

    const-string v8, "cbs"

    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscConnection;->IncludeFixedApn(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_11

    :cond_22
    iget-boolean v8, v1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableXcap:Z

    if-eqz v8, :cond_23

    const-string v8, "xcap"

    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscConnection;->IncludeFixedApn(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_11

    .line 1572
    :cond_23
    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_10

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    iget-object v9, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->apn:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_10

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    iget-object v9, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->nwkname:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_10

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    iget-object v9, p1, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->protocol:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_10

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->protocol:Ljava/lang/String;

    iget-object v9, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->ipVersion:Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertProtocolType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->roamingProtocol:Ljava/lang/String;

    iget-object v9, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->roamingIpVersion:Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertProtocolType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_24

    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->userId:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_25

    :cond_24
    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_10

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    iget-object v9, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->userId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    :cond_25
    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_26

    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->passwd:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_27

    :cond_26
    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_10

    iget-object v8, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    iget-object v9, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->passwd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 1588
    :cond_27
    move-object v4, v0

    .line 1589
    const/4 v3, 0x1

    .line 1590
    goto/16 :goto_1

    .line 1595
    .end local v0    # "carrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    :cond_28
    const-string v8, "CscConnection"

    const-string v9, "China feature"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1648
    :cond_29
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->proxy:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2a

    .line 1649
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->proxy:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->proxy:Ljava/lang/String;

    .line 1651
    :cond_2a
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->port:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2b

    .line 1652
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->port:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->port:Ljava/lang/String;

    .line 1654
    :cond_2b
    iget-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->homeurl:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1d

    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->url:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1d

    .line 1655
    iget-object v8, p2, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->url:Ljava/lang/String;

    iput-object v8, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->homeurl:Ljava/lang/String;

    goto/16 :goto_2

    .line 1663
    :cond_2c
    iput-boolean v11, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrierEnabled:Z

    goto/16 :goto_3

    .line 1669
    :cond_2d
    iput-boolean v12, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrierEnabledRoaming:Z

    goto/16 :goto_4

    .line 1676
    :cond_2e
    iput-boolean v11, v4, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->editable:Z

    goto/16 :goto_5
.end method

.method private checkNotSet(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1949
    if-nez p1, :cond_0

    .line 1950
    const-string p1, ""

    .line 1952
    .end local p1    # "value":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method private checkNotSetToZero(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1956
    if-nez p1, :cond_0

    .line 1957
    const-string p1, "0"

    .line 1959
    .end local p1    # "value":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method private convertBearer(Ljava/lang/String;)I
    .locals 2
    .param p1, "bearer"    # Ljava/lang/String;

    .prologue
    .line 580
    const/4 v0, 0x0

    .line 581
    .local v0, "ret":I
    const-string v1, "gprs"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 582
    const/4 v0, 0x1

    .line 626
    :goto_0
    return v0

    .line 583
    :cond_0
    const-string v1, "edge"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 584
    const/4 v0, 0x2

    goto :goto_0

    .line 585
    :cond_1
    const-string v1, "umts"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 586
    const/4 v0, 0x3

    goto :goto_0

    .line 587
    :cond_2
    const-string v1, "is95a"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 588
    const/4 v0, 0x4

    goto :goto_0

    .line 589
    :cond_3
    const-string v1, "is95b"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 590
    const/4 v0, 0x5

    goto :goto_0

    .line 591
    :cond_4
    const-string v1, "1xrtt"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 592
    const/4 v0, 0x6

    goto :goto_0

    .line 593
    :cond_5
    const-string v1, "evdo_0"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 594
    const/4 v0, 0x7

    goto :goto_0

    .line 595
    :cond_6
    const-string v1, "evdo_a"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 596
    const/16 v0, 0x8

    goto :goto_0

    .line 597
    :cond_7
    const-string v1, "hsdpa"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 598
    const/16 v0, 0x9

    goto :goto_0

    .line 599
    :cond_8
    const-string v1, "hsupa"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 600
    const/16 v0, 0xa

    goto :goto_0

    .line 601
    :cond_9
    const-string v1, "hspa"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 602
    const/16 v0, 0xb

    goto :goto_0

    .line 603
    :cond_a
    const-string v1, "evdo_b"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 604
    const/16 v0, 0xc

    goto :goto_0

    .line 605
    :cond_b
    const-string v1, "ehrpd"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 606
    const/16 v0, 0xd

    goto :goto_0

    .line 607
    :cond_c
    const-string v1, "lte"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 608
    const/16 v0, 0xe

    goto/16 :goto_0

    .line 609
    :cond_d
    const-string v1, "hspap"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 610
    const/16 v0, 0xf

    goto/16 :goto_0

    .line 611
    :cond_e
    const-string v1, "gsm"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 612
    const/16 v0, 0x10

    goto/16 :goto_0

    .line 613
    :cond_f
    const-string v1, "iwlan"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 614
    const/16 v0, 0x12

    goto/16 :goto_0

    .line 624
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private convertProtocolType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 1935
    if-nez p1, :cond_0

    .line 1936
    const-string v0, "IP"

    .line 1945
    :goto_0
    return-object v0

    .line 1938
    :cond_0
    const-string v0, "ipv4"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1939
    const-string v0, "IP"

    goto :goto_0

    .line 1940
    :cond_1
    const-string v0, "ipv6"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1941
    const-string v0, "IPV6"

    goto :goto_0

    .line 1942
    :cond_2
    const-string v0, "ipv4v6"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1943
    const-string v0, "IPV4V6"

    goto :goto_0

    .line 1945
    :cond_3
    const-string v0, "IP"

    goto :goto_0
.end method

.method private createCarriers()V
    .locals 6

    .prologue
    .line 1453
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCarriers:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1455
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mNwkInfos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;

    .line 1456
    .local v2, "nwkInfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mProfiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;

    .line 1457
    .local v3, "profile":Lcom/samsung/sec/android/application/csc/CscConnection$Profile;
    iget-object v4, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->nwkname:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->nwkname:Ljava/lang/String;

    iget-object v5, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1459
    invoke-direct {p0, v2, v3}, Lcom/samsung/sec/android/application/csc/CscConnection;->addProfileToCarriers(Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;Lcom/samsung/sec/android/application/csc/CscConnection$Profile;)V

    goto :goto_0

    .line 1463
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "nwkInfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    .end local v3    # "profile":Lcom/samsung/sec/android/application/csc/CscConnection$Profile;
    :cond_2
    return-void
.end method

.method private doActionCarrierTable(I)Z
    .locals 7
    .param p1, "action"    # I

    .prologue
    const/4 v6, 0x0

    .line 1394
    const/4 v2, 0x1

    .line 1395
    .local v2, "result":Z
    const/4 v3, 0x1

    .line 1397
    .local v3, "temp_result":Z
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCarriers:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;

    .line 1398
    .local v0, "carrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    if-nez p1, :cond_1

    .line 1399
    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscConnection;->insertCarrierTable(Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;)Z

    move-result v2

    goto :goto_0

    .line 1400
    :cond_1
    const/4 v4, 0x1

    if-ne p1, v4, :cond_0

    .line 1401
    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscConnection;->validateCarrierTable(Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;)Z

    move-result v3

    .line 1402
    const-string v4, "Settings.Connections.Profile.NetworkName"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1404
    const-string v4, "Settings.Connections.Profile.Editable"

    iget-boolean v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->editable:Z

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1406
    const-string v4, "Settings.Connections.Profile.EnableStatus"

    iget-boolean v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrierEnabled:Z

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    const-string v4, "Settings.Connections.Profile.ProfileName"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1412
    const-string v4, "Settings.Connections.Profile.URL"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->homeurl:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1414
    const-string v4, "Settings.Connections.MMS.URL"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsc:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1416
    const-string v4, "Settings.Connections.Profile.Auth"

    iget v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->authType:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    const-string v4, "Settings.Connections.Profile.Protocol"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->protocol:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1420
    const-string v4, "Settings.Connections.Profile.Proxy.ServAddr"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->proxy:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1422
    const-string v4, "Settings.Connections.Profile.Proxy.Port"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->port:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    const-string v4, "Settings.Connections.Profile.MMSProxy.ServAddr"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsProxy:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1426
    const-string v4, "Settings.Connections.Profile.MMSProxy.Port"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsPort:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1428
    const-string v4, "Settings.Connections.Profile.PSparam.APN"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1430
    const-string v4, "Settings.Connections.Profile.PSparam.UserId"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1432
    const-string v4, "Settings.Connections.Profile.PSparam.Password"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    const-string v4, "Settings.Connections.Profile.IpVersion"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->protocol:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    const-string v4, "Settings.Connections.Profile.RoamingIpVersion"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->roamingProtocol:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1438
    const-string v4, "Settings.Connections.ProfileHandle"

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->types:[Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    const-string v4, "Settings.Connections.Profile.MTUSize"

    iget v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mtu:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1442
    if-nez v3, :cond_0

    .line 1443
    move v2, v3

    goto/16 :goto_0

    .line 1449
    .end local v0    # "carrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    :cond_2
    return v2
.end method

.method private doActionDormPolicyTable(I)Z
    .locals 20
    .param p1, "action"    # I

    .prologue
    .line 1202
    const/16 v18, 0x1

    .line 1203
    .local v18, "result":Z
    const/16 v19, 0x1

    .line 1205
    .local v19, "temp_result":Z
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscConnection;->mNwkInfos:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;

    .line 1206
    .local v17, "nwkInfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    const-string v4, "5"

    .line 1207
    .local v4, "lcdonfdtime":Ljava/lang/String;
    const-string v5, "5"

    .line 1208
    .local v5, "lcdofffdtime":Ljava/lang/String;
    const-string v6, "-1"

    .line 1209
    .local v6, "lcdonfdtime_Rel8":Ljava/lang/String;
    const-string v7, "-1"

    .line 1210
    .local v7, "lcdofffdtime_Rel8":Ljava/lang/String;
    const/4 v9, 0x0

    .line 1212
    .local v9, "dormpolicyExist":Z
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscConnection;->mDormProfiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;

    .line 1213
    .local v8, "dormProfile":Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;
    move-object/from16 v0, v17

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->nwkname:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v0, v17

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->nwkname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1215
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "on"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1216
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "on"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1218
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1219
    iget-object v4, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    .line 1241
    :cond_2
    :goto_2
    const/4 v9, 0x1

    goto :goto_1

    .line 1220
    :cond_3
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "off"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1222
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1223
    iget-object v5, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    goto :goto_2

    .line 1225
    :cond_4
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "off"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1227
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "on"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1229
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1230
    const-string v4, "0"

    goto :goto_2

    .line 1231
    :cond_5
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "off"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1233
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1234
    const-string v5, "0"

    goto :goto_2

    .line 1236
    :cond_6
    const-string v4, "0"

    .line 1237
    const-string v5, "0"

    goto :goto_2

    .line 1246
    .end local v8    # "dormProfile":Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscConnection;->mDormProfiles_Rel8:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_8
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;

    .line 1247
    .restart local v8    # "dormProfile":Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;
    move-object/from16 v0, v17

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->nwkname:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    move-object/from16 v0, v17

    iget-object v1, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->nwkname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1249
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "on"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1250
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "on"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1252
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1253
    iget-object v6, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    goto :goto_3

    .line 1254
    :cond_9
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "off"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1256
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1257
    iget-object v7, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    goto :goto_3

    .line 1259
    :cond_a
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "off"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1261
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "on"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1263
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1264
    const-string v6, "0"

    goto/16 :goto_3

    .line 1265
    :cond_b
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "off"

    iget-object v2, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1267
    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1268
    const-string v7, "0"

    goto/16 :goto_3

    .line 1274
    .end local v8    # "dormProfile":Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;
    :cond_c
    if-eqz v9, :cond_0

    .line 1275
    if-nez p1, :cond_d

    .line 1276
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/samsung/sec/android/application/csc/CscConnection;->insertDormPolicyTable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v18

    goto/16 :goto_0

    .line 1278
    :cond_d
    const/4 v1, 0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_0

    .line 1279
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/samsung/sec/android/application/csc/CscConnection;->validateDormPolicyTable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v19

    .line 1281
    const/4 v15, 0x0

    .local v15, "nLcdonfdtime":I
    const/4 v13, 0x0

    .local v13, "nLcdofffdtime":I
    const/16 v16, 0x0

    .local v16, "nLcdonfdtime8":I
    const/4 v14, 0x0

    .line 1283
    .local v14, "nLcdofffdtime8":I
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 1284
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 1285
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 1286
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v14

    .line 1293
    :goto_4
    const-string v1, "Settings.Main.Network.FastDormancy.NetworkName"

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1295
    const-string v1, "Settings.Main.Network.FastDormancy.TimeoutValue"

    const/4 v2, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v4, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1297
    const-string v1, "Settings.Main.Network.FastDormancy.LCDStatus"

    const-string v2, "on"

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1299
    const-string v1, "Settings.Main.Network.FastDormancy.Version"

    const-string v2, "pre-rel8"

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1301
    const-string v1, "Settings.Main.Network.FastDormancy.NetworkName"

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1303
    const-string v1, "Settings.Main.Network.FastDormancy.TimeoutValue"

    const/4 v2, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v5, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1305
    const-string v1, "Settings.Main.Network.FastDormancy.LCDStatus"

    const-string v2, "off"

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1307
    const-string v1, "Settings.Main.Network.FastDormancy.Version"

    const-string v2, "pre-rel8"

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1309
    const-string v1, "Settings.Main.Network.FastDormancyRel8.NetworkName"

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1311
    const-string v1, "Settings.Main.Network.FastDormancyRel8.TimeoutValue"

    const/4 v2, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v6, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1313
    const-string v1, "Settings.Main.Network.FastDormancyRel8.LCDStatus"

    const-string v2, "on"

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1315
    const-string v1, "Settings.Main.Network.FastDormancyRel8.Version"

    const-string v2, "rel8"

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1317
    const-string v1, "Settings.Main.Network.FastDormancyRel8.NetworkName"

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1319
    const-string v1, "Settings.Main.Network.FastDormancyRel8.TimeoutValue"

    const/4 v2, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v6, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1321
    const-string v1, "Settings.Main.Network.FastDormancyRel8.LCDStatus"

    const-string v2, "off"

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1323
    const-string v1, "Settings.Main.Network.FastDormancyRel8.Version"

    const-string v2, "rel8"

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1325
    if-nez v19, :cond_0

    .line 1326
    move/from16 v18, v19

    goto/16 :goto_0

    .line 1287
    :catch_0
    move-exception v10

    .line 1288
    .local v10, "e":Ljava/lang/NumberFormatException;
    const-string v1, "CscConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error during parsing fd time for CscVerify. lcdonfdtime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",lcdofffdtime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",lcdonfdtime_Rel8="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",lcdofffdtime_Rel8="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1333
    .end local v4    # "lcdonfdtime":Ljava/lang/String;
    .end local v5    # "lcdofffdtime":Ljava/lang/String;
    .end local v6    # "lcdonfdtime_Rel8":Ljava/lang/String;
    .end local v7    # "lcdofffdtime_Rel8":Ljava/lang/String;
    .end local v9    # "dormpolicyExist":Z
    .end local v10    # "e":Ljava/lang/NumberFormatException;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "nLcdofffdtime":I
    .end local v14    # "nLcdofffdtime8":I
    .end local v15    # "nLcdonfdtime":I
    .end local v16    # "nLcdonfdtime8":I
    .end local v17    # "nwkInfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    :cond_e
    return v18
.end method

.method private doActionNwkInfoTable(I)Z
    .locals 14
    .param p1, "action"    # I

    .prologue
    .line 1088
    const/4 v12, 0x1

    .line 1089
    .local v12, "result":Z
    const/4 v13, 0x1

    .line 1091
    .local v13, "temp_result":Z
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mNwkInfos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;

    .line 1092
    .local v11, "nwkInfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    const-string v3, "on"

    .line 1094
    .local v3, "dormancy":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mDormProfiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;

    .line 1095
    .local v8, "dormProfile":Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;
    iget-object v0, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->nwkname:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    iget-object v1, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->nwkname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1097
    iget-object v0, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1098
    iget-object v3, v8, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    goto :goto_1

    .line 1102
    .end local v8    # "dormProfile":Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;
    :cond_2
    if-nez p1, :cond_3

    .line 1103
    iget-object v1, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    iget-object v2, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    iget-object v0, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscConnection;->getMTUsize(Ljava/lang/String;)I

    move-result v4

    iget-object v5, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->subsetcode:Ljava/lang/String;

    iget-object v6, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spcode:Ljava/lang/String;

    iget-object v7, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spname:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/samsung/sec/android/application/csc/CscConnection;->insertNwkInfoTable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    goto :goto_0

    .line 1106
    :cond_3
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1107
    iget-object v1, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    iget-object v2, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    iget-object v0, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscConnection;->getMTUsize(Ljava/lang/String;)I

    move-result v4

    iget-object v5, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->subsetcode:Ljava/lang/String;

    iget-object v6, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spcode:Ljava/lang/String;

    iget-object v7, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spname:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/samsung/sec/android/application/csc/CscConnection;->validateNwkInfoTable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    .line 1110
    const-string v0, "GeneralInfo.NetworkInfo.MCCMNC"

    iget-object v1, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v13, v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    const-string v0, "GeneralInfo.NetworkInfo.NetworkName"

    iget-object v1, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v13, v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1114
    const-string v0, "GeneralInfo.NetworkInfo.SubsetCode"

    iget-object v1, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->subsetcode:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v13, v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    const-string v0, "GeneralInfo.NetworkInfo.SPCode"

    iget-object v1, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spcode:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v13, v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1118
    const-string v0, "GeneralInfo.NetworkInfo.SPName"

    iget-object v1, v11, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spname:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v13, v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1120
    const-string v0, "Settings.Main.Network.FastDormancy.FastDormancyEnableStatus"

    const/4 v1, 0x0

    invoke-static {v13, v0, v3, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->log(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    if-nez v13, :cond_0

    .line 1123
    move v12, v13

    goto/16 :goto_0

    .line 1128
    .end local v3    # "dormancy":Ljava/lang/String;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "nwkInfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    :cond_4
    return v12
.end method

.method private getCarriersFromDB(Landroid/net/Uri;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;

    .prologue
    .line 2146
    :try_start_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 2151
    .local v6, "cursor":Landroid/database/Cursor;
    :goto_0
    return-object v6

    .line 2147
    .end local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 2148
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "CscConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception caught during nwkinfo query(getCarriersFromDB): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2149
    const/4 v6, 0x0

    .restart local v6    # "cursor":Landroid/database/Cursor;
    goto :goto_0
.end method

.method private getMTUsize(Ljava/lang/String;)I
    .locals 4
    .param p1, "nwkname"    # Ljava/lang/String;

    .prologue
    .line 1133
    const/16 v2, 0x5dc

    .line 1135
    .local v2, "mtu":I
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCarriers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;

    .line 1136
    .local v0, "carrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    iget-object v3, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1137
    iget v3, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mtu:I

    if-lez v3, :cond_1

    .line 1138
    iget v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mtu:I

    .line 1142
    .end local v0    # "carrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    :cond_1
    return v2
.end method

.method private insertCarrierTable(Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;)Z
    .locals 10
    .param p1, "carrier"    # Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1697
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1699
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "name"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1700
    const-string v5, "apn"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1701
    const-string v5, "proxy"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->proxy:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1702
    const-string v5, "port"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->port:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1703
    const-string v5, "mmsproxy"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsProxy:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1704
    const-string v5, "mmsport"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsPort:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1705
    const-string v5, "user"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1706
    const-string v5, "password"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1707
    const-string v5, "mmsc"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsc:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1708
    const-string v5, "mcc"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    invoke-virtual {v6, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1709
    const-string v5, "mnc"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1710
    const-string v5, "numeric"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1711
    const-string v5, "authtype"

    iget v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->authType:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1712
    const-string v5, "protocol"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->protocol:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1713
    const-string v5, "roaming_protocol"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->roamingProtocol:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1714
    const-string v5, "type"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->types:[Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1715
    const-string v5, "carrier_enabled"

    iget-boolean v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrierEnabled:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1716
    const-string v5, "carrier_enabled_roaming"

    iget-boolean v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrierEnabledRoaming:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1717
    const-string v5, "bearer"

    iget v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->bearer:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1718
    const-string v5, "mtu"

    iget v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mtu:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1719
    const-string v5, "profile_id"

    iget v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->profileId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1720
    const-string v5, "modem_cognitive"

    iget v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->modemCognitive:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1721
    const-string v5, "max_conns"

    iget v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->maxConns:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1722
    const-string v5, "wait_time"

    iget v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->waitTime:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1723
    const-string v5, "max_conns_time"

    iget v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->maxConnsTime:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1724
    const-string v5, "mvno_type"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mvnoType:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1725
    const-string v5, "mvno_match_data"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mvnoMatchData:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1726
    const-string v5, "homeurl"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->homeurl:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1727
    const-string v5, "nwkname"

    iget-object v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1728
    const-string v5, "preferred"

    iget-boolean v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->preferred:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1729
    const-string v5, "editable"

    iget-boolean v6, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->editable:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1731
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v5

    const-string v6, "CscFeature_RIL_ConfigApnServerInfo"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1734
    .local v2, "serverInfo":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1735
    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1736
    .local v3, "str":[Ljava/lang/String;
    const-string v0, ""

    .line 1737
    .local v0, "apn":Ljava/lang/String;
    const-string v1, ""

    .line 1739
    .local v1, "server":Ljava/lang/String;
    array-length v5, v3

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 1740
    aget-object v5, v3, v7

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1741
    aget-object v5, v3, v8

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1742
    const-string v5, "CscConnection"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "LSM : apn = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " server = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " carrier.apn = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1746
    :cond_0
    iget-object v5, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1747
    const-string v5, "server"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1750
    .end local v0    # "apn":Ljava/lang/String;
    .end local v1    # "server":Ljava/lang/String;
    .end local v3    # "str":[Ljava/lang/String;
    :cond_1
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_CARRIERS:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1751
    return v8
.end method

.method private insertDormPolicyTable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "plmn"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "lcdonfdtime"    # Ljava/lang/String;
    .param p4, "lcdofffdtime"    # Ljava/lang/String;
    .param p5, "lcdonfdtime_Rel8"    # Ljava/lang/String;
    .param p6, "lcdofffdtime_Rel8"    # Ljava/lang/String;

    .prologue
    .line 1340
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1342
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "plmn"

    invoke-direct {p0, p1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1343
    const-string v2, "nwkname"

    invoke-direct {p0, p2}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1344
    const-string v2, "lcdonfdtime"

    invoke-direct {p0, p3}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1345
    const-string v2, "lcdofffdtime"

    invoke-direct {p0, p4}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1346
    const-string v2, "lcdonfdtime_Rel8"

    invoke-direct {p0, p5}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1347
    const-string v2, "lcdofffdtime_Rel8"

    invoke-direct {p0, p6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1350
    :try_start_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_DORMPOLICY:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1356
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 1351
    :catch_0
    move-exception v0

    .line 1352
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "CscConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception caught during inserting network info: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1353
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private insertNwkInfoTable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "plmn"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "dormancy"    # Ljava/lang/String;
    .param p4, "mtu"    # I
    .param p5, "subsetcode"    # Ljava/lang/String;
    .param p6, "spcode"    # Ljava/lang/String;
    .param p7, "spname"    # Ljava/lang/String;

    .prologue
    .line 1147
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1148
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "plmn"

    invoke-direct {p0, p1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149
    const-string v2, "nwkname"

    invoke-direct {p0, p2}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    const-string v2, "dormancy"

    invoke-direct {p0, p3}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1151
    const-string v2, "mtu"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1152
    const-string v2, "subsetcode"

    invoke-direct {p0, p5}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    const-string v2, "spcode"

    invoke-direct {p0, p6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1154
    const-string v2, "spname"

    invoke-direct {p0, p7}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    :try_start_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_NWKINFO:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1163
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 1158
    :catch_0
    move-exception v0

    .line 1159
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "CscConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception caught during inserting network info: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1160
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private parse()V
    .locals 2

    .prologue
    .line 632
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v0, :cond_0

    .line 633
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 634
    :cond_0
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->parseNwkInfo()I

    .line 635
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->parseProfile()I

    .line 636
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->parseProfileHandle()I

    .line 637
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->parseDormProfile()I

    .line 638
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->parseDormProfile_Rel8()I

    .line 642
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v0, :cond_1

    .line 643
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 644
    :cond_1
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->parsePreferredApn()I

    .line 645
    return-void
.end method

.method private parseDormProfile()I
    .locals 9

    .prologue
    const/4 v6, -0x1

    .line 824
    const-string v7, "CscConnection"

    const-string v8, "parseDormProfile"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "Settings.Main.Network"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 827
    .local v3, "node":Lorg/w3c/dom/Node;
    if-nez v3, :cond_0

    .line 828
    const-string v7, "CscConnection"

    const-string v8, "no node"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    :goto_0
    return v6

    .line 832
    :cond_0
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "FastDormancy"

    invoke-virtual {v7, v3, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 833
    .local v4, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v4, :cond_1

    .line 834
    const-string v7, "CscConnection"

    const-string v8, "no nodelist"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 838
    :cond_1
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mDormProfiles:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 839
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mDormProfiles_Rel8:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 840
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    if-ge v2, v6, :cond_9

    .line 841
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;

    const/4 v6, 0x0

    invoke-direct {v1, p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;-><init>(Lcom/samsung/sec/android/application/csc/CscConnection;Lcom/samsung/sec/android/application/csc/CscConnection$1;)V

    .line 844
    .local v1, "dormProfile":Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v4, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "NetworkName"

    invoke-virtual {v6, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 845
    .local v5, "tempNode":Lorg/w3c/dom/Node;
    if-eqz v5, :cond_2

    .line 846
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->nwkname:Ljava/lang/String;

    .line 848
    :cond_2
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v4, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "FastDormancyEnableStatus"

    invoke-virtual {v6, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 849
    if-eqz v5, :cond_3

    .line 850
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    .line 852
    :cond_3
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v4, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "TimeoutValue"

    invoke-virtual {v6, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 853
    if-eqz v5, :cond_6

    .line 854
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    .line 862
    :goto_2
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v4, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "LCDStatus"

    invoke-virtual {v6, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 863
    if-eqz v5, :cond_4

    .line 864
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    .line 866
    :cond_4
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v4, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "Version"

    invoke-virtual {v6, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 867
    const/4 v0, 0x0

    .line 868
    .local v0, "FDversion":Ljava/lang/String;
    if-eqz v5, :cond_5

    .line 869
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    .line 871
    :cond_5
    const-string v6, "rel8"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 872
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mDormProfiles_Rel8:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 840
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 856
    .end local v0    # "FDversion":Ljava/lang/String;
    :cond_6
    iget-object v6, v1, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    iget-object v6, v1, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    const-string v7, "off"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 857
    const-string v6, "0"

    iput-object v6, v1, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    goto :goto_2

    .line 859
    :cond_7
    const-string v6, "5"

    iput-object v6, v1, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    goto :goto_2

    .line 874
    .restart local v0    # "FDversion":Ljava/lang/String;
    :cond_8
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mDormProfiles:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 881
    .end local v0    # "FDversion":Ljava/lang/String;
    .end local v1    # "dormProfile":Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;
    .end local v5    # "tempNode":Lorg/w3c/dom/Node;
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method private parseDormProfile_Rel8()I
    .locals 8

    .prologue
    const/4 v5, -0x1

    .line 888
    const-string v6, "CscConnection"

    const-string v7, "parseDormProfile_Rel8"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 890
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "Settings.Main.Network"

    invoke-virtual {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 891
    .local v2, "node":Lorg/w3c/dom/Node;
    if-nez v2, :cond_0

    .line 892
    const-string v6, "CscConnection"

    const-string v7, "no node"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    :goto_0
    return v5

    .line 896
    :cond_0
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "FastDormancyRel8"

    invoke-virtual {v6, v2, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 897
    .local v3, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v3, :cond_1

    .line 898
    const-string v6, "CscConnection"

    const-string v7, "no nodelist"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 903
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v1, v5, :cond_7

    .line 904
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;

    const/4 v5, 0x0

    invoke-direct {v0, p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;-><init>(Lcom/samsung/sec/android/application/csc/CscConnection;Lcom/samsung/sec/android/application/csc/CscConnection$1;)V

    .line 907
    .local v0, "dormProfile":Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v3, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "NetworkName"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 908
    .local v4, "tempNode":Lorg/w3c/dom/Node;
    if-eqz v4, :cond_2

    .line 909
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->nwkname:Ljava/lang/String;

    .line 911
    :cond_2
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v3, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "FastDormancyEnableStatus"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 912
    if-eqz v4, :cond_3

    .line 913
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    .line 915
    :cond_3
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v3, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "TimeoutValue"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 916
    if-eqz v4, :cond_5

    .line 917
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    .line 925
    :goto_2
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v3, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "LCDStatus"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 926
    if-eqz v4, :cond_4

    .line 927
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->lcdStatus:Ljava/lang/String;

    .line 933
    :cond_4
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mDormProfiles_Rel8:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 903
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 919
    :cond_5
    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->enable:Ljava/lang/String;

    const-string v6, "off"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 920
    const-string v5, "0"

    iput-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    goto :goto_2

    .line 922
    :cond_6
    const-string v5, "5"

    iput-object v5, v0, Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;->timeout:Ljava/lang/String;

    goto :goto_2

    .line 935
    .end local v0    # "dormProfile":Lcom/samsung/sec/android/application/csc/CscConnection$DormancyProfile;
    .end local v4    # "tempNode":Lorg/w3c/dom/Node;
    :cond_7
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method private parseNwkInfo()I
    .locals 8

    .prologue
    const/4 v5, -0x1

    .line 651
    const-string v6, "CscConnection"

    const-string v7, "parseNwkInfo"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v6, :cond_0

    .line 653
    new-instance v6, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 655
    :cond_0
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "GeneralInfo"

    invoke-virtual {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 656
    .local v1, "node":Lorg/w3c/dom/Node;
    if-nez v1, :cond_2

    .line 697
    :cond_1
    :goto_0
    return v5

    .line 659
    :cond_2
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "NetworkInfo"

    invoke-virtual {v6, v1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 660
    .local v2, "nodeList":Lorg/w3c/dom/NodeList;
    if-eqz v2, :cond_1

    .line 663
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mNwkInfos:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 665
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v0, v5, :cond_8

    .line 666
    new-instance v3, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;

    const/4 v5, 0x0

    invoke-direct {v3, p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;-><init>(Lcom/samsung/sec/android/application/csc/CscConnection;Lcom/samsung/sec/android/application/csc/CscConnection$1;)V

    .line 669
    .local v3, "nwkInfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "NetworkName"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 670
    .local v4, "tempNode":Lorg/w3c/dom/Node;
    if-eqz v4, :cond_3

    .line 671
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    .line 673
    :cond_3
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "MCCMNC"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 674
    if-eqz v4, :cond_4

    .line 675
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    .line 677
    :cond_4
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "SPCode"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 678
    if-eqz v4, :cond_5

    .line 679
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->subsetcode:Ljava/lang/String;

    .line 682
    :cond_5
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "SubsetCode"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 683
    if-eqz v4, :cond_6

    .line 684
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spcode:Ljava/lang/String;

    .line 686
    :cond_6
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "SPName"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 687
    if-eqz v4, :cond_7

    .line 688
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spname:Ljava/lang/String;

    .line 691
    :cond_7
    const-string v5, "CscConnection"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[NwkInfo] [nwkname : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] [plmn : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] [subsetcode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->subsetcode:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] [spcode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spcode:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] [spname : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spname:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mNwkInfos:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 665
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 697
    .end local v3    # "nwkInfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    .end local v4    # "tempNode":Lorg/w3c/dom/Node;
    :cond_8
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method private parsePreferredApn()I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 1069
    const-string v2, "CscConnection"

    const-string v3, "parsePreferredApn"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1071
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v2, :cond_0

    .line 1072
    const-string v2, "CscConnection"

    const-string v3, "no othersparser in preferred apn"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    :goto_0
    return v1

    .line 1075
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "DefaultApn.ProfileName"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1077
    .local v0, "node":Lorg/w3c/dom/Node;
    if-nez v0, :cond_1

    .line 1078
    const-string v2, "CscConnection"

    const-string v3, "no node"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1082
    :cond_1
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v1, v0}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mPreferredApnName:Ljava/lang/String;

    .line 1083
    const-string v1, "CscConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mPreferredApnName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mPreferredApnName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private parseProfile()I
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 704
    const-string v8, "CscConnection"

    const-string v9, "parserProfile"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "Settings.Connections"

    invoke-virtual {v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 707
    .local v1, "node":Lorg/w3c/dom/Node;
    if-nez v1, :cond_1

    .line 817
    :cond_0
    :goto_0
    return v6

    .line 710
    :cond_1
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "Profile"

    invoke-virtual {v8, v1, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 711
    .local v2, "nodeList":Lorg/w3c/dom/NodeList;
    if-eqz v2, :cond_0

    .line 714
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mProfiles:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 716
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    if-ge v0, v6, :cond_15

    .line 717
    new-instance v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;

    const/4 v6, 0x0

    invoke-direct {v3, p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;-><init>(Lcom/samsung/sec/android/application/csc/CscConnection;Lcom/samsung/sec/android/application/csc/CscConnection$1;)V

    .line 721
    .local v3, "profile":Lcom/samsung/sec/android/application/csc/CscConnection$Profile;
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "NetworkName"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 722
    .local v4, "tempNode":Lorg/w3c/dom/Node;
    if-eqz v4, :cond_2

    .line 723
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->nwkname:Ljava/lang/String;

    .line 725
    :cond_2
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "Editable"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 726
    if-eqz v4, :cond_3

    .line 727
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->editable:Ljava/lang/String;

    .line 729
    :cond_3
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "EnableStatus"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 730
    if-eqz v4, :cond_4

    .line 731
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->enableStatus:Ljava/lang/String;

    .line 733
    :cond_4
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "RoamingEnableStatus"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 734
    if-eqz v4, :cond_5

    .line 735
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->roamingEnableStatus:Ljava/lang/String;

    .line 737
    :cond_5
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "IpVersion"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 738
    if-eqz v4, :cond_6

    .line 739
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->ipVersion:Ljava/lang/String;

    .line 741
    :cond_6
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "RoamingIpVersion"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 742
    if-eqz v4, :cond_7

    .line 743
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->roamingIpVersion:Ljava/lang/String;

    .line 745
    :cond_7
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "ProfileName"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 746
    if-eqz v4, :cond_8

    .line 747
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->profName:Ljava/lang/String;

    .line 749
    :cond_8
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "URL"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 750
    if-eqz v4, :cond_9

    .line 751
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->url:Ljava/lang/String;

    .line 753
    :cond_9
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "Auth"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 754
    if-eqz v4, :cond_a

    .line 755
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->authtype:Ljava/lang/String;

    .line 757
    :cond_a
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "Protocol"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 758
    if-eqz v4, :cond_b

    .line 759
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->protocol:Ljava/lang/String;

    .line 761
    :cond_b
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "APN"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 762
    if-eqz v4, :cond_c

    .line 763
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->apn:Ljava/lang/String;

    .line 765
    :cond_c
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "MTUSize"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 766
    if-eqz v4, :cond_13

    .line 767
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->mtu:I

    .line 771
    :goto_2
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "Proxy"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 772
    if-eqz v4, :cond_f

    .line 775
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "EnableFlag"

    invoke-virtual {v6, v4, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 776
    .local v5, "tempNode2":Lorg/w3c/dom/Node;
    if-eqz v5, :cond_d

    .line 777
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->proxyEnable:Ljava/lang/String;

    .line 779
    :cond_d
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "ServAddr"

    invoke-virtual {v6, v4, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 780
    if-eqz v5, :cond_e

    .line 781
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->proxy:Ljava/lang/String;

    .line 783
    :cond_e
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "Port"

    invoke-virtual {v6, v4, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 784
    if-eqz v5, :cond_f

    .line 785
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->port:Ljava/lang/String;

    .line 788
    .end local v5    # "tempNode2":Lorg/w3c/dom/Node;
    :cond_f
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "PSparam"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 789
    if-eqz v4, :cond_12

    .line 792
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "APN"

    invoke-virtual {v6, v4, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 793
    .restart local v5    # "tempNode2":Lorg/w3c/dom/Node;
    if-eqz v5, :cond_10

    .line 794
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->apn:Ljava/lang/String;

    .line 796
    :cond_10
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "UserId"

    invoke-virtual {v6, v4, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 797
    if-eqz v5, :cond_11

    .line 798
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->userId:Ljava/lang/String;

    .line 800
    :cond_11
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "Password"

    invoke-virtual {v6, v4, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 801
    if-eqz v5, :cond_12

    .line 802
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->passwd:Ljava/lang/String;

    .line 805
    .end local v5    # "tempNode2":Lorg/w3c/dom/Node;
    :cond_12
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "Bearer"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 806
    if-eqz v4, :cond_14

    .line 807
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertBearer(Ljava/lang/String;)I

    move-result v6

    iput v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->bearer:I

    .line 815
    :goto_3
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mProfiles:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 716
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 769
    :cond_13
    const/16 v6, 0x5dc

    iput v6, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->mtu:I

    goto/16 :goto_2

    .line 809
    :cond_14
    iput v7, v3, Lcom/samsung/sec/android/application/csc/CscConnection$Profile;->bearer:I

    goto :goto_3

    .end local v3    # "profile":Lcom/samsung/sec/android/application/csc/CscConnection$Profile;
    .end local v4    # "tempNode":Lorg/w3c/dom/Node;
    :cond_15
    move v6, v7

    .line 817
    goto/16 :goto_0
.end method

.method private parseProfileHandle()I
    .locals 8

    .prologue
    const/4 v5, -0x1

    .line 943
    const-string v6, "CscConnection"

    const-string v7, "parserProfileHandle"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 945
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "Settings.Connections"

    invoke-virtual {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 946
    .local v1, "node":Lorg/w3c/dom/Node;
    if-nez v1, :cond_1

    .line 1035
    :cond_0
    :goto_0
    return v5

    .line 949
    :cond_1
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "ProfileHandle"

    invoke-virtual {v6, v1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 950
    .local v2, "nodeList":Lorg/w3c/dom/NodeList;
    if-eqz v2, :cond_0

    .line 953
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mProfileHandles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 955
    const-string v5, "CscConnection"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "the number of mProfileHandles = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 957
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v0, v5, :cond_12

    .line 958
    new-instance v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;

    const/4 v5, 0x0

    invoke-direct {v3, p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;-><init>(Lcom/samsung/sec/android/application/csc/CscConnection;Lcom/samsung/sec/android/application/csc/CscConnection$1;)V

    .line 961
    .local v3, "profHandle":Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "NetworkName"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 962
    .local v4, "tempNode":Lorg/w3c/dom/Node;
    if-eqz v4, :cond_2

    .line 963
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->nwkname:Ljava/lang/String;

    .line 965
    :cond_2
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfBrowser"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 966
    if-eqz v4, :cond_3

    .line 967
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profBrowser:Ljava/lang/String;

    .line 969
    :cond_3
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfMMS"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 970
    if-eqz v4, :cond_4

    .line 971
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profMms:Ljava/lang/String;

    .line 973
    :cond_4
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfEmail"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 974
    if-eqz v4, :cond_5

    .line 975
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profEmail:Ljava/lang/String;

    .line 977
    :cond_5
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfIM"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 978
    if-eqz v4, :cond_6

    .line 979
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profIm:Ljava/lang/String;

    .line 981
    :cond_6
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfStreaming"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 982
    if-eqz v4, :cond_7

    .line 983
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profStream:Ljava/lang/String;

    .line 985
    :cond_7
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfIntSharing"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 986
    if-eqz v4, :cond_8

    .line 987
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profDun:Ljava/lang/String;

    .line 989
    :cond_8
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfAGPS"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 990
    if-eqz v4, :cond_9

    .line 991
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profSupl:Ljava/lang/String;

    .line 993
    :cond_9
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfActiveSync"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 994
    if-eqz v4, :cond_a

    .line 995
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profActiveSync:Ljava/lang/String;

    .line 997
    :cond_a
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfCBS"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 998
    if-eqz v4, :cond_b

    .line 999
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profCbs:Ljava/lang/String;

    .line 1001
    :cond_b
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfFOTA"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 1002
    if-eqz v4, :cond_c

    .line 1003
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profFota:Ljava/lang/String;

    .line 1005
    :cond_c
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfIMS"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 1006
    if-eqz v4, :cond_d

    .line 1007
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profIms:Ljava/lang/String;

    .line 1009
    :cond_d
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfXCAP"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 1010
    if-eqz v4, :cond_e

    .line 1011
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profXcap:Ljava/lang/String;

    .line 1013
    :cond_e
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfSyncDM"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 1014
    if-eqz v4, :cond_f

    .line 1015
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profSyncDM:Ljava/lang/String;

    .line 1018
    :cond_f
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfWap"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 1019
    if-eqz v4, :cond_10

    .line 1020
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profWap:Ljava/lang/String;

    .line 1022
    :cond_10
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "ProfEpdg"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 1023
    if-eqz v4, :cond_11

    .line 1024
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v5, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;->profEpdg:Ljava/lang/String;

    .line 1032
    :cond_11
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mProfileHandles:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 957
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 1035
    .end local v3    # "profHandle":Lcom/samsung/sec/android/application/csc/CscConnection$ProfileHandle;
    .end local v4    # "tempNode":Lorg/w3c/dom/Node;
    :cond_12
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method private setDefaultAPN()I
    .locals 14

    .prologue
    .line 2026
    const-string v0, "DCGG"

    const-string v1, "GG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CG"

    const-string v1, "GG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2032
    :cond_0
    const-string v0, "CscConnection"

    const-string v1, "CGG setDefaultAPN return"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2033
    const/4 v11, -0x1

    .line 2087
    :goto_0
    return v11

    .line 2037
    :cond_1
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v0, :cond_2

    .line 2038
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 2041
    :cond_2
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "DefaultApn.ProfileName"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v9

    .line 2043
    .local v9, "node":Lorg/w3c/dom/Node;
    if-nez v9, :cond_3

    .line 2044
    const/4 v11, -0x1

    goto :goto_0

    .line 2046
    :cond_3
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v0, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    .line 2048
    .local v6, "DefaultProfName":Ljava/lang/String;
    const-string v0, "CscConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Default Profile Name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2052
    const-string v0, "gsm.sim.operator.numeric"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2054
    .local v10, "numeric":Ljava/lang/String;
    const-string v3, "name=? and numeric=?"

    .line 2055
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v6, v4, v0

    const/4 v0, 0x1

    aput-object v10, v4, v0

    .line 2057
    .local v4, "selectionArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v13, "_id"

    aput-object v13, v2, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2065
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    .line 2066
    :cond_4
    if-eqz v7, :cond_5

    .line 2067
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2068
    :cond_5
    const/4 v11, -0x1

    goto :goto_0

    .line 2075
    :cond_6
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2076
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2078
    .local v8, "key":Ljava/lang/String;
    const-string v0, "CscConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Default Profile key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2080
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 2081
    .local v12, "values":Landroid/content/ContentValues;
    const-string v0, "apn_id"

    invoke-virtual {v12, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2083
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "content://telephony/carriers/preferapn"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v12, v2, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v11

    .line 2086
    .local v11, "ret":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private validateCarrierTable(Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;)Z
    .locals 9
    .param p1, "carrier"    # Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1757
    const-string v3, "name=? and numeric=? and apn=? and user=? and password=? and proxy=? and port=? and mmsproxy=? and mmsport=? and mmsc=? and type=? and authtype=? and protocol=? and roaming_protocol=? and nwkname=?"

    .line 1758
    .local v3, "selection":Ljava/lang/String;
    const/16 v0, 0xf

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    iget-object v0, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    const/4 v0, 0x2

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->proxy:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x6

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->port:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x7

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsProxy:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x8

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsPort:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x9

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsc:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0xa

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->types:[Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0xb

    iget v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->authType:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0xc

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->protocol:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0xd

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->roamingProtocol:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0xe

    iget-object v1, p1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1774
    .local v4, "selectionArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_CARRIERS:Landroid/net/Uri;

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1776
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 1777
    :cond_0
    if-eqz v6, :cond_1

    .line 1778
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v8

    .line 1789
    :goto_0
    return v0

    .line 1783
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt v0, v7, :cond_3

    .line 1784
    const-string v0, "CscConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "validate same profile count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1785
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v7

    .line 1786
    goto :goto_0

    .line 1788
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v8

    .line 1789
    goto :goto_0
.end method

.method private validateDormPolicyTable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "plmn"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "lcdonfdtime"    # Ljava/lang/String;
    .param p4, "lcdofffdtime"    # Ljava/lang/String;
    .param p5, "lcdonfdtime_Rel8"    # Ljava/lang/String;
    .param p6, "lcdofffdtime_Rel8"    # Ljava/lang/String;

    .prologue
    .line 1361
    const-string v3, "plmn=? and nwkname=? and lcdonfdtime=? and lcdofffdtime=? and lcdonfdtime_Rel8=? and lcdofffdtime_Rel8=?"

    .line 1362
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-direct {p0, p2}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    invoke-direct {p0, p3}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSetToZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-direct {p0, p4}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSetToZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    invoke-direct {p0, p5}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSetToZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x5

    invoke-direct {p0, p6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSetToZero(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1371
    .local v4, "selectionArgs":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_DORMPOLICY:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1373
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 1374
    :cond_0
    if-eqz v6, :cond_1

    .line 1375
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1377
    :cond_1
    const/4 v0, 0x0

    .line 1389
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v0

    .line 1380
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1381
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1382
    const/4 v0, 0x1

    goto :goto_0

    .line 1385
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1386
    const/4 v0, 0x0

    goto :goto_0

    .line 1387
    .end local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 1388
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "CscConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception caught during nwkinfo query(compare): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1389
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private validateNwkInfoTable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "plmn"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "dormancy"    # Ljava/lang/String;
    .param p4, "mtu"    # I
    .param p5, "subsetcode"    # Ljava/lang/String;
    .param p6, "spcode"    # Ljava/lang/String;
    .param p7, "spname"    # Ljava/lang/String;

    .prologue
    .line 1168
    const-string v3, "plmn=? and nwkname=? and dormancy=? and mtu=? and subsetcode=? and spcode=? and spname=?"

    .line 1169
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x7

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-direct {p0, p2}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    invoke-direct {p0, p3}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    invoke-direct {p0, p5}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x5

    invoke-direct {p0, p6}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x6

    invoke-direct {p0, p7}, Lcom/samsung/sec/android/application/csc/CscConnection;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1179
    .local v4, "selectionArgs":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_NWKINFO:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1181
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 1182
    :cond_0
    if-eqz v6, :cond_1

    .line 1183
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1185
    :cond_1
    const/4 v0, 0x0

    .line 1197
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v0

    .line 1188
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1189
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1190
    const/4 v0, 0x1

    goto :goto_0

    .line 1193
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1194
    const/4 v0, 0x0

    goto :goto_0

    .line 1195
    .end local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 1196
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "CscConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception caught during nwkinfo query(compare): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected IncludeFixedApn(Ljava/lang/String;)Z
    .locals 6
    .param p1, "requestedApnType"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1467
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_RIL_ConfigFixedApn"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1468
    .local v0, "ConfigFixedApn":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1469
    const-string v4, "CscConnection"

    const-string v5, "IncludeFixedApn : ConfigFixedApn is empty! return.."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1478
    :cond_0
    :goto_0
    return v3

    .line 1472
    :cond_1
    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1473
    .local v2, "types":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v4, v2

    if-ge v1, v4, :cond_0

    .line 1474
    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1475
    const/4 v3, 0x1

    goto :goto_0

    .line 1473
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 532
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Connections.Profile.Bearer"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 533
    const-string v0, "Settings.Connections.Profile.Proxy.EnableFlag"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 534
    const-string v0, "Settings.Connections.Profile.PSparam.TrafficClass"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 535
    const-string v0, "Settings.Connections.Profile.RoamingEnableStatus"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 536
    const-string v0, "Settings.Connections.ProfileHandle.NetworkName"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 537
    const-string v0, "Settings.Connections.ProfileHandle.ProfBrowser"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 538
    const-string v0, "Settings.Connections.ProfileHandle.ProfMMS"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 539
    const-string v0, "Settings.Connections.ProfileHandle.ProfIM"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 540
    const-string v0, "Settings.Connections.ProfileHandle.ProfEmail"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 541
    const-string v0, "Settings.Connections.ProfileHandle.ProfStreaming"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 542
    const-string v0, "Settings.Connections.ProfileHandle.ProfIntSharing"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 543
    const-string v0, "Settings.Connections.ProfileHandle.ProfAGPS"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 544
    const-string v0, "Settings.Connections.ProfileHandle.ProfSGPS"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 545
    const-string v0, "Settings.Connections.ProfileHandle.ProfActiveSync"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 546
    const-string v0, "Settings.Connections.ProfileHandle.ProfCBS"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 547
    const-string v0, "Settings.Connections.ProfileHandle.ProfFOTA"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 548
    const-string v0, "Settings.Connections.ProfileHandle.ProfIMS"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 549
    const-string v0, "Settings.Connections.ProfileHandle.ProfXCAP"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 550
    const-string v0, "Settings.Connections.ProfileHandle.ProfSyncDM"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 551
    const-string v0, "Settings.Connections.ProfileHandle.ProfWap"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 552
    const-string v0, "Settings.Connections.ProfileHandle.ProfEpdg"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 553
    const-string v0, "Settings.Connections.ProfileHandle.ProfRCS"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 554
    const-string v0, "Settings.Main.Network.FastDormancyRel8.FastDormancyEnableStatus"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 555
    const-string v0, "Settings.Main.Network.AutoFastDormancy"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 556
    const-string v0, "Settings.Main.Network.AutoProfile"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 557
    const-string v0, "Settings.Main.Network.MaxPDP.MaxPDP2G"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 558
    const-string v0, "Settings.Main.Network.MaxPDP.MaxPDP3G"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 559
    const-string v0, "Settings.Connections.Profile.Proxy.EnableFlag"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 560
    const-string v0, "Settings.ConnManager.MetaNet.DestinationID"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 561
    const-string v0, "Settings.ConnManager.MetaNet.MetaName"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 562
    const-string v0, "Settings.ConnManager.NetMapping.NetMapNetwork"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 563
    const-string v0, "Settings.ConnManager.NetMapping.NetMapPattern"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 564
    const-string v0, "Settings.ConnManager.NetMapping.NetMapType"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 565
    const-string v0, "Settings.ConnManager.PrefConn.NetGUID"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 566
    const-string v0, "Settings.ConnManager.PrefConn.ProfConnName"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 567
    const-string v0, "Settings.Connections.ProfileHandle.ProfChatOn"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 568
    const-string v0, "Settings.Connections.Profile.DNS.EnableFlag"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 569
    const-string v0, "Settings.Connections.Profile.Gateway.EnableFlag"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 570
    const-string v0, "Settings.Connections.Profile.Gateway.SecureConn"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 571
    const-string v0, "Settings.Connections.Profile.Gateway.ServAddr"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 572
    const-string v0, "Settings.Connections.Profile.StaticIP.EnableFlag"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 573
    const-string v0, "Settings.Connections.ProfileHandle.ProfSNS"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 574
    const-string v0, "Settings.Connections.ProfileHandle.ProfSyncDS"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 575
    const-string v0, "Settings.Connections.ProfileHandle.ProfWidget"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 576
    const-string v0, "Settings.Connections.ProfileHandle.ProfAstore"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 577
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 491
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 493
    const-string v0, "CscConnection"

    const-string v1, "compare calls"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const-string v0, "NOERROR"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mLastFailCause:Ljava/lang/String;

    .line 497
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->parse()V

    .line 500
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->createCarriers()V

    .line 502
    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscConnection;->doActionNwkInfoTable(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 503
    const-string v0, "CscConnection"

    const-string v1, "The values of nwkinfo table aren\'t matched from DB and CSC"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    const-string v0, "CscConnection : The values of nwkinfo table aren\'t matched from DB and CSC"

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 506
    const-string v0, "NWKINFO TABLE ERROR"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mLastFailCause:Ljava/lang/String;

    .line 510
    :cond_0
    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscConnection;->doActionDormPolicyTable(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 511
    const-string v0, "CscConnection"

    const-string v1, "The values of dormancy table aren\'t matched from DB and CSC"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    const-string v0, "CscConnectionThe values of dormancy table aren\'t matched from DB and CSC"

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 514
    const-string v0, "DORMANCY TABLE ERROR"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mLastFailCause:Ljava/lang/String;

    .line 518
    :cond_1
    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscConnection;->doActionCarrierTable(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 519
    const-string v0, "CscConnection"

    const-string v1, "The values of carrier table aren\'t matched from DB and CSC"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    const-string v0, "CscConnectionThe values of carrier table aren\'t matched from DB and CSC"

    invoke-static {v0}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 522
    const-string v0, "CARRIER TABLE ERROR"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mLastFailCause:Ljava/lang/String;

    .line 526
    :cond_2
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mLastFailCause:Ljava/lang/String;

    return-object v0
.end method

.method convertAuthType(Ljava/lang/String;)I
    .locals 2
    .param p1, "authtype"    # Ljava/lang/String;

    .prologue
    const/4 v0, -0x1

    .line 1921
    if-nez p1, :cond_1

    .line 1931
    :cond_0
    :goto_0
    return v0

    .line 1924
    :cond_1
    const-string v1, "none"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1925
    const/4 v0, 0x0

    goto :goto_0

    .line 1926
    :cond_2
    const-string v1, "normal"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1927
    const/4 v0, 0x1

    goto :goto_0

    .line 1928
    :cond_3
    const-string v1, "secure"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1929
    const/4 v0, 0x2

    goto :goto_0
.end method

.method convertType([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "types"    # [Ljava/lang/String;

    .prologue
    .line 1910
    const-string v1, ""

    .line 1911
    .local v1, "type":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 1912
    if-nez v0, :cond_0

    .line 1913
    aget-object v1, p1, v0

    .line 1911
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1915
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1917
    :cond_1
    return-object v1
.end method

.method convertTypes(Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;)[Ljava/lang/String;
    .locals 5
    .param p1, "carrierTypes"    # Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;

    .prologue
    .line 1838
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1839
    .local v2, "typeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v4, "ro.csc.sales_code"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1840
    .local v1, "salesCode":Ljava/lang/String;
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableDefault:Z

    if-eqz v4, :cond_0

    .line 1841
    const-string v4, "default"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1844
    :cond_0
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableMms:Z

    if-eqz v4, :cond_1

    .line 1845
    const-string v4, "mms"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1851
    :cond_1
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableSupl:Z

    if-eqz v4, :cond_2

    .line 1852
    const-string v4, "supl"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1855
    :cond_2
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableDun:Z

    if-eqz v4, :cond_3

    .line 1856
    const-string v4, "dun"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1859
    :cond_3
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableHipri:Z

    if-eqz v4, :cond_4

    .line 1860
    const-string v4, "hipri"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1863
    :cond_4
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableCbs:Z

    if-eqz v4, :cond_5

    .line 1864
    const-string v4, "cbs"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1867
    :cond_5
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableFota:Z

    if-eqz v4, :cond_6

    .line 1868
    const-string v4, "fota"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1871
    :cond_6
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableIms:Z

    if-eqz v4, :cond_7

    .line 1872
    const-string v4, "ims"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1875
    :cond_7
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableXcap:Z

    if-eqz v4, :cond_8

    .line 1876
    const-string v4, "xcap"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1879
    :cond_8
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableEmail:Z

    if-eqz v4, :cond_9

    .line 1880
    const-string v4, "email"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1883
    :cond_9
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableSyncDm:Z

    if-eqz v4, :cond_b

    .line 1884
    const-string v4, "CHM"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "CHC"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1885
    :cond_a
    const-string v4, "cmdm"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1889
    :cond_b
    iget-boolean v4, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableWap:Z

    if-eqz v4, :cond_c

    .line 1890
    const-string v4, "wap"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1893
    :cond_c
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v3, v4, [Ljava/lang/String;

    .line 1895
    .local v3, "types":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_d

    .line 1896
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    aput-object v4, v3, v0

    .line 1895
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1899
    :cond_d
    invoke-virtual {v3}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    return-object v4
.end method

.method public createAllNetworkInfoList()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1998
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mNwkInfos:Ljava/util/ArrayList;

    .line 1999
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_NWKINFO:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2001
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 2002
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2003
    invoke-virtual {p0, v6}, Lcom/samsung/sec/android/application/csc/CscConnection;->createNetworkInfoList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mNwkInfos:Ljava/util/ArrayList;

    .line 2005
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2007
    :cond_1
    return-void
.end method

.method public createCarrierList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2091
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2092
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;>;"
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2094
    :cond_0
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;

    const/4 v5, 0x0

    invoke-direct {v1, p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;-><init>(Lcom/samsung/sec/android/application/csc/CscConnection;Lcom/samsung/sec/android/application/csc/CscConnection$1;)V

    .line 2095
    .local v1, "carrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    const-string v5, "name"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    .line 2097
    const-string v5, "apn"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    .line 2099
    const-string v5, "proxy"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->proxy:Ljava/lang/String;

    .line 2101
    const-string v5, "port"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->port:Ljava/lang/String;

    .line 2103
    const-string v5, "user"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    .line 2105
    const-string v5, "password"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    .line 2107
    const-string v5, "mmsc"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsc:Ljava/lang/String;

    .line 2109
    const-string v5, "numeric"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    .line 2111
    const-string v5, "mmsproxy"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsProxy:Ljava/lang/String;

    .line 2113
    const-string v5, "protocol"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->protocol:Ljava/lang/String;

    .line 2115
    const-string v5, "mmsport"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsPort:Ljava/lang/String;

    .line 2117
    const-string v5, "bearer"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection;->convertBearer(Ljava/lang/String;)I

    move-result v5

    iput v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->bearer:I

    .line 2119
    const-string v5, "authtype"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2121
    .local v0, "authtype":Ljava/lang/String;
    const-string v5, "nwkname"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 2122
    const-string v5, "nwkname"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    .line 2125
    :cond_1
    :try_start_0
    const-string v5, ""

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2126
    const/4 v5, 0x0

    iput v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->authType:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2133
    :goto_0
    const-string v5, "type"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2135
    .local v4, "type":Ljava/lang/String;
    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->types:[Ljava/lang/String;

    .line 2137
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2138
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2140
    .end local v0    # "authtype":Ljava/lang/String;
    .end local v1    # "carrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    .end local v4    # "type":Ljava/lang/String;
    :cond_2
    return-object v3

    .line 2128
    .restart local v0    # "authtype":Ljava/lang/String;
    .restart local v1    # "carrier":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    :cond_3
    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->authType:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2130
    :catch_0
    move-exception v2

    .line 2131
    .local v2, "e":Ljava/lang/NumberFormatException;
    iput v7, v1, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->authType:I

    goto :goto_0
.end method

.method public createNetworkInfoList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2010
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2011
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;>;"
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2013
    :cond_0
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;-><init>(Lcom/samsung/sec/android/application/csc/CscConnection;Lcom/samsung/sec/android/application/csc/CscConnection$1;)V

    .line 2014
    .local v0, "nwkinfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    const-string v2, "plmn"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    .line 2015
    const-string v2, "nwkname"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    .line 2016
    const-string v2, "spcode"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->subsetcode:Ljava/lang/String;

    .line 2017
    const-string v2, "subsetcode"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spcode:Ljava/lang/String;

    .line 2018
    const-string v2, "spname"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spname:Ljava/lang/String;

    .line 2019
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2020
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2022
    .end local v0    # "nwkinfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    :cond_1
    return-object v1
.end method

.method public devideUserApn()V
    .locals 15

    .prologue
    .line 2210
    const-string v12, "gsm.sim.operator.numeric"

    invoke-static {v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2211
    .local v9, "numeric":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "numeric=\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2212
    .local v10, "selection":Ljava/lang/String;
    const/4 v8, 0x0

    .line 2214
    .local v8, "matched":Z
    sget-object v12, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v12, v10}, Lcom/samsung/sec/android/application/csc/CscConnection;->getCarriersFromDB(Landroid/net/Uri;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 2215
    .local v4, "cTelephony":Landroid/database/Cursor;
    if-nez v4, :cond_0

    .line 2216
    const-string v12, "CscConnection"

    const-string v13, "Failed approach to telephony.db, return.."

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2272
    :goto_0
    return-void

    .line 2219
    :cond_0
    sget-object v12, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_CARRIERS:Landroid/net/Uri;

    invoke-direct {p0, v12, v10}, Lcom/samsung/sec/android/application/csc/CscConnection;->getCarriersFromDB(Landroid/net/Uri;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 2220
    .local v3, "cNwkInfo":Landroid/database/Cursor;
    if-nez v3, :cond_1

    .line 2221
    const-string v12, "CscConnection"

    const-string v13, "Failed approach to nwk_info.db, return.."

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2222
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2225
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2226
    .local v1, "TelephonyApn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2227
    .local v0, "PrevDefaultApn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;>;"
    invoke-virtual {p0, v4}, Lcom/samsung/sec/android/application/csc/CscConnection;->createCarrierList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2228
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscConnection;->createCarrierList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2229
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-nez v12, :cond_2

    .line 2230
    const-string v12, "CscConnection"

    const-string v13, "telephony.db is empty. return.."

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2231
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 2232
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2235
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-nez v12, :cond_3

    .line 2236
    const-string v12, "CscConnection"

    const-string v13, "nwk_info.db is empty. return.."

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2237
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 2238
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2241
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2242
    .local v2, "UserApn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;

    .line 2243
    .local v11, "telephonyapn":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    const/4 v8, 0x0

    .line 2244
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;

    .line 2245
    .local v5, "defaultapn":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    iget-object v12, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    iput-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->nwkname:Ljava/lang/String;

    .line 2246
    const/4 v12, 0x1

    iput-boolean v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrierEnabled:Z

    .line 2247
    iget-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    iget-object v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->carrier:Ljava/lang/String;

    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    iget-object v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->proxy:Ljava/lang/String;

    iget-object v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->proxy:Ljava/lang/String;

    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->port:Ljava/lang/String;

    iget-object v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->port:Ljava/lang/String;

    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    iget-object v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->user:Ljava/lang/String;

    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    iget-object v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->password:Ljava/lang/String;

    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsc:Ljava/lang/String;

    iget-object v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsc:Ljava/lang/String;

    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    iget-object v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->numeric:Ljava/lang/String;

    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsProxy:Ljava/lang/String;

    iget-object v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->mmsProxy:Ljava/lang/String;

    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->types:[Ljava/lang/String;

    iget-object v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->types:[Ljava/lang/String;

    invoke-virtual {p0, v12, v13}, Lcom/samsung/sec/android/application/csc/CscConnection;->hasSameTypes([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget-object v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->protocol:Ljava/lang/String;

    iget-object v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->protocol:Ljava/lang/String;

    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget v12, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->authType:I

    iget v13, v5, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->authType:I

    if-ne v12, v13, :cond_5

    .line 2259
    const-string v12, "CscConnection"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Found PrevDefaultAPN = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2260
    const/4 v8, 0x1

    .line 2264
    .end local v5    # "defaultapn":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    :cond_6
    if-nez v8, :cond_4

    .line 2265
    const-string v12, "CscConnection"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "added to uUserCarriers = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v11, Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;->apn:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2266
    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2269
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v11    # "telephonyapn":Lcom/samsung/sec/android/application/csc/CscConnection$Carrier;
    :cond_7
    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscConnection;->ConvertFromCarrierToContentValues(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v12

    sput-object v12, Lcom/samsung/sec/android/application/csc/CscConnection;->mUserApn:Ljava/util/ArrayList;

    .line 2270
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 2271
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 0
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 1968
    invoke-virtual {p0, p1}, Lcom/samsung/sec/android/application/csc/CscConnection;->makeNetworkInfo(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V

    .line 1969
    return-void
.end method

.method public hasSameTypes([Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 6
    .param p1, "ref"    # [Ljava/lang/String;
    .param p2, "comp"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 2189
    array-length v4, p1

    array-length v5, p2

    if-eq v4, v5, :cond_1

    .line 2205
    :cond_0
    :goto_0
    return v3

    .line 2192
    :cond_1
    const/4 v2, 0x0

    .line 2193
    .local v2, "res":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v4, p1

    if-ge v0, v4, :cond_4

    .line 2194
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    array-length v4, p2

    if-ge v1, v4, :cond_2

    .line 2195
    aget-object v4, p1, v0

    aget-object v5, p2, v1

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2196
    add-int/lit8 v2, v2, 0x1

    .line 2193
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2194
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2201
    .end local v1    # "j":I
    :cond_4
    array-length v4, p1

    if-ne v4, v2, :cond_0

    .line 2202
    const/4 v3, 0x1

    goto :goto_0
.end method

.method hasType([Ljava/lang/String;)Z
    .locals 1
    .param p1, "types"    # [Ljava/lang/String;

    .prologue
    .line 1903
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    .line 1904
    const/4 v0, 0x1

    .line 1906
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public makeNetworkInfo(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 6
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 1972
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->createAllNetworkInfoList()V

    .line 1974
    const-string v3, "CscConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ENCODE::mNwkInfos size is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mNwkInfos:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1975
    const-string v3, "GeneralInfo.NbNetworkInfo"

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mNwkInfos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mNwkInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;

    .line 1977
    .local v2, "nwkinfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    const-string v3, "NetworkInfo"

    invoke-virtual {p1, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->newElement(Ljava/lang/String;)Lcom/samsung/sec/android/application/csc/CscXMLElement;

    move-result-object v1

    .line 1978
    .local v1, "nwkelement":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    iget-object v3, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1979
    const-string v3, "MCCMNC"

    iget-object v4, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->plmn:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1981
    :cond_0
    iget-object v3, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1982
    const-string v3, "NetworkName"

    iget-object v4, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->nwkname:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1984
    :cond_1
    iget-object v3, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->subsetcode:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1985
    const-string v3, "SPCode"

    iget-object v4, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->subsetcode:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1987
    :cond_2
    iget-object v3, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spcode:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1988
    const-string v3, "SubsetCode"

    iget-object v4, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spcode:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1990
    :cond_3
    iget-object v3, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spname:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1991
    const-string v3, "SPName"

    iget-object v4, v2, Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;->spname:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993
    :cond_4
    const-string v3, "GeneralInfo"

    invoke-virtual {p1, v3, v1}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addElement(Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscXMLElement;)V

    goto :goto_0

    .line 1995
    .end local v1    # "nwkelement":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    .end local v2    # "nwkinfo":Lcom/samsung/sec/android/application/csc/CscConnection$NwkInfo;
    :cond_5
    return-void
.end method

.method markCarrierTypes(Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;[Ljava/lang/String;)V
    .locals 4
    .param p1, "carrierTypes"    # Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;
    .param p2, "types"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 1793
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 1835
    :cond_0
    return-void

    .line 1796
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 1797
    aget-object v1, p2, v0

    const-string v2, "default"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1798
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableDefault:Z

    .line 1800
    :cond_2
    aget-object v1, p2, v0

    const-string v2, "mms"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1801
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableMms:Z

    .line 1803
    :cond_3
    aget-object v1, p2, v0

    const-string v2, "supl"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1804
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableSupl:Z

    .line 1806
    :cond_4
    aget-object v1, p2, v0

    const-string v2, "dun"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1807
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableDun:Z

    .line 1809
    :cond_5
    aget-object v1, p2, v0

    const-string v2, "hipri"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1810
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableHipri:Z

    .line 1812
    :cond_6
    aget-object v1, p2, v0

    const-string v2, "cbs"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1813
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableCbs:Z

    .line 1815
    :cond_7
    aget-object v1, p2, v0

    const-string v2, "fota"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1816
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableFota:Z

    .line 1818
    :cond_8
    aget-object v1, p2, v0

    const-string v2, "ims"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1819
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableIms:Z

    .line 1821
    :cond_9
    aget-object v1, p2, v0

    const-string v2, "xcap"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1822
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableXcap:Z

    .line 1824
    :cond_a
    aget-object v1, p2, v0

    const-string v2, "email"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1825
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableEmail:Z

    .line 1827
    :cond_b
    aget-object v1, p2, v0

    const-string v2, "cmdm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1828
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableSyncDm:Z

    .line 1831
    :cond_c
    aget-object v1, p2, v0

    const-string v2, "wap"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1832
    iput-boolean v3, p1, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableWap:Z

    .line 1796
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method public update()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 418
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 420
    const-string v2, "CscConnection"

    const-string v3, "update calls"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->parse()V

    .line 426
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->createCarriers()V

    .line 429
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_RESTORE:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 431
    invoke-direct {p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection;->doActionNwkInfoTable(I)Z

    .line 432
    invoke-direct {p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection;->doActionDormPolicyTable(I)Z

    .line 433
    invoke-direct {p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection;->doActionCarrierTable(I)Z

    .line 435
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 437
    .local v0, "salesCode":Ljava/lang/String;
    const-string v2, "CTC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 438
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->setDefaultAPN()I

    move-result v2

    if-gez v2, :cond_0

    .line 439
    const-string v2, "CscConnection"

    const-string v3, "error during setting default APN"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.CSC_UPDATE_DONE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 446
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CSC_SHOW_SIM_PROFILE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 447
    .local v1, "simProfileIntent":Landroid/content/Intent;
    const-string v2, "force"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 448
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 450
    const-string v2, "CscConnection"

    const-string v3, "update calls done"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    return-void
.end method

.method public update(I)V
    .locals 7
    .param p1, "simSlot"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 454
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 456
    const-string v2, "CscConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "update calls : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->parse()V

    .line 462
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->createCarriers()V

    .line 465
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/samsung/sec/android/application/csc/CscConnection;->URI_RESTORE:Landroid/net/Uri;

    invoke-virtual {v2, v3, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 467
    invoke-direct {p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection;->doActionNwkInfoTable(I)Z

    .line 468
    invoke-direct {p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection;->doActionDormPolicyTable(I)Z

    .line 469
    invoke-direct {p0, v5}, Lcom/samsung/sec/android/application/csc/CscConnection;->doActionCarrierTable(I)Z

    .line 471
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 473
    .local v0, "salesCode":Ljava/lang/String;
    const-string v2, "CTC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 474
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->setDefaultAPN()I

    move-result v2

    if-gez v2, :cond_0

    .line 475
    const-string v2, "CscConnection"

    const-string v3, "error during setting default APN"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.CSC_UPDATE_DONE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 482
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CSC_SHOW_SIM_PROFILE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 483
    .local v1, "simProfileIntent":Landroid/content/Intent;
    const-string v2, "force"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 484
    const-string v2, "simSlot"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 485
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 487
    const-string v2, "CscConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "update calls done, simSlot"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    return-void
.end method

.method public updateForHomeFOTA(IZ)V
    .locals 2
    .param p1, "buildType"    # I
    .param p2, "isNewCscEdtion"    # Z

    .prologue
    .line 2275
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->CUSTOMER_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection;->mCscParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 2276
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Setting_IncludeApn4SwUpdate"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2278
    const-string v0, "CscConnection"

    const-string v1, "Set APN Update"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2279
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/sec/android/application/csc/CscReceiver;->mIsAPNRestore:Z

    .line 2280
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->devideUserApn()V

    .line 2281
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscConnection;->update()V

    .line 2285
    :goto_0
    return-void

    .line 2283
    :cond_0
    const-string v0, "CscConnection"

    const-string v1, "APN is not updated - CSC feature not enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
