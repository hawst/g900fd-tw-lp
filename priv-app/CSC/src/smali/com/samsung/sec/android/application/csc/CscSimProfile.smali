.class public Lcom/samsung/sec/android/application/csc/CscSimProfile;
.super Landroid/os/Handler;
.source "CscSimProfile.java"

# interfaces
.implements Lcom/samsung/sec/android/application/csc/UpdatableForHomeFOTA;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscSimProfile$clickListener;,
        Lcom/samsung/sec/android/application/csc/CscSimProfile$dismissListener;,
        Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    }
.end annotation


# static fields
.field public static final TELEPHONY_NO_UPDATE_URI:Landroid/net/Uri;

.field private static mInstance:Lcom/samsung/sec/android/application/csc/CscSimProfile;

.field private static mSimSlotNum:I


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field private allNetworkName:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;",
            ">;"
        }
    .end annotation
.end field

.field private allNetworkNameMultiSim:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectivityMgr:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentImsis:[Ljava/lang/String;

.field private mCurrentNumerics:[Ljava/lang/String;

.field private mCurrentNwknames:[Ljava/lang/String;

.field private mIndex:I

.field private mIsDisconnectingData:Z

.field private mIsForceShow:Z

.field private mIsFotaUpdate:Z

.field private mIsLoadProfileSIM:I

.field private mIsShowings:[Z

.field private mLastImsis:[Ljava/lang/String;

.field private mMMCscParser:Lcom/samsung/sec/android/application/csc/MMCscParser;

.field private mNetworkType:I

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPhysicalSimSlotCount:I

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private retryContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mInstance:Lcom/samsung/sec/android/application/csc/CscSimProfile;

    .line 89
    const-string v0, "content://telephony/carriers/no_update"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->TELEPHONY_NO_UPDATE_URI:Landroid/net/Uri;

    .line 145
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v0

    sput v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mSimSlotNum:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x0

    .line 202
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 61
    const-string v3, "CscSimProfile"

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->LOG_TAG:Ljava/lang/String;

    .line 69
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mMMCscParser:Lcom/samsung/sec/android/application/csc/MMCscParser;

    .line 75
    iput-boolean v6, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsDisconnectingData:Z

    .line 77
    iput v6, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mNetworkType:I

    .line 135
    iput-boolean v6, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsForceShow:Z

    .line 137
    iput-boolean v6, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsFotaUpdate:Z

    .line 143
    iput v6, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIndex:I

    .line 148
    sget v3, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mSimSlotNum:I

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNumerics:[Ljava/lang/String;

    .line 149
    sget v3, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mSimSlotNum:I

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNwknames:[Ljava/lang/String;

    .line 150
    sget v3, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mSimSlotNum:I

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    .line 151
    const-string v3, "ro.multisim.simslotcount"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mPhysicalSimSlotCount:I

    .line 152
    iget v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mPhysicalSimSlotCount:I

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mLastImsis:[Ljava/lang/String;

    .line 154
    sget v3, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mSimSlotNum:I

    new-array v3, v3, [Z

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsShowings:[Z

    .line 156
    iput v6, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    .line 158
    sget v3, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mSimSlotNum:I

    new-array v3, v3, [Ljava/util/ArrayList;

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkNameMultiSim:[Ljava/util/ArrayList;

    .line 184
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    .line 186
    new-instance v3, Lcom/samsung/sec/android/application/csc/CscSimProfile$1;

    invoke-direct {v3, p0}, Lcom/samsung/sec/android/application/csc/CscSimProfile$1;-><init>(Lcom/samsung/sec/android/application/csc/CscSimProfile;)V

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 203
    const-string v3, "CscSimProfile"

    const-string v4, "CscSimProfile Create!!"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    .line 207
    const-string v3, "phone"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 208
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v5, 0x40

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 210
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mConnectivityMgr:Landroid/net/ConnectivityManager;

    .line 213
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    const-string v4, "simprof.preferences_name"

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 215
    .local v1, "preferences":Landroid/content/SharedPreferences;
    new-instance v3, Lcom/samsung/sec/android/application/csc/MMCscParser;

    invoke-direct {v3}, Lcom/samsung/sec/android/application/csc/MMCscParser;-><init>()V

    iput-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mMMCscParser:Lcom/samsung/sec/android/application/csc/MMCscParser;

    .line 217
    if-eqz v1, :cond_4

    .line 218
    const/4 v2, 0x0

    .local v2, "simCnt":I
    :goto_0
    sget v3, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mSimSlotNum:I

    if-ge v2, v3, :cond_2

    .line 219
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNwknames:[Ljava/lang/String;

    const-string v4, "simprof.key.nwkname"

    invoke-virtual {p0, v4, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 221
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNumerics:[Ljava/lang/String;

    const-string v4, "simprof.key.mccmnc"

    invoke-virtual {p0, v4, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 223
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    const-string v4, "simprof.key.imsi"

    invoke-virtual {p0, v4, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 225
    const-string v3, "CscSimProfile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Load Previous CscSimProfile simslot"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNwknames:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNumerics:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 228
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v7, :cond_0

    .line 229
    const-string v3, "CscSimProfile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "    ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "xxxxxxxxx]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 231
    :cond_0
    const-string v3, "CscSimProfile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "    ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 233
    :cond_1
    const-string v3, "CscSimProfile"

    const-string v4, "    [NULL]"

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 237
    :cond_2
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 238
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v2, 0x0

    :goto_2
    iget v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mPhysicalSimSlotCount:I

    if-ge v2, v3, :cond_3

    .line 239
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mLastImsis:[Ljava/lang/String;

    const-string v4, "simprof.key.last_imsi"

    invoke-virtual {p0, v4, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 241
    const-string v3, "CscSimProfile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Load for physical slot ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v3, "simprof.key.last_imsi"

    invoke-virtual {p0, v3, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 238
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 245
    :cond_3
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 247
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "simCnt":I
    :cond_4
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/sec/android/application/csc/CscSimProfile;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscSimProfile;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsDisconnectingData:Z

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/sec/android/application/csc/CscSimProfile;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/sec/android/application/csc/CscSimProfile;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setNetworkType()V

    return-void
.end method

.method private checkNwkInfoProvider()V
    .locals 8

    .prologue
    .line 902
    const-string v0, "CscSimProfile"

    const-string v1, "checkNwkInfoProvider()"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    const/4 v6, 0x0

    .line 906
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://nwkinfo/nwkinfo"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, ""

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 912
    if-eqz v6, :cond_0

    .line 913
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 915
    :cond_0
    :goto_0
    return-void

    .line 908
    :catch_0
    move-exception v7

    .line 909
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v0, "CscSimProfile"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception caught during nwkinfo query: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 912
    if-eqz v6, :cond_0

    .line 913
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 912
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 913
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method private clearPreferences(I)V
    .locals 5
    .param p1, "slotID"    # I

    .prologue
    .line 456
    const-string v2, "CscSimProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clearPreferences for slot: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    const-string v3, "simprof.preferences_name"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 461
    .local v1, "preferences":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 462
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 464
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "simprof.key.nwkname"

    invoke-virtual {p0, v2, p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 465
    const-string v2, "simprof.key.mccmnc"

    invoke-virtual {p0, v2, p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 466
    const-string v2, "simprof.key.imsi"

    invoke-virtual {p0, v2, p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 468
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 471
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNwknames:[Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, p1

    .line 472
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNumerics:[Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, p1

    .line 473
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, p1

    .line 476
    if-eqz v1, :cond_1

    .line 477
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 478
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "simprof.key.last_imsi"

    invoke-virtual {p0, v2, p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 479
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 481
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mLastImsis:[Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, p1

    .line 482
    return-void
.end method

.method private clearProfile(I)V
    .locals 3
    .param p1, "simSlot"    # I

    .prologue
    .line 446
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 447
    .local v0, "salesCode":Ljava/lang/String;
    const-string v1, "DCM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 448
    const-string v1, "CscSimProfile"

    const-string v2, "Don\'t Clear Profile by DCM"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :goto_0
    return-void

    .line 450
    :cond_0
    const-string v1, "CscSimProfile"

    const-string v2, "Clear Profile"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-direct {p0, p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->clearPreferences(I)V

    goto :goto_0
.end method

.method private createAllNetworkList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 36
    .param p1, "simOperatorNumeric"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1205
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 1206
    .local v26, "networkResultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;>;"
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 1207
    .local v25, "networkList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;>;"
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 1208
    .local v29, "networkSubsetCodeFilteredList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;>;"
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 1209
    .local v27, "networkSPCodeFilteredList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;>;"
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 1211
    .local v28, "networkSPNameFilteredList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "plmn = \'"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\'"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 1212
    .local v32, "selection":Ljava/lang/String;
    const/16 v17, 0x0

    .line 1214
    .local v17, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 1216
    .local v9, "paystate":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    invoke-static {v2}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v30

    .line 1217
    .local v30, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v30, :cond_1

    .line 1218
    invoke-interface/range {v30 .. v30}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v35

    .line 1219
    .local v35, "sp":Landroid/content/SharedPreferences;
    const-string v24, "key_paystate"

    .line 1220
    .local v24, "keyPaystate":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    if-eqz v2, :cond_0

    .line 1221
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "key"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    add-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "_paystate"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 1223
    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, v35

    move-object/from16 v1, v24

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    .line 1226
    .end local v24    # "keyPaystate":Ljava/lang/String;
    .end local v35    # "sp":Landroid/content/SharedPreferences;
    :cond_1
    const-string v34, ""

    .line 1227
    .local v34, "simSubsetcode":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getSpcode(I)Ljava/lang/String;

    move-result-object v33

    .line 1228
    .local v33, "simSpcode":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getSPName(I)Ljava/lang/String;

    move-result-object v8

    .line 1229
    .local v8, "simSPName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getIMSI(I)Ljava/lang/String;

    move-result-object v20

    .line 1232
    .local v20, "imsi":Ljava/lang/String;
    const/16 v23, 0x0

    .line 1233
    .local v23, "isContainSubsetCode":Z
    const/16 v21, 0x0

    .line 1234
    .local v21, "isContainSPCode":Z
    const/16 v22, 0x0

    .line 1236
    .local v22, "isContainSPName":Z
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1237
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v7

    if-gt v2, v7, :cond_3

    .line 1238
    const-string v2, "CscSimProfile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid Imsi info : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, "simOperatorNumeric : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    const/4 v2, 0x0

    .line 1393
    :cond_2
    :goto_0
    return-object v2

    .line 1242
    :cond_3
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v7

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v34

    .line 1245
    :cond_4
    const-string v2, "CscSimProfile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Create Network List [SimNumeric : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, "]"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1249
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v7, "content://nwkinfo/nwkinfo"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 1252
    if-eqz v17, :cond_14

    .line 1253
    const-string v2, "CscSimProfile"

    const-string v7, "Check Subset & SP Code Including on DataBase"

    invoke-static {v2, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1256
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1258
    :cond_5
    const-string v2, "nwkname"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1259
    .local v3, "nwkname":Ljava/lang/String;
    const-string v2, "plmn"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1262
    .local v4, "numeric":Ljava/lang/String;
    const-string v2, "subsetcode"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1264
    .local v5, "subsetcode":Ljava/lang/String;
    const-string v2, "spcode"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1265
    .local v6, "spcode":Ljava/lang/String;
    const-string v2, "spname"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1267
    .local v16, "spname":Ljava/lang/String;
    const-string v2, "CscSimProfile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Query Profile [Network : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, "] [Numeric : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, "]"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1270
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1317
    :cond_6
    :goto_1
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1320
    .end local v3    # "nwkname":Ljava/lang/String;
    .end local v4    # "numeric":Ljava/lang/String;
    .end local v5    # "subsetcode":Ljava/lang/String;
    .end local v6    # "spcode":Ljava/lang/String;
    .end local v16    # "spname":Ljava/lang/String;
    :cond_7
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i$":Ljava/util/Iterator;
    :cond_8
    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    .line 1321
    .local v10, "item":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    if-eqz v23, :cond_d

    .line 1322
    iget-object v2, v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->subsetcode:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1323
    iget-object v2, v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->subsetcode:Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v11, v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->subsetcode:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    move-object/from16 v0, v34

    invoke-virtual {v0, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1325
    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1382
    .end local v10    # "item":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    .end local v19    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v18

    .line 1383
    .local v18, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v2, "CscSimProfile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception caught during nwkinfo query: "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1384
    const/4 v2, 0x0

    .line 1389
    if-eqz v17, :cond_2

    .line 1390
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1275
    .end local v18    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v3    # "nwkname":Ljava/lang/String;
    .restart local v4    # "numeric":Ljava/lang/String;
    .restart local v5    # "subsetcode":Ljava/lang/String;
    .restart local v6    # "spcode":Ljava/lang/String;
    .restart local v16    # "spname":Ljava/lang/String;
    :cond_9
    :try_start_2
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->isProfileExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v2, p0

    move-object/from16 v7, v33

    .line 1279
    invoke-direct/range {v2 .. v9}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->filterOut(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1284
    const-string v2, "CscSimProfile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[simSubsetcode : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v34

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, "] [simSpcode : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, "] [simSPName : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, "]"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1287
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    const/4 v2, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1292
    const/16 v23, 0x1

    .line 1297
    :cond_a
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->length()I

    move-result v7

    if-gt v2, v7, :cond_b

    const/4 v2, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    move-object/from16 v0, v33

    invoke-virtual {v0, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1301
    const/16 v21, 0x1

    .line 1304
    :cond_b
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1306
    const/16 v22, 0x1

    .line 1309
    :cond_c
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1310
    new-instance v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    move-object/from16 v11, p0

    move-object v12, v3

    move-object v13, v4

    move-object v14, v5

    move-object v15, v6

    invoke-direct/range {v10 .. v16}, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;-><init>(Lcom/samsung/sec/android/application/csc/CscSimProfile;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    .restart local v10    # "item":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1313
    const-string v2, "CscSimProfile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Add Profile [Network : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, "] [Numeric : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, "]"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 1385
    .end local v3    # "nwkname":Ljava/lang/String;
    .end local v4    # "numeric":Ljava/lang/String;
    .end local v5    # "subsetcode":Ljava/lang/String;
    .end local v6    # "spcode":Ljava/lang/String;
    .end local v10    # "item":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    .end local v16    # "spname":Ljava/lang/String;
    :catch_1
    move-exception v18

    .line 1386
    .local v18, "e":Ljava/lang/StringIndexOutOfBoundsException;
    :try_start_3
    const-string v2, "CscSimProfile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "StringIndexOutOfBoundsException occurred in createAllNetworkList: "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1387
    const/4 v2, 0x0

    .line 1389
    if-eqz v17, :cond_2

    .line 1390
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1329
    .end local v18    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    .restart local v10    # "item":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    .restart local v19    # "i$":Ljava/util/Iterator;
    :cond_d
    :try_start_4
    iget-object v2, v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->subsetcode:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1330
    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 1389
    .end local v10    # "item":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    .end local v19    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    if-eqz v17, :cond_e

    .line 1390
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v2

    .line 1336
    .restart local v19    # "i$":Ljava/util/Iterator;
    :cond_f
    :try_start_5
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_10
    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    .line 1337
    .restart local v10    # "item":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    if-eqz v21, :cond_11

    .line 1338
    iget-object v2, v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->spcode:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 1339
    iget-object v2, v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->spcode:Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v11, v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->spcode:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    move-object/from16 v0, v33

    invoke-virtual {v0, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1340
    move-object/from16 v0, v27

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1344
    :cond_11
    iget-object v2, v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->spcode:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1345
    move-object/from16 v0, v27

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1350
    .end local v10    # "item":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    :cond_12
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 1352
    .local v31, "salesCode":Ljava/lang/String;
    const-string v2, "TGY"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1353
    move-object/from16 v26, v27

    .line 1379
    :goto_4
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_14

    if-nez v23, :cond_13

    if-eqz v21, :cond_14

    .line 1380
    :cond_13
    const/4 v2, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    iget-object v2, v2, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->nwkname:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->sendDuplicatedNetworkIntent(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1389
    .end local v19    # "i$":Ljava/util/Iterator;
    .end local v31    # "salesCode":Ljava/lang/String;
    :cond_14
    if-eqz v17, :cond_15

    .line 1390
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    :cond_15
    move-object/from16 v2, v26

    .line 1393
    goto/16 :goto_0

    .line 1356
    .restart local v19    # "i$":Ljava/util/Iterator;
    .restart local v31    # "salesCode":Ljava/lang/String;
    :cond_16
    :try_start_6
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v7, 0x1

    if-le v2, v7, :cond_1a

    .line 1358
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_17
    :goto_5
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    .line 1359
    .restart local v10    # "item":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    if-eqz v22, :cond_18

    .line 1360
    iget-object v2, v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->spname:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 1361
    iget-object v2, v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->spname:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1362
    move-object/from16 v0, v28

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1366
    :cond_18
    iget-object v2, v10, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->spname:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1367
    move-object/from16 v0, v28

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_5

    .line 1372
    .end local v10    # "item":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    :cond_19
    move-object/from16 v26, v28

    goto :goto_4

    .line 1374
    :cond_1a
    move-object/from16 v26, v27

    goto :goto_4
.end method

.method private filterOut(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 3
    .param p1, "nwkname"    # Ljava/lang/String;
    .param p2, "numeric"    # Ljava/lang/String;
    .param p3, "subsetcode"    # Ljava/lang/String;
    .param p4, "spcode"    # Ljava/lang/String;
    .param p5, "simSpcode"    # Ljava/lang/String;
    .param p6, "simSPName"    # Ljava/lang/String;
    .param p7, "paystate"    # I

    .prologue
    const/4 v0, 0x1

    .line 1652
    const-string v1, "23410"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1654
    if-ne p7, v0, :cond_1

    const-string v1, "O2 Pay Monthly"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1679
    :cond_0
    :goto_0
    return v0

    .line 1659
    :cond_1
    const/4 v1, 0x2

    if-ne p7, v1, :cond_2

    const-string v1, "O2 Pay & Go"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1679
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1665
    :cond_3
    const-string v1, "21407"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1670
    const-string v1, "10"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "01"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "1"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "16"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_4
    const-string v1, "Jazztel"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1673
    const-string v1, "CscSimProfile"

    const-string v2, "Remove Jazztel network due to movistar SIM card"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscSimProfile;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 250
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mInstance:Lcom/samsung/sec/android/application/csc/CscSimProfile;

    if-nez v0, :cond_0

    .line 251
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;

    invoke-direct {v0, p0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mInstance:Lcom/samsung/sec/android/application/csc/CscSimProfile;

    .line 254
    :cond_0
    sget-object v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mInstance:Lcom/samsung/sec/android/application/csc/CscSimProfile;

    return-object v0
.end method

.method private isProfileExist(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "nwkname"    # Ljava/lang/String;
    .param p2, "numeric"    # Ljava/lang/String;

    .prologue
    .line 1398
    const/4 v6, 0x0

    .line 1399
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 1401
    .local v8, "exist":Z
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1402
    :cond_0
    const-string v0, "CscSimProfile"

    const-string v1, "NetworkName or Numeric is empty"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v9, v8

    .line 1426
    .end local v8    # "exist":Z
    .local v9, "exist":I
    :goto_0
    return v9

    .line 1406
    .end local v9    # "exist":I
    .restart local v8    # "exist":Z
    :cond_1
    new-instance v3, Ljava/lang/String;

    const-string v0, "numeric=? AND nwkname=?"

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1407
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    .line 1410
    .local v4, "selectionArgs":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://nwkinfo/nwkinfo/carriers"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1413
    if-eqz v6, :cond_2

    .line 1414
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 1415
    const-string v0, "CscSimProfile"

    const-string v1, "The profile is exist in carriers"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1416
    const/4 v8, 0x1

    .line 1423
    :cond_2
    if-eqz v6, :cond_3

    .line 1424
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move v9, v8

    .line 1426
    .restart local v9    # "exist":I
    goto :goto_0

    .line 1419
    .end local v9    # "exist":I
    :catch_0
    move-exception v7

    .line 1420
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v0, "CscSimProfile"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception caught during query: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1423
    if-eqz v6, :cond_4

    .line 1424
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    move v9, v8

    .restart local v9    # "exist":I
    goto :goto_0

    .line 1423
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v9    # "exist":I
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 1424
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private isShowSimProfile()Z
    .locals 4

    .prologue
    .line 881
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    .line 882
    .local v0, "numeric":Ljava/lang/String;
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 893
    .local v1, "salesCode":Ljava/lang/String;
    const-string v2, "ZZH"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "TGY"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const-string v2, "45403"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 894
    const-string v2, "CscSimProfile"

    const-string v3, "do not show the SIM profile "

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    const/4 v2, 0x0

    .line 898
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isShowing(ZI)V
    .locals 1
    .param p1, "flag"    # Z
    .param p2, "simSlot"    # I

    .prologue
    .line 1698
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsShowings:[Z

    aput-boolean p1, v0, p2

    .line 1699
    return-void
.end method

.method private isSimChanged(I)Z
    .locals 7
    .param p1, "simSlot"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x6

    .line 403
    invoke-virtual {p0, p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getIMSI(I)Ljava/lang/String;

    move-result-object v0

    .line 404
    .local v0, "imsi":Ljava/lang/String;
    const-string v2, "NULL"

    .line 405
    .local v2, "oldImsi":Ljava/lang/String;
    const-string v1, "NULL"

    .line 407
    .local v1, "newImsi":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    aget-object v4, v4, p1

    if-eqz v4, :cond_0

    .line 408
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v6, :cond_3

    .line 409
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    aget-object v5, v5, p1

    invoke-virtual {v5, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "xxxxxxxxx"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 413
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 414
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v6, :cond_4

    .line 415
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "xxxxxxxxx"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 422
    :cond_1
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 423
    const/4 v3, 0x1

    .line 426
    :cond_2
    return v3

    .line 411
    :cond_3
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    aget-object v2, v4, p1

    goto :goto_0

    .line 417
    :cond_4
    move-object v1, v0

    goto :goto_1
.end method

.method private sendDuplicatedNetworkIntent(Ljava/lang/String;)V
    .locals 0
    .param p1, "nwkname"    # Ljava/lang/String;

    .prologue
    .line 848
    return-void
.end method

.method private setCSCVoicemailnumber(Ljava/lang/String;I)V
    .locals 4
    .param p1, "nwkname"    # Ljava/lang/String;
    .param p2, "slotId"    # I

    .prologue
    .line 1623
    const-string v1, "CscSimProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Voicemail] setCSCVoicemailnumber :  send broadcast CSC_UPDATE_NETWORK_DONE with [ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " at slot "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CSC_UPDATE_NETWORK_DONE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1627
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "networkName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1628
    if-eqz p2, :cond_0

    .line 1629
    const-string v1, "SLOT_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1630
    :cond_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1631
    return-void
.end method

.method private setHomeUrl(Ljava/lang/String;)V
    .locals 4
    .param p1, "homeurl"    # Ljava/lang/String;

    .prologue
    .line 762
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 763
    const-string v1, "CscSimProfile"

    const-string v2, "setHomeUrl : nothing to configurate."

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    :goto_0
    return-void

    .line 767
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CSC_UPDATE_HOMEURL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 768
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "homeurl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 769
    const-string v1, "nwkname"

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNwknames:[Ljava/lang/String;

    iget v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 770
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private setNetworkType()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1594
    iget v5, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    invoke-static {v5}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v4

    .line 1595
    .local v4, "subIds":[J
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v5

    aget-wide v6, v4, v8

    invoke-virtual {v5, v6, v7}, Landroid/telephony/TelephonyManager;->getDataState(J)I

    move-result v3

    .line 1596
    .local v3, "state":I
    const/4 v1, 0x0

    .line 1597
    .local v1, "dataEnabled":Z
    iget v5, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    invoke-static {v5}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 1598
    .local v0, "currentPhone":Lcom/android/internal/telephony/Phone;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getDataEnabled()Z

    move-result v1

    .line 1600
    const-string v5, "CscSimProfile"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[LTEON] setNetworkType() DataState : ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "dataEnabled :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1603
    if-eqz v1, :cond_0

    const/4 v5, 0x2

    if-ne v3, v5, :cond_0

    .line 1604
    const-string v5, "CscSimProfile"

    const-string v6, "[LTEON] Currently Data is conncected!deactivate data connection"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    .line 1606
    iput-boolean v9, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsDisconnectingData:Z

    .line 1620
    :goto_0
    return-void

    .line 1607
    :cond_0
    if-ne v3, v9, :cond_1

    .line 1608
    const-string v5, "CscSimProfile"

    const-string v6, "[LTEON] Currently Data is conncecting!"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1609
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 1610
    .local v2, "msg":Landroid/os/Message;
    const-wide/16 v6, 0x3e8

    invoke-virtual {p0, v2, v6, v7}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 1612
    .end local v2    # "msg":Landroid/os/Message;
    :cond_1
    const-string v5, "CscSimProfile"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[LTEON] set network type ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mNetworkType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1613
    const/4 v5, 0x3

    invoke-virtual {p0, v5}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 1614
    .restart local v2    # "msg":Landroid/os/Message;
    if-eqz v0, :cond_2

    .line 1615
    iget v5, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mNetworkType:I

    invoke-interface {v0, v5, v2}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    goto :goto_0

    .line 1617
    :cond_2
    const-string v5, "CscSimProfile"

    const-string v6, "[LTEON] currentPhone is null. unable to set network type"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setPreferences(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p1, "nwkname"    # Ljava/lang/String;
    .param p2, "numeric"    # Ljava/lang/String;
    .param p3, "simSlot"    # I

    .prologue
    const/4 v6, 0x0

    .line 703
    if-nez p1, :cond_0

    .line 704
    const-string p1, ""

    .line 706
    :cond_0
    if-nez p2, :cond_1

    .line 707
    const-string p2, ""

    .line 709
    :cond_1
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    const-string v5, "simprof.preferences_name"

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 712
    .local v3, "preferences":Landroid/content/SharedPreferences;
    if-eqz v3, :cond_2

    .line 713
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 715
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "simprof.key.nwkname"

    invoke-virtual {p0, v4, p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 717
    const-string v4, "simprof.key.mccmnc"

    invoke-virtual {p0, v4, p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 719
    const-string v4, "simprof.key.imsi"

    invoke-virtual {p0, v4, p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getIMSI(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 721
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 726
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    const-string v5, "simprof.preferences_name"

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 728
    .local v2, "pref":Landroid/content/SharedPreferences;
    if-eqz v2, :cond_3

    .line 729
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 730
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v4, "simprof.key.last_imsi"

    invoke-virtual {p0, v4, p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getIMSI(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 732
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 735
    .end local v0    # "edit":Landroid/content/SharedPreferences$Editor;
    :cond_3
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNwknames:[Ljava/lang/String;

    aput-object p1, v4, p3

    .line 736
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNumerics:[Ljava/lang/String;

    aput-object p2, v4, p3

    .line 737
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentImsis:[Ljava/lang/String;

    invoke-virtual {p0, p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getIMSI(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, p3

    .line 738
    return-void
.end method

.method private setPreferredApn(II)V
    .locals 5
    .param p1, "pos"    # I
    .param p2, "simSlot"    # I

    .prologue
    const/4 v4, 0x0

    .line 748
    const-string v2, "content://telephony/carriers/preferapn"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 749
    .local v0, "preferapn_uri":Landroid/net/Uri;
    if-eqz p2, :cond_0

    .line 750
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://telephony/carriers/preferapn"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 752
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 754
    if-ltz p1, :cond_1

    .line 755
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 756
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "apn_id"

    invoke-static {v2, p2}, Lcom/samsung/android/telephony/MultiSimManager;->appendSimSlot(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 757
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 759
    .end local v1    # "values":Landroid/content/ContentValues;
    :cond_1
    return-void
.end method

.method private setProfile(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 30
    .param p1, "nwkname"    # Ljava/lang/String;
    .param p2, "numeric"    # Ljava/lang/String;
    .param p3, "slotID"    # I

    .prologue
    .line 485
    const-string v4, "DCGG"

    const-string v5, "GG"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "CG"

    const-string v5, "GG"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 492
    :cond_0
    const-string v4, "CscSimProfile"

    const-string v5, "CGG setProfile return"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    :cond_1
    :goto_0
    return-void

    .line 496
    :cond_2
    invoke-direct/range {p0 .. p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setPreferences(Ljava/lang/String;Ljava/lang/String;I)V

    .line 498
    const-string v4, "CscSimProfile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Set Profile : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " slot ID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mIsLoadProfileSIM : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    const/16 v19, 0x0

    .line 501
    .local v19, "isDBUpdated":Z
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 502
    const-string v4, "CscSimProfile"

    const-string v5, "No Numeric Data"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 507
    :cond_3
    if-eqz p1, :cond_4

    .line 508
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setCSCVoicemailnumber(Ljava/lang/String;I)V

    .line 510
    :cond_4
    const/4 v13, 0x0

    .line 514
    .local v13, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 516
    .local v8, "selectionArgs":[Ljava/lang/String;
    const-string v24, ""

    .line 518
    .local v24, "simSpcode":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getSpcode(I)Ljava/lang/String;

    move-result-object v15

    .line 519
    .local v15, "decSpcode":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 521
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    .line 522
    .local v20, "num":I
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v24

    .line 523
    const-string v4, "CscSimProfile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setProfile get spcode value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    .end local v20    # "num":I
    :cond_5
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 527
    new-instance v7, Ljava/lang/String;

    const-string v4, "numeric=? AND nwkname=?"

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 528
    .local v7, "selection":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v8, v4, [Ljava/lang/String;

    .end local v8    # "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    aput-object p2, v8, v4

    const/4 v4, 0x1

    aput-object p1, v8, v4

    .line 535
    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    :goto_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://nwkinfo/nwkinfo/carriers"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 538
    if-eqz v13, :cond_f

    .line 541
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_6

    .line 550
    new-instance v27, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "numeric = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' and (sim_slot = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' or sim_slot = \'-1\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 551
    .local v27, "where":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSimProfile;->TELEPHONY_NO_UPDATE_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v4, v5, v0, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 552
    const-string v4, "CscSimProfile"

    const-string v5, "Remove Carriers in TelephonyProvider"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    .end local v27    # "where":Ljava/lang/String;
    :cond_6
    const-string v4, "CscSimProfile"

    const-string v5, "Insert Carriers to TelephonyProvider"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 558
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setPreferredApn(II)V

    .line 561
    :cond_7
    new-instance v26, Landroid/content/ContentValues;

    invoke-direct/range {v26 .. v26}, Landroid/content/ContentValues;-><init>()V

    .line 565
    .local v26, "values":Landroid/content/ContentValues;
    const-string v4, "name"

    const-string v5, "name"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    const-string v4, "apn"

    const-string v5, "apn"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    const-string v4, "proxy"

    const-string v5, "proxy"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    const-string v4, "port"

    const-string v5, "port"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    const-string v4, "mmsproxy"

    const-string v5, "mmsproxy"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    const-string v4, "mmsport"

    const-string v5, "mmsport"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v4, "user"

    const-string v5, "user"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    const-string v4, "password"

    const-string v5, "password"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    const-string v4, "mmsc"

    const-string v5, "mmsc"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    const-string v4, "mcc"

    const-string v5, "mcc"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    const-string v4, "mnc"

    const-string v5, "mnc"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    const-string v4, "numeric"

    const-string v5, "numeric"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    const-string v4, "authtype"

    const-string v5, "authtype"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 591
    const-string v4, "protocol"

    const-string v5, "protocol"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    const-string v4, "roaming_protocol"

    const-string v5, "roaming_protocol"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    const-string v4, "type"

    const-string v5, "type"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    const-string v4, "carrier_enabled"

    const-string v5, "carrier_enabled"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    const-string v4, "bearer"

    const-string v5, "bearer"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    const-string v4, "mtu"

    const-string v5, "mtu"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    const-string v4, "profile_id"

    const-string v5, "profile_id"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    const-string v4, "modem_cognitive"

    const-string v5, "modem_cognitive"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    const-string v4, "max_conns"

    const-string v5, "max_conns"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    const-string v4, "wait_time"

    const-string v5, "wait_time"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    const-string v4, "max_conns_time"

    const-string v5, "max_conns_time"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    const-string v4, "sim_slot"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 621
    const-string v4, "server"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_8

    .line 623
    const-string v4, "server"

    const-string v5, "server"

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSimProfile;->TELEPHONY_NO_UPDATE_URI:Landroid/net/Uri;

    move-object/from16 v0, v26

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v12

    .line 631
    .local v12, "apnUri":Landroid/net/Uri;
    const-string v4, "preferred"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_9

    .line 632
    invoke-static {v12}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    long-to-int v11, v4

    .line 633
    .local v11, "apnPos":I
    const-string v4, "CscSimProfile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Set Prefferen Apn  : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v4}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setPreferredApn(II)V

    .line 637
    .end local v11    # "apnPos":I
    :cond_9
    const-string v4, "type"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 639
    .local v25, "types":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "default"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 640
    const-string v4, "homeurl"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setHomeUrl(Ljava/lang/String;)V

    .line 642
    :cond_a
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_7

    .line 643
    const/16 v19, 0x1

    .line 645
    .end local v12    # "apnUri":Landroid/net/Uri;
    .end local v25    # "types":Ljava/lang/String;
    .end local v26    # "values":Landroid/content/ContentValues;
    :cond_b
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Setting_IncludeApn4SwUpdate"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_f

    .line 647
    sget-object v10, Lcom/samsung/sec/android/application/csc/CscConnection;->mUserApn:Ljava/util/ArrayList;

    .line 648
    .local v10, "UserApn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    if-eqz v10, :cond_d

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_d

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_d

    .line 649
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/content/ContentValues;

    .line 650
    .local v14, "cv":Landroid/content/ContentValues;
    const-string v4, "CscSimProfile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "add userapn to telephony db"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "apn"

    invoke-virtual {v14, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSimProfile;->TELEPHONY_NO_UPDATE_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v14}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 662
    .end local v10    # "UserApn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    .end local v14    # "cv":Landroid/content/ContentValues;
    .end local v17    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v16

    .line 663
    .local v16, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v4, "CscSimProfile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception caught during insert : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 666
    if-eqz v13, :cond_1

    .line 667
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 530
    .end local v7    # "selection":Ljava/lang/String;
    .end local v16    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_c
    new-instance v7, Ljava/lang/String;

    const-string v4, "numeric=?"

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 531
    .restart local v7    # "selection":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    .end local v8    # "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    aput-object p2, v8, v4

    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 654
    .restart local v10    # "UserApn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_d
    :try_start_2
    const-string v4, "CscSimProfile"

    const-string v5, "UserApn is not exist!"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    :cond_e
    if-eqz v10, :cond_f

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_f

    .line 658
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscConnection;->mUserApn:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 666
    .end local v10    # "UserApn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_f
    if-eqz v13, :cond_10

    .line 667
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 670
    :cond_10
    invoke-static/range {p3 .. p3}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v22

    .line 671
    .local v22, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v22, :cond_14

    move-object/from16 v0, v22

    instance-of v4, v0, Lcom/android/internal/telephony/PhoneProxy;

    if-eqz v4, :cond_14

    .line 672
    check-cast v22, Lcom/android/internal/telephony/PhoneProxy;

    .end local v22    # "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual/range {v22 .. v22}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    move-result-object v22

    .line 673
    .restart local v22    # "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v22, :cond_13

    move-object/from16 v0, v22

    instance-of v4, v0, Lcom/android/internal/telephony/gsm/GSMPhone;

    if-eqz v4, :cond_13

    move-object/from16 v4, v22

    .line 674
    check-cast v4, Lcom/android/internal/telephony/gsm/GSMPhone;

    invoke-virtual {v4}, Lcom/android/internal/telephony/gsm/GSMPhone;->updateCurrentCarrierInProvider()Z

    .line 685
    :goto_3
    new-instance v18, Landroid/content/Intent;

    const-string v4, "android.intent.action.SIM_PROFILE_UPDATE_DONE"

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 686
    .local v18, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 689
    if-eqz v19, :cond_11

    .line 690
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 692
    :cond_11
    const-string v4, "ro.csc.sales_code"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 693
    .local v23, "salesCode":Ljava/lang/String;
    const-string v4, "ATT"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 694
    new-instance v21, Ljava/io/File;

    const-string v4, "/efs/apn-changes.xml"

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 695
    .local v21, "persistentFile":Ljava/io/File;
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v28, 0x0

    cmp-long v4, v4, v28

    if-eqz v4, :cond_1

    .line 696
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://telephony/carriers/dm_change"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/16 v28, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v4, v5, v6, v9, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    .line 666
    .end local v18    # "intent":Landroid/content/Intent;
    .end local v21    # "persistentFile":Ljava/io/File;
    .end local v22    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v23    # "salesCode":Ljava/lang/String;
    :catchall_0
    move-exception v4

    if-eqz v13, :cond_12

    .line 667
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_12
    throw v4

    .line 677
    .restart local v22    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_13
    const-string v4, "CscSimProfile"

    const-string v5, "phone is not GSMPhone or null."

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 681
    :cond_14
    const-string v4, "CscSimProfile"

    const-string v5, "phone is not PhoneProxy or null."

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private show(ZZ)V
    .locals 29
    .param p1, "force"    # Z
    .param p2, "fotaupdate"    # Z

    .prologue
    .line 926
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v22

    .line 927
    .local v22, "subIds":[J
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v25

    const/16 v26, 0x0

    aget-wide v26, v22, v26

    invoke-virtual/range {v25 .. v27}, Landroid/telephony/TelephonyManager;->getSimOperator(J)Ljava/lang/String;

    move-result-object v14

    .line 928
    .local v14, "numeric":Ljava/lang/String;
    const-string v25, "ro.csc.sales_code"

    invoke-static/range {v25 .. v25}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 929
    .local v16, "salesCode":Ljava/lang/String;
    const/4 v12, 0x0

    .line 930
    .local v12, "isCscUpdatedValue":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getIMSI(I)Ljava/lang/String;

    move-result-object v11

    .line 932
    .local v11, "imsi":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->checkNwkInfoProvider()V

    .line 933
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUtil;->isCscUpdated()Z

    move-result v25

    if-nez v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "csc.preferences_name"

    const-string v27, "csc.key.already_executed"

    invoke-static/range {v25 .. v27}, Lcom/samsung/sec/android/application/csc/CscUtil;->getCscPreferenceValue(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_3

    :cond_0
    const/4 v12, 0x1

    .line 936
    :goto_0
    const-string v25, "CscSimProfile"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Show Profile [Operator : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " ] [Force : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " ] [SimChanged : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->isSimChanged(I)Z

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " ] + [CscUpdated : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " ] [isFotaUpdate : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "] [SimSlot : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "]"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_1c

    if-eqz v12, :cond_1c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->isSimChanged(I)Z

    move-result v25

    if-nez v25, :cond_1

    if-eqz p1, :cond_1c

    .line 988
    :cond_1
    const/16 v20, 0x0

    .local v20, "simSlot":I
    :goto_1
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v25

    move/from16 v0, v20

    move/from16 v1, v25

    if-ge v0, v1, :cond_5

    .line 989
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v25, v0

    move/from16 v0, v25

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsShowings:[Z

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    aget-boolean v25, v25, v26

    if-eqz v25, :cond_4

    .line 990
    const-string v25, "CscSimProfile"

    const-string v26, "Now Showing Dialog"

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1201
    .end local v20    # "simSlot":I
    :cond_2
    :goto_2
    return-void

    .line 933
    :cond_3
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 988
    .restart local v20    # "simSlot":I
    :cond_4
    add-int/lit8 v20, v20, 0x1

    goto :goto_1

    .line 995
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->isSimChanged(I)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 1000
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v25

    const-string v26, "CscFeature_RIL_UseMpsForImsSetting"

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 1002
    const-string v25, "CscSimProfile"

    const-string v26, "set CscFeature_RIL_UseMpsForImsSetting!! true"

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mMMCscParser:Lcom/samsung/sec/android/application/csc/MMCscParser;

    move-object/from16 v25, v0

    const-string v26, "eMBMS"

    const-string v27, "Supported"

    invoke-virtual/range {v25 .. v27}, Lcom/samsung/sec/android/application/csc/MMCscParser;->getEMBMSValueFromMPS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1029
    .local v4, "SsEMBMSSupport":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_7

    .line 1030
    const-string v25, "CscSimProfile"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "set SsEMBMSSupport!!:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    const-string v25, "persist.radio.embms.support"

    move-object/from16 v0, v25

    invoke-static {v0, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    .end local v4    # "SsEMBMSSupport":Ljava/lang/String;
    :cond_7
    if-eqz p1, :cond_8

    .line 1037
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->clearPreferences(I)V

    .line 1040
    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->createAllNetworkList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    .line 1042
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    .line 1043
    const-string v25, "CscSimProfile"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Network List Size: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    :cond_9
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_e

    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->isShowSimProfile()Z

    move-result v25

    if-nez v25, :cond_e

    .line 1047
    const-string v25, "CscSimProfile"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "No Sim Profile Pop-up in Special Operators : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    const-string v25, ""

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v14, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setProfile(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1051
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    if-eqz v25, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_d

    .line 1052
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->nwkname:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v14, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setPreferences(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1053
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->nwkname:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setCSCVoicemailnumber(Ljava/lang/String;I)V

    goto/16 :goto_2

    .line 1005
    :cond_a
    const-string v25, "CscSimProfile"

    const-string v26, "set CscFeature_RIL_UseMpsForImsSetting!! false or none."

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mMMCscParser:Lcom/samsung/sec/android/application/csc/MMCscParser;

    move-object/from16 v25, v0

    const-string v26, "VoLTE"

    const-string v27, "SsRoutingPolicy"

    invoke-virtual/range {v25 .. v27}, Lcom/samsung/sec/android/application/csc/MMCscParser;->getIMSValueFromMPS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1009
    .local v6, "SsRoutingPolicy":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_b

    .line 1010
    const-string v25, "CscSimProfile"

    const-string v26, "set SsRoutingPolicy!!"

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    const-string v25, "persist.radio.ss.policy"

    move-object/from16 v0, v25

    invoke-static {v0, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mMMCscParser:Lcom/samsung/sec/android/application/csc/MMCscParser;

    move-object/from16 v25, v0

    const-string v26, "VoLTE"

    const-string v27, "UssdRoutingPolicy"

    invoke-virtual/range {v25 .. v27}, Lcom/samsung/sec/android/application/csc/MMCscParser;->getIMSValueFromMPS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1015
    .local v7, "UssdRoutingPolicy":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_c

    .line 1016
    const-string v25, "CscSimProfile"

    const-string v26, "set UssdRoutingPolicy!!"

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    const-string v25, "persist.radio.ussd.policy"

    move-object/from16 v0, v25

    invoke-static {v0, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mMMCscParser:Lcom/samsung/sec/android/application/csc/MMCscParser;

    move-object/from16 v25, v0

    const-string v26, "VoLTE"

    const-string v27, "SsFallbackByImsError"

    invoke-virtual/range {v25 .. v27}, Lcom/samsung/sec/android/application/csc/MMCscParser;->getIMSValueFromMPS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1021
    .local v5, "SsFallbackByImsError":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_6

    .line 1022
    const-string v25, "CscSimProfile"

    const-string v26, "set SsFallbackByImsError!!"

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    const-string v25, "persist.radio.ss.fallback"

    move-object/from16 v0, v25

    invoke-static {v0, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1055
    .end local v5    # "SsFallbackByImsError":Ljava/lang/String;
    .end local v6    # "SsRoutingPolicy":Ljava/lang/String;
    .end local v7    # "UssdRoutingPolicy":Ljava/lang/String;
    :cond_d
    const-string v25, "CscSimProfile"

    const-string v26, "[Voicemail] allNetworkName is NULL. It does not send broadcast. "

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1060
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    if-eqz v25, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_19

    .line 1062
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v25

    const-string v26, "CscFeature_RIL_DisablePromptPopup4SIMProfileSelection"

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_12

    .line 1068
    const-string v25, "DCM"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_f

    const-string v25, "KDI"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_f

    const-string v25, "CTC"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_10

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNumerics:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_10

    .line 1070
    const-string v25, "CscSimProfile"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Don\'t init apn list : sales = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ", old = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNumerics:[Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v28, v0

    aget-object v27, v27, v28

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ", new : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1078
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    if-eqz v25, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_11

    .line 1079
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->nwkname:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v14, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setPreferences(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1080
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->nwkname:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setCSCVoicemailnumber(Ljava/lang/String;I)V

    goto/16 :goto_2

    .line 1073
    :cond_10
    const-string v25, ""

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v14, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setProfile(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_4

    .line 1082
    :cond_11
    const-string v25, "CscSimProfile"

    const-string v26, "[Voicemail] allNetworkName is NULL. It does not send broadcast. "

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1087
    :cond_12
    const-string v26, "CSL"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    const/16 v27, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->spname:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_16

    .line 1088
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/telephony/TelephonyManager;->getGroupIdLevel1()Ljava/lang/String;

    move-result-object v21

    .line 1089
    .local v21, "spcode":Ljava/lang/String;
    const-string v25, "CscSimProfile"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "[HK_CSL] mTelephonyManager.getGroupIdLevel1() = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/telephony/TelephonyManager;->getGroupIdLevel1()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    if-eqz v21, :cond_16

    .line 1091
    const-string v25, "01010000ffffffff"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_13

    .line 1092
    const-string v25, "1O1O"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v14, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setProfile(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1093
    const-string v25, "CscSimProfile"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "[HK_CSL] AUTOAPNSETTING found 1010 spcode = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1095
    :cond_13
    const-string v25, "01020000ffffffff"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_14

    .line 1096
    const-string v25, "csl."

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v14, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setProfile(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1097
    const-string v25, "CscSimProfile"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "[HK_CSL] AUTOAPNSETTING found csl. spcode = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1099
    :cond_14
    const-string v25, "01030000ffffffff"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_15

    .line 1100
    const-string v25, "SUN Mobile"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v14, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setProfile(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1101
    const-string v25, "CscSimProfile"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "[HK_CSL] AUTOAPNSETTING found SUN Mobile spcode = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1104
    :cond_15
    const-string v25, "CscSimProfile"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "[HK_CSL] AUTOAPNSETTING can not find proper GID apn,don\'t return,pop up service selection for spcode = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1108
    .end local v21    # "spcode":Ljava/lang/String;
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    move/from16 v0, v25

    new-array v13, v0, [Ljava/lang/String;

    .line 1110
    .local v13, "items":[Ljava/lang/String;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    move/from16 v0, v25

    if-ge v10, v0, :cond_17

    .line 1111
    new-instance v26, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->nwkname:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v26, v13, v10

    .line 1110
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 1114
    :cond_17
    const/4 v8, 0x0

    .line 1115
    .local v8, "d":Landroid/app/AlertDialog;
    const/16 v17, 0x4

    .line 1121
    .local v17, "sec_theme":I
    new-instance v23, Lcom/samsung/sec/android/application/csc/CscSimProfile$clickListener;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile$clickListener;-><init>(Lcom/samsung/sec/android/application/csc/CscSimProfile;I)V

    .line 1123
    .local v23, "tempClickListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v24, Lcom/samsung/sec/android/application/csc/CscSimProfile$dismissListener;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile$dismissListener;-><init>(Lcom/samsung/sec/android/application/csc/CscSimProfile;I)V

    .line 1126
    .local v24, "tempDismissListener":Landroid/content/DialogInterface$OnDismissListener;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkNameMultiSim:[Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    aput-object v27, v25, v26

    .line 1127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkNameMultiSim:[Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/ArrayList;

    aput-object v25, v26, v27

    .line 1130
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const v26, 0x7f040001

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 1131
    .local v19, "simServiceTitle":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_18

    .line 1132
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    add-int/lit8 v26, v26, 0x1

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 1135
    :cond_18
    new-instance v25, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v25

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v23

    invoke-virtual {v0, v13, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v25

    const v26, 0x104000a

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v25

    const/high16 v26, 0x1040000

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    .line 1140
    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1141
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1142
    invoke-virtual {v8}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v25

    const/16 v26, 0x7d8

    invoke-virtual/range {v25 .. v26}, Landroid/view/Window;->setType(I)V

    .line 1143
    invoke-virtual {v8}, Landroid/app/AlertDialog;->show()V

    .line 1144
    const/16 v25, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->isShowing(ZI)V

    .line 1145
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIndex:I

    goto/16 :goto_2

    .line 1147
    .end local v8    # "d":Landroid/app/AlertDialog;
    .end local v10    # "i":I
    .end local v13    # "items":[Ljava/lang/String;
    .end local v17    # "sec_theme":I
    .end local v19    # "simServiceTitle":Ljava/lang/String;
    .end local v23    # "tempClickListener":Landroid/content/DialogInterface$OnClickListener;
    .end local v24    # "tempDismissListener":Landroid/content/DialogInterface$OnDismissListener;
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    if-eqz v25, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1b

    .line 1148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkName:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    .line 1149
    .local v18, "selectedProfile":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    if-eqz v18, :cond_2

    .line 1150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNumerics:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1a

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->nwkname:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNwknames:[Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v27, v0

    aget-object v26, v26, v27

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1a

    .line 1153
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->nwkname:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->numeric:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setPreferences(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1159
    const-string v25, "CscSimProfile"

    const-string v26, "sim changed but same operator."

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNwknames:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setCSCVoicemailnumber(Ljava/lang/String;I)V

    goto/16 :goto_2

    .line 1162
    :cond_1a
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->nwkname:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->numeric:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setProfile(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_2

    .line 1167
    .end local v18    # "selectedProfile":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    :cond_1b
    const-string v25, "CscSimProfile"

    const-string v26, "No Network Name on DB. Set Profile as Empty"

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1181
    const-string v25, ""

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v14, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setProfile(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_2

    .line 1184
    .end local v20    # "simSlot":I
    :cond_1c
    const-string v25, "CscSimProfile"

    const-string v26, "Unable to Show Sim Profile"

    invoke-static/range {v25 .. v26}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_1d

    .line 1188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mCurrentNwknames:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setCSCVoicemailnumber(Ljava/lang/String;I)V

    .line 1191
    :cond_1d
    if-eqz v11, :cond_2

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v25

    if-lez v25, :cond_2

    .line 1193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "simprof.preferences_name"

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v15

    .line 1194
    .local v15, "pref":Landroid/content/SharedPreferences;
    if-eqz v15, :cond_2

    .line 1195
    invoke-interface {v15}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    .line 1196
    .local v9, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v25, "simprof.key.last_imsi"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-interface {v9, v0, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1197
    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_2
.end method


# virtual methods
.method public MultiSimShow(ZZ)V
    .locals 2
    .param p1, "force"    # Z
    .param p2, "fotaupdate"    # Z

    .prologue
    .line 919
    sget v1, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mSimSlotNum:I

    add-int/lit8 v0, v1, -0x1

    .local v0, "simCnt":I
    :goto_0
    if-ltz v0, :cond_0

    .line 920
    iput v0, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    .line 921
    invoke-direct {p0, p1, p2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->show(ZZ)V

    .line 919
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 923
    :cond_0
    return-void
.end method

.method public appendSimslotString(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "simSlot"    # I

    .prologue
    .line 1691
    if-eqz p2, :cond_0

    .line 1692
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1694
    :cond_0
    return-object p1
.end method

.method public clearSimProfile()V
    .locals 2

    .prologue
    .line 867
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->clearSimProfile(I)V

    .line 868
    const-string v0, "CscSimProfile"

    const-string v1, "clearSimProfile ALL"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    return-void
.end method

.method public clearSimProfile(I)V
    .locals 4
    .param p1, "simSlot"    # I

    .prologue
    .line 873
    const-string v1, "CscSimProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clearSimProfile simSlot:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 875
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 876
    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->sendMessage(Landroid/os/Message;)Z

    .line 878
    return-void
.end method

.method public getIMSI(I)Ljava/lang/String;
    .locals 4
    .param p1, "simSlot"    # I

    .prologue
    .line 397
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    .line 398
    .local v0, "subIds":[J
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    const/4 v2, 0x0

    aget-wide v2, v0, v2

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->getSubscriberId(J)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getSPName(I)Ljava/lang/String;
    .locals 6
    .param p1, "simSlot"    # I

    .prologue
    .line 364
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 365
    .local v1, "subIds":[J
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    const-string v2, "gsm.sim.operator.alpha"

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    const-string v3, ""

    invoke-static {v2, v4, v5, v3}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 367
    .local v0, "alpha":Ljava/lang/String;
    const-string v2, "CscSimProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSPName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", simSlot: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    return-object v0
.end method

.method public getSpcode(I)Ljava/lang/String;
    .locals 9
    .param p1, "simSlot"    # I

    .prologue
    .line 330
    const/4 v2, 0x0

    .line 331
    .local v2, "phoneContext":Landroid/content/Context;
    const-string v4, ""

    .line 332
    .local v4, "spcode":Ljava/lang/String;
    const-string v1, "key_gid1"

    .line 333
    .local v1, "key_gid":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 334
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "key"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, p1, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_gid1"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 337
    :cond_0
    const-string v6, "CscSimProfile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "key_gid : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :try_start_0
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    const-string v7, "com.android.phone"

    const/4 v8, 0x3

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 346
    :goto_0
    if-nez v2, :cond_1

    .line 347
    const-string v6, "CscSimProfile"

    const-string v7, "Unable to get phone context"

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v4

    .line 360
    .end local v4    # "spcode":Ljava/lang/String;
    .local v5, "spcode":Ljava/lang/String;
    :goto_1
    return-object v5

    .line 342
    .end local v5    # "spcode":Ljava/lang/String;
    .restart local v4    # "spcode":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 343
    .local v0, "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    goto :goto_0

    .line 351
    .end local v0    # "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 353
    .local v3, "preferences":Landroid/content/SharedPreferences;
    if-eqz v3, :cond_2

    .line 354
    const-string v6, "CscSimProfile"

    const-string v7, "get phone shared preference"

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v6, ""

    invoke-interface {v3, v1, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 359
    :cond_2
    const-string v6, "CscSimProfile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get spcode : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v4

    .line 360
    .end local v4    # "spcode":Ljava/lang/String;
    .restart local v5    # "spcode":Ljava/lang/String;
    goto :goto_1
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 262
    const-string v2, "CscSimProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "entering handleMessage with event "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 265
    :pswitch_0
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-ne v2, v6, :cond_1

    .line 266
    iget-boolean v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsForceShow:Z

    iget-boolean v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsFotaUpdate:Z

    invoke-virtual {p0, v2, v3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->MultiSimShow(ZZ)V

    goto :goto_0

    .line 268
    :cond_1
    iget v2, p1, Landroid/os/Message;->arg1:I

    iput v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsLoadProfileSIM:I

    .line 269
    iget-boolean v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsForceShow:Z

    iget-boolean v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsFotaUpdate:Z

    invoke-direct {p0, v2, v3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->show(ZZ)V

    goto :goto_0

    .line 273
    :pswitch_1
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-ne v2, v6, :cond_2

    .line 274
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget v2, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mSimSlotNum:I

    if-ge v1, v2, :cond_0

    .line 275
    invoke-direct {p0, v1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->clearProfile(I)V

    .line 274
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 278
    .end local v1    # "i":I
    :cond_2
    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->clearProfile(I)V

    goto :goto_0

    .line 282
    :pswitch_2
    const-string v2, "CscSimProfile"

    const-string v3, "[LTEON] EVENT_SET_PREFERRED_NETWORK_TYPE"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 285
    .local v0, "ar":Landroid/os/AsyncResult;
    const-string v2, "CscSimProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[LTEON] retryContext = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->retryContext:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    iget v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->retryContext:I

    const/16 v3, 0x1e

    if-ne v2, v3, :cond_3

    .line 287
    const-string v2, "CscSimProfile"

    const-string v3, "[LTEON] retryContext[0] tried 30 times"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    iput v5, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->retryContext:I

    goto :goto_0

    .line 292
    :cond_3
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_4

    .line 293
    const-string v2, "CscSimProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[LTEON] set preferred network type, exception="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const-wide/16 v2, 0x7d0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 300
    :goto_2
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setNetworkType()V

    .line 301
    iget v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->retryContext:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->retryContext:I

    goto/16 :goto_0

    .line 303
    :cond_4
    const-string v2, "CscSimProfile"

    const-string v3, "[LTEON] SET_PREFERRED_NETWORK_TYPE_GWL success"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iput v5, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->retryContext:I

    .line 305
    iget-boolean v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsDisconnectingData:Z

    if-eqz v2, :cond_0

    .line 306
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    .line 307
    iput-boolean v5, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsDisconnectingData:Z

    .line 308
    const-string v2, "CscSimProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set default vale, mIsDisconnectingData = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsDisconnectingData:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 315
    .end local v0    # "ar":Landroid/os/AsyncResult;
    :pswitch_3
    const-string v2, "CscSimProfile"

    const-string v3, "[LTEOON] EVENT_CHECK_NETWORKMODE_STATE"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setNetworkType()V

    goto/16 :goto_0

    .line 297
    .restart local v0    # "ar":Landroid/os/AsyncResult;
    :catch_0
    move-exception v2

    goto :goto_2

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onClickPro(Landroid/content/DialogInterface;II)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I
    .param p3, "slotID"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 810
    const/4 v2, -0x2

    if-ne p2, v2, :cond_0

    .line 811
    invoke-direct {p0, v5, p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->isShowing(ZI)V

    .line 813
    const-string v2, "CscSimProfile"

    const-string v3, "Dialog Negative Button Click"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    invoke-static {p3}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 815
    .local v1, "subIds":[J
    const-string v2, ""

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    aget-wide v4, v1, v5

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->getSimOperator(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setProfile(Ljava/lang/String;Ljava/lang/String;I)V

    .line 817
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 834
    .end local v1    # "subIds":[J
    :goto_0
    return-void

    .line 818
    :cond_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_2

    .line 819
    const-string v2, "CscSimProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Dialog Positive Button Click "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    const/4 v0, 0x0

    .line 821
    .local v0, "selectedProfile":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->allNetworkNameMultiSim:[Ljava/util/ArrayList;

    aget-object v2, v2, p3

    iget v3, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIndex:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "selectedProfile":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    check-cast v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;

    .line 822
    .restart local v0    # "selectedProfile":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    invoke-direct {p0, v5, p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->isShowing(ZI)V

    .line 824
    if-eqz v0, :cond_1

    .line 825
    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->nwkname:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->numeric:Ljava/lang/String;

    invoke-direct {p0, v2, v3, p3}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setProfile(Ljava/lang/String;Ljava/lang/String;I)V

    .line 826
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 827
    iget-object v2, v0, Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;->nwkname:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->sendDuplicatedNetworkIntent(Ljava/lang/String;)V

    .line 829
    :cond_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 831
    .end local v0    # "selectedProfile":Lcom/samsung/sec/android/application/csc/CscSimProfile$NetworkProfile;
    :cond_2
    const-string v2, "CscSimProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Select Dialog Index : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    iput p2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIndex:I

    goto :goto_0
.end method

.method public onDismissPro(I)V
    .locals 6
    .param p1, "slotID"    # I

    .prologue
    const/4 v3, 0x0

    .line 786
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsShowings:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_0

    .line 787
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsShowings:[Z

    aput-boolean v3, v1, p1

    .line 789
    :cond_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsShowings:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_1

    .line 790
    const-string v1, "CscSimProfile"

    const-string v2, "No Network Selected : Insert All Network Apns"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    .line 792
    .local v0, "subIds":[J
    const-string v1, ""

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    aget-wide v4, v0, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getSimOperator(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->setProfile(Ljava/lang/String;Ljava/lang/String;I)V

    .line 794
    .end local v0    # "subIds":[J
    :cond_1
    invoke-direct {p0, v3, p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->isShowing(ZI)V

    .line 795
    return-void
.end method

.method public showSimProfile(ZZ)V
    .locals 2
    .param p1, "force"    # Z
    .param p2, "fotaupdate"    # Z

    .prologue
    .line 851
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->showSimProfile(ZZI)V

    .line 852
    const-string v0, "CscSimProfile"

    const-string v1, "showSimProfile ALL"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    return-void
.end method

.method public showSimProfile(ZZI)V
    .locals 4
    .param p1, "force"    # Z
    .param p2, "fotaupdate"    # Z
    .param p3, "simSlot"    # I

    .prologue
    .line 857
    const-string v1, "CscSimProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showSimProfile simSlot:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    iput-boolean p1, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsForceShow:Z

    .line 859
    iput-boolean p2, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mIsFotaUpdate:Z

    .line 860
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 861
    .local v0, "msg":Landroid/os/Message;
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 862
    invoke-virtual {p0, v0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->sendMessage(Landroid/os/Message;)Z

    .line 864
    return-void
.end method

.method public updateForHomeFOTA(IZ)V
    .locals 3
    .param p1, "buildType"    # I
    .param p2, "isNewCscEdtion"    # Z

    .prologue
    .line 1684
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CSC_SHOW_SIM_PROFILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1685
    .local v0, "simProfileIntent":Landroid/content/Intent;
    const-string v1, "fotaupdate"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1686
    const-string v1, "force"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1687
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscSimProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1688
    return-void
.end method
