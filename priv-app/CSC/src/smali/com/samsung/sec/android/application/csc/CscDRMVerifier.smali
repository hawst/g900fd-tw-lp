.class public Lcom/samsung/sec/android/application/csc/CscDRMVerifier;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscDRMVerifier.java"


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field private final PRECONFIGED_CUSTOMER_FLAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 16
    const-string v0, "CscDRMVerifier"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscDRMVerifier;->LOG_TAG:Ljava/lang/String;

    .line 18
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscDRMVerifier;->PRECONFIGED_CUSTOMER_FLAG:Ljava/lang/String;

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscDRMVerifier;->mResolver:Landroid/content/ContentResolver;

    .line 36
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscDRMVerifier;->mContext:Landroid/content/Context;

    .line 37
    return-void
.end method


# virtual methods
.method public update()V
    .locals 6

    .prologue
    .line 41
    const-string v3, "CscDRMVerifier"

    const-string v4, "DRM Checking Start...!!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscDRMVerifier;->PRECONFIGED_CUSTOMER_FLAG:Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 44
    .local v1, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    if-nez v1, :cond_0

    .line 45
    const-string v3, "CscDRMVerifier"

    const-string v4, "update memory alloc fail"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :cond_0
    const-string v3, "DRM.CMLA"

    invoke-virtual {v1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "DRMValue_CMLA":Ljava/lang/String;
    const-string v3, "CscDRMVerifier"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DRMValue_CMLA :: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    new-instance v2, Lcom/samsung/sec/android/jni/hdcp/HdcpKey;

    invoke-direct {v2}, Lcom/samsung/sec/android/jni/hdcp/HdcpKey;-><init>()V

    .line 53
    .local v2, "keyInstaller":Lcom/samsung/sec/android/jni/hdcp/HdcpKey;
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscDRMVerifier;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/jni/hdcp/HdcpKey;->installKey(Landroid/content/Context;)I

    .line 55
    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 60
    const-string v3, "CscDRMVerifier"

    const-string v4, "Calling installOmaCerts"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    :cond_1
    const-string v3, "CscDRMVerifier"

    const-string v4, "DRM Setting Update Complete!!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    return-void
.end method
