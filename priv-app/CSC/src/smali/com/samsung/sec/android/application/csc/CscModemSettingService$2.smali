.class Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;
.super Landroid/os/Handler;
.source "CscModemSettingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscModemSettingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;


# direct methods
.method constructor <init>(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V
    .locals 0

    .prologue
    .line 803
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v8, 0x7d1

    const/16 v6, 0x64

    const/4 v7, 0x0

    .line 805
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "error"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 806
    .local v2, "error":I
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 918
    :cond_0
    :goto_0
    return-void

    .line 809
    :pswitch_0
    const-string v4, "CscModemSettingService"

    const-string v5, "SEND_PRECONFIG_DONE response incoming!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    if-eqz v2, :cond_1

    .line 812
    const-string v4, "CscModemSettingService"

    const-string v5, "AsyncResult Exception Occur!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I
    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$400(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)I

    move-result v4

    if-ge v4, v6, :cond_0

    .line 815
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 816
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    invoke-virtual {v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->sendSalesCode()V

    .line 817
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # operator++ for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I
    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$408(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 818
    :catch_0
    move-exception v0

    .line 819
    .local v0, "e":Ljava/lang/InterruptedException;
    goto :goto_0

    .line 824
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # setter for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I
    invoke-static {v4, v7}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$402(Lcom/samsung/sec/android/application/csc/CscModemSettingService;I)I

    .line 825
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # invokes: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->readModemInfo()V
    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$500(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)V

    .line 826
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # setter for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mItemCount:I
    invoke-static {v4, v7}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$602(Lcom/samsung/sec/android/application/csc/CscModemSettingService;I)I

    .line 827
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v4, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    .line 828
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    invoke-virtual {v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->addModemInfoToBuffer()V

    .line 830
    const-string v4, "CscModemSettingService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateModemInfo() : mByteArray Size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    iget-object v6, v6, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    const/4 v5, 0x7

    invoke-virtual {v4, v5, v7, v8}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->sendByteBuffer(III)V

    goto :goto_0

    .line 837
    :pswitch_1
    const-string v4, "CscModemSettingService"

    const-string v5, "SEND_MODEM_UPDATE_DONE response incoming!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    if-eqz v2, :cond_2

    .line 840
    const-string v4, "CscModemSettingService"

    const-string v5, "AsyncResult Exception Occur!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I
    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$400(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)I

    move-result v4

    if-ge v4, v6, :cond_0

    .line 843
    const-wide/16 v4, 0x1f4

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 844
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    iget v5, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    const/16 v7, 0x7d1

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->sendByteBuffer(III)V

    .line 845
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # operator++ for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I
    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$408(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 846
    :catch_1
    move-exception v0

    .line 847
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    goto/16 :goto_0

    .line 852
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # setter for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I
    invoke-static {v4, v7}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$402(Lcom/samsung/sec/android/application/csc/CscModemSettingService;I)I

    .line 861
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    invoke-virtual {v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->sendResetMsg()V

    goto/16 :goto_0

    .line 866
    :pswitch_2
    const-string v4, "CscModemSettingService"

    const-string v5, "SEND_MODEM_COMPARE_DONE response incoming!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    if-eqz v2, :cond_0

    .line 869
    const-string v4, "CscModemSettingService"

    const-string v5, "AsyncResult Exception Occur!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 875
    :pswitch_3
    const-string v4, "CscModemSettingService"

    const-string v5, "SEND_RESET_DONE response incoming!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    if-eqz v2, :cond_3

    .line 878
    # getter for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->isMarvell:Z
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$700()Z

    move-result v4

    if-nez v4, :cond_4

    .line 879
    const-string v4, "CscModemSettingService"

    const-string v5, "AsyncResult Exception Occur!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I
    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$400(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)I

    move-result v4

    if-ge v4, v6, :cond_0

    .line 882
    const-wide/16 v4, 0x1f4

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 883
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    invoke-virtual {v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->sendResetMsg()V

    .line 884
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # operator++ for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I
    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$408(Lcom/samsung/sec/android/application/csc/CscModemSettingService;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 885
    :catch_2
    move-exception v0

    .line 886
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    goto/16 :goto_0

    .line 892
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_3
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    # setter for: Lcom/samsung/sec/android/application/csc/CscModemSettingService;->mRetryCount:I
    invoke-static {v4, v7}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->access$402(Lcom/samsung/sec/android/application/csc/CscModemSettingService;I)I

    .line 901
    :cond_4
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscModemSettingService$2;->this$0:Lcom/samsung/sec/android/application/csc/CscModemSettingService;

    const-string v5, "csc.preferences_name"

    invoke-virtual {v4, v5, v7}, Lcom/samsung/sec/android/application/csc/CscModemSettingService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 903
    .local v3, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 904
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "csc.key.csc_modem_setting_done"

    const/4 v5, 0x1

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 905
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 910
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "preferences":Landroid/content/SharedPreferences;
    :pswitch_4
    const-string v4, "CscModemSettingService"

    const-string v5, "SEND_MODEM_UPDATE_DONE_MARVELL"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    if-eqz v2, :cond_0

    .line 912
    const-string v4, "CscModemSettingService"

    const-string v5, "AsyncResult Exception Occur!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 806
    nop

    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
