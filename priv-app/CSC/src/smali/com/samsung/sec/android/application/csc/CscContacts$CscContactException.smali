.class Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;
.super Landroid/util/AndroidException;
.source "CscContacts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscContacts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CscContactException"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mStrTrace:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscContacts;


# direct methods
.method public constructor <init>(Lcom/samsung/sec/android/application/csc/CscContacts;Ljava/lang/String;)V
    .locals 0
    .param p2, "strTrace"    # Ljava/lang/String;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->this$0:Lcom/samsung/sec/android/application/csc/CscContacts;

    .line 155
    invoke-direct {p0}, Landroid/util/AndroidException;-><init>()V

    .line 156
    invoke-virtual {p0, p2}, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->setTrace(Ljava/lang/String;)V

    .line 157
    return-void
.end method


# virtual methods
.method public getTrace()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->mStrTrace:Ljava/lang/String;

    return-object v0
.end method

.method public setTrace(Ljava/lang/String;)V
    .locals 0
    .param p1, "strTrace"    # Ljava/lang/String;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscContacts$CscContactException;->mStrTrace:Ljava/lang/String;

    .line 161
    return-void
.end method
