.class public Lcom/samsung/sec/android/application/csc/CscReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CscReceiver.java"


# static fields
.field static APPinstallingDone:Z

.field private static FILE_NAME:Ljava/lang/String;

.field static currentMccMnc:Ljava/lang/String;

.field private static mCompleteMsgAlreadySent:Z

.field static mIsAPNRestore:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getIDPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscReceiver;->FILE_NAME:Ljava/lang/String;

    .line 59
    sput-boolean v1, Lcom/samsung/sec/android/application/csc/CscReceiver;->mCompleteMsgAlreadySent:Z

    .line 63
    sput-boolean v1, Lcom/samsung/sec/android/application/csc/CscReceiver;->mIsAPNRestore:Z

    .line 65
    sput-boolean v1, Lcom/samsung/sec/android/application/csc/CscReceiver;->APPinstallingDone:Z

    .line 67
    const-string v0, ""

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscReceiver;->currentMccMnc:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private isCscImage()Z
    .locals 2

    .prologue
    .line 260
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getIDPath()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/sec/android/application/csc/CscReceiver;->FILE_NAME:Ljava/lang/String;

    .line 264
    sget-object v1, Lcom/samsung/sec/android/application/csc/CscReceiver;->FILE_NAME:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 265
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/samsung/sec/android/application/csc/CscReceiver;->FILE_NAME:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 266
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 269
    .end local v0    # "f":Ljava/io/File;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 72
    if-nez p1, :cond_1

    .line 73
    const-string v15, "CscReceiver"

    const-string v16, "context is null"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    if-nez p2, :cond_2

    .line 78
    const-string v15, "CscReceiver"

    const-string v16, "intent is null"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 82
    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 83
    .local v3, "action":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 84
    const-string v15, "CscReceiver"

    const-string v16, "action is null"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 87
    :cond_3
    const-string v15, "CscReceiver"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "onReceive "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const-string v15, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 91
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v15}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v7

    .line 92
    .local v7, "host":Ljava/lang/String;
    const-string v15, "CscReceiver"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "[CscVerifier]SECRET_CODE, Host : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const-string v15, "9920"

    invoke-virtual {v15, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 95
    new-instance v8, Landroid/content/Intent;

    const-string v15, "android.intent.action.MAIN"

    invoke-direct {v8, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 96
    .local v8, "i":Landroid/content/Intent;
    const-class v15, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    move-object/from16 v0, p1

    invoke-virtual {v8, v0, v15}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 97
    const/high16 v15, 0x10000000

    invoke-virtual {v8, v15}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 98
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 104
    .end local v7    # "host":Ljava/lang/String;
    .end local v8    # "i":Landroid/content/Intent;
    :cond_4
    const-string v15, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 106
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sec/android/application/csc/CscReceiver;->isCscImage()Z

    move-result v15

    if-nez v15, :cond_5

    .line 107
    const-string v15, "CscReceiver"

    const-string v16, "This is not for CSC"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 119
    :cond_5
    new-instance v15, Landroid/content/Intent;

    const-class v16, Lcom/samsung/sec/android/application/csc/CscUpdateService;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 120
    :cond_6
    const-string v15, "android.intent.action.CSC_UPDATE_TEST"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 122
    new-instance v15, Landroid/content/Intent;

    const-class v16, Lcom/samsung/sec/android/application/csc/CscUpdateService;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v16, "MODE"

    const-string v17, "TEST"

    invoke-virtual/range {v15 .. v17}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 124
    :cond_7
    const-string v15, "android.intent.action.CSC_ENCODE"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 126
    new-instance v15, Landroid/content/Intent;

    const-class v16, Lcom/samsung/sec/android/application/csc/CscEncodeService;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 127
    :cond_8
    const-string v15, "android.intent.action.CSC_SHOW_SIM_PROFILE"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 130
    const-string v15, "force"

    const/16 v16, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 131
    .local v9, "isForceShow":Z
    const-string v15, "fotaupdate"

    const/16 v16, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 133
    .local v10, "isFotaUpdate":Z
    const-string v15, "CscReceiver"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "CSC_SHOW_SIM_PROFILE : mIsAPNRestore = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget-boolean v17, Lcom/samsung/sec/android/application/csc/CscReceiver;->mIsAPNRestore:Z

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", isForceShow = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    sget-boolean v15, Lcom/samsung/sec/android/application/csc/CscReceiver;->mIsAPNRestore:Z

    if-eqz v15, :cond_9

    .line 137
    const/4 v9, 0x1

    .line 138
    const/4 v15, 0x0

    sput-boolean v15, Lcom/samsung/sec/android/application/csc/CscReceiver;->mIsAPNRestore:Z

    .line 142
    :cond_9
    const-string v15, "simSlot"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 143
    invoke-static/range {p1 .. p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscSimProfile;

    move-result-object v15

    const-string v16, "simSlot"

    const/16 v17, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v15, v9, v10, v0}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->showSimProfile(ZZI)V

    goto/16 :goto_0

    .line 146
    :cond_a
    invoke-static/range {p1 .. p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscSimProfile;

    move-result-object v15

    invoke-virtual {v15, v9, v10}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->showSimProfile(ZZ)V

    goto/16 :goto_0

    .line 149
    .end local v9    # "isForceShow":Z
    .end local v10    # "isFotaUpdate":Z
    :cond_b
    const-string v15, "android.intent.action.CSC_CLEAR_SIM_PROFILE"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 150
    const-string v15, "simSlot"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 151
    invoke-static/range {p1 .. p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscSimProfile;

    move-result-object v15

    const-string v16, "simSlot"

    const/16 v17, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->clearSimProfile(I)V

    goto/16 :goto_0

    .line 154
    :cond_c
    invoke-static/range {p1 .. p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscSimProfile;

    move-result-object v15

    invoke-virtual {v15}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->clearSimProfile()V

    goto/16 :goto_0

    .line 156
    :cond_d
    const-string v15, "android.intent.action.CSC_UPDATE_HOMEURL"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 158
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v15

    const-string v16, "CscFeature_Web_EnableAutoSimHomeUrlInProfile"

    const/16 v17, 0x1

    invoke-virtual/range {v15 .. v17}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 160
    const-string v15, "homeurl"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 162
    .local v6, "homeurl":Ljava/lang/String;
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscBrowser;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Lcom/samsung/sec/android/application/csc/CscBrowser;-><init>(Landroid/content/Context;)V

    .line 163
    .local v4, "cscBrowser":Lcom/samsung/sec/android/application/csc/CscBrowser;
    invoke-virtual {v4, v6}, Lcom/samsung/sec/android/application/csc/CscBrowser;->setHomeUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 166
    .end local v4    # "cscBrowser":Lcom/samsung/sec/android/application/csc/CscBrowser;
    .end local v6    # "homeurl":Ljava/lang/String;
    :cond_e
    const-string v15, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_11

    .line 167
    const-string v15, "ss"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 168
    .local v14, "szSimState":Ljava/lang/String;
    const-string v15, "CscReceiver"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Recieve Sim State Changed : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const-string v15, "LOADED"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 172
    new-instance v5, Lcom/samsung/sec/android/application/csc/CscFota;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lcom/samsung/sec/android/application/csc/CscFota;-><init>(Landroid/content/Context;)V

    .line 173
    .local v5, "cscFota":Lcom/samsung/sec/android/application/csc/CscFota;
    invoke-virtual {v5}, Lcom/samsung/sec/android/application/csc/CscFota;->update()V

    .line 205
    .end local v5    # "cscFota":Lcom/samsung/sec/android/application/csc/CscFota;
    :cond_f
    const-string v15, "LOADED"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_10

    const-string v15, "READY"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 207
    :cond_10
    invoke-static/range {p1 .. p1}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->getInstance(Landroid/content/Context;)Lcom/samsung/sec/android/application/csc/CscSimProfile;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Lcom/samsung/sec/android/application/csc/CscSimProfile;->showSimProfile(ZZ)V

    goto/16 :goto_0

    .line 209
    .end local v14    # "szSimState":Ljava/lang/String;
    :cond_11
    const-string v15, "android.intent.action.CSC_UPDATE_CONNECTION"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_15

    .line 212
    const-string v15, "OPERATION"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_13

    .line 213
    const-string v15, "OPERATION"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 214
    .local v12, "strOper":Ljava/lang/String;
    const-string v15, "ignore"

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_12

    .line 215
    const/4 v15, 0x1

    sput-boolean v15, Lcom/samsung/sec/android/application/csc/CscReceiver;->mIsAPNRestore:Z

    .line 220
    .end local v12    # "strOper":Ljava/lang/String;
    :cond_12
    :goto_1
    const-string v15, "simSlot"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_14

    .line 221
    const-string v15, "CscReceiver"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "CSC_UPDATE_CONNECTION simSlot:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "simSlot"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    new-instance v15, Landroid/content/Intent;

    const-class v16, Lcom/samsung/sec/android/application/csc/CscUpdateService;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v16, "MODE"

    const-string v17, "CSCCONNECTION"

    invoke-virtual/range {v15 .. v17}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v15

    const-string v16, "simSlot"

    const-string v17, "simSlot"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v15 .. v17}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 217
    :cond_13
    const/4 v15, 0x1

    sput-boolean v15, Lcom/samsung/sec/android/application/csc/CscReceiver;->mIsAPNRestore:Z

    goto :goto_1

    .line 228
    :cond_14
    new-instance v15, Landroid/content/Intent;

    const-class v16, Lcom/samsung/sec/android/application/csc/CscUpdateService;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v16, "MODE"

    const-string v17, "CSCCONNECTION"

    invoke-virtual/range {v15 .. v17}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 231
    :cond_15
    const-string v15, "android.intent.action.PREINSTALLER_FINISH"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_16

    .line 232
    const/4 v15, 0x1

    sput-boolean v15, Lcom/samsung/sec/android/application/csc/CscReceiver;->APPinstallingDone:Z

    .line 233
    new-instance v15, Landroid/content/Intent;

    const-class v16, Lcom/samsung/sec/android/application/csc/CscUpdateService;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v16, "MODE"

    const-string v17, "CSCPREFERREDPACKAGE"

    invoke-virtual/range {v15 .. v17}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 242
    :cond_16
    const-string v15, "android.intent.action.CSC_CHAMELEON"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 243
    const-string v15, "CscReceiver"

    const-string v16, "start service to set Chameleon feature"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const/4 v11, 0x0

    .line 245
    .local v11, "reboot":Z
    const-string v15, "REBOOT"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_17

    .line 246
    const-string v15, "REBOOT"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 247
    .local v13, "strReboot":Ljava/lang/String;
    const-string v15, "TRUE"

    invoke-virtual {v15, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_17

    .line 248
    const/4 v11, 0x1

    .line 251
    .end local v13    # "strReboot":Ljava/lang/String;
    :cond_17
    new-instance v8, Landroid/content/Intent;

    const-class v15, Lcom/samsung/sec/android/application/csc/CscUpdateService;

    move-object/from16 v0, p1

    invoke-direct {v8, v0, v15}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 252
    .restart local v8    # "i":Landroid/content/Intent;
    const-string v15, "MODE"

    const-string v16, "CSCCHAMELEON"

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    const-string v15, "REBOOT"

    invoke-virtual {v8, v15, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 254
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method
