.class Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;
.super Ljava/lang/Object;
.source "CscTgManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscTgManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CscTgItem"
.end annotation


# instance fields
.field modemOnly:Z

.field pkgName:Ljava/lang/String;

.field tgObject:Lcom/samsung/sec/android/application/csc/CscTgBase;


# direct methods
.method private constructor <init>(Lcom/samsung/sec/android/application/csc/CscTgBase;ZLjava/lang/String;)V
    .locals 0
    .param p1, "cscTgObject"    # Lcom/samsung/sec/android/application/csc/CscTgBase;
    .param p2, "modemOnly"    # Z
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;->tgObject:Lcom/samsung/sec/android/application/csc/CscTgBase;

    .line 40
    iput-boolean p2, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;->modemOnly:Z

    .line 41
    iput-object p3, p0, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;->pkgName:Ljava/lang/String;

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/sec/android/application/csc/CscTgBase;ZLjava/lang/String;Lcom/samsung/sec/android/application/csc/CscTgManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/sec/android/application/csc/CscTgBase;
    .param p2, "x1"    # Z
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Lcom/samsung/sec/android/application/csc/CscTgManager$1;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/sec/android/application/csc/CscTgManager$CscTgItem;-><init>(Lcom/samsung/sec/android/application/csc/CscTgBase;ZLjava/lang/String;)V

    return-void
.end method
