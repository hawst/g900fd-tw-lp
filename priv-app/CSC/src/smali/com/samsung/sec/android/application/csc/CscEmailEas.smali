.class public Lcom/samsung/sec/android/application/csc/CscEmailEas;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscEmailEas.java"


# static fields
.field static final KEY_CSC:[Ljava/lang/String;

.field static final KEY_PRFS:[Ljava/lang/String;

.field static final USER_KEY_PRFS:[Ljava/lang/String;


# instance fields
.field private final CSCDATA_FILENAME:Ljava/lang/String;

.field private final CSC_FILE:Ljava/lang/String;

.field private final OTHERS_FILE:Ljava/lang/String;

.field private final PATH_MESSAGETONE:Ljava/lang/String;

.field private final TAG_CHECK_MESSAGETONE:Ljava/lang/String;

.field private final TAG_MESSAGETONE:Ljava/lang/String;

.field private csc_context:Landroid/content/Context;

.field private sPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Settings.Messages.Email.AutoAdvance"

    aput-object v1, v0, v3

    const-string v1, "Settings.Messages.Email.ConfirmDelete"

    aput-object v1, v0, v4

    const-string v1, "Settings.Messages.Email.EmailSending.AutoResendTime"

    aput-object v1, v0, v5

    const-string v1, "Settings.Messages.Email.EmailReceiving.PollTime"

    aput-object v1, v0, v6

    const-string v1, "Settings.ActiveSync.UseSSL"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Settings.ActiveSync.AcceptAllSSLCert"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Settings.ActiveSync.SyncCalendar"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Settings.ActiveSync.SyncContacts"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Settings.ActiveSync.RoamingSetting"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Settings.ActiveSync.Signature"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Settings.ActiveSync.SyncSchedule.PeakDuration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Settings.ActiveSync.SyncSchedule.OffPeakDuration"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Settings.ActiveSync.SyncSchedule.PeakDayStart"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Settings.ActiveSync.SyncSchedule.PeakDayEnd"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Settings.ActiveSync.SyncSchedule.PeakTimeStart"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "Settings.ActiveSync.SyncSchedule.PeakTimeEnd"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Settings.ActiveSync.EasEmail.PeriodEmail"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Settings.ActiveSync.EasEmail.SizeEmail"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Settings.ActiveSync.EasCalendar.PeriodCalendar"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "Settings.ActiveSync.SyncTasks"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "Settings.ActiveSync.SyncSms"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "Settings.Sound.MessageTone"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_CSC:[Ljava/lang/String;

    .line 59
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "csc_pref_key_Settings_Messages_Email_AutoAdvance"

    aput-object v1, v0, v3

    const-string v1, "csc_pref_key_Settings_Messages_Email_ConfirmDelete"

    aput-object v1, v0, v4

    const-string v1, "csc_pref_key_Settings_Messages_Email_EmailSending_AutoResendTime"

    aput-object v1, v0, v5

    const-string v1, "csc_pref_key_Settings_Messages_Email_EmailReceiving_PollTime"

    aput-object v1, v0, v6

    const-string v1, "csc_pref_key_Settings_ActiveSync_UseSSL"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "csc_pref_key_Settings_ActiveSync_AcceptAllSSLCert"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "csc_pref_key_Settings_ActiveSync_SyncCalendar"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "csc_pref_key_Settings_ActiveSync_SyncContacts"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "csc_pref_key_Settings_ActiveSync_RoamingSetting"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "csc_pref_key_Settings_ActiveSync_Signature"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "csc_pref_key_Settings_ActiveSync_SyncSchedule_PeakDuration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "csc_pref_key_Settings_ActiveSync_SyncSchedule_OffPeakDuration"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "csc_pref_key_Settings_ActiveSync_SyncSchedule_PeakDayStart"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "csc_pref_key_Settings_ActiveSync_SyncSchedule_PeakDayEnd"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "csc_pref_key_Settings_ActiveSync_SyncSchedule_PeakTimeStart"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "csc_pref_key_Settings_ActiveSync_SyncSchedule_PeakTimeEnd"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "csc_pref_key_Settings_ActiveSync_EasEmail_PeriodEmail"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "csc_pref_key_Settings_ActiveSync_EasEmail_SizeEmail"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "csc_pref_key_Settings_ActiveSync_EasCalendar_PeriodCalendar"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "csc_pref_key_Settings_ActiveSync_SyncTasks"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "csc_pref_key_Settings_ActiveSync_SyncSms"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "csc_pref_key_Settings_Sound_MessageTone"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_PRFS:[Ljava/lang/String;

    .line 84
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "pref_key_Settings_Messages_Email_AutoAdvance"

    aput-object v1, v0, v3

    const-string v1, "pref_key_Settings_Messages_Email_ConfirmDelete"

    aput-object v1, v0, v4

    const-string v1, "pref_key_Settings_Messages_Email_EmailSending_AutoResendTime"

    aput-object v1, v0, v5

    const-string v1, "pref_key_Settings_Messages_Email_EmailReceiving_PollTime"

    aput-object v1, v0, v6

    const-string v1, "pref_key_Settings_ActiveSync_UseSSL"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "pref_key_Settings_ActiveSync_AcceptAllSSLCert"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "pref_key_Settings_ActiveSync_SyncCalendar"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "pref_key_Settings_ActiveSync_SyncContacts"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "pref_key_Settings_ActiveSync_RoamingSetting"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "pref_key_Settings_ActiveSync_Signature"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "pref_key_Settings_ActiveSync_SyncSchedule_PeakDuration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "pref_key_Settings_ActiveSync_SyncSchedule_OffPeakDuration"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "pref_key_Settings_ActiveSync_SyncSchedule_PeakDayStart"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "pref_key_Settings_ActiveSync_SyncSchedule_PeakDayEnd"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "pref_key_Settings_ActiveSync_SyncSchedule_PeakTimeStart"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "pref_key_Settings_ActiveSync_SyncSchedule_PeakTimeEnd"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "pref_key_Settings_ActiveSync_EasEmail_PeriodEmail"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "pref_key_Settings_ActiveSync_EasEmail_SizeEmail"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "pref_key_Settings_ActiveSync_EasCalendar_PeriodCalendar"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "pref_key_Settings_ActiveSync_EasCalendar_SyncTasks"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "pref_key_Settings_ActiveSync_EasCalendar_SyncSms"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "pref_key_Settings_Sound_MessageTone"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->USER_KEY_PRFS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 27
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->CSC_FILE:Ljava/lang/String;

    .line 29
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->OTHERS_FILE:Ljava/lang/String;

    .line 31
    const-string v0, "CSCDATA_EmailEAS"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->CSCDATA_FILENAME:Ljava/lang/String;

    .line 33
    const-string v0, "Settings.Sound."

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->PATH_MESSAGETONE:Ljava/lang/String;

    .line 35
    const-string v0, "MessageTone"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->TAG_MESSAGETONE:Ljava/lang/String;

    .line 37
    const-string v0, "Settings.Sound.MessageTone"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->TAG_CHECK_MESSAGETONE:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->sPrefs:Landroid/content/SharedPreferences;

    .line 110
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->csc_context:Landroid/content/Context;

    .line 111
    return-void
.end method

.method private getMsgTone(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 10
    .param p1, "mParser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 232
    const-string v7, "Settings.Sound."

    invoke-virtual {p1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 233
    .local v0, "MsgToneNode":Lorg/w3c/dom/Node;
    const/4 v5, 0x0

    .line 235
    .local v5, "mSrc":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 255
    :cond_0
    :goto_0
    return-object v6

    .line 238
    :cond_1
    const-string v7, "MessageTone"

    invoke-virtual {p1, v0, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 240
    .local v2, "mCustomerList":Lorg/w3c/dom/NodeList;
    if-eqz v2, :cond_2

    .line 241
    invoke-interface {v2, v9}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Element;

    .line 242
    .local v1, "list":Lorg/w3c/dom/Element;
    const-string v7, "src"

    invoke-interface {v1, v7}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 245
    .end local v1    # "list":Lorg/w3c/dom/Element;
    :cond_2
    if-eqz v5, :cond_0

    .line 248
    const-string v7, "/"

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 250
    .local v4, "mSplitedPath":[Ljava/lang/String;
    array-length v7, v4

    add-int/lit8 v7, v7, -0x1

    aget-object v7, v4, v7

    const-string v8, "[.]"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 252
    .local v3, "mFileName":[Ljava/lang/String;
    array-length v7, v3

    if-lez v7, :cond_0

    .line 255
    aget-object v6, v3, v9

    goto :goto_0
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Main.Network.AutoEmailAccount"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 218
    const-string v0, "Settings.Messages.Email.EmailReceiving.AutoPoll"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 219
    const-string v0, "Settings.Messages.Email.EmailReceiving.KeepServer"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 15

    .prologue
    .line 136
    const/4 v6, 0x0

    .line 137
    .local v6, "Cscdata_Str":Ljava/lang/String;
    const/4 v7, 0x0

    .line 138
    .local v7, "EmailEasdata_Str":Ljava/lang/String;
    const-string v8, "NOERROR"

    .line 141
    .local v8, "compare_result":Ljava/lang/String;
    const/4 v12, 0x0

    .line 146
    .local v12, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :try_start_0
    new-instance v13, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->CSC_FILE:Ljava/lang/String;

    invoke-direct {v13, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 158
    .end local v12    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .local v13, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :try_start_1
    const-string v2, "content://com.android.email.service.PreferenceProvider/listall"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 160
    .local v1, "EmailEAS_PREFERENCE":Landroid/net/Uri;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->csc_context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 161
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 163
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_4

    .line 164
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 165
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 166
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-ge v11, v2, :cond_3

    .line 167
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_CSC:[Ljava/lang/String;

    aget-object v14, v2, v11

    .line 169
    .local v14, "source_value":Ljava/lang/String;
    const-string v2, "Settings.Sound.MessageTone"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    if-eqz v14, :cond_1

    .line 171
    invoke-direct {p0, v13}, Lcom/samsung/sec/android/application/csc/CscEmailEas;->getMsgTone(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v6

    .line 172
    const/4 v2, 0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 178
    :goto_1
    if-eqz v6, :cond_0

    .line 179
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 180
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_CSC:[Ljava/lang/String;

    aget-object v2, v2, v11

    invoke-static {v2, v6, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_CSC:[Ljava/lang/String;

    aget-object v3, v3, v11

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 182
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CscEmailEas : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 187
    :cond_0
    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    .line 166
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 174
    :cond_1
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_CSC:[Ljava/lang/String;

    aget-object v2, v2, v11

    invoke-virtual {v13, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 175
    const/4 v2, 0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 185
    :cond_2
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_CSC:[Ljava/lang/String;

    aget-object v2, v2, v11

    invoke-static {v2, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 208
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    .end local v1    # "EmailEAS_PREFERENCE":Landroid/net/Uri;
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v11    # "i":I
    .end local v14    # "source_value":Ljava/lang/String;
    :catch_0
    move-exception v10

    move-object v12, v13

    .line 209
    .end local v13    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .local v10, "ex":Ljava/lang/Exception;
    .restart local v12    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :goto_3
    const-string v2, "CscEmailEas : NOCSC"

    invoke-static {v2}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 210
    const-string v2, "NOCSC_EmailEas"

    .end local v10    # "ex":Ljava/lang/Exception;
    :goto_4
    return-object v2

    .line 190
    .end local v12    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v0    # "resolver":Landroid/content/ContentResolver;
    .restart local v1    # "EmailEAS_PREFERENCE":Landroid/net/Uri;
    .restart local v9    # "cursor":Landroid/database/Cursor;
    .restart local v13    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_3
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 192
    :cond_4
    const-string v2, "NOERROR"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v12, v13

    .end local v13    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v12    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto :goto_4

    .line 208
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    .end local v1    # "EmailEAS_PREFERENCE":Landroid/net/Uri;
    .end local v9    # "cursor":Landroid/database/Cursor;
    :catch_1
    move-exception v10

    goto :goto_3
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 0
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 228
    return-void
.end method

.method public update()V
    .locals 9

    .prologue
    .line 116
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v5}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 117
    .local v2, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.android.email.service.SharedPreferenceReceiver"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 119
    .local v1, "intent":Landroid/content/Intent;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v5, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_CSC:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_2

    .line 120
    const/4 v4, 0x0

    .line 121
    .local v4, "valueStr":Ljava/lang/String;
    sget-object v5, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_CSC:[Ljava/lang/String;

    aget-object v3, v5, v0

    .line 122
    .local v3, "source_value":Ljava/lang/String;
    const-string v5, "Settings.Sound.MessageTone"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    if-eqz v3, :cond_1

    .line 123
    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscEmailEas;->getMsgTone(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v4

    .line 127
    :goto_1
    if-eqz v4, :cond_0

    .line 128
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscEmailEas;->csc_context:Landroid/content/Context;

    const-string v6, "TYPE"

    const-string v7, "STRING"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "KEY"

    sget-object v8, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_PRFS:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "VALUE"

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 119
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 125
    :cond_1
    sget-object v5, Lcom/samsung/sec/android/application/csc/CscEmailEas;->KEY_CSC:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v2, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 132
    .end local v3    # "source_value":Ljava/lang/String;
    .end local v4    # "valueStr":Ljava/lang/String;
    :cond_2
    return-void
.end method
