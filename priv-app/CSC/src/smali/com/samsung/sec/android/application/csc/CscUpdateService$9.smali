.class Lcom/samsung/sec/android/application/csc/CscUpdateService$9;
.super Ljava/lang/Object;
.source "CscUpdateService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/sec/android/application/csc/CscUpdateService;->doUpdate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;


# direct methods
.method constructor <init>(Lcom/samsung/sec/android/application/csc/CscUpdateService;)V
    .locals 0

    .prologue
    .line 622
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$9;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 624
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$9;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->verifyResetDone()V

    .line 626
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$9;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    iget-object v2, v2, Lcom/samsung/sec/android/application/csc/CscUpdateService;->mUpdatables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sec/android/application/csc/Updatable;

    .line 627
    .local v1, "u":Lcom/samsung/sec/android/application/csc/Updatable;
    const-string v2, "CscUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mUpdatables start. ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$9;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscUpdateService;->arrayIndex:I
    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->access$000(Lcom/samsung/sec/android/application/csc/CscUpdateService;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    invoke-interface {v1}, Lcom/samsung/sec/android/application/csc/Updatable;->update()V

    .line 629
    const-string v2, "CscUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mUpdatables done. ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$9;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscUpdateService;->arrayIndex:I
    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->access$000(Lcom/samsung/sec/android/application/csc/CscUpdateService;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$9;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    # operator++ for: Lcom/samsung/sec/android/application/csc/CscUpdateService;->arrayIndex:I
    invoke-static {v2}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->access$008(Lcom/samsung/sec/android/application/csc/CscUpdateService;)I

    goto :goto_0

    .line 633
    .end local v1    # "u":Lcom/samsung/sec/android/application/csc/Updatable;
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$9;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscUpdateService;->checkAndSendModemSettingIntent()V

    .line 634
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscUtil;->setCscUpdated()V

    .line 635
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscUpdateService$9;->this$0:Lcom/samsung/sec/android/application/csc/CscUpdateService;

    const-string v3, "csc.preferences_name"

    const-string v4, "csc.key.already_executed"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/sec/android/application/csc/CscUtil;->setCscPreferenceValue(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 637
    return-void
.end method
