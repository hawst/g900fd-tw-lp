.class public Lcom/samsung/sec/android/application/csc/CscCalendar;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscCalendar.java"


# static fields
.field private static final PREFERENCE_CONTENT_URI:Landroid/net/Uri;


# instance fields
.field private final CSC_FILE:Ljava/lang/String;

.field private final Days:[Ljava/lang/String;

.field private final KEY_FIRST_DAY:Ljava/lang/String;

.field private final KEY_WEEKEND:Ljava/lang/String;

.field private final PATH_FIRST_DAY:Ljava/lang/String;

.field private final PATH_WEEKEND:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "content://com.sec.android.calendar.preference/Preference"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscCalendar;->PREFERENCE_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 26
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->CSC_FILE:Ljava/lang/String;

    .line 32
    const-string v0, "Settings.Main.Phone.WeekendSetting"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->PATH_WEEKEND:Ljava/lang/String;

    .line 37
    const-string v0, "Settings.Calendar.StartDay"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->PATH_FIRST_DAY:Ljava/lang/String;

    .line 39
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Locale default"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Sunday"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Monday"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Tuesday"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Wednesday"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Thursday"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Friday"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Saturday"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->Days:[Ljava/lang/String;

    .line 45
    const-string v0, "preferences_default_weekend"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->KEY_WEEKEND:Ljava/lang/String;

    .line 47
    const-string v0, "preferences_week_start_day"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->KEY_FIRST_DAY:Ljava/lang/String;

    .line 50
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->mContext:Landroid/content/Context;

    .line 51
    return-void
.end method

.method private getProperDayValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "values"    # Ljava/lang/String;

    .prologue
    .line 246
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->Days:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 247
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->Days:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 248
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 252
    :goto_1
    return-object v1

    .line 246
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 252
    :cond_1
    const-string v1, "2"

    goto :goto_1
.end method


# virtual methods
.method public compare()Ljava/lang/String;
    .locals 15

    .prologue
    .line 110
    const-string v12, "NOERROR"

    .line 112
    .local v12, "returnValue":Ljava/lang/String;
    new-instance v7, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->CSC_FILE:Ljava/lang/String;

    invoke-direct {v7, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 113
    .local v7, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    if-nez v7, :cond_0

    .line 114
    const-string v1, "CscCalendar"

    const-string v2, "CscCalendar::compare memory alloc fail"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v1, "NOCSC_Calendar"

    .line 199
    :goto_0
    return-object v1

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 118
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v8, 0x0

    .line 120
    .local v8, "cursor":Landroid/database/Cursor;
    const-string v1, "Settings.Main.Phone.WeekendSetting"

    invoke-virtual {v7, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 122
    .local v13, "values":Ljava/lang/String;
    const-string v14, "SATSUN"

    .line 124
    .local v14, "weekend":Ljava/lang/String;
    :try_start_0
    sget-object v1, Lcom/samsung/sec/android/application/csc/CscCalendar;->PREFERENCE_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "preferences_default_weekend"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 125
    if-eqz v8, :cond_1

    .line 126
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 127
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 128
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    .line 132
    :cond_1
    if-eqz v8, :cond_2

    .line 133
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 137
    :cond_2
    if-eqz v13, :cond_3

    .line 138
    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 139
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Settings.Main.Phone.WeekendSetting_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 140
    const-string v1, "Settings.Main.Phone.WeekendSetting"

    invoke-static {v1, v13, v14}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_3
    :goto_1
    const-string v1, "Settings.Calendar.StartDay"

    invoke-virtual {v7, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 147
    const-string v9, "2"

    .line 149
    .local v9, "firstDay":Ljava/lang/String;
    :try_start_1
    sget-object v1, Lcom/samsung/sec/android/application/csc/CscCalendar;->PREFERENCE_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "preferences_week_start_day"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 150
    if-eqz v8, :cond_4

    .line 151
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_4

    .line 152
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 153
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v9

    .line 157
    :cond_4
    if-eqz v8, :cond_5

    .line 158
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 162
    :cond_5
    if-eqz v13, :cond_8

    .line 163
    invoke-direct {p0, v13}, Lcom/samsung/sec/android/application/csc/CscCalendar;->getProperDayValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 164
    .local v11, "realValue":Ljava/lang/String;
    invoke-virtual {v11, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 166
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 167
    .local v10, "index":I
    if-ltz v10, :cond_6

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->Days:[Ljava/lang/String;

    array-length v1, v1

    if-le v10, v1, :cond_7

    .line 168
    :cond_6
    const/4 v10, 0x0

    .line 170
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Settings.Calendar.StartDay_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 171
    const-string v1, "Settings.Calendar.StartDay"

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->Days:[Ljava/lang/String;

    aget-object v2, v2, v10

    invoke-static {v1, v13, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    .end local v10    # "index":I
    .end local v11    # "realValue":Ljava/lang/String;
    :cond_8
    :goto_2
    const-string v3, "calendar_id!=2 AND calendar_id!=3 AND deleted!=1"

    .line 180
    .local v3, "where":Ljava/lang/String;
    const/4 v6, 0x0

    .line 182
    .local v6, "count":I
    :try_start_2
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 183
    if-eqz v8, :cond_9

    .line 184
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v6

    .line 187
    :cond_9
    if-eqz v8, :cond_a

    .line 188
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 191
    :cond_a
    if-lez v6, :cond_b

    .line 192
    const-string v12, "CALENDAR"

    .line 195
    :cond_b
    const-string v1, "NOERROR"

    invoke-virtual {v12, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 196
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CscCalendar : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    :cond_c
    move-object v1, v12

    .line 199
    goto/16 :goto_0

    .line 132
    .end local v3    # "where":Ljava/lang/String;
    .end local v6    # "count":I
    .end local v9    # "firstDay":Ljava/lang/String;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_d

    .line 133
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v1

    .line 142
    :cond_e
    const-string v1, "Settings.Main.Phone.WeekendSetting"

    invoke-static {v1, v13}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 157
    .restart local v9    # "firstDay":Ljava/lang/String;
    :catchall_1
    move-exception v1

    if-eqz v8, :cond_f

    .line 158
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_f
    throw v1

    .line 173
    .restart local v11    # "realValue":Ljava/lang/String;
    :cond_10
    const-string v1, "Settings.Calendar.StartDay"

    invoke-static {v1, v13}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 187
    .end local v11    # "realValue":Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v6    # "count":I
    :catchall_2
    move-exception v1

    if-eqz v8, :cond_11

    .line 188
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_11
    throw v1
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 12
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 57
    new-instance v6, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->CSC_FILE:Ljava/lang/String;

    invoke-direct {v6, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 59
    .local v6, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    if-nez v6, :cond_1

    .line 60
    const-string v1, "CscCalendar"

    const-string v2, "CscCalendar::encode memory alloc fail"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    const-string v1, "Settings.Main.Phone.WeekendSetting"

    invoke-virtual {v6, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 64
    .local v10, "values":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 65
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 66
    .local v7, "cursor":Landroid/database/Cursor;
    const-string v11, "SATSUN"

    .line 68
    .local v11, "weekend":Ljava/lang/String;
    :try_start_0
    sget-object v1, Lcom/samsung/sec/android/application/csc/CscCalendar;->PREFERENCE_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "preferences_default_weekend"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 69
    if-eqz v7, :cond_2

    .line 70
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 71
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 72
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 76
    :cond_2
    if-eqz v7, :cond_3

    .line 77
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 81
    :cond_3
    if-eqz v10, :cond_4

    .line 82
    const-string v1, "Settings.Main.Phone.WeekendSetting"

    invoke-virtual {p1, v1, v11}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_4
    const-string v1, "Settings.Calendar.StartDay"

    invoke-virtual {v6, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 86
    const-string v8, "2"

    .line 88
    .local v8, "firstDay":Ljava/lang/String;
    :try_start_1
    sget-object v1, Lcom/samsung/sec/android/application/csc/CscCalendar;->PREFERENCE_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "preferences_week_start_day"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 89
    if-eqz v7, :cond_5

    .line 90
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_5

    .line 91
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 92
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v8

    .line 96
    :cond_5
    if-eqz v7, :cond_6

    .line 97
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 101
    :cond_6
    if-eqz v10, :cond_0

    .line 102
    move-object v9, v8

    .line 103
    .local v9, "index":Ljava/lang/String;
    const-string v1, "Settings.Calendar.StartDay"

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->Days:[Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {p1, v1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 76
    .end local v8    # "firstDay":Ljava/lang/String;
    .end local v9    # "index":Ljava/lang/String;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_7

    .line 77
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v1

    .line 96
    .restart local v8    # "firstDay":Ljava/lang/String;
    :catchall_1
    move-exception v1

    if-eqz v7, :cond_8

    .line 97
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v1
.end method

.method public update()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 206
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v5}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 207
    .local v1, "cscParser":Lcom/samsung/sec/android/application/csc/CscParser;
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.android.calendar.preference.CscReceiver"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 209
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "CscCalendar"

    const-string v6, "CscCalendar Update()"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    const-string v5, "CscCaledar Update()"

    invoke-static {v9, v5}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 212
    if-nez v1, :cond_1

    .line 213
    const-string v5, "CscCalendar"

    const-string v6, "CscCalendar::update memory alloc fail"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 218
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 219
    .local v2, "cv":Landroid/content/ContentValues;
    const-string v5, "Settings.Main.Phone.WeekendSetting"

    invoke-virtual {v1, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 221
    .local v4, "values":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 222
    const-string v5, "preferences_default_weekend"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    const-string v5, "preferences_default_weekend"

    invoke-virtual {v2, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_2
    const-string v5, "Settings.Calendar.StartDay"

    invoke-virtual {v1, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 228
    if-eqz v4, :cond_3

    .line 229
    const-string v5, "CscCalendar"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "KEY_FIRST_DAY : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-direct {p0, v4}, Lcom/samsung/sec/android/application/csc/CscCalendar;->getProperDayValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const-string v5, "preferences_week_start_day"

    invoke-direct {p0, v4}, Lcom/samsung/sec/android/application/csc/CscCalendar;->getProperDayValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    const-string v5, "preferences_week_start_day"

    invoke-direct {p0, v4}, Lcom/samsung/sec/android/application/csc/CscCalendar;->getProperDayValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_3
    const-string v5, "CscCalendar"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscCalendar intent.getExtras() ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CscCaledar intent.getExtras()"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v5}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 236
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 237
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscCalendar;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v3, v8}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 239
    :cond_4
    invoke-virtual {v2}, Landroid/content/ContentValues;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 240
    sget-object v5, Lcom/samsung/sec/android/application/csc/CscCalendar;->PREFERENCE_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v5, v2, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0
.end method
