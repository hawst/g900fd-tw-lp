.class public Lcom/samsung/sec/android/application/csc/CscSmsMms;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscSmsMms.java"


# static fields
.field static final KEY_CSC:[Ljava/lang/String;

.field static final KEY_PRFS:[Ljava/lang/String;

.field static final KEY_PRFS_TYPE:[Ljava/lang/String;

.field static final USER_KEY_PRFS:[Ljava/lang/String;


# instance fields
.field private final CSC_FILE:Ljava/lang/String;

.field private final PATH_MESSAGETONE:Ljava/lang/String;

.field private final TAG_MESSAGETONE:Ljava/lang/String;

.field private ct:Landroid/content/Context;

.field private sPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    const/16 v0, 0x25

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Settings.Messages.PushMsg.RecOption"

    aput-object v1, v0, v3

    const-string v1, "Settings.Messages.PushMsg.PushLoading"

    aput-object v1, v0, v4

    const-string v1, "Settings.Messages.SMS.DeliveryReport"

    aput-object v1, v0, v5

    const-string v1, "Settings.Messages.SMS.MsgTypeThreshold.ThresholdValue"

    aput-object v1, v0, v6

    const-string v1, "Settings.Messages.SMS.MsgTypeThreshold.ThresholdType"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Settings.Messages.SMS.MaxRecipient"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Settings.Messages.SMS.CharSupport"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Settings.Messages.SMS.EmailGateway"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Settings.Messages.SMS.CellBroadcast"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Settings.Messages.SMS.CellBroadcastChannel"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Settings.Messages.SMS.TextTemplate"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Settings.Messages.SMS.DeleteOldMessage"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Settings.Messages.SMS.MsgSplitView"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Settings.Messages.SMS.FontSizeByVolumeKey"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Settings.Messages.SMS.DeleteOldMessageCntSms"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "Settings.Messages.SMS.DeleteOldMessageCntMms"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Settings.Messages.SMS.PreviewMessage"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Settings.Messages.MMS.MmsSending.ImageResizeResolution"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Settings.Messages.MMS.MmsSending.ReqDeliveryRep"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "Settings.Messages.MMS.MmsSending.ReqReadRep"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "Settings.Messages.MMS.MmsSending.Expiry"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "Settings.Messages.MMS.MmsSending.MessageSize"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "Settings.Messages.MMS.MmsSending.CreationMode"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "Settings.Messages.MMS.MmsSending.MaxRecipientMMS"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "Settings.Messages.MMS.MmsReceiving.Home"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "Settings.Messages.MMS.MmsReceiving.Roaming"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "Settings.Messages.MMS.SlideMaxCount"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "Settings.Main.Sound.MsgToneRepeatInterval"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "garbage"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "Settings.Main.Sound.MessageTone"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "Settings.Main.Sound.MsgToneAlertType"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "Settings.Messages.SMS.MmsAlert"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "Settings.Messages.MMS.GroupMessaging"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "Settings.Messages.SMS.PreviewMessageNoti"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "Settings.Messages.SMS.PromptToAccessLink"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "Settings.Messages.SMS.PrioritySender"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "Settings.Messages.SafeMode.PotentialThreatAlerts"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    .line 84
    const/16 v0, 0x25

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "csc_pref_key_enable_push_message"

    aput-object v1, v0, v3

    const-string v1, "csc_pref_key_service_loading_action"

    aput-object v1, v0, v4

    const-string v1, "csc_pref_key_sms_delivery_reports"

    aput-object v1, v0, v5

    const-string v1, "csc_pref_key_threshold"

    aput-object v1, v0, v6

    const-string v1, "csc_pref_key_threshold_Type"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "csc_pref_key_max_recipient"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "csc_pref_key_sms_input_mode"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "csc_pref_key_sms_email_gateway"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "csc_pref_key_cb_settings_activation"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "csc_pref_key_cb_settings_channel"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "csc_pref_key_sms_text_template"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "csc_pref_key_auto_delete"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "csc_pref_key_split_view"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "csc_pref_key_font_size_by_volume_key"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "csc_pref_max_sms_messages_per_threadd"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "csc_pref_max_mms_messages_per_thread"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "csc_pref_key_enable_preview_message"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "csc_pref_key_mms_image_resize_resolution"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "csc_pref_key_mms_delivery_reports"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "csc_pref_key_mms_read_reports"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "csc_pref_key_mms_expiry"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "csc_pref_key_mms_max_size"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "csc_pref_key_mms_creation_mode"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "csc_pref_key_mms_max_recipient"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "csc_pref_key_mms_auto_retrieval"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "csc_pref_key_mms_retrieval_during_roaming"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "csc_pref_key_mms_slide_max_count"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "csc_pref_key_msgtone_repeat_interval"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "csc_pref_key_enable_notifications"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "csc_pref_key_ringtone"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "garbage"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "garbage"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "garbage"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "garbage"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "garbage"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "csc_pref_key_enable_priority_senders"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "csc_pref_manage_access_authority"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    .line 123
    const/16 v0, 0x25

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "type_key_enable_push_message"

    aput-object v1, v0, v3

    const-string v1, "type_key_service_loading_action"

    aput-object v1, v0, v4

    const-string v1, "type_key_sms_delivery_reports"

    aput-object v1, v0, v5

    const-string v1, "type_key_threshold"

    aput-object v1, v0, v6

    const-string v1, "type_key_threshold_Type"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "type_key_max_recipient"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "type_key_sms_input_mode"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "type_key_sms_email_gateway"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "type_key_cb_settings_activation"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "type_key_cb_settings_channel"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "type_key_sms_text_template"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "type_key_auto_delete"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "type_key_split_view"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "type_key_font_size_by_volume_key"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "type_key_max_sms_messages_per_thread"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "type_key_max_mms_messages_per_thread"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "type_key_enable_preview_message"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "type_key_mms_image_resize_resolution"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "type_key_mms_delivery_reports"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "type_key_mms_read_reports"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "type_key_mms_expiry"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "type_key_mms_max_size"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "type_key_mms_creation_mode"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "type_key_mms_max_recipient"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "type_key_mms_auto_retrieval"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "type_key_mms_retrieval_during_roaming"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "type_key_mms_slide_max_count"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "type_key_msgtone_repeat_interval"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "type_key_enable_notifications"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "type_key_ringtone"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "garbage"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "garbage"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "garbage"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "garbage"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "garbage"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "type_key_enable_priority_senders"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "type_key_manage_access_authority"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    .line 161
    const/16 v0, 0x25

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "pref_key_enable_push_message"

    aput-object v1, v0, v3

    const-string v1, "pref_key_service_loading_action"

    aput-object v1, v0, v4

    const-string v1, "pref_key_sms_delivery_reports"

    aput-object v1, v0, v5

    const-string v1, "pref_key_threshold"

    aput-object v1, v0, v6

    const-string v1, "pref_key_threshold_type"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "pref_key_max_recipient"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "pref_key_sms_input_mode"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "pref_key_sms_email_gateway"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "pref_key_cb_settings_activation"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "pref_key_cb_settings_channel"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "pref_key_sms_text_template"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "pref_key_auto_delete"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "pref_key_split_view"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "pref_key_font_size_by_volume_key_enable"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "MaxSmsMessagesPerThread"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "MaxMmsMessagesPerThread"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "pref_key_enable_preview_message"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "pref_key_mms_image_resize_resolution"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "pref_key_mms_delivery_reports"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "pref_key_mms_read_reports"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "pref_key_mms_expiry"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "pref_key_mms_max_size"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "pref_key_mms_creation_mode"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "pref_key_mms_max_recipient"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "pref_key_mms_auto_retrieval"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "pref_key_mms_retrieval_during_roaming"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "pref_key_mms_slide_max_count"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "pref_key_msgtone_repeat_interval"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "pref_key_enable_notifications"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "pref_key_ringtone"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "pref_key_vibrateWhen_checkbox"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "pref_key_mms_change_over_alarm"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "pref_key_mms_group_mms"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "pref_key_enable_statusbar_preview_message"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "pref_key_urllink_option_enable"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "pref_key_priority_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "pref_manage_access_authority"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 27
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->CSC_FILE:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->sPrefs:Landroid/content/SharedPreferences;

    .line 32
    const-string v0, "Settings.Sound"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->PATH_MESSAGETONE:Ljava/lang/String;

    .line 34
    const-string v0, "MessageTone"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->TAG_MESSAGETONE:Ljava/lang/String;

    .line 205
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->ct:Landroid/content/Context;

    .line 206
    return-void
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1084
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Messages.MMS.MMSVersion"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1085
    const-string v0, "Settings.Messages.SMS.MsgTypeThreshold.ThresholdType"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1086
    const-string v0, "Settings.Sound.MessageTone.FileType"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1087
    const-string v0, "Settings.Messages.Sound.MessageTone.FileType"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1088
    const-string v0, "Main.Sound.MsgToneAlertType"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1089
    const-string v0, "Settings.Messages.MMS.MmsReceiving.SendReadRep"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1090
    const-string v0, "Settings.Messages.MMS.MmsReceiving.SendDeliveryRep"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1091
    const-string v0, "Settings.Messages.MMS.MmsSending.KeepCopy"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1092
    const-string v0, "Settings.Messages.SMS.KeepCopy"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1093
    const-string v0, "Settings.Messages.SMS.PageLimit"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1094
    const-string v0, "Settings.Messages.SMS.Bearer"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1095
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 24

    .prologue
    .line 483
    const/16 v20, 0x0

    .line 484
    .local v20, "valueStr":Ljava/lang/String;
    const/4 v9, 0x0

    .line 485
    .local v9, "context":Landroid/content/Context;
    const/4 v14, 0x0

    .line 486
    .local v14, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const/16 v21, 0x0

    .line 492
    .local v21, "valueStrArray":[Ljava/lang/String;
    :try_start_0
    new-instance v15, Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->CSC_FILE:Ljava/lang/String;

    invoke-direct {v15, v4}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 501
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .local v15, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :try_start_1
    const-string v4, "content://com.android.mms.csc.PreferenceProvider/listall"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 503
    .local v3, "MSG_PREFERENCE":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->ct:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 504
    .local v2, "resolver":Landroid/content/ContentResolver;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 506
    .local v10, "cursor":Landroid/database/Cursor;
    const/16 v22, 0x0

    .line 507
    .local v22, "xml_boolean":Z
    const/16 v23, -0x1

    .line 508
    .local v23, "xml_int":I
    const/16 v16, 0x0

    .line 509
    .local v16, "user_key_boolean":Z
    const/16 v17, -0x1

    .line 510
    .local v17, "user_key_int":I
    const/16 v19, 0x0

    .line 512
    .local v19, "user_key_str":Ljava/lang/String;
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_32

    .line 513
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 515
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-ge v12, v4, :cond_32

    .line 516
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v4, v4, v12

    invoke-virtual {v15, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 517
    const/4 v4, 0x0

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 519
    .local v18, "user_key_name":Ljava/lang/String;
    if-eqz v20, :cond_6

    if-eqz v18, :cond_6

    .line 520
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 521
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 523
    if-eqz v20, :cond_6

    .line 524
    const-string v4, "always"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 525
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "Always"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 527
    :cond_0
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 531
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .line 1077
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    .end local v3    # "MSG_PREFERENCE":Landroid/net/Uri;
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v12    # "i":I
    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .end local v16    # "user_key_boolean":Z
    .end local v17    # "user_key_int":I
    .end local v18    # "user_key_name":Ljava/lang/String;
    .end local v19    # "user_key_str":Ljava/lang/String;
    .end local v22    # "xml_boolean":Z
    .end local v23    # "xml_int":I
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :goto_1
    return-object v4

    .line 533
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v2    # "resolver":Landroid/content/ContentResolver;
    .restart local v3    # "MSG_PREFERENCE":Landroid/net/Uri;
    .restart local v10    # "cursor":Landroid/database/Cursor;
    .restart local v12    # "i":I
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v16    # "user_key_boolean":Z
    .restart local v17    # "user_key_int":I
    .restart local v18    # "user_key_name":Ljava/lang/String;
    .restart local v19    # "user_key_str":Ljava/lang/String;
    .restart local v22    # "xml_boolean":Z
    .restart local v23    # "xml_int":I
    :cond_1
    const-string v4, "prompt"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 534
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "Prompt"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 536
    :cond_2
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 540
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 542
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_3
    const-string v4, "never"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 543
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "Never"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 545
    :cond_4
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 549
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 552
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_5
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    :cond_6
    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    .line 515
    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 554
    :cond_7
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 555
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 557
    if-eqz v20, :cond_6

    .line 558
    const-string v4, "gsmalpha"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 559
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "GSM alphabet"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 561
    :cond_8
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 565
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 567
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_9
    const-string v4, "automatic"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 568
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "Automatic"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 570
    :cond_a
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 574
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 576
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_b
    const-string v4, "unicode"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 577
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    const-string v4, "Unicode"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 579
    :cond_c
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 583
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 587
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_d
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v4, v4, v5

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 1075
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    .end local v3    # "MSG_PREFERENCE":Landroid/net/Uri;
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v12    # "i":I
    .end local v16    # "user_key_boolean":Z
    .end local v17    # "user_key_int":I
    .end local v18    # "user_key_name":Ljava/lang/String;
    .end local v19    # "user_key_str":Ljava/lang/String;
    .end local v22    # "xml_boolean":Z
    .end local v23    # "xml_int":I
    :catch_0
    move-exception v11

    move-object v14, v15

    .line 1076
    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .local v11, "ex":Ljava/lang/Exception;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :goto_4
    const-string v4, "CscSmsMms : NOCSC"

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 1077
    const-string v4, "NOCSC_SmsMms"

    goto/16 :goto_1

    .line 589
    .end local v11    # "ex":Ljava/lang/Exception;
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v2    # "resolver":Landroid/content/ContentResolver;
    .restart local v3    # "MSG_PREFERENCE":Landroid/net/Uri;
    .restart local v10    # "cursor":Landroid/database/Cursor;
    .restart local v12    # "i":I
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v16    # "user_key_boolean":Z
    .restart local v17    # "user_key_int":I
    .restart local v18    # "user_key_name":Ljava/lang/String;
    .restart local v19    # "user_key_str":Ljava/lang/String;
    .restart local v22    # "xml_boolean":Z
    .restart local v23    # "xml_int":I
    :cond_e
    :try_start_2
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x7

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 590
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 592
    if-eqz v20, :cond_6

    .line 593
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_f

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 595
    :cond_f
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x7

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x7

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 599
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v6, 0x7

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 602
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_10
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v5, 0x7

    aget-object v4, v4, v5

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 604
    :cond_11
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0x9

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 605
    const-string v4, "Settings.Messages.SMS"

    const-string v5, "CellBroadcastChannel"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v4, v5}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getTagListsToString(Lcom/samsung/sec/android/application/csc/CscParser;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 607
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 609
    if-eqz v20, :cond_6

    .line 610
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_12

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 612
    :cond_12
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x9

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x9

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 616
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x9

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 619
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_13
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x9

    aget-object v4, v4, v5

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 621
    :cond_14
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0x11

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 622
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x11

    aget-object v4, v4, v5

    invoke-virtual {v15, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getStringMaxImageResolution(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 623
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 625
    if-eqz v20, :cond_6

    .line 626
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_15

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_16

    .line 628
    :cond_15
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x11

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x11

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 632
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x11

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 635
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_16
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x11

    aget-object v4, v4, v5

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 637
    :cond_17
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0x14

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 638
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x14

    aget-object v4, v4, v5

    invoke-virtual {v15, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getStringMmsExpire(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 639
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 641
    if-eqz v20, :cond_6

    .line 642
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_18

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_19

    .line 644
    :cond_18
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x14

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x14

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 648
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x14

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 650
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_19
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x14

    aget-object v4, v4, v5

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 652
    :cond_1a
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0x15

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 653
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x15

    aget-object v4, v4, v5

    invoke-virtual {v15, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getStringMessageSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 654
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 656
    if-eqz v20, :cond_6

    .line 657
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1b

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1c

    .line 659
    :cond_1b
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x15

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x15

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 663
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x15

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 665
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_1c
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x15

    aget-object v4, v4, v5

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 667
    :cond_1d
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0x16

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 668
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 670
    if-eqz v20, :cond_6

    .line 671
    const-string v4, "restricted"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 672
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1e

    const-string v4, "restricted"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_23

    .line 674
    :cond_1e
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x16

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x16

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 678
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x16

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 680
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_1f
    const-string v4, "warning"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 681
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_20

    const-string v4, "warning"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_23

    .line 683
    :cond_20
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x16

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x16

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 687
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x16

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 689
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_21
    const-string v4, "free"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 690
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_22

    const-string v4, "free"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_23

    .line 692
    :cond_22
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x16

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x16

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 696
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x16

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 700
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_23
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x16

    aget-object v4, v4, v5

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 702
    :cond_24
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0x1b

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 703
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x1b

    aget-object v4, v4, v5

    invoke-virtual {v15, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getStringMsgNotiAlertInterval(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 704
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 706
    if-eqz v20, :cond_6

    .line 707
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_25

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_26

    .line 709
    :cond_25
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x1b

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x1b

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 713
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0x1b

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 716
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_26
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0x1b

    aget-object v4, v4, v5

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 718
    :cond_27
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0xa

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 719
    const-string v4, "Settings.Messages.SMS"

    const-string v5, "TextTemplate"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v4, v5}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getTagListsToArray(Lcom/samsung/sec/android/application/csc/CscParser;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 720
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 721
    const-string v4, ";"

    const-string v5, ""

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 723
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 725
    .local v8, "CscTemplate":Ljava/lang/StringBuilder;
    const-string v4, "message template"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_key_str : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    const-string v4, "message template(1)"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "valueStrArray : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    if-eqz v21, :cond_28

    .line 729
    const/4 v13, 0x0

    .local v13, "k":I
    :goto_5
    move-object/from16 v0, v21

    array-length v4, v0

    if-ge v13, v4, :cond_28

    .line 730
    aget-object v4, v21, v13

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 729
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 734
    .end local v13    # "k":I
    :cond_28
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 735
    const-string v4, "message template(1)"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "valueStr : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    if-eqz v20, :cond_6

    .line 738
    const-string v4, "message template(1)"

    const-string v5, "valueStr != null"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    const-string v4, "default"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_29

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2a

    .line 742
    :cond_29
    const-string v4, "message template(1)"

    const-string v5, "not user_key_str.equals"

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0xa

    aget-object v4, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0xa

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 747
    const-string v4, "message template"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "return : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0xa

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v6, 0xa

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 751
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_2a
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v5, 0xa

    aget-object v4, v4, v5

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 754
    .end local v8    # "CscTemplate":Ljava/lang/StringBuilder;
    :cond_2b
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 755
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getIntFromString(Ljava/lang/String;)I

    move-result v23

    .line 756
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    add-int/lit8 v17, v4, -0x1

    .line 758
    move/from16 v0, v23

    move/from16 v1, v17

    if-eq v0, v1, :cond_31

    .line 759
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v4, v4, v12

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v5, v5, v12

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 763
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v5, v5, v12

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 765
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_2c
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x5

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2d

    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0x17

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2d

    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0x1a

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2e

    .line 768
    :cond_2d
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getIntFromString(Ljava/lang/String;)I

    move-result v23

    .line 769
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 771
    move/from16 v0, v23

    move/from16 v1, v17

    if-eq v0, v1, :cond_31

    .line 772
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v4, v4, v12

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v5, v5, v12

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 776
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v5, v5, v12

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 779
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_2e
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2f

    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0x1c

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2f

    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->USER_KEY_PRFS:[Ljava/lang/String;

    const/16 v5, 0x1d

    aget-object v4, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_30

    .line 782
    :cond_2f
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_3

    .line 785
    :cond_30
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v22

    .line 786
    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 787
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v16

    .line 789
    move/from16 v0, v22

    move/from16 v1, v16

    if-eq v0, v1, :cond_31

    .line 790
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v4, v4, v12

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscSmsMms : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v5, v5, v12

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 794
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v5, v5, v12

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 798
    .end local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    :cond_31
    sget-object v4, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v4, v4, v12

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 804
    .end local v12    # "i":I
    .end local v18    # "user_key_name":Ljava/lang/String;
    :cond_32
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 805
    const-string v4, "NOERROR"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v14, v15

    .end local v15    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    .restart local v14    # "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    goto/16 :goto_1

    .line 1075
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    .end local v3    # "MSG_PREFERENCE":Landroid/net/Uri;
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v16    # "user_key_boolean":Z
    .end local v17    # "user_key_int":I
    .end local v19    # "user_key_str":Ljava/lang/String;
    .end local v22    # "xml_boolean":Z
    .end local v23    # "xml_int":I
    :catch_1
    move-exception v11

    goto/16 :goto_4
.end method

.method public cutomerUpdate()V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 214
    const/4 v3, 0x0

    .line 215
    .local v3, "valueStr":Ljava/lang/String;
    const/4 v4, 0x0

    .line 216
    .local v4, "valueStrArray":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 218
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->CSC_FILE:Ljava/lang/String;

    invoke-direct {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 220
    .local v2, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    new-instance v1, Landroid/content/Intent;

    const-string v6, "com.android.mms.transaction.CscReceiver"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 222
    .local v1, "intent":Landroid/content/Intent;
    const/4 v5, 0x0

    .line 226
    .local v5, "xml":Z
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v8

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 228
    if-eqz v3, :cond_0

    .line 229
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 230
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    aget-object v6, v6, v8

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 234
    :cond_0
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v9

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getStringPushLoading(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 235
    if-eqz v3, :cond_1

    .line 236
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    aget-object v6, v6, v9

    const-string v7, "STRING"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    aget-object v7, v7, v9

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    :cond_1
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v10

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 242
    if-eqz v3, :cond_2

    .line 243
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 244
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    aget-object v6, v6, v10

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 249
    :cond_2
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v11

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 250
    if-eqz v3, :cond_3

    .line 251
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    aget-object v6, v6, v11

    const-string v7, "INT"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    aget-object v7, v7, v11

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 256
    :cond_3
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    aget-object v6, v6, v12

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 257
    if-eqz v3, :cond_4

    .line 258
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    aget-object v6, v6, v12

    const-string v7, "INT"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    aget-object v7, v7, v12

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 263
    :cond_4
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v7, 0x6

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getStringcharSupport(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 264
    if-eqz v3, :cond_5

    .line 265
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/4 v7, 0x6

    aget-object v6, v6, v7

    const-string v7, "STRING"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/4 v8, 0x6

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    :cond_5
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v7, 0x7

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 270
    if-eqz v3, :cond_6

    .line 271
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/4 v7, 0x7

    aget-object v6, v6, v7

    const-string v7, "STRING"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/4 v8, 0x7

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    :cond_6
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x8

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 276
    if-eqz v3, :cond_7

    .line 277
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 278
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x8

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x8

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 282
    :cond_7
    const-string v6, "Settings.Messages.SMS"

    const-string v7, "CellBroadcastChannel"

    invoke-virtual {p0, v2, v6, v7}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getTagListsToString(Lcom/samsung/sec/android/application/csc/CscParser;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 283
    if-eqz v3, :cond_8

    .line 284
    const-string v6, " "

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 285
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x9

    aget-object v6, v6, v7

    const-string v7, "STRING"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x9

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    :cond_8
    const-string v6, "Settings.Messages.SMS"

    const-string v7, "TextTemplate"

    invoke-virtual {p0, v2, v6, v7}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getTagListsToArray(Lcom/samsung/sec/android/application/csc/CscParser;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 290
    if-eqz v4, :cond_9

    .line 291
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0xa

    aget-object v6, v6, v7

    const-string v7, "STRINGARRAY"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0xa

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 295
    :cond_9
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0xb

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 296
    if-eqz v3, :cond_a

    .line 297
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 298
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0xb

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0xb

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 302
    :cond_a
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0xc

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 303
    if-eqz v3, :cond_b

    .line 304
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 305
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0xc

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0xc

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 309
    :cond_b
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0xd

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 310
    if-eqz v3, :cond_c

    .line 311
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 312
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0xd

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0xd

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 316
    :cond_c
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0xe

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 317
    if-eqz v3, :cond_d

    .line 318
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0xe

    aget-object v6, v6, v7

    const-string v7, "INT"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0xe

    aget-object v7, v7, v8

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 323
    :cond_d
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0xf

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 324
    if-eqz v3, :cond_e

    .line 325
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0xf

    aget-object v6, v6, v7

    const-string v7, "INT"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0xf

    aget-object v7, v7, v8

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 330
    :cond_e
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x10

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 331
    if-eqz v3, :cond_f

    .line 332
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 333
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x10

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x10

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 338
    :cond_f
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x11

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getStringMaxImageResolution(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 339
    if-eqz v3, :cond_10

    .line 340
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x11

    aget-object v6, v6, v7

    const-string v7, "STRING"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x11

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    :cond_10
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x12

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 345
    if-eqz v3, :cond_11

    .line 346
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 347
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x12

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x12

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 351
    :cond_11
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x13

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 352
    if-eqz v3, :cond_12

    .line 353
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 354
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x13

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x13

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 358
    :cond_12
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x14

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getStringMmsExpire(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 359
    if-eqz v3, :cond_13

    .line 360
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x14

    aget-object v6, v6, v7

    const-string v7, "STRING"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x14

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 364
    :cond_13
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x15

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getStringMessageSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 365
    if-eqz v3, :cond_14

    .line 366
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x15

    aget-object v6, v6, v7

    const-string v7, "STRING"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x15

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    :cond_14
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x16

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getStringCreationMode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 371
    if-eqz v3, :cond_15

    .line 372
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x16

    aget-object v6, v6, v7

    const-string v7, "STRING"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x16

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 376
    :cond_15
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x17

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 377
    if-eqz v3, :cond_16

    .line 378
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x17

    aget-object v6, v6, v7

    const-string v7, "INT"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x17

    aget-object v7, v7, v8

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 383
    :cond_16
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x18

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 384
    if-eqz v3, :cond_17

    .line 385
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 386
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x18

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x18

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 390
    :cond_17
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x19

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 391
    if-eqz v3, :cond_18

    .line 392
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 393
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x19

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x19

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 397
    :cond_18
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x1a

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 398
    if-eqz v3, :cond_19

    .line 399
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x1a

    aget-object v6, v6, v7

    const-string v7, "INT"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x1a

    aget-object v7, v7, v8

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 404
    :cond_19
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x1b

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getStringMsgNotiAlertInterval(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 405
    if-eqz v3, :cond_1a

    .line 406
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x1b

    aget-object v6, v6, v7

    const-string v7, "STRING"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x1b

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 410
    :cond_1a
    invoke-virtual {p0, v2}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getMsgTone(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v3

    .line 411
    if-eqz v3, :cond_1b

    .line 414
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x1c

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x1c

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 416
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x1d

    aget-object v6, v6, v7

    const-string v7, "STRING"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x1d

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 420
    :cond_1b
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x1e

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 421
    if-eqz v3, :cond_1c

    .line 422
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 423
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x1e

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x1e

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 427
    :cond_1c
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x1f

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 428
    if-eqz v3, :cond_1d

    .line 429
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 430
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x1f

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x1f

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 434
    :cond_1d
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x20

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 435
    if-eqz v3, :cond_1e

    .line 436
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 437
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x20

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x20

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 441
    :cond_1e
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x21

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 442
    if-eqz v3, :cond_1f

    .line 443
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 444
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x21

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x21

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 448
    :cond_1f
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x22

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 449
    if-eqz v3, :cond_20

    .line 450
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 451
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x22

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x22

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 455
    :cond_20
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x23

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 456
    if-eqz v3, :cond_21

    .line 457
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 458
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x23

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x23

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462
    :cond_21
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v7, 0x24

    aget-object v6, v6, v7

    invoke-virtual {v2, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 463
    if-eqz v3, :cond_22

    .line 464
    invoke-virtual {p0, v3}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v5

    .line 465
    sget-object v6, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS_TYPE:[Ljava/lang/String;

    const/16 v7, 0x24

    aget-object v6, v6, v7

    const-string v7, "BOOLEAN"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    sget-object v7, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_PRFS:[Ljava/lang/String;

    const/16 v8, 0x24

    aget-object v7, v7, v8

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 468
    :cond_22
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->ct:Landroid/content/Context;

    invoke-virtual {v6, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 469
    return-void
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 11
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 1099
    const/4 v10, 0x0

    .line 1100
    .local v10, "xmlValueStr":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1103
    .local v8, "userValueStr":Ljava/lang/String;
    new-instance v7, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->CSC_FILE:Ljava/lang/String;

    invoke-direct {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    .line 1112
    .local v7, "mParser":Lcom/samsung/sec/android/application/csc/CscParser;
    const-string v2, "content://com.android.mms.csc.PreferenceProvider/listall"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1113
    .local v1, "MSG_PREFERENCE":Landroid/net/Uri;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscSmsMms;->ct:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1114
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1116
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 1118
    .local v9, "user_boolean":Z
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_10

    .line 1119
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1123
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1124
    if-eqz v10, :cond_0

    .line 1125
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1126
    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v9

    .line 1127
    if-eqz v9, :cond_11

    .line 1128
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const-string v3, "on"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1135
    :cond_0
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1136
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1137
    if-eqz v10, :cond_1

    .line 1138
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1139
    const-string v2, "Always"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1140
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const-string v3, "always"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149
    :cond_1
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1150
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1151
    if-eqz v10, :cond_2

    .line 1152
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1153
    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v9

    .line 1154
    if-eqz v9, :cond_14

    .line 1155
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const-string v3, "on"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    :cond_2
    :goto_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1161
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1162
    if-eqz v10, :cond_3

    .line 1163
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {p1, v2, v10}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1171
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1172
    if-eqz v10, :cond_4

    .line 1173
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {p1, v2, v10}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1181
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1182
    if-eqz v10, :cond_5

    .line 1183
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {p1, v2, v10}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1192
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1193
    if-eqz v10, :cond_6

    .line 1194
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1195
    const-string v2, "GSM alphabet"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1196
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    const-string v3, "gsmalpha"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    :cond_6
    :goto_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1205
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1206
    if-eqz v10, :cond_7

    .line 1207
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-virtual {p1, v2, v10}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    :cond_7
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1216
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1217
    if-eqz v10, :cond_8

    .line 1218
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1219
    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v9

    .line 1220
    if-eqz v9, :cond_17

    .line 1221
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    const-string v3, "on"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    :cond_8
    :goto_4
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1228
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1231
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1232
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xa

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1233
    if-eqz v10, :cond_9

    .line 1234
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xa

    aget-object v2, v2, v3

    invoke-virtual {p1, v2, v10}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1242
    :cond_9
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1243
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xb

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1244
    if-eqz v10, :cond_a

    .line 1245
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1246
    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v9

    .line 1247
    if-eqz v9, :cond_18

    .line 1248
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xb

    aget-object v2, v2, v3

    const-string v3, "on"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1254
    :cond_a
    :goto_5
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1255
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xc

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1256
    if-eqz v10, :cond_b

    .line 1257
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1258
    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v9

    .line 1259
    if-eqz v9, :cond_19

    .line 1260
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xc

    aget-object v2, v2, v3

    const-string v3, "on"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1266
    :cond_b
    :goto_6
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1267
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xd

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1268
    if-eqz v10, :cond_c

    .line 1269
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xd

    aget-object v2, v2, v3

    invoke-virtual {p1, v2, v10}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1277
    :cond_c
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1278
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xe

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1279
    if-eqz v10, :cond_d

    .line 1280
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xe

    aget-object v2, v2, v3

    invoke-virtual {p1, v2, v10}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1288
    :cond_d
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1289
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xf

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1290
    if-eqz v10, :cond_e

    .line 1291
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1292
    const-string v2, "default"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 1293
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xf

    aget-object v2, v2, v3

    invoke-virtual {p1, v2, v8}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1295
    :cond_e
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1296
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0x10

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1299
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1300
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0x11

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1301
    if-eqz v10, :cond_f

    .line 1302
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1303
    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v9

    .line 1304
    if-eqz v9, :cond_1a

    .line 1305
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0x11

    aget-object v2, v2, v3

    const-string v3, "auto"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1311
    :cond_f
    :goto_7
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1312
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0x12

    aget-object v2, v2, v3

    invoke-virtual {v7, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1313
    if-eqz v10, :cond_10

    .line 1314
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1315
    invoke-virtual {p0, v8}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->getBooleanFromString(Ljava/lang/String;)Z

    move-result v9

    .line 1316
    if-eqz v9, :cond_1b

    .line 1317
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0x12

    aget-object v2, v2, v3

    const-string v3, "auto"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 1322
    :cond_10
    :goto_8
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1323
    return-void

    .line 1130
    :cond_11
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const-string v3, "off"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1141
    :cond_12
    const-string v2, "Prompt"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1142
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const-string v3, "prompt"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1143
    :cond_13
    const-string v2, "Never"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1144
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const-string v3, "never"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1157
    :cond_14
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const-string v3, "off"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1197
    :cond_15
    const-string v2, "Automatic"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1198
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    const-string v3, "automatic"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1199
    :cond_16
    const-string v2, "Unicode"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1200
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    const-string v3, "unicode"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1223
    :cond_17
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    const-string v3, "off"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1250
    :cond_18
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xb

    aget-object v2, v2, v3

    const-string v3, "off"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1262
    :cond_19
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0xc

    aget-object v2, v2, v3

    const-string v3, "off"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1307
    :cond_1a
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0x11

    aget-object v2, v2, v3

    const-string v3, "manual"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1319
    :cond_1b
    sget-object v2, Lcom/samsung/sec/android/application/csc/CscSmsMms;->KEY_CSC:[Ljava/lang/String;

    const/16 v3, 0x12

    aget-object v2, v2, v3

    const-string v3, "manual"

    invoke-virtual {p1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8
.end method

.method getBooleanFromString(Ljava/lang/String;)Z
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1491
    const-string v1, "on"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "true"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "auto"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "vib"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "vibmelody"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1493
    :cond_0
    const/4 v0, 0x1

    .line 1498
    :cond_1
    :goto_0
    return v0

    .line 1494
    :cond_2
    const-string v1, "off"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "false"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "manual"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "melody"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "mute"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0
.end method

.method getIntFromString(Ljava/lang/String;)I
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1502
    if-nez p1, :cond_0

    .line 1503
    const/4 v0, -0x1

    .line 1504
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method getMsgTone(Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 10
    .param p1, "mParser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 1411
    const-string v7, "Settings.Sound"

    invoke-virtual {p1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1412
    .local v0, "MsgToneNode":Lorg/w3c/dom/Node;
    const/4 v5, 0x0

    .line 1414
    .local v5, "mSrc":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 1434
    :cond_0
    :goto_0
    return-object v6

    .line 1417
    :cond_1
    const-string v7, "MessageTone"

    invoke-virtual {p1, v0, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 1419
    .local v2, "mCustomerList":Lorg/w3c/dom/NodeList;
    if-eqz v2, :cond_2

    .line 1420
    invoke-interface {v2, v9}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Element;

    .line 1421
    .local v1, "list":Lorg/w3c/dom/Element;
    const-string v7, "src"

    invoke-interface {v1, v7}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1424
    .end local v1    # "list":Lorg/w3c/dom/Element;
    :cond_2
    if-eqz v5, :cond_0

    .line 1427
    const-string v7, "/"

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1429
    .local v4, "mSplitedPath":[Ljava/lang/String;
    array-length v7, v4

    add-int/lit8 v7, v7, -0x1

    aget-object v7, v4, v7

    const-string v8, "[.]"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1431
    .local v3, "mFileName":[Ljava/lang/String;
    array-length v7, v3

    if-lez v7, :cond_0

    .line 1434
    aget-object v6, v3, v9

    goto :goto_0
.end method

.method getStringCreationMode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1470
    if-nez p1, :cond_1

    move-object p1, v0

    .line 1475
    .end local p1    # "text":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 1472
    .restart local p1    # "text":Ljava/lang/String;
    :cond_1
    const-string v1, "restricted"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "warning"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "free"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p1, v0

    .line 1475
    goto :goto_0
.end method

.method getStringMaxImageResolution(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1447
    if-nez p1, :cond_1

    move-object p1, v0

    .line 1456
    .end local p1    # "text":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 1450
    .restart local p1    # "text":Ljava/lang/String;
    :cond_1
    const-string v1, "qcif"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "qvga"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "vga"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "wvga"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "hd720"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "4vga"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "uxga"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "hd1080"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "qxga"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "wqxga"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "qsxga"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "quxga"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p1, v0

    .line 1456
    goto :goto_0
.end method

.method getStringMessageSize(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1479
    if-nez p1, :cond_1

    move-object p1, v0

    .line 1487
    .end local p1    # "text":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 1481
    .restart local p1    # "text":Ljava/lang/String;
    :cond_1
    const-string v1, "30"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "100"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "300"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "350"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "500"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "600"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1m"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1.2m"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1.5m"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "2m"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "3m"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p1, v0

    .line 1487
    goto :goto_0
.end method

.method getStringMmsExpire(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1460
    if-nez p1, :cond_1

    move-object p1, v0

    .line 1466
    .end local p1    # "text":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 1462
    .restart local p1    # "text":Ljava/lang/String;
    :cond_1
    const-string v1, "max"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1d"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "2d"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1w"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "2w"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p1, v0

    .line 1466
    goto :goto_0
.end method

.method getStringMsgNotiAlertInterval(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1438
    if-nez p1, :cond_1

    move-object p1, v0

    .line 1443
    .end local p1    # "text":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 1440
    .restart local p1    # "text":Ljava/lang/String;
    :cond_1
    const-string v1, "once"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "2m"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "5m"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "10m"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p1, v0

    .line 1443
    goto :goto_0
.end method

.method getStringPushLoading(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1327
    if-nez p1, :cond_1

    .line 1336
    :cond_0
    :goto_0
    return-object v0

    .line 1329
    :cond_1
    const-string v1, "always"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1330
    const-string v0, "Always"

    goto :goto_0

    .line 1331
    :cond_2
    const-string v1, "prompt"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1332
    const-string v0, "Prompt"

    goto :goto_0

    .line 1333
    :cond_3
    const-string v1, "never"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1334
    const-string v0, "Never"

    goto :goto_0
.end method

.method getStringcharSupport(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1341
    if-nez p1, :cond_1

    .line 1350
    :cond_0
    :goto_0
    return-object v0

    .line 1343
    :cond_1
    const-string v1, "gsmalpha"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1344
    const-string v0, "GSM alphabet"

    goto :goto_0

    .line 1345
    :cond_2
    const-string v1, "automatic"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1346
    const-string v0, "Automatic"

    goto :goto_0

    .line 1347
    :cond_3
    const-string v1, "unicode"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1348
    const-string v0, "Unicode"

    goto :goto_0
.end method

.method getTagListsToArray(Lcom/samsung/sec/android/application/csc/CscParser;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p1, "mParser"    # Lcom/samsung/sec/android/application/csc/CscParser;
    .param p2, "path_Node"    # Ljava/lang/String;
    .param p3, "tag_NodeList"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 1355
    invoke-virtual {p1, p2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1357
    .local v0, "BaseNode":Lorg/w3c/dom/Node;
    if-nez v0, :cond_1

    .line 1377
    :cond_0
    return-object v4

    .line 1360
    :cond_1
    invoke-virtual {p1, v0, p3}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 1362
    .local v1, "ListItems":Lorg/w3c/dom/NodeList;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    if-lez v7, :cond_0

    .line 1365
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    .line 1366
    .local v5, "nListItem":I
    if-lez v5, :cond_0

    .line 1368
    new-array v4, v5, [Ljava/lang/String;

    .line 1370
    .local v4, "items":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 1371
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 1372
    .local v3, "item":Lorg/w3c/dom/Node;
    invoke-virtual {p1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    .line 1374
    .local v6, "parameter":Ljava/lang/String;
    aput-object v6, v4, v2

    .line 1370
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method getTagListsToString(Lcom/samsung/sec/android/application/csc/CscParser;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "mParser"    # Lcom/samsung/sec/android/application/csc/CscParser;
    .param p2, "path_Node"    # Ljava/lang/String;
    .param p3, "tag_NodeList"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 1381
    invoke-virtual {p1, p2}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 1383
    .local v0, "BaseNode":Lorg/w3c/dom/Node;
    if-nez v0, :cond_1

    .line 1403
    :cond_0
    :goto_0
    return-object v6

    .line 1386
    :cond_1
    invoke-virtual {p1, v0, p3}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 1388
    .local v1, "ListItems":Lorg/w3c/dom/NodeList;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    if-lez v7, :cond_0

    .line 1391
    new-instance v4, Ljava/lang/StringBuffer;

    const-string v6, ""

    invoke-direct {v4, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1393
    .local v4, "items":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 1394
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 1395
    .local v3, "item":Lorg/w3c/dom/Node;
    invoke-virtual {p1, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    .line 1397
    .local v5, "parameter":Ljava/lang/String;
    if-lez v2, :cond_2

    .line 1398
    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1400
    :cond_2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1393
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1403
    .end local v3    # "item":Lorg/w3c/dom/Node;
    .end local v5    # "parameter":Ljava/lang/String;
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public update()V
    .locals 0

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscSmsMms;->cutomerUpdate()V

    .line 211
    return-void
.end method
