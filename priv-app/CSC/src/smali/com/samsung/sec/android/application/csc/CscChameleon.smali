.class public Lcom/samsung/sec/android/application/csc/CscChameleon;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscChameleon.java"


# static fields
.field private static final BROWSER_BOOKMARK_CONTENT_URI:Landroid/net/Uri;

.field private static final BROWSER_OPBOOKMARK_CONTENT_URI:Landroid/net/Uri;

.field private static final CHAMELEON_UPDATE_FILE:Ljava/lang/String;

.field private static final CSC_CHAMELEON_FILE:Ljava/lang/String;

.field private static final DATA_URI:Landroid/net/Uri;

.field private static final SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

.field private static final SEND_PACKAGE_BROWSER:Landroid/content/ComponentName;

.field private static final SEND_PACKAGE_SBROWSER:Landroid/content/ComponentName;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private outputContents:Ljava/lang/StringBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getChameleonPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->CSC_CHAMELEON_FILE:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/data/user/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/com.samsung.sec.android.application.csc/chameleon_update.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->CHAMELEON_UPDATE_FILE:Ljava/lang/String;

    .line 184
    const-string v0, "content://com.android.contacts/data/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->DATA_URI:Landroid/net/Uri;

    .line 196
    const-string v0, "content://com.android.browser/bookmarks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->BROWSER_BOOKMARK_CONTENT_URI:Landroid/net/Uri;

    .line 199
    const-string v0, "content://com.android.browser/opbookmarks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->BROWSER_OPBOOKMARK_CONTENT_URI:Landroid/net/Uri;

    .line 204
    const-string v0, "content://com.sec.android.app.sbrowser.operatorbookmarks/bookmarks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

    .line 207
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.browser"

    const-string v2, "com.android.browser.BrowserActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->SEND_PACKAGE_BROWSER:Landroid/content/ComponentName;

    .line 210
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.sbrowser"

    const-string v2, "com.sec.android.app.sbrowser.SBrowserMainActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->SEND_PACKAGE_SBROWSER:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 222
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 220
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->outputContents:Ljava/lang/StringBuffer;

    .line 223
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    .line 224
    return-void
.end method

.method private contactAlreadyExists(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "FirstName"    # Ljava/lang/String;
    .param p2, "LastName"    # Ljava/lang/String;
    .param p3, "PhoneNumber"    # Ljava/lang/String;

    .prologue
    .line 621
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 622
    .local v6, "mStringBuilder":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 623
    const-string v0, "null"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mimetype=\'vnd.android.cursor.item/name\' AND (data2=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "display_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 640
    :goto_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 642
    .local v3, "selectString":Ljava/lang/String;
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "data2"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "data3"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "mimetype"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "raw_contact_id"

    aput-object v1, v2, v0

    .line 646
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 649
    .local v7, "nameCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 650
    const-string v0, "null"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 651
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 652
    const-string v0, "CscChameleon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " has no phone number"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    const/4 v0, 0x1

    .line 680
    :goto_1
    return v0

    .line 635
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selectString":Ljava/lang/String;
    .end local v7    # "nameCursor":Landroid/database/Cursor;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mimetype=\'vnd.android.cursor.item/name\' AND data2=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "data3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 655
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "selectString":Ljava/lang/String;
    .restart local v7    # "nameCursor":Landroid/database/Cursor;
    :cond_1
    const-string v0, "raw_contact_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 657
    .local v9, "rawContactId":Ljava/lang/Long;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mimetype=\'vnd.android.cursor.item/phone_v2\' AND raw_contact_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "data1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 661
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string v1, "data1"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "raw_contact_id"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "mimetype"

    aput-object v1, v2, v0

    .line 665
    .restart local v2    # "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 668
    .local v8, "phoneCursor":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 669
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 670
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 671
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 673
    :cond_2
    if-eqz v8, :cond_3

    .line 674
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 677
    .end local v8    # "phoneCursor":Landroid/database/Cursor;
    .end local v9    # "rawContactId":Ljava/lang/Long;
    :cond_3
    if-eqz v7, :cond_4

    .line 678
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 680
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method private updateBrowser()V
    .locals 26

    .prologue
    .line 684
    const-string v22, "Start updateBrowser()"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 685
    const/4 v5, 0x0

    .line 686
    .local v5, "bUseStockBrowser":Z
    const/4 v4, 0x0

    .line 688
    .local v4, "bUseSBrowser":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v19

    .line 690
    .local v19, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscChameleon;->SEND_PACKAGE_BROWSER:Landroid/content/ComponentName;

    const/16 v23, 0x80

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 691
    const-string v22, "CSCChameleon"

    const-string v23, "find com.android.browser/com.android.browser.BrowserActivity"

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 692
    const/4 v5, 0x1

    .line 699
    :goto_0
    :try_start_1
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscChameleon;->SEND_PACKAGE_SBROWSER:Landroid/content/ComponentName;

    const/16 v23, 0x80

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 700
    const-string v22, "CSCChameleon"

    const-string v23, "find com.sec.android.app.sbrowser/com.sec.android.app.sbrowser.SBrowserMainActivity"

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 702
    const/4 v4, 0x1

    .line 710
    :goto_1
    if-nez v5, :cond_0

    if-nez v4, :cond_0

    .line 841
    :goto_2
    return-void

    .line 693
    :catch_0
    move-exception v9

    .line 694
    .local v9, "e":Ljava/lang/Exception;
    const-string v22, "CSCChameleon"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "cannot find com.android.browser/com.android.browser.BrowserActivity"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 703
    .end local v9    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v9

    .line 704
    .restart local v9    # "e":Ljava/lang/Exception;
    const-string v22, "CSCChameleon"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "cannot find com.sec.android.app.sbrowser/ccom.sec.android.app.sbrowser.SBrowserMainActivity"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 714
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    const-string v23, "Browser.HomePage"

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 715
    .local v11, "homepage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v22, v0

    const-string v23, "Browser.SearchEngine"

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 721
    .local v20, "searchEngine":Ljava/lang/String;
    if-eqz v11, :cond_3

    .line 723
    const/16 v22, 0x2c

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v12

    .line 725
    .local v12, "indexOfComma":I
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v12, v0, :cond_4

    .line 726
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v11, v0, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 727
    .local v7, "bookmarkUrls":Ljava/lang/String;
    add-int/lit8 v22, v12, 0x1

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 728
    .local v6, "bookmarkNames":Ljava/lang/String;
    const-string v22, ""

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 729
    const-string v6, "Home"

    .line 737
    :cond_1
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v16

    .line 740
    .local v16, "mResolver":Landroid/content/ContentResolver;
    if-eqz v7, :cond_5

    .line 741
    new-instance v15, Landroid/content/Intent;

    const-string v22, "android.intent.action.CHAMELEON_BROWSER_SET_HOMEPAGE"

    move-object/from16 v0, v22

    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 743
    .local v15, "it":Landroid/content/Intent;
    const-string v22, "homepage"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 744
    const/16 v22, 0x20

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 745
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 747
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const-string v23, "csc.preferences_name"

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v21

    .line 748
    .local v21, "sp":Landroid/content/SharedPreferences;
    invoke-interface/range {v21 .. v21}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    .line 749
    .local v10, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v22, "homepage"

    move-object/from16 v0, v22

    invoke-interface {v10, v0, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 750
    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 752
    const-string v22, "CSCChameleon"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, " ** Homepage set DONE.. homeURL - "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    .end local v10    # "ed":Landroid/content/SharedPreferences$Editor;
    .end local v15    # "it":Landroid/content/Intent;
    .end local v21    # "sp":Landroid/content/SharedPreferences;
    :goto_4
    if-eqz v5, :cond_2

    .line 759
    if-eqz v7, :cond_2

    .line 760
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 761
    .local v8, "bookmarkmap":Landroid/content/ContentValues;
    const-string v22, "title"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    const-string v22, "url"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    const-string v22, "folder"

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 764
    const-string v22, "parent"

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 766
    :try_start_2
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscChameleon;->BROWSER_BOOKMARK_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v14

    .line 773
    .local v14, "inserted_uri":Landroid/net/Uri;
    :goto_5
    if-eqz v14, :cond_2

    invoke-virtual {v14}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_2

    .line 774
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 775
    .local v18, "opbookmarkmap":Landroid/content/ContentValues;
    invoke-virtual {v14}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v13

    .line 776
    .local v13, "inserted_id":Ljava/lang/String;
    const-string v22, "bookmark_id"

    invoke-virtual {v14}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 779
    :try_start_3
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscChameleon;->BROWSER_OPBOOKMARK_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 780
    const-string v22, "CSCChameleon"

    const-string v23, " ** updateBookmarks : StockBrowser opbookmark inserted"

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_3

    .line 790
    .end local v8    # "bookmarkmap":Landroid/content/ContentValues;
    .end local v13    # "inserted_id":Ljava/lang/String;
    .end local v14    # "inserted_uri":Landroid/net/Uri;
    .end local v18    # "opbookmarkmap":Landroid/content/ContentValues;
    :cond_2
    :goto_6
    if-eqz v4, :cond_3

    .line 791
    if-eqz v7, :cond_3

    if-eqz v6, :cond_3

    .line 792
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 793
    .local v17, "opBookmark":Landroid/content/ContentValues;
    const-string v22, "title"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    const-string v22, "url"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    const-string v22, "editable"

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 797
    if-eqz v16, :cond_3

    .line 799
    :try_start_4
    sget-object v22, Lcom/samsung/sec/android/application/csc/CscChameleon;->SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE:Landroid/net/Uri;

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v14

    .line 801
    .restart local v14    # "inserted_uri":Landroid/net/Uri;
    const-string v22, "CSCChameleon"

    const-string v23, " ** updateBookmarks : SBrowser opbookmark inserted"

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_4

    .line 814
    .end local v6    # "bookmarkNames":Ljava/lang/String;
    .end local v7    # "bookmarkUrls":Ljava/lang/String;
    .end local v12    # "indexOfComma":I
    .end local v14    # "inserted_uri":Landroid/net/Uri;
    .end local v16    # "mResolver":Landroid/content/ContentResolver;
    .end local v17    # "opBookmark":Landroid/content/ContentValues;
    :cond_3
    :goto_7
    if-eqz v20, :cond_9

    .line 815
    new-instance v15, Landroid/content/Intent;

    const-string v22, "android.intent.action.CSC_BROWSER_SET_SEARCH_ENGINE"

    move-object/from16 v0, v22

    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 816
    .restart local v15    # "it":Landroid/content/Intent;
    const-string v22, "google"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 817
    const-string v20, "google"

    .line 828
    :goto_8
    const-string v22, "searchEngine"

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 829
    const/16 v22, 0x20

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 830
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 832
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const-string v23, "csc.preferences_name"

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v21

    .line 833
    .restart local v21    # "sp":Landroid/content/SharedPreferences;
    invoke-interface/range {v21 .. v21}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    .line 834
    .restart local v10    # "ed":Landroid/content/SharedPreferences$Editor;
    const-string v22, "searchEngine"

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-interface {v10, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 835
    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 837
    const-string v22, "CSCChameleon"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, " ** searchEngine set DONE.. searchEngine - "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 732
    .end local v10    # "ed":Landroid/content/SharedPreferences$Editor;
    .end local v15    # "it":Landroid/content/Intent;
    .end local v21    # "sp":Landroid/content/SharedPreferences;
    .restart local v12    # "indexOfComma":I
    :cond_4
    move-object v7, v11

    .line 733
    .restart local v7    # "bookmarkUrls":Ljava/lang/String;
    const-string v6, "Home"

    .restart local v6    # "bookmarkNames":Ljava/lang/String;
    goto/16 :goto_3

    .line 754
    .restart local v16    # "mResolver":Landroid/content/ContentResolver;
    :cond_5
    const-string v22, "CSCChameleon"

    const-string v23, " ** Homepage URL is NULL. Nothing\'s configured."

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 767
    .restart local v8    # "bookmarkmap":Landroid/content/ContentValues;
    :catch_2
    move-exception v9

    .line 768
    .local v9, "e":Ljava/lang/SecurityException;
    const-string v22, "CSCChameleon"

    const-string v23, " ** updateBookmarks :  BROWSER_BOOKMARK_CONTENT_URI SecurityException"

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    const/4 v14, 0x0

    .restart local v14    # "inserted_uri":Landroid/net/Uri;
    goto/16 :goto_5

    .line 782
    .end local v9    # "e":Ljava/lang/SecurityException;
    .restart local v13    # "inserted_id":Ljava/lang/String;
    .restart local v18    # "opbookmarkmap":Landroid/content/ContentValues;
    :catch_3
    move-exception v9

    .line 783
    .restart local v9    # "e":Ljava/lang/SecurityException;
    const-string v22, "CSCChameleon"

    const-string v23, " ** updateBookmarks :  BROWSER_OPBOOKMARK_CONTENT_URI SecurityException"

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 803
    .end local v8    # "bookmarkmap":Landroid/content/ContentValues;
    .end local v9    # "e":Ljava/lang/SecurityException;
    .end local v13    # "inserted_id":Ljava/lang/String;
    .end local v14    # "inserted_uri":Landroid/net/Uri;
    .end local v18    # "opbookmarkmap":Landroid/content/ContentValues;
    .restart local v17    # "opBookmark":Landroid/content/ContentValues;
    :catch_4
    move-exception v9

    .line 804
    .restart local v9    # "e":Ljava/lang/SecurityException;
    const-string v22, "CSCChameleon"

    const-string v23, " ** updateBookmarks :  SBROWSER_OPBOOKMARK_CONTENT_URI_TABLE SecurityException"

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 818
    .end local v6    # "bookmarkNames":Ljava/lang/String;
    .end local v7    # "bookmarkUrls":Ljava/lang/String;
    .end local v9    # "e":Ljava/lang/SecurityException;
    .end local v12    # "indexOfComma":I
    .end local v16    # "mResolver":Landroid/content/ContentResolver;
    .end local v17    # "opBookmark":Landroid/content/ContentValues;
    .restart local v15    # "it":Landroid/content/Intent;
    :cond_6
    const-string v22, "yahoo"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 819
    const-string v20, "yahoo"

    goto/16 :goto_8

    .line 820
    :cond_7
    const-string v22, "bing"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 821
    const-string v20, "bing"

    goto/16 :goto_8

    .line 823
    :cond_8
    const-string v22, "CSCChameleon"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, " ** Set searchEngine to google because chameleon.xml has wrong value : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    const-string v20, "google"

    goto/16 :goto_8

    .line 839
    .end local v15    # "it":Landroid/content/Intent;
    :cond_9
    const-string v22, "CSCChameleon"

    const-string v23, " ** searchEngine value is NULL. Nothing\'s configured."

    invoke-static/range {v22 .. v23}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private updateCall()V
    .locals 34

    .prologue
    .line 953
    const-string v32, "Start updateCall()"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 954
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.First"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 955
    .local v20, "callIntercept1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.Second"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 956
    .local v24, "callIntercept2":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.Third"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 957
    .local v25, "callIntercept3":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.Fourth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 958
    .local v26, "callIntercept4":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.Fifth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 959
    .local v27, "callIntercept5":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.Sixth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 960
    .local v28, "callIntercept6":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.Seventh"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 961
    .local v29, "callIntercept7":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.Eighth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 962
    .local v30, "callIntercept8":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.Ninth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 963
    .local v31, "callIntercept9":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.Tenth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 964
    .local v21, "callIntercept10":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.Eleventh"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 965
    .local v22, "callIntercept11":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "CallIntercept.Twelfth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 967
    .local v23, "callIntercept12":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.First"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 968
    .local v2, "adc1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Second"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 969
    .local v12, "adc2":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Third"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 970
    .local v13, "adc3":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Fourth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 971
    .local v14, "adc4":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Fifth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 972
    .local v15, "adc5":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Sixth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 973
    .local v16, "adc6":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Seventh"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 974
    .local v17, "adc7":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Eighth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 975
    .local v18, "adc8":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Ninth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 976
    .local v19, "adc9":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Tenth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 977
    .local v3, "adc10":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Eleventh"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 978
    .local v4, "adc11":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Twelfth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 979
    .local v5, "adc12":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Thirteenth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 980
    .local v6, "adc13":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Fourteenth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 981
    .local v7, "adc14":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Fifteenth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 982
    .local v8, "adc15":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Sixteenth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 983
    .local v9, "adc16":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Seventeenth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 984
    .local v10, "adc17":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v32, v0

    const-string v33, "Adc.Eighteenth"

    invoke-virtual/range {v32 .. v33}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 985
    .local v11, "adc18":Ljava/lang/String;
    return-void
.end method

.method private updateChameleon()V
    .locals 5

    .prologue
    .line 311
    const-string v2, "CscChameleon"

    const-string v3, "updateChameleon"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/samsung/sec/android/application/csc/CscChameleon;->CSC_CHAMELEON_FILE:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 314
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 317
    new-instance v2, Lcom/samsung/sec/android/application/csc/CscParser;

    sget-object v3, Lcom/samsung/sec/android/application/csc/CscChameleon;->CSC_CHAMELEON_FILE:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 319
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v2, :cond_0

    .line 320
    const-string v2, "CscChameleon"

    const-string v3, "CscChameleon::updateChameleon memory alloc fail"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    :goto_0
    return-void

    .line 324
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Operators.DefaultRinger"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 325
    .local v1, "parsedString":Ljava/lang/String;
    const-string v2, "CscChameleon"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Operators.DefaultRinger:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    if-eqz v1, :cond_1

    .line 327
    invoke-virtual {p0, v1}, Lcom/samsung/sec/android/application/csc/CscChameleon;->CopyRingtone(Ljava/lang/String;)V

    .line 331
    :cond_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Operators.DiagMslReq"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 332
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Operators.DiagMslReq:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 333
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Operators.TetheredData"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 334
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Operators.TetheredData:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 335
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Operators.LaunchId"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 336
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Operators.LaunchId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 337
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Operators.LaunchZone"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 338
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Operators.LaunchZone:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 339
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Operators.EnabledAppsVvm"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 340
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Operators.EnabledAppsVvm:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 341
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Operators.BrandAlpha"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 342
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Operators.BrandAlpha:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 343
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Operators.AndroidOperatorNetworkCode"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 344
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Operators.AndroidOperatorNetworkCode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 345
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Operators.SubscriberCarrierId"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 346
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Operators.SubscriberCarrierId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 349
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.First"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 350
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.First:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 351
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.Second"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 352
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.Second:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 353
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.Third"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 354
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.Third:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 355
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.Fourth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 356
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.Fourth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 357
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.Fifth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 358
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.Fifth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 359
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.Sixth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 360
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.Sixth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 361
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.Seventh"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 362
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.Seventh:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 363
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.Eighth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 364
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.Eighth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 365
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.Ninth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 366
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.Ninth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 367
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.Tenth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 368
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.Tenth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 369
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.Eleventh"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 370
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.Eleventh:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 371
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "CallIntercept.Twelfth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 372
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CallIntercept.Twelfth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 375
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.First"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 376
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.First:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 377
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Second"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 378
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Second:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 379
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Third"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 380
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Third:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 381
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Fourth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 382
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Fourth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 383
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Fifth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 384
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Fifth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 385
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Sixth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 386
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Sixth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 387
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Seventh"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 388
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Seventh:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 389
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Eighth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 390
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Eighth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 391
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Ninth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 392
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Ninth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 393
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Tenth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 394
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Tenth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 395
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Eleventh"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 396
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Eleventh:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 397
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Twelfth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 398
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Twelfth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 399
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Thirteenth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 400
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Thirteenth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 401
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Fourteenth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 402
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Fourteenth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 403
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Fifteenth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 404
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Fifteenth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 405
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Sixteenth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 406
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Sixteenth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 407
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Seventeenth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 408
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Seventeenth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 409
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Adc.Eighteenth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 410
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adc.Eighteenth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 413
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "RoamPref.MenuDisplay"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 414
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RoamPref.MenuDisplay:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 415
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "RoamPref.HomeOnly"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 416
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RoamPref.HomeOnly:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 419
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Browser.HomePage"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 420
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Browser.HomePage:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 421
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Browser.SearchEngine"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 422
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Browser.SearchEngine:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 425
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "WiFi.Ssid"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 426
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WiFi.Ssid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 427
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "WiFi.MaxUsers"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 428
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WiFi.MaxUsers:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 429
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "WiFi.GsmMaxUsers"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 430
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WiFi.GsmMaxUsers:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 431
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "WiFi.DomRoamMaxUsers"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 432
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WiFi.DomRoamMaxUsers:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 433
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "WiFi.IntRoamMaxUsers"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 434
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WiFi.IntRoamMaxUsers:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 437
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Mms.ServerUrl"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 438
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Mms.ServerUrl:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 439
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Mms.Proxy"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 440
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Mms.Proxy:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 443
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Contact.First"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 444
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Contact.First:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 445
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Contact.Second"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 446
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Contact.Second:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 447
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Contact.Third"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 448
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Contact.Third:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 449
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Contact.Fourth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 450
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Contact.Fourth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 451
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Contact.Fifth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 452
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Contact.Fifth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 453
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Contact.Sixth"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 454
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Contact.Sixth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 457
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Operators.CarrierHomePage"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 458
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Operators.CarrierHomePage:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 459
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Operators.BrandAlpha"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 460
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Operators.BrandAlpha:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 462
    if-nez v1, :cond_2

    .line 463
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SKIP: There is dummy "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/sec/android/application/csc/CscChameleon;->CSC_CHAMELEON_FILE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 468
    :cond_2
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateModemSetting()V

    .line 469
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateHomescreen()V

    .line 470
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateSettings()V

    .line 471
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateCall()V

    .line 472
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateContacts()V

    .line 473
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateBrowser()V

    .line 474
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateConnection()V

    .line 475
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateSmsMms()V

    .line 476
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateTelephony()V

    .line 477
    const-string v2, "Chameleon Update Complete"

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 479
    .end local v1    # "parsedString":Ljava/lang/String;
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR: There is no "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/sec/android/application/csc/CscChameleon;->CSC_CHAMELEON_FILE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 480
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateSystemSelect(ZLjava/lang/String;)V

    goto/16 :goto_0
.end method

.method private updateConnection()V
    .locals 10

    .prologue
    .line 844
    const-string v7, "Start updateConnection()"

    invoke-direct {p0, v7}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 845
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "Operators.TetheredData"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 846
    .local v6, "tetheredData":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "WiFi.Ssid"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 847
    .local v5, "ssid":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "WiFi.MaxUsers"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 848
    .local v3, "maxUsers":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "WiFi.GsmMaxUsers"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 849
    .local v1, "gsmMaxUsers":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "WiFi.DomRoamMaxUsers"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 850
    .local v0, "domRoamMaxUsers":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "WiFi.IntRoamMaxUsers"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 852
    .local v2, "intRoamMaxUsers":Ljava/lang/String;
    const-string v7, "CscChameleon"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[updateConnection] tetheredData : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    const-string v7, "CscChameleon"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[updateConnection] ssid : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    const-string v7, "CscChameleon"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[updateConnection] maxUsers : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    const-string v7, "CscChameleon"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[updateConnection] gsmMaxUsers : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    const-string v7, "CscChameleon"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[updateConnection] domRoamMaxUsers : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    const-string v7, "CscChameleon"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[updateConnection] intRoamMaxUsers : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    new-instance v4, Landroid/content/Intent;

    const-string v7, "com.samsung.sec.android.application.csc.chameleon_wifi"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 861
    .local v4, "sprintHotspotIntent":Landroid/content/Intent;
    const-string v7, "chameleon_wifi_tetheredData"

    invoke-virtual {v4, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 862
    const-string v7, "chameleon_wifi_ssid"

    invoke-virtual {v4, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 863
    const-string v7, "chameleon_wifi_maxUsers"

    invoke-virtual {v4, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 864
    const-string v7, "chameleon_wifi_gsmMaxUsers"

    invoke-virtual {v4, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 865
    const-string v7, "chameleon_wifi_domRoamMaxUsers"

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 866
    const-string v7, "chameleon_wifi_intRoamMaxUsers"

    invoke-virtual {v4, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 867
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v4}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 868
    return-void
.end method

.method private updateContacts()V
    .locals 27

    .prologue
    .line 485
    const-string v24, "Start updateContacts()"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 486
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 488
    .local v19, "pathOfContacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v24, "Contact.First"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 489
    const-string v24, "Contact.Second"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 490
    const-string v24, "Contact.Third"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 491
    const-string v24, "Contact.Fourth"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 492
    const-string v24, "Contact.Fifth"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493
    const-string v24, "Contact.Sixth"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 495
    const/4 v4, 0x0

    .line 496
    .local v4, "FIRST_NAME":I
    const/4 v5, 0x1

    .line 497
    .local v5, "LAST_NAME":I
    const/4 v6, 0x2

    .line 498
    .local v6, "PHONE_NUMBER":I
    const/4 v7, 0x3

    .line 499
    .local v7, "PHONE_TYPE":I
    const/4 v8, 0x4

    .line 504
    .local v8, "WEBSITE":I
    const/16 v16, 0x0

    .line 505
    .local v16, "numOfContactsInserted":I
    const/16 v17, 0x0

    .line 506
    .local v17, "numOfContactsSkipped":I
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v21

    .line 509
    .local v21, "size":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    .line 511
    .local v15, "mResolver":Landroid/content/ContentResolver;
    const-string v24, "content://com.android.contacts"

    invoke-static/range {v24 .. v24}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v15, v0}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v11

    .line 513
    .local v11, "client":Landroid/content/ContentProviderClient;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 515
    .local v9, "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move/from16 v0, v21

    if-ge v14, v0, :cond_1

    .line 516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    move-object/from16 v25, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 517
    .local v18, "path":Ljava/lang/String;
    if-eqz v18, :cond_0

    const-string v24, "null"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 518
    :cond_0
    sub-int v16, v14, v17

    .line 519
    const-string v24, "CscChameleon"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Stop parsing. Parsed contacts: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    .end local v18    # "path":Ljava/lang/String;
    :cond_1
    :try_start_0
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_2

    .line 609
    invoke-virtual {v11, v9}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 610
    const-string v24, "CscChameleon"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Updated contacts: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 617
    :cond_2
    :goto_1
    return-void

    .line 523
    .restart local v18    # "path":Ljava/lang/String;
    :cond_3
    new-instance v22, Ljava/util/StringTokenizer;

    const-string v24, ","

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    .local v22, "tokenizer":Ljava/util/StringTokenizer;
    const/4 v12, 0x0

    .line 525
    .local v12, "columnOfRecord":I
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 526
    .local v23, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_2
    invoke-virtual/range {v22 .. v22}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v24

    if-eqz v24, :cond_4

    .line 527
    invoke-virtual/range {v22 .. v22}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v12, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 528
    const-string v25, "CscChameleon"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "token: "

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v26, ", "

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 532
    :cond_4
    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const/16 v25, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    const/16 v26, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/sec/android/application/csc/CscChameleon;->contactAlreadyExists(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 534
    add-int/lit8 v17, v17, 0x1

    .line 535
    const-string v25, "CscChameleon"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v26, " already exists. Skipping the insertion"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    :cond_5
    :goto_3
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 540
    :cond_6
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v20

    .line 542
    .local v20, "rawContactIdIndex":I
    sget-object v24, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v24 .. v24}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    .line 544
    .local v10, "builder":Landroid/content/ContentProviderOperation$Builder;
    const-string v24, "account_name"

    const-string v25, "vnd.sec.contact.phone"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 545
    const-string v24, "account_type"

    const-string v25, "vnd.sec.contact.phone"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 547
    const-string v24, "ReadOnly"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v25

    const-string v26, "CscFeature_Contact_SetPropertyForPreloadedContact"

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 549
    const-string v24, "data_set"

    const-string v25, "preload"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 551
    :cond_7
    invoke-virtual {v10}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    sget-object v24, Lcom/samsung/sec/android/application/csc/CscChameleon;->DATA_URI:Landroid/net/Uri;

    invoke-static/range {v24 .. v24}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    .line 555
    const-string v24, "raw_contact_id"

    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 556
    const-string v24, "mimetype"

    const-string v25, "vnd.android.cursor.item/name"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 557
    const-string v24, "data2"

    const/16 v25, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 558
    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "null"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_8

    .line 559
    const-string v24, "data3"

    const/16 v25, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 563
    :cond_8
    invoke-virtual {v10}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 566
    const/16 v24, 0x2

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "null"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_9

    .line 567
    sget-object v24, Lcom/samsung/sec/android/application/csc/CscChameleon;->DATA_URI:Landroid/net/Uri;

    invoke-static/range {v24 .. v24}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    .line 568
    const-string v24, "raw_contact_id"

    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 569
    const-string v24, "mimetype"

    const-string v25, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 571
    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "Mobile"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 572
    const-string v24, "data2"

    const/16 v25, 0x2

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 592
    :goto_4
    const-string v24, "data1"

    const/16 v25, 0x2

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 593
    const-string v24, "is_primary"

    const/16 v25, 0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 594
    invoke-virtual {v10}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    :cond_9
    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "null"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_5

    .line 599
    sget-object v24, Lcom/samsung/sec/android/application/csc/CscChameleon;->DATA_URI:Landroid/net/Uri;

    invoke-static/range {v24 .. v24}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    .line 600
    const-string v24, "raw_contact_id"

    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 601
    const-string v24, "mimetype"

    const-string v25, "vnd.android.cursor.item/website"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 602
    const-string v24, "data1"

    const/16 v25, 0x4

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 603
    invoke-virtual {v10}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 573
    :cond_a
    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "Home"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 574
    const-string v24, "data2"

    const/16 v25, 0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto/16 :goto_4

    .line 575
    :cond_b
    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "Work"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 576
    const-string v24, "data2"

    const/16 v25, 0x3

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto/16 :goto_4

    .line 577
    :cond_c
    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "Fax Work"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_d

    .line 578
    const-string v24, "data2"

    const/16 v25, 0x4

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto/16 :goto_4

    .line 579
    :cond_d
    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "Fax Home"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_e

    .line 580
    const-string v24, "data2"

    const/16 v25, 0x5

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto/16 :goto_4

    .line 581
    :cond_e
    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "Pager"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 582
    const-string v24, "data2"

    const/16 v25, 0x6

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto/16 :goto_4

    .line 583
    :cond_f
    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "Other"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_10

    .line 584
    const-string v24, "data2"

    const/16 v25, 0x7

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto/16 :goto_4

    .line 585
    :cond_10
    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    const-string v25, "Callback"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_11

    .line 586
    const-string v24, "data2"

    const/16 v25, 0x8

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto/16 :goto_4

    .line 588
    :cond_11
    const-string v24, "data2"

    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 589
    const-string v24, "data3"

    const/16 v25, 0x3

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto/16 :goto_4

    .line 614
    .end local v10    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .end local v12    # "columnOfRecord":I
    .end local v18    # "path":Ljava/lang/String;
    .end local v20    # "rawContactIdIndex":I
    .end local v22    # "tokenizer":Ljava/util/StringTokenizer;
    .end local v23    # "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v13

    .line 615
    .local v13, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v13}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto/16 :goto_1

    .line 612
    .end local v13    # "e":Landroid/content/OperationApplicationException;
    :catch_1
    move-exception v24

    goto/16 :goto_1
.end method

.method private updateHomescreen()V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 917
    const-string v7, "Start updateLauncher()"

    invoke-direct {p0, v7}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 918
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 919
    .local v5, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 920
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v3, Landroid/content/Intent;

    const-string v7, "android.intent.action.CSC_CHAMELEON_UPDATE_LAUNCHER"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 922
    .local v3, "intent":Landroid/content/Intent;
    const-string v7, "counter"

    invoke-interface {v5, v7, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 923
    .local v0, "counter":I
    const-string v7, "isFactoryReset"

    invoke-interface {v5, v7, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 924
    .local v4, "isFactoryReset":Z
    const-string v7, "CscChameleon"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "updateHomeScreen() was called for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " times."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "Operators.SubscriberCarrierId"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    if-gt v0, v2, :cond_1

    .line 930
    if-nez v0, :cond_0

    .line 932
    const-string v7, "isFactoryReset"

    invoke-interface {v1, v7, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 933
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 934
    const-string v7, "CscChameleon"

    const-string v8, "Factory Data Reset occurred"

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    :cond_0
    const-string v7, "CscChameleon"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "updateHomeScreen() - Reseller ID from Parser is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v10, "Operators.SubscriberCarrierId"

    invoke-virtual {v9, v10}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    :cond_1
    if-gt v0, v2, :cond_3

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "Operators.SubscriberCarrierId"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    if-nez v4, :cond_3

    .line 941
    .local v2, "flag":Z
    :goto_0
    const-string v6, "CscChameleon"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateHomeScreen() - will launch.db be deleted? "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    const-string v6, "delete_db"

    invoke-virtual {v3, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 943
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 944
    const-string v6, "CscChameleon"

    const-string v7, "updateHomeScreen() - Broadcast intent sent"

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 946
    const/4 v6, 0x2

    if-gt v0, v6, :cond_2

    .line 947
    const-string v6, "counter"

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v6, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 948
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 950
    :cond_2
    return-void

    .end local v2    # "flag":Z
    :cond_3
    move v2, v6

    .line 940
    goto :goto_0
.end method

.method private updateModemSetting()V
    .locals 6

    .prologue
    .line 988
    const-string v3, "Start updateModemSetting()"

    invoke-direct {p0, v3}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 989
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Operators.DiagMslReq"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 990
    .local v0, "diagMslReq":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Operators.TetheredData"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 991
    .local v2, "tetheredData":Ljava/lang/String;
    const-string v3, "CscChameleon"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tethereddata"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.samsung.sec.android.application.csc.chameleon_diag"

    const-string v4, "android_secret_code://diag_msl"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 994
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "String"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 995
    const-string v3, "tethered"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 996
    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 997
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 999
    return-void
.end method

.method private updateSettings()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1002
    const-string v4, "Start updateSettings()"

    invoke-direct {p0, v4}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 1003
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "RoamPref.MenuDisplay"

    invoke-virtual {v4, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1004
    .local v2, "menuDisplay":Ljava/lang/String;
    const-string v4, "persist.sys.roaming_menu"

    invoke-static {v4, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "RoamPref.HomeOnly"

    invoke-virtual {v4, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1007
    .local v0, "homeOnly":Ljava/lang/String;
    invoke-direct {p0, v7, v0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateSystemSelect(ZLjava/lang/String;)V

    .line 1009
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "Operators.TetheredData"

    invoke-virtual {v4, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1010
    .local v3, "tetherdData":Ljava/lang/String;
    const-string v4, "persist.sys.tether_data"

    invoke-static {v4, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.CSC_CHAMELEON_UPDATE_SETTINGS"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1013
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "roaming_menu"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1014
    const-string v4, "tether_data"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1015
    const-string v4, "operators_carrierhomepage"

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v6, "Operators.CarrierHomePage"

    invoke-virtual {v5, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1016
    const-string v4, "operators_brandalpha"

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v6, "Operators.BrandAlpha"

    invoke-virtual {v5, v6}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1017
    const/16 v4, 0x20

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1018
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1021
    new-instance v4, Lcom/samsung/sec/android/application/csc/CscSettings;

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/sec/android/application/csc/CscSettings;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v7}, Lcom/samsung/sec/android/application/csc/CscSettings;->updateRingtones(I)V

    .line 1022
    return-void
.end method

.method private updateSmsMms()V
    .locals 7

    .prologue
    .line 871
    const-string v4, "Start updateSmsMms()"

    invoke-direct {p0, v4}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 872
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "Mms.ServerUrl"

    invoke-virtual {v4, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 873
    .local v3, "serverURL":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "Mms.Proxy"

    invoke-virtual {v4, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 874
    .local v2, "proxy":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v5, "Operators.AndroidOperatorNetworkCode"

    invoke-virtual {v4, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 876
    .local v1, "networkcode":Ljava/lang/String;
    const-string v4, "CscChameleon"

    const-string v5, "updateSmsMms() "

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    const-string v4, "CscChameleon"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mms.ServerUrl:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    const-string v4, "CscChameleon"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mms.Proxy:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.CHAMELEON_SMS_MMS_UPDATE"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 882
    .local v0, "mmsProvIntent":Landroid/content/Intent;
    const-string v4, "serverURL"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 883
    const-string v4, "proxy"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 884
    const-string v4, "networkcode"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 885
    const-string v4, "key"

    const-string v5, "MmsProvisioning"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 886
    const/16 v4, 0x20

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 887
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 889
    return-void
.end method

.method private updateSystemSelect(ZLjava/lang/String;)V
    .locals 2
    .param p1, "fileExists"    # Z
    .param p2, "homeOnly"    # Ljava/lang/String;

    .prologue
    .line 1025
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CSC_CHAMELEON_UPDATE_CALL_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1026
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "file_exists"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1027
    if-eqz p2, :cond_0

    .line 1028
    const-string v1, "home_only"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1029
    :cond_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1030
    return-void
.end method

.method private updateTelephony()V
    .locals 9

    .prologue
    .line 892
    const-string v6, "Start updateTelephony()"

    invoke-direct {p0, v6}, Lcom/samsung/sec/android/application/csc/CscChameleon;->writeLog(Ljava/lang/String;)V

    .line 893
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "Operators.BrandAlpha"

    invoke-virtual {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 894
    .local v0, "brandAlpha":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "Operators.AndroidOperatorNetworkCode"

    invoke-virtual {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 895
    .local v2, "networkCode":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "Operators.SubscriberCarrierId"

    invoke-virtual {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 896
    .local v4, "resellerId":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "Mms.ServerUrl"

    invoke-virtual {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 897
    .local v5, "serverUrl":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "Mms.Proxy"

    invoke-virtual {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 899
    .local v3, "proxy":Ljava/lang/String;
    const-string v6, "CscChameleon"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Start updateTelephony() - Operators.BrandAlpha:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "Operators.AndroidOperatorNetworkCode"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "Operators.SubscriberCarrierId"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "Mms.ServerUrl"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "Mms.Proxy"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    new-instance v1, Landroid/content/Intent;

    const-string v6, "android.intent.action.CHAMELEON_TELEPHONY_UPDATE"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 907
    .local v1, "intentChameleonTelephonyUpdate":Landroid/content/Intent;
    const-string v6, "brandalpha"

    invoke-virtual {v1, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 908
    const-string v6, "networkcode"

    invoke-virtual {v1, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 909
    const-string v6, "resellerid"

    invoke-virtual {v1, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 910
    const-string v6, "serverURL"

    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 911
    const-string v6, "proxy"

    invoke-virtual {v1, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 912
    const/16 v6, 0x20

    invoke-virtual {v1, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 913
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 914
    return-void
.end method

.method private writeLog(Ljava/lang/String;)V
    .locals 7
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 275
    const/4 v2, 0x0

    .line 277
    .local v2, "fos":Ljava/io/OutputStreamWriter;
    :try_start_0
    const-string v4, "CscChameleon"

    invoke-static {v4, p1}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->outputContents:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 280
    new-instance v1, Ljava/io/File;

    sget-object v4, Lcom/samsung/sec/android/application/csc/CscChameleon;->CHAMELEON_UPDATE_FILE:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 281
    .local v1, "file":Ljava/io/File;
    new-instance v3, Ljava/io/OutputStreamWriter;

    new-instance v4, Ljava/io/FileOutputStream;

    const/4 v5, 0x1

    invoke-direct {v4, v1, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    const-string v5, "UTF-8"

    invoke-direct {v3, v4, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    .end local v2    # "fos":Ljava/io/OutputStreamWriter;
    .local v3, "fos":Ljava/io/OutputStreamWriter;
    :try_start_1
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->outputContents:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 284
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->outputContents:Ljava/lang/StringBuffer;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->setLength(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 289
    if-eqz v3, :cond_0

    .line 290
    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 295
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v2    # "fos":Ljava/io/OutputStreamWriter;
    :cond_1
    :goto_0
    return-void

    .line 291
    .end local v2    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "fos":Ljava/io/OutputStreamWriter;
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "CscChameleon"

    const-string v5, "Failed to writeLog(close): "

    invoke-static {v4, v5, v0}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v2, v3

    .line 294
    .end local v3    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v2    # "fos":Ljava/io/OutputStreamWriter;
    goto :goto_0

    .line 285
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "file":Ljava/io/File;
    :catch_1
    move-exception v0

    .line 286
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    const-string v4, "CscChameleon"

    const-string v5, "Failed to writeLog: "

    invoke-static {v4, v5, v0}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 289
    if-eqz v2, :cond_1

    .line 290
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 291
    :catch_2
    move-exception v0

    .line 292
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "CscChameleon"

    const-string v5, "Failed to writeLog(close): "

    invoke-static {v4, v5, v0}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 288
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 289
    :goto_2
    if-eqz v2, :cond_2

    .line 290
    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 293
    :cond_2
    :goto_3
    throw v4

    .line 291
    :catch_3
    move-exception v0

    .line 292
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "CscChameleon"

    const-string v6, "Failed to writeLog(close): "

    invoke-static {v5, v6, v0}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3

    .line 288
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "fos":Ljava/io/OutputStreamWriter;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v2    # "fos":Ljava/io/OutputStreamWriter;
    goto :goto_2

    .line 285
    .end local v2    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v3    # "fos":Ljava/io/OutputStreamWriter;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v2    # "fos":Ljava/io/OutputStreamWriter;
    goto :goto_1
.end method


# virtual methods
.method public CopyRingtone(Ljava/lang/String;)V
    .locals 15
    .param p1, "uriPath"    # Ljava/lang/String;

    .prologue
    .line 1033
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1034
    const-string v2, "CscChameleon"

    const-string v3, "File path for ringtone is empty"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    :cond_0
    :goto_0
    return-void

    .line 1038
    :cond_1
    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 1039
    .local v13, "uri":Landroid/net/Uri;
    new-instance v12, Ljava/io/File;

    invoke-virtual {v13}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v12, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1041
    .local v12, "ringtoneFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1042
    const-string v2, "CscChameleon"

    const-string v3, "Ringtone doesn\'t exist"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1046
    :cond_2
    sget-object v2, Landroid/os/Environment;->DIRECTORY_RINGTONES:Ljava/lang/String;

    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1049
    .local v0, "dir":Ljava/io/File;
    const/4 v8, 0x0

    .line 1050
    .local v8, "fis":Ljava/io/FileInputStream;
    const/4 v10, 0x0

    .line 1051
    .local v10, "fos":Ljava/io/FileOutputStream;
    const/4 v1, 0x0

    .line 1052
    .local v1, "fcIn":Ljava/nio/channels/FileChannel;
    const/4 v6, 0x0

    .line 1055
    .local v6, "fcOut":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1056
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .local v9, "fis":Ljava/io/FileInputStream;
    if-nez v9, :cond_6

    .line 1057
    :try_start_1
    const-string v2, "CscChameleon"

    const-string v3, "Ringtone source file don\'t exist"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1075
    if-eqz v9, :cond_3

    .line 1076
    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 1077
    :cond_3
    if-eqz v10, :cond_4

    .line 1078
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 1079
    :cond_4
    if-eqz v1, :cond_5

    .line 1080
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 1081
    :cond_5
    if-eqz v6, :cond_0

    .line 1082
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1083
    :catch_0
    move-exception v7

    .line 1084
    .local v7, "e":Ljava/lang/Exception;
    const-string v2, "CscChameleon"

    const-string v3, "Failed to close files when copying ringtone"

    invoke-static {v2, v3, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 1061
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_6
    :try_start_3
    new-instance v11, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v11, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1062
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .local v11, "fos":Ljava/io/FileOutputStream;
    if-nez v11, :cond_a

    .line 1063
    :try_start_4
    const-string v2, "CscChameleon"

    const-string v3, "Ringtone target file problem"

    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1075
    if-eqz v9, :cond_7

    .line 1076
    :try_start_5
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 1077
    :cond_7
    if-eqz v11, :cond_8

    .line 1078
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    .line 1079
    :cond_8
    if-eqz v1, :cond_9

    .line 1080
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 1081
    :cond_9
    if-eqz v6, :cond_0

    .line 1082
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 1083
    :catch_1
    move-exception v7

    .line 1084
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v2, "CscChameleon"

    const-string v3, "Failed to close files when copying ringtone"

    invoke-static {v2, v3, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 1067
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_a
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 1068
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    .line 1069
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1075
    if-eqz v9, :cond_b

    .line 1076
    :try_start_7
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 1077
    :cond_b
    if-eqz v11, :cond_c

    .line 1078
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    .line 1079
    :cond_c
    if-eqz v1, :cond_d

    .line 1080
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 1081
    :cond_d
    if-eqz v6, :cond_e

    .line 1082
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    .line 1088
    :cond_e
    :goto_1
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MEDIA_MOUNTED"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "file://"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1083
    :catch_2
    move-exception v7

    .line 1084
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v2, "CscChameleon"

    const-string v3, "Failed to close files when copying ringtone"

    invoke-static {v2, v3, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 1070
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v7

    .line 1071
    .restart local v7    # "e":Ljava/lang/Exception;
    :goto_2
    :try_start_8
    const-string v2, "CscChameleon"

    const-string v3, "Failed to copy ringtone to SD card"

    invoke-static {v2, v3, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1075
    if-eqz v8, :cond_f

    .line 1076
    :try_start_9
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    .line 1077
    :cond_f
    if-eqz v10, :cond_10

    .line 1078
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 1079
    :cond_10
    if-eqz v1, :cond_11

    .line 1080
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 1081
    :cond_11
    if-eqz v6, :cond_0

    .line 1082
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_0

    .line 1083
    :catch_4
    move-exception v7

    .line 1084
    const-string v2, "CscChameleon"

    const-string v3, "Failed to close files when copying ringtone"

    invoke-static {v2, v3, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 1074
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 1075
    :goto_3
    if-eqz v8, :cond_12

    .line 1076
    :try_start_a
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    .line 1077
    :cond_12
    if-eqz v10, :cond_13

    .line 1078
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 1079
    :cond_13
    if-eqz v1, :cond_14

    .line 1080
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 1081
    :cond_14
    if-eqz v6, :cond_15

    .line 1082
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    .line 1085
    :cond_15
    :goto_4
    throw v2

    .line 1083
    :catch_5
    move-exception v7

    .line 1084
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v3, "CscChameleon"

    const-string v4, "Failed to close files when copying ringtone"

    invoke-static {v3, v4, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_4

    .line 1074
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v2

    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v2

    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 1070
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v7

    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v7

    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public compare()Ljava/lang/String;
    .locals 8

    .prologue
    .line 229
    const-string v1, "NOERROR"

    .line 230
    .local v1, "compareresult":Ljava/lang/String;
    const/4 v5, 0x0

    .line 231
    .local v5, "parsedString":Ljava/lang/String;
    const/4 v6, 0x0

    .line 232
    .local v6, "str":Ljava/lang/String;
    const/4 v3, 0x0

    .line 235
    .local v3, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    const-string v7, "/proc/mounts"

    invoke-direct {v4, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-direct {v7, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 238
    .local v0, "bufferReader":Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 239
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 241
    :cond_0
    if-eqz v5, :cond_1

    const-string v7, "/carrier"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 242
    const-string v1, "/carrier unmounted!"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 248
    :cond_1
    if-eqz v4, :cond_2

    .line 249
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    move-object v3, v4

    .line 255
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :cond_3
    :goto_1
    return-object v1

    .line 250
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v2

    .line 251
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v3, v4

    .line 253
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 244
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 245
    .restart local v2    # "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 248
    if-eqz v3, :cond_3

    .line 249
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 250
    :catch_2
    move-exception v2

    .line 251
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 247
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    .line 248
    :goto_3
    if-eqz v3, :cond_4

    .line 249
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 252
    :cond_4
    :goto_4
    throw v7

    .line 250
    :catch_3
    move-exception v2

    .line 251
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 247
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 244
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 0
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 1098
    return-void
.end method

.method public reboot()V
    .locals 4

    .prologue
    .line 300
    const-wide/16 v2, 0x2710

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 306
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscChameleon;->mContext:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 307
    .local v1, "pm":Landroid/os/PowerManager;
    const-string v2, "CscChameleon"

    invoke-virtual {v1, v2}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 308
    .end local v1    # "pm":Landroid/os/PowerManager;
    :goto_0
    return-void

    .line 301
    :catch_0
    move-exception v0

    .line 302
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public update()V
    .locals 0

    .prologue
    .line 259
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateChameleon()V

    .line 260
    return-void
.end method

.method public update(Z)V
    .locals 2
    .param p1, "reboot"    # Z

    .prologue
    .line 267
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateChameleon()V

    .line 268
    const/4 v0, 0x1

    if-ne v0, p1, :cond_0

    .line 269
    const-string v0, "CscChameleon"

    const-string v1, "Reboot after 10 secs to make some time-buffers chameleon is updated"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->reboot()V

    .line 272
    :cond_0
    return-void
.end method

.method public updateForSubUser()V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscChameleon;->updateChameleon()V

    .line 264
    return-void
.end method
