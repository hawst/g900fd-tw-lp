.class Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;
.super Ljava/lang/Object;
.source "CscConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CarrierTypes"
.end annotation


# instance fields
.field public isEnableCbs:Z

.field public isEnableDefault:Z

.field public isEnableDun:Z

.field public isEnableEmail:Z

.field public isEnableFota:Z

.field public isEnableHipri:Z

.field public isEnableIms:Z

.field public isEnableMms:Z

.field public isEnableSupl:Z

.field public isEnableSyncDm:Z

.field public isEnableWap:Z

.field public isEnableXcap:Z

.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscConnection;


# direct methods
.method private constructor <init>(Lcom/samsung/sec/android/application/csc/CscConnection;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 204
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->this$0:Lcom/samsung/sec/android/application/csc/CscConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableDefault:Z

    .line 207
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableMms:Z

    .line 209
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableSupl:Z

    .line 211
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableDun:Z

    .line 213
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableHipri:Z

    .line 215
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableCbs:Z

    .line 217
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableFota:Z

    .line 219
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableIms:Z

    .line 221
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableXcap:Z

    .line 223
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableSyncDm:Z

    .line 225
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableWap:Z

    .line 227
    iput-boolean v0, p0, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;->isEnableEmail:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/sec/android/application/csc/CscConnection;Lcom/samsung/sec/android/application/csc/CscConnection$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/sec/android/application/csc/CscConnection;
    .param p2, "x1"    # Lcom/samsung/sec/android/application/csc/CscConnection$1;

    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/samsung/sec/android/application/csc/CscConnection$CarrierTypes;-><init>(Lcom/samsung/sec/android/application/csc/CscConnection;)V

    return-void
.end method
