.class Lcom/samsung/sec/android/application/csc/CscEncodeService$1;
.super Ljava/lang/Object;
.source "CscEncodeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/sec/android/application/csc/CscEncodeService;->doEncode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscEncodeService;


# direct methods
.method constructor <init>(Lcom/samsung/sec/android/application/csc/CscEncodeService;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscEncodeService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscEncodeService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 44
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscEncodeService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscEncodeService;

    new-instance v3, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    const-string v4, "/sdcard/customer_enc.xml"

    invoke-direct {v3, v4}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/samsung/sec/android/application/csc/CscEncodeService;->mCscXMLEncoder:Lcom/samsung/sec/android/application/csc/CscXMLEncoder;
    invoke-static {v2, v3}, Lcom/samsung/sec/android/application/csc/CscEncodeService;->access$002(Lcom/samsung/sec/android/application/csc/CscEncodeService;Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .line 45
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscEncodeService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscEncodeService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscEncodeService;->mCscXMLEncoder:Lcom/samsung/sec/android/application/csc/CscXMLEncoder;
    invoke-static {v2}, Lcom/samsung/sec/android/application/csc/CscEncodeService;->access$000(Lcom/samsung/sec/android/application/csc/CscEncodeService;)Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    move-result-object v2

    const-string v3, "CustomerData"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->startXML(Ljava/lang/String;)V

    .line 47
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscEncodeService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscEncodeService;

    iget-object v2, v2, Lcom/samsung/sec/android/application/csc/CscEncodeService;->mEncodables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/sec/android/application/csc/Encodable;

    .line 48
    .local v0, "encodable":Lcom/samsung/sec/android/application/csc/Encodable;
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscEncodeService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscEncodeService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscEncodeService;->mCscXMLEncoder:Lcom/samsung/sec/android/application/csc/CscXMLEncoder;
    invoke-static {v2}, Lcom/samsung/sec/android/application/csc/CscEncodeService;->access$000(Lcom/samsung/sec/android/application/csc/CscEncodeService;)Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/samsung/sec/android/application/csc/Encodable;->encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V

    goto :goto_0

    .line 51
    .end local v0    # "encodable":Lcom/samsung/sec/android/application/csc/Encodable;
    :cond_0
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscEncodeService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscEncodeService;

    # getter for: Lcom/samsung/sec/android/application/csc/CscEncodeService;->mCscXMLEncoder:Lcom/samsung/sec/android/application/csc/CscXMLEncoder;
    invoke-static {v2}, Lcom/samsung/sec/android/application/csc/CscEncodeService;->access$000(Lcom/samsung/sec/android/application/csc/CscEncodeService;)Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscXMLEncoder;->endXML()V

    .line 52
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscEncodeService$1;->this$0:Lcom/samsung/sec/android/application/csc/CscEncodeService;

    invoke-virtual {v2}, Lcom/samsung/sec/android/application/csc/CscEncodeService;->copyOrgCustomerXml()V

    .line 54
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    move-result-object v2

    const-string v3, "Completed"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->endLog(Ljava/lang/String;)V

    .line 55
    return-void
.end method
