.class Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;
.super Landroid/util/AndroidException;
.source "CscCamera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscCamera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CscCameraException"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mStrTrace:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscCamera;


# direct methods
.method public constructor <init>(Lcom/samsung/sec/android/application/csc/CscCamera;Ljava/lang/String;)V
    .locals 0
    .param p2, "strTrace"    # Ljava/lang/String;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->this$0:Lcom/samsung/sec/android/application/csc/CscCamera;

    .line 206
    invoke-direct {p0}, Landroid/util/AndroidException;-><init>()V

    .line 207
    invoke-virtual {p0, p2}, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->setTrace(Ljava/lang/String;)V

    .line 208
    return-void
.end method


# virtual methods
.method public getTrace()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->mStrTrace:Ljava/lang/String;

    return-object v0
.end method

.method public setTrace(Ljava/lang/String;)V
    .locals 0
    .param p1, "strTrace"    # Ljava/lang/String;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscCamera$CscCameraException;->mStrTrace:Ljava/lang/String;

    .line 212
    return-void
.end method
