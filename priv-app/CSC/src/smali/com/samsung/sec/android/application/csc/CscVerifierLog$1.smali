.class Lcom/samsung/sec/android/application/csc/CscVerifierLog$1;
.super Ljava/lang/Object;
.source "CscVerifierLog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/sec/android/application/csc/CscVerifierLog;->saveLogFile()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscVerifierLog;


# direct methods
.method constructor <init>(Lcom/samsung/sec/android/application/csc/CscVerifierLog;)V
    .locals 0

    .prologue
    .line 537
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 539
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->getInstance()Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    move-result-object v0

    .line 540
    .local v0, "cscVerifierActivity":Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
    const/4 v2, 0x0

    .line 541
    .local v2, "file":Ljava/io/File;
    const/4 v4, 0x0

    .line 544
    .local v4, "fos":Ljava/io/FileOutputStream;
    if-eqz v0, :cond_0

    .line 545
    :try_start_0
    const-string v6, "Saving log file to sdcard..."

    invoke-virtual {v0, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->updateToast(Ljava/lang/String;)V

    .line 547
    :cond_0
    new-instance v3, Ljava/io/File;

    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierLog;->LOG_FILE_PATH:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->access$000()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    .end local v2    # "file":Ljava/io/File;
    .local v3, "file":Ljava/io/File;
    :try_start_1
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 550
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .local v5, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mLog:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->access$100(Lcom/samsung/sec/android/application/csc/CscVerifierLog;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "\n"

    const-string v8, "\r\n"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "UTF-8"

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/FileOutputStream;->write([B)V

    .line 552
    if-eqz v0, :cond_1

    .line 553
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Saved "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierLog;->LOG_FILE_PATH:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->access$000()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->updateToast(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 560
    :cond_1
    if-eqz v5, :cond_2

    .line 561
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 565
    :cond_2
    :goto_0
    const-string v6, "CscVerifierLog"

    const-string v7, "Finish saveLogFile"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    # invokes: Lcom/samsung/sec/android/application/csc/CscVerifierLog;->clearLog()V
    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->access$200(Lcom/samsung/sec/android/application/csc/CscVerifierLog;)V

    .line 567
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    # setter for: Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mbSavingLog:Z
    invoke-static {v6, v9}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->access$302(Lcom/samsung/sec/android/application/csc/CscVerifierLog;Z)Z

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v2, v3

    .line 569
    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    :goto_1
    return-void

    .line 562
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 563
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 554
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v1

    .line 555
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_2
    if-eqz v0, :cond_3

    .line 556
    :try_start_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed Saving log file : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->updateToast(Ljava/lang/String;)V

    .line 557
    :cond_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 560
    if-eqz v4, :cond_4

    .line 561
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 565
    :cond_4
    :goto_3
    const-string v6, "CscVerifierLog"

    const-string v7, "Finish saveLogFile"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    # invokes: Lcom/samsung/sec/android/application/csc/CscVerifierLog;->clearLog()V
    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->access$200(Lcom/samsung/sec/android/application/csc/CscVerifierLog;)V

    .line 567
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    # setter for: Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mbSavingLog:Z
    invoke-static {v6, v9}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->access$302(Lcom/samsung/sec/android/application/csc/CscVerifierLog;Z)Z

    goto :goto_1

    .line 562
    :catch_2
    move-exception v1

    .line 563
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 559
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 560
    :goto_4
    if-eqz v4, :cond_5

    .line 561
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 565
    :cond_5
    :goto_5
    const-string v7, "CscVerifierLog"

    const-string v8, "Finish saveLogFile"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    # invokes: Lcom/samsung/sec/android/application/csc/CscVerifierLog;->clearLog()V
    invoke-static {v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->access$200(Lcom/samsung/sec/android/application/csc/CscVerifierLog;)V

    .line 567
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscVerifierLog$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierLog;

    # setter for: Lcom/samsung/sec/android/application/csc/CscVerifierLog;->mbSavingLog:Z
    invoke-static {v7, v9}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->access$302(Lcom/samsung/sec/android/application/csc/CscVerifierLog;Z)Z

    throw v6

    .line 562
    :catch_3
    move-exception v1

    .line 563
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 559
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "file":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    goto :goto_4

    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v6

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v2, v3

    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    goto :goto_4

    .line 554
    .end local v2    # "file":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    goto :goto_2

    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v1

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v2, v3

    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    goto :goto_2
.end method
