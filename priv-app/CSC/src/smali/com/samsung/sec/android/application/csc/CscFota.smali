.class public Lcom/samsung/sec/android/application/csc/CscFota;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscFota.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscFota$1;,
        Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;,
        Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;
    }
.end annotation


# instance fields
.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final DEFAULT_FOTA_BEARER:Ljava/lang/String;

.field private final LOG_TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

.field private mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

.field private mParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 17
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscFota;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 19
    const-string v0, "default"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscFota;->DEFAULT_FOTA_BEARER:Ljava/lang/String;

    .line 35
    const-string v0, "CscFota"

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscFota;->LOG_TAG:Ljava/lang/String;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 68
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mContext:Landroid/content/Context;

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mResolver:Landroid/content/ContentResolver;

    .line 70
    return-void
.end method

.method private loadFotaProfiles()I
    .locals 9

    .prologue
    const/4 v5, -0x1

    .line 124
    const/4 v3, 0x0

    .line 128
    .local v3, "size":I
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "Settings"

    invoke-virtual {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 129
    .local v1, "node":Lorg/w3c/dom/Node;
    if-nez v1, :cond_1

    .line 164
    :cond_0
    :goto_0
    return v5

    .line 132
    :cond_1
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "FOTA"

    invoke-virtual {v6, v1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 134
    .local v2, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v2, :cond_2

    .line 135
    const-string v6, "CscFota"

    const-string v7, "No FOTA TAG"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 139
    :cond_2
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    .line 141
    const-string v6, "CscFota"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "size: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " - loadFotaProfiles"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    if-lez v3, :cond_0

    .line 147
    new-array v5, v3, [Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    iput-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    .line 149
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_5

    .line 150
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    new-instance v6, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;-><init>(Lcom/samsung/sec/android/application/csc/CscFota;Lcom/samsung/sec/android/application/csc/CscFota$1;)V

    aput-object v6, v5, v0

    .line 152
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "NetworkName"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 153
    .local v4, "temp":Lorg/w3c/dom/Node;
    if-eqz v4, :cond_3

    .line 154
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->nwkName:Ljava/lang/String;

    .line 156
    :cond_3
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "FotaBearer"

    invoke-virtual {v5, v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 157
    if-eqz v4, :cond_4

    .line 158
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->fotaBearer:Ljava/lang/String;

    .line 160
    :cond_4
    const-string v5, "CscFota"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "nwkName: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->nwkName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "fotaBearer: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->fotaBearer:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 164
    .end local v4    # "temp":Lorg/w3c/dom/Node;
    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method private loadNetworkInfo()I
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 73
    const/4 v4, 0x0

    .line 77
    .local v4, "size":I
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "GeneralInfo"

    invoke-virtual {v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 78
    .local v2, "node":Lorg/w3c/dom/Node;
    if-nez v2, :cond_0

    .line 120
    :goto_0
    return v6

    .line 81
    :cond_0
    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v9, "NetworkInfo"

    invoke-virtual {v8, v2, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 83
    .local v3, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v3, :cond_1

    .line 84
    const-string v7, "CscFota"

    const-string v8, "No network info"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 88
    :cond_1
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 90
    const-string v6, "CscFota"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "size: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    new-array v6, v4, [Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    iput-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    .line 94
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v4, :cond_4

    .line 95
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    new-instance v8, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;-><init>(Lcom/samsung/sec/android/application/csc/CscFota;Lcom/samsung/sec/android/application/csc/CscFota$1;)V

    aput-object v8, v6, v0

    .line 97
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "NetworkName"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 99
    .local v5, "temp":Lorg/w3c/dom/Node;
    if-eqz v5, :cond_2

    .line 100
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v6, v6, v0

    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v8, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mName:Ljava/lang/String;

    .line 103
    :cond_2
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "MCCMNC"

    invoke-virtual {v6, v8, v9}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 105
    if-eqz v5, :cond_3

    .line 106
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v6, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "mccMnc":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v8, 0x4

    if-le v6, v8, :cond_3

    .line 111
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v6, v6, v0

    invoke-virtual {v1, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMcc:Ljava/lang/String;

    .line 113
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v6, v6, v0

    invoke-virtual {v1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMnc:Ljava/lang/String;

    .line 114
    const-string v6, "CscFota"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Load NwkInfo["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v9, v9, v0

    iget-object v9, v9, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMcc:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v9, v9, v0

    iget-object v9, v9, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMnc:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v9, v9, v0

    iget-object v9, v9, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    .end local v1    # "mccMnc":Ljava/lang/String;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .end local v5    # "temp":Lorg/w3c/dom/Node;
    :cond_4
    move v6, v7

    .line 120
    goto/16 :goto_0
.end method


# virtual methods
.method public addUnusedItem(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 358
    .local p1, "filter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "Settings.Main.Network.AutoFOTA"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 359
    const-string v0, "Settings.Main.Network.AutoSyncML"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 360
    const-string v0, "Settings.SyncML.DevManagement.AccName"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 361
    const-string v0, "Settings.SyncML.DevManagement.AddParam.AddressType"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 362
    const-string v0, "Settings.SyncML.DevManagement.AddParam.ApAddressType"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 363
    const-string v0, "Settings.SyncML.DevManagement.AddParam.ApBearerType"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 364
    const-string v0, "Settings.SyncML.DevManagement.AddParam.ProxyAddressType"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 365
    const-string v0, "Settings.SyncML.DevManagement.AuthType"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 366
    const-string v0, "Settings.SyncML.DevManagement.Password"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 367
    const-string v0, "Settings.SyncML.DevManagement.ServAddr"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 368
    const-string v0, "Settings.SyncML.DevManagement.ServID"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 369
    const-string v0, "Settings.SyncML.DevManagement.ServPwd"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 370
    const-string v0, "Settings.SyncML.DevManagement.UserId"

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 371
    return-void
.end method

.method public compare()Ljava/lang/String;
    .locals 5

    .prologue
    .line 324
    const-string v0, "NOERROR"

    .line 325
    .local v0, "answer":Ljava/lang/String;
    const-string v1, "CscFota"

    const-string v2, "CscFota compare()"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    const-string v1, "READY"

    const-string v2, "gsm.sim.state"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 328
    const-string v1, "CscFota"

    const-string v2, "no SIM - no need to compare CSCFOTA"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    const-string v1, "Settings.FOTA.NetworkName"

    const-string v2, "No SIM"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const-string v2, "Settings.FOTA.FotaBearer"

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "SMLDM_BEARER"

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    const-string v1, "WIFI_DEDICATED"

    :goto_0
    invoke-static {v2, v1}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    .end local v0    # "answer":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v0

    .line 330
    .restart local v0    # "answer":Ljava/lang/String;
    :cond_1
    const-string v1, "NORMAL"

    goto :goto_0

    .line 335
    :cond_2
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v1, :cond_3

    .line 336
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscFota;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 338
    :cond_3
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscFota;->loadFotaProfiles()I

    move-result v1

    if-eqz v1, :cond_4

    .line 339
    const-string v1, "CscFota"

    const-string v2, "no FOTA profiles in customer"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 343
    :cond_4
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscFota;->loadNetworkInfo()I

    move-result v1

    if-eqz v1, :cond_5

    .line 344
    const-string v1, "CscFota"

    const-string v2, "error during loading network info"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscFota;->compareToDb()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 348
    const-string v1, "CscFota : Failure to compare CSCFOTA"

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 349
    const-string v0, "Failure to compare CSCFOTA"

    goto :goto_1
.end method

.method compareToDb()Ljava/lang/Boolean;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 220
    const/4 v3, 0x0

    .line 221
    .local v3, "nBearer":I
    const/4 v4, 0x0

    .line 222
    .local v4, "nBearerFromSetting":I
    const-string v5, ""

    .line 223
    .local v5, "simNumeric":Ljava/lang/String;
    const-string v6, ""

    .line 225
    .local v6, "temp_netname":Ljava/lang/String;
    const-string v7, "gsm.sim.operator.numeric"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 226
    const-string v7, "CscFota"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Current SIM : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    if-nez v5, :cond_0

    .line 228
    const-string v7, "CscFota"

    const-string v8, "[Compare]gsm.sim.operator.numeric == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 299
    :goto_0
    return-object v7

    .line 232
    :cond_0
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    if-nez v7, :cond_1

    .line 233
    const-string v7, "CscFota"

    const-string v8, "[Compare]No network info, write nothing to DB"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    goto :goto_0

    .line 237
    :cond_1
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    array-length v7, v7

    if-ne v7, v10, :cond_4

    const-string v7, "DEFAULT"

    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v8, v8, v11

    iget-object v8, v8, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->nwkName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "N/R"

    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v8, v8, v11

    iget-object v8, v8, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->nwkName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_2
    const-string v7, "WIFI_DEDICATED"

    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v8, v8, v11

    iget-object v8, v8, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->fotaBearer:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 242
    const-string v7, "CscFota"

    const-string v8, "All operator => default : WIFI_DEDICATED"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const/4 v3, 0x1

    .line 270
    :cond_3
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "SMLDM_BEARER"

    invoke-static {v7, v8, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 272
    const-string v7, "CscFota"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SMLDM_BEARER from CSC : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " SMLDM_BEARER from Settings : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    if-ne v3, v4, :cond_b

    .line 275
    const-string v7, "CscFota"

    const-string v8, "CSCFOTA compare return true"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    const-string v7, ""

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 277
    const-string v7, "Settings.FOTA.NetworkName"

    const-string v8, "default"

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string v8, "Settings.FOTA.FotaBearer"

    if-ne v3, v10, :cond_8

    const-string v7, "WIFI_DEDICATED"

    :goto_1
    invoke-static {v8, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :goto_2
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    goto/16 :goto_0

    .line 245
    :cond_4
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    array-length v7, v7

    if-ge v1, v7, :cond_3

    .line 247
    const-string v0, "default"

    .line 249
    .local v0, "fotaBearer":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    if-eqz v7, :cond_5

    .line 250
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_4
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    array-length v7, v7

    if-ge v2, v7, :cond_5

    .line 251
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mName:Ljava/lang/String;

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mName:Ljava/lang/String;

    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v8, v8, v2

    iget-object v8, v8, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->nwkName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 253
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v7, v7, v2

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->fotaBearer:Ljava/lang/String;

    if-eqz v7, :cond_5

    .line 254
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v7, v7, v2

    iget-object v0, v7, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->fotaBearer:Ljava/lang/String;

    .line 260
    .end local v2    # "j":I
    :cond_5
    const-string v7, "CscFota"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Add fotabearer : NwkInfo["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v9, v9, v1

    iget-object v9, v9, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMcc:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v9, v9, v1

    iget-object v9, v9, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMnc:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v9, v9, v1

    iget-object v9, v9, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v8, v8, v1

    iget-object v8, v8, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMcc:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v8, v8, v1

    iget-object v8, v8, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMnc:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    const-string v7, "WIFI_DEDICATED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 264
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v7, v7, v1

    iget-object v6, v7, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mName:Ljava/lang/String;

    .line 265
    const/4 v3, 0x1

    .line 245
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    .line 250
    .restart local v2    # "j":I
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    .line 278
    .end local v0    # "fotaBearer":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "j":I
    :cond_8
    const-string v7, "NORMAL"

    goto/16 :goto_1

    .line 281
    :cond_9
    const-string v7, "Settings.FOTA.NetworkName"

    invoke-static {v7, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v8, "Settings.FOTA.FotaBearer"

    if-ne v3, v10, :cond_a

    const-string v7, "WIFI_DEDICATED"

    :goto_5
    invoke-static {v8, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->pass(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_a
    const-string v7, "NORMAL"

    goto :goto_5

    .line 287
    :cond_b
    const-string v7, "CscFota"

    const-string v8, "CSCFOTA compare return false"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    const-string v7, ""

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 289
    const-string v7, "Settings.FOTA.NetworkName"

    const-string v8, "default"

    const-string v9, "default"

    invoke-static {v7, v8, v9}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const-string v9, "Settings.FOTA.FotaBearer"

    if-ne v3, v10, :cond_c

    const-string v7, "WIFI_DEDICATED"

    move-object v8, v7

    :goto_6
    if-ne v4, v10, :cond_d

    const-string v7, "WIFI_DEDICATED"

    :goto_7
    invoke-static {v9, v8, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :goto_8
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    goto/16 :goto_0

    .line 290
    :cond_c
    const-string v7, "NORMAL"

    move-object v8, v7

    goto :goto_6

    :cond_d
    const-string v7, "NORMAL"

    goto :goto_7

    .line 293
    :cond_e
    const-string v7, "Settings.FOTA.NetworkName"

    invoke-static {v7, v6, v6}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    const-string v9, "Settings.FOTA.FotaBearer"

    if-ne v3, v10, :cond_f

    const-string v7, "WIFI_DEDICATED"

    move-object v8, v7

    :goto_9
    if-ne v4, v10, :cond_10

    const-string v7, "WIFI_DEDICATED"

    :goto_a
    invoke-static {v9, v8, v7}, Lcom/samsung/sec/android/application/csc/CscVerifierLog;->fail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    :cond_f
    const-string v7, "NORMAL"

    move-object v8, v7

    goto :goto_9

    :cond_10
    const-string v7, "NORMAL"

    goto :goto_a
.end method

.method public update()V
    .locals 3

    .prologue
    .line 303
    const-string v0, "CscFota"

    const-string v1, "CscFota update()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v0, :cond_0

    .line 306
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscFota;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 308
    :cond_0
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscFota;->loadFotaProfiles()I

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    const-string v0, "CscFota"

    const-string v1, "no FOTA profiles in customer"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "SMLDM_BEARER"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 311
    const-string v0, "CscFota"

    const-string v1, "putInt SMLDM_BEARER : 0"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CSC_FOTA_UPDATE_DONE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 313
    const-string v0, "CscFota"

    const-string v1, "sendBroadcast : android.intent.action.CSC_FOTA_UPDATE_DONE"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    :goto_0
    return-void

    .line 317
    :cond_1
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscFota;->loadNetworkInfo()I

    move-result v0

    if-eqz v0, :cond_2

    .line 318
    const-string v0, "CscFota"

    const-string v1, "error during loading network info"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscFota;->writeToDb()V

    goto :goto_0
.end method

.method writeToDb()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 168
    const/4 v3, 0x0

    .line 169
    .local v3, "nBearer":I
    const-string v4, ""

    .line 170
    .local v4, "simNumeric":Ljava/lang/String;
    const-string v5, "gsm.sim.operator.numeric"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 171
    const-string v5, "CscFota"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Current SIM : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    if-nez v4, :cond_0

    .line 174
    const-string v5, "CscFota"

    const-string v6, "gsm.sim.operator.numeric == null"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    if-nez v5, :cond_1

    .line 179
    const-string v5, "CscFota"

    const-string v6, "No network info, write nothing to DB"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 183
    :cond_1
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    array-length v5, v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    const-string v5, "DEFAULT"

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v6, v6, v8

    iget-object v6, v6, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->nwkName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "N/R"

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v6, v6, v8

    iget-object v6, v6, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->nwkName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_2
    const-string v5, "WIFI_DEDICATED"

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v6, v6, v8

    iget-object v6, v6, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->fotaBearer:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 188
    const-string v5, "CscFota"

    const-string v6, "All operator => default : WIFI_DEDICATED"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    const/4 v3, 0x1

    .line 213
    :cond_3
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mResolver:Landroid/content/ContentResolver;

    const-string v6, "SMLDM_BEARER"

    invoke-static {v5, v6, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 214
    const-string v5, "CscFota"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "putInt SMLDM_BEARER : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.CSC_FOTA_UPDATE_DONE"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 216
    const-string v5, "CscFota"

    const-string v6, "sendBroadcast : android.intent.action.CSC_FOTA_UPDATE_DONE"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 191
    :cond_4
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 193
    const-string v0, "default"

    .line 195
    .local v0, "fotaBearer":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    if-eqz v5, :cond_5

    .line 196
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    array-length v5, v5

    if-ge v2, v5, :cond_5

    .line 197
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mName:Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mName:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v6, v6, v2

    iget-object v6, v6, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->nwkName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 199
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v5, v5, v2

    iget-object v5, v5, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->fotaBearer:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 200
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mFotaProfile:[Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;

    aget-object v5, v5, v2

    iget-object v0, v5, Lcom/samsung/sec/android/application/csc/CscFota$FotaProfile;->fotaBearer:Ljava/lang/String;

    .line 206
    .end local v2    # "j":I
    :cond_5
    const-string v5, "CscFota"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Add fotabearer : NwkInfo["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMcc:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMnc:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v6, v6, v1

    iget-object v6, v6, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMcc:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscFota;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;

    aget-object v6, v6, v1

    iget-object v6, v6, Lcom/samsung/sec/android/application/csc/CscFota$NwkInfo;->mMnc:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "WIFI_DEDICATED"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 210
    const/4 v3, 0x1

    .line 191
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 196
    .restart local v2    # "j":I
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2
.end method
