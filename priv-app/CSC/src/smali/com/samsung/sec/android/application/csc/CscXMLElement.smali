.class public Lcom/samsung/sec/android/application/csc/CscXMLElement;
.super Ljava/lang/Object;
.source "CscXMLElement.java"


# instance fields
.field doc:Lorg/w3c/dom/Document;

.field private element:Lorg/w3c/dom/Element;


# direct methods
.method public constructor <init>(Lorg/w3c/dom/Element;Lorg/w3c/dom/Document;)V
    .locals 0
    .param p1, "element"    # Lorg/w3c/dom/Element;
    .param p2, "doc"    # Lorg/w3c/dom/Document;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->element:Lorg/w3c/dom/Element;

    .line 27
    iput-object p2, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->doc:Lorg/w3c/dom/Document;

    .line 28
    return-void
.end method

.method private getFullNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 5
    .param p1, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 209
    const-string v0, ""

    .line 212
    .local v0, "fullTagPath":Ljava/lang/String;
    :cond_0
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v1

    .line 213
    .local v1, "nodeName":Ljava/lang/String;
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_4

    .line 215
    const-string v2, "CustomerData"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v2

    if-nez v2, :cond_2

    .line 231
    :cond_1
    :goto_0
    return-object v0

    .line 220
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 221
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 222
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object p1

    .line 229
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 224
    :cond_4
    const-string v2, "CscXMLElement"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFullNodeName(): nodeName is null / tag:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getFullTagSet(Ljava/util/Set;Lorg/w3c/dom/Node;)V
    .locals 9
    .param p2, "parent"    # Lorg/w3c/dom/Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/w3c/dom/Node;",
            ")V"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "tagSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 238
    .local v2, "current":Lorg/w3c/dom/NodeList;
    if-eqz v2, :cond_2

    .line 239
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 241
    .local v4, "n":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_2

    .line 242
    invoke-interface {v2, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 244
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 245
    .local v1, "childs":Lorg/w3c/dom/NodeList;
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-lez v8, :cond_1

    .line 246
    invoke-direct {p0, p1, v0}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->getFullTagSet(Ljava/util/Set;Lorg/w3c/dom/Node;)V

    .line 241
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 249
    :cond_1
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v7

    .line 250
    .local v7, "value":Ljava/lang/String;
    invoke-direct {p0, p2}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->getFullNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    .line 251
    .local v5, "parentTagPath":Ljava/lang/String;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 253
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 254
    .local v6, "trimValue":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 255
    invoke-interface {p1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 265
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v1    # "childs":Lorg/w3c/dom/NodeList;
    .end local v3    # "i":I
    .end local v4    # "n":I
    .end local v5    # "parentTagPath":Ljava/lang/String;
    .end local v6    # "trimValue":Ljava/lang/String;
    .end local v7    # "value":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private makeTagPath(Ljava/lang/String;Z)Lorg/w3c/dom/Node;
    .locals 5
    .param p1, "tagPath"    # Ljava/lang/String;
    .param p2, "bAlwayMakeLastNode"    # Z

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->element:Lorg/w3c/dom/Element;

    .line 146
    .local v0, "node":Lorg/w3c/dom/Node;
    const/4 v1, 0x0

    .line 148
    .local v1, "prevNode":Lorg/w3c/dom/Node;
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v4, "."

    invoke-direct {v3, p1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    .local v3, "tokenizer":Ljava/util/StringTokenizer;
    :cond_0
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 152
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 154
    .local v2, "token":Ljava/lang/String;
    move-object v1, v0

    .line 155
    invoke-direct {p0, v0, v2}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->searchTag(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v0, :cond_0

    .line 159
    :cond_1
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->doc:Lorg/w3c/dom/Document;

    invoke-interface {v4, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 160
    invoke-interface {v1, v0}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_0

    .line 164
    .end local v2    # "token":Ljava/lang/String;
    :cond_2
    return-object v0
.end method

.method private searchTag(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 2
    .param p1, "parent"    # Lorg/w3c/dom/Node;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 201
    invoke-direct {p0, p1, p2}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->searchTags(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 202
    .local v0, "nodeList":Ljava/util/List;, "Ljava/util/List<Lorg/w3c/dom/Node;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 203
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Node;

    .line 205
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private searchTags(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "parent"    # Lorg/w3c/dom/Node;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Node;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/w3c/dom/Node;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 183
    .local v1, "children":Lorg/w3c/dom/NodeList;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 185
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lorg/w3c/dom/Node;>;"
    if-eqz v1, :cond_1

    .line 186
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    .line 188
    .local v3, "n":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 189
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 191
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 192
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 197
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v2    # "i":I
    .end local v3    # "n":I
    :cond_1
    return-object v4
.end method


# virtual methods
.method public addAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "atrrName"    # Ljava/lang/String;
    .param p2, "atrrValue"    # Ljava/lang/String;

    .prologue
    .line 114
    const-string v1, "CscXMLElement"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " addAttribute("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->doc:Lorg/w3c/dom/Document;

    invoke-interface {v1, p1}, Lorg/w3c/dom/Document;->createAttribute(Ljava/lang/String;)Lorg/w3c/dom/Attr;

    move-result-object v0

    .line 117
    .local v0, "attr":Lorg/w3c/dom/Attr;
    if-eqz v0, :cond_0

    .line 118
    invoke-interface {v0, p2}, Lorg/w3c/dom/Attr;->setValue(Ljava/lang/String;)V

    .line 119
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->element:Lorg/w3c/dom/Element;

    invoke-interface {v1, v0}, Lorg/w3c/dom/Element;->setAttributeNode(Lorg/w3c/dom/Attr;)Lorg/w3c/dom/Attr;

    .line 121
    :cond_0
    return-void
.end method

.method public addElement(Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscXMLElement;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "cscElement"    # Lcom/samsung/sec/android/application/csc/CscXMLElement;

    .prologue
    .line 124
    const-string v1, "CscXMLElement"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addElement("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", CscXMLElement)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->element:Lorg/w3c/dom/Element;

    .line 127
    .local v0, "node":Lorg/w3c/dom/Node;
    if-eqz p1, :cond_0

    .line 128
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->makeTagPath(Ljava/lang/String;Z)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 132
    :cond_0
    if-eqz p2, :cond_1

    .line 134
    invoke-virtual {p2}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->getElement()Lorg/w3c/dom/Element;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_1
    const-string v1, "CscXMLElement"

    const-string v2, "addValue(): cscElement is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 31
    const-string v2, "CscXMLElement"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addValue("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->element:Lorg/w3c/dom/Element;

    .line 35
    .local v1, "node":Lorg/w3c/dom/Node;
    if-eqz p1, :cond_0

    .line 36
    const/4 v2, 0x1

    invoke-direct {p0, p1, v2}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->makeTagPath(Ljava/lang/String;Z)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 40
    :cond_0
    if-eqz p2, :cond_2

    .line 42
    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->doc:Lorg/w3c/dom/Document;

    invoke-interface {v2, p2}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    .line 43
    .local v0, "newTextNode":Lorg/w3c/dom/Node;
    if-eqz v0, :cond_1

    .line 44
    invoke-interface {v1, v0}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 48
    .end local v0    # "newTextNode":Lorg/w3c/dom/Node;
    :cond_1
    :goto_0
    return-void

    .line 46
    :cond_2
    const-string v2, "CscXMLElement"

    const-string v3, "addValue(): value is null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "atrrName"    # Ljava/lang/String;
    .param p4, "atrrValue"    # Ljava/lang/String;

    .prologue
    .line 51
    const-string v4, "."

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 52
    .local v0, "lastTokenIndex":I
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 53
    .local v2, "tagName":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 55
    .local v3, "tagPath":Ljava/lang/String;
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscXMLElement;

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->doc:Lorg/w3c/dom/Document;

    invoke-interface {v4, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->doc:Lorg/w3c/dom/Document;

    invoke-direct {v1, v4, v5}, Lcom/samsung/sec/android/application/csc/CscXMLElement;-><init>(Lorg/w3c/dom/Element;Lorg/w3c/dom/Document;)V

    .line 57
    .local v1, "newElement":Lcom/samsung/sec/android/application/csc/CscXMLElement;
    if-eqz v1, :cond_0

    .line 58
    const/4 v4, 0x0

    invoke-virtual {v1, v4, p2}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-virtual {v1, p3, p4}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0, v3, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->addElement(Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscXMLElement;)V

    .line 62
    :cond_0
    return-void
.end method

.method public getElement()Lorg/w3c/dom/Element;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->element:Lorg/w3c/dom/Element;

    return-object v0
.end method

.method getFullTagSet()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 269
    .local v0, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->element:Lorg/w3c/dom/Element;

    invoke-direct {p0, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->getFullTagSet(Ljava/util/Set;Lorg/w3c/dom/Node;)V

    .line 270
    return-object v0
.end method

.method public getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->searchTag(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 102
    .local v1, "node":Lorg/w3c/dom/Node;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 104
    .local v2, "stringValue":Ljava/lang/StringBuffer;
    if-eqz v1, :cond_0

    .line 105
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 106
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    .end local v0    # "idx":I
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public searchTag(Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->element:Lorg/w3c/dom/Element;

    .line 169
    .local v0, "node":Lorg/w3c/dom/Node;
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, "."

    invoke-direct {v2, p1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    .local v2, "tokenizer":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 173
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 174
    .local v1, "token":Ljava/lang/String;
    invoke-direct {p0, v0, v1}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->searchTag(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 175
    goto :goto_0

    .line 177
    .end local v1    # "token":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public setValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 65
    const-string v4, "CscXMLElement"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setValue("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->element:Lorg/w3c/dom/Element;

    .line 69
    .local v3, "node":Lorg/w3c/dom/Node;
    if-eqz p1, :cond_0

    .line 70
    invoke-direct {p0, p1, v7}, Lcom/samsung/sec/android/application/csc/CscXMLElement;->makeTagPath(Ljava/lang/String;Z)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 74
    :cond_0
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 76
    .local v0, "children":Lorg/w3c/dom/NodeList;
    if-eqz v0, :cond_2

    .line 77
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 78
    .local v1, "n":I
    const/4 v2, 0x0

    .line 79
    .local v2, "newTextNode":Lorg/w3c/dom/Node;
    if-eqz p2, :cond_1

    .line 80
    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscXMLElement;->doc:Lorg/w3c/dom/Document;

    invoke-interface {v4, p2}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v2

    .line 83
    :cond_1
    if-lez v1, :cond_4

    .line 84
    if-eqz v2, :cond_3

    .line 85
    invoke-interface {v0, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lorg/w3c/dom/Node;->replaceChild(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 98
    .end local v1    # "n":I
    .end local v2    # "newTextNode":Lorg/w3c/dom/Node;
    :cond_2
    :goto_0
    return-void

    .line 87
    .restart local v1    # "n":I
    .restart local v2    # "newTextNode":Lorg/w3c/dom/Node;
    :cond_3
    const-string v4, "CscXMLElement"

    const-string v5, "setValue(): value is null"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-interface {v0, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/w3c/dom/Node;->removeChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_0

    .line 91
    :cond_4
    if-eqz v2, :cond_5

    .line 92
    invoke-interface {v3, v2}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_0

    .line 94
    :cond_5
    const-string v4, "CscXMLElement"

    const-string v5, "setValue(): value is null"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
