.class public Lcom/samsung/sec/android/application/csc/CscNetwork;
.super Lcom/samsung/sec/android/application/csc/CscTgBase;
.source "CscNetwork.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sec/android/application/csc/CscNetwork$1;,
        Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;
    }
.end annotation


# instance fields
.field private final DEFAULT_CSC_FILE:Ljava/lang/String;

.field private final NOERROR:Ljava/lang/String;

.field private final OTHERS_CSC_FILE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

.field private mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mOtherSosInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

.field private mParser:Lcom/samsung/sec/android/application/csc/CscParser;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mResolver:Landroid/content/ContentResolver;

.field private sPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 104
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscTgBase;-><init>()V

    .line 34
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->DEFAULT_CSC_FILE:Ljava/lang/String;

    .line 38
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->OTHERS_CSC_FILE:Ljava/lang/String;

    .line 84
    const-string v1, "NOERROR"

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->NOERROR:Ljava/lang/String;

    .line 92
    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 94
    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 98
    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 100
    iput-object v2, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->sPrefs:Landroid/content/SharedPreferences;

    .line 105
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mResolver:Landroid/content/ContentResolver;

    .line 108
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mContext:Landroid/content/Context;

    .line 109
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-nez v1, :cond_0

    .line 111
    :try_start_0
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mPhone:Lcom/android/internal/telephony/Phone;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v1, "CscNetwork"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PhoneFactory.getDefaultPhone() has failed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private compareSOSNumber()Ljava/lang/String;
    .locals 9

    .prologue
    .line 314
    const-string v0, "NOERROR"

    .line 316
    .local v0, "answer":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscNetwork;->loadSOSNumber()Ljava/lang/String;

    move-result-object v5

    .line 317
    .local v5, "src":Ljava/lang/String;
    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 318
    const-string v6, "CscNetwork"

    const-string v7, "There is NO SOS number to compare"

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 345
    .end local v0    # "answer":Ljava/lang/String;
    .local v1, "answer":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 322
    .end local v1    # "answer":Ljava/lang/String;
    .restart local v0    # "answer":Ljava/lang/String;
    :cond_0
    const/4 v6, 0x1

    new-array v4, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "loadEmergencyCallNumberSpec"

    aput-object v7, v4, v6

    .line 325
    .local v4, "param":[Ljava/lang/String;
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    .line 326
    .local v3, "msg":Landroid/os/Message;
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v6, :cond_2

    .line 327
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v6, v4, v3}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    .line 332
    iget-object v2, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 334
    .local v2, "dst":Ljava/lang/String;
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 335
    const-string v6, "CscNetwork"

    const-string v7, "compareSOSNumber() failed"

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    const-string v6, "CscNetwork"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Customer spec]\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const-string v6, "CscNetwork"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Stored in Phone class]\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    const-string v6, "CscNetworkcompareSOSNumber() failed"

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 339
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscNetwork[Customer spec]\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 340
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CscNetwork[Stored in Phone class]\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 342
    const-string v0, "Settings.Main.Network.SOSNumber"

    :cond_1
    move-object v1, v0

    .line 345
    .end local v0    # "answer":Ljava/lang/String;
    .restart local v1    # "answer":Ljava/lang/String;
    goto/16 :goto_0

    .line 329
    .end local v1    # "answer":Ljava/lang/String;
    .end local v2    # "dst":Ljava/lang/String;
    .restart local v0    # "answer":Ljava/lang/String;
    :cond_2
    const-string v6, "CscNetwork"

    const-string v7, "mPhone is null"

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 330
    .end local v0    # "answer":Ljava/lang/String;
    .restart local v1    # "answer":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method private loadNetworkInfo()I
    .locals 9

    .prologue
    const/4 v6, -0x1

    .line 119
    const/4 v1, 0x0

    .line 120
    .local v1, "networkSize":I
    const/4 v5, 0x0

    .line 124
    .local v5, "voiceMailSize":I
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v7, :cond_0

    .line 125
    new-instance v7, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 130
    :cond_0
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "GeneralInfo"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 131
    .local v2, "node":Lorg/w3c/dom/Node;
    if-nez v2, :cond_1

    .line 162
    :goto_0
    return v6

    .line 134
    :cond_1
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "NetworkInfo"

    invoke-virtual {v7, v2, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 135
    .local v3, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v3, :cond_2

    .line 136
    const-string v7, "CscNetwork"

    const-string v8, "No network info"

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :cond_2
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 142
    new-array v6, v1, [Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    iput-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    .line 143
    const-string v6, "CscNetwork"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Network numbers : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_5

    .line 147
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    new-instance v7, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;-><init>(Lcom/samsung/sec/android/application/csc/CscNetwork;Lcom/samsung/sec/android/application/csc/CscNetwork$1;)V

    aput-object v7, v6, v0

    .line 150
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "NetworkName"

    invoke-virtual {v6, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 151
    .local v4, "temp":Lorg/w3c/dom/Node;
    if-eqz v4, :cond_3

    .line 152
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    aget-object v6, v6, v0

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v7, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;->mName:Ljava/lang/String;

    .line 156
    :cond_3
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "MCCMNC"

    invoke-virtual {v6, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 157
    if-eqz v4, :cond_4

    .line 158
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    aget-object v6, v6, v0

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v7, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;->mMccMnc:Ljava/lang/String;

    .line 146
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 162
    .end local v4    # "temp":Lorg/w3c/dom/Node;
    :cond_5
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private loadSOSNumber()Ljava/lang/String;
    .locals 10

    .prologue
    .line 250
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v7, ""

    invoke-direct {v0, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 252
    .local v0, "finalSosSpec":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscNetwork;->getOtherSosNumber()Ljava/lang/String;

    move-result-object v4

    .line 255
    .local v4, "otherSosNumber":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v7, :cond_0

    .line 256
    new-instance v7, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v8, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 258
    :cond_0
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "Settings.Main.Network.NbSOSNumber"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 259
    .local v2, "nbSOSNumber":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v8, "Settings.Main.Network.NbSOSNumberNoSIM"

    invoke-virtual {v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 261
    .local v3, "nbSOSNumberNoSIM":Ljava/lang/String;
    const-string v7, "Settings.Main.Network."

    const-string v8, "SOSNumber"

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {p0, v7, v8, v9}, Lcom/samsung/sec/android/application/csc/CscNetwork;->getSOSnumber(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v7

    const-string v8, " "

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 264
    .local v5, "strSOSNumber":Ljava/lang/String;
    const-string v7, "Settings.Main.Network."

    const-string v8, "SOSNumberNoSIM"

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {p0, v7, v8, v9}, Lcom/samsung/sec/android/application/csc/CscNetwork;->getSOSnumber(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v7

    const-string v8, " "

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 271
    .local v6, "strSOSNumberNoSIM":Ljava/lang/String;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_1

    .line 272
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 273
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    array-length v7, v7

    if-ge v1, v7, :cond_3

    .line 274
    if-lez v1, :cond_2

    .line 275
    const-string v7, "\n"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 277
    :cond_2
    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mNwkInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;->mMccMnc:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 273
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 281
    :cond_3
    const-string v7, "CscNetwork"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "loadSOSNumber: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method private updateSOSNumber()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 287
    const/4 v0, 0x1

    .line 288
    .local v0, "answer":Z
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscNetwork;->loadSOSNumber()Ljava/lang/String;

    move-result-object v4

    .line 289
    .local v4, "sosNumber":Ljava/lang/String;
    const-string v7, ""

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 290
    const-string v6, "CscNetwork"

    const-string v7, "There is NO SOS number to update"

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :goto_0
    return v5

    .line 294
    :cond_0
    const/4 v7, 0x2

    new-array v3, v7, [Ljava/lang/String;

    const-string v7, "setEmergencyNumbers"

    aput-object v7, v3, v6

    aput-object v4, v3, v5

    .line 297
    .local v3, "param":[Ljava/lang/String;
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 299
    .local v2, "msg":Landroid/os/Message;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v5, :cond_1

    .line 300
    iget-object v5, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v6, 0x0

    invoke-interface {v5, v3, v6}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    :goto_1
    move v5, v0

    .line 310
    goto :goto_0

    .line 302
    :cond_1
    const-string v5, "CscNetwork"

    const-string v7, "mPhone is null"

    invoke-static {v5, v7}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v5, v6

    .line 303
    goto :goto_0

    .line 305
    :catch_0
    move-exception v1

    .line 306
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "CscNetwork"

    const-string v6, "updateSOSNumber() failed. (Exception occurs)"

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public compare()Ljava/lang/String;
    .locals 3

    .prologue
    .line 447
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 448
    const-string v0, "NOERROR"

    .line 450
    .local v0, "answer":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscNetwork;->compareXtraSettings()Ljava/lang/String;

    move-result-object v0

    .line 455
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscNetwork;->loadNetworkInfo()I

    move-result v1

    if-eqz v1, :cond_0

    .line 456
    const-string v1, "CscNetwork"

    const-string v2, "error during loading network info"

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const-string v1, "CscNetworkerror during loading network info"

    invoke-static {v1}, Lcom/samsung/sec/android/application/csc/CscCompareResetverifyLog;->addFailLogLine(Ljava/lang/String;)V

    .line 458
    const-string v1, "GeneralInfoNetworkInfo"

    .line 464
    :goto_0
    return-object v1

    .line 461
    :cond_0
    const-string v1, "NOERROR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 462
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscNetwork;->compareSOSNumber()Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v1, v0

    .line 464
    goto :goto_0
.end method

.method public compareXtraSettings()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 365
    const-string v0, "NOERROR"

    .line 369
    .local v0, "answer":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v1, :cond_0

    .line 370
    new-instance v1, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 372
    :cond_0
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v2, "Network.gps_xtra"

    invoke-virtual {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 373
    const-string v1, "CscNetwork"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XTRA setting : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v4, "Network.gps_xtra"

    invoke-virtual {v3, v4}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v2, "Network.gps_xtra"

    invoke-virtual {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "on"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 375
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "xtra_enabled"

    invoke-static {v1, v2, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 376
    const-string v0, "NOERROR"

    .line 387
    :cond_1
    :goto_0
    return-object v0

    .line 378
    :cond_2
    const-string v0, "Network.gps_xtra"

    goto :goto_0

    .line 379
    :cond_3
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v2, "Network.gps_xtra"

    invoke-virtual {v1, v2}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "off"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 380
    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "xtra_enabled"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_4

    .line 381
    const-string v0, "NOERROR"

    goto :goto_0

    .line 383
    :cond_4
    const-string v0, "Network.gps_xtra"

    goto :goto_0
.end method

.method public encode(Lcom/samsung/sec/android/application/csc/CscXMLEncoder;)V
    .locals 0
    .param p1, "cscXMLEncoder"    # Lcom/samsung/sec/android/application/csc/CscXMLEncoder;

    .prologue
    .line 473
    return-void
.end method

.method public getOtherSosNumber()Ljava/lang/String;
    .locals 10

    .prologue
    .line 197
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v6, :cond_0

    .line 198
    new-instance v6, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 200
    :cond_0
    new-instance v3, Ljava/lang/StringBuffer;

    const-string v6, ""

    invoke-direct {v3, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 202
    .local v3, "otherSosResult":Ljava/lang/StringBuffer;
    const/4 v4, 0x0

    .line 209
    .local v4, "sosEntrySize":I
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "SOS"

    invoke-virtual {v6, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 210
    .local v1, "node":Lorg/w3c/dom/Node;
    if-nez v1, :cond_1

    .line 211
    const-string v6, ""

    .line 246
    :goto_0
    return-object v6

    .line 213
    :cond_1
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v7, "SOSEntry"

    invoke-virtual {v6, v1, v7}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 214
    .local v2, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v2, :cond_2

    .line 215
    const-string v6, "CscNetwork"

    const-string v7, "No SOS entry"

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string v6, ""

    goto :goto_0

    .line 220
    :cond_2
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 221
    new-array v6, v4, [Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    iput-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherSosInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    .line 222
    const-string v6, "CscNetwork"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SOSEntry numbers : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v4, :cond_5

    .line 226
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherSosInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    new-instance v7, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;-><init>(Lcom/samsung/sec/android/application/csc/CscNetwork;Lcom/samsung/sec/android/application/csc/CscNetwork$1;)V

    aput-object v7, v6, v0

    .line 229
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "MCCMNC"

    invoke-virtual {v6, v7, v8}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 230
    .local v5, "temp":Lorg/w3c/dom/Node;
    if-eqz v5, :cond_4

    .line 231
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherSosInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    aget-object v6, v6, v0

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {v7, v5}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;->mMccMnc:Ljava/lang/String;

    .line 232
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherSosInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    aget-object v6, v6, v0

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "SOSNumber"

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {p0, v7, v8, v9}, Lcom/samsung/sec/android/application/csc/CscNetwork;->getSOSnumber(Lorg/w3c/dom/Node;Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v7

    const-string v8, " "

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;->mSosNumber:Ljava/lang/String;

    .line 234
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherSosInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    aget-object v6, v6, v0

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    const-string v8, "SOSNumberNoSIM"

    iget-object v9, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    invoke-virtual {p0, v7, v8, v9}, Lcom/samsung/sec/android/application/csc/CscNetwork;->getSOSnumber(Lorg/w3c/dom/Node;Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v7

    const-string v8, " "

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;->mNoSimSosNumber:Ljava/lang/String;

    .line 237
    if-lez v0, :cond_3

    .line 238
    const-string v6, "\n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 240
    :cond_3
    iget-object v6, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherSosInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    aget-object v6, v6, v0

    iget-object v6, v6, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;->mMccMnc:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherSosInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;->mSosNumber:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherSosInfo:[Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/samsung/sec/android/application/csc/CscNetwork$NwkInfo;->mNoSimSosNumber:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 225
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 246
    .end local v5    # "temp":Lorg/w3c/dom/Node;
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0
.end method

.method public getSOSnumber(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "cscParser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 166
    invoke-virtual {p3, p1}, Lcom/samsung/sec/android/application/csc/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 168
    .local v0, "networkNode":Lorg/w3c/dom/Node;
    if-nez v0, :cond_0

    .line 169
    const-string v1, ""

    .line 171
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, v0, p2, p3}, Lcom/samsung/sec/android/application/csc/CscNetwork;->getSOSnumber(Lorg/w3c/dom/Node;Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getSOSnumber(Lorg/w3c/dom/Node;Ljava/lang/String;Lcom/samsung/sec/android/application/csc/CscParser;)Ljava/lang/String;
    .locals 8
    .param p1, "parentNode"    # Lorg/w3c/dom/Node;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "cscParser"    # Lcom/samsung/sec/android/application/csc/CscParser;

    .prologue
    .line 175
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v5, ""

    invoke-direct {v0, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 177
    .local v0, "SOSnumber":Ljava/lang/StringBuffer;
    invoke-virtual {p3, p1, p2}, Lcom/samsung/sec/android/application/csc/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 179
    .local v4, "numberNodeList":Lorg/w3c/dom/NodeList;
    if-nez v4, :cond_0

    .line 180
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    .line 192
    :goto_0
    return-object v5

    .line 182
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 183
    invoke-interface {v4, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 184
    .local v3, "numberNode":Lorg/w3c/dom/Node;
    invoke-virtual {p3, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v1

    .line 185
    .local v1, "currentNumber":Ljava/lang/String;
    const-string v5, "CscNetwork"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " > number ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    if-lez v2, :cond_1

    .line 188
    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 189
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 182
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 192
    .end local v1    # "currentNumber":Ljava/lang/String;
    .end local v3    # "numberNode":Lorg/w3c/dom/Node;
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public update()V
    .locals 2

    .prologue
    .line 423
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->DEFAULT_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 425
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscNetwork;->loadNetworkInfo()I

    move-result v0

    if-eqz v0, :cond_1

    .line 426
    const-string v0, "CscNetwork"

    const-string v1, "error during loading network info"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 431
    :cond_1
    invoke-direct {p0}, Lcom/samsung/sec/android/application/csc/CscNetwork;->updateSOSNumber()Z

    move-result v0

    if-nez v0, :cond_2

    .line 432
    const-string v0, "CscNetwork"

    const-string v1, "SOS number setting : failed"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/sec/android/application/csc/CscNetwork;->updateXtraSettings()Z

    move-result v0

    if-nez v0, :cond_0

    .line 436
    const-string v0, "CscNetwork"

    const-string v1, "XTRA setting : failed"

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateXtraSettings()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 351
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    if-nez v0, :cond_0

    .line 352
    new-instance v0, Lcom/samsung/sec/android/application/csc/CscParser;

    iget-object v1, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->OTHERS_CSC_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Network.gps_xtra"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 355
    const-string v0, "CscNetwork"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XTRA setting : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v3, "Network.gps_xtra"

    invoke-virtual {v2, v3}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/sec/android/application/csc/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Network.gps_xtra"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 357
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "xtra_enabled"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 361
    :cond_1
    :goto_0
    return v4

    .line 358
    :cond_2
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mOtherParser:Lcom/samsung/sec/android/application/csc/CscParser;

    const-string v1, "Network.gps_xtra"

    invoke-virtual {v0, v1}, Lcom/samsung/sec/android/application/csc/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 359
    iget-object v0, p0, Lcom/samsung/sec/android/application/csc/CscNetwork;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "xtra_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method
