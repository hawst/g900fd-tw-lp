.class public Lorg/tartarus/snowball/ext/GermanStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "GermanStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final g_s_ending:[C

.field private static final g_st_ending:[C

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_p2:I

.field private I_x:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/GermanStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    .line 19
    const/4 v0, 0x6

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "U"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "Y"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e4"

    const/4 v2, 0x0

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f6"

    const/4 v2, 0x0

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00fc"

    const/4 v2, 0x0

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/GermanStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 28
    const/4 v0, 0x7

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 29
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 30
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "em"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 31
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ern"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "es"

    const/4 v2, 0x5

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 28
    sput-object v6, Lorg/tartarus/snowball/ext/GermanStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 38
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "st"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "est"

    const/4 v2, 0x2

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 38
    sput-object v6, Lorg/tartarus/snowball/ext/GermanStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 45
    const/4 v0, 0x2

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ig"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lich"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 45
    sput-object v6, Lorg/tartarus/snowball/ext/GermanStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 50
    const/16 v0, 0x8

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "end"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ig"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ung"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lich"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isch"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ik"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "heit"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "keit"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/GermanStemmer;->methodObject:Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 50
    sput-object v6, Lorg/tartarus/snowball/ext/GermanStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 61
    const/16 v0, 0x14

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x8

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x20

    aput-char v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0x8

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/GermanStemmer;->g_v:[C

    .line 63
    const/4 v0, 0x3

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/tartarus/snowball/ext/GermanStemmer;->g_s_ending:[C

    .line 65
    const/4 v0, 0x3

    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lorg/tartarus/snowball/ext/GermanStemmer;->g_st_ending:[C

    return-void

    .line 63
    :array_0
    .array-data 2
        0x75s
        0x1es
        0x5s
    .end array-data

    .line 65
    nop

    :array_1
    .array-data 2
        0x75s
        0x1es
        0x4s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/GermanStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/GermanStemmer;

    .prologue
    .line 72
    iget v0, p1, Lorg/tartarus/snowball/ext/GermanStemmer;->I_x:I

    iput v0, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_x:I

    .line 73
    iget v0, p1, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p2:I

    .line 74
    iget v0, p1, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p1:I

    .line 75
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 76
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 363
    iget v0, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 365
    const/4 v0, 0x0

    .line 367
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 371
    iget v0, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 373
    const/4 v0, 0x0

    .line 375
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_regions()Z
    .locals 7

    .prologue
    const/16 v6, 0xfc

    const/16 v5, 0x61

    const/4 v2, 0x0

    .line 200
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p1:I

    .line 201
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p2:I

    .line 203
    iget v1, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 207
    .local v1, "v_1":I
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    add-int/lit8 v0, v3, 0x3

    .line 208
    .local v0, "c":I
    if-ltz v0, :cond_0

    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    if-le v0, v3, :cond_1

    .line 294
    :cond_0
    :goto_0
    return v2

    .line 212
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 215
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_x:I

    .line 216
    iput v1, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 221
    :goto_1
    sget-object v3, Lorg/tartarus/snowball/ext/GermanStemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/GermanStemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_3

    .line 227
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    if-ge v3, v4, :cond_0

    .line 231
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto :goto_1

    .line 247
    :cond_2
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 237
    :cond_3
    sget-object v3, Lorg/tartarus/snowball/ext/GermanStemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/GermanStemmer;->out_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_4

    .line 243
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    if-lt v3, v4, :cond_2

    goto :goto_0

    .line 250
    :cond_4
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p1:I

    .line 254
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p1:I

    iget v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_x:I

    if-lt v3, v4, :cond_5

    .line 264
    :goto_2
    sget-object v3, Lorg/tartarus/snowball/ext/GermanStemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/GermanStemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_7

    .line 270
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    if-ge v3, v4, :cond_0

    .line 274
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto :goto_2

    .line 258
    :cond_5
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_x:I

    iput v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p1:I

    goto :goto_2

    .line 290
    :cond_6
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 280
    :cond_7
    sget-object v3, Lorg/tartarus/snowball/ext/GermanStemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/GermanStemmer;->out_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_8

    .line 286
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    if-lt v3, v4, :cond_6

    goto :goto_0

    .line 293
    :cond_8
    iget v2, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->I_p2:I

    .line 294
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private r_postlude()Z
    .locals 4

    .prologue
    .line 303
    :goto_0
    iget v1, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 307
    .local v1, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->bra:I

    .line 309
    sget-object v2, Lorg/tartarus/snowball/ext/GermanStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x6

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/GermanStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 310
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 356
    :cond_0
    :pswitch_0
    iput v1, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 359
    const/4 v2, 0x1

    return v2

    .line 315
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->ket:I

    .line 316
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 322
    :pswitch_1
    const-string/jumbo v2, "y"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 327
    :pswitch_2
    const-string v2, "u"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 332
    :pswitch_3
    const-string v2, "a"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 337
    :pswitch_4
    const-string v2, "o"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 342
    :pswitch_5
    const-string v2, "u"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 347
    :pswitch_6
    iget v2, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 351
    iget v2, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto :goto_0

    .line 316
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private r_prelude()Z
    .locals 11

    .prologue
    const/16 v10, 0xfc

    const/16 v9, 0x61

    const/4 v8, 0x1

    .line 87
    iget v0, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 91
    .local v0, "v_1":I
    :goto_0
    iget v1, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 96
    .local v1, "v_2":I
    iget v2, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 100
    .local v2, "v_3":I
    iget v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->bra:I

    .line 102
    const-string/jumbo v6, "\u00df"

    invoke-virtual {p0, v8, v6}, Lorg/tartarus/snowball/ext/GermanStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 112
    iput v2, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 114
    iget v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    if-lt v6, v7, :cond_2

    .line 122
    iput v1, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 125
    iput v0, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 129
    :goto_1
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 134
    .local v3, "v_4":I
    :goto_2
    iget v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 137
    .local v4, "v_5":I
    sget-object v6, Lorg/tartarus/snowball/ext/GermanStemmer;->g_v:[C

    invoke-virtual {p0, v6, v9, v10}, Lorg/tartarus/snowball/ext/GermanStemmer;->in_grouping([CII)Z

    move-result v6

    if-nez v6, :cond_3

    .line 182
    :cond_0
    iput v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 183
    iget v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    if-lt v6, v7, :cond_6

    .line 191
    iput v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 194
    return v8

    .line 107
    .end local v3    # "v_4":I
    .end local v4    # "v_5":I
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->ket:I

    .line 109
    const-string v6, "ss"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 118
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto :goto_0

    .line 142
    .restart local v3    # "v_4":I
    .restart local v4    # "v_5":I
    :cond_3
    iget v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->bra:I

    .line 145
    iget v5, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 149
    .local v5, "v_6":I
    const-string v6, "u"

    invoke-virtual {p0, v8, v6}, Lorg/tartarus/snowball/ext/GermanStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 163
    :cond_4
    iput v5, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 166
    const-string/jumbo v6, "y"

    invoke-virtual {p0, v8, v6}, Lorg/tartarus/snowball/ext/GermanStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 171
    iget v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->ket:I

    .line 172
    sget-object v6, Lorg/tartarus/snowball/ext/GermanStemmer;->g_v:[C

    invoke-virtual {p0, v6, v9, v10}, Lorg/tartarus/snowball/ext/GermanStemmer;->in_grouping([CII)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 177
    const-string v6, "Y"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 179
    :goto_3
    iput v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto :goto_1

    .line 154
    :cond_5
    iget v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->ket:I

    .line 155
    sget-object v6, Lorg/tartarus/snowball/ext/GermanStemmer;->g_v:[C

    invoke-virtual {p0, v6, v9, v10}, Lorg/tartarus/snowball/ext/GermanStemmer;->in_grouping([CII)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 160
    const-string v6, "U"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 187
    .end local v5    # "v_6":I
    :cond_6
    iget v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto :goto_2
.end method

.method private r_standard_suffix()Z
    .locals 14

    .prologue
    .line 391
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    sub-int v2, v11, v12

    .line 395
    .local v2, "v_1":I
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->ket:I

    .line 397
    sget-object v11, Lorg/tartarus/snowball/ext/GermanStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v12, 0x7

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/GermanStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 398
    .local v0, "among_var":I
    if-nez v0, :cond_3

    .line 428
    :cond_0
    :goto_0
    :pswitch_0
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v2

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 430
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    sub-int v3, v11, v12

    .line 434
    .local v3, "v_2":I
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->ket:I

    .line 436
    sget-object v11, Lorg/tartarus/snowball/ext/GermanStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/4 v12, 0x4

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/GermanStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 437
    if-nez v0, :cond_4

    .line 476
    :cond_1
    :goto_1
    :pswitch_1
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v3

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 478
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    sub-int v4, v11, v12

    .line 482
    .local v4, "v_3":I
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->ket:I

    .line 484
    sget-object v11, Lorg/tartarus/snowball/ext/GermanStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/16 v12, 0x8

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/GermanStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 485
    if-nez v0, :cond_5

    .line 639
    :cond_2
    :goto_2
    :pswitch_2
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 640
    const/4 v11, 0x1

    return v11

    .line 403
    .end local v3    # "v_2":I
    .end local v4    # "v_3":I
    :cond_3
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->bra:I

    .line 405
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->r_R1()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 409
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 415
    :pswitch_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_del()V

    goto :goto_0

    .line 419
    :pswitch_4
    sget-object v11, Lorg/tartarus/snowball/ext/GermanStemmer;->g_s_ending:[C

    const/16 v12, 0x62

    const/16 v13, 0x74

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/GermanStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 424
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_del()V

    goto :goto_0

    .line 442
    .restart local v3    # "v_2":I
    :cond_4
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->bra:I

    .line 444
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->r_R1()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 448
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 454
    :pswitch_5
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_del()V

    goto :goto_1

    .line 458
    :pswitch_6
    sget-object v11, Lorg/tartarus/snowball/ext/GermanStemmer;->g_st_ending:[C

    const/16 v12, 0x62

    const/16 v13, 0x74

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/GermanStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 464
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    add-int/lit8 v1, v11, -0x3

    .line 465
    .local v1, "c":I
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit_backward:I

    if-gt v11, v1, :cond_1

    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    if-gt v1, v11, :cond_1

    .line 469
    iput v1, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 472
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_del()V

    goto :goto_1

    .line 490
    .end local v1    # "c":I
    .restart local v4    # "v_3":I
    :cond_5
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->bra:I

    .line 492
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->r_R2()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 496
    packed-switch v0, :pswitch_data_2

    goto :goto_2

    .line 502
    :pswitch_7
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_del()V

    .line 504
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    sub-int v5, v11, v12

    .line 508
    .local v5, "v_4":I
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->ket:I

    .line 510
    const/4 v11, 0x2

    const-string v12, "ig"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/GermanStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 512
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto/16 :goto_2

    .line 516
    :cond_6
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->bra:I

    .line 519
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    sub-int v6, v11, v12

    .line 522
    .local v6, "v_5":I
    const/4 v11, 0x1

    const-string v12, "e"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/GermanStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_7

    .line 529
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v6

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 532
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->r_R2()Z

    move-result v11

    if-nez v11, :cond_8

    .line 534
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto/16 :goto_2

    .line 526
    :cond_7
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto/16 :goto_2

    .line 538
    :cond_8
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_del()V

    goto/16 :goto_2

    .line 545
    .end local v5    # "v_4":I
    .end local v6    # "v_5":I
    :pswitch_8
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    sub-int v7, v11, v12

    .line 548
    .local v7, "v_6":I
    const/4 v11, 0x1

    const-string v12, "e"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/GermanStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 554
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v7

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 557
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_del()V

    goto/16 :goto_2

    .line 562
    .end local v7    # "v_6":I
    :pswitch_9
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_del()V

    .line 564
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    sub-int v8, v11, v12

    .line 568
    .local v8, "v_7":I
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->ket:I

    .line 571
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    sub-int v9, v11, v12

    .line 574
    .local v9, "v_8":I
    const/4 v11, 0x2

    const-string v12, "er"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/GermanStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 580
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v9

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 582
    const/4 v11, 0x2

    const-string v12, "en"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/GermanStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 584
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto/16 :goto_2

    .line 589
    :cond_9
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->bra:I

    .line 591
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->r_R1()Z

    move-result v11

    if-nez v11, :cond_a

    .line 593
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto/16 :goto_2

    .line 597
    :cond_a
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_del()V

    goto/16 :goto_2

    .line 603
    .end local v8    # "v_7":I
    .end local v9    # "v_8":I
    :pswitch_a
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_del()V

    .line 605
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    sub-int v10, v11, v12

    .line 609
    .local v10, "v_9":I
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->ket:I

    .line 611
    sget-object v11, Lorg/tartarus/snowball/ext/GermanStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/4 v12, 0x2

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/GermanStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 612
    if-nez v0, :cond_b

    .line 614
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto/16 :goto_2

    .line 618
    :cond_b
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->bra:I

    .line 620
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->r_R2()Z

    move-result v11

    if-nez v11, :cond_c

    .line 622
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto/16 :goto_2

    .line 625
    :cond_c
    packed-switch v0, :pswitch_data_3

    goto/16 :goto_2

    .line 627
    :pswitch_b
    iget v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    goto/16 :goto_2

    .line 632
    :pswitch_c
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->slice_del()V

    goto/16 :goto_2

    .line 409
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 448
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 496
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 625
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 697
    instance-of v0, p1, Lorg/tartarus/snowball/ext/GermanStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 702
    const-class v0, Lorg/tartarus/snowball/ext/GermanStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 6

    .prologue
    .line 651
    iget v0, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 654
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->r_prelude()Z

    move-result v4

    if-nez v4, :cond_0

    .line 659
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 661
    iget v1, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 664
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->r_mark_regions()Z

    move-result v4

    if-nez v4, :cond_1

    .line 669
    :cond_1
    iput v1, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 671
    iget v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit_backward:I

    iget v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iput v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 673
    iget v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    sub-int v2, v4, v5

    .line 676
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->r_standard_suffix()Z

    move-result v4

    if-nez v4, :cond_2

    .line 681
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 682
    iget v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->limit_backward:I

    iput v4, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 683
    iget v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 686
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/GermanStemmer;->r_postlude()Z

    move-result v4

    if-nez v4, :cond_3

    .line 691
    :cond_3
    iput v3, p0, Lorg/tartarus/snowball/ext/GermanStemmer;->cursor:I

    .line 692
    const/4 v4, 0x1

    return v4
.end method
