.class public Lorg/tartarus/snowball/ext/FinnishStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "FinnishStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final a_6:[Lorg/tartarus/snowball/Among;

.field private static final a_7:[Lorg/tartarus/snowball/Among;

.field private static final a_8:[Lorg/tartarus/snowball/Among;

.field private static final a_9:[Lorg/tartarus/snowball/Among;

.field private static final g_AEI:[C

.field private static final g_V1:[C

.field private static final g_V2:[C

.field private static final g_particle_end:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private B_ending_removed:Z

.field private I_p1:I

.field private I_p2:I

.field private S_x:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/FinnishStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    .line 19
    const/16 v0, 0xa

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pa"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sti"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kaan"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "han"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kin"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "h\u00e4n"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "k\u00e4\u00e4n"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ko"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "p\u00e4"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 29
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "k\u00f6"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 32
    const/4 v0, 0x6

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lla"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "na"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ssa"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ta"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lta"

    const/4 v2, 0x3

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sta"

    const/4 v2, 0x3

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 32
    sput-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 41
    const/4 v0, 0x6

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ll\u00e4"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 43
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n\u00e4"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 44
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ss\u00e4"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u00e4"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lt\u00e4"

    const/4 v2, 0x3

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "st\u00e4"

    const/4 v2, 0x3

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 41
    sput-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 50
    const/4 v0, 0x2

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lle"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ine"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 50
    sput-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 55
    const/16 v0, 0x9

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nsa"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mme"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nne"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ni"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 60
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "si"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "an"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e4n"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ns\u00e4"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 55
    sput-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 67
    const/4 v0, 0x7

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aa"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 69
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ee"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 70
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ii"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 71
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oo"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uu"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e4\u00e4"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f6\u00f6"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 67
    sput-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 77
    const/16 v0, 0x1e

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 79
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lla"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "na"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ssa"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ta"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lta"

    const/4 v2, 0x4

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sta"

    const/4 v2, 0x4

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 85
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tta"

    const/4 v2, 0x4

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lle"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ine"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 88
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ksi"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 89
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 90
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "han"

    const/16 v2, 0xb

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 91
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "den"

    const/16 v2, 0xb

    const/4 v3, -0x1

    const-string v4, "r_VI"

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 92
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "seen"

    const/16 v2, 0xb

    const/4 v3, -0x1

    const-string v4, "r_LONG"

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 93
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "hen"

    const/16 v2, 0xb

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tten"

    const/16 v2, 0xb

    const/4 v3, -0x1

    const-string v4, "r_VI"

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 95
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "hin"

    const/16 v2, 0xb

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "siin"

    const/16 v2, 0xb

    const/4 v3, -0x1

    const-string v4, "r_VI"

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 97
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "hon"

    const/16 v2, 0xb

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 98
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "h\u00e4n"

    const/16 v2, 0xb

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "h\u00f6n"

    const/16 v2, 0xb

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e4"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 101
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ll\u00e4"

    const/16 v2, 0x16

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n\u00e4"

    const/16 v2, 0x16

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 103
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ss\u00e4"

    const/16 v2, 0x16

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 104
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u00e4"

    const/16 v2, 0x16

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 105
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lt\u00e4"

    const/16 v2, 0x1a

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "st\u00e4"

    const/16 v2, 0x1a

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tt\u00e4"

    const/16 v2, 0x1a

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 77
    sput-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    .line 110
    const/16 v0, 0xe

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 111
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eja"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mma"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imma"

    const/4 v2, 0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mpa"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 115
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "impa"

    const/4 v2, 0x3

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 116
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mmi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "immi"

    const/4 v2, 0x5

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 118
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mpi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 119
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "impi"

    const/4 v2, 0x7

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ej\u00e4"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 121
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mm\u00e4"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 122
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imm\u00e4"

    const/16 v2, 0xa

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 123
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mp\u00e4"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 124
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imp\u00e4"

    const/16 v2, 0xc

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 110
    sput-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    .line 127
    const/4 v0, 0x2

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 128
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 129
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "j"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 127
    sput-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    .line 132
    const/4 v0, 0x2

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 133
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mma"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imma"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->methodObject:Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 132
    sput-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_9:[Lorg/tartarus/snowball/Among;

    .line 137
    const/16 v0, 0x11

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x8

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_AEI:[C

    .line 139
    const/16 v0, 0x13

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x8

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x20

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V1:[C

    .line 141
    const/16 v0, 0x13

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x8

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x20

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V2:[C

    .line 143
    const/16 v0, 0x13

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x61

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x18

    aput-char v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x8

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x20

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_particle_end:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->S_x:Ljava/lang/StringBuilder;

    .line 13
    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/FinnishStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/FinnishStemmer;

    .prologue
    .line 151
    iget-boolean v0, p1, Lorg/tartarus/snowball/ext/FinnishStemmer;->B_ending_removed:Z

    iput-boolean v0, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->B_ending_removed:Z

    .line 152
    iget-object v0, p1, Lorg/tartarus/snowball/ext/FinnishStemmer;->S_x:Ljava/lang/StringBuilder;

    iput-object v0, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->S_x:Ljava/lang/StringBuilder;

    .line 153
    iget v0, p1, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p2:I

    .line 154
    iget v0, p1, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    .line 155
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 156
    return-void
.end method

.method private r_LONG()Z
    .locals 2

    .prologue
    .line 410
    sget-object v0, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lorg/tartarus/snowball/ext/FinnishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 412
    const/4 v0, 0x0

    .line 414
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 242
    iget v0, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 244
    const/4 v0, 0x0

    .line 246
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_VI()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 420
    const-string v2, "i"

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 428
    :cond_0
    :goto_0
    return v0

    .line 424
    :cond_1
    sget-object v2, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V2:[C

    const/16 v3, 0x61

    const/16 v4, 0xf6

    invoke-virtual {p0, v2, v3, v4}, Lorg/tartarus/snowball/ext/FinnishStemmer;->in_grouping_b([CII)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 428
    goto :goto_0
.end method

.method private r_case_ending()Z
    .locals 12

    .prologue
    const/16 v11, 0xf6

    const/16 v10, 0x61

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 440
    iget v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v1, v8, v9

    .line 442
    .local v1, "v_1":I
    iget v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    if-ge v8, v9, :cond_1

    .line 577
    :cond_0
    :goto_0
    :pswitch_0
    return v6

    .line 446
    :cond_1
    iget v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    iput v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 447
    iget v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 448
    .local v2, "v_2":I
    iget v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 449
    iget v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v8, v1

    iput v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 452
    iget v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 454
    sget-object v8, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    const/16 v9, 0x1e

    invoke-virtual {p0, v8, v9}, Lorg/tartarus/snowball/ext/FinnishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 455
    .local v0, "among_var":I
    if-nez v0, :cond_2

    .line 457
    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    goto :goto_0

    .line 461
    :cond_2
    iget v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 462
    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 463
    packed-switch v0, :pswitch_data_0

    .line 574
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    .line 576
    iput-boolean v7, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->B_ending_removed:Z

    move v6, v7

    .line 577
    goto :goto_0

    .line 469
    :pswitch_1
    const-string v8, "a"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    goto :goto_0

    .line 477
    :pswitch_2
    const-string v8, "e"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    goto :goto_0

    .line 485
    :pswitch_3
    const-string v8, "i"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    goto :goto_0

    .line 493
    :pswitch_4
    const-string v8, "o"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    goto :goto_0

    .line 501
    :pswitch_5
    const-string/jumbo v8, "\u00e4"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    goto :goto_0

    .line 509
    :pswitch_6
    const-string/jumbo v8, "\u00f6"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    goto :goto_0

    .line 517
    :pswitch_7
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v3, v6, v8

    .line 521
    .local v3, "v_3":I
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v4, v6, v8

    .line 524
    .local v4, "v_4":I
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v5, v6, v8

    .line 527
    .local v5, "v_5":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->r_LONG()Z

    move-result v6

    if-nez v6, :cond_4

    .line 533
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v6, v5

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 535
    const/4 v6, 0x2

    const-string v8, "ie"

    invoke-virtual {p0, v6, v8}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 537
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    goto :goto_1

    .line 541
    :cond_4
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v6, v4

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 543
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v8, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    if-gt v6, v8, :cond_5

    .line 545
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    goto :goto_1

    .line 548
    :cond_5
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 550
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    goto/16 :goto_1

    .line 555
    .end local v3    # "v_3":I
    .end local v4    # "v_4":I
    .end local v5    # "v_5":I
    :pswitch_8
    sget-object v8, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V1:[C

    invoke-virtual {p0, v8, v10, v11}, Lorg/tartarus/snowball/ext/FinnishStemmer;->in_grouping_b([CII)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 559
    sget-object v8, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V1:[C

    invoke-virtual {p0, v8, v10, v11}, Lorg/tartarus/snowball/ext/FinnishStemmer;->out_grouping_b([CII)Z

    move-result v8

    if-nez v8, :cond_3

    goto/16 :goto_0

    .line 567
    :pswitch_9
    const-string v8, "e"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    goto/16 :goto_0

    .line 463
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private r_i_plural()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 640
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v0, v3, v4

    .line 642
    .local v0, "v_1":I
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    if-ge v3, v4, :cond_0

    .line 664
    :goto_0
    return v2

    .line 646
    :cond_0
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 647
    iget v1, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 648
    .local v1, "v_2":I
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 649
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v3, v0

    iput v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 652
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 654
    sget-object v3, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    const/4 v4, 0x2

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/FinnishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v3

    if-nez v3, :cond_1

    .line 656
    iput v1, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    goto :goto_0

    .line 660
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 661
    iput v1, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 663
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    .line 664
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private r_mark_regions()Z
    .locals 7

    .prologue
    const/16 v6, 0xf6

    const/16 v5, 0x61

    const/4 v2, 0x0

    .line 162
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    .line 163
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p2:I

    .line 167
    :goto_0
    iget v0, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 169
    .local v0, "v_1":I
    sget-object v3, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V1:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/FinnishStemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_1

    .line 176
    iput v0, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 177
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    if-lt v3, v4, :cond_2

    .line 238
    :cond_0
    :goto_1
    return v2

    .line 173
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 187
    :goto_2
    sget-object v3, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V1:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/FinnishStemmer;->out_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_3

    .line 193
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    if-ge v3, v4, :cond_0

    .line 197
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    goto :goto_2

    .line 181
    :cond_2
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    goto :goto_0

    .line 200
    :cond_3
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    .line 204
    :goto_3
    iget v1, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 206
    .local v1, "v_3":I
    sget-object v3, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V1:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/FinnishStemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_4

    .line 213
    iput v1, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 214
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    if-ge v3, v4, :cond_0

    .line 218
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    goto :goto_3

    .line 210
    :cond_4
    iput v1, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 224
    :goto_4
    sget-object v3, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V1:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/FinnishStemmer;->out_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_5

    .line 230
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    if-ge v3, v4, :cond_0

    .line 234
    iget v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    goto :goto_4

    .line 237
    :cond_5
    iget v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p2:I

    .line 238
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private r_other_endings()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 587
    iget v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 589
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p2:I

    if-ge v5, v6, :cond_1

    .line 632
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 593
    :cond_1
    iget v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p2:I

    iput v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 594
    iget v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 595
    .local v2, "v_2":I
    iget v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 596
    iget v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 599
    iget v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 601
    sget-object v5, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    const/16 v6, 0xe

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/FinnishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 602
    .local v0, "among_var":I
    if-nez v0, :cond_2

    .line 604
    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    goto :goto_0

    .line 608
    :cond_2
    iget v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 609
    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 610
    packed-switch v0, :pswitch_data_0

    .line 631
    :goto_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    .line 632
    const/4 v4, 0x1

    goto :goto_0

    .line 617
    :pswitch_1
    iget v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v3, v5, v6

    .line 620
    .local v3, "v_3":I
    const/4 v5, 0x2

    const-string v6, "po"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 626
    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    goto :goto_1

    .line 610
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_particle_etc()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 255
    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 257
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    if-ge v4, v5, :cond_0

    .line 299
    :goto_0
    :pswitch_0
    return v3

    .line 261
    :cond_0
    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 262
    iget v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 263
    .local v2, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 264
    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 267
    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 269
    sget-object v4, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0xa

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/FinnishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 270
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 272
    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    goto :goto_0

    .line 276
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 277
    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 278
    packed-switch v0, :pswitch_data_0

    .line 298
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    .line 299
    const/4 v3, 0x1

    goto :goto_0

    .line 283
    :pswitch_1
    sget-object v4, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_particle_end:[C

    const/16 v5, 0x61

    const/16 v6, 0xf6

    invoke-virtual {p0, v4, v5, v6}, Lorg/tartarus/snowball/ext/FinnishStemmer;->in_grouping_b([CII)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    .line 291
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->r_R2()Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    .line 278
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_possessive()Z
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 309
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 311
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    if-ge v6, v7, :cond_1

    .line 405
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 315
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 316
    iget v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 317
    .local v2, "v_2":I
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 318
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 321
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 323
    sget-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/16 v7, 0x9

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/FinnishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 324
    .local v0, "among_var":I
    if-nez v0, :cond_2

    .line 326
    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    goto :goto_0

    .line 330
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 331
    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 332
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v4, v5

    .line 405
    goto :goto_0

    .line 339
    :pswitch_1
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 342
    .local v3, "v_3":I
    const-string v6, "k"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 348
    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 351
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    goto :goto_1

    .line 356
    .end local v3    # "v_3":I
    :pswitch_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    .line 358
    iget v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 360
    const/4 v6, 0x3

    const-string v7, "kse"

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 365
    iget v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 367
    const-string v4, "ksi"

    invoke-virtual {p0, v4}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 372
    :pswitch_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    goto :goto_1

    .line 377
    :pswitch_4
    sget-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    invoke-virtual {p0, v6, v8}, Lorg/tartarus/snowball/ext/FinnishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v6

    if-eqz v6, :cond_0

    .line 382
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    goto :goto_1

    .line 387
    :pswitch_5
    sget-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    invoke-virtual {p0, v6, v8}, Lorg/tartarus/snowball/ext/FinnishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v6

    if-eqz v6, :cond_0

    .line 392
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    goto :goto_1

    .line 397
    :pswitch_6
    sget-object v6, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/4 v7, 0x2

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/FinnishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v6

    if-eqz v6, :cond_0

    .line 402
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    goto :goto_1

    .line 332
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private r_t_plural()Z
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 677
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v1, v9, v10

    .line 679
    .local v1, "v_1":I
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    if-ge v9, v10, :cond_1

    .line 755
    :cond_0
    :goto_0
    :pswitch_0
    return v7

    .line 683
    :cond_1
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 684
    iget v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 685
    .local v2, "v_2":I
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 686
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v9, v1

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 689
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 691
    const-string v9, "t"

    invoke-virtual {p0, v8, v9}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 693
    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    goto :goto_0

    .line 697
    :cond_2
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 699
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v3, v9, v10

    .line 700
    .local v3, "v_3":I
    sget-object v9, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V1:[C

    const/16 v10, 0x61

    const/16 v11, 0xf6

    invoke-virtual {p0, v9, v10, v11}, Lorg/tartarus/snowball/ext/FinnishStemmer;->in_grouping_b([CII)Z

    move-result v9

    if-nez v9, :cond_3

    .line 702
    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    goto :goto_0

    .line 705
    :cond_3
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v9, v3

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 707
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    .line 708
    iput v2, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 710
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v4, v9, v10

    .line 712
    .local v4, "v_4":I
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p2:I

    if-lt v9, v10, :cond_0

    .line 716
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p2:I

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 717
    iget v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 718
    .local v5, "v_5":I
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 719
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v9, v4

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 722
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 724
    sget-object v9, Lorg/tartarus/snowball/ext/FinnishStemmer;->a_9:[Lorg/tartarus/snowball/Among;

    invoke-virtual {p0, v9, v12}, Lorg/tartarus/snowball/ext/FinnishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 725
    .local v0, "among_var":I
    if-nez v0, :cond_4

    .line 727
    iput v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    goto :goto_0

    .line 731
    :cond_4
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 732
    iput v5, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 733
    packed-switch v0, :pswitch_data_0

    .line 754
    :goto_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    move v7, v8

    .line 755
    goto :goto_0

    .line 740
    :pswitch_1
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v6, v9, v10

    .line 743
    .local v6, "v_6":I
    const-string v9, "po"

    invoke-virtual {p0, v12, v9}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 749
    iget v7, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v7, v6

    iput v7, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    goto :goto_1

    .line 733
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_tidy()Z
    .locals 15

    .prologue
    const/16 v14, 0xf6

    const/16 v13, 0x61

    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 770
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v0, v11, v12

    .line 772
    .local v0, "v_1":I
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    if-ge v11, v12, :cond_1

    .line 924
    :cond_0
    :goto_0
    return v9

    .line 776
    :cond_1
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->I_p1:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 777
    iget v1, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 778
    .local v1, "v_2":I
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 779
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v11, v0

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 782
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v2, v11, v12

    .line 786
    .local v2, "v_3":I
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v3, v11, v12

    .line 788
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->r_LONG()Z

    move-result v11

    if-nez v11, :cond_6

    .line 807
    :cond_2
    :goto_1
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v11, v2

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 809
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v4, v11, v12

    .line 813
    .local v4, "v_5":I
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 814
    sget-object v11, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_AEI:[C

    const/16 v12, 0xe4

    invoke-virtual {p0, v11, v13, v12}, Lorg/tartarus/snowball/ext/FinnishStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_7

    .line 827
    :cond_3
    :goto_2
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 829
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v5, v11, v12

    .line 833
    .local v5, "v_6":I
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 835
    const-string v11, "j"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_8

    .line 862
    :cond_4
    :goto_3
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 864
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v7, v11, v12

    .line 868
    .local v7, "v_8":I
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 870
    const-string v11, "o"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_a

    .line 884
    :cond_5
    :goto_4
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v11, v7

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 885
    iput v1, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    .line 889
    :goto_5
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v8, v11, v12

    .line 891
    .local v8, "v_9":I
    sget-object v11, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V1:[C

    invoke-virtual {p0, v11, v13, v14}, Lorg/tartarus/snowball/ext/FinnishStemmer;->out_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_b

    .line 898
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 899
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    if-le v11, v12, :cond_0

    .line 903
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    goto :goto_5

    .line 792
    .end local v4    # "v_5":I
    .end local v5    # "v_6":I
    .end local v7    # "v_8":I
    .end local v8    # "v_9":I
    :cond_6
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v11, v3

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 795
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 797
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    if-le v11, v12, :cond_2

    .line 801
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 803
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 805
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    goto/16 :goto_1

    .line 819
    .restart local v4    # "v_5":I
    :cond_7
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 820
    sget-object v11, Lorg/tartarus/snowball/ext/FinnishStemmer;->g_V1:[C

    invoke-virtual {p0, v11, v13, v14}, Lorg/tartarus/snowball/ext/FinnishStemmer;->out_grouping_b([CII)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 825
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    goto/16 :goto_2

    .line 840
    .restart local v5    # "v_6":I
    :cond_8
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 843
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v6, v11, v12

    .line 846
    .local v6, "v_7":I
    const-string v11, "o"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 852
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v11, v6

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 854
    const-string v11, "u"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 860
    :cond_9
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    goto/16 :goto_3

    .line 875
    .end local v6    # "v_7":I
    .restart local v7    # "v_8":I
    :cond_a
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 877
    const-string v11, "j"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 882
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    goto/16 :goto_4

    .line 895
    .restart local v8    # "v_9":I
    :cond_b
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 906
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->ket:I

    .line 908
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    if-le v11, v12, :cond_0

    .line 912
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 914
    iget v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->bra:I

    .line 916
    iget-object v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->S_x:Ljava/lang/StringBuilder;

    invoke-virtual {p0, v11}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_to(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v11

    iput-object v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->S_x:Ljava/lang/StringBuilder;

    .line 918
    iget-object v11, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->S_x:Ljava/lang/StringBuilder;

    invoke-virtual {p0, v11}, Lorg/tartarus/snowball/ext/FinnishStemmer;->eq_v_b(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 923
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->slice_del()V

    move v9, v10

    .line 924
    goto/16 :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1043
    instance-of v0, p1, Lorg/tartarus/snowball/ext/FinnishStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1048
    const-class v0, Lorg/tartarus/snowball/ext/FinnishStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 11

    .prologue
    .line 940
    iget v0, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 943
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->r_mark_regions()Z

    move-result v9

    if-nez v9, :cond_0

    .line 948
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 950
    const/4 v9, 0x0

    iput-boolean v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->B_ending_removed:Z

    .line 952
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 955
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v1, v9, v10

    .line 958
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->r_particle_etc()Z

    move-result v9

    if-nez v9, :cond_1

    .line 963
    :cond_1
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v9, v1

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 965
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v2, v9, v10

    .line 968
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->r_possessive()Z

    move-result v9

    if-nez v9, :cond_2

    .line 973
    :cond_2
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v9, v2

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 975
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v3, v9, v10

    .line 978
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->r_case_ending()Z

    move-result v9

    if-nez v9, :cond_3

    .line 983
    :cond_3
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v9, v3

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 985
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v4, v9, v10

    .line 988
    .local v4, "v_5":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->r_other_endings()Z

    move-result v9

    if-nez v9, :cond_4

    .line 993
    :cond_4
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v9, v4

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 996
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v5, v9, v10

    .line 1000
    .local v5, "v_6":I
    iget-boolean v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->B_ending_removed:Z

    if-nez v9, :cond_7

    .line 1016
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v9, v5

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 1018
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v7, v9, v10

    .line 1021
    .local v7, "v_8":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->r_t_plural()Z

    move-result v9

    if-nez v9, :cond_5

    .line 1026
    :cond_5
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v9, v7

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 1029
    .end local v7    # "v_8":I
    :goto_0
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v8, v9, v10

    .line 1032
    .local v8, "v_9":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->r_tidy()Z

    move-result v9

    if-nez v9, :cond_6

    .line 1037
    :cond_6
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v9, v8

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    .line 1038
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit_backward:I

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    const/4 v9, 0x1

    return v9

    .line 1005
    .end local v8    # "v_9":I
    :cond_7
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    sub-int v6, v9, v10

    .line 1008
    .local v6, "v_7":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FinnishStemmer;->r_i_plural()Z

    move-result v9

    if-nez v9, :cond_8

    .line 1013
    :cond_8
    iget v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->limit:I

    sub-int/2addr v9, v6

    iput v9, p0, Lorg/tartarus/snowball/ext/FinnishStemmer;->cursor:I

    goto :goto_0
.end method
