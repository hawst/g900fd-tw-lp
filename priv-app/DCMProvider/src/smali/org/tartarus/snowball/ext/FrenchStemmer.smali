.class public Lorg/tartarus/snowball/ext/FrenchStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "FrenchStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final a_6:[Lorg/tartarus/snowball/Among;

.field private static final a_7:[Lorg/tartarus/snowball/Among;

.field private static final a_8:[Lorg/tartarus/snowball/Among;

.field private static final g_keep_with_s:[C

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_p2:I

.field private I_pV:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/FrenchStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    .line 19
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "col"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "par"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tap"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 25
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "I"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "U"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 29
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "Y"

    const/4 v2, 0x0

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 25
    sput-object v6, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 32
    const/4 v0, 0x6

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iqU"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abl"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "I\u00e8r"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00e8r"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eus"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iv"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 32
    sput-object v6, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 41
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 43
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abil"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 44
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iv"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 41
    sput-object v6, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 47
    const/16 v0, 0x2b

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iqUe"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atrice"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ance"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ence"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "logie"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "able"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isme"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "euse"

    const/4 v2, -0x1

    const/16 v3, 0xb

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iste"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ive"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "if"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "usion"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 60
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ation"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ution"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ateur"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iqUes"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atrices"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ances"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ences"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "logies"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ables"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 69
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ismes"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 70
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "euses"

    const/4 v2, -0x1

    const/16 v3, 0xb

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 71
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "istes"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ives"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ifs"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "usions"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 75
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ations"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 76
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "utions"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 77
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ateurs"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ments"

    const/4 v2, -0x1

    const/16 v3, 0xf

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 79
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ements"

    const/16 v2, 0x1e

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issements"

    const/16 v2, 0x1f

    const/16 v3, 0xc

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "it\u00e9s"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ment"

    const/4 v2, -0x1

    const/16 v3, 0xf

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ement"

    const/16 v2, 0x22

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issement"

    const/16 v2, 0x23

    const/16 v3, 0xc

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 85
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amment"

    const/16 v2, 0x22

    const/16 v3, 0xd

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x26

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "emment"

    const/16 v2, 0x22

    const/16 v3, 0xe

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x27

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aux"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x28

    .line 88
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eaux"

    const/16 v2, 0x27

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x29

    .line 89
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eux"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2a

    .line 90
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "it\u00e9"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 47
    sput-object v6, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 93
    const/16 v0, 0x23

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ira"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 95
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ie"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isse"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 97
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issante"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 98
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irai"

    const/4 v2, 0x4

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 101
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iras"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ies"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 103
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eemes"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 104
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isses"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 105
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issantes"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eetes"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "is"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irais"

    const/16 v2, 0xd

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 109
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issais"

    const/16 v2, 0xd

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irions"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 111
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issions"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irons"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issons"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issants"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 115
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "it"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 116
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irait"

    const/16 v2, 0x15

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issait"

    const/16 v2, 0x15

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 118
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issant"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 119
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iraIent"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issaIent"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 121
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irent"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 122
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issent"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 123
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iront"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 124
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eet"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 125
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iriez"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 126
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issiez"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 127
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irez"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 128
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issez"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 93
    sput-object v6, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 131
    const/16 v0, 0x26

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 132
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 133
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "era"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asse"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 135
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ante"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 136
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9e"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 137
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ai"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 138
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erai"

    const/4 v2, 0x5

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 139
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 140
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "as"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 141
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eras"

    const/16 v2, 0x8

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2mes"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 143
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asses"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "antes"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 145
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2tes"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 146
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9es"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ais"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 148
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erais"

    const/16 v2, 0xf

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 149
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ions"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 150
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erions"

    const/16 v2, 0x11

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 151
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "assions"

    const/16 v2, 0x11

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 152
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erons"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 153
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ants"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 154
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9s"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 155
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ait"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 156
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erait"

    const/16 v2, 0x17

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 157
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ant"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 158
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aIent"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 159
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eraIent"

    const/16 v2, 0x1a

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 160
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e8rent"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 161
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "assent"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 162
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eront"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 163
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2t"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 164
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ez"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 165
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iez"

    const/16 v2, 0x20

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 166
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eriez"

    const/16 v2, 0x21

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 167
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "assiez"

    const/16 v2, 0x21

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 168
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erez"

    const/16 v2, 0x20

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 169
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 131
    sput-object v6, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    .line 172
    const/4 v0, 0x7

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 173
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 174
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "I\u00e8re"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 175
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00e8re"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 176
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ion"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 177
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "Ier"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 178
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ier"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 179
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eb"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 172
    sput-object v6, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    .line 182
    const/4 v0, 0x5

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 183
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ell"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 184
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eill"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 185
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "enn"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 186
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "onn"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 187
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ett"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/FrenchStemmer;->methodObject:Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 182
    sput-object v6, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    .line 190
    const/16 v0, 0x14

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x80

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x82

    aput-char v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x67

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x8

    aput-char v2, v0, v1

    const/16 v1, 0x13

    const/4 v2, 0x5

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    .line 192
    const/16 v0, 0x11

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x14

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x80

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_keep_with_s:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/FrenchStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/FrenchStemmer;

    .prologue
    .line 199
    iget v0, p1, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_p2:I

    .line 200
    iget v0, p1, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_p1:I

    .line 201
    iget v0, p1, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_pV:I

    iput v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_pV:I

    .line 202
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 203
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 553
    iget v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 555
    const/4 v0, 0x0

    .line 557
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 561
    iget v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 563
    const/4 v0, 0x0

    .line 565
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_RV()Z
    .locals 2

    .prologue
    .line 545
    iget v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_pV:I

    iget v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 547
    const/4 v0, 0x0

    .line 549
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_i_verb_suffix()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1042
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 1044
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_pV:I

    if-ge v4, v5, :cond_0

    .line 1080
    :goto_0
    return v3

    .line 1048
    :cond_0
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_pV:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1049
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    .line 1050
    .local v2, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    .line 1051
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1054
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 1056
    sget-object v4, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x23

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/FrenchStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1057
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 1059
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    goto :goto_0

    .line 1063
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 1064
    packed-switch v0, :pswitch_data_0

    .line 1079
    :goto_1
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    .line 1080
    const/4 v3, 0x1

    goto :goto_0

    .line 1066
    :pswitch_0
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    goto :goto_0

    .line 1070
    :pswitch_1
    sget-object v4, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    const/16 v5, 0x61

    const/16 v6, 0xfb

    invoke-virtual {p0, v4, v5, v6}, Lorg/tartarus/snowball/ext/FrenchStemmer;->out_grouping_b([CII)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1072
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    goto :goto_0

    .line 1076
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto :goto_1

    .line 1064
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 7

    .prologue
    const/16 v6, 0xfb

    const/16 v5, 0x61

    .line 346
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_pV:I

    .line 347
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_p1:I

    .line 348
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_p2:I

    .line 350
    iget v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 355
    .local v0, "v_1":I
    iget v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 358
    .local v1, "v_2":I
    sget-object v3, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/FrenchStemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_3

    .line 374
    :cond_0
    iput v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 377
    sget-object v3, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/4 v4, 0x3

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v3

    if-nez v3, :cond_4

    .line 383
    iput v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 386
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    if-lt v3, v4, :cond_5

    .line 411
    :cond_1
    :goto_0
    iput v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 413
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 420
    .local v2, "v_4":I
    :goto_1
    sget-object v3, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/FrenchStemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_8

    .line 426
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    if-lt v3, v4, :cond_6

    .line 485
    :cond_2
    :goto_2
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 486
    const/4 v3, 0x1

    return v3

    .line 362
    .end local v2    # "v_4":I
    :cond_3
    sget-object v3, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/FrenchStemmer;->in_grouping([CII)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 367
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    if-ge v3, v4, :cond_0

    .line 371
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 409
    :cond_4
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_pV:I

    goto :goto_0

    .line 390
    :cond_5
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 395
    :goto_3
    sget-object v3, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/FrenchStemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_4

    .line 401
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    if-ge v3, v4, :cond_1

    .line 405
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto :goto_3

    .line 430
    .restart local v2    # "v_4":I
    :cond_6
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto :goto_1

    .line 446
    :cond_7
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 436
    :cond_8
    sget-object v3, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/FrenchStemmer;->out_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_9

    .line 442
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    if-lt v3, v4, :cond_7

    goto :goto_2

    .line 449
    :cond_9
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_p1:I

    .line 454
    :goto_4
    sget-object v3, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/FrenchStemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_b

    .line 460
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    if-ge v3, v4, :cond_2

    .line 464
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto :goto_4

    .line 480
    :cond_a
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 470
    :cond_b
    sget-object v3, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/FrenchStemmer;->out_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_c

    .line 476
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    if-lt v3, v4, :cond_a

    goto/16 :goto_2

    .line 483
    :cond_c
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_p2:I

    goto/16 :goto_2
.end method

.method private r_postlude()Z
    .locals 4

    .prologue
    .line 495
    :goto_0
    iget v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 499
    .local v1, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 501
    sget-object v2, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/FrenchStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 502
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 538
    :cond_0
    :pswitch_0
    iput v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 541
    const/4 v2, 0x1

    return v2

    .line 507
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 508
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 514
    :pswitch_1
    const-string v2, "i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 519
    :pswitch_2
    const-string v2, "u"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 524
    :pswitch_3
    const-string/jumbo v2, "y"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 529
    :pswitch_4
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 533
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto :goto_0

    .line 508
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private r_prelude()Z
    .locals 9

    .prologue
    const/16 v8, 0xfb

    const/16 v7, 0x61

    const/4 v6, 0x1

    .line 213
    :goto_0
    iget v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 218
    .local v0, "v_1":I
    :goto_1
    iget v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 223
    .local v1, "v_2":I
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 226
    .local v2, "v_3":I
    sget-object v4, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    invoke-virtual {p0, v4, v7, v8}, Lorg/tartarus/snowball/ext/FrenchStemmer;->in_grouping([CII)Z

    move-result v4

    if-nez v4, :cond_3

    .line 284
    :cond_0
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 288
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 290
    const-string/jumbo v4, "y"

    invoke-virtual {p0, v6, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 304
    :cond_1
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 307
    const-string v4, "q"

    invoke-virtual {p0, v6, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 326
    :cond_2
    iput v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 327
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    if-lt v4, v5, :cond_a

    .line 335
    iput v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 338
    return v6

    .line 231
    :cond_3
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 234
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 238
    .local v3, "v_4":I
    const-string v4, "u"

    invoke-virtual {p0, v6, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 252
    :cond_4
    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 256
    const-string v4, "i"

    invoke-virtual {p0, v6, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 270
    :cond_5
    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 273
    const-string/jumbo v4, "y"

    invoke-virtual {p0, v6, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 278
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 280
    const-string v4, "Y"

    invoke-virtual {p0, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 323
    .end local v3    # "v_4":I
    :goto_2
    iput v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto :goto_0

    .line 243
    .restart local v3    # "v_4":I
    :cond_6
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 244
    sget-object v4, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    invoke-virtual {p0, v4, v7, v8}, Lorg/tartarus/snowball/ext/FrenchStemmer;->in_grouping([CII)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 249
    const-string v4, "U"

    invoke-virtual {p0, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 261
    :cond_7
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 262
    sget-object v4, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    invoke-virtual {p0, v4, v7, v8}, Lorg/tartarus/snowball/ext/FrenchStemmer;->in_grouping([CII)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 267
    const-string v4, "I"

    invoke-virtual {p0, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 295
    .end local v3    # "v_4":I
    :cond_8
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 296
    sget-object v4, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    invoke-virtual {p0, v4, v7, v8}, Lorg/tartarus/snowball/ext/FrenchStemmer;->in_grouping([CII)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 301
    const-string v4, "Y"

    invoke-virtual {p0, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 312
    :cond_9
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 314
    const-string v4, "u"

    invoke-virtual {p0, v6, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 319
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 321
    const-string v4, "U"

    invoke-virtual {p0, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 331
    :cond_a
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1
.end method

.method private r_residual_suffix()Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1167
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v1, v8, v9

    .line 1171
    .local v1, "v_1":I
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 1173
    const-string v8, "s"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1175
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v8, v1

    iput v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1192
    :goto_0
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v3, v8, v9

    .line 1194
    .local v3, "v_3":I
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v9, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_pV:I

    if-ge v8, v9, :cond_2

    .line 1271
    :goto_1
    return v6

    .line 1179
    .end local v3    # "v_3":I
    :cond_0
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 1181
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v2, v8, v9

    .line 1182
    .local v2, "v_2":I
    sget-object v8, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_keep_with_s:[C

    const/16 v9, 0x61

    const/16 v10, 0xe8

    invoke-virtual {p0, v8, v9, v10}, Lorg/tartarus/snowball/ext/FrenchStemmer;->out_grouping_b([CII)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1184
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v8, v1

    iput v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto :goto_0

    .line 1187
    :cond_1
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v8, v2

    iput v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1189
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto :goto_0

    .line 1198
    .end local v2    # "v_2":I
    .restart local v3    # "v_3":I
    :cond_2
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_pV:I

    iput v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1199
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    .line 1200
    .local v4, "v_4":I
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    .line 1201
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v8, v3

    iput v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1204
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 1206
    sget-object v8, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    const/4 v9, 0x7

    invoke-virtual {p0, v8, v9}, Lorg/tartarus/snowball/ext/FrenchStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1207
    .local v0, "among_var":I
    if-nez v0, :cond_3

    .line 1209
    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    goto :goto_1

    .line 1213
    :cond_3
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 1214
    packed-switch v0, :pswitch_data_0

    .line 1270
    :goto_2
    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    move v6, v7

    .line 1271
    goto :goto_1

    .line 1216
    :pswitch_0
    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    goto :goto_1

    .line 1221
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v8

    if-nez v8, :cond_4

    .line 1223
    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    goto :goto_1

    .line 1228
    :cond_4
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v5, v8, v9

    .line 1231
    .local v5, "v_5":I
    const-string v8, "s"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 1237
    iget v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v8, v5

    iput v8, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1239
    const-string v8, "t"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 1241
    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    goto :goto_1

    .line 1246
    :cond_5
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto :goto_2

    .line 1251
    .end local v5    # "v_5":I
    :pswitch_2
    const-string v6, "i"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1256
    :pswitch_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto :goto_2

    .line 1261
    :pswitch_4
    const/4 v8, 0x2

    const-string v9, "gu"

    invoke-virtual {p0, v8, v9}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 1263
    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    goto/16 :goto_1

    .line 1267
    :cond_6
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto :goto_2

    .line 1214
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private r_standard_suffix()Z
    .locals 15

    .prologue
    .line 583
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 585
    sget-object v12, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/16 v13, 0x2b

    invoke-virtual {p0, v12, v13}, Lorg/tartarus/snowball/ext/FrenchStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 586
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 588
    const/4 v12, 0x0

    .line 1034
    :goto_0
    return v12

    .line 591
    :cond_0
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 592
    packed-switch v0, :pswitch_data_0

    .line 1034
    :goto_1
    const/4 v12, 0x1

    goto :goto_0

    .line 594
    :pswitch_0
    const/4 v12, 0x0

    goto :goto_0

    .line 598
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_1

    .line 600
    const/4 v12, 0x0

    goto :goto_0

    .line 603
    :cond_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto :goto_1

    .line 608
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_2

    .line 610
    const/4 v12, 0x0

    goto :goto_0

    .line 613
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    .line 615
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v1, v12, v13

    .line 619
    .local v1, "v_1":I
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 621
    const/4 v12, 0x2

    const-string v13, "ic"

    invoke-virtual {p0, v12, v13}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_3

    .line 623
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v1

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto :goto_1

    .line 627
    :cond_3
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 630
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v4, v12, v13

    .line 634
    .local v4, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_4

    .line 642
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v4

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 644
    const-string v12, "iqU"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 639
    :cond_4
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto :goto_1

    .line 651
    .end local v1    # "v_1":I
    .end local v4    # "v_2":I
    :pswitch_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_5

    .line 653
    const/4 v12, 0x0

    goto :goto_0

    .line 656
    :cond_5
    const-string v12, "log"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 661
    :pswitch_4
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_6

    .line 663
    const/4 v12, 0x0

    goto :goto_0

    .line 666
    :cond_6
    const-string v12, "u"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 671
    :pswitch_5
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_7

    .line 673
    const/4 v12, 0x0

    goto :goto_0

    .line 676
    :cond_7
    const-string v12, "ent"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 681
    :pswitch_6
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_RV()Z

    move-result v12

    if-nez v12, :cond_8

    .line 683
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 686
    :cond_8
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    .line 688
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v5, v12, v13

    .line 692
    .local v5, "v_3":I
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 694
    sget-object v12, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/4 v13, 0x6

    invoke-virtual {p0, v12, v13}, Lorg/tartarus/snowball/ext/FrenchStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 695
    if-nez v0, :cond_9

    .line 697
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v5

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 701
    :cond_9
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 702
    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    .line 704
    :pswitch_7
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v5

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 709
    :pswitch_8
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_a

    .line 711
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v5

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 715
    :cond_a
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    .line 717
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 719
    const/4 v12, 0x2

    const-string v13, "at"

    invoke-virtual {p0, v12, v13}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_b

    .line 721
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v5

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 725
    :cond_b
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 727
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_c

    .line 729
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v5

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 733
    :cond_c
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto/16 :goto_1

    .line 739
    :pswitch_9
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v6, v12, v13

    .line 743
    .local v6, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_d

    .line 751
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v6

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 754
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R1()Z

    move-result v12

    if-nez v12, :cond_e

    .line 756
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v5

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 748
    :cond_d
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto/16 :goto_1

    .line 760
    :cond_e
    const-string v12, "eux"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 766
    .end local v6    # "v_4":I
    :pswitch_a
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_f

    .line 768
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v5

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 772
    :cond_f
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto/16 :goto_1

    .line 777
    :pswitch_b
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_RV()Z

    move-result v12

    if-nez v12, :cond_10

    .line 779
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v5

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 783
    :cond_10
    const-string v12, "i"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 791
    .end local v5    # "v_3":I
    :pswitch_c
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_11

    .line 793
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 796
    :cond_11
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    .line 798
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v7, v12, v13

    .line 802
    .local v7, "v_5":I
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 804
    sget-object v12, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/4 v13, 0x3

    invoke-virtual {p0, v12, v13}, Lorg/tartarus/snowball/ext/FrenchStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 805
    if-nez v0, :cond_12

    .line 807
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v7

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 811
    :cond_12
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 812
    packed-switch v0, :pswitch_data_2

    goto/16 :goto_1

    .line 814
    :pswitch_d
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v7

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 820
    :pswitch_e
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v8, v12, v13

    .line 824
    .local v8, "v_6":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_13

    .line 832
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v8

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 834
    const-string v12, "abl"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 829
    :cond_13
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto/16 :goto_1

    .line 841
    .end local v8    # "v_6":I
    :pswitch_f
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v9, v12, v13

    .line 845
    .local v9, "v_7":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_14

    .line 853
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v9

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 855
    const-string v12, "iqU"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 850
    :cond_14
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto/16 :goto_1

    .line 861
    .end local v9    # "v_7":I
    :pswitch_10
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_15

    .line 863
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v7

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 867
    :cond_15
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto/16 :goto_1

    .line 875
    .end local v7    # "v_5":I
    :pswitch_11
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_16

    .line 877
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 880
    :cond_16
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    .line 882
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v10, v12, v13

    .line 886
    .local v10, "v_8":I
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 888
    const/4 v12, 0x2

    const-string v13, "at"

    invoke-virtual {p0, v12, v13}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_17

    .line 890
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v10

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 894
    :cond_17
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 896
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_18

    .line 898
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v10

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 902
    :cond_18
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    .line 904
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 906
    const/4 v12, 0x2

    const-string v13, "ic"

    invoke-virtual {p0, v12, v13}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_19

    .line 908
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v10

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto/16 :goto_1

    .line 912
    :cond_19
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 915
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v11, v12, v13

    .line 919
    .local v11, "v_9":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_1a

    .line 927
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v11

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 929
    const-string v12, "iqU"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 924
    :cond_1a
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto/16 :goto_1

    .line 936
    .end local v10    # "v_8":I
    .end local v11    # "v_9":I
    :pswitch_12
    const-string v12, "eau"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 941
    :pswitch_13
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R1()Z

    move-result v12

    if-nez v12, :cond_1b

    .line 943
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 946
    :cond_1b
    const-string v12, "al"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 952
    :pswitch_14
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v2, v12, v13

    .line 956
    .local v2, "v_10":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v12

    if-nez v12, :cond_1c

    .line 964
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v2

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 967
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R1()Z

    move-result v12

    if-nez v12, :cond_1d

    .line 969
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 961
    :cond_1c
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto/16 :goto_1

    .line 972
    :cond_1d
    const-string v12, "eux"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 978
    .end local v2    # "v_10":I
    :pswitch_15
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R1()Z

    move-result v12

    if-nez v12, :cond_1e

    .line 980
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 982
    :cond_1e
    sget-object v12, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    const/16 v13, 0x61

    const/16 v14, 0xfb

    invoke-virtual {p0, v12, v13, v14}, Lorg/tartarus/snowball/ext/FrenchStemmer;->out_grouping_b([CII)Z

    move-result v12

    if-nez v12, :cond_1f

    .line 984
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 987
    :cond_1f
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto/16 :goto_1

    .line 992
    :pswitch_16
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_RV()Z

    move-result v12

    if-nez v12, :cond_20

    .line 994
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 999
    :cond_20
    const-string v12, "ant"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 1000
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 1004
    :pswitch_17
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_RV()Z

    move-result v12

    if-nez v12, :cond_21

    .line 1006
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 1011
    :cond_21
    const-string v12, "ent"

    invoke-virtual {p0, v12}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 1012
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 1016
    :pswitch_18
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v3, v12, v13

    .line 1018
    .local v3, "v_11":I
    sget-object v12, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    const/16 v13, 0x61

    const/16 v14, 0xfb

    invoke-virtual {p0, v12, v13, v14}, Lorg/tartarus/snowball/ext/FrenchStemmer;->in_grouping_b([CII)Z

    move-result v12

    if-nez v12, :cond_22

    .line 1020
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 1023
    :cond_22
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_RV()Z

    move-result v12

    if-nez v12, :cond_23

    .line 1025
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 1027
    :cond_23
    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v12, v3

    iput v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1031
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    .line 1032
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 592
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_c
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch

    .line 702
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 812
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method private r_un_accent()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1305
    const/4 v0, 0x1

    .line 1310
    .local v0, "v_1":I
    :goto_0
    sget-object v4, Lorg/tartarus/snowball/ext/FrenchStemmer;->g_v:[C

    const/16 v5, 0x61

    const/16 v6, 0xfb

    invoke-virtual {p0, v4, v5, v6}, Lorg/tartarus/snowball/ext/FrenchStemmer;->out_grouping_b([CII)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1319
    if-lez v0, :cond_2

    .line 1348
    :cond_0
    :goto_1
    return v2

    .line 1314
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 1307
    goto :goto_0

    .line 1325
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 1328
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 1331
    .local v1, "v_3":I
    const-string/jumbo v4, "\u00e9"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1337
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1339
    const-string/jumbo v4, "\u00e8"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1345
    :cond_3
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 1347
    const-string v2, "e"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    move v2, v3

    .line 1348
    goto :goto_1
.end method

.method private r_un_double()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1278
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v0, v2, v3

    .line 1280
    .local v0, "v_1":I
    sget-object v2, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x5

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/FrenchStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v2

    if-nez v2, :cond_1

    .line 1297
    :cond_0
    :goto_0
    return v1

    .line 1284
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1286
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 1288
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    if-le v2, v3, :cond_0

    .line 1292
    iget v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1294
    iget v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 1296
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    .line 1297
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private r_verb_suffix()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1089
    iget v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 1091
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_pV:I

    if-ge v6, v7, :cond_0

    .line 1155
    :goto_0
    return v4

    .line 1095
    :cond_0
    iget v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->I_pV:I

    iput v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1096
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    .line 1097
    .local v2, "v_2":I
    iget v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    .line 1098
    iget v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1101
    iget v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 1103
    sget-object v6, Lorg/tartarus/snowball/ext/FrenchStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    const/16 v7, 0x26

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/FrenchStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1104
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 1106
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    goto :goto_0

    .line 1110
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 1111
    packed-switch v0, :pswitch_data_0

    .line 1154
    :goto_1
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    move v4, v5

    .line 1155
    goto :goto_0

    .line 1113
    :pswitch_0
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    goto :goto_0

    .line 1118
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_R2()Z

    move-result v6

    if-nez v6, :cond_2

    .line 1120
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    goto :goto_0

    .line 1124
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto :goto_1

    .line 1129
    :pswitch_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto :goto_1

    .line 1134
    :pswitch_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    .line 1136
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v3, v4, v6

    .line 1140
    .local v3, "v_3":I
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 1142
    const-string v4, "e"

    invoke-virtual {p0, v5, v4}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1144
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto :goto_1

    .line 1148
    :cond_3
    iget v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 1150
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_del()V

    goto :goto_1

    .line 1111
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1510
    instance-of v0, p1, Lorg/tartarus/snowball/ext/FrenchStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1515
    const-class v0, Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 14

    .prologue
    const/4 v13, 0x1

    .line 1366
    iget v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1369
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_prelude()Z

    move-result v11

    if-nez v11, :cond_0

    .line 1374
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1376
    iget v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1379
    .local v3, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_mark_regions()Z

    move-result v11

    if-nez v11, :cond_1

    .line 1384
    :cond_1
    iput v3, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1386
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1389
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v4, v11, v12

    .line 1394
    .local v4, "v_3":I
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v5, v11, v12

    .line 1398
    .local v5, "v_4":I
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v6, v11, v12

    .line 1402
    .local v6, "v_5":I
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v7, v11, v12

    .line 1405
    .local v7, "v_6":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_standard_suffix()Z

    move-result v11

    if-nez v11, :cond_6

    .line 1411
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v11, v7

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1414
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_i_verb_suffix()Z

    move-result v11

    if-nez v11, :cond_6

    .line 1420
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v11, v7

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1422
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_verb_suffix()Z

    move-result v11

    if-nez v11, :cond_6

    .line 1466
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1468
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_residual_suffix()Z

    move-result v11

    if-nez v11, :cond_2

    .line 1474
    :cond_2
    :goto_0
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1476
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v10, v11, v12

    .line 1479
    .local v10, "v_9":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_un_double()Z

    move-result v11

    if-nez v11, :cond_3

    .line 1484
    :cond_3
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1486
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v1, v11, v12

    .line 1489
    .local v1, "v_10":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_un_accent()Z

    move-result v11

    if-nez v11, :cond_4

    .line 1494
    :cond_4
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v11, v1

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1495
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit_backward:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1496
    iget v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1499
    .local v2, "v_11":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/FrenchStemmer;->r_postlude()Z

    move-result v11

    if-nez v11, :cond_5

    .line 1504
    :cond_5
    iput v2, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1505
    return v13

    .line 1427
    .end local v1    # "v_10":I
    .end local v2    # "v_11":I
    .end local v10    # "v_9":I
    :cond_6
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v11, v6

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1429
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v8, v11, v12

    .line 1433
    .local v8, "v_7":I
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->ket:I

    .line 1436
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    sub-int v9, v11, v12

    .line 1440
    .local v9, "v_8":I
    const-string v11, "Y"

    invoke-virtual {p0, v13, v11}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_7

    .line 1450
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v11, v9

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    .line 1453
    const-string/jumbo v11, "\u00e7"

    invoke-virtual {p0, v13, v11}, Lorg/tartarus/snowball/ext/FrenchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_8

    .line 1455
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    goto :goto_0

    .line 1445
    :cond_7
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 1447
    const-string v11, "i"

    invoke-virtual {p0, v11}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1459
    :cond_8
    iget v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/FrenchStemmer;->bra:I

    .line 1461
    const-string v11, "c"

    invoke-virtual {p0, v11}, Lorg/tartarus/snowball/ext/FrenchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
