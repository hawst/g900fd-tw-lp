.class public Lorg/tartarus/snowball/ext/KpStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "KpStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final a_6:[Lorg/tartarus/snowball/Among;

.field private static final a_7:[Lorg/tartarus/snowball/Among;

.field private static final g_AIOU:[C

.field private static final g_AOU:[C

.field private static final g_v:[C

.field private static final g_v_WX:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private B_GE_removed:Z

.field private B_Y_found:Z

.field private B_stemmed:Z

.field private I_p1:I

.field private I_p2:I

.field private I_x:I

.field private S_ch:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/KpStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    .line 19
    const/4 v0, 0x7

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nde"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "\'s"

    const/4 v2, 0x2

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "es"

    const/4 v2, 0x2

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ies"

    const/4 v2, 0x4

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aus"

    const/4 v2, 0x2

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 29
    const/16 v0, 0xb

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 30
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "de"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 31
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ge"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ische"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "je"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lijke"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "le"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ene"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "re"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "se"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "te"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ieve"

    const/4 v2, -0x1

    const/16 v3, 0xb

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 29
    sput-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 43
    const/16 v0, 0xe

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 44
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "heid"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "fie"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gie"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atie"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isme"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ing"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "arij"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erij"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sel"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "rder"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ster"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iteit"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dst"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tst"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 43
    sput-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 60
    const/16 v0, 0x10

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "end"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atief"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erig"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "achtig"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ioneel"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "baar"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "laar"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "naar"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 69
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "raar"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 70
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eriger"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 71
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "achtiger"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lijker"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tant"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erigst"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 75
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "achtigst"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 76
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lijkst"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 60
    sput-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 79
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ig"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iger"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "igst"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 79
    sput-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 85
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ft"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kt"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 88
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pt"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 85
    sput-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 91
    const/16 v0, 0x16

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 92
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bb"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 93
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "cc"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dd"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 95
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ff"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gg"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 97
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "hh"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 98
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "jj"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kk"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ll"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 101
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mm"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nn"

    const/4 v2, -0x1

    const/16 v3, 0xb

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 103
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pp"

    const/4 v2, -0x1

    const/16 v3, 0xc

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 104
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "qq"

    const/4 v2, -0x1

    const/16 v3, 0xd

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 105
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "rr"

    const/4 v2, -0x1

    const/16 v3, 0xe

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ss"

    const/4 v2, -0x1

    const/16 v3, 0xf

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tt"

    const/4 v2, -0x1

    const/16 v3, 0x10

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "v"

    const/4 v2, -0x1

    const/16 v3, 0x15

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 109
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "vv"

    const/16 v2, 0x10

    const/16 v3, 0x11

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "ww"

    const/4 v2, -0x1

    const/16 v3, 0x12

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 111
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "xx"

    const/4 v2, -0x1

    const/16 v3, 0x13

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "z"

    const/4 v2, -0x1

    const/16 v3, 0x16

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zz"

    const/16 v2, 0x14

    const/16 v3, 0x14

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 91
    sput-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    .line 116
    const/4 v0, 0x2

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 118
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->methodObject:Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 116
    sput-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    .line 121
    const/4 v0, 0x4

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    .line 123
    const/4 v0, 0x4

    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lorg/tartarus/snowball/ext/KpStemmer;->g_v_WX:[C

    .line 125
    const/4 v0, 0x3

    new-array v0, v0, [C

    fill-array-data v0, :array_2

    sput-object v0, Lorg/tartarus/snowball/ext/KpStemmer;->g_AOU:[C

    .line 127
    const/4 v0, 0x3

    new-array v0, v0, [C

    fill-array-data v0, :array_3

    sput-object v0, Lorg/tartarus/snowball/ext/KpStemmer;->g_AIOU:[C

    return-void

    .line 121
    nop

    :array_0
    .array-data 2
        0x11s
        0x41s
        0x10s
        0x1s
    .end array-data

    .line 123
    :array_1
    .array-data 2
        0x11s
        0x41s
        0xd0s
        0x1s
    .end array-data

    .line 125
    :array_2
    .array-data 2
        0x1s
        0x40s
        0x10s
    .end array-data

    .line 127
    nop

    :array_3
    .array-data 2
        0x1s
        0x41s
        0x10s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->S_ch:Ljava/lang/StringBuilder;

    .line 13
    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/KpStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/KpStemmer;

    .prologue
    .line 138
    iget-boolean v0, p1, Lorg/tartarus/snowball/ext/KpStemmer;->B_GE_removed:Z

    iput-boolean v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->B_GE_removed:Z

    .line 139
    iget-boolean v0, p1, Lorg/tartarus/snowball/ext/KpStemmer;->B_stemmed:Z

    iput-boolean v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->B_stemmed:Z

    .line 140
    iget-boolean v0, p1, Lorg/tartarus/snowball/ext/KpStemmer;->B_Y_found:Z

    iput-boolean v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->B_Y_found:Z

    .line 141
    iget v0, p1, Lorg/tartarus/snowball/ext/KpStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_p2:I

    .line 142
    iget v0, p1, Lorg/tartarus/snowball/ext/KpStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_p1:I

    .line 143
    iget v0, p1, Lorg/tartarus/snowball/ext/KpStemmer;->I_x:I

    iput v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_x:I

    .line 144
    iget-object v0, p1, Lorg/tartarus/snowball/ext/KpStemmer;->S_ch:Ljava/lang/StringBuilder;

    iput-object v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->S_ch:Ljava/lang/StringBuilder;

    .line 145
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 146
    return-void
.end method

.method private r_C()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 234
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v0, v3, v4

    .line 238
    .local v0, "v_1":I
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v1, v3, v4

    .line 241
    .local v1, "v_2":I
    const/4 v3, 0x2

    const-string v4, "ij"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 247
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 249
    sget-object v3, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    const/16 v4, 0x61

    const/16 v5, 0x79

    invoke-virtual {p0, v3, v4, v5}, Lorg/tartarus/snowball/ext/KpStemmer;->out_grouping_b([CII)Z

    move-result v3

    if-nez v3, :cond_1

    .line 254
    :cond_0
    :goto_0
    return v2

    .line 253
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 254
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private r_Lose_infix()Z
    .locals 10

    .prologue
    const/16 v9, 0x79

    const/16 v8, 0x61

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1635
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    if-lt v6, v7, :cond_1

    .line 1717
    :cond_0
    :goto_0
    return v4

    .line 1639
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1646
    :goto_1
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 1648
    const/4 v6, 0x2

    const-string v7, "ge"

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1656
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    if-ge v6, v7, :cond_0

    .line 1660
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    goto :goto_1

    .line 1653
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 1663
    iget v1, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1666
    .local v1, "v_2":I
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    add-int/lit8 v0, v6, 0x3

    .line 1667
    .local v0, "c":I
    if-ltz v0, :cond_0

    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    if-gt v0, v6, :cond_0

    .line 1671
    iput v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1673
    iput v1, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1678
    :goto_2
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1680
    .local v2, "v_3":I
    sget-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v6, v8, v9}, Lorg/tartarus/snowball/ext/KpStemmer;->in_grouping([CII)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1687
    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1688
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    if-ge v6, v7, :cond_0

    .line 1692
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    goto :goto_2

    .line 1684
    :cond_3
    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1697
    :goto_3
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1699
    .local v3, "v_4":I
    sget-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v6, v8, v9}, Lorg/tartarus/snowball/ext/KpStemmer;->out_grouping([CII)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1706
    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1707
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    if-ge v6, v7, :cond_0

    .line 1711
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    goto :goto_3

    .line 1703
    :cond_4
    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1714
    iput-boolean v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->B_GE_removed:Z

    .line 1716
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    move v4, v5

    .line 1717
    goto :goto_0
.end method

.method private r_Lose_prefix()Z
    .locals 10

    .prologue
    const/16 v9, 0x79

    const/16 v8, 0x61

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1563
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 1565
    const/4 v6, 0x2

    const-string v7, "ge"

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1626
    :cond_0
    :goto_0
    return v4

    .line 1570
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 1572
    iget v1, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1575
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    add-int/lit8 v0, v6, 0x3

    .line 1576
    .local v0, "c":I
    if-ltz v0, :cond_0

    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    if-gt v0, v6, :cond_0

    .line 1580
    iput v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1582
    iput v1, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1587
    :goto_1
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1589
    .local v2, "v_2":I
    sget-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v6, v8, v9}, Lorg/tartarus/snowball/ext/KpStemmer;->in_grouping([CII)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1596
    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1597
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    if-ge v6, v7, :cond_0

    .line 1601
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    goto :goto_1

    .line 1593
    :cond_2
    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1606
    :goto_2
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1608
    .local v3, "v_3":I
    sget-object v6, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v6, v8, v9}, Lorg/tartarus/snowball/ext/KpStemmer;->out_grouping([CII)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1615
    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1616
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    if-ge v6, v7, :cond_0

    .line 1620
    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    goto :goto_2

    .line 1612
    :cond_3
    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1623
    iput-boolean v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->B_GE_removed:Z

    .line 1625
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    move v4, v5

    .line 1626
    goto :goto_0
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 151
    iget v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_x:I

    .line 152
    iget v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_x:I

    iget v1, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_p1:I

    if-ge v0, v1, :cond_0

    .line 154
    const/4 v0, 0x0

    .line 156
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 162
    iget v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_x:I

    .line 163
    iget v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_x:I

    iget v1, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_p2:I

    if-ge v0, v1, :cond_0

    .line 165
    const/4 v0, 0x0

    .line 167
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_Step_1()Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 397
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 399
    sget-object v7, Lorg/tartarus/snowball/ext/KpStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/4 v8, 0x7

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/KpStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 400
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 663
    :cond_0
    :goto_0
    :pswitch_0
    return v5

    .line 406
    :cond_1
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 407
    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    move v5, v6

    .line 663
    goto :goto_0

    .line 413
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto :goto_1

    .line 418
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 424
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v1, v7, v8

    .line 428
    .local v1, "v_1":I
    const-string v7, "t"

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 439
    :goto_2
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v7, v1

    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 442
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 447
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto :goto_1

    .line 433
    :cond_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v7

    if-nez v7, :cond_0

    goto :goto_2

    .line 452
    .end local v1    # "v_1":I
    :pswitch_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 457
    const-string v5, "ie"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 463
    :pswitch_4
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v2, v7, v8

    .line 467
    .local v2, "v_2":I
    const-string v7, "ar"

    invoke-virtual {p0, v9, v7}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 492
    :cond_4
    :goto_3
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v7, v2

    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 496
    const-string v7, "er"

    invoke-virtual {p0, v9, v7}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 516
    :cond_5
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v7, v2

    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 519
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 524
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 529
    const-string v5, "e"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 472
    :cond_6
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 477
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 482
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 484
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    .line 486
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_lengthen_V()Z

    move-result v7

    if-nez v7, :cond_2

    goto :goto_3

    .line 501
    :cond_7
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 506
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 511
    iget v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 513
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto/16 :goto_1

    .line 535
    .end local v2    # "v_2":I
    :pswitch_5
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 540
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_V()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 545
    const-string v5, "au"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 551
    :pswitch_6
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v3, v7, v8

    .line 555
    .local v3, "v_3":I
    const/4 v7, 0x3

    const-string v8, "hed"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 570
    :cond_8
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v7, v3

    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 574
    const-string v7, "nd"

    invoke-virtual {p0, v9, v7}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_c

    .line 582
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v7, v3

    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 586
    const-string v7, "d"

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_d

    .line 606
    :cond_9
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v7, v3

    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 611
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v4, v7, v8

    .line 614
    .local v4, "v_4":I
    const-string v7, "i"

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_e

    .line 620
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v7, v4

    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 622
    const-string v7, "j"

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_e

    .line 636
    :cond_a
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v7, v3

    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 639
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 644
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 649
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    .line 651
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_lengthen_V()Z

    move-result v7

    if-nez v7, :cond_2

    goto/16 :goto_0

    .line 560
    .end local v4    # "v_4":I
    :cond_b
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 565
    iget v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 567
    const-string v5, "heid"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 579
    :cond_c
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto/16 :goto_1

    .line 591
    :cond_d
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 596
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 601
    iget v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 603
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto/16 :goto_1

    .line 628
    .restart local v4    # "v_4":I
    :cond_e
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_V()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 633
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto/16 :goto_1

    .line 660
    .end local v3    # "v_3":I
    .end local v4    # "v_4":I
    :pswitch_7
    const-string v5, "nd"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 407
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private r_Step_1c()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1482
    iget v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 1484
    sget-object v5, Lorg/tartarus/snowball/ext/KpStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/KpStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1485
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 1554
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 1491
    :cond_1
    iget v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 1493
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1498
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1502
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v3, v4

    .line 1554
    goto :goto_0

    .line 1509
    :pswitch_1
    iget v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 1513
    .local v1, "v_1":I
    const-string v5, "n"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1524
    :goto_2
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1527
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto :goto_1

    .line 1518
    :cond_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_2

    .line 1533
    .end local v1    # "v_1":I
    :pswitch_2
    iget v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 1537
    .local v2, "v_2":I
    const-string v5, "h"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1548
    :goto_3
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v3, v2

    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1551
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto :goto_1

    .line 1542
    :cond_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_3

    .line 1502
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_Step_2()Z
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 671
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 673
    sget-object v4, Lorg/tartarus/snowball/ext/KpStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0xb

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/KpStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 674
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 959
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 680
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 681
    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    move v2, v3

    .line 959
    goto :goto_0

    .line 688
    :pswitch_1
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 692
    .local v1, "v_1":I
    const-string v4, "\'t"

    invoke-virtual {p0, v6, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 702
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 706
    const-string v4, "et"

    invoke-virtual {p0, v6, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 726
    :cond_3
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 730
    const-string v4, "rnt"

    invoke-virtual {p0, v7, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 740
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 744
    const-string v4, "t"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 764
    :cond_4
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 768
    const-string v4, "ink"

    invoke-virtual {p0, v7, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 778
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 782
    const-string v4, "mp"

    invoke-virtual {p0, v6, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 792
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 796
    const-string v4, "\'"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 811
    :cond_5
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 814
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 816
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 821
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 826
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto :goto_1

    .line 697
    :cond_6
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 699
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto :goto_1

    .line 711
    :cond_7
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 713
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 718
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 723
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto/16 :goto_1

    .line 735
    :cond_8
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 737
    const-string v2, "rn"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 749
    :cond_9
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 751
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 756
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_VX()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 761
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto/16 :goto_1

    .line 773
    :cond_a
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 775
    const-string v2, "ing"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 787
    :cond_b
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 789
    const-string v2, "m"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 801
    :cond_c
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 803
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 808
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto/16 :goto_1

    .line 832
    .end local v1    # "v_1":I
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 837
    const-string v2, "g"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 842
    :pswitch_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 847
    const-string v2, "lijk"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 852
    :pswitch_4
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 857
    const-string v2, "isch"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 862
    :pswitch_5
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 867
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 872
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto/16 :goto_1

    .line 877
    :pswitch_6
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 882
    const-string v2, "t"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 887
    :pswitch_7
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 892
    const-string v2, "s"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 897
    :pswitch_8
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 902
    const-string v2, "r"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 907
    :pswitch_9
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 912
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    .line 914
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    const-string v6, "l"

    invoke-virtual {p0, v4, v5, v6}, Lorg/tartarus/snowball/ext/KpStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 916
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_lengthen_V()Z

    move-result v4

    if-nez v4, :cond_2

    goto/16 :goto_0

    .line 924
    :pswitch_a
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 929
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 934
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    .line 936
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    const-string v6, "en"

    invoke-virtual {p0, v4, v5, v6}, Lorg/tartarus/snowball/ext/KpStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 938
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_lengthen_V()Z

    move-result v4

    if-nez v4, :cond_2

    goto/16 :goto_0

    .line 946
    :pswitch_b
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 951
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 956
    const-string v2, "ief"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 681
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private r_Step_3()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 966
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 968
    sget-object v2, Lorg/tartarus/snowball/ext/KpStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0xe

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/KpStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 969
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 1114
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 975
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 976
    packed-switch v0, :pswitch_data_0

    .line 1114
    :cond_2
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 982
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 987
    const-string v1, "eer"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 992
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 997
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    .line 999
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_lengthen_V()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 1007
    :pswitch_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1012
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto :goto_1

    .line 1017
    :pswitch_4
    const-string v1, "r"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1022
    :pswitch_5
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1027
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    .line 1029
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_lengthen_V()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 1037
    :pswitch_6
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1042
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1047
    const-string v1, "aar"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1052
    :pswitch_7
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1057
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    .line 1059
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    const-string v4, "f"

    invoke-virtual {p0, v2, v3, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 1061
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_lengthen_V()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 1069
    :pswitch_8
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1074
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    .line 1076
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    const-string v4, "g"

    invoke-virtual {p0, v2, v3, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 1078
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_lengthen_V()Z

    move-result v2

    if-nez v2, :cond_2

    goto/16 :goto_0

    .line 1086
    :pswitch_9
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1091
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1096
    const-string v1, "t"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1101
    :pswitch_a
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1106
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1111
    const-string v1, "d"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 976
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private r_Step_4()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1123
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v1, v3, v4

    .line 1127
    .local v1, "v_1":I
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 1129
    sget-object v3, Lorg/tartarus/snowball/ext/KpStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/16 v4, 0x10

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1130
    .local v0, "among_var":I
    if-nez v0, :cond_2

    .line 1268
    :cond_0
    :goto_0
    :pswitch_0
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1271
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 1273
    sget-object v3, Lorg/tartarus/snowball/ext/KpStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/4 v4, 0x3

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1274
    if-nez v0, :cond_4

    .line 1306
    :cond_1
    :goto_1
    :pswitch_1
    return v2

    .line 1136
    :cond_2
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 1137
    packed-switch v0, :pswitch_data_0

    .line 1306
    :cond_3
    :goto_2
    const/4 v2, 0x1

    goto :goto_1

    .line 1143
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1148
    const-string v2, "ie"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1153
    :pswitch_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1158
    const-string v2, "eer"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1163
    :pswitch_4
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1168
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto :goto_2

    .line 1173
    :pswitch_5
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1178
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_V()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1183
    const-string v2, "n"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1188
    :pswitch_6
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1193
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_V()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1198
    const-string v2, "l"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1203
    :pswitch_7
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1208
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_V()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1213
    const-string v2, "r"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1218
    :pswitch_8
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1223
    const-string v2, "teer"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1228
    :pswitch_9
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1233
    const-string v2, "lijk"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1238
    :pswitch_a
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1243
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    goto :goto_2

    .line 1248
    :pswitch_b
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1253
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1258
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    .line 1260
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_lengthen_V()Z

    move-result v3

    if-nez v3, :cond_3

    goto/16 :goto_0

    .line 1280
    :cond_4
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 1281
    packed-switch v0, :pswitch_data_1

    goto/16 :goto_2

    .line 1287
    :pswitch_c
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1292
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_C()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1297
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_del()V

    .line 1299
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_lengthen_V()Z

    move-result v3

    if-nez v3, :cond_3

    goto/16 :goto_1

    .line 1137
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 1281
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_c
    .end packed-switch
.end method

.method private r_Step_6()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1349
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 1351
    sget-object v2, Lorg/tartarus/snowball/ext/KpStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x16

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/KpStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1352
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 1473
    :goto_0
    :pswitch_0
    return v1

    .line 1358
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 1359
    packed-switch v0, :pswitch_data_0

    .line 1473
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 1365
    :pswitch_1
    const-string v1, "b"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1370
    :pswitch_2
    const-string v1, "c"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1375
    :pswitch_3
    const-string v1, "d"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1380
    :pswitch_4
    const-string v1, "f"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1385
    :pswitch_5
    const-string v1, "g"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1390
    :pswitch_6
    const-string v1, "h"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1395
    :pswitch_7
    const-string v1, "j"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1400
    :pswitch_8
    const-string v1, "k"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1405
    :pswitch_9
    const-string v1, "l"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1410
    :pswitch_a
    const-string v1, "m"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1415
    :pswitch_b
    const-string v1, "n"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1420
    :pswitch_c
    const-string v1, "p"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1425
    :pswitch_d
    const-string v1, "q"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1430
    :pswitch_e
    const-string v1, "r"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1435
    :pswitch_f
    const-string v1, "s"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1440
    :pswitch_10
    const-string v1, "t"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1445
    :pswitch_11
    const-string v1, "v"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1450
    :pswitch_12
    const-string/jumbo v1, "w"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1455
    :pswitch_13
    const-string/jumbo v1, "x"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1460
    :pswitch_14
    const-string/jumbo v1, "z"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1465
    :pswitch_15
    const-string v1, "f"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1470
    :pswitch_16
    const-string v1, "s"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1359
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method

.method private r_Step_7()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1313
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 1315
    sget-object v2, Lorg/tartarus/snowball/ext/KpStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/KpStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1316
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 1342
    :goto_0
    :pswitch_0
    return v1

    .line 1322
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 1323
    packed-switch v0, :pswitch_data_0

    .line 1342
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 1329
    :pswitch_1
    const-string v1, "k"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1334
    :pswitch_2
    const-string v1, "f"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1339
    :pswitch_3
    const-string v1, "p"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1323
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_V()Z
    .locals 5

    .prologue
    .line 174
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v0, v2, v3

    .line 178
    .local v0, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v1, v2, v3

    .line 180
    .local v1, "v_2":I
    sget-object v2, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    const/16 v3, 0x61

    const/16 v4, 0x79

    invoke-virtual {p0, v2, v3, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->in_grouping_b([CII)Z

    move-result v2

    if-nez v2, :cond_0

    .line 186
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v2, v1

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 188
    const/4 v2, 0x2

    const-string v3, "ij"

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 190
    const/4 v2, 0x0

    .line 194
    :goto_0
    return v2

    .line 193
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 194
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private r_VX()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 201
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v0, v3, v4

    .line 204
    .local v0, "v_1":I
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    if-gt v3, v4, :cond_1

    .line 227
    :cond_0
    :goto_0
    return v2

    .line 208
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 211
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v1, v3, v4

    .line 213
    .local v1, "v_2":I
    sget-object v3, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    const/16 v4, 0x61

    const/16 v5, 0x79

    invoke-virtual {p0, v3, v4, v5}, Lorg/tartarus/snowball/ext/KpStemmer;->in_grouping_b([CII)Z

    move-result v3

    if-nez v3, :cond_2

    .line 219
    iget v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 221
    const/4 v3, 0x2

    const-string v4, "ij"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 226
    :cond_2
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 227
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private r_lengthen_V()Z
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/16 v13, 0x75

    const/16 v12, 0x79

    const/16 v11, 0x61

    .line 267
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v1, v9, v10

    .line 270
    .local v1, "v_1":I
    sget-object v9, Lorg/tartarus/snowball/ext/KpStemmer;->g_v_WX:[C

    invoke-virtual {p0, v9, v11, v12}, Lorg/tartarus/snowball/ext/KpStemmer;->out_grouping_b([CII)Z

    move-result v9

    if-nez v9, :cond_1

    .line 385
    :cond_0
    :goto_0
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v9, v1

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 386
    return v14

    .line 275
    :cond_1
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 278
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v2, v9, v10

    .line 281
    .local v2, "v_2":I
    sget-object v9, Lorg/tartarus/snowball/ext/KpStemmer;->g_AOU:[C

    invoke-virtual {p0, v9, v11, v13}, Lorg/tartarus/snowball/ext/KpStemmer;->in_grouping_b([CII)Z

    move-result v9

    if-nez v9, :cond_5

    .line 310
    :cond_2
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v9, v2

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 313
    const-string v9, "e"

    invoke-virtual {p0, v14, v9}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 318
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 320
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v5, v9, v10

    .line 324
    .local v5, "v_5":I
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v6, v9, v10

    .line 326
    .local v6, "v_6":I
    sget-object v9, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v9, v11, v12}, Lorg/tartarus/snowball/ext/KpStemmer;->out_grouping_b([CII)Z

    move-result v9

    if-nez v9, :cond_3

    .line 332
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v9, v6

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 334
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    if-gt v9, v10, :cond_0

    .line 341
    :cond_3
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v7, v9, v10

    .line 343
    .local v7, "v_7":I
    sget-object v9, Lorg/tartarus/snowball/ext/KpStemmer;->g_AIOU:[C

    invoke-virtual {p0, v9, v11, v13}, Lorg/tartarus/snowball/ext/KpStemmer;->in_grouping_b([CII)Z

    move-result v9

    if-nez v9, :cond_0

    .line 349
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v9, v7

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 353
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v8, v9, v10

    .line 357
    .local v8, "v_8":I
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    if-gt v9, v10, :cond_7

    .line 372
    :cond_4
    :goto_1
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v9, v8

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 374
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v9, v5

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 377
    .end local v5    # "v_5":I
    .end local v6    # "v_6":I
    .end local v7    # "v_7":I
    .end local v8    # "v_8":I
    :goto_2
    iget-object v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->S_ch:Ljava/lang/StringBuilder;

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_to(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v9

    iput-object v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->S_ch:Ljava/lang/StringBuilder;

    .line 380
    iget v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 381
    .local v0, "c":I
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget-object v11, p0, Lorg/tartarus/snowball/ext/KpStemmer;->S_ch:Ljava/lang/StringBuilder;

    invoke-virtual {p0, v9, v10, v11}, Lorg/tartarus/snowball/ext/KpStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 382
    iput v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    goto/16 :goto_0

    .line 286
    .end local v0    # "c":I
    :cond_5
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 288
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v3, v9, v10

    .line 292
    .local v3, "v_3":I
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    sub-int v4, v9, v10

    .line 294
    .local v4, "v_4":I
    sget-object v9, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v9, v11, v12}, Lorg/tartarus/snowball/ext/KpStemmer;->out_grouping_b([CII)Z

    move-result v9

    if-nez v9, :cond_6

    .line 300
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v9, v4

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 302
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    if-gt v9, v10, :cond_2

    .line 307
    :cond_6
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    sub-int/2addr v9, v3

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    goto :goto_2

    .line 361
    .end local v3    # "v_3":I
    .end local v4    # "v_4":I
    .restart local v5    # "v_5":I
    .restart local v6    # "v_6":I
    .restart local v7    # "v_7":I
    .restart local v8    # "v_8":I
    :cond_7
    iget v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 362
    sget-object v9, Lorg/tartarus/snowball/ext/KpStemmer;->g_AIOU:[C

    invoke-virtual {p0, v9, v11, v13}, Lorg/tartarus/snowball/ext/KpStemmer;->in_grouping_b([CII)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 366
    sget-object v9, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v9, v11, v12}, Lorg/tartarus/snowball/ext/KpStemmer;->out_grouping_b([CII)Z

    move-result v9

    if-nez v9, :cond_0

    goto :goto_1
.end method

.method private r_measure()Z
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/16 v10, 0x79

    const/16 v9, 0x61

    .line 1729
    iget v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1733
    .local v0, "v_1":I
    iget v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    iput v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1735
    iget v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_p1:I

    .line 1737
    iget v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_p2:I

    .line 1739
    iput v0, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1741
    iget v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1748
    .local v2, "v_2":I
    :cond_0
    sget-object v8, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v8, v9, v10}, Lorg/tartarus/snowball/ext/KpStemmer;->out_grouping([CII)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1758
    const/4 v3, 0x1

    .line 1762
    .local v3, "v_4":I
    :goto_0
    iget v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1767
    .local v4, "v_5":I
    iget v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1770
    .local v5, "v_6":I
    const-string v8, "ij"

    invoke-virtual {p0, v11, v8}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1776
    iput v5, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1777
    sget-object v8, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v8, v9, v10}, Lorg/tartarus/snowball/ext/KpStemmer;->in_grouping([CII)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1785
    iput v4, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1788
    if-lez v3, :cond_3

    .line 1855
    :cond_1
    :goto_1
    iput v2, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1856
    const/4 v8, 0x1

    return v8

    .line 1782
    :cond_2
    add-int/lit8 v3, v3, -0x1

    .line 1783
    goto :goto_0

    .line 1793
    :cond_3
    sget-object v8, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v8, v9, v10}, Lorg/tartarus/snowball/ext/KpStemmer;->out_grouping([CII)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1798
    iget v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_p1:I

    .line 1803
    :cond_4
    sget-object v8, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v8, v9, v10}, Lorg/tartarus/snowball/ext/KpStemmer;->out_grouping([CII)Z

    move-result v8

    if-nez v8, :cond_4

    .line 1813
    const/4 v6, 0x1

    .line 1817
    .local v6, "v_8":I
    :goto_2
    iget v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1822
    .local v7, "v_9":I
    iget v1, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1825
    .local v1, "v_10":I
    const-string v8, "ij"

    invoke-virtual {p0, v11, v8}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 1831
    iput v1, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1832
    sget-object v8, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v8, v9, v10}, Lorg/tartarus/snowball/ext/KpStemmer;->in_grouping([CII)Z

    move-result v8

    if-nez v8, :cond_5

    .line 1840
    iput v7, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1843
    if-gtz v6, :cond_1

    .line 1848
    sget-object v8, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    invoke-virtual {p0, v8, v9, v10}, Lorg/tartarus/snowball/ext/KpStemmer;->out_grouping([CII)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1853
    iget v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/KpStemmer;->I_p2:I

    goto :goto_1

    .line 1837
    :cond_5
    add-int/lit8 v6, v6, -0x1

    .line 1838
    goto :goto_2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 2190
    instance-of v0, p1, Lorg/tartarus/snowball/ext/KpStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 2195
    const-class v0, Lorg/tartarus/snowball/ext/KpStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 26

    .prologue
    .line 1882
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->B_Y_found:Z

    .line 1884
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->B_stemmed:Z

    .line 1886
    move-object/from16 v0, p0

    iget v4, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1890
    .local v4, "v_1":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 1892
    const/16 v23, 0x1

    const-string/jumbo v24, "y"

    move-object/from16 v0, p0

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_1

    .line 1903
    :goto_0
    move-object/from16 v0, p0

    iput v4, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1905
    move-object/from16 v0, p0

    iget v14, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1910
    .local v14, "v_2":I
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v16, v0

    .line 1916
    .local v16, "v_3":I
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v17, v0

    .line 1919
    .local v17, "v_4":I
    sget-object v23, Lorg/tartarus/snowball/ext/KpStemmer;->g_v:[C

    const/16 v24, 0x61

    const/16 v25, 0x79

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/KpStemmer;->in_grouping([CII)Z

    move-result v23

    if-nez v23, :cond_2

    .line 1935
    :cond_0
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1936
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_3

    .line 1948
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1952
    move-object/from16 v0, p0

    iput v14, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1954
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_measure()Z

    move-result v23

    if-nez v23, :cond_4

    .line 1956
    const/16 v23, 0x0

    .line 2185
    :goto_3
    return v23

    .line 1897
    .end local v14    # "v_2":I
    .end local v16    # "v_3":I
    .end local v17    # "v_4":I
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 1899
    const-string v23, "Y"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 1901
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->B_Y_found:Z

    goto :goto_0

    .line 1924
    .restart local v14    # "v_2":I
    .restart local v16    # "v_3":I
    .restart local v17    # "v_4":I
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 1926
    const/16 v23, 0x1

    const-string/jumbo v24, "y"

    move-object/from16 v0, p0

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 1931
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 1932
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1943
    const-string v23, "Y"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 1945
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->B_Y_found:Z

    goto/16 :goto_1

    .line 1940
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    goto/16 :goto_2

    .line 1959
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1962
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v24, v0

    sub-int v18, v23, v24

    .line 1966
    .local v18, "v_5":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_Step_1()Z

    move-result v23

    if-nez v23, :cond_a

    .line 1973
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    sub-int v23, v23, v18

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1975
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v24, v0

    sub-int v19, v23, v24

    .line 1979
    .local v19, "v_6":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_Step_2()Z

    move-result v23

    if-nez v23, :cond_b

    .line 1986
    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    sub-int v23, v23, v19

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 1988
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v24, v0

    sub-int v20, v23, v24

    .line 1992
    .local v20, "v_7":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_Step_3()Z

    move-result v23

    if-nez v23, :cond_c

    .line 1999
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    sub-int v23, v23, v20

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2001
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v24, v0

    sub-int v21, v23, v24

    .line 2005
    .local v21, "v_8":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_Step_4()Z

    move-result v23

    if-nez v23, :cond_d

    .line 2012
    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    sub-int v23, v23, v21

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2013
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2014
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->B_GE_removed:Z

    .line 2016
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v22, v0

    .line 2020
    .local v22, "v_9":I
    move-object/from16 v0, p0

    iget v5, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2022
    .local v5, "v_10":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_Lose_prefix()Z

    move-result v23

    if-nez v23, :cond_e

    .line 2033
    :cond_5
    :goto_8
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2035
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2038
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v24, v0

    sub-int v6, v23, v24

    .line 2042
    .local v6, "v_11":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->B_GE_removed:Z

    move/from16 v23, v0

    if-nez v23, :cond_f

    .line 2052
    :cond_6
    :goto_9
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    sub-int v23, v23, v6

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2053
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2054
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->B_GE_removed:Z

    .line 2056
    move-object/from16 v0, p0

    iget v7, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2060
    .local v7, "v_12":I
    move-object/from16 v0, p0

    iget v8, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2062
    .local v8, "v_13":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_Lose_infix()Z

    move-result v23

    if-nez v23, :cond_10

    .line 2073
    :cond_7
    :goto_a
    move-object/from16 v0, p0

    iput v7, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2075
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2078
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v24, v0

    sub-int v9, v23, v24

    .line 2082
    .local v9, "v_14":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->B_GE_removed:Z

    move/from16 v23, v0

    if-nez v23, :cond_11

    .line 2092
    :cond_8
    :goto_b
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    sub-int v23, v23, v9

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2093
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2094
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2097
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v24, v0

    sub-int v10, v23, v24

    .line 2101
    .local v10, "v_15":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_Step_7()Z

    move-result v23

    if-nez v23, :cond_12

    .line 2108
    :goto_c
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    sub-int v23, v23, v10

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2110
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v24, v0

    sub-int v11, v23, v24

    .line 2117
    .local v11, "v_16":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->B_stemmed:Z

    move/from16 v23, v0

    if-nez v23, :cond_13

    .line 2124
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->B_GE_removed:Z

    move/from16 v23, v0

    if-nez v23, :cond_13

    .line 2135
    :cond_9
    :goto_d
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v23, v0

    sub-int v23, v23, v11

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2136
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit_backward:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2137
    move-object/from16 v0, p0

    iget v12, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2141
    .local v12, "v_18":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->B_Y_found:Z

    move/from16 v23, v0

    if-nez v23, :cond_15

    .line 2184
    :goto_e
    move-object/from16 v0, p0

    iput v12, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2185
    const/16 v23, 0x1

    goto/16 :goto_3

    .line 1971
    .end local v5    # "v_10":I
    .end local v6    # "v_11":I
    .end local v7    # "v_12":I
    .end local v8    # "v_13":I
    .end local v9    # "v_14":I
    .end local v10    # "v_15":I
    .end local v11    # "v_16":I
    .end local v12    # "v_18":I
    .end local v19    # "v_6":I
    .end local v20    # "v_7":I
    .end local v21    # "v_8":I
    .end local v22    # "v_9":I
    :cond_a
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->B_stemmed:Z

    goto/16 :goto_4

    .line 1984
    .restart local v19    # "v_6":I
    :cond_b
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->B_stemmed:Z

    goto/16 :goto_5

    .line 1997
    .restart local v20    # "v_7":I
    :cond_c
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->B_stemmed:Z

    goto/16 :goto_6

    .line 2010
    .restart local v21    # "v_8":I
    :cond_d
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->B_stemmed:Z

    goto/16 :goto_7

    .line 2026
    .restart local v5    # "v_10":I
    .restart local v22    # "v_9":I
    :cond_e
    move-object/from16 v0, p0

    iput v5, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2028
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_measure()Z

    move-result v23

    if-nez v23, :cond_5

    goto/16 :goto_8

    .line 2047
    .restart local v6    # "v_11":I
    :cond_f
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_Step_1c()Z

    move-result v23

    if-nez v23, :cond_6

    goto/16 :goto_9

    .line 2066
    .restart local v7    # "v_12":I
    .restart local v8    # "v_13":I
    :cond_10
    move-object/from16 v0, p0

    iput v8, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2068
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_measure()Z

    move-result v23

    if-nez v23, :cond_7

    goto/16 :goto_a

    .line 2087
    .restart local v9    # "v_14":I
    :cond_11
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_Step_1c()Z

    move-result v23

    if-nez v23, :cond_8

    goto/16 :goto_b

    .line 2106
    .restart local v10    # "v_15":I
    :cond_12
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->B_stemmed:Z

    goto/16 :goto_c

    .line 2130
    .restart local v11    # "v_16":I
    :cond_13
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/KpStemmer;->r_Step_6()Z

    move-result v23

    if-nez v23, :cond_9

    goto/16 :goto_d

    .line 2165
    .restart local v12    # "v_18":I
    .local v13, "v_19":I
    .local v15, "v_20":I
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->ket:I

    .line 2166
    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2177
    const-string/jumbo v23, "y"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/tartarus/snowball/ext/KpStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 2148
    .end local v13    # "v_19":I
    .end local v15    # "v_20":I
    :cond_15
    move-object/from16 v0, p0

    iget v13, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2154
    .restart local v13    # "v_19":I
    :goto_f
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2158
    .restart local v15    # "v_20":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->bra:I

    .line 2160
    const/16 v23, 0x1

    const-string v24, "Y"

    move-object/from16 v0, p0

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/KpStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_14

    .line 2169
    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    .line 2170
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->limit:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_16

    .line 2180
    move-object/from16 v0, p0

    iput v13, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    goto/16 :goto_e

    .line 2174
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/KpStemmer;->cursor:I

    goto :goto_f
.end method
