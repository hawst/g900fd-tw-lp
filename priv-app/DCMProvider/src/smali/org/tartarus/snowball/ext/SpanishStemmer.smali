.class public Lorg/tartarus/snowball/ext/SpanishStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "SpanishStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final a_6:[Lorg/tartarus/snowball/Among;

.field private static final a_7:[Lorg/tartarus/snowball/Among;

.field private static final a_8:[Lorg/tartarus/snowball/Among;

.field private static final a_9:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_p2:I

.field private I_pV:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/SpanishStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    .line 19
    const/4 v0, 0x6

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ed"

    const/4 v2, 0x0

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f3"

    const/4 v2, 0x0

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00fa"

    const/4 v2, 0x0

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 28
    const/16 v0, 0xd

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 29
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "la"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 30
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sela"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 31
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "le"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "me"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "se"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lo"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "selo"

    const/4 v2, 0x5

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "las"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "selas"

    const/4 v2, 0x7

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "les"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "los"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "selos"

    const/16 v2, 0xa

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nos"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 28
    sput-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 44
    const/16 v0, 0xb

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ando"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iendo"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yendo"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1ndo"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00e9ndo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1r"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9r"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edr"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 44
    sput-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 58
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 60
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ad"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "os"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iv"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 58
    sput-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 65
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "able"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ible"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ante"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 65
    sput-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 71
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abil"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iv"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 71
    sput-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 77
    const/16 v0, 0x2e

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ica"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 79
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ancia"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "encia"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "adora"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osa"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ista"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iva"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 85
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "anza"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "log\u00eda"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "idad"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 88
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "able"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 89
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ible"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 90
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ante"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 91
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mente"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 92
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amente"

    const/16 v2, 0xd

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 93
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aci\u00f3n"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uci\u00f3n"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 95
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ico"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ismo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 97
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oso"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 98
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amiento"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imiento"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivo"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 101
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ador"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icas"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 103
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ancias"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 104
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "encias"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 105
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "adoras"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osas"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "istas"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivas"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 109
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "anzas"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "log\u00edas"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 111
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "idades"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ables"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ibles"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aciones"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 115
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uciones"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x26

    .line 116
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "adores"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x27

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "antes"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x28

    .line 118
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x29

    .line 119
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ismos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2a

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2b

    .line 121
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amientos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2c

    .line 122
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imientos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2d

    .line 123
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivos"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 77
    sput-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    .line 126
    const/16 v0, 0xc

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 127
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "ya"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 128
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "ye"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 129
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yan"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 130
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yen"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 131
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yeron"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 132
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yendo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 133
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yas"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 135
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yes"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 136
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yais"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 137
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yamos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 138
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "y\u00f3"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 126
    sput-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    .line 141
    const/16 v0, 0x60

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aba"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 143
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ada"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ida"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 145
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ara"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 146
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iera"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eda"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 148
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00eda"

    const/4 v2, 0x5

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 149
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00eda"

    const/4 v2, 0x5

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 150
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00eda"

    const/4 v2, 0x5

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 151
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ad"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 152
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ed"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 153
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "id"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 154
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ase"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 155
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iese"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 156
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aste"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 157
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iste"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 158
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "an"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 159
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aban"

    const/16 v2, 0x10

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 160
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aran"

    const/16 v2, 0x10

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 161
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ieran"

    const/16 v2, 0x10

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 162
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edan"

    const/16 v2, 0x10

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 163
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00edan"

    const/16 v2, 0x14

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 164
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00edan"

    const/16 v2, 0x14

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 165
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00edan"

    const/16 v2, 0x14

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 166
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 167
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asen"

    const/16 v2, 0x18

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 168
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iesen"

    const/16 v2, 0x18

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 169
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aron"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 170
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ieron"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 171
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e1n"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 172
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e1n"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 173
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e1n"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 174
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ado"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 175
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ido"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 176
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ando"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 177
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iendo"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 178
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 179
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x26

    .line 180
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x27

    .line 181
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "as"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x28

    .line 182
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abas"

    const/16 v2, 0x27

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x29

    .line 183
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "adas"

    const/16 v2, 0x27

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2a

    .line 184
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "idas"

    const/16 v2, 0x27

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2b

    .line 185
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aras"

    const/16 v2, 0x27

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2c

    .line 186
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ieras"

    const/16 v2, 0x27

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2d

    .line 187
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edas"

    const/16 v2, 0x27

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2e

    .line 188
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00edas"

    const/16 v2, 0x2d

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2f

    .line 189
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00edas"

    const/16 v2, 0x2d

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x30

    .line 190
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00edas"

    const/16 v2, 0x2d

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x31

    .line 191
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "es"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x32

    .line 192
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ases"

    const/16 v2, 0x31

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x33

    .line 193
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ieses"

    const/16 v2, 0x31

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x34

    .line 194
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abais"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x35

    .line 195
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "arais"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x36

    .line 196
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ierais"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x37

    .line 197
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edais"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x38

    .line 198
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00edais"

    const/16 v2, 0x37

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x39

    .line 199
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00edais"

    const/16 v2, 0x37

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3a

    .line 200
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00edais"

    const/16 v2, 0x37

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3b

    .line 201
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aseis"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3c

    .line 202
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ieseis"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3d

    .line 203
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asteis"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3e

    .line 204
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isteis"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3f

    .line 205
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1is"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x40

    .line 206
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9is"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x41

    .line 207
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e9is"

    const/16 v2, 0x40

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x42

    .line 208
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e9is"

    const/16 v2, 0x40

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x43

    .line 209
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e9is"

    const/16 v2, 0x40

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x44

    .line 210
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ados"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x45

    .line 211
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "idos"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x46

    .line 212
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amos"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x47

    .line 213
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1bamos"

    const/16 v2, 0x46

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x48

    .line 214
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1ramos"

    const/16 v2, 0x46

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x49

    .line 215
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00e9ramos"

    const/16 v2, 0x46

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4a

    .line 216
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edamos"

    const/16 v2, 0x46

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4b

    .line 217
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00edamos"

    const/16 v2, 0x4a

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4c

    .line 218
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00edamos"

    const/16 v2, 0x4a

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4d

    .line 219
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00edamos"

    const/16 v2, 0x4a

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4e

    .line 220
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "emos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4f

    .line 221
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aremos"

    const/16 v2, 0x4e

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x50

    .line 222
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eremos"

    const/16 v2, 0x4e

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x51

    .line 223
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iremos"

    const/16 v2, 0x4e

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x52

    .line 224
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1semos"

    const/16 v2, 0x4e

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x53

    .line 225
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00e9semos"

    const/16 v2, 0x4e

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x54

    .line 226
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imos"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x55

    .line 227
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e1s"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x56

    .line 228
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e1s"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x57

    .line 229
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e1s"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x58

    .line 230
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eds"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x59

    .line 231
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e1"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5a

    .line 232
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e1"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5b

    .line 233
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e1"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5c

    .line 234
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e9"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5d

    .line 235
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e9"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5e

    .line 236
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e9"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5f

    .line 237
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00f3"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 141
    sput-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    .line 240
    const/16 v0, 0x8

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 241
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 242
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 243
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "o"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 244
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "os"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 245
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 246
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 247
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ed"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 248
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f3"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 240
    sput-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_9:[Lorg/tartarus/snowball/Among;

    .line 251
    const/16 v0, 0x14

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/4 v2, 0x4

    aput-char v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0xa

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/SpanishStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/SpanishStemmer;

    .prologue
    .line 258
    iget v0, p1, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_p2:I

    .line 259
    iget v0, p1, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_p1:I

    .line 260
    iget v0, p1, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_pV:I

    iput v0, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_pV:I

    .line 261
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 262
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 541
    iget v0, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 543
    const/4 v0, 0x0

    .line 545
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 549
    iget v0, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 551
    const/4 v0, 0x0

    .line 553
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_RV()Z
    .locals 2

    .prologue
    .line 533
    iget v0, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_pV:I

    iget v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 535
    const/4 v0, 0x0

    .line 537
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_attached_pronoun()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 560
    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 562
    sget-object v3, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/16 v4, 0xd

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/SpanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v3

    if-nez v3, :cond_1

    .line 633
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 567
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 569
    sget-object v3, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v4, 0xb

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/SpanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 570
    .local v0, "among_var":I
    if-eqz v0, :cond_0

    .line 575
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_RV()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 579
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v1, v2

    .line 633
    goto :goto_0

    .line 585
    :pswitch_1
    iget v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 587
    const-string v1, "iendo"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 592
    :pswitch_2
    iget v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 594
    const-string v1, "ando"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 599
    :pswitch_3
    iget v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 601
    const-string v1, "ar"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 606
    :pswitch_4
    iget v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 608
    const-string v1, "er"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 613
    :pswitch_5
    iget v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 615
    const-string v1, "ir"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 620
    :pswitch_6
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto :goto_1

    .line 625
    :pswitch_7
    const-string v3, "u"

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/SpanishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 630
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto :goto_1

    .line 579
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 9

    .prologue
    const/16 v8, 0xfc

    const/16 v7, 0x61

    .line 271
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_pV:I

    .line 272
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_p1:I

    .line 273
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_p2:I

    .line 275
    iget v0, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 280
    .local v0, "v_1":I
    iget v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 283
    .local v1, "v_2":I
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_3

    .line 339
    :cond_0
    iput v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 341
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_6

    .line 389
    :cond_1
    :goto_0
    iput v0, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 391
    iget v4, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 398
    .local v4, "v_8":I
    :goto_1
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_c

    .line 404
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    if-lt v5, v6, :cond_a

    .line 463
    :cond_2
    :goto_2
    iput v4, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 464
    const/4 v5, 0x1

    return v5

    .line 289
    .end local v4    # "v_8":I
    :cond_3
    iget v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 292
    .local v2, "v_3":I
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_5

    .line 314
    :goto_3
    iput v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 316
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->in_grouping([CII)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 324
    :goto_4
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 330
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    if-ge v5, v6, :cond_0

    .line 334
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto :goto_4

    .line 310
    :cond_4
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 300
    :cond_5
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 306
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    if-lt v5, v6, :cond_4

    goto :goto_3

    .line 347
    .end local v2    # "v_3":I
    :cond_6
    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 350
    .local v3, "v_6":I
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_9

    .line 372
    :goto_5
    iput v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 374
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->in_grouping([CII)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 379
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    if-ge v5, v6, :cond_1

    .line 383
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 387
    .end local v3    # "v_6":I
    :cond_7
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_pV:I

    goto :goto_0

    .line 368
    .restart local v3    # "v_6":I
    :cond_8
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 358
    :cond_9
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 364
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    if-lt v5, v6, :cond_8

    goto :goto_5

    .line 408
    .end local v3    # "v_6":I
    .restart local v4    # "v_8":I
    :cond_a
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 424
    :cond_b
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 414
    :cond_c
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_d

    .line 420
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    if-lt v5, v6, :cond_b

    goto/16 :goto_2

    .line 427
    :cond_d
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_p1:I

    .line 432
    :goto_6
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_f

    .line 438
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    if-ge v5, v6, :cond_2

    .line 442
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto :goto_6

    .line 458
    :cond_e
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 448
    :cond_f
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_10

    .line 454
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    if-lt v5, v6, :cond_e

    goto/16 :goto_2

    .line 461
    :cond_10
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_p2:I

    goto/16 :goto_2
.end method

.method private r_postlude()Z
    .locals 4

    .prologue
    .line 473
    :goto_0
    iget v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 477
    .local v1, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 479
    sget-object v2, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x6

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/SpanishStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 480
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 526
    :cond_0
    :pswitch_0
    iput v1, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 529
    const/4 v2, 0x1

    return v2

    .line 485
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 486
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 492
    :pswitch_1
    const-string v2, "a"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 497
    :pswitch_2
    const-string v2, "e"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 502
    :pswitch_3
    const-string v2, "i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 507
    :pswitch_4
    const-string v2, "o"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 512
    :pswitch_5
    const-string v2, "u"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 517
    :pswitch_6
    iget v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 521
    iget v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto :goto_0

    .line 486
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private r_residual_suffix()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1033
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 1035
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_9:[Lorg/tartarus/snowball/Among;

    const/16 v6, 0x8

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/SpanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1036
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 1098
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 1041
    :cond_1
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 1042
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v3, v4

    .line 1098
    goto :goto_0

    .line 1048
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_RV()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1053
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto :goto_1

    .line 1058
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_RV()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1063
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    .line 1065
    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v1, v3, v5

    .line 1069
    .local v1, "v_1":I
    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 1071
    const-string v3, "u"

    invoke-virtual {p0, v4, v3}, Lorg/tartarus/snowball/ext/SpanishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1073
    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto :goto_1

    .line 1077
    :cond_2
    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 1079
    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v2, v3, v5

    .line 1081
    .local v2, "v_2":I
    const-string v3, "g"

    invoke-virtual {p0, v4, v3}, Lorg/tartarus/snowball/ext/SpanishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1083
    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto :goto_1

    .line 1086
    :cond_3
    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v3, v2

    iput v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1088
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_RV()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1090
    iget v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto :goto_1

    .line 1094
    :cond_4
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto :goto_1

    .line 1042
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_standard_suffix()Z
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v6, 0x0

    .line 645
    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 647
    sget-object v7, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    const/16 v8, 0x2e

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 648
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 908
    :cond_0
    :goto_0
    :pswitch_0
    return v6

    .line 653
    :cond_1
    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 654
    packed-switch v0, :pswitch_data_0

    .line 908
    :goto_1
    const/4 v6, 0x1

    goto :goto_0

    .line 660
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 665
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto :goto_1

    .line 670
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 675
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    .line 677
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 681
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 683
    const-string v6, "ic"

    invoke-virtual {p0, v9, v6}, Lorg/tartarus/snowball/ext/SpanishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 685
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto :goto_1

    .line 689
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 691
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v6

    if-nez v6, :cond_3

    .line 693
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto :goto_1

    .line 697
    :cond_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto :goto_1

    .line 703
    .end local v1    # "v_1":I
    :pswitch_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 708
    const-string v6, "log"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 713
    :pswitch_4
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 718
    const-string v6, "u"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 723
    :pswitch_5
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 728
    const-string v6, "ente"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 733
    :pswitch_6
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R1()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 738
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    .line 740
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 744
    .local v2, "v_2":I
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 746
    sget-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/4 v7, 0x4

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/SpanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 747
    if-nez v0, :cond_4

    .line 749
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 753
    :cond_4
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 755
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v6

    if-nez v6, :cond_5

    .line 757
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 761
    :cond_5
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    .line 762
    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    .line 764
    :pswitch_7
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 769
    :pswitch_8
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 771
    const-string v6, "at"

    invoke-virtual {p0, v9, v6}, Lorg/tartarus/snowball/ext/SpanishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 773
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 777
    :cond_6
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 779
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v6

    if-nez v6, :cond_7

    .line 781
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 785
    :cond_7
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto/16 :goto_1

    .line 793
    .end local v2    # "v_2":I
    :pswitch_9
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 798
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    .line 800
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 804
    .local v3, "v_3":I
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 806
    sget-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    invoke-virtual {p0, v6, v10}, Lorg/tartarus/snowball/ext/SpanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 807
    if-nez v0, :cond_8

    .line 809
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 813
    :cond_8
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 814
    packed-switch v0, :pswitch_data_2

    goto/16 :goto_1

    .line 816
    :pswitch_a
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 821
    :pswitch_b
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v6

    if-nez v6, :cond_9

    .line 823
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 827
    :cond_9
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto/16 :goto_1

    .line 835
    .end local v3    # "v_3":I
    :pswitch_c
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 840
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    .line 842
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v4, v6, v7

    .line 846
    .local v4, "v_4":I
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 848
    sget-object v6, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    invoke-virtual {p0, v6, v10}, Lorg/tartarus/snowball/ext/SpanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 849
    if-nez v0, :cond_a

    .line 851
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v4

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 855
    :cond_a
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 856
    packed-switch v0, :pswitch_data_3

    goto/16 :goto_1

    .line 858
    :pswitch_d
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v4

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 863
    :pswitch_e
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v6

    if-nez v6, :cond_b

    .line 865
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v4

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 869
    :cond_b
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto/16 :goto_1

    .line 877
    .end local v4    # "v_4":I
    :pswitch_f
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 882
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    .line 884
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v5, v6, v7

    .line 888
    .local v5, "v_5":I
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 890
    const-string v6, "at"

    invoke-virtual {p0, v9, v6}, Lorg/tartarus/snowball/ext/SpanishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_c

    .line 892
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v5

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 896
    :cond_c
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 898
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_R2()Z

    move-result v6

    if-nez v6, :cond_d

    .line 900
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v5

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto/16 :goto_1

    .line 904
    :cond_d
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto/16 :goto_1

    .line 654
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
    .end packed-switch

    .line 762
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 814
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 856
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method private r_verb_suffix()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 965
    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v1, v7, v8

    .line 967
    .local v1, "v_1":I
    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iget v8, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_pV:I

    if-ge v7, v8, :cond_0

    .line 1024
    :goto_0
    :pswitch_0
    return v5

    .line 971
    :cond_0
    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_pV:I

    iput v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 972
    iget v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit_backward:I

    .line 973
    .local v2, "v_2":I
    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit_backward:I

    .line 974
    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v7, v1

    iput v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 977
    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 979
    sget-object v7, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    const/16 v8, 0x60

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/SpanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 980
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 982
    iput v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit_backward:I

    goto :goto_0

    .line 986
    :cond_1
    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 987
    iput v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit_backward:I

    .line 988
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v5, v6

    .line 1024
    goto :goto_0

    .line 994
    :pswitch_1
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v3, v5, v7

    .line 998
    .local v3, "v_3":I
    const-string v5, "u"

    invoke-virtual {p0, v6, v5}, Lorg/tartarus/snowball/ext/SpanishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1000
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1014
    :goto_2
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 1016
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto :goto_1

    .line 1004
    :cond_2
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v4, v5, v7

    .line 1006
    .local v4, "v_4":I
    const-string v5, "g"

    invoke-virtual {p0, v6, v5}, Lorg/tartarus/snowball/ext/SpanishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1008
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto :goto_2

    .line 1011
    :cond_3
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    goto :goto_2

    .line 1021
    .end local v3    # "v_3":I
    .end local v4    # "v_4":I
    :pswitch_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto :goto_1

    .line 988
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_y_verb_suffix()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 917
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 919
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_pV:I

    if-ge v5, v6, :cond_1

    .line 954
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 923
    :cond_1
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->I_pV:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 924
    iget v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit_backward:I

    .line 925
    .local v2, "v_2":I
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit_backward:I

    .line 926
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 929
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->ket:I

    .line 931
    sget-object v5, Lorg/tartarus/snowball/ext/SpanishStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    const/16 v6, 0xc

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/SpanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 932
    .local v0, "among_var":I
    if-nez v0, :cond_2

    .line 934
    iput v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit_backward:I

    goto :goto_0

    .line 938
    :cond_2
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->bra:I

    .line 939
    iput v2, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit_backward:I

    .line 940
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v3, v4

    .line 954
    goto :goto_0

    .line 946
    :pswitch_1
    const-string v5, "u"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/SpanishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 951
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->slice_del()V

    goto :goto_1

    .line 940
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1191
    instance-of v0, p1, Lorg/tartarus/snowball/ext/SpanishStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1196
    const-class v0, Lorg/tartarus/snowball/ext/SpanishStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 8

    .prologue
    .line 1111
    iget v0, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1114
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_mark_regions()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1119
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1121
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit_backward:I

    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1124
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 1127
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_attached_pronoun()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1132
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1134
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 1139
    .local v2, "v_3":I
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 1142
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_standard_suffix()Z

    move-result v6

    if-nez v6, :cond_2

    .line 1148
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1151
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_y_verb_suffix()Z

    move-result v6

    if-nez v6, :cond_2

    .line 1157
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1159
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_verb_suffix()Z

    move-result v6

    if-nez v6, :cond_2

    .line 1165
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1167
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    sub-int v4, v6, v7

    .line 1170
    .local v4, "v_5":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_residual_suffix()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1175
    :cond_3
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit:I

    sub-int/2addr v6, v4

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1176
    iget v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->limit_backward:I

    iput v6, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1177
    iget v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1180
    .local v5, "v_6":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SpanishStemmer;->r_postlude()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1185
    :cond_4
    iput v5, p0, Lorg/tartarus/snowball/ext/SpanishStemmer;->cursor:I

    .line 1186
    const/4 v6, 0x1

    return v6
.end method
