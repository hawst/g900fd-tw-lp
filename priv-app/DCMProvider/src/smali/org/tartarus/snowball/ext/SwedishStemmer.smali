.class public Lorg/tartarus/snowball/ext/SwedishStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "SwedishStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final g_s_ending:[C

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_x:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x6

    const/4 v14, 0x2

    const/4 v6, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/SwedishStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    .line 19
    const/16 v0, 0x25

    new-array v13, v0, [Lorg/tartarus/snowball/Among;

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v6

    .line 21
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "arna"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v13, v3

    .line 22
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "erna"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v13, v14

    const/4 v0, 0x3

    .line 23
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "heterna"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v14

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/4 v0, 0x4

    .line 24
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "orna"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v13, v0

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ad"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v15

    const/4 v0, 0x7

    .line 27
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ade"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v15

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x8

    .line 28
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ande"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v15

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x9

    .line 29
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "arne"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v15

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0xa

    .line 30
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "are"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v15

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0xb

    .line 31
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "aste"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v15

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v7, 0xc

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    const/16 v0, 0xd

    .line 33
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "anden"

    const/16 v9, 0xc

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0xe

    .line 34
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "aren"

    const/16 v9, 0xc

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0xf

    .line 35
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "heten"

    const/16 v9, 0xc

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v7, 0x10

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ern"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    const/16 v7, 0x11

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    const/16 v7, 0x12

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    const/16 v0, 0x13

    .line 39
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "heter"

    const/16 v9, 0x12

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v7, 0x14

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "or"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    const/16 v0, 0x15

    .line 41
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "s"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v2

    move v10, v14

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x16

    .line 42
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "as"

    const/16 v9, 0x15

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x17

    .line 43
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "arnas"

    const/16 v9, 0x16

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x18

    .line 44
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ernas"

    const/16 v9, 0x16

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x19

    .line 45
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ornas"

    const/16 v9, 0x16

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x1a

    .line 46
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "es"

    const/16 v9, 0x15

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x1b

    .line 47
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ades"

    const/16 v9, 0x1a

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x1c

    .line 48
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "andes"

    const/16 v9, 0x1a

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x1d

    .line 49
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ens"

    const/16 v9, 0x15

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x1e

    .line 50
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "arens"

    const/16 v9, 0x1d

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x1f

    .line 51
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "hetens"

    const/16 v9, 0x1d

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x20

    .line 52
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "erns"

    const/16 v9, 0x15

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v7, 0x21

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "at"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    const/16 v7, 0x22

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "andet"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    const/16 v7, 0x23

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "het"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    const/16 v7, 0x24

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ast"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    .line 19
    sput-object v13, Lorg/tartarus/snowball/ext/SwedishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 59
    const/4 v0, 0x7

    new-array v0, v0, [Lorg/tartarus/snowball/Among;

    .line 60
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "dd"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v2

    move v10, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v0, v6

    .line 61
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "gd"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v2

    move v10, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v0, v3

    .line 62
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "nn"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v2

    move v10, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v0, v14

    const/4 v1, 0x3

    .line 63
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "dt"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v2

    move v10, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v0, v1

    const/4 v1, 0x4

    .line 64
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "gt"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v2

    move v10, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v0, v1

    const/4 v1, 0x5

    .line 65
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "kt"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v2

    move v10, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v0, v1

    .line 66
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "tt"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v2

    move v10, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v0, v15

    .line 59
    sput-object v0, Lorg/tartarus/snowball/ext/SwedishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 69
    const/4 v0, 0x5

    new-array v13, v0, [Lorg/tartarus/snowball/Among;

    .line 70
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ig"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v6

    .line 71
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "lig"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v13, v3

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "els"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v14

    const/4 v0, 0x3

    .line 73
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "fullt"

    const/4 v10, 0x3

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/4 v0, 0x4

    .line 74
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "l\u00f6st"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/SwedishStemmer;->methodObject:Lorg/tartarus/snowball/ext/SwedishStemmer;

    move v9, v2

    move v10, v14

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    .line 69
    sput-object v13, Lorg/tartarus/snowball/ext/SwedishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 77
    const/16 v0, 0x13

    new-array v0, v0, [C

    const/16 v1, 0x11

    aput-char v1, v0, v6

    const/16 v1, 0x41

    aput-char v1, v0, v3

    const/16 v1, 0x10

    aput-char v1, v0, v14

    const/4 v1, 0x3

    aput-char v3, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x18

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x20

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/SwedishStemmer;->g_v:[C

    .line 79
    const/4 v0, 0x3

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/tartarus/snowball/ext/SwedishStemmer;->g_s_ending:[C

    return-void

    nop

    :array_0
    .array-data 2
        0x77s
        0x7fs
        0x95s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/SwedishStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/SwedishStemmer;

    .prologue
    .line 85
    iget v0, p1, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_x:I

    iput v0, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_x:I

    .line 86
    iget v0, p1, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    .line 87
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 88
    return-void
.end method

.method private r_consonant_pair()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 214
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    sub-int v0, v4, v5

    .line 216
    .local v0, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    if-ge v4, v5, :cond_0

    .line 249
    :goto_0
    return v3

    .line 220
    :cond_0
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 221
    iget v1, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    .line 222
    .local v1, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    .line 223
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    sub-int/2addr v4, v0

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 226
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    sub-int v2, v4, v5

    .line 228
    .local v2, "v_3":I
    sget-object v4, Lorg/tartarus/snowball/ext/SwedishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v5, 0x7

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/SwedishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v4

    if-nez v4, :cond_1

    .line 230
    iput v1, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    goto :goto_0

    .line 233
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 236
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->ket:I

    .line 238
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    if-gt v4, v5, :cond_2

    .line 240
    iput v1, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    goto :goto_0

    .line 243
    :cond_2
    iget v3, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 245
    iget v3, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->bra:I

    .line 247
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SwedishStemmer;->slice_del()V

    .line 248
    iput v1, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    .line 249
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private r_main_suffix()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 165
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 167
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    if-ge v4, v5, :cond_1

    .line 206
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 171
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 172
    iget v2, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    .line 173
    .local v2, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    .line 174
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 177
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->ket:I

    .line 179
    sget-object v4, Lorg/tartarus/snowball/ext/SwedishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x25

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/SwedishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 180
    .local v0, "among_var":I
    if-nez v0, :cond_2

    .line 182
    iput v2, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    goto :goto_0

    .line 186
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->bra:I

    .line 187
    iput v2, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    .line 188
    packed-switch v0, :pswitch_data_0

    .line 206
    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    .line 194
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SwedishStemmer;->slice_del()V

    goto :goto_1

    .line 198
    :pswitch_2
    sget-object v4, Lorg/tartarus/snowball/ext/SwedishStemmer;->g_s_ending:[C

    const/16 v5, 0x62

    const/16 v6, 0x79

    invoke-virtual {p0, v4, v5, v6}, Lorg/tartarus/snowball/ext/SwedishStemmer;->in_grouping_b([CII)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 203
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SwedishStemmer;->slice_del()V

    goto :goto_1

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 8

    .prologue
    const/16 v7, 0xf6

    const/16 v6, 0x61

    const/4 v3, 0x0

    .line 94
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    .line 96
    iget v1, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 100
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    add-int/lit8 v0, v4, 0x3

    .line 101
    .local v0, "c":I
    if-ltz v0, :cond_0

    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    if-le v0, v4, :cond_1

    .line 156
    :cond_0
    :goto_0
    return v3

    .line 105
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 108
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_x:I

    .line 109
    iput v1, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 113
    :goto_1
    iget v2, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 115
    .local v2, "v_2":I
    sget-object v4, Lorg/tartarus/snowball/ext/SwedishStemmer;->g_v:[C

    invoke-virtual {p0, v4, v6, v7}, Lorg/tartarus/snowball/ext/SwedishStemmer;->in_grouping([CII)Z

    move-result v4

    if-nez v4, :cond_2

    .line 122
    iput v2, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 123
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    if-ge v4, v5, :cond_0

    .line 127
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    goto :goto_1

    .line 119
    :cond_2
    iput v2, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 133
    :goto_2
    sget-object v4, Lorg/tartarus/snowball/ext/SwedishStemmer;->g_v:[C

    invoke-virtual {p0, v4, v6, v7}, Lorg/tartarus/snowball/ext/SwedishStemmer;->out_grouping([CII)Z

    move-result v4

    if-nez v4, :cond_3

    .line 139
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    if-ge v4, v5, :cond_0

    .line 143
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    goto :goto_2

    .line 146
    :cond_3
    iget v3, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    .line 150
    iget v3, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_x:I

    if-lt v3, v4, :cond_4

    .line 156
    :goto_3
    const/4 v3, 0x1

    goto :goto_0

    .line 154
    :cond_4
    iget v3, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_x:I

    iput v3, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    goto :goto_3
.end method

.method private r_other_suffix()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 257
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 259
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    if-ge v4, v5, :cond_0

    .line 300
    :goto_0
    return v3

    .line 263
    :cond_0
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->I_p1:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 264
    iget v2, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    .line 265
    .local v2, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    .line 266
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 269
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->ket:I

    .line 271
    sget-object v4, Lorg/tartarus/snowball/ext/SwedishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/4 v5, 0x5

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/SwedishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 272
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 274
    iput v2, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    goto :goto_0

    .line 278
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->bra:I

    .line 279
    packed-switch v0, :pswitch_data_0

    .line 299
    :goto_1
    iput v2, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    .line 300
    const/4 v3, 0x1

    goto :goto_0

    .line 281
    :pswitch_0
    iput v2, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    goto :goto_0

    .line 286
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/SwedishStemmer;->slice_del()V

    goto :goto_1

    .line 291
    :pswitch_2
    const-string v3, "l\u00f6s"

    invoke-virtual {p0, v3}, Lorg/tartarus/snowball/ext/SwedishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 296
    :pswitch_3
    const-string v3, "full"

    invoke-virtual {p0, v3}, Lorg/tartarus/snowball/ext/SwedishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 279
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 358
    instance-of v0, p1, Lorg/tartarus/snowball/ext/SwedishStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 363
    const-class v0, Lorg/tartarus/snowball/ext/SwedishStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 6

    .prologue
    .line 311
    iget v0, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 314
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SwedishStemmer;->r_mark_regions()Z

    move-result v4

    if-nez v4, :cond_0

    .line 319
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 321
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 324
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 327
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SwedishStemmer;->r_main_suffix()Z

    move-result v4

    if-nez v4, :cond_1

    .line 332
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 334
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    sub-int v2, v4, v5

    .line 337
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SwedishStemmer;->r_consonant_pair()Z

    move-result v4

    if-nez v4, :cond_2

    .line 342
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 344
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    sub-int v3, v4, v5

    .line 347
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/SwedishStemmer;->r_other_suffix()Z

    move-result v4

    if-nez v4, :cond_3

    .line 352
    :cond_3
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    .line 353
    iget v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->limit_backward:I

    iput v4, p0, Lorg/tartarus/snowball/ext/SwedishStemmer;->cursor:I

    const/4 v4, 0x1

    return v4
.end method
