.class public Lorg/tartarus/snowball/ext/IrishStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "IrishStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_p2:I

.field private I_pV:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x5

    const/4 v3, 0x4

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, -0x1

    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/IrishStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/IrishStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    .line 19
    const/16 v0, 0x18

    new-array v10, v0, [Lorg/tartarus/snowball/Among;

    const/4 v6, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "b\'"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 21
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "bh"

    const/16 v7, 0xe

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v11

    .line 22
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "bhf"

    const/16 v7, 0x9

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v12

    const/4 v0, 0x3

    .line 23
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "bp"

    const/16 v7, 0xb

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 24
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ch"

    const/16 v7, 0xf

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v3

    .line 25
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "d\'"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v13

    const/4 v0, 0x6

    .line 26
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "d\'fh"

    const/4 v7, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v13

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x7

    .line 27
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "dh"

    const/16 v7, 0x10

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x8

    .line 28
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "dt"

    const/16 v7, 0xd

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x9

    .line 29
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "fh"

    const/16 v7, 0x11

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xa

    .line 30
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gc"

    const/4 v7, 0x7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xb

    .line 31
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gh"

    const/16 v7, 0x12

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xc

    .line 32
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "h-"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xd

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "m\'"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xe

    .line 34
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "mb"

    const/4 v7, 0x6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xf

    .line 35
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "mh"

    const/16 v7, 0x13

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x10

    .line 36
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "n-"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x11

    .line 37
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "nd"

    const/16 v7, 0x8

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x12

    .line 38
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ng"

    const/16 v7, 0xa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x13

    .line 39
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ph"

    const/16 v7, 0x14

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x14

    .line 40
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "sh"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v13

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x15

    .line 41
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "t-"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x16

    .line 42
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "th"

    const/16 v7, 0x15

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x17

    .line 43
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ts"

    const/16 v7, 0xc

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 19
    sput-object v10, Lorg/tartarus/snowball/ext/IrishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 46
    const/16 v0, 0x10

    new-array v0, v0, [Lorg/tartarus/snowball/Among;

    const/4 v1, 0x0

    .line 47
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u00edochta"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    .line 48
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "a\u00edochta"

    const/4 v6, 0x0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v11

    .line 49
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ire"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v12

    const/4 v1, 0x3

    .line 50
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "aire"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v12

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    .line 51
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "abh"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v3

    .line 52
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eabh"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v3

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v13

    const/4 v1, 0x6

    .line 53
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ibh"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/4 v1, 0x7

    .line 54
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "aibh"

    const/4 v6, 0x6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0x8

    .line 55
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "amh"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0x9

    .line 56
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eamh"

    const/16 v6, 0x8

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0xa

    .line 57
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "imh"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0xb

    .line 58
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "aimh"

    const/16 v6, 0xa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0xc

    .line 59
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u00edocht"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0xd

    .line 60
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "a\u00edocht"

    const/16 v6, 0xc

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0xe

    .line 61
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ir\u00ed"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0xf

    .line 62
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "air\u00ed"

    const/16 v6, 0xe

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    .line 46
    sput-object v0, Lorg/tartarus/snowball/ext/IrishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 65
    const/16 v0, 0x19

    new-array v10, v0, [Lorg/tartarus/snowball/Among;

    const/4 v0, 0x0

    .line 66
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u00f3ideacha"

    const/4 v7, 0x6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 67
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "patacha"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v13

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v11

    .line 68
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "achta"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v12

    const/4 v0, 0x3

    .line 69
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "arcachta"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v12

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 70
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eachta"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v12

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v3

    .line 71
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "grafa\u00edochta"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v13

    const/4 v0, 0x6

    .line 72
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "paite"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v13

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x7

    .line 73
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ach"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x8

    .line 74
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "each"

    const/4 v6, 0x7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x9

    .line 75
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u00f3ideach"

    const/16 v6, 0x8

    const/4 v7, 0x6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xa

    .line 76
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gineach"

    const/16 v6, 0x8

    const/4 v7, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xb

    .line 77
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "patach"

    const/4 v6, 0x7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v13

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xc

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "grafa\u00edoch"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xd

    .line 79
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "pataigh"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v13

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xe

    .line 80
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u00f3idigh"

    const/4 v7, 0x6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xf

    .line 81
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "acht\u00fail"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x10

    .line 82
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eacht\u00fail"

    const/16 v6, 0xf

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x11

    .line 83
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gineas"

    const/4 v7, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x12

    .line 84
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ginis"

    const/4 v7, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x13

    .line 85
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "acht"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x14

    .line 86
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "arcacht"

    const/16 v6, 0x13

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x15

    .line 87
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eacht"

    const/16 v6, 0x13

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x16

    .line 88
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "grafa\u00edocht"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x17

    .line 89
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "arcachta\u00ed"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x18

    .line 90
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "grafa\u00edochta\u00ed"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 65
    sput-object v10, Lorg/tartarus/snowball/ext/IrishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 93
    const/16 v0, 0xc

    new-array v0, v0, [Lorg/tartarus/snowball/Among;

    const/4 v1, 0x0

    .line 94
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "imid"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    .line 95
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "aimid"

    const/4 v6, 0x0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v11

    .line 96
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u00edmid"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v12

    const/4 v1, 0x3

    .line 97
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "a\u00edmid"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v12

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    .line 98
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "adh"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v3

    .line 99
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eadh"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v3

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v13

    const/4 v1, 0x6

    .line 100
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "faidh"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/4 v1, 0x7

    .line 101
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "fidh"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0x8

    .line 102
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u00e1il"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0x9

    .line 103
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ain"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0xa

    .line 104
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tear"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    const/16 v1, 0xb

    .line 105
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tar"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/IrishStemmer;->methodObject:Lorg/tartarus/snowball/ext/IrishStemmer;

    move v6, v2

    move v7, v12

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    .line 93
    sput-object v0, Lorg/tartarus/snowball/ext/IrishStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 108
    const/16 v0, 0x14

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/16 v1, 0x41

    aput-char v1, v0, v11

    const/16 v1, 0x10

    aput-char v1, v0, v12

    const/16 v1, 0x10

    aput-char v11, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/16 v1, 0x12

    aput-char v3, v0, v1

    const/16 v1, 0x13

    aput-char v12, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/IrishStemmer;->g_v:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/IrishStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/IrishStemmer;

    .prologue
    .line 115
    iget v0, p1, Lorg/tartarus/snowball/ext/IrishStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_p2:I

    .line 116
    iget v0, p1, Lorg/tartarus/snowball/ext/IrishStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_p1:I

    .line 117
    iget v0, p1, Lorg/tartarus/snowball/ext/IrishStemmer;->I_pV:I

    iput v0, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_pV:I

    .line 118
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 119
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 363
    iget v0, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 365
    const/4 v0, 0x0

    .line 367
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 371
    iget v0, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 373
    const/4 v0, 0x0

    .line 375
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_RV()Z
    .locals 2

    .prologue
    .line 355
    iget v0, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_pV:I

    iget v1, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 357
    const/4 v0, 0x0

    .line 359
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_deriv()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 422
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->ket:I

    .line 424
    sget-object v2, Lorg/tartarus/snowball/ext/IrishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x19

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/IrishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 425
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 470
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 430
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->bra:I

    .line 431
    packed-switch v0, :pswitch_data_0

    .line 470
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 437
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 442
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_del()V

    goto :goto_1

    .line 447
    :pswitch_2
    const-string v1, "arc"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 452
    :pswitch_3
    const-string v1, "gin"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 457
    :pswitch_4
    const-string v1, "graf"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 462
    :pswitch_5
    const-string v1, "paite"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 467
    :pswitch_6
    const-string/jumbo v1, "\u00f3id"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 431
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private r_initial_morph()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 233
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->bra:I

    .line 235
    sget-object v2, Lorg/tartarus/snowball/ext/IrishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x18

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/IrishStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 236
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 351
    :goto_0
    :pswitch_0
    return v1

    .line 241
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->ket:I

    .line 242
    packed-switch v0, :pswitch_data_0

    .line 351
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 248
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_del()V

    goto :goto_1

    .line 253
    :pswitch_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_del()V

    goto :goto_1

    .line 258
    :pswitch_3
    const-string v1, "f"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 263
    :pswitch_4
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_del()V

    goto :goto_1

    .line 268
    :pswitch_5
    const-string v1, "s"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 273
    :pswitch_6
    const-string v1, "b"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 278
    :pswitch_7
    const-string v1, "c"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 283
    :pswitch_8
    const-string v1, "d"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 288
    :pswitch_9
    const-string v1, "f"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 293
    :pswitch_a
    const-string v1, "g"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 298
    :pswitch_b
    const-string v1, "p"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 303
    :pswitch_c
    const-string v1, "s"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 308
    :pswitch_d
    const-string v1, "t"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 313
    :pswitch_e
    const-string v1, "b"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 318
    :pswitch_f
    const-string v1, "c"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 323
    :pswitch_10
    const-string v1, "d"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 328
    :pswitch_11
    const-string v1, "f"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 333
    :pswitch_12
    const-string v1, "g"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 338
    :pswitch_13
    const-string v1, "m"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 343
    :pswitch_14
    const-string v1, "p"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 348
    :pswitch_15
    const-string v1, "t"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 6

    .prologue
    const/16 v5, 0xfa

    const/16 v4, 0x61

    .line 125
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_pV:I

    .line 126
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_p1:I

    .line 127
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_p2:I

    .line 129
    iget v0, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 136
    .local v0, "v_1":I
    :goto_0
    sget-object v2, Lorg/tartarus/snowball/ext/IrishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v4, v5}, Lorg/tartarus/snowball/ext/IrishStemmer;->in_grouping([CII)Z

    move-result v2

    if-nez v2, :cond_2

    .line 142
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    if-lt v2, v3, :cond_1

    .line 151
    :goto_1
    iput v0, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 153
    iget v1, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 160
    .local v1, "v_3":I
    :goto_2
    sget-object v2, Lorg/tartarus/snowball/ext/IrishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v4, v5}, Lorg/tartarus/snowball/ext/IrishStemmer;->in_grouping([CII)Z

    move-result v2

    if-nez v2, :cond_5

    .line 166
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    if-lt v2, v3, :cond_3

    .line 225
    :cond_0
    :goto_3
    iput v1, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 226
    const/4 v2, 0x1

    return v2

    .line 146
    .end local v1    # "v_3":I
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    goto :goto_0

    .line 149
    :cond_2
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_pV:I

    goto :goto_1

    .line 170
    .restart local v1    # "v_3":I
    :cond_3
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    goto :goto_2

    .line 186
    :cond_4
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 176
    :cond_5
    sget-object v2, Lorg/tartarus/snowball/ext/IrishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v4, v5}, Lorg/tartarus/snowball/ext/IrishStemmer;->out_grouping([CII)Z

    move-result v2

    if-nez v2, :cond_6

    .line 182
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    if-lt v2, v3, :cond_4

    goto :goto_3

    .line 189
    :cond_6
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_p1:I

    .line 194
    :goto_4
    sget-object v2, Lorg/tartarus/snowball/ext/IrishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v4, v5}, Lorg/tartarus/snowball/ext/IrishStemmer;->in_grouping([CII)Z

    move-result v2

    if-nez v2, :cond_8

    .line 200
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 204
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    goto :goto_4

    .line 220
    :cond_7
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 210
    :cond_8
    sget-object v2, Lorg/tartarus/snowball/ext/IrishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v4, v5}, Lorg/tartarus/snowball/ext/IrishStemmer;->out_grouping([CII)Z

    move-result v2

    if-nez v2, :cond_9

    .line 216
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    if-lt v2, v3, :cond_7

    goto :goto_3

    .line 223
    :cond_9
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->I_p2:I

    goto :goto_3
.end method

.method private r_noun_sfx()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 382
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->ket:I

    .line 384
    sget-object v2, Lorg/tartarus/snowball/ext/IrishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x10

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/IrishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 385
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 415
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 390
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->bra:I

    .line 391
    packed-switch v0, :pswitch_data_0

    .line 415
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 397
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 402
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_del()V

    goto :goto_1

    .line 407
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 412
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_del()V

    goto :goto_1

    .line 391
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_verb_sfx()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 477
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->ket:I

    .line 479
    sget-object v2, Lorg/tartarus/snowball/ext/IrishStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0xc

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/IrishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 480
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 510
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 485
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->bra:I

    .line 486
    packed-switch v0, :pswitch_data_0

    .line 510
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 492
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->r_RV()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 497
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_del()V

    goto :goto_1

    .line 502
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 507
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->slice_del()V

    goto :goto_1

    .line 486
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 579
    instance-of v0, p1, Lorg/tartarus/snowball/ext/IrishStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 584
    const-class v0, Lorg/tartarus/snowball/ext/IrishStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 7

    .prologue
    .line 522
    iget v0, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 525
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->r_initial_morph()Z

    move-result v5

    if-nez v5, :cond_0

    .line 530
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 532
    iget v1, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 535
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->r_mark_regions()Z

    move-result v5

    if-nez v5, :cond_1

    .line 540
    :cond_1
    iput v1, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 542
    iget v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit_backward:I

    iget v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 545
    iget v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 548
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->r_noun_sfx()Z

    move-result v5

    if-nez v5, :cond_2

    .line 553
    :cond_2
    iget v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 555
    iget v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    sub-int v3, v5, v6

    .line 558
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->r_deriv()Z

    move-result v5

    if-nez v5, :cond_3

    .line 563
    :cond_3
    iget v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 565
    iget v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    sub-int v4, v5, v6

    .line 568
    .local v4, "v_5":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/IrishStemmer;->r_verb_sfx()Z

    move-result v5

    if-nez v5, :cond_4

    .line 573
    :cond_4
    iget v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    .line 574
    iget v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->limit_backward:I

    iput v5, p0, Lorg/tartarus/snowball/ext/IrishStemmer;->cursor:I

    const/4 v5, 0x1

    return v5
.end method
