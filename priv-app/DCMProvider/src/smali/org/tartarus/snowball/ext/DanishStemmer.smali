.class public Lorg/tartarus/snowball/ext/DanishStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "DanishStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final g_s_ending:[C

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_x:I

.field private S_ch:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v6, 0x0

    const/16 v15, 0x10

    const/4 v14, 0x3

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/DanishStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/DanishStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    .line 19
    const/16 v0, 0x20

    new-array v13, v0, [Lorg/tartarus/snowball/Among;

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "hed"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v6

    .line 21
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ethed"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v13, v3

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ered"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v14

    const/4 v0, 0x4

    .line 24
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "erede"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v14

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/4 v0, 0x5

    .line 25
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ende"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v14

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/4 v0, 0x6

    .line 26
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "erende"

    const/4 v9, 0x5

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/4 v0, 0x7

    .line 27
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ene"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v14

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x8

    .line 28
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "erne"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v14

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x9

    .line 29
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ere"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v14

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v7, 0xa

    .line 30
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    const/16 v0, 0xb

    .line 31
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "heden"

    const/16 v9, 0xa

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0xc

    .line 32
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "eren"

    const/16 v9, 0xa

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v7, 0xd

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    const/16 v0, 0xe

    .line 34
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "heder"

    const/16 v9, 0xd

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0xf

    .line 35
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "erer"

    const/16 v9, 0xd

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    .line 36
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "s"

    const/4 v10, 0x2

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v15

    const/16 v0, 0x11

    .line 37
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "heds"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v15

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x12

    .line 38
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "es"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v15

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x13

    .line 39
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "endes"

    const/16 v9, 0x12

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x14

    .line 40
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "erendes"

    const/16 v9, 0x13

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x15

    .line 41
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "enes"

    const/16 v9, 0x12

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x16

    .line 42
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ernes"

    const/16 v9, 0x12

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x17

    .line 43
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "eres"

    const/16 v9, 0x12

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x18

    .line 44
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ens"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v15

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x19

    .line 45
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "hedens"

    const/16 v9, 0x18

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x1a

    .line 46
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "erens"

    const/16 v9, 0x18

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x1b

    .line 47
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ers"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v15

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x1c

    .line 48
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "ets"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v15

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v0, 0x1d

    .line 49
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "erets"

    const/16 v9, 0x1c

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    const/16 v7, 0x1e

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "et"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v7

    const/16 v0, 0x1f

    .line 51
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "eret"

    const/16 v9, 0x1e

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    .line 19
    sput-object v13, Lorg/tartarus/snowball/ext/DanishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 54
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/tartarus/snowball/Among;

    .line 55
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "gd"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v2

    move v10, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v0, v6

    .line 56
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "dt"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v2

    move v10, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v0, v3

    const/4 v1, 0x2

    .line 57
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "gt"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v2

    move v10, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v0, v1

    .line 58
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "kt"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v2

    move v10, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v0, v14

    .line 54
    sput-object v0, Lorg/tartarus/snowball/ext/DanishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 61
    const/4 v0, 0x5

    new-array v13, v0, [Lorg/tartarus/snowball/Among;

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ig"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v6

    .line 63
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "lig"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v13, v3

    const/4 v0, 0x2

    .line 64
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "elig"

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v3

    move v10, v3

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "els"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v13, v14

    const/4 v0, 0x4

    .line 66
    new-instance v7, Lorg/tartarus/snowball/Among;

    const-string v8, "l\u00f8st"

    const/4 v10, 0x2

    const-string v11, ""

    sget-object v12, Lorg/tartarus/snowball/ext/DanishStemmer;->methodObject:Lorg/tartarus/snowball/ext/DanishStemmer;

    move v9, v2

    invoke-direct/range {v7 .. v12}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v7, v13, v0

    .line 61
    sput-object v13, Lorg/tartarus/snowball/ext/DanishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 69
    const/16 v0, 0x13

    new-array v0, v0, [C

    const/16 v1, 0x11

    aput-char v1, v0, v6

    const/16 v1, 0x41

    aput-char v1, v0, v3

    const/4 v1, 0x2

    aput-char v15, v0, v1

    aput-char v3, v0, v14

    const/16 v1, 0x30

    aput-char v1, v0, v15

    const/16 v1, 0x12

    const/16 v2, 0x80

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/DanishStemmer;->g_v:[C

    .line 71
    const/16 v0, 0x11

    new-array v0, v0, [C

    const/16 v1, 0xef

    aput-char v1, v0, v6

    const/16 v1, 0xfe

    aput-char v1, v0, v3

    const/4 v1, 0x2

    const/16 v2, 0x2a

    aput-char v2, v0, v1

    aput-char v14, v0, v14

    aput-char v15, v0, v15

    sput-object v0, Lorg/tartarus/snowball/ext/DanishStemmer;->g_s_ending:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->S_ch:Ljava/lang/StringBuilder;

    .line 13
    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/DanishStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/DanishStemmer;

    .prologue
    .line 78
    iget v0, p1, Lorg/tartarus/snowball/ext/DanishStemmer;->I_x:I

    iput v0, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_x:I

    .line 79
    iget v0, p1, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    .line 80
    iget-object v0, p1, Lorg/tartarus/snowball/ext/DanishStemmer;->S_ch:Ljava/lang/StringBuilder;

    iput-object v0, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->S_ch:Ljava/lang/StringBuilder;

    .line 81
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 82
    return-void
.end method

.method private r_consonant_pair()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 209
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    sub-int v0, v4, v5

    .line 212
    .local v0, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 214
    .local v1, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    if-ge v4, v5, :cond_1

    .line 245
    :cond_0
    :goto_0
    return v3

    .line 218
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 219
    iget v2, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 220
    .local v2, "v_3":I
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 221
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 224
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->ket:I

    .line 226
    sget-object v4, Lorg/tartarus/snowball/ext/DanishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v5, 0x4

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/DanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v4

    if-nez v4, :cond_2

    .line 228
    iput v2, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    goto :goto_0

    .line 232
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->bra:I

    .line 233
    iput v2, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 234
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    sub-int/2addr v4, v0

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 236
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    if-le v4, v5, :cond_0

    .line 240
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 242
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->bra:I

    .line 244
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->slice_del()V

    .line 245
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private r_main_suffix()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 159
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 161
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    if-ge v4, v5, :cond_1

    .line 200
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 165
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 166
    iget v2, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 167
    .local v2, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 168
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 171
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->ket:I

    .line 173
    sget-object v4, Lorg/tartarus/snowball/ext/DanishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x20

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/DanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 174
    .local v0, "among_var":I
    if-nez v0, :cond_2

    .line 176
    iput v2, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    goto :goto_0

    .line 180
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->bra:I

    .line 181
    iput v2, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 182
    packed-switch v0, :pswitch_data_0

    .line 200
    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    .line 188
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->slice_del()V

    goto :goto_1

    .line 192
    :pswitch_2
    sget-object v4, Lorg/tartarus/snowball/ext/DanishStemmer;->g_s_ending:[C

    const/16 v5, 0x61

    const/16 v6, 0xe5

    invoke-virtual {p0, v4, v5, v6}, Lorg/tartarus/snowball/ext/DanishStemmer;->in_grouping_b([CII)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 197
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->slice_del()V

    goto :goto_1

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 8

    .prologue
    const/16 v7, 0xf8

    const/16 v6, 0x61

    const/4 v3, 0x0

    .line 88
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    .line 90
    iget v1, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 94
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    add-int/lit8 v0, v4, 0x3

    .line 95
    .local v0, "c":I
    if-ltz v0, :cond_0

    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    if-le v0, v4, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v3

    .line 99
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 102
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_x:I

    .line 103
    iput v1, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 107
    :goto_1
    iget v2, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 109
    .local v2, "v_2":I
    sget-object v4, Lorg/tartarus/snowball/ext/DanishStemmer;->g_v:[C

    invoke-virtual {p0, v4, v6, v7}, Lorg/tartarus/snowball/ext/DanishStemmer;->in_grouping([CII)Z

    move-result v4

    if-nez v4, :cond_2

    .line 116
    iput v2, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 117
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    if-ge v4, v5, :cond_0

    .line 121
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    goto :goto_1

    .line 113
    :cond_2
    iput v2, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 127
    :goto_2
    sget-object v4, Lorg/tartarus/snowball/ext/DanishStemmer;->g_v:[C

    invoke-virtual {p0, v4, v6, v7}, Lorg/tartarus/snowball/ext/DanishStemmer;->out_grouping([CII)Z

    move-result v4

    if-nez v4, :cond_3

    .line 133
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    if-ge v4, v5, :cond_0

    .line 137
    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    goto :goto_2

    .line 140
    :cond_3
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    .line 144
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_x:I

    if-lt v3, v4, :cond_4

    .line 150
    :goto_3
    const/4 v3, 0x1

    goto :goto_0

    .line 148
    :cond_4
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_x:I

    iput v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    goto :goto_3
.end method

.method private r_other_suffix()Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x0

    .line 256
    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 260
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->ket:I

    .line 262
    const-string v6, "st"

    invoke-virtual {p0, v8, v6}, Lorg/tartarus/snowball/ext/DanishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 276
    :cond_0
    :goto_0
    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 278
    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 280
    .local v2, "v_2":I
    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    if-ge v6, v7, :cond_2

    .line 325
    :goto_1
    :pswitch_0
    return v5

    .line 267
    .end local v2    # "v_2":I
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->bra:I

    .line 269
    const-string v6, "ig"

    invoke-virtual {p0, v8, v6}, Lorg/tartarus/snowball/ext/DanishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 274
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->slice_del()V

    goto :goto_0

    .line 284
    .restart local v2    # "v_2":I
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    iput v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 285
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 286
    .local v3, "v_3":I
    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 287
    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 290
    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->ket:I

    .line 292
    sget-object v6, Lorg/tartarus/snowball/ext/DanishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/4 v7, 0x5

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/DanishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 293
    .local v0, "among_var":I
    if-nez v0, :cond_3

    .line 295
    iput v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    goto :goto_1

    .line 299
    :cond_3
    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->bra:I

    .line 300
    iput v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 301
    packed-switch v0, :pswitch_data_0

    .line 325
    :goto_2
    const/4 v5, 0x1

    goto :goto_1

    .line 307
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->slice_del()V

    .line 309
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    sub-int v4, v5, v6

    .line 312
    .local v4, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->r_consonant_pair()Z

    move-result v5

    if-nez v5, :cond_4

    .line 317
    :cond_4
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    goto :goto_2

    .line 322
    .end local v4    # "v_4":I
    :pswitch_2
    const-string v5, "l\u00f8s"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/DanishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 301
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_undouble()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 333
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    sub-int v0, v3, v4

    .line 335
    .local v0, "v_1":I
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    if-ge v3, v4, :cond_1

    .line 363
    :cond_0
    :goto_0
    return v2

    .line 339
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->I_p1:I

    iput v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 340
    iget v1, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 341
    .local v1, "v_2":I
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 342
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    sub-int/2addr v3, v0

    iput v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 345
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->ket:I

    .line 346
    sget-object v3, Lorg/tartarus/snowball/ext/DanishStemmer;->g_v:[C

    const/16 v4, 0x61

    const/16 v5, 0xf8

    invoke-virtual {p0, v3, v4, v5}, Lorg/tartarus/snowball/ext/DanishStemmer;->out_grouping_b([CII)Z

    move-result v3

    if-nez v3, :cond_2

    .line 348
    iput v1, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    goto :goto_0

    .line 352
    :cond_2
    iget v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->bra:I

    .line 354
    iget-object v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->S_ch:Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lorg/tartarus/snowball/ext/DanishStemmer;->slice_to(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v3

    iput-object v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->S_ch:Ljava/lang/StringBuilder;

    .line 355
    iput v1, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    .line 357
    iget-object v3, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->S_ch:Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lorg/tartarus/snowball/ext/DanishStemmer;->eq_v_b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 362
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->slice_del()V

    .line 363
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 432
    instance-of v0, p1, Lorg/tartarus/snowball/ext/DanishStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 437
    const-class v0, Lorg/tartarus/snowball/ext/DanishStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 7

    .prologue
    .line 375
    iget v0, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 378
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->r_mark_regions()Z

    move-result v5

    if-nez v5, :cond_0

    .line 383
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 385
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 388
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 391
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->r_main_suffix()Z

    move-result v5

    if-nez v5, :cond_1

    .line 396
    :cond_1
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 398
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 401
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->r_consonant_pair()Z

    move-result v5

    if-nez v5, :cond_2

    .line 406
    :cond_2
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 408
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    sub-int v3, v5, v6

    .line 411
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->r_other_suffix()Z

    move-result v5

    if-nez v5, :cond_3

    .line 416
    :cond_3
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 418
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    sub-int v4, v5, v6

    .line 421
    .local v4, "v_5":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DanishStemmer;->r_undouble()Z

    move-result v5

    if-nez v5, :cond_4

    .line 426
    :cond_4
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    .line 427
    iget v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->limit_backward:I

    iput v5, p0, Lorg/tartarus/snowball/ext/DanishStemmer;->cursor:I

    const/4 v5, 0x1

    return v5
.end method
