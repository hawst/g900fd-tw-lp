.class public Lorg/tartarus/snowball/ext/TurkishStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "TurkishStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_10:[Lorg/tartarus/snowball/Among;

.field private static final a_11:[Lorg/tartarus/snowball/Among;

.field private static final a_12:[Lorg/tartarus/snowball/Among;

.field private static final a_13:[Lorg/tartarus/snowball/Among;

.field private static final a_14:[Lorg/tartarus/snowball/Among;

.field private static final a_15:[Lorg/tartarus/snowball/Among;

.field private static final a_16:[Lorg/tartarus/snowball/Among;

.field private static final a_17:[Lorg/tartarus/snowball/Among;

.field private static final a_18:[Lorg/tartarus/snowball/Among;

.field private static final a_19:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_20:[Lorg/tartarus/snowball/Among;

.field private static final a_21:[Lorg/tartarus/snowball/Among;

.field private static final a_22:[Lorg/tartarus/snowball/Among;

.field private static final a_23:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final a_6:[Lorg/tartarus/snowball/Among;

.field private static final a_7:[Lorg/tartarus/snowball/Among;

.field private static final a_8:[Lorg/tartarus/snowball/Among;

.field private static final a_9:[Lorg/tartarus/snowball/Among;

.field private static final g_U:[C

.field private static final g_vowel:[C

.field private static final g_vowel1:[C

.field private static final g_vowel2:[C

.field private static final g_vowel3:[C

.field private static final g_vowel4:[C

.field private static final g_vowel5:[C

.field private static final g_vowel6:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private B_continue_stemming_noun_suffixes:Z

.field private I_strlen:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v2, -0x1

    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/TurkishStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/TurkishStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    .line 19
    const/16 v0, 0xa

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "m"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "miz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "niz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    const/4 v7, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "muz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nuz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "m\u00fcz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n\u00fcz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "m\u0131z"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 29
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n\u0131z"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 32
    new-array v6, v10, [Lorg/tartarus/snowball/Among;

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "leri"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lar\u0131"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 32
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 37
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ni"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n\u00fc"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n\u0131"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 37
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 44
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "in"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "un"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00fcn"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0131n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 44
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 51
    new-array v6, v10, [Lorg/tartarus/snowball/Among;

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 51
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 56
    new-array v6, v10, [Lorg/tartarus/snowball/Among;

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "na"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ne"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 56
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 61
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "da"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ta"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "de"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "te"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 61
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    .line 68
    new-array v6, v10, [Lorg/tartarus/snowball/Among;

    .line 69
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nda"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 70
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nde"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 68
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    .line 73
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dan"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 75
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tan"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 76
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "den"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 77
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ten"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 73
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    .line 80
    new-array v6, v10, [Lorg/tartarus/snowball/Among;

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ndan"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nden"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 80
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_9:[Lorg/tartarus/snowball/Among;

    .line 85
    new-array v6, v10, [Lorg/tartarus/snowball/Among;

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "la"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "le"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 85
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_10:[Lorg/tartarus/snowball/Among;

    .line 90
    new-array v6, v10, [Lorg/tartarus/snowball/Among;

    .line 91
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ca"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 92
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ce"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 90
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_11:[Lorg/tartarus/snowball/Among;

    .line 95
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "im"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 97
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "um"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 98
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00fcm"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0131m"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 95
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_12:[Lorg/tartarus/snowball/Among;

    .line 102
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 103
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sin"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 104
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sun"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 105
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s\u00fcn"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s\u0131n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 102
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_13:[Lorg/tartarus/snowball/Among;

    .line 109
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 111
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00fcz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0131z"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 109
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_14:[Lorg/tartarus/snowball/Among;

    .line 116
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "siniz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 118
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sunuz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 119
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s\u00fcn\u00fcz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s\u0131n\u0131z"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 116
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_15:[Lorg/tartarus/snowball/Among;

    .line 123
    new-array v6, v10, [Lorg/tartarus/snowball/Among;

    .line 124
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lar"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 125
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ler"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 123
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_16:[Lorg/tartarus/snowball/Among;

    .line 128
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 129
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "niz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 130
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nuz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 131
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n\u00fcz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 132
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n\u0131z"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 128
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_17:[Lorg/tartarus/snowball/Among;

    .line 135
    const/16 v0, 0x8

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 136
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dir"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 137
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tir"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 138
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dur"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 139
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tur"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    const/4 v7, 0x4

    .line 140
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d\u00fcr"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 141
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u00fcr"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d\u0131r"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 143
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u0131r"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 135
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_18:[Lorg/tartarus/snowball/Among;

    .line 146
    new-array v6, v10, [Lorg/tartarus/snowball/Among;

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "cas\u0131na"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 148
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "cesine"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 146
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_19:[Lorg/tartarus/snowball/Among;

    .line 151
    const/16 v0, 0x20

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 152
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "di"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 153
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ti"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 154
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dik"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 155
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tik"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    const/4 v7, 0x4

    .line 156
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "duk"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 157
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tuk"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 158
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d\u00fck"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 159
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u00fck"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 160
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d\u0131k"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 161
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u0131k"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 162
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dim"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 163
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tim"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 164
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dum"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 165
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tum"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 166
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d\u00fcm"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 167
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u00fcm"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 168
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d\u0131m"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 169
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u0131m"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 170
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "din"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 171
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tin"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 172
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dun"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 173
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tun"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 174
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d\u00fcn"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 175
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u00fcn"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 176
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d\u0131n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 177
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u0131n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 178
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "du"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 179
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 180
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d\u00fc"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 181
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u00fc"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 182
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d\u0131"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 183
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u0131"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 151
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_20:[Lorg/tartarus/snowball/Among;

    .line 186
    const/16 v0, 0x8

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 187
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 188
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "se"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 189
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sak"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 190
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sek"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    const/4 v7, 0x4

    .line 191
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sam"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 192
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sem"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 193
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "san"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 194
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sen"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 186
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_21:[Lorg/tartarus/snowball/Among;

    .line 197
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 198
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mi\u015f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 199
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mu\u015f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 200
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "m\u00fc\u015f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 201
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "m\u0131\u015f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 197
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_22:[Lorg/tartarus/snowball/Among;

    .line 204
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 205
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "b"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v8

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v9

    .line 206
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "c"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v8

    .line 207
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    move v3, v11

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 208
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u011f"

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/TurkishStemmer;->methodObject:Lorg/tartarus/snowball/ext/TurkishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 204
    sput-object v6, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_23:[Lorg/tartarus/snowball/Among;

    .line 211
    const/16 v0, 0x1b

    new-array v0, v0, [C

    const/16 v1, 0x11

    aput-char v1, v0, v9

    const/16 v1, 0x41

    aput-char v1, v0, v8

    const/16 v1, 0x10

    aput-char v1, v0, v10

    const/16 v1, 0x12

    const/16 v2, 0x20

    aput-char v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0x8

    aput-char v2, v0, v1

    const/16 v1, 0x1a

    aput-char v8, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    .line 213
    const/16 v0, 0x1a

    new-array v0, v0, [C

    aput-char v8, v0, v9

    const/16 v1, 0x10

    aput-char v1, v0, v8

    const/16 v1, 0x12

    const/16 v2, 0x8

    aput-char v2, v0, v1

    const/16 v1, 0x19

    aput-char v8, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_U:[C

    .line 215
    const/16 v0, 0x1b

    new-array v0, v0, [C

    aput-char v8, v0, v9

    const/16 v1, 0x40

    aput-char v1, v0, v8

    const/16 v1, 0x10

    aput-char v1, v0, v10

    const/16 v1, 0x1a

    aput-char v8, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel1:[C

    .line 217
    const/16 v0, 0x13

    new-array v0, v0, [C

    const/16 v1, 0x11

    aput-char v1, v0, v9

    const/16 v1, 0x12

    const/16 v2, 0x82

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel2:[C

    .line 219
    const/16 v0, 0x1b

    new-array v0, v0, [C

    aput-char v8, v0, v9

    const/16 v1, 0x1a

    aput-char v8, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel3:[C

    .line 221
    new-array v0, v8, [C

    const/16 v1, 0x11

    aput-char v1, v0, v9

    sput-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel4:[C

    .line 223
    new-array v0, v8, [C

    const/16 v1, 0x41

    aput-char v1, v0, v9

    sput-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel5:[C

    .line 225
    new-array v0, v8, [C

    const/16 v1, 0x41

    aput-char v1, v0, v9

    sput-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel6:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/TurkishStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/TurkishStemmer;

    .prologue
    .line 231
    iget-boolean v0, p1, Lorg/tartarus/snowball/ext/TurkishStemmer;->B_continue_stemming_noun_suffixes:Z

    iput-boolean v0, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->B_continue_stemming_noun_suffixes:Z

    .line 232
    iget v0, p1, Lorg/tartarus/snowball/ext/TurkishStemmer;->I_strlen:I

    iput v0, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->I_strlen:I

    .line 233
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 234
    return-void
.end method

.method private r_append_U_to_stems_ending_with_d_or_g()Z
    .locals 23

    .prologue
    .line 2689
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v5, v20, v21

    .line 2693
    .local v5, "v_1":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v12, v20, v21

    .line 2696
    .local v12, "v_2":I
    const/16 v20, 0x1

    const-string v21, "d"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_0

    .line 2702
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v12

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2704
    const/16 v20, 0x1

    const-string v21, "g"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_0

    .line 2706
    const/16 v20, 0x0

    .line 2922
    :goto_0
    return v20

    .line 2709
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v5

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2712
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v13, v20, v21

    .line 2716
    .local v13, "v_3":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v14, v20, v21

    .line 2722
    .local v14, "v_4":I
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v15, v20, v21

    .line 2724
    .local v15, "v_5":I
    sget-object v20, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    const/16 v21, 0x61

    const/16 v22, 0x131

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v20

    if-nez v20, :cond_4

    .line 2731
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v15

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2732
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-gt v0, v1, :cond_6

    .line 2765
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v13

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2769
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v17, v20, v21

    .line 2775
    .local v17, "v_7":I
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v18, v20, v21

    .line 2777
    .local v18, "v_8":I
    sget-object v20, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    const/16 v21, 0x61

    const/16 v22, 0x131

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v20

    if-nez v20, :cond_7

    .line 2784
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v18

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2785
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-gt v0, v1, :cond_9

    .line 2818
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v13

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2822
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v6, v20, v21

    .line 2828
    .local v6, "v_10":I
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v7, v20, v21

    .line 2830
    .local v7, "v_11":I
    sget-object v20, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    const/16 v21, 0x61

    const/16 v22, 0x131

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v20

    if-nez v20, :cond_a

    .line 2837
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2838
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-gt v0, v1, :cond_c

    .line 2871
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v13

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2874
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v9, v20, v21

    .line 2880
    .local v9, "v_13":I
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v10, v20, v21

    .line 2882
    .local v10, "v_14":I
    sget-object v20, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    const/16 v21, 0x61

    const/16 v22, 0x131

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v20

    if-nez v20, :cond_d

    .line 2889
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v10

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2890
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-gt v0, v1, :cond_e

    .line 2892
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 2728
    .end local v6    # "v_10":I
    .end local v7    # "v_11":I
    .end local v9    # "v_13":I
    .end local v10    # "v_14":I
    .end local v17    # "v_7":I
    .end local v18    # "v_8":I
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v15

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2740
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v16, v20, v21

    .line 2743
    .local v16, "v_6":I
    const/16 v20, 0x1

    const-string v21, "a"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_5

    .line 2749
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v16

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2751
    const/16 v20, 0x1

    const-string/jumbo v21, "\u0131"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 2756
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v14

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2759
    move-object/from16 v0, p0

    iget v4, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2760
    .local v4, "c":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    const-string/jumbo v22, "\u0131"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/TurkishStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 2761
    move-object/from16 v0, p0

    iput v4, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2922
    .end local v16    # "v_6":I
    :goto_5
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 2736
    .end local v4    # "c":I
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_1

    .line 2781
    .restart local v17    # "v_7":I
    .restart local v18    # "v_8":I
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v18

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2793
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v19, v20, v21

    .line 2796
    .local v19, "v_9":I
    const/16 v20, 0x1

    const-string v21, "e"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_8

    .line 2802
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v19

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2804
    const/16 v20, 0x1

    const-string v21, "i"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 2809
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v17

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2812
    move-object/from16 v0, p0

    iget v4, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2813
    .restart local v4    # "c":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    const-string v22, "i"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/TurkishStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 2814
    move-object/from16 v0, p0

    iput v4, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_5

    .line 2789
    .end local v4    # "c":I
    .end local v19    # "v_9":I
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_2

    .line 2834
    .restart local v6    # "v_10":I
    .restart local v7    # "v_11":I
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2846
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v8, v20, v21

    .line 2849
    .local v8, "v_12":I
    const/16 v20, 0x1

    const-string v21, "o"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_b

    .line 2855
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2857
    const/16 v20, 0x1

    const-string v21, "u"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 2862
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v6

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2865
    move-object/from16 v0, p0

    iget v4, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2866
    .restart local v4    # "c":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    const-string v22, "u"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/TurkishStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 2867
    move-object/from16 v0, p0

    iput v4, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_5

    .line 2842
    .end local v4    # "c":I
    .end local v8    # "v_12":I
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2886
    .restart local v9    # "v_13":I
    .restart local v10    # "v_14":I
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v10

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2898
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v11, v20, v21

    .line 2901
    .local v11, "v_15":I
    const/16 v20, 0x1

    const-string/jumbo v21, "\u00f6"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_f

    .line 2907
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v11

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2909
    const/16 v20, 0x1

    const-string/jumbo v21, "\u00fc"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_f

    .line 2911
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 2894
    .end local v11    # "v_15":I
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_4

    .line 2914
    .restart local v11    # "v_15":I
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v9

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2917
    move-object/from16 v0, p0

    iget v4, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2918
    .restart local v4    # "c":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v21, v0

    const-string/jumbo v22, "\u00fc"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/TurkishStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 2919
    move-object/from16 v0, p0

    iput v4, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_5
.end method

.method private r_check_vowel_harmony()Z
    .locals 14

    .prologue
    .line 250
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v0, v11, v12

    .line 256
    .local v0, "v_1":I
    :goto_0
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v3, v11, v12

    .line 258
    .local v3, "v_2":I
    sget-object v11, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    const/16 v12, 0x61

    const/16 v13, 0x131

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_0

    .line 265
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v3

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 266
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v11, v12, :cond_1

    .line 268
    const/4 v11, 0x0

    .line 506
    :goto_1
    return v11

    .line 262
    :cond_0
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v3

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 275
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v4, v11, v12

    .line 279
    .local v4, "v_3":I
    const/4 v11, 0x1

    const-string v12, "a"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 304
    :goto_2
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 308
    const/4 v11, 0x1

    const-string v12, "e"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 333
    :goto_3
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 337
    const/4 v11, 0x1

    const-string/jumbo v12, "\u0131"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 362
    :goto_4
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 366
    const/4 v11, 0x1

    const-string v12, "i"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_c

    .line 391
    :goto_5
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 395
    const/4 v11, 0x1

    const-string v12, "o"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_f

    .line 420
    :goto_6
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 424
    const/4 v11, 0x1

    const-string/jumbo v12, "\u00f6"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_12

    .line 449
    :goto_7
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 453
    const/4 v11, 0x1

    const-string v12, "u"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_15

    .line 478
    :goto_8
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 481
    const/4 v11, 0x1

    const-string/jumbo v12, "\u00fc"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_18

    .line 483
    const/4 v11, 0x0

    goto :goto_1

    .line 270
    .end local v4    # "v_3":I
    :cond_1
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_0

    .line 300
    .restart local v4    # "v_3":I
    .local v5, "v_4":I
    :cond_2
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 286
    .end local v5    # "v_4":I
    :cond_3
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v5, v11, v12

    .line 288
    .restart local v5    # "v_4":I
    sget-object v11, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel1:[C

    const/16 v12, 0x61

    const/16 v13, 0x131

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_4

    .line 295
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 296
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v11, v12, :cond_2

    goto/16 :goto_2

    .line 292
    :cond_4
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 505
    .end local v5    # "v_4":I
    :goto_9
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v0

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 506
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 329
    .local v6, "v_5":I
    :cond_5
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 315
    .end local v6    # "v_5":I
    :cond_6
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v6, v11, v12

    .line 317
    .restart local v6    # "v_5":I
    sget-object v11, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel2:[C

    const/16 v12, 0x65

    const/16 v13, 0xfc

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_7

    .line 324
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v6

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 325
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v11, v12, :cond_5

    goto/16 :goto_3

    .line 321
    :cond_7
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v6

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_9

    .line 358
    .end local v6    # "v_5":I
    .local v7, "v_6":I
    :cond_8
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 344
    .end local v7    # "v_6":I
    :cond_9
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v7, v11, v12

    .line 346
    .restart local v7    # "v_6":I
    sget-object v11, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel3:[C

    const/16 v12, 0x61

    const/16 v13, 0x131

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_a

    .line 353
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v7

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 354
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v11, v12, :cond_8

    goto/16 :goto_4

    .line 350
    :cond_a
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v7

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_9

    .line 387
    .end local v7    # "v_6":I
    .local v8, "v_7":I
    :cond_b
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 373
    .end local v8    # "v_7":I
    :cond_c
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v8, v11, v12

    .line 375
    .restart local v8    # "v_7":I
    sget-object v11, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel4:[C

    const/16 v12, 0x65

    const/16 v13, 0x69

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_d

    .line 382
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 383
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v11, v12, :cond_b

    goto/16 :goto_5

    .line 379
    :cond_d
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_9

    .line 416
    .end local v8    # "v_7":I
    .local v9, "v_8":I
    :cond_e
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 402
    .end local v9    # "v_8":I
    :cond_f
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v9, v11, v12

    .line 404
    .restart local v9    # "v_8":I
    sget-object v11, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel5:[C

    const/16 v12, 0x6f

    const/16 v13, 0x75

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_10

    .line 411
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v9

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 412
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v11, v12, :cond_e

    goto/16 :goto_6

    .line 408
    :cond_10
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v9

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_9

    .line 445
    .end local v9    # "v_8":I
    .local v10, "v_9":I
    :cond_11
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 431
    .end local v10    # "v_9":I
    :cond_12
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v10, v11, v12

    .line 433
    .restart local v10    # "v_9":I
    sget-object v11, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel6:[C

    const/16 v12, 0xf6

    const/16 v13, 0xfc

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_13

    .line 440
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 441
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v11, v12, :cond_11

    goto/16 :goto_7

    .line 437
    :cond_13
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_9

    .line 474
    .end local v10    # "v_9":I
    .local v1, "v_10":I
    :cond_14
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 460
    .end local v1    # "v_10":I
    :cond_15
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v1, v11, v12

    .line 462
    .restart local v1    # "v_10":I
    sget-object v11, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel5:[C

    const/16 v12, 0x6f

    const/16 v13, 0x75

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_16

    .line 469
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v1

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 470
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v11, v12, :cond_14

    goto/16 :goto_8

    .line 466
    :cond_16
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v1

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_9

    .line 502
    .end local v1    # "v_10":I
    .local v2, "v_11":I
    :cond_17
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 488
    .end local v2    # "v_11":I
    :cond_18
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v2, v11, v12

    .line 490
    .restart local v2    # "v_11":I
    sget-object v11, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel6:[C

    const/16 v12, 0xf6

    const/16 v13, 0xfc

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_19

    .line 497
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v2

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 498
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v11, v12, :cond_17

    .line 500
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 494
    :cond_19
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v2

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_9
.end method

.method private r_is_reserved_word()Z
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v3, 0x0

    .line 2979
    iget v0, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2982
    .local v0, "v_1":I
    iget v1, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2989
    .local v1, "v_2":I
    :goto_0
    const-string v4, "ad"

    invoke-virtual {p0, v6, v4}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2995
    iget v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    if-lt v4, v5, :cond_2

    .line 3011
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3013
    iget v2, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3020
    .local v2, "v_4":I
    :goto_1
    const-string v4, "soyad"

    invoke-virtual {p0, v7, v4}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 3026
    iget v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    if-lt v4, v5, :cond_4

    .line 3041
    .end local v2    # "v_4":I
    :cond_1
    :goto_2
    return v3

    .line 2999
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_0

    .line 3002
    :cond_3
    iput v6, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->I_strlen:I

    .line 3004
    iget v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->I_strlen:I

    iget v5, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    if-ne v4, v5, :cond_0

    .line 3008
    iput v1, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3041
    :goto_3
    const/4 v3, 0x1

    goto :goto_2

    .line 3030
    .restart local v2    # "v_4":I
    :cond_4
    iget v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_1

    .line 3033
    :cond_5
    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->I_strlen:I

    .line 3035
    iget v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->I_strlen:I

    iget v5, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    if-ne v4, v5, :cond_1

    .line 3039
    iput v2, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_3
.end method

.method private r_mark_DA()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 972
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 981
    :cond_0
    :goto_0
    return v0

    .line 977
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 981
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_DAn()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1002
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1011
    :cond_0
    :goto_0
    return v0

    .line 1007
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1011
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_DUr()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1181
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1190
    :cond_0
    :goto_0
    return v0

    .line 1186
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_18:[Lorg/tartarus/snowball/Among;

    const/16 v2, 0x8

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1190
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_cAsInA()Z
    .locals 2

    .prologue
    .line 1196
    sget-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_19:[Lorg/tartarus/snowball/Among;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 1198
    const/4 v0, 0x0

    .line 1200
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_ki()Z
    .locals 2

    .prologue
    .line 1053
    const/4 v0, 0x2

    const-string v1, "ki"

    invoke-virtual {p0, v0, v1}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1055
    const/4 v0, 0x0

    .line 1057
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_lAr()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1151
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1160
    :cond_0
    :goto_0
    return v0

    .line 1156
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_16:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1160
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_lArI()Z
    .locals 2

    .prologue
    .line 870
    sget-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 872
    const/4 v0, 0x0

    .line 874
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_nA()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 957
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 966
    :cond_0
    :goto_0
    return v0

    .line 962
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 966
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_nU()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 900
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 909
    :cond_0
    :goto_0
    return v0

    .line 905
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 909
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_nUn()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 915
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 930
    :cond_0
    :goto_0
    return v0

    .line 920
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 926
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_n_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 930
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_nUz()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1166
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1175
    :cond_0
    :goto_0
    return v0

    .line 1171
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_17:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1175
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_ncA()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1063
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1078
    :cond_0
    :goto_0
    return v0

    .line 1068
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_11:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1074
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_n_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1078
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_ndA()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 987
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 996
    :cond_0
    :goto_0
    return v0

    .line 992
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 996
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_ndAn()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1017
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1026
    :cond_0
    :goto_0
    return v0

    .line 1022
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_9:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1026
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_possessives()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 834
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v2, 0xa

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 844
    :cond_0
    :goto_0
    return v0

    .line 840
    :cond_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_U_vowel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 844
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_sU()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 850
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 864
    :cond_0
    :goto_0
    return v0

    .line 854
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_U:[C

    const/16 v2, 0x69

    const/16 v3, 0x131

    invoke-virtual {p0, v1, v2, v3}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 860
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_s_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 864
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_sUn()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1105
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1114
    :cond_0
    :goto_0
    return v0

    .line 1110
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_13:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1114
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_sUnUz()Z
    .locals 2

    .prologue
    .line 1141
    sget-object v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_15:[Lorg/tartarus/snowball/Among;

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 1143
    const/4 v0, 0x0

    .line 1145
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_suffix_with_optional_U_vowel()Z
    .locals 13

    .prologue
    const/16 v12, 0x69

    const/16 v11, 0x61

    const/4 v7, 0x0

    const/16 v10, 0x131

    .line 763
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v0, v8, v9

    .line 768
    .local v0, "v_1":I
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v1, v8, v9

    .line 769
    .local v1, "v_2":I
    sget-object v8, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_U:[C

    invoke-virtual {p0, v8, v12, v10}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v8

    if-nez v8, :cond_2

    .line 790
    :cond_0
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v8, v0

    iput v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 795
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v3, v8, v9

    .line 799
    .local v3, "v_4":I
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v4, v8, v9

    .line 800
    .local v4, "v_5":I
    sget-object v8, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_U:[C

    invoke-virtual {p0, v8, v12, v10}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v8

    if-nez v8, :cond_3

    .line 807
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v8, v3

    iput v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 810
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v5, v8, v9

    .line 813
    .local v5, "v_6":I
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v8, v9, :cond_4

    .line 828
    .end local v3    # "v_4":I
    .end local v4    # "v_5":I
    .end local v5    # "v_6":I
    :cond_1
    :goto_0
    return v7

    .line 773
    :cond_2
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v8, v1

    iput v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 775
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-le v8, v9, :cond_0

    .line 779
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 782
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v2, v8, v9

    .line 783
    .local v2, "v_3":I
    sget-object v8, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    invoke-virtual {p0, v8, v11, v10}, Lorg/tartarus/snowball/ext/TurkishStemmer;->out_grouping_b([CII)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 787
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v2

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 828
    .end local v2    # "v_3":I
    :goto_1
    const/4 v7, 0x1

    goto :goto_0

    .line 804
    .restart local v3    # "v_4":I
    .restart local v4    # "v_5":I
    :cond_3
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v8, v4

    iput v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_0

    .line 817
    .restart local v5    # "v_6":I
    :cond_4
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 820
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v6, v8, v9

    .line 821
    .local v6, "v_7":I
    sget-object v8, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    invoke-virtual {p0, v8, v11, v10}, Lorg/tartarus/snowball/ext/TurkishStemmer;->out_grouping_b([CII)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 825
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v6

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 826
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v5

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_1
.end method

.method private r_mark_suffix_with_optional_n_consonant()Z
    .locals 13

    .prologue
    const/16 v12, 0x131

    const/16 v11, 0x61

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 520
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v0, v9, v10

    .line 525
    .local v0, "v_1":I
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v1, v9, v10

    .line 527
    .local v1, "v_2":I
    const-string v9, "n"

    invoke-virtual {p0, v8, v9}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 548
    :cond_0
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v9, v0

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 553
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v3, v9, v10

    .line 557
    .local v3, "v_4":I
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v4, v9, v10

    .line 559
    .local v4, "v_5":I
    const-string v9, "n"

    invoke-virtual {p0, v8, v9}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 566
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v9, v3

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 569
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v5, v9, v10

    .line 572
    .local v5, "v_6":I
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v9, v10, :cond_4

    .line 587
    .end local v3    # "v_4":I
    .end local v4    # "v_5":I
    .end local v5    # "v_6":I
    :cond_1
    :goto_0
    return v7

    .line 531
    :cond_2
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v9, v1

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 533
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-le v9, v10, :cond_0

    .line 537
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 540
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v2, v9, v10

    .line 541
    .local v2, "v_3":I
    sget-object v9, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    invoke-virtual {p0, v9, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 545
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v2

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .end local v2    # "v_3":I
    :goto_1
    move v7, v8

    .line 587
    goto :goto_0

    .line 563
    .restart local v3    # "v_4":I
    .restart local v4    # "v_5":I
    :cond_3
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v8, v4

    iput v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_0

    .line 576
    .restart local v5    # "v_6":I
    :cond_4
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 579
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v6, v9, v10

    .line 580
    .local v6, "v_7":I
    sget-object v9, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    invoke-virtual {p0, v9, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 584
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v6

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 585
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v5

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_1
.end method

.method private r_mark_suffix_with_optional_s_consonant()Z
    .locals 13

    .prologue
    const/16 v12, 0x131

    const/16 v11, 0x61

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 601
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v0, v9, v10

    .line 606
    .local v0, "v_1":I
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v1, v9, v10

    .line 608
    .local v1, "v_2":I
    const-string v9, "s"

    invoke-virtual {p0, v8, v9}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 629
    :cond_0
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v9, v0

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 634
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v3, v9, v10

    .line 638
    .local v3, "v_4":I
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v4, v9, v10

    .line 640
    .local v4, "v_5":I
    const-string v9, "s"

    invoke-virtual {p0, v8, v9}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 647
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v9, v3

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 650
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v5, v9, v10

    .line 653
    .local v5, "v_6":I
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v9, v10, :cond_4

    .line 668
    .end local v3    # "v_4":I
    .end local v4    # "v_5":I
    .end local v5    # "v_6":I
    :cond_1
    :goto_0
    return v7

    .line 612
    :cond_2
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v9, v1

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 614
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-le v9, v10, :cond_0

    .line 618
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 621
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v2, v9, v10

    .line 622
    .local v2, "v_3":I
    sget-object v9, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    invoke-virtual {p0, v9, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 626
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v2

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .end local v2    # "v_3":I
    :goto_1
    move v7, v8

    .line 668
    goto :goto_0

    .line 644
    .restart local v3    # "v_4":I
    .restart local v4    # "v_5":I
    :cond_3
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v8, v4

    iput v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_0

    .line 657
    .restart local v5    # "v_6":I
    :cond_4
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 660
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v6, v9, v10

    .line 661
    .local v6, "v_7":I
    sget-object v9, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    invoke-virtual {p0, v9, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 665
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v6

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 666
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v5

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_1
.end method

.method private r_mark_suffix_with_optional_y_consonant()Z
    .locals 13

    .prologue
    const/16 v12, 0x131

    const/16 v11, 0x61

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 682
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v0, v9, v10

    .line 687
    .local v0, "v_1":I
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v1, v9, v10

    .line 689
    .local v1, "v_2":I
    const-string/jumbo v9, "y"

    invoke-virtual {p0, v8, v9}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 710
    :cond_0
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v9, v0

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 715
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v3, v9, v10

    .line 719
    .local v3, "v_4":I
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v4, v9, v10

    .line 721
    .local v4, "v_5":I
    const-string/jumbo v9, "y"

    invoke-virtual {p0, v8, v9}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 728
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v9, v3

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 731
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v5, v9, v10

    .line 734
    .local v5, "v_6":I
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-gt v9, v10, :cond_4

    .line 749
    .end local v3    # "v_4":I
    .end local v4    # "v_5":I
    .end local v5    # "v_6":I
    :cond_1
    :goto_0
    return v7

    .line 693
    :cond_2
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v9, v1

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 695
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    if-le v9, v10, :cond_0

    .line 699
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 702
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v2, v9, v10

    .line 703
    .local v2, "v_3":I
    sget-object v9, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    invoke-virtual {p0, v9, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 707
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v2

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .end local v2    # "v_3":I
    :goto_1
    move v7, v8

    .line 749
    goto :goto_0

    .line 725
    .restart local v3    # "v_4":I
    .restart local v4    # "v_5":I
    :cond_3
    iget v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v8, v4

    iput v8, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_0

    .line 738
    .restart local v5    # "v_6":I
    :cond_4
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 741
    iget v9, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v6, v9, v10

    .line 742
    .local v6, "v_7":I
    sget-object v9, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    invoke-virtual {p0, v9, v11, v12}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 746
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v6

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 747
    iget v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v7, v5

    iput v7, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_1
.end method

.method private r_mark_yA()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 936
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 951
    :cond_0
    :goto_0
    return v0

    .line 941
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 947
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_y_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 951
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_yDU()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1206
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1221
    :cond_0
    :goto_0
    return v0

    .line 1211
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_20:[Lorg/tartarus/snowball/Among;

    const/16 v2, 0x20

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1217
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_y_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1221
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_yU()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 880
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 894
    :cond_0
    :goto_0
    return v0

    .line 884
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_U:[C

    const/16 v2, 0x69

    const/16 v3, 0x131

    invoke-virtual {p0, v1, v2, v3}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping_b([CII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 890
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_y_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 894
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_yUm()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1084
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1099
    :cond_0
    :goto_0
    return v0

    .line 1089
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_12:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1095
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_y_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1099
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_yUz()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1120
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1135
    :cond_0
    :goto_0
    return v0

    .line 1125
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_14:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1131
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_y_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1135
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_yken()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1264
    const/4 v1, 0x3

    const-string v2, "ken"

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1274
    :cond_0
    :goto_0
    return v0

    .line 1270
    :cond_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_y_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1274
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_ylA()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1032
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1047
    :cond_0
    :goto_0
    return v0

    .line 1037
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_10:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1043
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_y_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1047
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_ymUs_()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1243
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_check_vowel_harmony()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1258
    :cond_0
    :goto_0
    return v0

    .line 1248
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_22:[Lorg/tartarus/snowball/Among;

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1254
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_y_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1258
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_ysA()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1227
    sget-object v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_21:[Lorg/tartarus/snowball/Among;

    const/16 v2, 0x8

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 1237
    :cond_0
    :goto_0
    return v0

    .line 1233
    :cond_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_suffix_with_optional_y_consonant()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1237
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_more_than_one_syllable_word()Z
    .locals 6

    .prologue
    .line 2930
    iget v0, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2934
    .local v0, "v_1":I
    const/4 v1, 0x2

    .line 2938
    .local v1, "v_2":I
    :goto_0
    iget v2, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2945
    .local v2, "v_3":I
    :goto_1
    sget-object v3, Lorg/tartarus/snowball/ext/TurkishStemmer;->g_vowel:[C

    const/16 v4, 0x61

    const/16 v5, 0x131

    invoke-virtual {p0, v3, v4, v5}, Lorg/tartarus/snowball/ext/TurkishStemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2951
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    if-lt v3, v4, :cond_0

    .line 2960
    iput v2, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2963
    if-lez v1, :cond_2

    .line 2965
    const/4 v3, 0x0

    .line 2969
    :goto_2
    return v3

    .line 2955
    :cond_0
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_1

    .line 2957
    :cond_1
    add-int/lit8 v1, v1, -0x1

    .line 2958
    goto :goto_0

    .line 2968
    :cond_2
    iput v0, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2969
    const/4 v3, 0x1

    goto :goto_2
.end method

.method private r_post_process_last_consonants()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2635
    iget v2, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2637
    sget-object v2, Lorg/tartarus/snowball/ext/TurkishStemmer;->a_23:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/TurkishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 2638
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 2668
    :goto_0
    :pswitch_0
    return v1

    .line 2643
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2644
    packed-switch v0, :pswitch_data_0

    .line 2668
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 2650
    :pswitch_1
    const-string v1, "p"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2655
    :pswitch_2
    const-string/jumbo v1, "\u00e7"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2660
    :pswitch_3
    const-string v1, "t"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2665
    :pswitch_4
    const-string v1, "k"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2644
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private r_postlude()Z
    .locals 5

    .prologue
    .line 3051
    iget v0, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3055
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_is_reserved_word()Z

    move-result v3

    if-nez v3, :cond_2

    .line 3061
    iput v0, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3064
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3067
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v1, v3, v4

    .line 3070
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_append_U_to_stems_ending_with_d_or_g()Z

    move-result v3

    if-nez v3, :cond_0

    .line 3075
    :cond_0
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3077
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v2, v3, v4

    .line 3080
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_post_process_last_consonants()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3085
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v3, v2

    iput v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3086
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    iput v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    const/4 v3, 0x1

    .end local v1    # "v_2":I
    .end local v2    # "v_3":I
    :goto_0
    return v3

    .line 3059
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private r_stem_nominal_verb_suffixes()Z
    .locals 14

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1290
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1292
    iput-boolean v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->B_continue_stemming_noun_suffixes:Z

    .line 1295
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v0, v12, v13

    .line 1300
    .local v0, "v_1":I
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v2, v12, v13

    .line 1303
    .local v2, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ymUs_()Z

    move-result v12

    if-nez v12, :cond_3

    .line 1309
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v2

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1312
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yDU()Z

    move-result v12

    if-nez v12, :cond_3

    .line 1318
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v2

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1321
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ysA()Z

    move-result v12

    if-nez v12, :cond_3

    .line 1327
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v2

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1329
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yken()Z

    move-result v12

    if-nez v12, :cond_3

    .line 1336
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v0

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1340
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_cAsInA()Z

    move-result v12

    if-nez v12, :cond_1

    .line 1401
    :cond_0
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v0

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1405
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v12

    if-nez v12, :cond_4

    .line 1462
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v0

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1466
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_nUz()Z

    move-result v12

    if-nez v12, :cond_6

    .line 1491
    :goto_0
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v0

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1497
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v7, v12, v13

    .line 1500
    .local v7, "v_7":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sUnUz()Z

    move-result v12

    if-nez v12, :cond_7

    .line 1506
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v7

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1509
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yUz()Z

    move-result v12

    if-nez v12, :cond_7

    .line 1515
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v7

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1518
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sUn()Z

    move-result v12

    if-nez v12, :cond_7

    .line 1524
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v7

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1526
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yUm()Z

    move-result v12

    if-nez v12, :cond_7

    .line 1550
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v0

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1553
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_DUr()Z

    move-result v12

    if-nez v12, :cond_8

    .line 1629
    .end local v7    # "v_7":I
    :goto_1
    return v10

    .line 1347
    :cond_1
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v3, v12, v13

    .line 1350
    .local v3, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sUnUz()Z

    move-result v12

    if-nez v12, :cond_2

    .line 1356
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v3

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1359
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v12

    if-nez v12, :cond_2

    .line 1365
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v3

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1368
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yUm()Z

    move-result v12

    if-nez v12, :cond_2

    .line 1374
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v3

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1377
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sUn()Z

    move-result v12

    if-nez v12, :cond_2

    .line 1383
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v3

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1386
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yUz()Z

    move-result v12

    if-nez v12, :cond_2

    .line 1392
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v3

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1395
    :cond_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ymUs_()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1626
    .end local v3    # "v_3":I
    :cond_3
    :goto_2
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1628
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    move v10, v11

    .line 1629
    goto :goto_1

    .line 1410
    :cond_4
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1412
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1414
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v4, v12, v13

    .line 1418
    .local v4, "v_4":I
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1422
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v5, v12, v13

    .line 1425
    .local v5, "v_5":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_DUr()Z

    move-result v12

    if-nez v12, :cond_5

    .line 1431
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v5

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1434
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yDU()Z

    move-result v12

    if-nez v12, :cond_5

    .line 1440
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v5

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1443
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ysA()Z

    move-result v12

    if-nez v12, :cond_5

    .line 1449
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v5

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1451
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ymUs_()Z

    move-result v12

    if-nez v12, :cond_5

    .line 1453
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v4

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1459
    :cond_5
    iput-boolean v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->B_continue_stemming_noun_suffixes:Z

    goto :goto_2

    .line 1473
    .end local v4    # "v_4":I
    .end local v5    # "v_5":I
    :cond_6
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v6, v12, v13

    .line 1476
    .local v6, "v_6":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yDU()Z

    move-result v12

    if-nez v12, :cond_3

    .line 1482
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v6

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1484
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ysA()Z

    move-result v12

    if-nez v12, :cond_3

    goto/16 :goto_0

    .line 1532
    .end local v6    # "v_6":I
    .restart local v7    # "v_7":I
    :cond_7
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1534
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1536
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v8, v10, v12

    .line 1540
    .local v8, "v_8":I
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1542
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ymUs_()Z

    move-result v10

    if-nez v10, :cond_3

    .line 1544
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v10, v8

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_2

    .line 1558
    .end local v8    # "v_8":I
    :cond_8
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1560
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1562
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v9, v10, v12

    .line 1566
    .local v9, "v_9":I
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1570
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v1, v10, v12

    .line 1573
    .local v1, "v_10":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sUnUz()Z

    move-result v10

    if-nez v10, :cond_9

    .line 1579
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v10, v1

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1582
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v10

    if-nez v10, :cond_9

    .line 1588
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v10, v1

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1591
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yUm()Z

    move-result v10

    if-nez v10, :cond_9

    .line 1597
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v10, v1

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1600
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sUn()Z

    move-result v10

    if-nez v10, :cond_9

    .line 1606
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v10, v1

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1609
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yUz()Z

    move-result v10

    if-nez v10, :cond_9

    .line 1615
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v10, v1

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1618
    :cond_9
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ymUs_()Z

    move-result v10

    if-nez v10, :cond_3

    .line 1620
    iget v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v10, v9

    iput v10, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_2
.end method

.method private r_stem_noun_suffixes()Z
    .locals 31

    .prologue
    .line 1942
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v2, v29, v30

    .line 1946
    .local v2, "v_1":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1948
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_0

    .line 1969
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1973
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1975
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ncA()Z

    move-result v29

    if-nez v29, :cond_2

    .line 2081
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2085
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2089
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v26, v29, v30

    .line 2092
    .local v26, "v_7":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ndA()Z

    move-result v29

    if-nez v29, :cond_7

    .line 2098
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v26

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2100
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_nA()Z

    move-result v29

    if-nez v29, :cond_7

    .line 2169
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2173
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2177
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v3, v29, v30

    .line 2180
    .local v3, "v_10":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ndAn()Z

    move-result v29

    if-nez v29, :cond_b

    .line 2186
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v3

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2188
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_nU()Z

    move-result v29

    if-nez v29, :cond_b

    .line 2243
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2247
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2249
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_DAn()Z

    move-result v29

    if-nez v29, :cond_e

    .line 2340
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2344
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2347
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v10, v29, v30

    .line 2350
    .local v10, "v_17":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_nUn()Z

    move-result v29

    if-nez v29, :cond_12

    .line 2356
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v10

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2358
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ylA()Z

    move-result v29

    if-nez v29, :cond_12

    .line 2457
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2461
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2463
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lArI()Z

    move-result v29

    if-nez v29, :cond_16

    .line 2473
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2477
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2483
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2487
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2490
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v16, v29, v30

    .line 2493
    .local v16, "v_22":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_DA()Z

    move-result v29

    if-nez v29, :cond_17

    .line 2499
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v16

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2502
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yU()Z

    move-result v29

    if-nez v29, :cond_17

    .line 2508
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v16

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2510
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_yA()Z

    move-result v29

    if-nez v29, :cond_17

    .line 2578
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2581
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2584
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v20, v29, v30

    .line 2587
    .local v20, "v_26":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_possessives()Z

    move-result v29

    if-nez v29, :cond_1a

    .line 2593
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v20

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2595
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sU()Z

    move-result v29

    if-nez v29, :cond_1a

    .line 2597
    const/16 v29, 0x0

    .line 2628
    .end local v3    # "v_10":I
    .end local v10    # "v_17":I
    .end local v16    # "v_22":I
    .end local v20    # "v_26":I
    .end local v26    # "v_7":I
    :goto_2
    return v29

    .line 1953
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1955
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1957
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v13, v29, v30

    .line 1961
    .local v13, "v_2":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 1963
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v13

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2628
    .end local v13    # "v_2":I
    :cond_1
    :goto_3
    const/16 v29, 0x1

    goto :goto_2

    .line 1980
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1982
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1984
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v22, v29, v30

    .line 1989
    .local v22, "v_3":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v23, v29, v30

    .line 1993
    .local v23, "v_4":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1995
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lArI()Z

    move-result v29

    if-nez v29, :cond_3

    .line 2005
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v23

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2009
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2012
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v24, v29, v30

    .line 2015
    .local v24, "v_5":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_possessives()Z

    move-result v29

    if-nez v29, :cond_4

    .line 2021
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v24

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2023
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sU()Z

    move-result v29

    if-nez v29, :cond_4

    .line 2057
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v23

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2060
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2062
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_6

    .line 2064
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v22

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2000
    .end local v24    # "v_5":I
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2002
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    goto/16 :goto_3

    .line 2029
    .restart local v24    # "v_5":I
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2031
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2033
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v25, v29, v30

    .line 2037
    .local v25, "v_6":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2039
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_5

    .line 2041
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v25

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2045
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2047
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2049
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2051
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v25

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2068
    .end local v25    # "v_6":I
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2070
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2072
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2074
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v22

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2108
    .end local v22    # "v_3":I
    .end local v23    # "v_4":I
    .end local v24    # "v_5":I
    .restart local v26    # "v_7":I
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v27, v29, v30

    .line 2112
    .local v27, "v_8":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lArI()Z

    move-result v29

    if-nez v29, :cond_8

    .line 2122
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v27

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2126
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sU()Z

    move-result v29

    if-nez v29, :cond_9

    .line 2159
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v27

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2162
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    goto/16 :goto_0

    .line 2117
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2119
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    goto/16 :goto_3

    .line 2131
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2133
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2135
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v28, v29, v30

    .line 2139
    .local v28, "v_9":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2141
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_a

    .line 2143
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v28

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2147
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2149
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2151
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2153
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v28

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2196
    .end local v27    # "v_8":I
    .end local v28    # "v_9":I
    .restart local v3    # "v_10":I
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v4, v29, v30

    .line 2200
    .local v4, "v_11":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sU()Z

    move-result v29

    if-nez v29, :cond_c

    .line 2233
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v4

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2236
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lArI()Z

    move-result v29

    if-nez v29, :cond_1

    goto/16 :goto_1

    .line 2205
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2207
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2209
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v5, v29, v30

    .line 2213
    .local v5, "v_12":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2215
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_d

    .line 2217
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v5

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2221
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2223
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2225
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2227
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v5

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2254
    .end local v4    # "v_11":I
    .end local v5    # "v_12":I
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2256
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2258
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v6, v29, v30

    .line 2262
    .local v6, "v_13":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2266
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v7, v29, v30

    .line 2270
    .local v7, "v_14":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_possessives()Z

    move-result v29

    if-nez v29, :cond_f

    .line 2303
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v7

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2307
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_11

    .line 2328
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v7

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2331
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2333
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v6

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2275
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2277
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2279
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v8, v29, v30

    .line 2283
    .local v8, "v_15":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2285
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_10

    .line 2287
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v8

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2291
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2293
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2295
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2297
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v8

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2312
    .end local v8    # "v_15":I
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2314
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2316
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v9, v29, v30

    .line 2320
    .local v9, "v_16":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2322
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v9

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2364
    .end local v6    # "v_13":I
    .end local v7    # "v_14":I
    .end local v9    # "v_16":I
    .restart local v10    # "v_17":I
    :cond_12
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2366
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2368
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v11, v29, v30

    .line 2373
    .local v11, "v_18":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v12, v29, v30

    .line 2377
    .local v12, "v_19":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2379
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_13

    .line 2394
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v12

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2398
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2401
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v14, v29, v30

    .line 2404
    .local v14, "v_20":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_possessives()Z

    move-result v29

    if-nez v29, :cond_14

    .line 2410
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v14

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2412
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sU()Z

    move-result v29

    if-nez v29, :cond_14

    .line 2446
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v12

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2448
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2450
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v11

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2384
    .end local v14    # "v_20":I
    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2386
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2388
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    goto :goto_4

    .line 2418
    .restart local v14    # "v_20":I
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2420
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2422
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v15, v29, v30

    .line 2426
    .local v15, "v_21":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2428
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_15

    .line 2430
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v15

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2434
    :cond_15
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2436
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2438
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2440
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v15

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2468
    .end local v11    # "v_18":I
    .end local v12    # "v_19":I
    .end local v14    # "v_20":I
    .end local v15    # "v_21":I
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2470
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    goto/16 :goto_3

    .line 2516
    .restart local v16    # "v_22":I
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2518
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2520
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v17, v29, v30

    .line 2524
    .local v17, "v_23":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2528
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v18, v29, v30

    .line 2532
    .local v18, "v_24":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_possessives()Z

    move-result v29

    if-nez v29, :cond_18

    .line 2555
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v18

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2557
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_19

    .line 2559
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v17

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2537
    :cond_18
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2539
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2541
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v19, v29, v30

    .line 2545
    .local v19, "v_25":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2547
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_19

    .line 2549
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v19

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 2564
    .end local v19    # "v_25":I
    :cond_19
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2566
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2568
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2570
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2572
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v17

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2601
    .end local v17    # "v_23":I
    .end local v18    # "v_24":I
    .restart local v20    # "v_26":I
    :cond_1a
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2603
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2605
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v30, v0

    sub-int v21, v29, v30

    .line 2609
    .local v21, "v_27":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 2611
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v29

    if-nez v29, :cond_1b

    .line 2613
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v21

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3

    .line 2617
    :cond_1b
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 2619
    invoke-virtual/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 2621
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v29

    if-nez v29, :cond_1

    .line 2623
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    move/from16 v29, v0

    sub-int v29, v29, v21

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_3
.end method

.method private r_stem_suffix_chain_before_ki()Z
    .locals 14

    .prologue
    const/4 v11, 0x0

    .line 1646
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1648
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ki()Z

    move-result v12

    if-nez v12, :cond_1

    .line 1908
    :cond_0
    :goto_0
    return v11

    .line 1655
    :cond_1
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v0, v12, v13

    .line 1659
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_DA()Z

    move-result v12

    if-nez v12, :cond_3

    .line 1739
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v0

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1743
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_nUn()Z

    move-result v12

    if-nez v12, :cond_7

    .line 1837
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v0

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1840
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_ndA()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1847
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v13, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v1, v12, v13

    .line 1851
    .local v1, "v_10":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lArI()Z

    move-result v12

    if-nez v12, :cond_b

    .line 1861
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v1

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1866
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sU()Z

    move-result v12

    if-nez v12, :cond_c

    .line 1899
    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v12, v1

    iput v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1902
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1908
    .end local v1    # "v_10":I
    :cond_2
    :goto_1
    const/4 v11, 0x1

    goto :goto_0

    .line 1664
    :cond_3
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1666
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1668
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v3, v11, v12

    .line 1672
    .local v3, "v_2":I
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1675
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v4, v11, v12

    .line 1679
    .local v4, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v11

    if-nez v11, :cond_4

    .line 1700
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1703
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_possessives()Z

    move-result v11

    if-nez v11, :cond_5

    .line 1705
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v3

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_1

    .line 1684
    :cond_4
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1686
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1688
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v5, v11, v12

    .line 1692
    .local v5, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v11

    if-nez v11, :cond_2

    .line 1694
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_1

    .line 1709
    .end local v5    # "v_4":I
    :cond_5
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1711
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1713
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v6, v11, v12

    .line 1717
    .local v6, "v_5":I
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1719
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v11

    if-nez v11, :cond_6

    .line 1721
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v6

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_1

    .line 1725
    :cond_6
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1727
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1729
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v11

    if-nez v11, :cond_2

    .line 1731
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v6

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto :goto_1

    .line 1748
    .end local v3    # "v_2":I
    .end local v4    # "v_3":I
    .end local v6    # "v_5":I
    :cond_7
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1750
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1752
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v7, v11, v12

    .line 1756
    .local v7, "v_6":I
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1759
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v8, v11, v12

    .line 1763
    .local v8, "v_7":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lArI()Z

    move-result v11

    if-nez v11, :cond_8

    .line 1773
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1777
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1780
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v9, v11, v12

    .line 1783
    .local v9, "v_8":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_possessives()Z

    move-result v11

    if-nez v11, :cond_9

    .line 1789
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v9

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1791
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_sU()Z

    move-result v11

    if-nez v11, :cond_9

    .line 1825
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 1828
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v11

    if-nez v11, :cond_2

    .line 1830
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v7

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_1

    .line 1768
    .end local v9    # "v_8":I
    :cond_8
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1770
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    goto/16 :goto_1

    .line 1797
    .restart local v9    # "v_8":I
    :cond_9
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1799
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1801
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v10, v11, v12

    .line 1805
    .local v10, "v_9":I
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1807
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v11

    if-nez v11, :cond_a

    .line 1809
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_1

    .line 1813
    :cond_a
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1815
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1817
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v11

    if-nez v11, :cond_2

    .line 1819
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_1

    .line 1856
    .end local v7    # "v_6":I
    .end local v8    # "v_7":I
    .end local v9    # "v_8":I
    .end local v10    # "v_9":I
    .restart local v1    # "v_10":I
    :cond_b
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1858
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    goto/16 :goto_1

    .line 1871
    :cond_c
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1873
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1875
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v2, v11, v12

    .line 1879
    .local v2, "v_11":I
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->ket:I

    .line 1881
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_mark_lAr()Z

    move-result v11

    if-nez v11, :cond_d

    .line 1883
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v2

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_1

    .line 1887
    :cond_d
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->bra:I

    .line 1889
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->slice_del()V

    .line 1891
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_suffix_chain_before_ki()Z

    move-result v11

    if-nez v11, :cond_2

    .line 1893
    iget v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v11, v2

    iput v11, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    goto/16 :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 3139
    instance-of v0, p1, Lorg/tartarus/snowball/ext/TurkishStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 3144
    const-class v0, Lorg/tartarus/snowball/ext/TurkishStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 3096
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_more_than_one_syllable_word()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3134
    :cond_0
    :goto_0
    return v2

    .line 3102
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3105
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v0, v3, v4

    .line 3108
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_nominal_verb_suffixes()Z

    move-result v3

    if-nez v3, :cond_2

    .line 3113
    :cond_2
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v3, v0

    iput v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3115
    iget-boolean v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->B_continue_stemming_noun_suffixes:Z

    if-eqz v3, :cond_0

    .line 3120
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    sub-int v1, v3, v4

    .line 3123
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_stem_noun_suffixes()Z

    move-result v3

    if-nez v3, :cond_3

    .line 3128
    :cond_3
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3129
    iget v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->limit_backward:I

    iput v3, p0, Lorg/tartarus/snowball/ext/TurkishStemmer;->cursor:I

    .line 3130
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/TurkishStemmer;->r_postlude()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3134
    const/4 v2, 0x1

    goto :goto_0
.end method
