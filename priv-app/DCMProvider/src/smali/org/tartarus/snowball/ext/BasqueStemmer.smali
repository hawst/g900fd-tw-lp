.class public Lorg/tartarus/snowball/ext/BasqueStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "BasqueStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_p2:I

.field private I_pV:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/16 v13, 0x48

    const/16 v12, 0x6c

    const/4 v11, 0x2

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/BasqueStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    .line 19
    const/16 v0, 0x6d

    new-array v10, v0, [Lorg/tartarus/snowball/Among;

    const/4 v6, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "idea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 21
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "bidea"

    const/4 v6, 0x0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v3

    .line 22
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kidea"

    const/4 v6, 0x0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v11

    const/4 v0, 0x3

    .line 23
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "pidea"

    const/4 v6, 0x0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v6, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kundea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "galea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x6

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tailea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x7

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tzailea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x8

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gunea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x9

    .line 29
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kunea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xa

    .line 30
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tzaga"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xb

    .line 31
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gaia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xc

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aldia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xd

    .line 33
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "taldia"

    const/16 v6, 0xc

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xe

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "karia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xf

    .line 35
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "garria"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x10

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "karria"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x11

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ka"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x12

    .line 38
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzaka"

    const/16 v6, 0x11

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x13

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "la"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x14

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mena"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x15

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pena"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x16

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kina"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x17

    .line 43
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ezina"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x18

    .line 44
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tezina"

    const/16 v6, 0x17

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x19

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kuna"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x1a

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tuna"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x1b

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kizuna"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x1c

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "era"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x1d

    .line 49
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "bera"

    const/16 v6, 0x1c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x1e

    .line 50
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "arabera"

    const/16 v6, 0x1d

    const/4 v7, 0x4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x1f

    .line 51
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kera"

    const/16 v6, 0x1c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x20

    .line 52
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "pera"

    const/16 v6, 0x1c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x21

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "orra"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x22

    .line 54
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "korra"

    const/16 v6, 0x21

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x23

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dura"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x24

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gura"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x25

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kura"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x26

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tura"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x27

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eta"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x28

    .line 60
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "keta"

    const/16 v6, 0x27

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x29

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gailua"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x2a

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eza"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x2b

    .line 63
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "erreza"

    const/16 v6, 0x2a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x2c

    .line 64
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tza"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x2d

    .line 65
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gaitza"

    const/16 v6, 0x2c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x2e

    .line 66
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kaitza"

    const/16 v6, 0x2c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x2f

    .line 67
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kuntza"

    const/16 v6, 0x2c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x30

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ide"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x31

    .line 69
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "bide"

    const/16 v6, 0x30

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x32

    .line 70
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kide"

    const/16 v6, 0x30

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x33

    .line 71
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "pide"

    const/16 v6, 0x30

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x34

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kunde"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x35

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tzake"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x36

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tzeke"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x37

    .line 75
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "le"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x38

    .line 76
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gale"

    const/16 v6, 0x37

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x39

    .line 77
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "taile"

    const/16 v6, 0x37

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x3a

    .line 78
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzaile"

    const/16 v6, 0x37

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x3b

    .line 79
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gune"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x3c

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kune"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x3d

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tze"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x3e

    .line 82
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "atze"

    const/16 v6, 0x3d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x3f

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gai"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x40

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aldi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x41

    .line 85
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "taldi"

    const/16 v6, 0x40

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x42

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ki"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x43

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ari"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x44

    .line 88
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kari"

    const/16 v6, 0x43

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x45

    .line 89
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "lari"

    const/16 v6, 0x43

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x46

    .line 90
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tari"

    const/16 v6, 0x43

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x47

    .line 91
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "etari"

    const/16 v6, 0x46

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 92
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "garri"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v13

    const/16 v6, 0x49

    .line 93
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "karri"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x4a

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "arazi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x4b

    .line 95
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tarazi"

    const/16 v6, 0x4a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x4c

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "an"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x4d

    .line 97
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ean"

    const/16 v6, 0x4c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x4e

    .line 98
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "rean"

    const/16 v6, 0x4d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x4f

    .line 99
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kan"

    const/16 v6, 0x4c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x50

    .line 100
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "etan"

    const/16 v6, 0x4c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x51

    .line 101
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "atseden"

    const/4 v7, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x52

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "men"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x53

    .line 103
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pen"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x54

    .line 104
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kin"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x55

    .line 105
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "rekin"

    const/16 v6, 0x54

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x56

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ezin"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x57

    .line 107
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tezin"

    const/16 v6, 0x56

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x58

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tun"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x59

    .line 109
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kizun"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x5a

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "go"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x5b

    .line 111
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ago"

    const/16 v6, 0x5a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x5c

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tio"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x5d

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dako"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x5e

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "or"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x5f

    .line 115
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kor"

    const/16 v6, 0x5e

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x60

    .line 116
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tzat"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x61

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "du"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x62

    .line 118
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gailu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x63

    .line 119
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x64

    .line 120
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "atu"

    const/16 v6, 0x63

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x65

    .line 121
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "aldatu"

    const/16 v6, 0x64

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x66

    .line 122
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tatu"

    const/16 v6, 0x64

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x67

    .line 123
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "baditu"

    const/16 v6, 0x63

    const/4 v7, 0x5

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x68

    .line 124
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ez"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x69

    .line 125
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "errez"

    const/16 v6, 0x68

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x6a

    .line 126
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzez"

    const/16 v6, 0x68

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x6b

    .line 127
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gaitz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 128
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kaitz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v12

    .line 19
    sput-object v10, Lorg/tartarus/snowball/ext/BasqueStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 131
    const/16 v0, 0x127

    new-array v10, v0, [Lorg/tartarus/snowball/Among;

    const/4 v6, 0x0

    .line 132
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ada"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 133
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kada"

    const/4 v6, 0x0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v3

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "anda"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v11

    const/4 v6, 0x3

    .line 135
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "denda"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x4

    .line 136
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gabea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x5

    .line 137
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kabea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x6

    .line 138
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aldea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v0, 0x7

    .line 139
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kaldea"

    const/4 v6, 0x6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x8

    .line 140
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "taldea"

    const/4 v6, 0x6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x9

    .line 141
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ordea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xa

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zalea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xb

    .line 143
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzalea"

    const/16 v6, 0xa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xc

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gilea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xd

    .line 145
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "emea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xe

    .line 146
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kumea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xf

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x10

    .line 148
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "enea"

    const/16 v6, 0xf

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x11

    .line 149
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zionea"

    const/16 v6, 0xf

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x12

    .line 150
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "unea"

    const/16 v6, 0xf

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x13

    .line 151
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gunea"

    const/16 v6, 0x12

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x14

    .line 152
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x15

    .line 153
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aurrea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x16

    .line 154
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x17

    .line 155
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kotea"

    const/16 v6, 0x16

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x18

    .line 156
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "artea"

    const/16 v6, 0x16

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x19

    .line 157
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ostea"

    const/16 v6, 0x16

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x1a

    .line 158
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "etxea"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x1b

    .line 159
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ga"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x1c

    .line 160
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "anga"

    const/16 v6, 0x1b

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x1d

    .line 161
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gaia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x1e

    .line 162
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aldia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x1f

    .line 163
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "taldia"

    const/16 v6, 0x1e

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x20

    .line 164
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "handia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x21

    .line 165
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mendia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x22

    .line 166
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "geia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x23

    .line 167
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "egia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x24

    .line 168
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "degia"

    const/16 v6, 0x23

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x25

    .line 169
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tegia"

    const/16 v6, 0x23

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x26

    .line 170
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nahia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x27

    .line 171
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ohia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x28

    .line 172
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x29

    .line 173
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tokia"

    const/16 v6, 0x28

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x2a

    .line 174
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x2b

    .line 175
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "koia"

    const/16 v6, 0x2a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x2c

    .line 176
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aria"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x2d

    .line 177
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "karia"

    const/16 v6, 0x2c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x2e

    .line 178
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "laria"

    const/16 v6, 0x2c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x2f

    .line 179
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "taria"

    const/16 v6, 0x2c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x30

    .line 180
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eria"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x31

    .line 181
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "keria"

    const/16 v6, 0x30

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x32

    .line 182
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "teria"

    const/16 v6, 0x30

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x33

    .line 183
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "garria"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x34

    .line 184
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "larria"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x35

    .line 185
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kirria"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x36

    .line 186
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "duria"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x37

    .line 187
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x38

    .line 188
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x39

    .line 189
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ezia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x3a

    .line 190
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bizia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x3b

    .line 191
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ontzia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x3c

    .line 192
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ka"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x3d

    .line 193
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "joka"

    const/16 v6, 0x3c

    const/4 v7, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x3e

    .line 194
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "aurka"

    const/16 v6, 0x3c

    const/16 v7, 0xa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x3f

    .line 195
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ska"

    const/16 v6, 0x3c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x40

    .line 196
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "xka"

    const/16 v6, 0x3c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x41

    .line 197
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zka"

    const/16 v6, 0x3c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x42

    .line 198
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gibela"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x43

    .line 199
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gela"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x44

    .line 200
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kaila"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x45

    .line 201
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "skila"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x46

    .line 202
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tila"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x47

    .line 203
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ola"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 204
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "na"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v13

    const/16 v0, 0x49

    .line 205
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kana"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x4a

    .line 206
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ena"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x4b

    .line 207
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "garrena"

    const/16 v6, 0x4a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x4c

    .line 208
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gerrena"

    const/16 v6, 0x4a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x4d

    .line 209
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "urrena"

    const/16 v6, 0x4a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x4e

    .line 210
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zaina"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x4f

    .line 211
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzaina"

    const/16 v6, 0x4e

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x50

    .line 212
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kina"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x51

    .line 213
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "mina"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x52

    .line 214
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "garna"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x53

    .line 215
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "una"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x54

    .line 216
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "duna"

    const/16 v6, 0x53

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x55

    .line 217
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "asuna"

    const/16 v6, 0x53

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x56

    .line 218
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tasuna"

    const/16 v6, 0x55

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x57

    .line 219
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ondoa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x58

    .line 220
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kondoa"

    const/16 v6, 0x57

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x59

    .line 221
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ngoa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x5a

    .line 222
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zioa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x5b

    .line 223
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "koa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x5c

    .line 224
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "takoa"

    const/16 v6, 0x5b

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x5d

    .line 225
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zkoa"

    const/16 v6, 0x5b

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x5e

    .line 226
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "noa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x5f

    .line 227
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zinoa"

    const/16 v6, 0x5e

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x60

    .line 228
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aroa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x61

    .line 229
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "taroa"

    const/16 v6, 0x60

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x62

    .line 230
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zaroa"

    const/16 v6, 0x60

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x63

    .line 231
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eroa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x64

    .line 232
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oroa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x65

    .line 233
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osoa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x66

    .line 234
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "toa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x67

    .line 235
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ttoa"

    const/16 v6, 0x66

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x68

    .line 236
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "ztoa"

    const/16 v6, 0x66

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x69

    .line 237
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "txoa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x6a

    .line 238
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tzoa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x6b

    .line 239
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f1oa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 240
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ra"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v12

    const/16 v0, 0x6d

    .line 241
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ara"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x6e

    .line 242
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "dara"

    const/16 v6, 0x6d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x6f

    .line 243
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "liara"

    const/16 v6, 0x6d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x70

    .line 244
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tiara"

    const/16 v6, 0x6d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x71

    .line 245
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tara"

    const/16 v6, 0x6d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x72

    .line 246
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "etara"

    const/16 v6, 0x71

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x73

    .line 247
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzara"

    const/16 v6, 0x6d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x74

    .line 248
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "bera"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x75

    .line 249
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kera"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x76

    .line 250
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "pera"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x77

    .line 251
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ora"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v12

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x78

    .line 252
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzarra"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x79

    .line 253
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "korra"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x7a

    .line 254
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tra"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x7b

    .line 255
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x7c

    .line 256
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "osa"

    const/16 v6, 0x7b

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x7d

    .line 257
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ta"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x7e

    .line 258
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eta"

    const/16 v6, 0x7d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x7f

    .line 259
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "keta"

    const/16 v6, 0x7e

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x80

    .line 260
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "sta"

    const/16 v6, 0x7d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x81

    .line 261
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dua"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x82

    .line 262
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "mendua"

    const/16 v6, 0x81

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x83

    .line 263
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ordua"

    const/16 v6, 0x81

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x84

    .line 264
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lekua"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x85

    .line 265
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "burua"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x86

    .line 266
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "durua"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x87

    .line 267
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tsua"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x88

    .line 268
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tua"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x89

    .line 269
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "mentua"

    const/16 v6, 0x88

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x8a

    .line 270
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "estua"

    const/16 v6, 0x88

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x8b

    .line 271
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "txua"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x8c

    .line 272
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zua"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x8d

    .line 273
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzua"

    const/16 v6, 0x8c

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x8e

    .line 274
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "za"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x8f

    .line 275
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eza"

    const/16 v6, 0x8e

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x90

    .line 276
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eroza"

    const/16 v6, 0x8e

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x91

    .line 277
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tza"

    const/16 v6, 0x8e

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x92

    .line 278
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "koitza"

    const/16 v6, 0x91

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x93

    .line 279
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "antza"

    const/16 v6, 0x91

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x94

    .line 280
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gintza"

    const/16 v6, 0x91

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x95

    .line 281
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kintza"

    const/16 v6, 0x91

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x96

    .line 282
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kuntza"

    const/16 v6, 0x91

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x97

    .line 283
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gabe"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x98

    .line 284
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kabe"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x99

    .line 285
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kide"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x9a

    .line 286
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "alde"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x9b

    .line 287
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kalde"

    const/16 v6, 0x9a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x9c

    .line 288
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "talde"

    const/16 v6, 0x9a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x9d

    .line 289
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "orde"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x9e

    .line 290
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ge"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x9f

    .line 291
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zale"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xa0

    .line 292
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzale"

    const/16 v6, 0x9f

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xa1

    .line 293
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gile"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xa2

    .line 294
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eme"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xa3

    .line 295
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kume"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xa4

    .line 296
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ne"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xa5

    .line 297
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zione"

    const/16 v6, 0xa4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xa6

    .line 298
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "une"

    const/16 v6, 0xa4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xa7

    .line 299
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gune"

    const/16 v6, 0xa6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xa8

    .line 300
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pe"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xa9

    .line 301
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aurre"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xaa

    .line 302
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "te"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xab

    .line 303
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kote"

    const/16 v6, 0xaa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xac

    .line 304
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "arte"

    const/16 v6, 0xaa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xad

    .line 305
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "oste"

    const/16 v6, 0xaa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xae

    .line 306
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "etxe"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xaf

    .line 307
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gai"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xb0

    .line 308
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "di"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xb1

    .line 309
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "aldi"

    const/16 v6, 0xb0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xb2

    .line 310
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "taldi"

    const/16 v6, 0xb1

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xb3

    .line 311
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "geldi"

    const/16 v6, 0xb0

    const/16 v7, 0x8

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xb4

    .line 312
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "handi"

    const/16 v6, 0xb0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xb5

    .line 313
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "mendi"

    const/16 v6, 0xb0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xb6

    .line 314
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gei"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xb7

    .line 315
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "egi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xb8

    .line 316
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "degi"

    const/16 v6, 0xb7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xb9

    .line 317
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tegi"

    const/16 v6, 0xb7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xba

    .line 318
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nahi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xbb

    .line 319
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ohi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xbc

    .line 320
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ki"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xbd

    .line 321
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "toki"

    const/16 v6, 0xbc

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xbe

    .line 322
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xbf

    .line 323
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "goi"

    const/16 v6, 0xbe

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xc0

    .line 324
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "koi"

    const/16 v6, 0xbe

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xc1

    .line 325
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ari"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xc2

    .line 326
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kari"

    const/16 v6, 0xc1

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xc3

    .line 327
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "lari"

    const/16 v6, 0xc1

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xc4

    .line 328
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tari"

    const/16 v6, 0xc1

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xc5

    .line 329
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "garri"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xc6

    .line 330
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "larri"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xc7

    .line 331
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kirri"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xc8

    .line 332
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "duri"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xc9

    .line 333
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xca

    .line 334
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ti"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xcb

    .line 335
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ontzi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xcc

    .line 336
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f1i"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xcd

    .line 337
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ak"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xce

    .line 338
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ek"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xcf

    .line 339
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tarik"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xd0

    .line 340
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gibel"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xd1

    .line 341
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ail"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xd2

    .line 342
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kail"

    const/16 v6, 0xd1

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xd3

    .line 343
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kan"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xd4

    .line 344
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tan"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xd5

    .line 345
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "etan"

    const/16 v6, 0xd4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xd6

    .line 346
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "en"

    const/4 v7, 0x4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xd7

    .line 347
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ren"

    const/16 v6, 0xd6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xd8

    .line 348
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "garren"

    const/16 v6, 0xd7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xd9

    .line 349
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gerren"

    const/16 v6, 0xd7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xda

    .line 350
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "urren"

    const/16 v6, 0xd7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xdb

    .line 351
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ten"

    const/16 v6, 0xd6

    const/4 v7, 0x4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xdc

    .line 352
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzen"

    const/16 v6, 0xd6

    const/4 v7, 0x4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xdd

    .line 353
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zain"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xde

    .line 354
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzain"

    const/16 v6, 0xdd

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xdf

    .line 355
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kin"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xe0

    .line 356
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "min"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xe1

    .line 357
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dun"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xe2

    .line 358
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asun"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xe3

    .line 359
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tasun"

    const/16 v6, 0xe2

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xe4

    .line 360
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aizun"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xe5

    .line 361
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ondo"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xe6

    .line 362
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kondo"

    const/16 v6, 0xe5

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xe7

    .line 363
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "go"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xe8

    .line 364
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ngo"

    const/16 v6, 0xe7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xe9

    .line 365
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zio"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xea

    .line 366
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ko"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xeb

    .line 367
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "trako"

    const/16 v6, 0xea

    const/4 v7, 0x5

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xec

    .line 368
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tako"

    const/16 v6, 0xea

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xed

    .line 369
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "etako"

    const/16 v6, 0xec

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xee

    .line 370
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eko"

    const/16 v6, 0xea

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xef

    .line 371
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tariko"

    const/16 v6, 0xea

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xf0

    .line 372
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "sko"

    const/16 v6, 0xea

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xf1

    .line 373
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tuko"

    const/16 v6, 0xea

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xf2

    .line 374
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "minutuko"

    const/16 v6, 0xf1

    const/4 v7, 0x6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xf3

    .line 375
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zko"

    const/16 v6, 0xea

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xf4

    .line 376
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "no"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xf5

    .line 377
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zino"

    const/16 v6, 0xf4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xf6

    .line 378
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ro"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xf7

    .line 379
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "aro"

    const/16 v6, 0xf6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xf8

    .line 380
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "igaro"

    const/16 v6, 0xf7

    const/16 v7, 0x9

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xf9

    .line 381
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "taro"

    const/16 v6, 0xf7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xfa

    .line 382
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zaro"

    const/16 v6, 0xf7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xfb

    .line 383
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ero"

    const/16 v6, 0xf6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xfc

    .line 384
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "giro"

    const/16 v6, 0xf6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xfd

    .line 385
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "oro"

    const/16 v6, 0xf6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xfe

    .line 386
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oso"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xff

    .line 387
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "to"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x100

    .line 388
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tto"

    const/16 v6, 0xff

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x101

    .line 389
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zto"

    const/16 v6, 0xff

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x102

    .line 390
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "txo"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x103

    .line 391
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tzo"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x104

    .line 392
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gintzo"

    const/16 v6, 0x103

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x105

    .line 393
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f1o"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x106

    .line 394
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zp"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x107

    .line 395
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x108

    .line 396
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "dar"

    const/16 v6, 0x107

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x109

    .line 397
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "behar"

    const/16 v6, 0x107

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x10a

    .line 398
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zehar"

    const/16 v6, 0x107

    const/4 v7, 0x7

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x10b

    .line 399
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "liar"

    const/16 v6, 0x107

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x10c

    .line 400
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tiar"

    const/16 v6, 0x107

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x10d

    .line 401
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tar"

    const/16 v6, 0x107

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x10e

    .line 402
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzar"

    const/16 v6, 0x107

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x10f

    .line 403
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "or"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x110

    .line 404
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "kor"

    const/16 v6, 0x10f

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x111

    .line 405
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "os"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x112

    .line 406
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ket"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x113

    .line 407
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "du"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x114

    .line 408
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "mendu"

    const/16 v6, 0x113

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x115

    .line 409
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ordu"

    const/16 v6, 0x113

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x116

    .line 410
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "leku"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x117

    .line 411
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "buru"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x118

    .line 412
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "duru"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x119

    .line 413
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tsu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x11a

    .line 414
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x11b

    .line 415
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tatu"

    const/16 v6, 0x11a

    const/4 v7, 0x4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x11c

    .line 416
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "mentu"

    const/16 v6, 0x11a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x11d

    .line 417
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "estu"

    const/16 v6, 0x11a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x11e

    .line 418
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "txu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x11f

    .line 419
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x120

    .line 420
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tzu"

    const/16 v6, 0x11f

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x121

    .line 421
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "gintzu"

    const/16 v6, 0x120

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x122

    .line 422
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "z"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x123

    .line 423
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ez"

    const/16 v6, 0x122

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x124

    .line 424
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eroz"

    const/16 v6, 0x122

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x125

    .line 425
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "tz"

    const/16 v6, 0x122

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x126

    .line 426
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "koitz"

    const/16 v6, 0x125

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 131
    sput-object v10, Lorg/tartarus/snowball/ext/BasqueStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 429
    const/16 v0, 0x13

    new-array v10, v0, [Lorg/tartarus/snowball/Among;

    const/4 v0, 0x0

    .line 430
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "zlea"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 431
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "keria"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v3

    .line 432
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "la"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v11

    const/4 v6, 0x3

    .line 433
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "era"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x4

    .line 434
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dade"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x5

    .line 435
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tade"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x6

    .line 436
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "date"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x7

    .line 437
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tate"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x8

    .line 438
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x9

    .line 439
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ki"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xa

    .line 440
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ik"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xb

    .line 441
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "lanik"

    const/16 v6, 0xa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xc

    .line 442
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "rik"

    const/16 v6, 0xa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xd

    .line 443
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "larik"

    const/16 v6, 0xc

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xe

    .line 444
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "ztik"

    const/16 v6, 0xa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xf

    .line 445
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "go"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x10

    .line 446
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ro"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x11

    .line 447
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ero"

    const/16 v6, 0x10

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x12

    .line 448
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "to"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->methodObject:Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 429
    sput-object v10, Lorg/tartarus/snowball/ext/BasqueStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 451
    const/4 v0, 0x3

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    return-void

    :array_0
    .array-data 2
        0x11s
        0x41s
        0x10s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/BasqueStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/BasqueStemmer;

    .prologue
    .line 458
    iget v0, p1, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_p2:I

    .line 459
    iget v0, p1, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_p1:I

    .line 460
    iget v0, p1, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_pV:I

    iput v0, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_pV:I

    .line 461
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 462
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 684
    iget v0, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 686
    const/4 v0, 0x0

    .line 688
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 676
    iget v0, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 678
    const/4 v0, 0x0

    .line 680
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_RV()Z
    .locals 2

    .prologue
    .line 668
    iget v0, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_pV:I

    iget v1, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 670
    const/4 v0, 0x0

    .line 672
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_aditzak()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 695
    iget v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->ket:I

    .line 697
    sget-object v2, Lorg/tartarus/snowball/ext/BasqueStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x6d

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/BasqueStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 698
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 743
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 703
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->bra:I

    .line 704
    packed-switch v0, :pswitch_data_0

    .line 743
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 710
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->r_RV()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 715
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_del()V

    goto :goto_1

    .line 720
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 725
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_del()V

    goto :goto_1

    .line 730
    :pswitch_3
    const-string v1, "atseden"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 735
    :pswitch_4
    const-string v1, "arabera"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 740
    :pswitch_5
    const-string v1, "baditu"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 704
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private r_adjetiboak()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 835
    iget v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->ket:I

    .line 837
    sget-object v2, Lorg/tartarus/snowball/ext/BasqueStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x13

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/BasqueStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 838
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 863
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 843
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->bra:I

    .line 844
    packed-switch v0, :pswitch_data_0

    .line 863
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 850
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->r_RV()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 855
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_del()V

    goto :goto_1

    .line 860
    :pswitch_2
    const-string/jumbo v1, "z"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 844
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_izenak()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 750
    iget v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->ket:I

    .line 752
    sget-object v2, Lorg/tartarus/snowball/ext/BasqueStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x127

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/BasqueStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 753
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 828
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 758
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->bra:I

    .line 759
    packed-switch v0, :pswitch_data_0

    .line 828
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 765
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->r_RV()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 770
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_del()V

    goto :goto_1

    .line 775
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 780
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_del()V

    goto :goto_1

    .line 785
    :pswitch_3
    const-string v1, "jok"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 790
    :pswitch_4
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 795
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_del()V

    goto :goto_1

    .line 800
    :pswitch_5
    const-string v1, "tra"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 805
    :pswitch_6
    const-string v1, "minutu"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 810
    :pswitch_7
    const-string/jumbo v1, "zehar"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 815
    :pswitch_8
    const-string v1, "geldi"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 820
    :pswitch_9
    const-string v1, "igaro"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 825
    :pswitch_a
    const-string v1, "aurka"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/BasqueStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 759
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 9

    .prologue
    const/16 v8, 0x75

    const/16 v7, 0x61

    .line 471
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_pV:I

    .line 472
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_p1:I

    .line 473
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_p2:I

    .line 475
    iget v0, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 480
    .local v0, "v_1":I
    iget v1, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 483
    .local v1, "v_2":I
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_3

    .line 539
    :cond_0
    iput v1, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 541
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_6

    .line 589
    :cond_1
    :goto_0
    iput v0, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 591
    iget v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 598
    .local v4, "v_8":I
    :goto_1
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_c

    .line 604
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    if-lt v5, v6, :cond_a

    .line 663
    :cond_2
    :goto_2
    iput v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 664
    const/4 v5, 0x1

    return v5

    .line 489
    .end local v4    # "v_8":I
    :cond_3
    iget v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 492
    .local v2, "v_3":I
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_5

    .line 514
    :goto_3
    iput v2, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 516
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->in_grouping([CII)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 524
    :goto_4
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 530
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    if-ge v5, v6, :cond_0

    .line 534
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    goto :goto_4

    .line 510
    :cond_4
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 500
    :cond_5
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 506
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    if-lt v5, v6, :cond_4

    goto :goto_3

    .line 547
    .end local v2    # "v_3":I
    :cond_6
    iget v3, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 550
    .local v3, "v_6":I
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_9

    .line 572
    :goto_5
    iput v3, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 574
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->in_grouping([CII)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 579
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    if-ge v5, v6, :cond_1

    .line 583
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 587
    .end local v3    # "v_6":I
    :cond_7
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_pV:I

    goto :goto_0

    .line 568
    .restart local v3    # "v_6":I
    :cond_8
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 558
    :cond_9
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 564
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    if-lt v5, v6, :cond_8

    goto :goto_5

    .line 608
    .end local v3    # "v_6":I
    .restart local v4    # "v_8":I
    :cond_a
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    goto/16 :goto_1

    .line 624
    :cond_b
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 614
    :cond_c
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_d

    .line 620
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    if-lt v5, v6, :cond_b

    goto/16 :goto_2

    .line 627
    :cond_d
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_p1:I

    .line 632
    :goto_6
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_f

    .line 638
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    if-ge v5, v6, :cond_2

    .line 642
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    goto :goto_6

    .line 658
    :cond_e
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 648
    :cond_f
    sget-object v5, Lorg/tartarus/snowball/ext/BasqueStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/BasqueStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_10

    .line 654
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    if-lt v5, v6, :cond_e

    goto/16 :goto_2

    .line 661
    :cond_10
    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->I_p2:I

    goto/16 :goto_2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 931
    instance-of v0, p1, Lorg/tartarus/snowball/ext/BasqueStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 936
    const-class v0, Lorg/tartarus/snowball/ext/BasqueStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 6

    .prologue
    .line 874
    iget v0, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 877
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->r_mark_regions()Z

    move-result v4

    if-nez v4, :cond_0

    .line 882
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 884
    iget v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit_backward:I

    iget v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    iput v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 889
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 892
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->r_aditzak()Z

    move-result v4

    if-nez v4, :cond_1

    .line 898
    iget v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 904
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    sub-int v2, v4, v5

    .line 907
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->r_izenak()Z

    move-result v4

    if-nez v4, :cond_2

    .line 913
    iget v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 917
    iget v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    sub-int v3, v4, v5

    .line 920
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/BasqueStemmer;->r_adjetiboak()Z

    move-result v4

    if-nez v4, :cond_3

    .line 925
    :cond_3
    iget v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    .line 926
    iget v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->limit_backward:I

    iput v4, p0, Lorg/tartarus/snowball/ext/BasqueStemmer;->cursor:I

    const/4 v4, 0x1

    return v4
.end method
