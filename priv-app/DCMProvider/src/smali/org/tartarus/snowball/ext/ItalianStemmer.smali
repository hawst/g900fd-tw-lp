.class public Lorg/tartarus/snowball/ext/ItalianStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "ItalianStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final a_6:[Lorg/tartarus/snowball/Among;

.field private static final a_7:[Lorg/tartarus/snowball/Among;

.field private static final g_AEIO:[C

.field private static final g_CG:[C

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_p2:I

.field private I_pV:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/ItalianStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    .line 19
    const/4 v0, 0x7

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "qu"

    const/4 v2, 0x0

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ed"

    const/4 v2, 0x0

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f3"

    const/4 v2, 0x0

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00fa"

    const/4 v2, 0x0

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 29
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 30
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 31
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "I"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "U"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 29
    sput-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 35
    const/16 v0, 0x25

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "la"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "cela"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gliela"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mela"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tela"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "vela"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "le"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 43
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "cele"

    const/4 v2, 0x6

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 44
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gliele"

    const/4 v2, 0x6

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mele"

    const/4 v2, 0x6

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tele"

    const/4 v2, 0x6

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "vele"

    const/4 v2, 0x6

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ne"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "cene"

    const/16 v2, 0xc

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gliene"

    const/16 v2, 0xc

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mene"

    const/16 v2, 0xc

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sene"

    const/16 v2, 0xc

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tene"

    const/16 v2, 0xc

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "vene"

    const/16 v2, 0xc

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ci"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "li"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "celi"

    const/16 v2, 0x14

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "glieli"

    const/16 v2, 0x14

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "meli"

    const/16 v2, 0x14

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 60
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "teli"

    const/16 v2, 0x14

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "veli"

    const/16 v2, 0x14

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gli"

    const/16 v2, 0x14

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mi"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "si"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ti"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "vi"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lo"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "celo"

    const/16 v2, 0x1f

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 69
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "glielo"

    const/16 v2, 0x1f

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 70
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "melo"

    const/16 v2, 0x1f

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 71
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "telo"

    const/16 v2, 0x1f

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "velo"

    const/16 v2, 0x1f

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 35
    sput-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 75
    const/4 v0, 0x5

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 76
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ando"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 77
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "endo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 79
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 75
    sput-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 83
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 85
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abil"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "os"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iv"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 83
    sput-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 90
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 91
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 92
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abil"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 93
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iv"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 90
    sput-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 96
    const/16 v0, 0x33

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 97
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ica"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 98
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "logia"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osa"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ista"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 101
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iva"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "anza"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 103
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "enza"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 104
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ice"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 105
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atrice"

    const/4 v2, 0x7

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iche"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "logie"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abile"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 109
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ibile"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "usione"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 111
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "azione"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uzione"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atore"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ose"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 115
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ante"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 116
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mente"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amente"

    const/16 v2, 0x13

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 118
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iste"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 119
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ive"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "anze"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 121
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "enze"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 122
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ici"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 123
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atrici"

    const/16 v2, 0x19

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 124
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ichi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 125
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abili"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 126
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ibili"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 127
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ismi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 128
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "usioni"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 129
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "azioni"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 130
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uzioni"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 131
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atori"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 132
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 133
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "anti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amenti"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x26

    .line 135
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imenti"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x27

    .line 136
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x28

    .line 137
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivi"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x29

    .line 138
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ico"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2a

    .line 139
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ismo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2b

    .line 140
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oso"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2c

    .line 141
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amento"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2d

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imento"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2e

    .line 143
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivo"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2f

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "it\u00e0"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x30

    .line 145
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ist\u00e0"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x31

    .line 146
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ist\u00e8"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x32

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ist\u00ec"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 96
    sput-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    .line 150
    const/16 v0, 0x57

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 151
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isca"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 152
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "enda"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 153
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ata"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 154
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ita"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 155
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uta"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 156
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ava"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 157
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eva"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 158
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iva"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 159
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erebbe"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 160
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irebbe"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 161
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isce"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 162
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ende"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 163
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "are"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 164
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ere"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 165
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ire"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 166
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asse"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 167
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ate"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 168
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "avate"

    const/16 v2, 0x10

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 169
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "evate"

    const/16 v2, 0x10

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 170
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivate"

    const/16 v2, 0x10

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 171
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ete"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 172
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erete"

    const/16 v2, 0x14

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 173
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irete"

    const/16 v2, 0x14

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 174
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ite"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 175
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ereste"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 176
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ireste"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 177
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ute"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 178
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erai"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 179
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irai"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 180
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isci"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 181
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "endi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 182
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erei"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 183
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irei"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 184
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "assi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 185
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ati"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 186
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 187
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eresti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 188
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iresti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x26

    .line 189
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x27

    .line 190
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "avi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x28

    .line 191
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "evi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x29

    .line 192
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2a

    .line 193
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isco"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2b

    .line 194
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ando"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2c

    .line 195
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "endo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2d

    .line 196
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "Yamo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2e

    .line 197
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iamo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2f

    .line 198
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "avamo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x30

    .line 199
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "evamo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x31

    .line 200
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivamo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x32

    .line 201
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eremo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x33

    .line 202
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iremo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x34

    .line 203
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "assimo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x35

    .line 204
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ammo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x36

    .line 205
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "emmo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x37

    .line 206
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eremmo"

    const/16 v2, 0x36

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x38

    .line 207
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iremmo"

    const/16 v2, 0x36

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x39

    .line 208
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "immo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3a

    .line 209
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ano"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3b

    .line 210
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iscano"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3c

    .line 211
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "avano"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3d

    .line 212
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "evano"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3e

    .line 213
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivano"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3f

    .line 214
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eranno"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x40

    .line 215
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iranno"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x41

    .line 216
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ono"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x42

    .line 217
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iscono"

    const/16 v2, 0x41

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x43

    .line 218
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "arono"

    const/16 v2, 0x41

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x44

    .line 219
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erono"

    const/16 v2, 0x41

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x45

    .line 220
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irono"

    const/16 v2, 0x41

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x46

    .line 221
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erebbero"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x47

    .line 222
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irebbero"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x48

    .line 223
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "assero"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x49

    .line 224
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "essero"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4a

    .line 225
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issero"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4b

    .line 226
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ato"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4c

    .line 227
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ito"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4d

    .line 228
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uto"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4e

    .line 229
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "avo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4f

    .line 230
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "evo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x50

    .line 231
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x51

    .line 232
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x52

    .line 233
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x53

    .line 234
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e0"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x54

    .line 235
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e0"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x55

    .line 236
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00f2"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x56

    .line 237
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00f2"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 150
    sput-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    .line 240
    const/16 v0, 0x14

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x80

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x80

    aput-char v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x8

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/4 v2, 0x2

    aput-char v2, v0, v1

    const/16 v1, 0x13

    const/4 v2, 0x1

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    .line 242
    const/16 v0, 0x13

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x80

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x80

    aput-char v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x8

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/4 v2, 0x2

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_AEIO:[C

    .line 244
    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_CG:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/ItalianStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/ItalianStemmer;

    .prologue
    .line 251
    iget v0, p1, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_p2:I

    .line 252
    iget v0, p1, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_p1:I

    .line 253
    iget v0, p1, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_pV:I

    iput v0, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_pV:I

    .line 254
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 255
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 665
    iget v0, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 667
    const/4 v0, 0x0

    .line 669
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 673
    iget v0, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 675
    const/4 v0, 0x0

    .line 677
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_RV()Z
    .locals 2

    .prologue
    .line 657
    iget v0, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_pV:I

    iget v1, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 659
    const/4 v0, 0x0

    .line 661
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_attached_pronoun()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 684
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 686
    sget-object v2, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x25

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/ItalianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v2

    if-nez v2, :cond_1

    .line 718
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 691
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 693
    sget-object v2, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x5

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/ItalianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 694
    .local v0, "among_var":I
    if-eqz v0, :cond_0

    .line 700
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_RV()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 704
    packed-switch v0, :pswitch_data_0

    .line 718
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 710
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    goto :goto_1

    .line 715
    :pswitch_2
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 704
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 9

    .prologue
    const/16 v8, 0xf9

    const/16 v7, 0x61

    .line 410
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_pV:I

    .line 411
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_p1:I

    .line 412
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_p2:I

    .line 414
    iget v0, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 419
    .local v0, "v_1":I
    iget v1, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 422
    .local v1, "v_2":I
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_3

    .line 478
    :cond_0
    iput v1, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 480
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_6

    .line 528
    :cond_1
    :goto_0
    iput v0, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 530
    iget v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 537
    .local v4, "v_8":I
    :goto_1
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_c

    .line 543
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    if-lt v5, v6, :cond_a

    .line 602
    :cond_2
    :goto_2
    iput v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 603
    const/4 v5, 0x1

    return v5

    .line 428
    .end local v4    # "v_8":I
    :cond_3
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 431
    .local v2, "v_3":I
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_5

    .line 453
    :goto_3
    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 455
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping([CII)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 463
    :goto_4
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 469
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    if-ge v5, v6, :cond_0

    .line 473
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_4

    .line 449
    :cond_4
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 439
    :cond_5
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 445
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    if-lt v5, v6, :cond_4

    goto :goto_3

    .line 486
    .end local v2    # "v_3":I
    :cond_6
    iget v3, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 489
    .local v3, "v_6":I
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_9

    .line 511
    :goto_5
    iput v3, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 513
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping([CII)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 518
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    if-ge v5, v6, :cond_1

    .line 522
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 526
    .end local v3    # "v_6":I
    :cond_7
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_pV:I

    goto :goto_0

    .line 507
    .restart local v3    # "v_6":I
    :cond_8
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 497
    :cond_9
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 503
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    if-lt v5, v6, :cond_8

    goto :goto_5

    .line 547
    .end local v3    # "v_6":I
    .restart local v4    # "v_8":I
    :cond_a
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 563
    :cond_b
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 553
    :cond_c
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_d

    .line 559
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    if-lt v5, v6, :cond_b

    goto/16 :goto_2

    .line 566
    :cond_d
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_p1:I

    .line 571
    :goto_6
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_f

    .line 577
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    if-ge v5, v6, :cond_2

    .line 581
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_6

    .line 597
    :cond_e
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 587
    :cond_f
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/ItalianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_10

    .line 593
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    if-lt v5, v6, :cond_e

    goto/16 :goto_2

    .line 600
    :cond_10
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_p2:I

    goto/16 :goto_2
.end method

.method private r_postlude()Z
    .locals 4

    .prologue
    .line 612
    :goto_0
    iget v1, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 616
    .local v1, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 618
    sget-object v2, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/ItalianStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 619
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 650
    :cond_0
    :pswitch_0
    iput v1, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 653
    const/4 v2, 0x1

    return v2

    .line 624
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 625
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 631
    :pswitch_1
    const-string v2, "i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 636
    :pswitch_2
    const-string v2, "u"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 641
    :pswitch_3
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 645
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_0

    .line 625
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_prelude()Z
    .locals 11

    .prologue
    const/16 v10, 0xf9

    const/16 v9, 0x61

    const/4 v8, 0x1

    .line 266
    iget v1, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 270
    .local v1, "v_1":I
    :goto_0
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 274
    .local v2, "v_2":I
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 276
    sget-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/4 v7, 0x7

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/ItalianStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 277
    .local v0, "among_var":I
    if-nez v0, :cond_2

    .line 328
    :cond_0
    :pswitch_0
    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 331
    iput v1, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 335
    :goto_1
    iget v3, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 340
    .local v3, "v_3":I
    :goto_2
    iget v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 343
    .local v4, "v_4":I
    sget-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v6, v9, v10}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping([CII)Z

    move-result v6

    if-nez v6, :cond_3

    .line 388
    :cond_1
    iput v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 389
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    if-lt v6, v7, :cond_6

    .line 397
    iput v3, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 400
    return v8

    .line 282
    .end local v3    # "v_3":I
    .end local v4    # "v_4":I
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 283
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 289
    :pswitch_1
    const-string/jumbo v6, "\u00e0"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 294
    :pswitch_2
    const-string/jumbo v6, "\u00e8"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 299
    :pswitch_3
    const-string/jumbo v6, "\u00ec"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 304
    :pswitch_4
    const-string/jumbo v6, "\u00f2"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 309
    :pswitch_5
    const-string/jumbo v6, "\u00f9"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 314
    :pswitch_6
    const-string v6, "qU"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 319
    :pswitch_7
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    if-ge v6, v7, :cond_0

    .line 323
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_0

    .line 348
    .restart local v3    # "v_3":I
    .restart local v4    # "v_4":I
    :cond_3
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 351
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 355
    .local v5, "v_5":I
    const-string v6, "u"

    invoke-virtual {p0, v8, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 369
    :cond_4
    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 372
    const-string v6, "i"

    invoke-virtual {p0, v8, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 377
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 378
    sget-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v6, v9, v10}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping([CII)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 383
    const-string v6, "I"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 385
    :goto_3
    iput v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_1

    .line 360
    :cond_5
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 361
    sget-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_v:[C

    invoke-virtual {p0, v6, v9, v10}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping([CII)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 366
    const-string v6, "U"

    invoke-virtual {p0, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 393
    .end local v5    # "v_5":I
    :cond_6
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_2

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private r_standard_suffix()Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x0

    .line 729
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 731
    sget-object v6, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    const/16 v7, 0x33

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/ItalianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 732
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 978
    :cond_0
    :goto_0
    :pswitch_0
    return v5

    .line 737
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 738
    packed-switch v0, :pswitch_data_0

    .line 978
    :goto_1
    const/4 v5, 0x1

    goto :goto_0

    .line 744
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 749
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    goto :goto_1

    .line 754
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 759
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    .line 761
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 765
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 767
    const-string v5, "ic"

    invoke-virtual {p0, v8, v5}, Lorg/tartarus/snowball/ext/ItalianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 769
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_1

    .line 773
    :cond_2
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 775
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_3

    .line 777
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_1

    .line 781
    :cond_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    goto :goto_1

    .line 787
    .end local v1    # "v_1":I
    :pswitch_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 792
    const-string v5, "log"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 797
    :pswitch_4
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 802
    const-string v5, "u"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 807
    :pswitch_5
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 812
    const-string v5, "ente"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 817
    :pswitch_6
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_RV()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 822
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    goto :goto_1

    .line 827
    :pswitch_7
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R1()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 832
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    .line 834
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 838
    .local v2, "v_2":I
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 840
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/4 v6, 0x4

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 841
    if-nez v0, :cond_4

    .line 843
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 847
    :cond_4
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 849
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_5

    .line 851
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 855
    :cond_5
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    .line 856
    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    .line 858
    :pswitch_8
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 863
    :pswitch_9
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 865
    const-string v5, "at"

    invoke-virtual {p0, v8, v5}, Lorg/tartarus/snowball/ext/ItalianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 867
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 871
    :cond_6
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 873
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_7

    .line 875
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 879
    :cond_7
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    goto/16 :goto_1

    .line 887
    .end local v2    # "v_2":I
    :pswitch_a
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 892
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    .line 894
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    sub-int v3, v5, v6

    .line 898
    .local v3, "v_3":I
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 900
    sget-object v5, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/4 v6, 0x3

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/ItalianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 901
    if-nez v0, :cond_8

    .line 903
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 907
    :cond_8
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 908
    packed-switch v0, :pswitch_data_2

    goto/16 :goto_1

    .line 910
    :pswitch_b
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 915
    :pswitch_c
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_9

    .line 917
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 921
    :cond_9
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    goto/16 :goto_1

    .line 929
    .end local v3    # "v_3":I
    :pswitch_d
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 934
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    .line 936
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    sub-int v4, v5, v6

    .line 940
    .local v4, "v_4":I
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 942
    const-string v5, "at"

    invoke-virtual {p0, v8, v5}, Lorg/tartarus/snowball/ext/ItalianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 944
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 948
    :cond_a
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 950
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_b

    .line 952
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 956
    :cond_b
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    .line 958
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 960
    const-string v5, "ic"

    invoke-virtual {p0, v8, v5}, Lorg/tartarus/snowball/ext/ItalianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 962
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 966
    :cond_c
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 968
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_d

    .line 970
    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto/16 :goto_1

    .line 974
    :cond_d
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    goto/16 :goto_1

    .line 738
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_d
    .end packed-switch

    .line 856
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 908
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method private r_verb_suffix()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 986
    iget v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 988
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_pV:I

    if-ge v4, v5, :cond_0

    .line 1019
    :goto_0
    return v3

    .line 992
    :cond_0
    iget v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->I_pV:I

    iput v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 993
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit_backward:I

    .line 994
    .local v2, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit_backward:I

    .line 995
    iget v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 998
    iget v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 1000
    sget-object v4, Lorg/tartarus/snowball/ext/ItalianStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x57

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/ItalianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1001
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 1003
    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit_backward:I

    goto :goto_0

    .line 1007
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 1008
    packed-switch v0, :pswitch_data_0

    .line 1018
    :goto_1
    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit_backward:I

    .line 1019
    const/4 v3, 0x1

    goto :goto_0

    .line 1010
    :pswitch_0
    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit_backward:I

    goto :goto_0

    .line 1015
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    goto :goto_1

    .line 1008
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_vowel_suffix()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1027
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iget v3, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    sub-int v0, v2, v3

    .line 1031
    .local v0, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 1032
    sget-object v2, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_AEIO:[C

    const/16 v3, 0x61

    const/16 v4, 0xf2

    invoke-virtual {p0, v2, v3, v4}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping_b([CII)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1034
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1067
    :goto_0
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iget v3, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    sub-int v1, v2, v3

    .line 1071
    .local v1, "v_2":I
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 1073
    const-string v2, "h"

    invoke-virtual {p0, v5, v2}, Lorg/tartarus/snowball/ext/ItalianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1075
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v2, v1

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1094
    :goto_1
    return v5

    .line 1038
    .end local v1    # "v_2":I
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 1040
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_RV()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1042
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_0

    .line 1046
    :cond_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    .line 1048
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->ket:I

    .line 1050
    const-string v2, "i"

    invoke-virtual {p0, v5, v2}, Lorg/tartarus/snowball/ext/ItalianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1052
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_0

    .line 1056
    :cond_2
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 1058
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_RV()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1060
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_0

    .line 1064
    :cond_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    goto :goto_0

    .line 1079
    .restart local v1    # "v_2":I
    :cond_4
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->bra:I

    .line 1080
    sget-object v2, Lorg/tartarus/snowball/ext/ItalianStemmer;->g_CG:[C

    const/16 v3, 0x63

    const/16 v4, 0x67

    invoke-virtual {p0, v2, v3, v4}, Lorg/tartarus/snowball/ext/ItalianStemmer;->in_grouping_b([CII)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1082
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v2, v1

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_1

    .line 1086
    :cond_5
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_RV()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1088
    iget v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v2, v1

    iput v2, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    goto :goto_1

    .line 1092
    :cond_6
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->slice_del()V

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1189
    instance-of v0, p1, Lorg/tartarus/snowball/ext/ItalianStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1194
    const-class v0, Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 9

    .prologue
    .line 1108
    iget v0, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1111
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_prelude()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1116
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1118
    iget v1, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1121
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_mark_regions()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1126
    :cond_1
    iput v1, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1128
    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit_backward:I

    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iput v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1131
    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    sub-int v2, v7, v8

    .line 1134
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_attached_pronoun()Z

    move-result v7

    if-nez v7, :cond_2

    .line 1139
    :cond_2
    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v7, v2

    iput v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1141
    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    sub-int v3, v7, v8

    .line 1146
    .local v3, "v_4":I
    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    sub-int v4, v7, v8

    .line 1149
    .local v4, "v_5":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_standard_suffix()Z

    move-result v7

    if-nez v7, :cond_3

    .line 1155
    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v7, v4

    iput v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1157
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_verb_suffix()Z

    move-result v7

    if-nez v7, :cond_3

    .line 1163
    :cond_3
    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v7, v3

    iput v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1165
    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    sub-int v5, v7, v8

    .line 1168
    .local v5, "v_6":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_vowel_suffix()Z

    move-result v7

    if-nez v7, :cond_4

    .line 1173
    :cond_4
    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit:I

    sub-int/2addr v7, v5

    iput v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1174
    iget v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->limit_backward:I

    iput v7, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1175
    iget v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1178
    .local v6, "v_7":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ItalianStemmer;->r_postlude()Z

    move-result v7

    if-nez v7, :cond_5

    .line 1183
    :cond_5
    iput v6, p0, Lorg/tartarus/snowball/ext/ItalianStemmer;->cursor:I

    .line 1184
    const/4 v7, 0x1

    return v7
.end method
