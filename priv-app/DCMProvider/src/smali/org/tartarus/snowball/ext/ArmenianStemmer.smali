.class public Lorg/tartarus/snowball/ext/ArmenianStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "ArmenianStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p2:I

.field private I_pV:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/16 v13, 0xd

    const/16 v12, 0x8

    const/16 v11, 0x1b

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/ArmenianStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    .line 19
    const/16 v0, 0x17

    new-array v10, v0, [Lorg/tartarus/snowball/Among;

    const/4 v6, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0580\u0578\u0580\u0564"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 21
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0580\u0578\u0580\u0564"

    const/4 v6, 0x0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v3

    const/4 v6, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u056c\u056b"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x3

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u056f\u056b"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0580\u0561\u056f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0572"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x6

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u056f\u0561\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x7

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0580\u0561\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v12

    const/16 v0, 0x9

    .line 29
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u056f\u0565\u0576"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xa

    .line 30
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0580\u0565\u0576"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xb

    .line 31
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0580\u0567\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xc

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u056b\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 33
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0563\u056b\u0576"

    const/16 v6, 0xc

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v13

    const/16 v0, 0xe

    .line 34
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u057e\u056b\u0576"

    const/16 v6, 0xc

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xf

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u056c\u0561\u0575\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x10

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u057e\u0578\u0582\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x11

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u057a\u0565\u057d"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x12

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u056b\u057e"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x13

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u057f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x14

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u057e\u0565\u057f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x15

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u056f\u0578\u057f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x16

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0562\u0561\u0580"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 19
    sput-object v10, Lorg/tartarus/snowball/ext/ArmenianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 45
    const/16 v0, 0x47

    new-array v10, v0, [Lorg/tartarus/snowball/Among;

    const/4 v6, 0x0

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 47
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0581\u0561"

    const/4 v6, 0x0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v3

    const/4 v0, 0x2

    .line 48
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0581\u0561"

    const/4 v6, 0x0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v6, 0x3

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u057e\u0565"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x4

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0581\u0580\u056b"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x5

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0581\u056b"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x6

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0581\u056b"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v0, 0x7

    .line 53
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0565\u0581\u056b"

    const/4 v6, 0x6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u056c"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v12

    const/16 v0, 0x9

    .line 55
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0568\u0561\u056c"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xa

    .line 56
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0576\u0561\u056c"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xb

    .line 57
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0576\u0561\u056c"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xc

    .line 58
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0581\u0576\u0561\u056c"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u056c"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v13

    const/16 v0, 0xe

    .line 60
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0568\u0565\u056c"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xf

    .line 61
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0576\u0565\u056c"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x10

    .line 62
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0581\u0576\u0565\u056c"

    const/16 v6, 0xf

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x11

    .line 63
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0581\u0576\u0565\u056c"

    const/16 v6, 0x10

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x12

    .line 64
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0579\u0565\u056c"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x13

    .line 65
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0565\u056c"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x14

    .line 66
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0581\u057e\u0565\u056c"

    const/16 v6, 0x13

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x15

    .line 67
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0581\u057e\u0565\u056c"

    const/16 v6, 0x13

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x16

    .line 68
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057f\u0565\u056c"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x17

    .line 69
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u057f\u0565\u056c"

    const/16 v6, 0x16

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x18

    .line 70
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u057f\u0565\u056c"

    const/16 v6, 0x16

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x19

    .line 71
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u056f\u0578\u057f\u0565\u056c"

    const/16 v6, 0x18

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x1a

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u057e\u0561\u056e"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0582\u0574"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v11

    const/16 v0, 0x1c

    .line 74
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0578\u0582\u0574"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x1d

    .line 75
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x1e

    .line 76
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0581\u0561\u0576"

    const/16 v6, 0x1d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x1f

    .line 77
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0581\u0561\u0576"

    const/16 v6, 0x1e

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x20

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0581\u0580\u056b\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x21

    .line 79
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0581\u056b\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x22

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0581\u056b\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x23

    .line 81
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0565\u0581\u056b\u0576"

    const/16 v6, 0x22

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x24

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u056c\u056b\u057d"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x25

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u056c\u056b\u057d"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x26

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u057e"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x27

    .line 85
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0581\u0561\u057e"

    const/16 v6, 0x26

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x28

    .line 86
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0581\u0561\u057e"

    const/16 v6, 0x26

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x29

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u056c\u0578\u057e"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x2a

    .line 88
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u056c\u0578\u057e"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x2b

    .line 89
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0580"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x2c

    .line 90
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0581\u0561\u0580"

    const/16 v6, 0x2b

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x2d

    .line 91
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0581\u0561\u0580"

    const/16 v6, 0x2b

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x2e

    .line 92
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0581\u0580\u056b\u0580"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x2f

    .line 93
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0581\u056b\u0580"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x30

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0581\u056b\u0580"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x31

    .line 95
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0565\u0581\u056b\u0580"

    const/16 v6, 0x30

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x32

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0581"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x33

    .line 97
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0581"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x34

    .line 98
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0581\u0580\u0565\u0581"

    const/16 v6, 0x33

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x35

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u056c\u0578\u0582\u0581"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x36

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u056c\u0578\u0582\u0581"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x37

    .line 101
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u056c\u0578\u0582"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x38

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u056c\u0578\u0582"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x39

    .line 103
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0584"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x3a

    .line 104
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0581\u0561\u0584"

    const/16 v6, 0x39

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x3b

    .line 105
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0581\u0561\u0584"

    const/16 v6, 0x3a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x3c

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0581\u0580\u056b\u0584"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x3d

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0581\u056b\u0584"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x3e

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0581\u056b\u0584"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x3f

    .line 109
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0565\u0581\u056b\u0584"

    const/16 v6, 0x3e

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x40

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0576\u0584"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x41

    .line 111
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0581\u0561\u0576\u0584"

    const/16 v6, 0x40

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x42

    .line 112
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0581\u0561\u0576\u0584"

    const/16 v6, 0x41

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x43

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0581\u0580\u056b\u0576\u0584"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x44

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0581\u056b\u0576\u0584"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x45

    .line 115
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0581\u056b\u0576\u0584"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x46

    .line 116
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0565\u0581\u056b\u0576\u0584"

    const/16 v6, 0x45

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 45
    sput-object v10, Lorg/tartarus/snowball/ext/ArmenianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 119
    const/16 v0, 0x28

    new-array v10, v0, [Lorg/tartarus/snowball/Among;

    const/4 v6, 0x0

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0580\u0564"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 121
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0582\u0575\u0569"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v3

    const/4 v6, 0x2

    .line 122
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0582\u0570\u056b"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x3

    .line 123
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0581\u056b"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x4

    .line 124
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u056b\u056c"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x5

    .line 125
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u056f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v0, 0x6

    .line 126
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0575\u0561\u056f"

    const/4 v6, 0x5

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x7

    .line 127
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0576\u0561\u056f"

    const/4 v6, 0x5

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 128
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u056b\u056f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v12

    const/16 v6, 0x9

    .line 129
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0582\u056f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xa

    .line 130
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xb

    .line 131
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057a\u0561\u0576"

    const/16 v6, 0xa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xc

    .line 132
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057d\u057f\u0561\u0576"

    const/16 v6, 0xa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 133
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0580\u0561\u0576"

    const/16 v6, 0xa

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v13

    const/16 v6, 0xe

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0572\u0567\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xf

    .line 135
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0575\u0578\u0582\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x10

    .line 136
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u0582\u0569\u0575\u0578\u0582\u0576"

    const/16 v6, 0xf

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x11

    .line 137
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u056e\u0578"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x12

    .line 138
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u056b\u0579"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x13

    .line 139
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0582\u057d"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x14

    .line 140
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0582\u057d\u057f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x15

    .line 141
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0563\u0561\u0580"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x16

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u057e\u0578\u0580"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x17

    .line 143
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u057e\u0578\u0580"

    const/16 v6, 0x16

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x18

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0581"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x19

    .line 145
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0576\u0585\u0581"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x1a

    .line 146
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0582"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0584"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v11

    const/16 v0, 0x1c

    .line 148
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0579\u0565\u0584"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x1d

    .line 149
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u056b\u0584"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x1e

    .line 150
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u056c\u056b\u0584"

    const/16 v6, 0x1d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x1f

    .line 151
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0576\u056b\u0584"

    const/16 v6, 0x1d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x20

    .line 152
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0561\u056e\u0584"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x21

    .line 153
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u0582\u0575\u0584"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x22

    .line 154
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0576\u0584"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x23

    .line 155
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u0576\u0584"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x24

    .line 156
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u0582\u0576\u0584"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x25

    .line 157
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0574\u0578\u0582\u0576\u0584"

    const/16 v6, 0x24

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x26

    .line 158
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u056b\u0579\u0584"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x27

    .line 159
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0580\u0584"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 119
    sput-object v10, Lorg/tartarus/snowball/ext/ArmenianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 162
    const/16 v0, 0x39

    new-array v10, v0, [Lorg/tartarus/snowball/Among;

    const/4 v6, 0x0

    .line 163
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u057d\u0561"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 164
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u057e\u0561"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v3

    const/4 v6, 0x2

    .line 165
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0574\u0562"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x3

    .line 166
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0564"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v0, 0x4

    .line 167
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0576\u0564"

    const/4 v6, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x5

    .line 168
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u0582\u0569\u0575\u0561\u0576\u0564"

    const/4 v6, 0x4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x6

    .line 169
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0561\u0576\u0564"

    const/4 v6, 0x4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x7

    .line 170
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u057b\u0564"

    const/4 v6, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 171
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0580\u0564"

    const/4 v6, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v12

    const/16 v0, 0x9

    .line 172
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0576\u0565\u0580\u0564"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xa

    .line 173
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u0582\u0564"

    const/4 v6, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xb

    .line 174
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0568"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xc

    .line 175
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0576\u0568"

    const/16 v6, 0xb

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 176
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u0582\u0569\u0575\u0561\u0576\u0568"

    const/16 v6, 0xc

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v13

    const/16 v0, 0xe

    .line 177
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0561\u0576\u0568"

    const/16 v6, 0xc

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0xf

    .line 178
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u057b\u0568"

    const/16 v6, 0xb

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x10

    .line 179
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0580\u0568"

    const/16 v6, 0xb

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x11

    .line 180
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0576\u0565\u0580\u0568"

    const/16 v6, 0x10

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x12

    .line 181
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u056b"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x13

    .line 182
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u056b"

    const/16 v6, 0x12

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x14

    .line 183
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0580\u056b"

    const/16 v6, 0x12

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x15

    .line 184
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0576\u0565\u0580\u056b"

    const/16 v6, 0x14

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x16

    .line 185
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0561\u0576\u0578\u0582\u0574"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x17

    .line 186
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0580\u0578\u0582\u0574"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x18

    .line 187
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0576\u0565\u0580\u0578\u0582\u0574"

    const/16 v6, 0x17

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x19

    .line 188
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0576"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x1a

    .line 189
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0576"

    const/16 v6, 0x19

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 190
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u0582\u0569\u0575\u0561\u0576"

    const/16 v6, 0x1a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v11

    const/16 v0, 0x1c

    .line 191
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0561\u0576"

    const/16 v6, 0x1a

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x1d

    .line 192
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u056b\u0576"

    const/16 v6, 0x19

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x1e

    .line 193
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0580\u056b\u0576"

    const/16 v6, 0x1d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x1f

    .line 194
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0576\u0565\u0580\u056b\u0576"

    const/16 v6, 0x1e

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x20

    .line 195
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u0582\u0569\u0575\u0561\u0576\u0576"

    const/16 v6, 0x19

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x21

    .line 196
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0580\u0576"

    const/16 v6, 0x19

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x22

    .line 197
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0576\u0565\u0580\u0576"

    const/16 v6, 0x21

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x23

    .line 198
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0578\u0582\u0576"

    const/16 v6, 0x19

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x24

    .line 199
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u057b"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x25

    .line 200
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0582\u0569\u0575\u0561\u0576\u057d"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x26

    .line 201
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u057e\u0561\u0576\u057d"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x27

    .line 202
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u057b\u057d"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x28

    .line 203
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u057e"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x29

    .line 204
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0561\u0576\u0578\u057e"

    const/16 v6, 0x28

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x2a

    .line 205
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u057e\u0578\u057e"

    const/16 v6, 0x28

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x2b

    .line 206
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0565\u0580\u0578\u057e"

    const/16 v6, 0x28

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x2c

    .line 207
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0576\u0565\u0580\u0578\u057e"

    const/16 v6, 0x2b

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x2d

    .line 208
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0580"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x2e

    .line 209
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "\u0576\u0565\u0580"

    const/16 v6, 0x2d

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x2f

    .line 210
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0581"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x30

    .line 211
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u056b\u0581"

    const/16 v2, 0x2f

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x31

    .line 212
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u057e\u0561\u0576\u056b\u0581"

    const/16 v2, 0x30

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x32

    .line 213
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u057b\u056b\u0581"

    const/16 v2, 0x30

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x33

    .line 214
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u057e\u056b\u0581"

    const/16 v2, 0x30

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x34

    .line 215
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0565\u0580\u056b\u0581"

    const/16 v2, 0x30

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x35

    .line 216
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0576\u0565\u0580\u056b\u0581"

    const/16 v2, 0x34

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x36

    .line 217
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0581\u056b\u0581"

    const/16 v2, 0x30

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x37

    .line 218
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0581"

    const/16 v2, 0x2f

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x38

    .line 219
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0578\u0582\u0581"

    const/16 v2, 0x2f

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/ArmenianStemmer;->methodObject:Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 162
    sput-object v10, Lorg/tartarus/snowball/ext/ArmenianStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 222
    const/4 v0, 0x5

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0xd1

    aput-char v2, v0, v1

    const/4 v1, 0x4

    aput-char v1, v0, v3

    const/4 v1, 0x2

    const/16 v2, 0x80

    aput-char v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x12

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->g_v:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/ArmenianStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/ArmenianStemmer;

    .prologue
    .line 228
    iget v0, p1, Lorg/tartarus/snowball/ext/ArmenianStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->I_p2:I

    .line 229
    iget v0, p1, Lorg/tartarus/snowball/ext/ArmenianStemmer;->I_pV:I

    iput v0, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->I_pV:I

    .line 230
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 231
    return-void
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 316
    iget v0, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 318
    const/4 v0, 0x0

    .line 320
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_adjective()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 327
    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->ket:I

    .line 329
    sget-object v2, Lorg/tartarus/snowball/ext/ArmenianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x17

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 330
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 345
    :goto_0
    :pswitch_0
    return v1

    .line 335
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->bra:I

    .line 336
    packed-switch v0, :pswitch_data_0

    .line 345
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 342
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->slice_del()V

    goto :goto_1

    .line 336
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_ending()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 402
    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->ket:I

    .line 404
    sget-object v2, Lorg/tartarus/snowball/ext/ArmenianStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x39

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 405
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 425
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 410
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->bra:I

    .line 412
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 416
    packed-switch v0, :pswitch_data_0

    .line 425
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 422
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->slice_del()V

    goto :goto_1

    .line 416
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 5

    .prologue
    const/16 v4, 0x585

    const/16 v3, 0x561

    .line 236
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    iput v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->I_pV:I

    .line 237
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    iput v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->I_p2:I

    .line 239
    iget v0, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 246
    .local v0, "v_1":I
    :goto_0
    sget-object v1, Lorg/tartarus/snowball/ext/ArmenianStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->in_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_2

    .line 252
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    if-lt v1, v2, :cond_1

    .line 311
    :cond_0
    :goto_1
    iput v0, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 312
    const/4 v1, 0x1

    return v1

    .line 256
    :cond_1
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    goto :goto_0

    .line 259
    :cond_2
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->I_pV:I

    .line 264
    :goto_2
    sget-object v1, Lorg/tartarus/snowball/ext/ArmenianStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->out_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_4

    .line 270
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    if-ge v1, v2, :cond_0

    .line 274
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    goto :goto_2

    .line 290
    :cond_3
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 280
    :cond_4
    sget-object v1, Lorg/tartarus/snowball/ext/ArmenianStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->in_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_6

    .line 286
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    if-lt v1, v2, :cond_3

    goto :goto_1

    .line 306
    :cond_5
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 296
    :cond_6
    sget-object v1, Lorg/tartarus/snowball/ext/ArmenianStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->out_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_7

    .line 302
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    if-lt v1, v2, :cond_5

    goto :goto_1

    .line 309
    :cond_7
    iget v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->I_p2:I

    goto :goto_1
.end method

.method private r_noun()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 377
    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->ket:I

    .line 379
    sget-object v2, Lorg/tartarus/snowball/ext/ArmenianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x28

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 380
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 395
    :goto_0
    :pswitch_0
    return v1

    .line 385
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->bra:I

    .line 386
    packed-switch v0, :pswitch_data_0

    .line 395
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 392
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->slice_del()V

    goto :goto_1

    .line 386
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_verb()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 352
    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->ket:I

    .line 354
    sget-object v2, Lorg/tartarus/snowball/ext/ArmenianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x47

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 355
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 370
    :goto_0
    :pswitch_0
    return v1

    .line 360
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->bra:I

    .line 361
    packed-switch v0, :pswitch_data_0

    .line 370
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 367
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->slice_del()V

    goto :goto_1

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 508
    instance-of v0, p1, Lorg/tartarus/snowball/ext/ArmenianStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 513
    const-class v0, Lorg/tartarus/snowball/ext/ArmenianStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 9

    .prologue
    .line 439
    iget v0, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 442
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->r_mark_regions()Z

    move-result v7

    if-nez v7, :cond_0

    .line 447
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 449
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit_backward:I

    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    iput v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 451
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    sub-int v1, v7, v8

    .line 453
    .local v1, "v_2":I
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iget v8, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->I_pV:I

    if-ge v7, v8, :cond_1

    .line 455
    const/4 v7, 0x0

    .line 503
    :goto_0
    return v7

    .line 457
    :cond_1
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->I_pV:I

    iput v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 458
    iget v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit_backward:I

    .line 459
    .local v2, "v_3":I
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit_backward:I

    .line 460
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    sub-int/2addr v7, v1

    iput v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 463
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    sub-int v3, v7, v8

    .line 466
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->r_ending()Z

    move-result v7

    if-nez v7, :cond_2

    .line 471
    :cond_2
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    sub-int/2addr v7, v3

    iput v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 473
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    sub-int v4, v7, v8

    .line 476
    .local v4, "v_5":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->r_verb()Z

    move-result v7

    if-nez v7, :cond_3

    .line 481
    :cond_3
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    sub-int/2addr v7, v4

    iput v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 483
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    sub-int v5, v7, v8

    .line 486
    .local v5, "v_6":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->r_adjective()Z

    move-result v7

    if-nez v7, :cond_4

    .line 491
    :cond_4
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    sub-int/2addr v7, v5

    iput v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 493
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    sub-int v6, v7, v8

    .line 496
    .local v6, "v_7":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/ArmenianStemmer;->r_noun()Z

    move-result v7

    if-nez v7, :cond_5

    .line 501
    :cond_5
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit:I

    sub-int/2addr v7, v6

    iput v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    .line 502
    iput v2, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit_backward:I

    .line 503
    iget v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->limit_backward:I

    iput v7, p0, Lorg/tartarus/snowball/ext/ArmenianStemmer;->cursor:I

    const/4 v7, 0x1

    goto :goto_0
.end method
