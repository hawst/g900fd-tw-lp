.class public Lorg/tartarus/snowball/ext/LovinsStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "LovinsStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/16 v13, 0xe9

    const/16 v12, 0x89

    const/16 v11, 0x21

    const/4 v2, -0x1

    const/4 v10, 0x1

    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/LovinsStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    .line 19
    const/16 v0, 0x9

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "f"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ph"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "th"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "l"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "or"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "es"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/LovinsStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 31
    const/16 v0, 0x126

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    const/4 v6, 0x0

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s\'"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v10

    const/4 v0, 0x2

    .line 34
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ia"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x3

    .line 35
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ata"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v6, 0x4

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v0, 0x5

    .line 37
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aic"

    const/4 v5, 0x4

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x6

    .line 38
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "allic"

    const/4 v5, 0x4

    const-string v7, "r_BB"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x7

    .line 39
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aric"

    const/4 v5, 0x4

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8

    .line 40
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "atic"

    const/4 v5, 0x4

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9

    .line 41
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "itic"

    const/4 v5, 0x4

    const-string v7, "r_H"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa

    .line 42
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "antic"

    const/4 v5, 0x4

    const-string v7, "r_C"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb

    .line 43
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "istic"

    const/4 v5, 0x4

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc

    .line 44
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "alistic"

    const/16 v5, 0xb

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd

    .line 45
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aristic"

    const/16 v5, 0xb

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xe

    .line 46
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ivistic"

    const/16 v5, 0xb

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xf

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ed"

    const-string v4, "r_E"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x10

    .line 48
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "anced"

    const/16 v5, 0xf

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x11

    .line 49
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "enced"

    const/16 v5, 0xf

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x12

    .line 50
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ished"

    const/16 v5, 0xf

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x13

    .line 51
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ied"

    const/16 v5, 0xf

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x14

    .line 52
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ened"

    const/16 v5, 0xf

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x15

    .line 53
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ioned"

    const/16 v5, 0xf

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x16

    .line 54
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ated"

    const/16 v5, 0xf

    const-string v7, "r_I"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x17

    .line 55
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ented"

    const/16 v5, 0xf

    const-string v7, "r_C"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x18

    .line 56
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ized"

    const/16 v5, 0xf

    const-string v7, "r_F"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x19

    .line 57
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arized"

    const/16 v5, 0x18

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x1a

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oid"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x1b

    .line 59
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aroid"

    const/16 v5, 0x1a

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x1c

    .line 60
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "hood"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x1d

    .line 61
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ehood"

    const/16 v5, 0x1c

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1e

    .line 62
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ihood"

    const/16 v5, 0x1c

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1f

    .line 63
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "elihood"

    const/16 v5, 0x1e

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x20

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "ward"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v11

    const/16 v0, 0x22

    .line 66
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ae"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x23

    .line 67
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ance"

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x24

    .line 68
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icance"

    const/16 v5, 0x23

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x25

    .line 69
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ence"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x26

    .line 70
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ide"

    const-string v7, "r_L"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x27

    .line 71
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icide"

    const/16 v5, 0x26

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x28

    .line 72
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "otide"

    const/16 v5, 0x26

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x29

    .line 73
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "age"

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x2a

    .line 74
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "able"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x2b

    .line 75
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "atable"

    const/16 v5, 0x2a

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x2c

    .line 76
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "izable"

    const/16 v5, 0x2a

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x2d

    .line 77
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arizable"

    const/16 v5, 0x2c

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x2e

    .line 78
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ible"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x2f

    .line 79
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "encible"

    const/16 v5, 0x2e

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x30

    .line 80
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ene"

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x31

    .line 81
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ine"

    const-string v7, "r_M"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x32

    .line 82
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "idine"

    const/16 v5, 0x31

    const-string v7, "r_I"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x33

    .line 83
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "one"

    const-string v7, "r_R"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x34

    .line 84
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ature"

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x35

    .line 85
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eature"

    const/16 v5, 0x34

    const-string v7, "r_Z"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x36

    .line 86
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ese"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x37

    .line 87
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "wise"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x38

    .line 88
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ate"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x39

    .line 89
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entiate"

    const/16 v5, 0x38

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x3a

    .line 90
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "inate"

    const/16 v5, 0x38

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x3b

    .line 91
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ionate"

    const/16 v5, 0x38

    const-string v7, "r_D"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x3c

    .line 92
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ite"

    const-string v7, "r_AA"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x3d

    .line 93
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ive"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x3e

    .line 94
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ative"

    const/16 v5, 0x3d

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x3f

    .line 95
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ize"

    const-string v7, "r_F"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x40

    .line 96
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "alize"

    const/16 v5, 0x3f

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x41

    .line 97
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icalize"

    const/16 v5, 0x40

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x42

    .line 98
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ialize"

    const/16 v5, 0x40

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x43

    .line 99
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entialize"

    const/16 v5, 0x42

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x44

    .line 100
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ionalize"

    const/16 v5, 0x40

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x45

    .line 101
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arize"

    const/16 v5, 0x3f

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x46

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ing"

    const-string v4, "r_N"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x47

    .line 103
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ancing"

    const/16 v5, 0x46

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x48

    .line 104
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "encing"

    const/16 v5, 0x46

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x49

    .line 105
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aging"

    const/16 v5, 0x46

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x4a

    .line 106
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ening"

    const/16 v5, 0x46

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x4b

    .line 107
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ioning"

    const/16 v5, 0x46

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x4c

    .line 108
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ating"

    const/16 v5, 0x46

    const-string v7, "r_I"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x4d

    .line 109
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "enting"

    const/16 v5, 0x46

    const-string v7, "r_C"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x4e

    .line 110
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "ying"

    const/16 v5, 0x46

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x4f

    .line 111
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "izing"

    const/16 v5, 0x46

    const-string v7, "r_F"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x50

    .line 112
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arizing"

    const/16 v5, 0x4f

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x51

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ish"

    const-string v4, "r_C"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x52

    .line 114
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "yish"

    const/16 v5, 0x51

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x53

    .line 115
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x54

    .line 116
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "al"

    const-string v4, "r_BB"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x55

    .line 117
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ical"

    const/16 v5, 0x54

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x56

    .line 118
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aical"

    const/16 v5, 0x55

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x57

    .line 119
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "istical"

    const/16 v5, 0x55

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x58

    .line 120
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "oidal"

    const/16 v5, 0x54

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x59

    .line 121
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eal"

    const/16 v5, 0x54

    const-string v7, "r_Y"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5a

    .line 122
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ial"

    const/16 v5, 0x54

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5b

    .line 123
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ancial"

    const/16 v5, 0x5a

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5c

    .line 124
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arial"

    const/16 v5, 0x5a

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5d

    .line 125
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ential"

    const/16 v5, 0x5a

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5e

    .line 126
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ional"

    const/16 v5, 0x54

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5f

    .line 127
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ational"

    const/16 v5, 0x5e

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x60

    .line 128
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "izational"

    const/16 v5, 0x5f

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x61

    .line 129
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ental"

    const/16 v5, 0x54

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x62

    .line 130
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ful"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x63

    .line 131
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eful"

    const/16 v5, 0x62

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x64

    .line 132
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iful"

    const/16 v5, 0x62

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x65

    .line 133
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yl"

    const-string v4, "r_R"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x66

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ism"

    const-string v4, "r_B"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x67

    .line 135
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icism"

    const/16 v5, 0x66

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x68

    .line 136
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "oidism"

    const/16 v5, 0x66

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x69

    .line 137
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "alism"

    const/16 v5, 0x66

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x6a

    .line 138
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icalism"

    const/16 v5, 0x69

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x6b

    .line 139
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ionalism"

    const/16 v5, 0x69

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x6c

    .line 140
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "inism"

    const/16 v5, 0x66

    const-string v7, "r_J"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x6d

    .line 141
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ativism"

    const/16 v5, 0x66

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x6e

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "um"

    const-string v4, "r_U"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x6f

    .line 143
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ium"

    const/16 v5, 0x6e

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x70

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ian"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x71

    .line 145
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ician"

    const/16 v5, 0x70

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x72

    .line 146
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const-string v4, "r_F"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x73

    .line 147
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ogen"

    const/16 v5, 0x72

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x74

    .line 148
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "on"

    const-string v4, "r_S"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x75

    .line 149
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ion"

    const/16 v5, 0x74

    const-string v7, "r_Q"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x76

    .line 150
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ation"

    const/16 v5, 0x75

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x77

    .line 151
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ication"

    const/16 v5, 0x76

    const-string v7, "r_G"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x78

    .line 152
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entiation"

    const/16 v5, 0x76

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x79

    .line 153
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ination"

    const/16 v5, 0x76

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x7a

    .line 154
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "isation"

    const/16 v5, 0x76

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x7b

    .line 155
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arisation"

    const/16 v5, 0x7a

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x7c

    .line 156
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entation"

    const/16 v5, 0x76

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x7d

    .line 157
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ization"

    const/16 v5, 0x76

    const-string v7, "r_F"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x7e

    .line 158
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arization"

    const/16 v5, 0x7d

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x7f

    .line 159
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "action"

    const/16 v5, 0x75

    const-string v7, "r_G"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x80

    .line 160
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "o"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x81

    .line 161
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar"

    const-string v4, "r_X"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x82

    .line 162
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ear"

    const/16 v5, 0x81

    const-string v7, "r_Y"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x83

    .line 163
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ier"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x84

    .line 164
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ariser"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x85

    .line 165
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "izer"

    const-string v4, "r_F"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x86

    .line 166
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arizer"

    const/16 v5, 0x85

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x87

    .line 167
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "or"

    const-string v4, "r_T"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x88

    .line 168
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ator"

    const/16 v5, 0x87

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    .line 169
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s"

    const-string v4, "r_W"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v12

    const/16 v0, 0x8a

    .line 170
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "\'s"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8b

    .line 171
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "as"

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8c

    .line 172
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ics"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8d

    .line 173
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "istics"

    const/16 v5, 0x8c

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8e

    .line 174
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "es"

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8f

    .line 175
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ances"

    const/16 v5, 0x8e

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x90

    .line 176
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ences"

    const/16 v5, 0x8e

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x91

    .line 177
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ides"

    const/16 v5, 0x8e

    const-string v7, "r_L"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x92

    .line 178
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "oides"

    const/16 v5, 0x91

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x93

    .line 179
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ages"

    const/16 v5, 0x8e

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x94

    .line 180
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ies"

    const/16 v5, 0x8e

    const-string v7, "r_P"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x95

    .line 181
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "acies"

    const/16 v5, 0x94

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x96

    .line 182
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ancies"

    const/16 v5, 0x94

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x97

    .line 183
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "encies"

    const/16 v5, 0x94

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x98

    .line 184
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aries"

    const/16 v5, 0x94

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x99

    .line 185
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ities"

    const/16 v5, 0x94

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9a

    .line 186
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "alities"

    const/16 v5, 0x99

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9b

    .line 187
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ivities"

    const/16 v5, 0x99

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9c

    .line 188
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ines"

    const/16 v5, 0x8e

    const-string v7, "r_M"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9d

    .line 189
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "nesses"

    const/16 v5, 0x8e

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9e

    .line 190
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ates"

    const/16 v5, 0x8e

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9f

    .line 191
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "atives"

    const/16 v5, 0x8e

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa0

    .line 192
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ings"

    const-string v7, "r_N"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa1

    .line 193
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "is"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa2

    .line 194
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "als"

    const-string v7, "r_BB"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa3

    .line 195
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ials"

    const/16 v5, 0xa2

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa4

    .line 196
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entials"

    const/16 v5, 0xa3

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa5

    .line 197
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ionals"

    const/16 v5, 0xa2

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa6

    .line 198
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "isms"

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa7

    .line 199
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ians"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa8

    .line 200
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icians"

    const/16 v5, 0xa7

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa9

    .line 201
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ions"

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xaa

    .line 202
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ations"

    const/16 v5, 0xa9

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xab

    .line 203
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arisations"

    const/16 v5, 0xaa

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xac

    .line 204
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entations"

    const/16 v5, 0xaa

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xad

    .line 205
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "izations"

    const/16 v5, 0xaa

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xae

    .line 206
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arizations"

    const/16 v5, 0xad

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xaf

    .line 207
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ars"

    const-string v7, "r_O"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb0

    .line 208
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iers"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb1

    .line 209
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "izers"

    const-string v7, "r_F"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb2

    .line 210
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ators"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb3

    .line 211
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "less"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb4

    .line 212
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eless"

    const/16 v5, 0xb3

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb5

    .line 213
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ness"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb6

    .line 214
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eness"

    const/16 v5, 0xb5

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb7

    .line 215
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ableness"

    const/16 v5, 0xb6

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb8

    .line 216
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eableness"

    const/16 v5, 0xb7

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb9

    .line 217
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ibleness"

    const/16 v5, 0xb6

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xba

    .line 218
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ateness"

    const/16 v5, 0xb6

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xbb

    .line 219
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iteness"

    const/16 v5, 0xb6

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xbc

    .line 220
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iveness"

    const/16 v5, 0xb6

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xbd

    .line 221
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ativeness"

    const/16 v5, 0xbc

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xbe

    .line 222
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ingness"

    const/16 v5, 0xb5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xbf

    .line 223
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ishness"

    const/16 v5, 0xb5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc0

    .line 224
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iness"

    const/16 v5, 0xb5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc1

    .line 225
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ariness"

    const/16 v5, 0xc0

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc2

    .line 226
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "alness"

    const/16 v5, 0xb5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc3

    .line 227
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icalness"

    const/16 v5, 0xc2

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc4

    .line 228
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "antialness"

    const/16 v5, 0xc2

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc5

    .line 229
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entialness"

    const/16 v5, 0xc2

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc6

    .line 230
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ionalness"

    const/16 v5, 0xc2

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc7

    .line 231
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "fulness"

    const/16 v5, 0xb5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc8

    .line 232
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "lessness"

    const/16 v5, 0xb5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc9

    .line 233
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ousness"

    const/16 v5, 0xb5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xca

    .line 234
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eousness"

    const/16 v5, 0xc9

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xcb

    .line 235
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iousness"

    const/16 v5, 0xc9

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xcc

    .line 236
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "itousness"

    const/16 v5, 0xc9

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xcd

    .line 237
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entness"

    const/16 v5, 0xb5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xce

    .line 238
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ants"

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xcf

    .line 239
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ists"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd0

    .line 240
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icists"

    const/16 v5, 0xcf

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd1

    .line 241
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "us"

    const-string v7, "r_V"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd2

    .line 242
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ous"

    const/16 v5, 0xd1

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd3

    .line 243
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eous"

    const/16 v5, 0xd2

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd4

    .line 244
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aceous"

    const/16 v5, 0xd3

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd5

    .line 245
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "antaneous"

    const/16 v5, 0xd3

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd6

    .line 246
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ious"

    const/16 v5, 0xd2

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd7

    .line 247
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "acious"

    const/16 v5, 0xd6

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd8

    .line 248
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "itous"

    const/16 v5, 0xd2

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xd9

    .line 249
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ant"

    const-string v4, "r_B"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xda

    .line 250
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icant"

    const/16 v5, 0xd9

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xdb

    .line 251
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ent"

    const-string v4, "r_C"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xdc

    .line 252
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ement"

    const/16 v5, 0xdb

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xdd

    .line 253
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "izement"

    const/16 v5, 0xdc

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xde

    .line 254
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ist"

    const-string v4, "r_A"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xdf

    .line 255
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icist"

    const/16 v5, 0xde

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xe0

    .line 256
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "alist"

    const/16 v5, 0xde

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xe1

    .line 257
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icalist"

    const/16 v5, 0xe0

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xe2

    .line 258
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ialist"

    const/16 v5, 0xe0

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xe3

    .line 259
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ionist"

    const/16 v5, 0xde

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xe4

    .line 260
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entist"

    const/16 v5, 0xde

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xe5

    .line 261
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "y"

    const-string v4, "r_B"

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xe6

    .line 262
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "acy"

    const/16 v5, 0xe5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xe7

    .line 263
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ancy"

    const/16 v5, 0xe5

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xe8

    .line 264
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ency"

    const/16 v5, 0xe5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    .line 265
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ly"

    const/16 v5, 0xe5

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v13

    const/16 v0, 0xea

    .line 266
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ealy"

    const-string v7, "r_Y"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xeb

    .line 267
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ably"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xec

    .line 268
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ibly"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xed

    .line 269
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "edly"

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xee

    .line 270
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iedly"

    const/16 v5, 0xed

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xef

    .line 271
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ely"

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xf0

    .line 272
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ately"

    const/16 v5, 0xef

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xf1

    .line 273
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ively"

    const/16 v5, 0xef

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xf2

    .line 274
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "atively"

    const/16 v5, 0xf1

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xf3

    .line 275
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ingly"

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xf4

    .line 276
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "atingly"

    const/16 v5, 0xf3

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xf5

    .line 277
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ily"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xf6

    .line 278
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "lily"

    const/16 v5, 0xf5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xf7

    .line 279
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arily"

    const/16 v5, 0xf5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xf8

    .line 280
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ally"

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xf9

    .line 281
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ically"

    const/16 v5, 0xf8

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xfa

    .line 282
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aically"

    const/16 v5, 0xf9

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xfb

    .line 283
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "allically"

    const/16 v5, 0xf9

    const-string v7, "r_C"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xfc

    .line 284
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "istically"

    const/16 v5, 0xf9

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xfd

    .line 285
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "alistically"

    const/16 v5, 0xfc

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xfe

    .line 286
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "oidally"

    const/16 v5, 0xf8

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xff

    .line 287
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ially"

    const/16 v5, 0xf8

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x100

    .line 288
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entially"

    const/16 v5, 0xff

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x101

    .line 289
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ionally"

    const/16 v5, 0xf8

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x102

    .line 290
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ationally"

    const/16 v5, 0x101

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x103

    .line 291
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "izationally"

    const/16 v5, 0x102

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x104

    .line 292
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entally"

    const/16 v5, 0xf8

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x105

    .line 293
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "fully"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x106

    .line 294
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "efully"

    const/16 v5, 0x105

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x107

    .line 295
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ifully"

    const/16 v5, 0x105

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x108

    .line 296
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "enly"

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x109

    .line 297
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arly"

    const-string v7, "r_K"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x10a

    .line 298
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "early"

    const/16 v5, 0x109

    const-string v7, "r_Y"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x10b

    .line 299
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "lessly"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x10c

    .line 300
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ously"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x10d

    .line 301
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eously"

    const/16 v5, 0x10c

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x10e

    .line 302
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iously"

    const/16 v5, 0x10c

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x10f

    .line 303
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ently"

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x110

    .line 304
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ary"

    const/16 v5, 0xe5

    const-string v7, "r_F"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x111

    .line 305
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ery"

    const/16 v5, 0xe5

    const-string v7, "r_E"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x112

    .line 306
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icianry"

    const/16 v5, 0xe5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x113

    .line 307
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "atory"

    const/16 v5, 0xe5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x114

    .line 308
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ity"

    const/16 v5, 0xe5

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x115

    .line 309
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "acity"

    const/16 v5, 0x114

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x116

    .line 310
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icity"

    const/16 v5, 0x114

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x117

    .line 311
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eity"

    const/16 v5, 0x114

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x118

    .line 312
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ality"

    const/16 v5, 0x114

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x119

    .line 313
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "icality"

    const/16 v5, 0x118

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x11a

    .line 314
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iality"

    const/16 v5, 0x118

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x11b

    .line 315
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "antiality"

    const/16 v5, 0x11a

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x11c

    .line 316
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "entiality"

    const/16 v5, 0x11a

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x11d

    .line 317
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ionality"

    const/16 v5, 0x118

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x11e

    .line 318
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "elity"

    const/16 v5, 0x114

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x11f

    .line 319
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ability"

    const/16 v5, 0x114

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x120

    .line 320
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "izability"

    const/16 v5, 0x11f

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x121

    .line 321
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arizability"

    const/16 v5, 0x120

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x122

    .line 322
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ibility"

    const/16 v5, 0x114

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x123

    .line 323
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "inity"

    const/16 v5, 0x114

    const-string v7, "r_CC"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x124

    .line 324
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arity"

    const/16 v5, 0x114

    const-string v7, "r_B"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x125

    .line 325
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ivity"

    const/16 v5, 0x114

    const-string v7, "r_A"

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    .line 31
    sput-object v9, Lorg/tartarus/snowball/ext/LovinsStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 328
    const/16 v0, 0xa

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 329
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bb"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 330
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dd"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    const/4 v7, 0x2

    .line 331
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gg"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 332
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ll"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 333
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mm"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 334
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nn"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 335
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pp"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 336
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "rr"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 337
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ss"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 338
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tt"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 328
    sput-object v6, Lorg/tartarus/snowball/ext/LovinsStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 341
    const/16 v0, 0x22

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    const/4 v6, 0x0

    .line 342
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uad"

    const/16 v3, 0x12

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 343
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "vad"

    const/16 v3, 0x13

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v10

    const/4 v6, 0x2

    .line 344
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "cid"

    const/16 v3, 0x14

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x3

    .line 345
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lid"

    const/16 v3, 0x15

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x4

    .line 346
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erid"

    const/16 v3, 0x16

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x5

    .line 347
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pand"

    const/16 v3, 0x17

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x6

    .line 348
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "end"

    const/16 v3, 0x18

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x7

    .line 349
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ond"

    const/16 v3, 0x19

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x8

    .line 350
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lud"

    const/16 v3, 0x1a

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x9

    .line 351
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "rud"

    const/16 v3, 0x1b

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xa

    .line 352
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ul"

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb

    .line 353
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "her"

    const/16 v3, 0x1c

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc

    .line 354
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "metr"

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd

    .line 355
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "istr"

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe

    .line 356
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "urs"

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf

    .line 357
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uct"

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x10

    .line 358
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "et"

    const/16 v3, 0x20

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x11

    .line 359
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mit"

    const/16 v3, 0x1d

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x12

    .line 360
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ent"

    const/16 v3, 0x1e

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x13

    .line 361
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "umpt"

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x14

    .line 362
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "rpt"

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x15

    .line 363
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ert"

    const/16 v3, 0x1f

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x16

    .line 364
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yt"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v11

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x17

    .line 365
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iev"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x18

    .line 366
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "olv"

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x19

    .line 367
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ax"

    const/16 v3, 0xe

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1a

    .line 368
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ex"

    const/16 v3, 0xf

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x1b

    .line 369
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "bex"

    const/16 v5, 0x1a

    const/16 v6, 0xa

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1c

    .line 370
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "dex"

    const/16 v5, 0x1a

    const/16 v6, 0xb

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1d

    .line 371
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "pex"

    const/16 v5, 0x1a

    const/16 v6, 0xc

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1e

    .line 372
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "tex"

    const/16 v5, 0x1a

    const/16 v6, 0xd

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x1f

    .line 373
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ix"

    const/16 v3, 0x10

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x20

    .line 374
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lux"

    const/16 v3, 0x11

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 375
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "yz"

    const/16 v3, 0x22

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/LovinsStemmer;->methodObject:Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v11

    .line 341
    sput-object v9, Lorg/tartarus/snowball/ext/LovinsStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 376
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/LovinsStemmer;)V
    .locals 0
    .param p1, "other"    # Lorg/tartarus/snowball/ext/LovinsStemmer;

    .prologue
    .line 380
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 381
    return-void
.end method

.method private r_A()Z
    .locals 2

    .prologue
    .line 387
    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v1, -0x2

    .line 388
    .local v0, "c":I
    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v1, v0, :cond_0

    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v1, :cond_1

    .line 390
    :cond_0
    const/4 v1, 0x0

    .line 394
    :goto_0
    return v1

    .line 392
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 394
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private r_AA()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1432
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v3, v4

    .line 1435
    .local v1, "v_1":I
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v3, -0x2

    .line 1436
    .local v0, "c":I
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v3, v0, :cond_0

    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v3, :cond_1

    .line 1448
    :cond_0
    :goto_0
    return v2

    .line 1440
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1442
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1444
    sget-object v3, Lorg/tartarus/snowball/ext/LovinsStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v4, 0x9

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/LovinsStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v3

    if-eqz v3, :cond_0

    .line 1448
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private r_B()Z
    .locals 2

    .prologue
    .line 401
    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v1, -0x3

    .line 402
    .local v0, "c":I
    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v1, v0, :cond_0

    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v1, :cond_1

    .line 404
    :cond_0
    const/4 v1, 0x0

    .line 408
    :goto_0
    return v1

    .line 406
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 408
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private r_BB()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1457
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 1460
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v5, -0x3

    .line 1461
    .local v0, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v5, v0, :cond_0

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v5, :cond_1

    .line 1494
    :cond_0
    :goto_0
    return v4

    .line 1465
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1467
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1470
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 1473
    .local v2, "v_2":I
    const/4 v5, 0x3

    const-string v6, "met"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1479
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1483
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v3, v5, v6

    .line 1486
    .local v3, "v_3":I
    const/4 v5, 0x4

    const-string v6, "ryst"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1492
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1494
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private r_C()Z
    .locals 2

    .prologue
    .line 415
    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v1, -0x4

    .line 416
    .local v0, "c":I
    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v1, v0, :cond_0

    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v1, :cond_1

    .line 418
    :cond_0
    const/4 v1, 0x0

    .line 422
    :goto_0
    return v1

    .line 420
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 422
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private r_CC()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1501
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 1504
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v4, -0x2

    .line 1505
    .local v0, "c":I
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v4, v0, :cond_0

    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v4, :cond_1

    .line 1517
    :cond_0
    :goto_0
    return v2

    .line 1509
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1511
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1513
    const-string v4, "l"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 1517
    goto :goto_0
.end method

.method private r_D()Z
    .locals 2

    .prologue
    .line 429
    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v1, -0x5

    .line 430
    .local v0, "c":I
    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v1, v0, :cond_0

    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v1, :cond_1

    .line 432
    :cond_0
    const/4 v1, 0x0

    .line 436
    :goto_0
    return v1

    .line 434
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 436
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private r_E()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 444
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 447
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v5, -0x2

    .line 448
    .local v0, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v5, v0, :cond_0

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v5, :cond_1

    :cond_0
    move v3, v4

    .line 468
    :goto_0
    return v3

    .line 452
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 454
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 457
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 460
    .local v2, "v_2":I
    const-string v5, "e"

    invoke-virtual {p0, v3, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 466
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    goto :goto_0

    :cond_2
    move v3, v4

    .line 464
    goto :goto_0
.end method

.method private r_F()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 476
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 479
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v5, -0x3

    .line 480
    .local v0, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v5, v0, :cond_0

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v5, :cond_1

    :cond_0
    move v3, v4

    .line 500
    :goto_0
    return v3

    .line 484
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 486
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 489
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 492
    .local v2, "v_2":I
    const-string v5, "e"

    invoke-virtual {p0, v3, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 498
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    goto :goto_0

    :cond_2
    move v3, v4

    .line 496
    goto :goto_0
.end method

.method private r_G()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 507
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 510
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v4, -0x3

    .line 511
    .local v0, "c":I
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v4, v0, :cond_0

    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v4, :cond_1

    .line 523
    :cond_0
    :goto_0
    return v2

    .line 515
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 517
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 519
    const-string v4, "f"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 523
    goto :goto_0
.end method

.method private r_H()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 531
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 534
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v5, -0x2

    .line 535
    .local v0, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v5, v0, :cond_0

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v5, :cond_1

    .line 560
    :cond_0
    :goto_0
    return v3

    .line 539
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 541
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 544
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 547
    .local v2, "v_2":I
    const-string v5, "t"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 553
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 555
    const/4 v5, 0x2

    const-string v6, "ll"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    move v3, v4

    .line 560
    goto :goto_0
.end method

.method private r_I()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 569
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 572
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v6, -0x2

    .line 573
    .local v0, "c":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v6, v0, :cond_0

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v6, :cond_1

    :cond_0
    move v4, v5

    .line 606
    :goto_0
    return v4

    .line 577
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 579
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 582
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 585
    .local v2, "v_2":I
    const-string v6, "o"

    invoke-virtual {p0, v4, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 591
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 595
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 598
    .local v3, "v_3":I
    const-string v6, "e"

    invoke-virtual {p0, v4, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 604
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    goto :goto_0

    .end local v3    # "v_3":I
    :cond_2
    move v4, v5

    .line 589
    goto :goto_0

    .restart local v3    # "v_3":I
    :cond_3
    move v4, v5

    .line 602
    goto :goto_0
.end method

.method private r_J()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 615
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 618
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v6, -0x2

    .line 619
    .local v0, "c":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v6, v0, :cond_0

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v6, :cond_1

    :cond_0
    move v4, v5

    .line 652
    :goto_0
    return v4

    .line 623
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 625
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 628
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 631
    .local v2, "v_2":I
    const-string v6, "a"

    invoke-virtual {p0, v4, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 637
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 641
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 644
    .local v3, "v_3":I
    const-string v6, "e"

    invoke-virtual {p0, v4, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 650
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    goto :goto_0

    .end local v3    # "v_3":I
    :cond_2
    move v4, v5

    .line 635
    goto :goto_0

    .restart local v3    # "v_3":I
    :cond_3
    move v4, v5

    .line 648
    goto :goto_0
.end method

.method private r_K()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 660
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 663
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v5, -0x3

    .line 664
    .local v0, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v5, v0, :cond_0

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v5, :cond_1

    .line 710
    :cond_0
    :goto_0
    return v3

    .line 668
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 670
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 673
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 676
    .local v2, "v_2":I
    const-string v5, "l"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 682
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 685
    const-string v5, "i"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 691
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 694
    const-string v5, "e"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 699
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-le v5, v6, :cond_0

    .line 703
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 705
    const-string v5, "u"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    move v3, v4

    .line 710
    goto :goto_0
.end method

.method private r_L()Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 721
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v8, v9

    .line 724
    .local v1, "v_1":I
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v8, -0x2

    .line 725
    .local v0, "c":I
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v8, v0, :cond_0

    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v8, :cond_1

    .line 785
    :cond_0
    :goto_0
    return v6

    .line 729
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 731
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v8, v1

    iput v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 734
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v8, v9

    .line 737
    .local v2, "v_2":I
    const-string v8, "u"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 743
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v8, v2

    iput v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 747
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v3, v8, v9

    .line 750
    .local v3, "v_3":I
    const-string/jumbo v8, "x"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 756
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v8, v3

    iput v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 760
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v4, v8, v9

    .line 764
    .local v4, "v_4":I
    const-string v8, "s"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 783
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v4

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    move v6, v7

    .line 785
    goto :goto_0

    .line 770
    :cond_3
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v5, v8, v9

    .line 773
    .local v5, "v_5":I
    const-string v8, "o"

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 779
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v7, v5

    iput v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    goto :goto_0
.end method

.method private r_M()Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 796
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v8, v9

    .line 799
    .local v1, "v_1":I
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v8, -0x2

    .line 800
    .local v0, "c":I
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v8, v0, :cond_0

    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v8, :cond_1

    :cond_0
    move v6, v7

    .line 859
    :goto_0
    return v6

    .line 804
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 806
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v8, v1

    iput v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 809
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v8, v9

    .line 812
    .local v2, "v_2":I
    const-string v8, "a"

    invoke-virtual {p0, v6, v8}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 818
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v8, v2

    iput v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 822
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v3, v8, v9

    .line 825
    .local v3, "v_3":I
    const-string v8, "c"

    invoke-virtual {p0, v6, v8}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 831
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v8, v3

    iput v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 835
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v4, v8, v9

    .line 838
    .local v4, "v_4":I
    const-string v8, "e"

    invoke-virtual {p0, v6, v8}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 844
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v8, v4

    iput v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 848
    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v5, v8, v9

    .line 851
    .local v5, "v_5":I
    const-string v8, "m"

    invoke-virtual {p0, v6, v8}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 857
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v7, v5

    iput v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    goto :goto_0

    .end local v3    # "v_3":I
    .end local v4    # "v_4":I
    .end local v5    # "v_5":I
    :cond_2
    move v6, v7

    .line 816
    goto :goto_0

    .restart local v3    # "v_3":I
    :cond_3
    move v6, v7

    .line 829
    goto :goto_0

    .restart local v4    # "v_4":I
    :cond_4
    move v6, v7

    .line 842
    goto :goto_0

    .restart local v5    # "v_5":I
    :cond_5
    move v6, v7

    .line 855
    goto :goto_0
.end method

.method private r_N()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 868
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 871
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v6, -0x3

    .line 872
    .local v0, "c":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v6, v0, :cond_0

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v6, :cond_1

    .line 919
    :cond_0
    :goto_0
    return v4

    .line 876
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 878
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 882
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v6, -0x2

    .line 883
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v6, v0, :cond_0

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-gt v0, v6, :cond_0

    .line 887
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 891
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 895
    .local v2, "v_2":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 898
    .local v3, "v_3":I
    const-string v6, "s"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 904
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    :goto_1
    move v4, v5

    .line 919
    goto :goto_0

    .line 908
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 911
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v6, -0x2

    .line 912
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v6, v0, :cond_0

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-gt v0, v6, :cond_0

    .line 916
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    goto :goto_1
.end method

.method private r_O()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 927
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 930
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v5, -0x2

    .line 931
    .local v0, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v5, v0, :cond_0

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v5, :cond_1

    .line 956
    :cond_0
    :goto_0
    return v3

    .line 935
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 937
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 940
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 943
    .local v2, "v_2":I
    const-string v5, "l"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 949
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 951
    const-string v5, "i"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    move v3, v4

    .line 956
    goto :goto_0
.end method

.method private r_P()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 964
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 967
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v5, -0x2

    .line 968
    .local v0, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v5, v0, :cond_0

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v5, :cond_1

    :cond_0
    move v3, v4

    .line 988
    :goto_0
    return v3

    .line 972
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 974
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 977
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 980
    .local v2, "v_2":I
    const-string v5, "c"

    invoke-virtual {p0, v3, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 986
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    goto :goto_0

    :cond_2
    move v3, v4

    .line 984
    goto :goto_0
.end method

.method private r_Q()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 998
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v7, v8

    .line 1001
    .local v1, "v_1":I
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v7, -0x2

    .line 1002
    .local v0, "c":I
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v7, v0, :cond_0

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v7, :cond_1

    :cond_0
    move v5, v6

    .line 1047
    :goto_0
    return v5

    .line 1006
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1008
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v7, v1

    iput v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1010
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v7, v8

    .line 1013
    .local v2, "v_2":I
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v7, -0x3

    .line 1014
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v7, v0, :cond_2

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v7, :cond_3

    :cond_2
    move v5, v6

    .line 1016
    goto :goto_0

    .line 1018
    :cond_3
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1020
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v7, v2

    iput v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1023
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v3, v7, v8

    .line 1026
    .local v3, "v_3":I
    const-string v7, "l"

    invoke-virtual {p0, v5, v7}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 1032
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v7, v3

    iput v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1036
    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v8, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v4, v7, v8

    .line 1039
    .local v4, "v_4":I
    const-string v7, "n"

    invoke-virtual {p0, v5, v7}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 1045
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v4

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    goto :goto_0

    .end local v4    # "v_4":I
    :cond_4
    move v5, v6

    .line 1030
    goto :goto_0

    .restart local v4    # "v_4":I
    :cond_5
    move v5, v6

    .line 1043
    goto :goto_0
.end method

.method private r_R()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1055
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 1058
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v5, -0x2

    .line 1059
    .local v0, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v5, v0, :cond_0

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v5, :cond_1

    .line 1084
    :cond_0
    :goto_0
    return v3

    .line 1063
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1065
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1068
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 1071
    .local v2, "v_2":I
    const-string v5, "n"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1077
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1079
    const-string v5, "r"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    move v3, v4

    .line 1084
    goto :goto_0
.end method

.method private r_S()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1093
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 1096
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v6, -0x2

    .line 1097
    .local v0, "c":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v6, v0, :cond_0

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v6, :cond_1

    .line 1136
    :cond_0
    :goto_0
    return v4

    .line 1101
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1103
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1106
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 1109
    .local v2, "v_2":I
    const/4 v6, 0x2

    const-string v7, "dr"

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1115
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1118
    const-string v6, "t"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1124
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 1127
    .local v3, "v_3":I
    const-string v6, "t"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1133
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .end local v3    # "v_3":I
    :cond_2
    move v4, v5

    .line 1136
    goto :goto_0
.end method

.method private r_T()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 1145
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 1148
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v6, -0x2

    .line 1149
    .local v0, "c":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v6, v0, :cond_0

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v6, :cond_1

    .line 1188
    :cond_0
    :goto_0
    return v4

    .line 1153
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1155
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1158
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 1161
    .local v2, "v_2":I
    const-string v6, "s"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1167
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1170
    const-string v6, "t"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1176
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 1179
    .local v3, "v_3":I
    const-string v6, "o"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1185
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .end local v3    # "v_3":I
    :cond_2
    move v4, v5

    .line 1188
    goto :goto_0
.end method

.method private r_U()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1196
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 1199
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v5, -0x2

    .line 1200
    .local v0, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v5, v0, :cond_0

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v5, :cond_1

    .line 1243
    :cond_0
    :goto_0
    return v3

    .line 1204
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1206
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1209
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 1212
    .local v2, "v_2":I
    const-string v5, "l"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1218
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1221
    const-string v5, "m"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1227
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1230
    const-string v5, "n"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1236
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1238
    const-string v5, "r"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    move v3, v4

    .line 1243
    goto :goto_0
.end method

.method private r_V()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1250
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 1253
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v4, -0x2

    .line 1254
    .local v0, "c":I
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v4, v0, :cond_0

    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v4, :cond_1

    .line 1266
    :cond_0
    :goto_0
    return v2

    .line 1258
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1260
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1262
    const-string v4, "c"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 1266
    goto :goto_0
.end method

.method private r_W()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1275
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 1278
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v6, -0x2

    .line 1279
    .local v0, "c":I
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v6, v0, :cond_0

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v6, :cond_1

    :cond_0
    move v4, v5

    .line 1312
    :goto_0
    return v4

    .line 1283
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1285
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1288
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 1291
    .local v2, "v_2":I
    const-string v6, "s"

    invoke-virtual {p0, v4, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1297
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1301
    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 1304
    .local v3, "v_3":I
    const-string v6, "u"

    invoke-virtual {p0, v4, v6}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1310
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    goto :goto_0

    .end local v3    # "v_3":I
    :cond_2
    move v4, v5

    .line 1295
    goto :goto_0

    .restart local v3    # "v_3":I
    :cond_3
    move v4, v5

    .line 1308
    goto :goto_0
.end method

.method private r_X()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1320
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 1323
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v5, -0x2

    .line 1324
    .local v0, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v5, v0, :cond_0

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v5, :cond_1

    .line 1370
    :cond_0
    :goto_0
    return v3

    .line 1328
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1330
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1333
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 1336
    .local v2, "v_2":I
    const-string v5, "l"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1342
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1345
    const-string v5, "i"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1351
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1354
    const-string v5, "e"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1359
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-le v5, v6, :cond_0

    .line 1363
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1365
    const-string v5, "u"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    move v3, v4

    .line 1370
    goto :goto_0
.end method

.method private r_Y()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1377
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v3, v4

    .line 1380
    .local v1, "v_1":I
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v3, -0x2

    .line 1381
    .local v0, "c":I
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v3, v0, :cond_0

    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v3, :cond_1

    .line 1393
    :cond_0
    :goto_0
    return v2

    .line 1385
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1387
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1389
    const/4 v3, 0x2

    const-string v4, "in"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1393
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private r_Z()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1401
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 1404
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v0, v5, -0x2

    .line 1405
    .local v0, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-gt v5, v0, :cond_0

    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    if-le v0, v5, :cond_1

    :cond_0
    move v3, v4

    .line 1425
    :goto_0
    return v3

    .line 1409
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1411
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1414
    iget v5, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 1417
    .local v2, "v_2":I
    const-string v5, "f"

    invoke-virtual {p0, v3, v5}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1423
    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    goto :goto_0

    :cond_2
    move v3, v4

    .line 1421
    goto :goto_0
.end method

.method private r_endings()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1524
    iget v2, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->ket:I

    .line 1526
    sget-object v2, Lorg/tartarus/snowball/ext/LovinsStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x126

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/LovinsStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1527
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 1542
    :goto_0
    :pswitch_0
    return v1

    .line 1532
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->bra:I

    .line 1533
    packed-switch v0, :pswitch_data_0

    .line 1542
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 1539
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_del()V

    goto :goto_1

    .line 1533
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_respell()Z
    .locals 13

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1583
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->ket:I

    .line 1585
    sget-object v11, Lorg/tartarus/snowball/ext/LovinsStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/16 v12, 0x22

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/LovinsStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1586
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 1870
    :cond_0
    :goto_0
    :pswitch_0
    return v9

    .line 1591
    :cond_1
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->bra:I

    .line 1592
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v9, v10

    .line 1870
    goto :goto_0

    .line 1598
    :pswitch_1
    const-string v9, "ief"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1603
    :pswitch_2
    const-string v9, "uc"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1608
    :pswitch_3
    const-string v9, "um"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1613
    :pswitch_4
    const-string v9, "rb"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1618
    :pswitch_5
    const-string v9, "ur"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1623
    :pswitch_6
    const-string v9, "ister"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1628
    :pswitch_7
    const-string v9, "meter"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1633
    :pswitch_8
    const-string v9, "olut"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1639
    :pswitch_9
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v11, v12

    .line 1642
    .local v1, "v_1":I
    const-string v11, "a"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 1648
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v11, v1

    iput v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1652
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v11, v12

    .line 1655
    .local v2, "v_2":I
    const-string v11, "i"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 1661
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v11, v2

    iput v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1665
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v3, v11, v12

    .line 1668
    .local v3, "v_3":I
    const-string v11, "o"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 1674
    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v9, v3

    iput v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1677
    const-string v9, "l"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1682
    .end local v1    # "v_1":I
    .end local v2    # "v_2":I
    .end local v3    # "v_3":I
    :pswitch_a
    const-string v9, "bic"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1687
    :pswitch_b
    const-string v9, "dic"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1692
    :pswitch_c
    const-string v9, "pic"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1697
    :pswitch_d
    const-string v9, "tic"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1702
    :pswitch_e
    const-string v9, "ac"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1707
    :pswitch_f
    const-string v9, "ec"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1712
    :pswitch_10
    const-string v9, "ic"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1717
    :pswitch_11
    const-string v9, "luc"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1722
    :pswitch_12
    const-string v9, "uas"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1727
    :pswitch_13
    const-string v9, "vas"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1732
    :pswitch_14
    const-string v9, "cis"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1737
    :pswitch_15
    const-string v9, "lis"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1742
    :pswitch_16
    const-string v9, "eris"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1747
    :pswitch_17
    const-string v9, "pans"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1753
    :pswitch_18
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v4, v11, v12

    .line 1756
    .local v4, "v_4":I
    const-string v11, "s"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 1762
    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v9, v4

    iput v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1765
    const-string v9, "ens"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1770
    .end local v4    # "v_4":I
    :pswitch_19
    const-string v9, "ons"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1775
    :pswitch_1a
    const-string v9, "lus"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1780
    :pswitch_1b
    const-string v9, "rus"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1786
    :pswitch_1c
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v5, v11, v12

    .line 1789
    .local v5, "v_5":I
    const-string v11, "p"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 1795
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1799
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v6, v11, v12

    .line 1802
    .local v6, "v_6":I
    const-string v11, "t"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 1808
    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v9, v6

    iput v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1811
    const-string v9, "hes"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1816
    .end local v5    # "v_5":I
    .end local v6    # "v_6":I
    :pswitch_1d
    const-string v9, "mis"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1822
    :pswitch_1e
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v7, v11, v12

    .line 1825
    .local v7, "v_7":I
    const-string v11, "m"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 1831
    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v9, v7

    iput v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1834
    const-string v9, "ens"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1839
    .end local v7    # "v_7":I
    :pswitch_1f
    const-string v9, "ers"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1845
    :pswitch_20
    iget v11, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v8, v11, v12

    .line 1848
    .local v8, "v_8":I
    const-string v11, "n"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/LovinsStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 1854
    iget v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v9, v8

    iput v9, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1857
    const-string v9, "es"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1862
    .end local v8    # "v_8":I
    :pswitch_21
    const-string/jumbo v9, "ys"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1867
    :pswitch_22
    const-string/jumbo v9, "ys"

    invoke-virtual {p0, v9}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1592
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
    .end packed-switch
.end method

.method private r_undouble()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1549
    iget v2, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v0, v2, v3

    .line 1551
    .local v0, "v_1":I
    sget-object v2, Lorg/tartarus/snowball/ext/LovinsStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0xa

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/LovinsStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v2

    if-nez v2, :cond_1

    .line 1568
    :cond_0
    :goto_0
    return v1

    .line 1555
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1557
    iget v2, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->ket:I

    .line 1559
    iget v2, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    if-le v2, v3, :cond_0

    .line 1563
    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1565
    iget v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->bra:I

    .line 1567
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/LovinsStemmer;->slice_del()V

    .line 1568
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1917
    instance-of v0, p1, Lorg/tartarus/snowball/ext/LovinsStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1922
    const-class v0, Lorg/tartarus/snowball/ext/LovinsStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 5

    .prologue
    .line 1880
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1883
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v0, v3, v4

    .line 1886
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/LovinsStemmer;->r_endings()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1891
    :cond_0
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v3, v0

    iput v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1893
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v1, v3, v4

    .line 1896
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/LovinsStemmer;->r_undouble()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1901
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1903
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    sub-int v2, v3, v4

    .line 1906
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/LovinsStemmer;->r_respell()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1911
    :cond_2
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit:I

    sub-int/2addr v3, v2

    iput v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    .line 1912
    iget v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->limit_backward:I

    iput v3, p0, Lorg/tartarus/snowball/ext/LovinsStemmer;->cursor:I

    const/4 v3, 0x1

    return v3
.end method
