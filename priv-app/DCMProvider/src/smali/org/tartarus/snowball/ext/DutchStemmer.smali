.class public Lorg/tartarus/snowball/ext/DutchStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "DutchStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final g_v_I:[C

.field private static final g_v_j:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private B_e_found:Z

.field private I_p1:I

.field private I_p2:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/DutchStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    .line 19
    const/16 v0, 0xb

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e4"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eb"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ed"

    const/4 v2, 0x0

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ef"

    const/4 v2, 0x0

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f3"

    const/4 v2, 0x0

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f6"

    const/4 v2, 0x0

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 29
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00fa"

    const/4 v2, 0x0

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 30
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00fc"

    const/4 v2, 0x0

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/DutchStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 33
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "I"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "Y"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 33
    sput-object v6, Lorg/tartarus/snowball/ext/DutchStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 39
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dd"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kk"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tt"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 39
    sput-object v6, Lorg/tartarus/snowball/ext/DutchStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 45
    const/4 v0, 0x5

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ene"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "se"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "heden"

    const/4 v2, 0x2

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 45
    sput-object v6, Lorg/tartarus/snowball/ext/DutchStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 53
    const/4 v0, 0x6

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "end"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ig"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ing"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lijk"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "baar"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bar"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 53
    sput-object v6, Lorg/tartarus/snowball/ext/DutchStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 62
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aa"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ee"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oo"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uu"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/DutchStemmer;->methodObject:Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 62
    sput-object v6, Lorg/tartarus/snowball/ext/DutchStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 69
    const/16 v0, 0x11

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x80

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v:[C

    .line 71
    const/16 v0, 0x14

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/4 v1, 0x6

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0x80

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v_I:[C

    .line 73
    const/16 v0, 0x11

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x43

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x80

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v_j:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/DutchStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/DutchStemmer;

    .prologue
    .line 80
    iget v0, p1, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p2:I

    .line 81
    iget v0, p1, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p1:I

    .line 82
    iget-boolean v0, p1, Lorg/tartarus/snowball/ext/DutchStemmer;->B_e_found:Z

    iput-boolean v0, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->B_e_found:Z

    .line 83
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 84
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 376
    iget v0, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 378
    const/4 v0, 0x0

    .line 380
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 384
    iget v0, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 386
    const/4 v0, 0x0

    .line 388
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_e_ending()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 421
    iput-boolean v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->B_e_found:Z

    .line 423
    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 425
    const-string v3, "e"

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/DutchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 452
    :cond_0
    :goto_0
    return v1

    .line 430
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 432
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 437
    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v0, v3, v4

    .line 438
    .local v0, "v_1":I
    sget-object v3, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v:[C

    const/16 v4, 0x61

    const/16 v5, 0xe8

    invoke-virtual {p0, v3, v4, v5}, Lorg/tartarus/snowball/ext/DutchStemmer;->out_grouping_b([CII)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 442
    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v3, v0

    iput v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 444
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    .line 446
    iput-boolean v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->B_e_found:Z

    .line 448
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_undouble()Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 452
    goto :goto_0
.end method

.method private r_en_ending()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 460
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_R1()Z

    move-result v3

    if-nez v3, :cond_1

    .line 491
    :cond_0
    :goto_0
    return v2

    .line 465
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v0, v3, v4

    .line 466
    .local v0, "v_1":I
    sget-object v3, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v:[C

    const/16 v4, 0x61

    const/16 v5, 0xe8

    invoke-virtual {p0, v3, v4, v5}, Lorg/tartarus/snowball/ext/DutchStemmer;->out_grouping_b([CII)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 470
    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v3, v0

    iput v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 473
    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v1, v3, v4

    .line 476
    .local v1, "v_2":I
    const/4 v3, 0x3

    const-string v4, "gem"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/DutchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 482
    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 485
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    .line 487
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_undouble()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 491
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private r_mark_regions()Z
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/16 v4, 0xe8

    const/16 v3, 0x61

    const/4 v0, 0x0

    .line 243
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p1:I

    .line 244
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p2:I

    .line 249
    :goto_0
    sget-object v1, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/DutchStemmer;->in_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_3

    .line 255
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    if-lt v1, v2, :cond_1

    .line 322
    :cond_0
    :goto_1
    return v0

    .line 259
    :cond_1
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    goto :goto_0

    .line 275
    :cond_2
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 265
    :cond_3
    sget-object v1, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/DutchStemmer;->out_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_4

    .line 271
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    if-lt v1, v2, :cond_2

    goto :goto_1

    .line 278
    :cond_4
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p1:I

    .line 282
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p1:I

    if-lt v1, v5, :cond_5

    .line 292
    :goto_2
    sget-object v1, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/DutchStemmer;->in_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_7

    .line 298
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    if-ge v1, v2, :cond_0

    .line 302
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    goto :goto_2

    .line 286
    :cond_5
    iput v5, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p1:I

    goto :goto_2

    .line 318
    :cond_6
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 308
    :cond_7
    sget-object v1, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/DutchStemmer;->out_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_8

    .line 314
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    if-lt v1, v2, :cond_6

    goto :goto_1

    .line 321
    :cond_8
    iget v0, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v0, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->I_p2:I

    .line 322
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private r_postlude()Z
    .locals 4

    .prologue
    .line 331
    :goto_0
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 335
    .local v1, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 337
    sget-object v2, Lorg/tartarus/snowball/ext/DutchStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/DutchStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 338
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 369
    :cond_0
    :pswitch_0
    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 372
    const/4 v2, 0x1

    return v2

    .line 343
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 344
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 350
    :pswitch_1
    const-string/jumbo v2, "y"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 355
    :pswitch_2
    const-string v2, "i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 360
    :pswitch_3
    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 364
    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    goto :goto_0

    .line 344
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_prelude()Z
    .locals 12

    .prologue
    const/16 v11, 0xe8

    const/16 v10, 0x61

    const/4 v9, 0x1

    .line 96
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 100
    .local v1, "v_1":I
    :goto_0
    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 104
    .local v2, "v_2":I
    iget v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 106
    sget-object v7, Lorg/tartarus/snowball/ext/DutchStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v8, 0xb

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/DutchStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 107
    .local v0, "among_var":I
    if-nez v0, :cond_2

    .line 153
    :cond_0
    :pswitch_0
    iput v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 156
    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 158
    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 162
    .local v3, "v_3":I
    iget v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 164
    const-string/jumbo v7, "y"

    invoke-virtual {p0, v9, v7}, Lorg/tartarus/snowball/ext/DutchStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 166
    iput v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 177
    :goto_1
    iget v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 182
    .local v4, "v_4":I
    :goto_2
    iget v5, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 185
    .local v5, "v_5":I
    sget-object v7, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v:[C

    invoke-virtual {p0, v7, v10, v11}, Lorg/tartarus/snowball/ext/DutchStemmer;->in_grouping([CII)Z

    move-result v7

    if-nez v7, :cond_4

    .line 226
    :cond_1
    iput v5, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 227
    iget v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iget v8, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    if-lt v7, v8, :cond_7

    .line 235
    iput v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 238
    return v9

    .line 112
    .end local v3    # "v_3":I
    .end local v4    # "v_4":I
    .end local v5    # "v_5":I
    :cond_2
    iget v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 113
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 119
    :pswitch_1
    const-string v7, "a"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 124
    :pswitch_2
    const-string v7, "e"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 129
    :pswitch_3
    const-string v7, "i"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 134
    :pswitch_4
    const-string v7, "o"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 139
    :pswitch_5
    const-string v7, "u"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 144
    :pswitch_6
    iget v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iget v8, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    if-ge v7, v8, :cond_0

    .line 148
    iget v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    goto :goto_0

    .line 170
    .restart local v3    # "v_3":I
    :cond_3
    iget v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 172
    const-string v7, "Y"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 190
    .restart local v4    # "v_4":I
    .restart local v5    # "v_5":I
    :cond_4
    iget v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 193
    iget v6, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 197
    .local v6, "v_6":I
    const-string v7, "i"

    invoke-virtual {p0, v9, v7}, Lorg/tartarus/snowball/ext/DutchStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 211
    :cond_5
    iput v6, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 214
    const-string/jumbo v7, "y"

    invoke-virtual {p0, v9, v7}, Lorg/tartarus/snowball/ext/DutchStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 219
    iget v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 221
    const-string v7, "Y"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 223
    :goto_3
    iput v5, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    goto :goto_1

    .line 202
    :cond_6
    iget v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 203
    sget-object v7, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v:[C

    invoke-virtual {p0, v7, v10, v11}, Lorg/tartarus/snowball/ext/DutchStemmer;->in_grouping([CII)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 208
    const-string v7, "I"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 231
    .end local v6    # "v_6":I
    :cond_7
    iget v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    goto/16 :goto_2

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private r_standard_suffix()Z
    .locals 14

    .prologue
    .line 508
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v1, v11, v12

    .line 512
    .local v1, "v_1":I
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 514
    sget-object v11, Lorg/tartarus/snowball/ext/DutchStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/4 v12, 0x5

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/DutchStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 515
    .local v0, "among_var":I
    if-nez v0, :cond_5

    .line 558
    :cond_0
    :goto_0
    :pswitch_0
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v11, v1

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 560
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v3, v11, v12

    .line 563
    .local v3, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_e_ending()Z

    move-result v11

    if-nez v11, :cond_1

    .line 568
    :cond_1
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v11, v3

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 570
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v4, v11, v12

    .line 574
    .local v4, "v_3":I
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 576
    const/4 v11, 0x4

    const-string v12, "heid"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/DutchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 617
    :cond_2
    :goto_1
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 619
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v6, v11, v12

    .line 623
    .local v6, "v_5":I
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 625
    sget-object v11, Lorg/tartarus/snowball/ext/DutchStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/4 v12, 0x6

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/DutchStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 626
    if-nez v0, :cond_7

    .line 753
    :cond_3
    :goto_2
    :pswitch_1
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v11, v6

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 755
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v10, v11, v12

    .line 758
    .local v10, "v_9":I
    sget-object v11, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v_I:[C

    const/16 v12, 0x49

    const/16 v13, 0xe8

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/DutchStemmer;->out_grouping_b([CII)Z

    move-result v11

    if-nez v11, :cond_a

    .line 788
    :cond_4
    :goto_3
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 789
    const/4 v11, 0x1

    return v11

    .line 520
    .end local v3    # "v_2":I
    .end local v4    # "v_3":I
    .end local v6    # "v_5":I
    .end local v10    # "v_9":I
    :cond_5
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 521
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 527
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_R1()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 532
    const-string v11, "heid"

    invoke-virtual {p0, v11}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 537
    :pswitch_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_en_ending()Z

    move-result v11

    if-nez v11, :cond_0

    goto :goto_0

    .line 545
    :pswitch_4
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_R1()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 549
    sget-object v11, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v_j:[C

    const/16 v12, 0x61

    const/16 v13, 0xe8

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/DutchStemmer;->out_grouping_b([CII)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 554
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    goto/16 :goto_0

    .line 581
    .restart local v3    # "v_2":I
    .restart local v4    # "v_3":I
    :cond_6
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 583
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_R2()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 589
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v5, v11, v12

    .line 592
    .local v5, "v_4":I
    const/4 v11, 0x1

    const-string v12, "c"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/DutchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 598
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 601
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    .line 603
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 605
    const/4 v11, 0x2

    const-string v12, "en"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/DutchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 610
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 612
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_en_ending()Z

    move-result v11

    if-nez v11, :cond_2

    goto/16 :goto_1

    .line 631
    .end local v5    # "v_4":I
    .restart local v6    # "v_5":I
    :cond_7
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 632
    packed-switch v0, :pswitch_data_1

    goto/16 :goto_2

    .line 638
    :pswitch_5
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_R2()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 643
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    .line 646
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v7, v11, v12

    .line 650
    .local v7, "v_6":I
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 652
    const/4 v11, 0x2

    const-string v12, "ig"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/DutchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 680
    :cond_8
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v11, v7

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 682
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_undouble()Z

    move-result v11

    if-nez v11, :cond_3

    goto/16 :goto_2

    .line 657
    :cond_9
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 659
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_R2()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 665
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v8, v11, v12

    .line 668
    .local v8, "v_7":I
    const/4 v11, 0x1

    const-string v12, "e"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/DutchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_8

    .line 674
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 677
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    goto/16 :goto_2

    .line 691
    .end local v7    # "v_6":I
    .end local v8    # "v_7":I
    :pswitch_6
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_R2()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 697
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v9, v11, v12

    .line 700
    .local v9, "v_8":I
    const/4 v11, 0x1

    const-string v12, "e"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/DutchStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 706
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v11, v9

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 709
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    goto/16 :goto_2

    .line 714
    .end local v9    # "v_8":I
    :pswitch_7
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_R2()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 719
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    .line 721
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_e_ending()Z

    move-result v11

    if-nez v11, :cond_3

    goto/16 :goto_2

    .line 729
    :pswitch_8
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_R2()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 734
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    goto/16 :goto_2

    .line 739
    :pswitch_9
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_R2()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 744
    iget-boolean v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->B_e_found:Z

    if-eqz v11, :cond_3

    .line 749
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    goto/16 :goto_2

    .line 763
    .restart local v10    # "v_9":I
    :cond_a
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v2, v11, v12

    .line 766
    .local v2, "v_10":I
    sget-object v11, Lorg/tartarus/snowball/ext/DutchStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/4 v12, 0x4

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/DutchStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v11

    if-eqz v11, :cond_4

    .line 770
    sget-object v11, Lorg/tartarus/snowball/ext/DutchStemmer;->g_v:[C

    const/16 v12, 0x61

    const/16 v13, 0xe8

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/DutchStemmer;->out_grouping_b([CII)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 774
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v11, v2

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 776
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 778
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit_backward:I

    if-le v11, v12, :cond_4

    .line 782
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 784
    iget v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 786
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    goto/16 :goto_3

    .line 521
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 632
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private r_undouble()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 395
    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v0, v2, v3

    .line 397
    .local v0, "v_1":I
    sget-object v2, Lorg/tartarus/snowball/ext/DutchStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/DutchStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v2

    if-nez v2, :cond_1

    .line 414
    :cond_0
    :goto_0
    return v1

    .line 401
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 403
    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->ket:I

    .line 405
    iget v2, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit_backward:I

    if-le v2, v3, :cond_0

    .line 409
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 411
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->bra:I

    .line 413
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->slice_del()V

    .line 414
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 846
    instance-of v0, p1, Lorg/tartarus/snowball/ext/DutchStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 851
    const-class v0, Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 6

    .prologue
    .line 800
    iget v0, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 803
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_prelude()Z

    move-result v4

    if-nez v4, :cond_0

    .line 808
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 810
    iget v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 813
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_mark_regions()Z

    move-result v4

    if-nez v4, :cond_1

    .line 818
    :cond_1
    iput v1, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 820
    iget v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit_backward:I

    iget v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 822
    iget v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    sub-int v2, v4, v5

    .line 825
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_standard_suffix()Z

    move-result v4

    if-nez v4, :cond_2

    .line 830
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 831
    iget v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->limit_backward:I

    iput v4, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 832
    iget v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 835
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/DutchStemmer;->r_postlude()Z

    move-result v4

    if-nez v4, :cond_3

    .line 840
    :cond_3
    iput v3, p0, Lorg/tartarus/snowball/ext/DutchStemmer;->cursor:I

    .line 841
    const/4 v4, 0x1

    return v4
.end method
