.class public Lorg/tartarus/snowball/ext/EnglishStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "EnglishStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_10:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final a_6:[Lorg/tartarus/snowball/Among;

.field private static final a_7:[Lorg/tartarus/snowball/Among;

.field private static final a_8:[Lorg/tartarus/snowball/Among;

.field private static final a_9:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final g_v_WXY:[C

.field private static final g_valid_LI:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private B_Y_found:Z

.field private I_p1:I

.field private I_p2:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/EnglishStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    .line 19
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "arsen"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "commun"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gener"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 25
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "\'"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "\'s\'"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "\'s"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 25
    sput-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 31
    const/4 v0, 0x6

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ied"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ies"

    const/4 v2, 0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sses"

    const/4 v2, 0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ss"

    const/4 v2, 0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "us"

    const/4 v2, 0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 31
    sput-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 40
    const/16 v0, 0xd

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bb"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 43
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dd"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 44
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ff"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gg"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bl"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mm"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nn"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pp"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "rr"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "at"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tt"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iz"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 40
    sput-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 56
    const/4 v0, 0x6

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ed"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eed"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ing"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 60
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "edly"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eedly"

    const/4 v2, 0x3

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ingly"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 56
    sput-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 65
    const/16 v0, 0x18

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "anci"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "enci"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ogi"

    const/4 v2, -0x1

    const/16 v3, 0xd

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 69
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "li"

    const/4 v2, -0x1

    const/16 v3, 0x10

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 70
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bli"

    const/4 v2, 0x3

    const/16 v3, 0xc

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 71
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abli"

    const/4 v2, 0x4

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "alli"

    const/4 v2, 0x3

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "fulli"

    const/4 v2, 0x3

    const/16 v3, 0xe

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lessli"

    const/4 v2, 0x3

    const/16 v3, 0xf

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 75
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ousli"

    const/4 v2, 0x3

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 76
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "entli"

    const/4 v2, 0x3

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 77
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aliti"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "biliti"

    const/4 v2, -0x1

    const/16 v3, 0xc

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 79
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iviti"

    const/4 v2, -0x1

    const/16 v3, 0xb

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tional"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ational"

    const/16 v2, 0xe

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "alism"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ation"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ization"

    const/16 v2, 0x11

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 85
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "izer"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ator"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iveness"

    const/4 v2, -0x1

    const/16 v3, 0xb

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 88
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "fulness"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 89
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ousness"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 65
    sput-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 92
    const/16 v0, 0x9

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 93
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icate"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ative"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 95
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "alize"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iciti"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 97
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ical"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 98
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tional"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ational"

    const/4 v2, 0x5

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ful"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 101
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ness"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 92
    sput-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    .line 104
    const/16 v0, 0x12

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 105
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ance"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ence"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "able"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 109
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ible"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ate"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 111
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ive"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ize"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "al"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 115
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ism"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 116
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ion"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 118
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ous"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 119
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ant"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ent"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 121
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ment"

    const/16 v2, 0xf

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 122
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ement"

    const/16 v2, 0x10

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 104
    sput-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    .line 125
    const/4 v0, 0x2

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 126
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 127
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "l"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 125
    sput-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    .line 130
    const/16 v0, 0x8

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 131
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "succeed"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 132
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "proceed"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 133
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "exceed"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "canning"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 135
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "inning"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 136
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "earring"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 137
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "herring"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 138
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "outing"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 130
    sput-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_9:[Lorg/tartarus/snowball/Among;

    .line 141
    const/16 v0, 0x12

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "andes"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 143
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atlas"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bias"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 145
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "cosmos"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 146
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dying"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "early"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 148
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gently"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 149
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "howe"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 150
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "idly"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 151
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lying"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 152
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "news"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 153
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "only"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 154
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "singly"

    const/4 v2, -0x1

    const/16 v3, 0xb

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 155
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "skies"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 156
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "skis"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 157
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sky"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 158
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tying"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 159
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ugly"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->methodObject:Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 141
    sput-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_10:[Lorg/tartarus/snowball/Among;

    .line 162
    const/4 v0, 0x4

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    .line 164
    const/4 v0, 0x5

    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v_WXY:[C

    .line 166
    const/4 v0, 0x3

    new-array v0, v0, [C

    fill-array-data v0, :array_2

    sput-object v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_valid_LI:[C

    return-void

    .line 162
    nop

    :array_0
    .array-data 2
        0x11s
        0x41s
        0x10s
        0x1s
    .end array-data

    .line 164
    :array_1
    .array-data 2
        0x1s
        0x11s
        0x41s
        0xd0s
        0x1s
    .end array-data

    .line 166
    nop

    :array_2
    .array-data 2
        0x37s
        0x8ds
        0x2s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/EnglishStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/EnglishStemmer;

    .prologue
    .line 173
    iget-boolean v0, p1, Lorg/tartarus/snowball/ext/EnglishStemmer;->B_Y_found:Z

    iput-boolean v0, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->B_Y_found:Z

    .line 174
    iget v0, p1, Lorg/tartarus/snowball/ext/EnglishStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->I_p2:I

    .line 175
    iget v0, p1, Lorg/tartarus/snowball/ext/EnglishStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->I_p1:I

    .line 176
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 177
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 415
    iget v0, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 417
    const/4 v0, 0x0

    .line 419
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 423
    iget v0, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 425
    const/4 v0, 0x0

    .line 427
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_Step_1a()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 436
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 440
    .local v2, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 442
    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v6, 0x3

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/EnglishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 443
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 445
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 462
    :goto_0
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 464
    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/4 v6, 0x6

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/EnglishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 465
    if-nez v0, :cond_2

    .line 532
    :cond_0
    :goto_1
    :pswitch_0
    return v4

    .line 449
    :cond_1
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 450
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 452
    :pswitch_1
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    goto :goto_0

    .line 457
    :pswitch_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    goto :goto_0

    .line 470
    :cond_2
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 471
    packed-switch v0, :pswitch_data_1

    .line 532
    :goto_2
    const/4 v4, 0x1

    goto :goto_1

    .line 477
    :pswitch_3
    const-string v4, "ss"

    invoke-virtual {p0, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 483
    :pswitch_4
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    sub-int v3, v4, v5

    .line 488
    .local v3, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v1, v4, -0x2

    .line 489
    .local v1, "c":I
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit_backward:I

    if-gt v4, v1, :cond_3

    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    if-le v1, v4, :cond_4

    .line 499
    :cond_3
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 501
    const-string v4, "ie"

    invoke-virtual {p0, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 493
    :cond_4
    iput v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 496
    const-string v4, "i"

    invoke-virtual {p0, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 507
    .end local v1    # "c":I
    .end local v3    # "v_2":I
    :pswitch_5
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit_backward:I

    if-le v5, v6, :cond_0

    .line 511
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 516
    :goto_3
    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    const/16 v6, 0x61

    const/16 v7, 0x79

    invoke-virtual {p0, v5, v6, v7}, Lorg/tartarus/snowball/ext/EnglishStemmer;->in_grouping_b([CII)Z

    move-result v5

    if-nez v5, :cond_5

    .line 522
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit_backward:I

    if-le v5, v6, :cond_0

    .line 526
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    goto :goto_3

    .line 529
    :cond_5
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    goto :goto_2

    .line 450
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 471
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private r_Step_1b()Z
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 542
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 544
    sget-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/4 v7, 0x6

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/EnglishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 545
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 648
    :cond_0
    :goto_0
    :pswitch_0
    return v5

    .line 550
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 551
    packed-switch v0, :pswitch_data_0

    .line 648
    :goto_1
    const/4 v5, 0x1

    goto :goto_0

    .line 557
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_R1()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 562
    const-string v5, "ee"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 567
    :pswitch_2
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 572
    .local v2, "v_1":I
    :goto_2
    sget-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    const/16 v7, 0x61

    const/16 v8, 0x79

    invoke-virtual {p0, v6, v7, v8}, Lorg/tartarus/snowball/ext/EnglishStemmer;->in_grouping_b([CII)Z

    move-result v6

    if-nez v6, :cond_2

    .line 578
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit_backward:I

    if-le v6, v7, :cond_0

    .line 582
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    goto :goto_2

    .line 584
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 586
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    .line 588
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 590
    .local v3, "v_3":I
    sget-object v6, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/16 v7, 0xd

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/EnglishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 591
    if-eqz v0, :cond_0

    .line 595
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 596
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 603
    :pswitch_3
    iget v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 604
    .local v1, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    const-string v7, "e"

    invoke-virtual {p0, v5, v6, v7}, Lorg/tartarus/snowball/ext/EnglishStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 605
    iput v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    goto :goto_1

    .line 611
    .end local v1    # "c":I
    :pswitch_4
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 613
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit_backward:I

    if-le v6, v7, :cond_0

    .line 617
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 619
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 621
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    goto :goto_1

    .line 626
    :pswitch_5
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->I_p1:I

    if-ne v6, v7, :cond_0

    .line 631
    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    sub-int v4, v6, v7

    .line 633
    .local v4, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_shortv()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 637
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 640
    iget v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 641
    .restart local v1    # "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    const-string v7, "e"

    invoke-virtual {p0, v5, v6, v7}, Lorg/tartarus/snowball/ext/EnglishStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 642
    iput v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    goto/16 :goto_1

    .line 551
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 596
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private r_Step_1c()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 656
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 659
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    sub-int v0, v4, v5

    .line 662
    .local v0, "v_1":I
    const-string/jumbo v4, "y"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 668
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v4, v0

    iput v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 670
    const-string v4, "Y"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 696
    :cond_0
    :goto_0
    return v2

    .line 676
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 677
    sget-object v4, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    const/16 v5, 0x61

    const/16 v6, 0x79

    invoke-virtual {p0, v4, v5, v6}, Lorg/tartarus/snowball/ext/EnglishStemmer;->out_grouping_b([CII)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 683
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 686
    .local v1, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit_backward:I

    if-le v4, v5, :cond_0

    .line 692
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v2, v1

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 695
    const-string v2, "i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    move v2, v3

    .line 696
    goto :goto_0
.end method

.method private r_Step_2()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 703
    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 705
    sget-object v3, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/16 v4, 0x18

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 706
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 810
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 711
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 713
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 717
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v1, v2

    .line 810
    goto :goto_0

    .line 723
    :pswitch_1
    const-string v1, "tion"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 728
    :pswitch_2
    const-string v1, "ence"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 733
    :pswitch_3
    const-string v1, "ance"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 738
    :pswitch_4
    const-string v1, "able"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 743
    :pswitch_5
    const-string v1, "ent"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 748
    :pswitch_6
    const-string v1, "ize"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 753
    :pswitch_7
    const-string v1, "ate"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 758
    :pswitch_8
    const-string v1, "al"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 763
    :pswitch_9
    const-string v1, "ful"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 768
    :pswitch_a
    const-string v1, "ous"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 773
    :pswitch_b
    const-string v1, "ive"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 778
    :pswitch_c
    const-string v1, "ble"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 783
    :pswitch_d
    const-string v3, "l"

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/EnglishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 788
    const-string v1, "og"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 793
    :pswitch_e
    const-string v1, "ful"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 798
    :pswitch_f
    const-string v1, "less"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 802
    :pswitch_10
    sget-object v3, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_valid_LI:[C

    const/16 v4, 0x63

    const/16 v5, 0x74

    invoke-virtual {p0, v3, v4, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->in_grouping_b([CII)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 807
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    goto :goto_1

    .line 717
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method private r_Step_3()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 817
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 819
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x9

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/EnglishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 820
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 870
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 825
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 827
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 831
    packed-switch v0, :pswitch_data_0

    .line 870
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 837
    :pswitch_1
    const-string v1, "tion"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 842
    :pswitch_2
    const-string v1, "ate"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 847
    :pswitch_3
    const-string v1, "al"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 852
    :pswitch_4
    const-string v1, "ic"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 857
    :pswitch_5
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    goto :goto_1

    .line 862
    :pswitch_6
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 867
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    goto :goto_1

    .line 831
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private r_Step_4()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 878
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 880
    sget-object v4, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x12

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 881
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 924
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 886
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 888
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_R2()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 892
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v2, v3

    .line 924
    goto :goto_0

    .line 898
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    goto :goto_1

    .line 904
    :pswitch_2
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 907
    .local v1, "v_1":I
    const-string v4, "s"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 913
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 915
    const-string v4, "t"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 921
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    goto :goto_1

    .line 892
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_Step_5()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 933
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 935
    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/EnglishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 936
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 998
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 941
    :cond_1
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 942
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v3, v4

    .line 998
    goto :goto_0

    .line 949
    :pswitch_1
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    sub-int v1, v5, v6

    .line 952
    .local v1, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_2

    .line 958
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 961
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_R1()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 967
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    sub-int v2, v5, v6

    .line 970
    .local v2, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_shortv()Z

    move-result v5

    if-nez v5, :cond_0

    .line 976
    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v3, v2

    iput v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 980
    .end local v2    # "v_2":I
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    goto :goto_1

    .line 985
    .end local v1    # "v_1":I
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_R2()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 990
    const-string v5, "l"

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 995
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    goto :goto_1

    .line 942
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_exception1()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1024
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 1026
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_10:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x12

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/EnglishStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 1027
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 1097
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 1032
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 1034
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    if-lt v2, v3, :cond_0

    .line 1038
    packed-switch v0, :pswitch_data_0

    .line 1097
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 1044
    :pswitch_1
    const-string v1, "ski"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1049
    :pswitch_2
    const-string v1, "sky"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1054
    :pswitch_3
    const-string v1, "die"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1059
    :pswitch_4
    const-string v1, "lie"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1064
    :pswitch_5
    const-string v1, "tie"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1069
    :pswitch_6
    const-string v1, "idl"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1074
    :pswitch_7
    const-string v1, "gentl"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1079
    :pswitch_8
    const-string v1, "ugli"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1084
    :pswitch_9
    const-string v1, "earli"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1089
    :pswitch_a
    const-string v1, "onli"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1094
    :pswitch_b
    const-string v1, "singl"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1038
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private r_exception2()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1004
    iget v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 1006
    sget-object v1, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_9:[Lorg/tartarus/snowball/Among;

    const/16 v2, 0x8

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/EnglishStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 1017
    :cond_0
    :goto_0
    return v0

    .line 1011
    :cond_1
    iget v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 1013
    iget v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit_backward:I

    if-gt v1, v2, :cond_0

    .line 1017
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_regions()Z
    .locals 6

    .prologue
    const/16 v5, 0x79

    const/16 v4, 0x61

    .line 280
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->I_p1:I

    .line 281
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->I_p2:I

    .line 283
    iget v0, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 288
    .local v0, "v_1":I
    iget v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 291
    .local v1, "v_2":I
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/EnglishStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v2

    if-nez v2, :cond_4

    .line 297
    iput v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 303
    :goto_0
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v4, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->in_grouping([CII)Z

    move-result v2

    if-nez v2, :cond_3

    .line 309
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    if-lt v2, v3, :cond_1

    .line 369
    :cond_0
    :goto_1
    iput v0, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 370
    const/4 v2, 0x1

    return v2

    .line 313
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    goto :goto_0

    .line 329
    :cond_2
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 319
    :cond_3
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v4, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->out_grouping([CII)Z

    move-result v2

    if-nez v2, :cond_4

    .line 325
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    if-lt v2, v3, :cond_2

    goto :goto_1

    .line 333
    :cond_4
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->I_p1:I

    .line 338
    :goto_2
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v4, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->in_grouping([CII)Z

    move-result v2

    if-nez v2, :cond_6

    .line 344
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 348
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    goto :goto_2

    .line 364
    :cond_5
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 354
    :cond_6
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v4, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->out_grouping([CII)Z

    move-result v2

    if-nez v2, :cond_7

    .line 360
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    if-lt v2, v3, :cond_5

    goto :goto_1

    .line 367
    :cond_7
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->I_p2:I

    goto :goto_1
.end method

.method private r_postlude()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1105
    iget-boolean v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->B_Y_found:Z

    if-nez v3, :cond_1

    .line 1107
    const/4 v2, 0x0

    .line 1147
    :goto_0
    return v2

    .line 1129
    .local v0, "v_1":I
    .local v1, "v_2":I
    :cond_0
    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 1130
    iput v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1141
    const-string/jumbo v3, "y"

    invoke-virtual {p0, v3}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 1112
    .end local v0    # "v_1":I
    .end local v1    # "v_2":I
    :cond_1
    iget v0, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1118
    .restart local v0    # "v_1":I
    :goto_1
    iget v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1122
    .restart local v1    # "v_2":I
    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 1124
    const-string v3, "Y"

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/EnglishStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1133
    iput v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1134
    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    if-lt v3, v4, :cond_2

    .line 1144
    iput v0, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    goto :goto_0

    .line 1138
    :cond_2
    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    goto :goto_1
.end method

.method private r_prelude()Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 187
    const/4 v5, 0x0

    iput-boolean v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->B_Y_found:Z

    .line 189
    iget v0, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 193
    .local v0, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 195
    const-string v5, "\'"

    invoke-virtual {p0, v8, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 204
    :goto_0
    iput v0, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 206
    iget v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 210
    .local v1, "v_2":I
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 212
    const-string/jumbo v5, "y"

    invoke-virtual {p0, v8, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 223
    :goto_1
    iput v1, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 225
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 230
    .local v2, "v_3":I
    :goto_2
    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 236
    .local v3, "v_4":I
    :goto_3
    iget v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 239
    .local v4, "v_5":I
    sget-object v5, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    const/16 v6, 0x61

    const/16 v7, 0x79

    invoke-virtual {p0, v5, v6, v7}, Lorg/tartarus/snowball/ext/EnglishStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_3

    .line 255
    :cond_0
    iput v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 256
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    if-lt v5, v6, :cond_4

    .line 268
    iput v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 272
    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 273
    return v8

    .line 200
    .end local v1    # "v_2":I
    .end local v2    # "v_3":I
    .end local v3    # "v_4":I
    .end local v4    # "v_5":I
    :cond_1
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 202
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_del()V

    goto :goto_0

    .line 217
    .restart local v1    # "v_2":I
    :cond_2
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 219
    const-string v5, "Y"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 221
    iput-boolean v8, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->B_Y_found:Z

    goto :goto_1

    .line 244
    .restart local v2    # "v_3":I
    .restart local v3    # "v_4":I
    .restart local v4    # "v_5":I
    :cond_3
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->bra:I

    .line 246
    const-string/jumbo v5, "y"

    invoke-virtual {p0, v8, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 251
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->ket:I

    .line 252
    iput v4, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 263
    const-string v5, "Y"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/EnglishStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 265
    iput-boolean v8, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->B_Y_found:Z

    goto :goto_2

    .line 260
    :cond_4
    iget v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    goto :goto_3
.end method

.method private r_shortv()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0x61

    const/16 v4, 0x79

    .line 378
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    sub-int v0, v2, v3

    .line 381
    .local v0, "v_1":I
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v_WXY:[C

    const/16 v3, 0x59

    invoke-virtual {p0, v2, v3, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->out_grouping_b([CII)Z

    move-result v2

    if-nez v2, :cond_2

    .line 395
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 397
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v5, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->out_grouping_b([CII)Z

    move-result v2

    if-nez v2, :cond_4

    .line 411
    :cond_1
    :goto_0
    return v1

    .line 385
    :cond_2
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v5, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->in_grouping_b([CII)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 389
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v5, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->out_grouping_b([CII)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 411
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 401
    :cond_4
    sget-object v2, Lorg/tartarus/snowball/ext/EnglishStemmer;->g_v:[C

    invoke-virtual {p0, v2, v5, v4}, Lorg/tartarus/snowball/ext/EnglishStemmer;->in_grouping_b([CII)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 406
    iget v2, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit_backward:I

    if-le v2, v3, :cond_3

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1323
    instance-of v0, p1, Lorg/tartarus/snowball/ext/EnglishStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1328
    const-class v0, Lorg/tartarus/snowball/ext/EnglishStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 17

    .prologue
    .line 1168
    move-object/from16 v0, p0

    iget v2, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1171
    .local v2, "v_1":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_exception1()Z

    move-result v15

    if-nez v15, :cond_1

    .line 1177
    move-object/from16 v0, p0

    iput v2, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1181
    move-object/from16 v0, p0

    iget v7, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1185
    .local v7, "v_2":I
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    add-int/lit8 v1, v15, 0x3

    .line 1186
    .local v1, "c":I
    if-ltz v1, :cond_0

    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    if-le v1, v15, :cond_2

    .line 1194
    :cond_0
    move-object/from16 v0, p0

    iput v7, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1318
    .end local v1    # "c":I
    .end local v7    # "v_2":I
    :cond_1
    :goto_0
    const/4 v15, 0x1

    return v15

    .line 1190
    .restart local v1    # "c":I
    .restart local v7    # "v_2":I
    :cond_2
    move-object/from16 v0, p0

    iput v1, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1198
    move-object/from16 v0, p0

    iput v2, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1201
    move-object/from16 v0, p0

    iget v8, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1204
    .local v8, "v_3":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_prelude()Z

    move-result v15

    if-nez v15, :cond_3

    .line 1209
    :cond_3
    move-object/from16 v0, p0

    iput v8, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1211
    move-object/from16 v0, p0

    iget v9, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1214
    .local v9, "v_4":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_mark_regions()Z

    move-result v15

    if-nez v15, :cond_4

    .line 1219
    :cond_4
    move-object/from16 v0, p0

    iput v9, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1221
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit_backward:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1224
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    move/from16 v16, v0

    sub-int v10, v15, v16

    .line 1227
    .local v10, "v_5":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_Step_1a()Z

    move-result v15

    if-nez v15, :cond_5

    .line 1232
    :cond_5
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v15, v10

    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1235
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    move/from16 v16, v0

    sub-int v11, v15, v16

    .line 1238
    .local v11, "v_6":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_exception2()Z

    move-result v15

    if-nez v15, :cond_c

    .line 1244
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v15, v11

    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1247
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    move/from16 v16, v0

    sub-int v12, v15, v16

    .line 1250
    .local v12, "v_7":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_Step_1b()Z

    move-result v15

    if-nez v15, :cond_6

    .line 1255
    :cond_6
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v15, v12

    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1257
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    move/from16 v16, v0

    sub-int v13, v15, v16

    .line 1260
    .local v13, "v_8":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_Step_1c()Z

    move-result v15

    if-nez v15, :cond_7

    .line 1265
    :cond_7
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v15, v13

    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1267
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    move/from16 v16, v0

    sub-int v14, v15, v16

    .line 1270
    .local v14, "v_9":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_Step_2()Z

    move-result v15

    if-nez v15, :cond_8

    .line 1275
    :cond_8
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v15, v14

    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1277
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    move/from16 v16, v0

    sub-int v3, v15, v16

    .line 1280
    .local v3, "v_10":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_Step_3()Z

    move-result v15

    if-nez v15, :cond_9

    .line 1285
    :cond_9
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v15, v3

    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1287
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    move/from16 v16, v0

    sub-int v4, v15, v16

    .line 1290
    .local v4, "v_11":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_Step_4()Z

    move-result v15

    if-nez v15, :cond_a

    .line 1295
    :cond_a
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v15, v4

    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1297
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    move/from16 v16, v0

    sub-int v5, v15, v16

    .line 1300
    .local v5, "v_12":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_Step_5()Z

    move-result v15

    if-nez v15, :cond_b

    .line 1305
    :cond_b
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit:I

    sub-int/2addr v15, v5

    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1307
    .end local v3    # "v_10":I
    .end local v4    # "v_11":I
    .end local v5    # "v_12":I
    .end local v12    # "v_7":I
    .end local v13    # "v_8":I
    .end local v14    # "v_9":I
    :cond_c
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->limit_backward:I

    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1308
    move-object/from16 v0, p0

    iget v6, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    .line 1311
    .local v6, "v_13":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/EnglishStemmer;->r_postlude()Z

    move-result v15

    if-nez v15, :cond_d

    .line 1316
    :cond_d
    move-object/from16 v0, p0

    iput v6, v0, Lorg/tartarus/snowball/ext/EnglishStemmer;->cursor:I

    goto/16 :goto_0
.end method
