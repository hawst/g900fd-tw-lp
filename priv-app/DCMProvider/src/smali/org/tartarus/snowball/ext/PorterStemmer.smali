.class public Lorg/tartarus/snowball/ext/PorterStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "PorterStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final g_v_WXY:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private B_Y_found:Z

.field private I_p1:I

.field private I_p2:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/PorterStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    .line 19
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ies"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sses"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ss"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/PorterStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 26
    const/16 v0, 0xd

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bb"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 29
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dd"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 30
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ff"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 31
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gg"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bl"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mm"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nn"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pp"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "rr"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "at"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tt"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iz"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 26
    sput-object v6, Lorg/tartarus/snowball/ext/PorterStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 42
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 43
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ed"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 44
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eed"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ing"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 42
    sput-object v6, Lorg/tartarus/snowball/ext/PorterStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 48
    const/16 v0, 0x14

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "anci"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "enci"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abli"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eli"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "alli"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ousli"

    const/4 v2, -0x1

    const/16 v3, 0xc

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "entli"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aliti"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "biliti"

    const/4 v2, -0x1

    const/16 v3, 0xe

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iviti"

    const/4 v2, -0x1

    const/16 v3, 0xd

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tional"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 60
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ational"

    const/16 v2, 0xa

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "alism"

    const/4 v2, -0x1

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ation"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ization"

    const/16 v2, 0xd

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "izer"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ator"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iveness"

    const/4 v2, -0x1

    const/16 v3, 0xd

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "fulness"

    const/4 v2, -0x1

    const/16 v3, 0xb

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ousness"

    const/4 v2, -0x1

    const/16 v3, 0xc

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 48
    sput-object v6, Lorg/tartarus/snowball/ext/PorterStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 71
    const/4 v0, 0x7

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icate"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ative"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "alize"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 75
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iciti"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 76
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ical"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 77
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ful"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ness"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 71
    sput-object v6, Lorg/tartarus/snowball/ext/PorterStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 81
    const/16 v0, 0x13

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ance"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ence"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 85
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "able"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ible"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ate"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 88
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ive"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 89
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ize"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 90
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 91
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "al"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 92
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ism"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 93
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ion"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 95
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ous"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ant"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 97
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ent"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 98
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ment"

    const/16 v2, 0xf

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ement"

    const/16 v2, 0x10

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ou"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PorterStemmer;->methodObject:Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 81
    sput-object v6, Lorg/tartarus/snowball/ext/PorterStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 103
    const/4 v0, 0x4

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v:[C

    .line 105
    const/4 v0, 0x5

    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v_WXY:[C

    return-void

    .line 103
    nop

    :array_0
    .array-data 2
        0x11s
        0x41s
        0x10s
        0x1s
    .end array-data

    .line 105
    :array_1
    .array-data 2
        0x1s
        0x11s
        0x41s
        0xd0s
        0x1s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/PorterStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/PorterStemmer;

    .prologue
    .line 112
    iget-boolean v0, p1, Lorg/tartarus/snowball/ext/PorterStemmer;->B_Y_found:Z

    iput-boolean v0, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->B_Y_found:Z

    .line 113
    iget v0, p1, Lorg/tartarus/snowball/ext/PorterStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->I_p2:I

    .line 114
    iget v0, p1, Lorg/tartarus/snowball/ext/PorterStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->I_p1:I

    .line 115
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 116
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 136
    iget v0, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 138
    const/4 v0, 0x0

    .line 140
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 144
    iget v0, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 146
    const/4 v0, 0x0

    .line 148
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_Step_1a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 155
    iget v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 157
    sget-object v2, Lorg/tartarus/snowball/ext/PorterStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 158
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 183
    :goto_0
    :pswitch_0
    return v1

    .line 163
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 164
    packed-switch v0, :pswitch_data_0

    .line 183
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 170
    :pswitch_1
    const-string v1, "ss"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 175
    :pswitch_2
    const-string v1, "i"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 180
    :pswitch_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_del()V

    goto :goto_1

    .line 164
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_Step_1b()Z
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 193
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 195
    sget-object v6, Lorg/tartarus/snowball/ext/PorterStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/4 v7, 0x3

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/PorterStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 196
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 299
    :cond_0
    :goto_0
    :pswitch_0
    return v5

    .line 201
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 202
    packed-switch v0, :pswitch_data_0

    .line 299
    :goto_1
    const/4 v5, 0x1

    goto :goto_0

    .line 208
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_R1()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 213
    const-string v5, "ee"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 218
    :pswitch_2
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 223
    .local v2, "v_1":I
    :goto_2
    sget-object v6, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v:[C

    const/16 v7, 0x61

    const/16 v8, 0x79

    invoke-virtual {p0, v6, v7, v8}, Lorg/tartarus/snowball/ext/PorterStemmer;->in_grouping_b([CII)Z

    move-result v6

    if-nez v6, :cond_2

    .line 229
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit_backward:I

    if-le v6, v7, :cond_0

    .line 233
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    goto :goto_2

    .line 235
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 237
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_del()V

    .line 239
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 241
    .local v3, "v_3":I
    sget-object v6, Lorg/tartarus/snowball/ext/PorterStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/16 v7, 0xd

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/PorterStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 242
    if-eqz v0, :cond_0

    .line 246
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 247
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 254
    :pswitch_3
    iget v1, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 255
    .local v1, "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    const-string v7, "e"

    invoke-virtual {p0, v5, v6, v7}, Lorg/tartarus/snowball/ext/PorterStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 256
    iput v1, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    goto :goto_1

    .line 262
    .end local v1    # "c":I
    :pswitch_4
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 264
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit_backward:I

    if-le v6, v7, :cond_0

    .line 268
    iget v5, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 270
    iget v5, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 272
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_del()V

    goto :goto_1

    .line 277
    :pswitch_5
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->I_p1:I

    if-ne v6, v7, :cond_0

    .line 282
    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    sub-int v4, v6, v7

    .line 284
    .local v4, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_shortv()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 288
    iget v5, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 291
    iget v1, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 292
    .restart local v1    # "c":I
    iget v5, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    const-string v7, "e"

    invoke-virtual {p0, v5, v6, v7}, Lorg/tartarus/snowball/ext/PorterStemmer;->insert(IILjava/lang/CharSequence;)V

    .line 293
    iput v1, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    goto/16 :goto_1

    .line 202
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 247
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private r_Step_1c()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 306
    iget v3, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 309
    iget v3, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    sub-int v0, v3, v4

    .line 312
    .local v0, "v_1":I
    const-string/jumbo v3, "y"

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 318
    iget v3, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    sub-int/2addr v3, v0

    iput v3, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 320
    const-string v3, "Y"

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 345
    :cond_0
    :goto_0
    return v1

    .line 326
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 331
    :goto_1
    sget-object v3, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v:[C

    const/16 v4, 0x61

    const/16 v5, 0x79

    invoke-virtual {p0, v3, v4, v5}, Lorg/tartarus/snowball/ext/PorterStemmer;->in_grouping_b([CII)Z

    move-result v3

    if-nez v3, :cond_2

    .line 337
    iget v3, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit_backward:I

    if-le v3, v4, :cond_0

    .line 341
    iget v3, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    goto :goto_1

    .line 344
    :cond_2
    const-string v1, "i"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    move v1, v2

    .line 345
    goto :goto_0
.end method

.method private r_Step_2()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 352
    iget v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 354
    sget-object v2, Lorg/tartarus/snowball/ext/PorterStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x14

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 355
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 440
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 360
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 362
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 366
    packed-switch v0, :pswitch_data_0

    .line 440
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 372
    :pswitch_1
    const-string v1, "tion"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 377
    :pswitch_2
    const-string v1, "ence"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 382
    :pswitch_3
    const-string v1, "ance"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 387
    :pswitch_4
    const-string v1, "able"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 392
    :pswitch_5
    const-string v1, "ent"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 397
    :pswitch_6
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 402
    :pswitch_7
    const-string v1, "ize"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 407
    :pswitch_8
    const-string v1, "ate"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 412
    :pswitch_9
    const-string v1, "al"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 417
    :pswitch_a
    const-string v1, "al"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 422
    :pswitch_b
    const-string v1, "ful"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 427
    :pswitch_c
    const-string v1, "ous"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 432
    :pswitch_d
    const-string v1, "ive"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 437
    :pswitch_e
    const-string v1, "ble"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 366
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method private r_Step_3()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 447
    iget v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 449
    sget-object v2, Lorg/tartarus/snowball/ext/PorterStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x7

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 450
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 480
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 455
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 457
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 461
    packed-switch v0, :pswitch_data_0

    .line 480
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 467
    :pswitch_1
    const-string v1, "al"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 472
    :pswitch_2
    const-string v1, "ic"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 477
    :pswitch_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_del()V

    goto :goto_1

    .line 461
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_Step_4()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 488
    iget v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 490
    sget-object v4, Lorg/tartarus/snowball/ext/PorterStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x13

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/PorterStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 491
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 534
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 496
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 498
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_R2()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 502
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v2, v3

    .line 534
    goto :goto_0

    .line 508
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_del()V

    goto :goto_1

    .line 514
    :pswitch_2
    iget v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 517
    .local v1, "v_1":I
    const-string v4, "s"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/PorterStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 523
    iget v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 525
    const-string v4, "t"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/PorterStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 531
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_del()V

    goto :goto_1

    .line 502
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_Step_5a()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 542
    iget v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 544
    const-string v4, "e"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/PorterStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 584
    :cond_0
    :goto_0
    return v2

    .line 549
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 552
    iget v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    sub-int v0, v4, v5

    .line 555
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_R2()Z

    move-result v4

    if-nez v4, :cond_2

    .line 561
    iget v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    sub-int/2addr v4, v0

    iput v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 564
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 570
    iget v4, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 573
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_shortv()Z

    move-result v4

    if-nez v4, :cond_0

    .line 579
    iget v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    sub-int/2addr v2, v1

    iput v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 583
    .end local v1    # "v_2":I
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_del()V

    move v2, v3

    .line 584
    goto :goto_0
.end method

.method private r_Step_5b()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 590
    iget v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 592
    const-string v2, "l"

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/PorterStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 610
    :cond_0
    :goto_0
    return v0

    .line 597
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 599
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 604
    const-string v2, "l"

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/PorterStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 609
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_del()V

    move v0, v1

    .line 610
    goto :goto_0
.end method

.method private r_shortv()Z
    .locals 5

    .prologue
    const/16 v4, 0x61

    const/16 v3, 0x79

    const/4 v0, 0x0

    .line 120
    sget-object v1, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v_WXY:[C

    const/16 v2, 0x59

    invoke-virtual {p0, v1, v2, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->out_grouping_b([CII)Z

    move-result v1

    if-nez v1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v0

    .line 124
    :cond_1
    sget-object v1, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v:[C

    invoke-virtual {p0, v1, v4, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->in_grouping_b([CII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    sget-object v1, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v:[C

    invoke-virtual {p0, v1, v4, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->out_grouping_b([CII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 915
    instance-of v0, p1, Lorg/tartarus/snowball/ext/PorterStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 920
    const-class v0, Lorg/tartarus/snowball/ext/PorterStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 23

    .prologue
    .line 633
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->B_Y_found:Z

    .line 635
    move-object/from16 v0, p0

    iget v4, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 639
    .local v4, "v_1":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 641
    const/16 v20, 0x1

    const-string/jumbo v21, "y"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/PorterStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_a

    .line 652
    :goto_0
    move-object/from16 v0, p0

    iput v4, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 654
    move-object/from16 v0, p0

    iget v15, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 659
    .local v15, "v_2":I
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v17, v0

    .line 665
    .local v17, "v_3":I
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v18, v0

    .line 668
    .local v18, "v_4":I
    sget-object v20, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v:[C

    const/16 v21, 0x61

    const/16 v22, 0x79

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->in_grouping([CII)Z

    move-result v20

    if-nez v20, :cond_b

    .line 684
    :cond_0
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 685
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_c

    .line 697
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 701
    move-object/from16 v0, p0

    iput v15, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 702
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->I_p1:I

    .line 703
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->I_p2:I

    .line 705
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v19, v0

    .line 712
    .local v19, "v_5":I
    :goto_3
    sget-object v20, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v:[C

    const/16 v21, 0x61

    const/16 v22, 0x79

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->in_grouping([CII)Z

    move-result v20

    if-nez v20, :cond_f

    .line 718
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_d

    .line 777
    :cond_1
    :goto_4
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 779
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->limit_backward:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 782
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v5, v20, v21

    .line 785
    .local v5, "v_10":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_Step_1a()Z

    move-result v20

    if-nez v20, :cond_2

    .line 790
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v5

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 792
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v6, v20, v21

    .line 795
    .local v6, "v_11":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_Step_1b()Z

    move-result v20

    if-nez v20, :cond_3

    .line 800
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v6

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 802
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v7, v20, v21

    .line 805
    .local v7, "v_12":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_Step_1c()Z

    move-result v20

    if-nez v20, :cond_4

    .line 810
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 812
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v8, v20, v21

    .line 815
    .local v8, "v_13":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_Step_2()Z

    move-result v20

    if-nez v20, :cond_5

    .line 820
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 822
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v9, v20, v21

    .line 825
    .local v9, "v_14":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_Step_3()Z

    move-result v20

    if-nez v20, :cond_6

    .line 830
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v9

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 832
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v10, v20, v21

    .line 835
    .local v10, "v_15":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_Step_4()Z

    move-result v20

    if-nez v20, :cond_7

    .line 840
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v10

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 842
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v11, v20, v21

    .line 845
    .local v11, "v_16":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_Step_5a()Z

    move-result v20

    if-nez v20, :cond_8

    .line 850
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v11

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 852
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v21, v0

    sub-int v12, v20, v21

    .line 855
    .local v12, "v_17":I
    invoke-direct/range {p0 .. p0}, Lorg/tartarus/snowball/ext/PorterStemmer;->r_Step_5b()Z

    move-result v20

    if-nez v20, :cond_9

    .line 860
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v20, v0

    sub-int v20, v20, v12

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 861
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit_backward:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 862
    move-object/from16 v0, p0

    iget v13, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 866
    .local v13, "v_18":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->B_Y_found:Z

    move/from16 v20, v0

    if-nez v20, :cond_15

    .line 909
    :goto_5
    move-object/from16 v0, p0

    iput v13, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 910
    const/16 v20, 0x1

    return v20

    .line 646
    .end local v5    # "v_10":I
    .end local v6    # "v_11":I
    .end local v7    # "v_12":I
    .end local v8    # "v_13":I
    .end local v9    # "v_14":I
    .end local v10    # "v_15":I
    .end local v11    # "v_16":I
    .end local v12    # "v_17":I
    .end local v13    # "v_18":I
    .end local v15    # "v_2":I
    .end local v17    # "v_3":I
    .end local v18    # "v_4":I
    .end local v19    # "v_5":I
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 648
    const-string v20, "Y"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 650
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->B_Y_found:Z

    goto/16 :goto_0

    .line 673
    .restart local v15    # "v_2":I
    .restart local v17    # "v_3":I
    .restart local v18    # "v_4":I
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 675
    const/16 v20, 0x1

    const-string/jumbo v21, "y"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/PorterStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 680
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 681
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 692
    const-string v20, "Y"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 694
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->B_Y_found:Z

    goto/16 :goto_1

    .line 689
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    goto/16 :goto_2

    .line 722
    .restart local v19    # "v_5":I
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    goto/16 :goto_3

    .line 738
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 728
    :cond_f
    sget-object v20, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v:[C

    const/16 v21, 0x61

    const/16 v22, 0x79

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->out_grouping([CII)Z

    move-result v20

    if-nez v20, :cond_10

    .line 734
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_e

    goto/16 :goto_4

    .line 741
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->I_p1:I

    .line 746
    :goto_6
    sget-object v20, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v:[C

    const/16 v21, 0x61

    const/16 v22, 0x79

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->in_grouping([CII)Z

    move-result v20

    if-nez v20, :cond_12

    .line 752
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_1

    .line 756
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    goto :goto_6

    .line 772
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 762
    :cond_12
    sget-object v20, Lorg/tartarus/snowball/ext/PorterStemmer;->g_v:[C

    const/16 v21, 0x61

    const/16 v22, 0x79

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lorg/tartarus/snowball/ext/PorterStemmer;->out_grouping([CII)Z

    move-result v20

    if-nez v20, :cond_13

    .line 768
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_11

    goto/16 :goto_4

    .line 775
    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->I_p2:I

    goto/16 :goto_4

    .line 890
    .restart local v5    # "v_10":I
    .restart local v6    # "v_11":I
    .restart local v7    # "v_12":I
    .restart local v8    # "v_13":I
    .restart local v9    # "v_14":I
    .restart local v10    # "v_15":I
    .restart local v11    # "v_16":I
    .restart local v12    # "v_17":I
    .restart local v13    # "v_18":I
    .local v14, "v_19":I
    .local v16, "v_20":I
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->ket:I

    .line 891
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 902
    const-string/jumbo v20, "y"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/tartarus/snowball/ext/PorterStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 873
    .end local v14    # "v_19":I
    .end local v16    # "v_20":I
    :cond_15
    move-object/from16 v0, p0

    iget v14, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 879
    .restart local v14    # "v_19":I
    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v16, v0

    .line 883
    .restart local v16    # "v_20":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->bra:I

    .line 885
    const/16 v20, 0x1

    const-string v21, "Y"

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/tartarus/snowball/ext/PorterStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_14

    .line 894
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    .line 895
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->limit:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_16

    .line 905
    move-object/from16 v0, p0

    iput v14, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    goto/16 :goto_5

    .line 899
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/ext/PorterStemmer;->cursor:I

    goto :goto_7
.end method
