.class public Lorg/tartarus/snowball/ext/RussianStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "RussianStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final a_6:[Lorg/tartarus/snowball/Among;

.field private static final a_7:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p2:I

.field private I_pV:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/RussianStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    .line 19
    const/16 v0, 0x9

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0432"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0432"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u0432"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0432\u0448\u0438"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0432\u0448\u0438"

    const/4 v2, 0x3

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u0432\u0448\u0438"

    const/4 v2, 0x3

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0432\u0448\u0438\u0441\u044c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0432\u0448\u0438\u0441\u044c"

    const/4 v2, 0x6

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u0432\u0448\u0438\u0441\u044c"

    const/4 v2, 0x6

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/RussianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 31
    const/16 v0, 0x1a

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0435"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0435"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e\u0435"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u0435"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u043c\u0438"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u043c\u0438"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0439"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0439"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e\u0439"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u0439"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 43
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 44
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0433\u043e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e\u0433\u043e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u043c\u0443"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e\u043c\u0443"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0445"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u0445"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u044e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e\u044e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0443\u044e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044e\u044e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0430\u044f"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044f\u044f"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 31
    sput-object v6, Lorg/tartarus/snowball/ext/RussianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 60
    const/16 v0, 0x8

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043d\u043d"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0432\u0448"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0432\u0448"

    const/4 v2, 0x2

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u0432\u0448"

    const/4 v2, 0x2

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0449"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044e\u0449"

    const/4 v2, 0x5

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0443\u044e\u0449"

    const/4 v2, 0x6

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 60
    sput-object v6, Lorg/tartarus/snowball/ext/RussianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 71
    const/4 v0, 0x2

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0441\u044c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0441\u044f"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 71
    sput-object v6, Lorg/tartarus/snowball/ext/RussianStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 76
    const/16 v0, 0x2e

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 77
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043b\u0430"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u043b\u0430"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 79
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u043b\u0430"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043d\u0430"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u043d\u0430"

    const/4 v2, 0x3

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0442\u0435"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0442\u0435"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0439\u0442\u0435"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 85
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0439\u0442\u0435"

    const/4 v2, 0x7

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0443\u0439\u0442\u0435"

    const/4 v2, 0x7

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043b\u0438"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 88
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u043b\u0438"

    const/16 v2, 0xa

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 89
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u043b\u0438"

    const/16 v2, 0xa

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 90
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0439"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 91
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0439"

    const/16 v2, 0xd

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 92
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0443\u0439"

    const/16 v2, 0xd

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 93
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043b"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u043b"

    const/16 v2, 0x10

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 95
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u043b"

    const/16 v2, 0x10

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 97
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 98
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043d"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u043d"

    const/16 v2, 0x16

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 101
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043b\u043e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u043b\u043e"

    const/16 v2, 0x18

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 103
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u043b\u043e"

    const/16 v2, 0x18

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 104
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043d\u043e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 105
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u043d\u043e"

    const/16 v2, 0x1b

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043d\u043d\u043e"

    const/16 v2, 0x1b

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0442"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0443\u0435\u0442"

    const/16 v2, 0x1e

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 109
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0442"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u0442"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 111
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044e\u0442"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0443\u044e\u0442"

    const/16 v2, 0x22

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044f\u0442"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043d\u044b"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x26

    .line 115
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u043d\u044b"

    const/16 v2, 0x25

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x27

    .line 116
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0442\u044c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x28

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0442\u044c"

    const/16 v2, 0x27

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x29

    .line 118
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b\u0442\u044c"

    const/16 v2, 0x27

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2a

    .line 119
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0448\u044c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2b

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0448\u044c"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2c

    .line 121
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044e"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2d

    .line 122
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0443\u044e"

    const/16 v2, 0x2c

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 76
    sput-object v6, Lorg/tartarus/snowball/ext/RussianStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 125
    const/16 v0, 0x24

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 126
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0430"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 127
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0432"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 128
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e\u0432"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 129
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 130
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0435"

    const/4 v2, 0x3

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 131
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044c\u0435"

    const/4 v2, 0x3

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 132
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 133
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0438"

    const/4 v2, 0x6

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0438"

    const/4 v2, 0x6

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 135
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0430\u043c\u0438"

    const/4 v2, 0x6

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 136
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044f\u043c\u0438"

    const/4 v2, 0x6

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 137
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u044f\u043c\u0438"

    const/16 v2, 0xa

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 138
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0439"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 139
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0439"

    const/16 v2, 0xc

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 140
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0435\u0439"

    const/16 v2, 0xd

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 141
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0439"

    const/16 v2, 0xc

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e\u0439"

    const/16 v2, 0xc

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 143
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0430\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 145
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u0435\u043c"

    const/16 v2, 0x12

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 146
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044f\u043c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 148
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u044f\u043c"

    const/16 v2, 0x15

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 149
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 150
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0443"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 151
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0430\u0445"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 152
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044f\u0445"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 153
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u044f\u0445"

    const/16 v2, 0x1a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 154
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044b"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 155
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 156
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 157
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u044e"

    const/16 v2, 0x1e

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 158
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044c\u044e"

    const/16 v2, 0x1e

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 159
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044f"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 160
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0438\u044f"

    const/16 v2, 0x21

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 161
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044c\u044f"

    const/16 v2, 0x21

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 125
    sput-object v6, Lorg/tartarus/snowball/ext/RussianStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 164
    const/4 v0, 0x2

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 165
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e\u0441\u0442"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 166
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043e\u0441\u0442\u044c"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 164
    sput-object v6, Lorg/tartarus/snowball/ext/RussianStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    .line 169
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 170
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0439\u0448\u0435"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 171
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u043d"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 172
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0435\u0439\u0448"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 173
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u044c"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RussianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 169
    sput-object v6, Lorg/tartarus/snowball/ext/RussianStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    .line 176
    const/4 v0, 0x4

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/tartarus/snowball/ext/RussianStemmer;->g_v:[C

    return-void

    :array_0
    .array-data 2
        0x21s
        0x41s
        0x8s
        0xe8s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/RussianStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/RussianStemmer;

    .prologue
    .line 182
    iget v0, p1, Lorg/tartarus/snowball/ext/RussianStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->I_p2:I

    .line 183
    iget v0, p1, Lorg/tartarus/snowball/ext/RussianStemmer;->I_pV:I

    iput v0, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->I_pV:I

    .line 184
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 185
    return-void
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 270
    iget v0, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 272
    const/4 v0, 0x0

    .line 274
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_adjectival()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 357
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->r_adjective()Z

    move-result v4

    if-nez v4, :cond_0

    .line 359
    const/4 v3, 0x0

    .line 411
    :goto_0
    return v3

    .line 362
    :cond_0
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 366
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->ket:I

    .line 368
    sget-object v4, Lorg/tartarus/snowball/ext/RussianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x8

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/RussianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 369
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 371
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    goto :goto_0

    .line 375
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->bra:I

    .line 376
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 378
    :pswitch_0
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    goto :goto_0

    .line 384
    :pswitch_1
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v2, v4, v5

    .line 387
    .local v2, "v_2":I
    const-string/jumbo v4, "\u0430"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/RussianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 393
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 395
    const-string/jumbo v4, "\u044f"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/RussianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 397
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    goto :goto_0

    .line 402
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_0

    .line 407
    .end local v2    # "v_2":I
    :pswitch_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_0

    .line 376
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_adjective()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 330
    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->ket:I

    .line 332
    sget-object v2, Lorg/tartarus/snowball/ext/RussianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x1a

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/RussianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 333
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 348
    :goto_0
    :pswitch_0
    return v1

    .line 338
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->bra:I

    .line 339
    packed-switch v0, :pswitch_data_0

    .line 348
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 345
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1

    .line 339
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_derivational()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 517
    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->ket:I

    .line 519
    sget-object v2, Lorg/tartarus/snowball/ext/RussianStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/RussianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 520
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 540
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 525
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->bra:I

    .line 527
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 531
    packed-switch v0, :pswitch_data_0

    .line 540
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 537
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1

    .line 531
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 5

    .prologue
    const/16 v4, 0x44f

    const/16 v3, 0x430

    .line 190
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iput v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->I_pV:I

    .line 191
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iput v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->I_p2:I

    .line 193
    iget v0, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 200
    .local v0, "v_1":I
    :goto_0
    sget-object v1, Lorg/tartarus/snowball/ext/RussianStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/RussianStemmer;->in_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_2

    .line 206
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    if-lt v1, v2, :cond_1

    .line 265
    :cond_0
    :goto_1
    iput v0, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 266
    const/4 v1, 0x1

    return v1

    .line 210
    :cond_1
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    goto :goto_0

    .line 213
    :cond_2
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->I_pV:I

    .line 218
    :goto_2
    sget-object v1, Lorg/tartarus/snowball/ext/RussianStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/RussianStemmer;->out_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_4

    .line 224
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    if-ge v1, v2, :cond_0

    .line 228
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    goto :goto_2

    .line 244
    :cond_3
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 234
    :cond_4
    sget-object v1, Lorg/tartarus/snowball/ext/RussianStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/RussianStemmer;->in_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_6

    .line 240
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    if-lt v1, v2, :cond_3

    goto :goto_1

    .line 260
    :cond_5
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 250
    :cond_6
    sget-object v1, Lorg/tartarus/snowball/ext/RussianStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/RussianStemmer;->out_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_7

    .line 256
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    if-lt v1, v2, :cond_5

    goto :goto_1

    .line 263
    :cond_7
    iget v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->I_p2:I

    goto :goto_1
.end method

.method private r_noun()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 492
    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->ket:I

    .line 494
    sget-object v2, Lorg/tartarus/snowball/ext/RussianStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x24

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/RussianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 495
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 510
    :goto_0
    :pswitch_0
    return v1

    .line 500
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->bra:I

    .line 501
    packed-switch v0, :pswitch_data_0

    .line 510
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 507
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1

    .line 501
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_perfective_gerund()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 282
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->ket:I

    .line 284
    sget-object v4, Lorg/tartarus/snowball/ext/RussianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x9

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/RussianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 285
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 323
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 290
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->bra:I

    .line 291
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v2, v3

    .line 323
    goto :goto_0

    .line 298
    :pswitch_1
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 301
    .local v1, "v_1":I
    const-string/jumbo v4, "\u0430"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/RussianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 307
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 309
    const-string/jumbo v4, "\u044f"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/RussianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 315
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1

    .line 320
    .end local v1    # "v_1":I
    :pswitch_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1

    .line 291
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_reflexive()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 418
    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->ket:I

    .line 420
    sget-object v2, Lorg/tartarus/snowball/ext/RussianStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/RussianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 421
    .local v0, "among_var":I
    if-nez v0, :cond_0

    .line 436
    :goto_0
    :pswitch_0
    return v1

    .line 426
    :cond_0
    iget v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->bra:I

    .line 427
    packed-switch v0, :pswitch_data_0

    .line 436
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 433
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1

    .line 427
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_tidy_up()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 547
    iget v3, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->ket:I

    .line 549
    sget-object v3, Lorg/tartarus/snowball/ext/RussianStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/RussianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 550
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 596
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 555
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->bra:I

    .line 556
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v1, v2

    .line 596
    goto :goto_0

    .line 562
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    .line 564
    iget v3, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->ket:I

    .line 566
    const-string/jumbo v3, "\u043d"

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/RussianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 571
    iget v3, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->bra:I

    .line 573
    const-string/jumbo v3, "\u043d"

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/RussianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 578
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1

    .line 583
    :pswitch_2
    const-string/jumbo v3, "\u043d"

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/RussianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 588
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1

    .line 593
    :pswitch_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1

    .line 556
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_verb()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 444
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->ket:I

    .line 446
    sget-object v4, Lorg/tartarus/snowball/ext/RussianStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x2e

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/RussianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 447
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 485
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 452
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->bra:I

    .line 453
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v2, v3

    .line 485
    goto :goto_0

    .line 460
    :pswitch_1
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 463
    .local v1, "v_1":I
    const-string/jumbo v4, "\u0430"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/RussianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 469
    iget v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 471
    const-string/jumbo v4, "\u044f"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/RussianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 477
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1

    .line 482
    .end local v1    # "v_1":I
    :pswitch_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1

    .line 453
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 736
    instance-of v0, p1, Lorg/tartarus/snowball/ext/RussianStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 741
    const-class v0, Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 13

    .prologue
    const/4 v10, 0x1

    .line 613
    iget v0, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 616
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->r_mark_regions()Z

    move-result v11

    if-nez v11, :cond_0

    .line 621
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 623
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit_backward:I

    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 625
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v2, v11, v12

    .line 627
    .local v2, "v_2":I
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iget v12, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->I_pV:I

    if-ge v11, v12, :cond_1

    .line 629
    const/4 v10, 0x0

    .line 731
    :goto_0
    return v10

    .line 631
    :cond_1
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->I_pV:I

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 632
    iget v3, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit_backward:I

    .line 633
    .local v3, "v_3":I
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit_backward:I

    .line 634
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v11, v2

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 637
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v4, v11, v12

    .line 642
    .local v4, "v_4":I
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v5, v11, v12

    .line 645
    .local v5, "v_5":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->r_perfective_gerund()Z

    move-result v11

    if-nez v11, :cond_3

    .line 651
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 654
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v6, v11, v12

    .line 657
    .local v6, "v_6":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->r_reflexive()Z

    move-result v11

    if-nez v11, :cond_2

    .line 659
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v11, v6

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 665
    :cond_2
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v7, v11, v12

    .line 668
    .local v7, "v_7":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->r_adjectival()Z

    move-result v11

    if-nez v11, :cond_3

    .line 674
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v11, v7

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 677
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->r_verb()Z

    move-result v11

    if-nez v11, :cond_3

    .line 683
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v11, v7

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 685
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->r_noun()Z

    move-result v11

    if-nez v11, :cond_3

    .line 692
    .end local v6    # "v_6":I
    .end local v7    # "v_7":I
    :cond_3
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 694
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v8, v11, v12

    .line 698
    .local v8, "v_8":I
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->ket:I

    .line 700
    const-string/jumbo v11, "\u0438"

    invoke-virtual {p0, v10, v11}, Lorg/tartarus/snowball/ext/RussianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 702
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 711
    :goto_1
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v9, v11, v12

    .line 714
    .local v9, "v_9":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->r_derivational()Z

    move-result v11

    if-nez v11, :cond_4

    .line 719
    :cond_4
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v11, v9

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 721
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    sub-int v1, v11, v12

    .line 724
    .local v1, "v_10":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->r_tidy_up()Z

    move-result v11

    if-nez v11, :cond_5

    .line 729
    :cond_5
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit:I

    sub-int/2addr v11, v1

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    .line 730
    iput v3, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit_backward:I

    .line 731
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->limit_backward:I

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    goto/16 :goto_0

    .line 706
    .end local v1    # "v_10":I
    .end local v9    # "v_9":I
    :cond_6
    iget v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/RussianStemmer;->bra:I

    .line 708
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RussianStemmer;->slice_del()V

    goto :goto_1
.end method
