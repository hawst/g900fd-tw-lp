.class public Lorg/tartarus/snowball/ext/German2Stemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "German2Stemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final g_s_ending:[C

.field private static final g_st_ending:[C

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_p2:I

.field private I_x:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/German2Stemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    .line 19
    const/4 v0, 0x6

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ae"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oe"

    const/4 v2, 0x0

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "qu"

    const/4 v2, 0x0

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ue"

    const/4 v2, 0x0

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00df"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/German2Stemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 28
    const/4 v0, 0x6

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 29
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 30
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "U"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 31
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "Y"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e4"

    const/4 v2, 0x0

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f6"

    const/4 v2, 0x0

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00fc"

    const/4 v2, 0x0

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 28
    sput-object v6, Lorg/tartarus/snowball/ext/German2Stemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 37
    const/4 v0, 0x7

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "em"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ern"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 43
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 44
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "es"

    const/4 v2, 0x5

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 37
    sput-object v6, Lorg/tartarus/snowball/ext/German2Stemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 47
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "st"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "est"

    const/4 v2, 0x2

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 47
    sput-object v6, Lorg/tartarus/snowball/ext/German2Stemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 54
    const/4 v0, 0x2

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ig"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lich"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 54
    sput-object v6, Lorg/tartarus/snowball/ext/German2Stemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 59
    const/16 v0, 0x8

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 60
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "end"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ig"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ung"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lich"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isch"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ik"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "heit"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "keit"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/German2Stemmer;->methodObject:Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 59
    sput-object v6, Lorg/tartarus/snowball/ext/German2Stemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 70
    const/16 v0, 0x14

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x1

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x8

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x20

    aput-char v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0x8

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/German2Stemmer;->g_v:[C

    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/tartarus/snowball/ext/German2Stemmer;->g_s_ending:[C

    .line 74
    const/4 v0, 0x3

    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lorg/tartarus/snowball/ext/German2Stemmer;->g_st_ending:[C

    return-void

    .line 72
    :array_0
    .array-data 2
        0x75s
        0x1es
        0x5s
    .end array-data

    .line 74
    nop

    :array_1
    .array-data 2
        0x75s
        0x1es
        0x4s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/German2Stemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/German2Stemmer;

    .prologue
    .line 81
    iget v0, p1, Lorg/tartarus/snowball/ext/German2Stemmer;->I_x:I

    iput v0, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_x:I

    .line 82
    iget v0, p1, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p2:I

    .line 83
    iget v0, p1, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p1:I

    .line 84
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 85
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 401
    iget v0, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 403
    const/4 v0, 0x0

    .line 405
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 409
    iget v0, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 411
    const/4 v0, 0x0

    .line 413
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_regions()Z
    .locals 7

    .prologue
    const/16 v6, 0xfc

    const/16 v5, 0x61

    const/4 v2, 0x0

    .line 238
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p1:I

    .line 239
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iput v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p2:I

    .line 241
    iget v1, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 245
    .local v1, "v_1":I
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    add-int/lit8 v0, v3, 0x3

    .line 246
    .local v0, "c":I
    if-ltz v0, :cond_0

    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    if-le v0, v3, :cond_1

    .line 332
    :cond_0
    :goto_0
    return v2

    .line 250
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 253
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_x:I

    .line 254
    iput v1, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 259
    :goto_1
    sget-object v3, Lorg/tartarus/snowball/ext/German2Stemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/German2Stemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_3

    .line 265
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    if-ge v3, v4, :cond_0

    .line 269
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto :goto_1

    .line 285
    :cond_2
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 275
    :cond_3
    sget-object v3, Lorg/tartarus/snowball/ext/German2Stemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/German2Stemmer;->out_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_4

    .line 281
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    if-lt v3, v4, :cond_2

    goto :goto_0

    .line 288
    :cond_4
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p1:I

    .line 292
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p1:I

    iget v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_x:I

    if-lt v3, v4, :cond_5

    .line 302
    :goto_2
    sget-object v3, Lorg/tartarus/snowball/ext/German2Stemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/German2Stemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_7

    .line 308
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    if-ge v3, v4, :cond_0

    .line 312
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto :goto_2

    .line 296
    :cond_5
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_x:I

    iput v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p1:I

    goto :goto_2

    .line 328
    :cond_6
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 318
    :cond_7
    sget-object v3, Lorg/tartarus/snowball/ext/German2Stemmer;->g_v:[C

    invoke-virtual {p0, v3, v5, v6}, Lorg/tartarus/snowball/ext/German2Stemmer;->out_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_8

    .line 324
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    if-lt v3, v4, :cond_6

    goto :goto_0

    .line 331
    :cond_8
    iget v2, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->I_p2:I

    .line 332
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private r_postlude()Z
    .locals 4

    .prologue
    .line 341
    :goto_0
    iget v1, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 345
    .local v1, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->bra:I

    .line 347
    sget-object v2, Lorg/tartarus/snowball/ext/German2Stemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x6

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/German2Stemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 348
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 394
    :cond_0
    :pswitch_0
    iput v1, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 397
    const/4 v2, 0x1

    return v2

    .line 353
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->ket:I

    .line 354
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 360
    :pswitch_1
    const-string/jumbo v2, "y"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 365
    :pswitch_2
    const-string v2, "u"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 370
    :pswitch_3
    const-string v2, "a"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 375
    :pswitch_4
    const-string v2, "o"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 380
    :pswitch_5
    const-string v2, "u"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 385
    :pswitch_6
    iget v2, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 389
    iget v2, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto :goto_0

    .line 354
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private r_prelude()Z
    .locals 12

    .prologue
    const/16 v11, 0xfc

    const/16 v10, 0x61

    const/4 v9, 0x1

    .line 96
    iget v2, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 100
    .local v2, "v_1":I
    :goto_0
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 105
    .local v3, "v_2":I
    :goto_1
    iget v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 108
    .local v4, "v_3":I
    sget-object v7, Lorg/tartarus/snowball/ext/German2Stemmer;->g_v:[C

    invoke-virtual {p0, v7, v10, v11}, Lorg/tartarus/snowball/ext/German2Stemmer;->in_grouping([CII)Z

    move-result v7

    if-nez v7, :cond_2

    .line 153
    :cond_0
    iput v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 154
    iget v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iget v8, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    if-lt v7, v8, :cond_5

    .line 162
    iput v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 165
    iput v2, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 169
    :goto_2
    iget v6, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 173
    .local v6, "v_5":I
    iget v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->bra:I

    .line 175
    sget-object v7, Lorg/tartarus/snowball/ext/German2Stemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/4 v8, 0x6

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/German2Stemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 176
    .local v0, "among_var":I
    if-nez v0, :cond_6

    .line 229
    :cond_1
    :pswitch_0
    iput v6, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 232
    return v9

    .line 113
    .end local v0    # "among_var":I
    .end local v6    # "v_5":I
    :cond_2
    iget v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->bra:I

    .line 116
    iget v5, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 120
    .local v5, "v_4":I
    const-string v7, "u"

    invoke-virtual {p0, v9, v7}, Lorg/tartarus/snowball/ext/German2Stemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 134
    :cond_3
    iput v5, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 137
    const-string/jumbo v7, "y"

    invoke-virtual {p0, v9, v7}, Lorg/tartarus/snowball/ext/German2Stemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 142
    iget v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->ket:I

    .line 143
    sget-object v7, Lorg/tartarus/snowball/ext/German2Stemmer;->g_v:[C

    invoke-virtual {p0, v7, v10, v11}, Lorg/tartarus/snowball/ext/German2Stemmer;->in_grouping([CII)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 148
    const-string v7, "Y"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 150
    :goto_3
    iput v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto :goto_0

    .line 125
    :cond_4
    iget v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->ket:I

    .line 126
    sget-object v7, Lorg/tartarus/snowball/ext/German2Stemmer;->g_v:[C

    invoke-virtual {p0, v7, v10, v11}, Lorg/tartarus/snowball/ext/German2Stemmer;->in_grouping([CII)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 131
    const-string v7, "U"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 158
    .end local v5    # "v_4":I
    :cond_5
    iget v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto :goto_1

    .line 181
    .restart local v0    # "among_var":I
    .restart local v6    # "v_5":I
    :cond_6
    iget v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->ket:I

    .line 182
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 188
    :pswitch_1
    const-string v7, "ss"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 193
    :pswitch_2
    const-string/jumbo v7, "\u00e4"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 198
    :pswitch_3
    const-string/jumbo v7, "\u00f6"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 203
    :pswitch_4
    const-string/jumbo v7, "\u00fc"

    invoke-virtual {p0, v7}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 209
    :pswitch_5
    iget v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    add-int/lit8 v1, v7, 0x2

    .line 210
    .local v1, "c":I
    if-ltz v1, :cond_1

    iget v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    if-gt v1, v7, :cond_1

    .line 214
    iput v1, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto/16 :goto_2

    .line 220
    .end local v1    # "c":I
    :pswitch_6
    iget v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iget v8, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    if-ge v7, v8, :cond_1

    .line 224
    iget v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto/16 :goto_2

    .line 182
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private r_standard_suffix()Z
    .locals 14

    .prologue
    .line 429
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    sub-int v2, v11, v12

    .line 433
    .local v2, "v_1":I
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->ket:I

    .line 435
    sget-object v11, Lorg/tartarus/snowball/ext/German2Stemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/4 v12, 0x7

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/German2Stemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 436
    .local v0, "among_var":I
    if-nez v0, :cond_3

    .line 466
    :cond_0
    :goto_0
    :pswitch_0
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v2

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 468
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    sub-int v3, v11, v12

    .line 472
    .local v3, "v_2":I
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->ket:I

    .line 474
    sget-object v11, Lorg/tartarus/snowball/ext/German2Stemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/4 v12, 0x4

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/German2Stemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 475
    if-nez v0, :cond_4

    .line 514
    :cond_1
    :goto_1
    :pswitch_1
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v3

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 516
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    sub-int v4, v11, v12

    .line 520
    .local v4, "v_3":I
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->ket:I

    .line 522
    sget-object v11, Lorg/tartarus/snowball/ext/German2Stemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/16 v12, 0x8

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/German2Stemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 523
    if-nez v0, :cond_5

    .line 677
    :cond_2
    :goto_2
    :pswitch_2
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v4

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 678
    const/4 v11, 0x1

    return v11

    .line 441
    .end local v3    # "v_2":I
    .end local v4    # "v_3":I
    :cond_3
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->bra:I

    .line 443
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->r_R1()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 447
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 453
    :pswitch_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_del()V

    goto :goto_0

    .line 457
    :pswitch_4
    sget-object v11, Lorg/tartarus/snowball/ext/German2Stemmer;->g_s_ending:[C

    const/16 v12, 0x62

    const/16 v13, 0x74

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/German2Stemmer;->in_grouping_b([CII)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 462
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_del()V

    goto :goto_0

    .line 480
    .restart local v3    # "v_2":I
    :cond_4
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->bra:I

    .line 482
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->r_R1()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 486
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 492
    :pswitch_5
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_del()V

    goto :goto_1

    .line 496
    :pswitch_6
    sget-object v11, Lorg/tartarus/snowball/ext/German2Stemmer;->g_st_ending:[C

    const/16 v12, 0x62

    const/16 v13, 0x74

    invoke-virtual {p0, v11, v12, v13}, Lorg/tartarus/snowball/ext/German2Stemmer;->in_grouping_b([CII)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 502
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    add-int/lit8 v1, v11, -0x3

    .line 503
    .local v1, "c":I
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit_backward:I

    if-gt v11, v1, :cond_1

    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    if-gt v1, v11, :cond_1

    .line 507
    iput v1, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 510
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_del()V

    goto :goto_1

    .line 528
    .end local v1    # "c":I
    .restart local v4    # "v_3":I
    :cond_5
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->bra:I

    .line 530
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->r_R2()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 534
    packed-switch v0, :pswitch_data_2

    goto :goto_2

    .line 540
    :pswitch_7
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_del()V

    .line 542
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    sub-int v5, v11, v12

    .line 546
    .local v5, "v_4":I
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->ket:I

    .line 548
    const/4 v11, 0x2

    const-string v12, "ig"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/German2Stemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 550
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto/16 :goto_2

    .line 554
    :cond_6
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->bra:I

    .line 557
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    sub-int v6, v11, v12

    .line 560
    .local v6, "v_5":I
    const/4 v11, 0x1

    const-string v12, "e"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/German2Stemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_7

    .line 567
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v6

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 570
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->r_R2()Z

    move-result v11

    if-nez v11, :cond_8

    .line 572
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto/16 :goto_2

    .line 564
    :cond_7
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v5

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto/16 :goto_2

    .line 576
    :cond_8
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_del()V

    goto/16 :goto_2

    .line 583
    .end local v5    # "v_4":I
    .end local v6    # "v_5":I
    :pswitch_8
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    sub-int v7, v11, v12

    .line 586
    .local v7, "v_6":I
    const/4 v11, 0x1

    const-string v12, "e"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/German2Stemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 592
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v7

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 595
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_del()V

    goto/16 :goto_2

    .line 600
    .end local v7    # "v_6":I
    :pswitch_9
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_del()V

    .line 602
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    sub-int v8, v11, v12

    .line 606
    .local v8, "v_7":I
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->ket:I

    .line 609
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    sub-int v9, v11, v12

    .line 612
    .local v9, "v_8":I
    const/4 v11, 0x2

    const-string v12, "er"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/German2Stemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 618
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v9

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 620
    const/4 v11, 0x2

    const-string v12, "en"

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/German2Stemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 622
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto/16 :goto_2

    .line 627
    :cond_9
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->bra:I

    .line 629
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->r_R1()Z

    move-result v11

    if-nez v11, :cond_a

    .line 631
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v8

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto/16 :goto_2

    .line 635
    :cond_a
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_del()V

    goto/16 :goto_2

    .line 641
    .end local v8    # "v_7":I
    .end local v9    # "v_8":I
    :pswitch_a
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_del()V

    .line 643
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iget v12, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    sub-int v10, v11, v12

    .line 647
    .local v10, "v_9":I
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->ket:I

    .line 649
    sget-object v11, Lorg/tartarus/snowball/ext/German2Stemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/4 v12, 0x2

    invoke-virtual {p0, v11, v12}, Lorg/tartarus/snowball/ext/German2Stemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 650
    if-nez v0, :cond_b

    .line 652
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto/16 :goto_2

    .line 656
    :cond_b
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->bra:I

    .line 658
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->r_R2()Z

    move-result v11

    if-nez v11, :cond_c

    .line 660
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto/16 :goto_2

    .line 663
    :cond_c
    packed-switch v0, :pswitch_data_3

    goto/16 :goto_2

    .line 665
    :pswitch_b
    iget v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v11, v10

    iput v11, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    goto/16 :goto_2

    .line 670
    :pswitch_c
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->slice_del()V

    goto/16 :goto_2

    .line 447
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 486
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 534
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 663
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 735
    instance-of v0, p1, Lorg/tartarus/snowball/ext/German2Stemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 740
    const-class v0, Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 6

    .prologue
    .line 689
    iget v0, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 692
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->r_prelude()Z

    move-result v4

    if-nez v4, :cond_0

    .line 697
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 699
    iget v1, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 702
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->r_mark_regions()Z

    move-result v4

    if-nez v4, :cond_1

    .line 707
    :cond_1
    iput v1, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 709
    iget v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit_backward:I

    iget v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iput v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 711
    iget v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    sub-int v2, v4, v5

    .line 714
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->r_standard_suffix()Z

    move-result v4

    if-nez v4, :cond_2

    .line 719
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 720
    iget v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->limit_backward:I

    iput v4, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 721
    iget v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 724
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/German2Stemmer;->r_postlude()Z

    move-result v4

    if-nez v4, :cond_3

    .line 729
    :cond_3
    iput v3, p0, Lorg/tartarus/snowball/ext/German2Stemmer;->cursor:I

    .line 730
    const/4 v4, 0x1

    return v4
.end method
