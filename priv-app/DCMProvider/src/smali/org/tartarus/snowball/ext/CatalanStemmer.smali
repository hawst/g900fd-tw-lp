.class public Lorg/tartarus/snowball/ext/CatalanStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "CatalanStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_p2:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/16 v12, 0x4c

    const/16 v11, 0x8e

    const/4 v2, -0x1

    const/4 v10, 0x1

    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/CatalanStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    .line 19
    const/16 v0, 0xd

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/16 v3, 0xd

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v13

    .line 21
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00b7"

    const/16 v6, 0xc

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v10

    const/4 v0, 0x2

    .line 22
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e0"

    const/4 v6, 0x2

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x3

    .line 23
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x4

    .line 24
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e8"

    const/4 v6, 0x4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x5

    .line 25
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9"

    const/4 v6, 0x3

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x6

    .line 26
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00ec"

    const/4 v6, 0x6

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x7

    .line 27
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00ed"

    const/4 v6, 0x5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8

    .line 28
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00ef"

    const/16 v6, 0xb

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9

    .line 29
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00f2"

    const/16 v6, 0x8

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa

    .line 30
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00f3"

    const/4 v6, 0x7

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb

    .line 31
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00fa"

    const/16 v6, 0x9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc

    .line 32
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00fc"

    const/16 v6, 0xa

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    .line 19
    sput-object v9, Lorg/tartarus/snowball/ext/CatalanStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 35
    const/16 v0, 0x27

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "la"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v13

    .line 37
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "-la"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v10

    const/4 v0, 0x2

    .line 38
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "sela"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v6, 0x3

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "le"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x4

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "me"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v0, 0x5

    .line 41
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "-me"

    const/4 v5, 0x4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v6, 0x6

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "se"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x7

    .line 43
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "-te"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x8

    .line 44
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "hi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x9

    .line 45
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "\'hi"

    const/16 v5, 0x8

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xa

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "li"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xb

    .line 47
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "-li"

    const/16 v5, 0xa

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xc

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "\'l"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "\'m"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "-m"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "\'n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x10

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "-n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x11

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ho"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x12

    .line 54
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "\'ho"

    const/16 v5, 0x11

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x13

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lo"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x14

    .line 56
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "selo"

    const/16 v5, 0x13

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x15

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "\'s"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x16

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "las"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x17

    .line 59
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "selas"

    const/16 v5, 0x16

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x18

    .line 60
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "les"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x19

    .line 61
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "-les"

    const/16 v5, 0x18

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x1a

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "\'ls"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1b

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "-ls"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1c

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "\'ns"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1d

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "-ns"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1e

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ens"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1f

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "los"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x20

    .line 68
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "selos"

    const/16 v5, 0x1f

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x21

    .line 69
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x22

    .line 70
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "-nos"

    const/16 v5, 0x21

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x23

    .line 71
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "vos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x24

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "us"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x25

    .line 73
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "-us"

    const/16 v5, 0x24

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x26

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "\'t"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 35
    sput-object v9, Lorg/tartarus/snowball/ext/CatalanStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 77
    const/16 v0, 0xc8

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ica"

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v13

    .line 79
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "l\u00f3gica"

    const/4 v6, 0x3

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v10

    const/4 v6, 0x2

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "enca"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x3

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ada"

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x4

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ancia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x5

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "encia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x6

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e8ncia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x7

    .line 85
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edcia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x8

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "logia"

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x9

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "inia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xa

    .line 88
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00edinia"

    const/16 v5, 0x9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xb

    .line 89
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eria"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc

    .line 90
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e0ria"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd

    .line 91
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "at\u00f2ria"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe

    .line 92
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "alla"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf

    .line 93
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ella"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x10

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edvola"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x11

    .line 95
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ima"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x12

    .line 96
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00edssima"

    const/16 v5, 0x11

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x13

    .line 97
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "qu\u00edssima"

    const/16 v5, 0x12

    const/4 v6, 0x5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x14

    .line 98
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ana"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x15

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ina"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x16

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "era"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x17

    .line 101
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "sfera"

    const/16 v5, 0x16

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x18

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ora"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x19

    .line 103
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "dora"

    const/16 v5, 0x18

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1a

    .line 104
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "adora"

    const/16 v5, 0x19

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x1b

    .line 105
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "adura"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1c

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "esa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1d

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1e

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "assa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1f

    .line 109
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "essa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x20

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x21

    .line 111
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eta"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x22

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ita"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x23

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ota"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x24

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ista"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x25

    .line 115
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ialista"

    const/16 v5, 0x24

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x26

    .line 116
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ionista"

    const/16 v5, 0x24

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x27

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iva"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x28

    .line 118
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ativa"

    const/16 v5, 0x27

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x29

    .line 119
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n\u00e7a"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x2a

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "log\u00eda"

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x2b

    .line 121
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x2c

    .line 122
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00edstic"

    const/16 v5, 0x2b

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x2d

    .line 123
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "enc"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x2e

    .line 124
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "esc"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x2f

    .line 125
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ud"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x30

    .line 126
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atge"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x31

    .line 127
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ble"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x32

    .line 128
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "able"

    const/16 v5, 0x31

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x33

    .line 129
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ible"

    const/16 v5, 0x31

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x34

    .line 130
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isme"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x35

    .line 131
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ialisme"

    const/16 v5, 0x34

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x36

    .line 132
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ionisme"

    const/16 v5, 0x34

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x37

    .line 133
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ivisme"

    const/16 v5, 0x34

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x38

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aire"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x39

    .line 135
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icte"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x3a

    .line 136
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iste"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x3b

    .line 137
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ici"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x3c

    .line 138
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edci"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x3d

    .line 139
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "logi"

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x3e

    .line 140
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ari"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x3f

    .line 141
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tori"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x40

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "al"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x41

    .line 143
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "il"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x42

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "all"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x43

    .line 145
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ell"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x44

    .line 146
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edvol"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x45

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isam"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x46

    .line 148
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issem"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x47

    .line 149
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ecssem"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x48

    .line 150
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edssem"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x49

    .line 151
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edssim"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x4a

    .line 152
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "qu\u00edssim"

    const/16 v5, 0x49

    const/4 v6, 0x5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x4b

    .line 153
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amen"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 154
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ecssin"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v12

    const/16 v6, 0x4d

    .line 155
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x4e

    .line 156
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ificar"

    const/16 v5, 0x4d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x4f

    .line 157
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "egar"

    const/16 v5, 0x4d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x50

    .line 158
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ejar"

    const/16 v5, 0x4d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x51

    .line 159
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "itar"

    const/16 v5, 0x4d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x52

    .line 160
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "itzar"

    const/16 v5, 0x4d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x53

    .line 161
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "fer"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x54

    .line 162
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "or"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x55

    .line 163
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "dor"

    const/16 v5, 0x54

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x56

    .line 164
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dur"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x57

    .line 165
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "doras"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x58

    .line 166
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ics"

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x59

    .line 167
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "l\u00f3gics"

    const/16 v5, 0x58

    const/4 v6, 0x3

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x5a

    .line 168
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uds"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x5b

    .line 169
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nces"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x5c

    .line 170
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ades"

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x5d

    .line 171
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ancies"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x5e

    .line 172
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "encies"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x5f

    .line 173
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e8ncies"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x60

    .line 174
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edcies"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x61

    .line 175
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "logies"

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x62

    .line 176
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "inies"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x63

    .line 177
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ednies"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x64

    .line 178
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eries"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x65

    .line 179
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e0ries"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x66

    .line 180
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "at\u00f2ries"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x67

    .line 181
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bles"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x68

    .line 182
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ables"

    const/16 v5, 0x67

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x69

    .line 183
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ibles"

    const/16 v5, 0x67

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x6a

    .line 184
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imes"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x6b

    .line 185
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00edssimes"

    const/16 v5, 0x6a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x6c

    .line 186
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "qu\u00edssimes"

    const/16 v5, 0x6b

    const/4 v6, 0x5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x6d

    .line 187
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "formes"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x6e

    .line 188
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ismes"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x6f

    .line 189
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ialismes"

    const/16 v5, 0x6e

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x70

    .line 190
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ines"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x71

    .line 191
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eres"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x72

    .line 192
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ores"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x73

    .line 193
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "dores"

    const/16 v5, 0x72

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x74

    .line 194
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "idores"

    const/16 v5, 0x73

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x75

    .line 195
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dures"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x76

    .line 196
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eses"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x77

    .line 197
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oses"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x78

    .line 198
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asses"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x79

    .line 199
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ictes"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x7a

    .line 200
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ites"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x7b

    .line 201
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "otes"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x7c

    .line 202
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "istes"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x7d

    .line 203
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ialistes"

    const/16 v5, 0x7c

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x7e

    .line 204
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ionistes"

    const/16 v5, 0x7c

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x7f

    .line 205
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iques"

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x80

    .line 206
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "l\u00f3giques"

    const/16 v5, 0x7f

    const/4 v6, 0x3

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x81

    .line 207
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ives"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x82

    .line 208
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "atives"

    const/16 v5, 0x81

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x83

    .line 209
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "log\u00edes"

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x84

    .line 210
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "alleng\u00fces"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x85

    .line 211
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icis"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x86

    .line 212
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edcis"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x87

    .line 213
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "logis"

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x88

    .line 214
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aris"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x89

    .line 215
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "toris"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x8a

    .line 216
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ls"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x8b

    .line 217
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "als"

    const/16 v5, 0x8a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8c

    .line 218
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ells"

    const/16 v5, 0x8a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x8d

    .line 219
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ims"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 220
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00edssims"

    const/16 v5, 0x8d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v11

    const/16 v0, 0x8f

    .line 221
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "qu\u00edssims"

    const/4 v6, 0x5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x90

    .line 222
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ions"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x91

    .line 223
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "cions"

    const/16 v5, 0x90

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x92

    .line 224
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "acions"

    const/16 v5, 0x91

    const/4 v6, 0x2

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x93

    .line 225
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "esos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x94

    .line 226
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x95

    .line 227
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "assos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x96

    .line 228
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x97

    .line 229
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ers"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x98

    .line 230
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ors"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x99

    .line 231
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "dors"

    const/16 v5, 0x98

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9a

    .line 232
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "adors"

    const/16 v5, 0x99

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9b

    .line 233
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "idors"

    const/16 v5, 0x99

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x9c

    .line 234
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ats"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x9d

    .line 235
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "itats"

    const/16 v5, 0x9c

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9e

    .line 236
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "bilitats"

    const/16 v5, 0x9d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9f

    .line 237
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ivitats"

    const/16 v5, 0x9d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa0

    .line 238
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ativitats"

    const/16 v5, 0x9f

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa1

    .line 239
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00eftats"

    const/16 v5, 0x9c

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xa2

    .line 240
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ets"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xa3

    .line 241
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ants"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xa4

    .line 242
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ents"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xa5

    .line 243
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ments"

    const/16 v5, 0xa4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa6

    .line 244
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aments"

    const/16 v5, 0xa5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xa7

    .line 245
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ots"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xa8

    .line 246
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uts"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xa9

    .line 247
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ius"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xaa

    .line 248
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "trius"

    const/16 v5, 0xa9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xab

    .line 249
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "atius"

    const/16 v5, 0xa9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xac

    .line 250
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e8s"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xad

    .line 251
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9s"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xae

    .line 252
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eds"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xaf

    .line 253
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "d\u00eds"

    const/16 v5, 0xae

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xb0

    .line 254
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f3s"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb1

    .line 255
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itat"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xb2

    .line 256
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "bilitat"

    const/16 v5, 0xb1

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb3

    .line 257
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ivitat"

    const/16 v5, 0xb1

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb4

    .line 258
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ativitat"

    const/16 v5, 0xb3

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xb5

    .line 259
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eftat"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb6

    .line 260
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "et"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb7

    .line 261
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ant"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb8

    .line 262
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ent"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xb9

    .line 263
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ient"

    const/16 v5, 0xb8

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xba

    .line 264
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ment"

    const/16 v5, 0xb8

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xbb

    .line 265
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ament"

    const/16 v5, 0xba

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xbc

    .line 266
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "isament"

    const/16 v5, 0xbb

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xbd

    .line 267
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ot"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xbe

    .line 268
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isseu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xbf

    .line 269
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ecsseu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc0

    .line 270
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edsseu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc1

    .line 271
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "triu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc2

    .line 272
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edssiu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc3

    .line 273
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atiu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc4

    .line 274
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f3"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xc5

    .line 275
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "i\u00f3"

    const/16 v5, 0xc4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc6

    .line 276
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ci\u00f3"

    const/16 v5, 0xc5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc7

    .line 277
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aci\u00f3"

    const/16 v5, 0xc6

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    .line 77
    sput-object v9, Lorg/tartarus/snowball/ext/CatalanStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 280
    const/16 v0, 0x11b

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    .line 281
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aba"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v13

    .line 282
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "esca"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v10

    const/4 v6, 0x2

    .line 283
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isca"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x3

    .line 284
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00efsca"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x4

    .line 285
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ada"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x5

    .line 286
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ida"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x6

    .line 287
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uda"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x7

    .line 288
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00efda"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x8

    .line 289
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ia"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x9

    .line 290
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aria"

    const/16 v5, 0x8

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa

    .line 291
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iria"

    const/16 v5, 0x8

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xb

    .line 292
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ara"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc

    .line 293
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iera"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd

    .line 294
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ira"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe

    .line 295
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "adora"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf

    .line 296
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00efra"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x10

    .line 297
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ava"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x11

    .line 298
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ixa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x12

    .line 299
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itza"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x13

    .line 300
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eda"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x14

    .line 301
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ar\u00eda"

    const/16 v5, 0x13

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x15

    .line 302
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "er\u00eda"

    const/16 v5, 0x13

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x16

    .line 303
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ir\u00eda"

    const/16 v5, 0x13

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x17

    .line 304
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00efa"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x18

    .line 305
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isc"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x19

    .line 306
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00efsc"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1a

    .line 307
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ad"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1b

    .line 308
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ed"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1c

    .line 309
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "id"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1d

    .line 310
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ie"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1e

    .line 311
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "re"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x1f

    .line 312
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "dre"

    const/16 v5, 0x1e

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x20

    .line 313
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ase"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x21

    .line 314
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iese"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x22

    .line 315
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aste"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x23

    .line 316
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iste"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x24

    .line 317
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ii"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x25

    .line 318
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ini"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x26

    .line 319
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "esqui"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x27

    .line 320
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eixi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x28

    .line 321
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itzi"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x29

    .line 322
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "am"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x2a

    .line 323
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "em"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x2b

    .line 324
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x2c

    .line 325
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "irem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x2d

    .line 326
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e0rem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x2e

    .line 327
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00edrem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x2f

    .line 328
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e0ssem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x30

    .line 329
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9ssem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x31

    .line 330
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iguem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x32

    .line 331
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efguem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x33

    .line 332
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "avem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x34

    .line 333
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e0vem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x35

    .line 334
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1vem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x36

    .line 335
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ir\u00ecem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x37

    .line 336
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00edem"

    const/16 v5, 0x2a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x38

    .line 337
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ar\u00edem"

    const/16 v5, 0x37

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x39

    .line 338
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ir\u00edem"

    const/16 v5, 0x37

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x3a

    .line 339
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "assim"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x3b

    .line 340
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "essim"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x3c

    .line 341
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issim"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x3d

    .line 342
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e0ssim"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x3e

    .line 343
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e8ssim"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x3f

    .line 344
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9ssim"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x40

    .line 345
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edssim"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x41

    .line 346
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00efm"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x42

    .line 347
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "an"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x43

    .line 348
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aban"

    const/16 v5, 0x42

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x44

    .line 349
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arian"

    const/16 v5, 0x42

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x45

    .line 350
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aran"

    const/16 v5, 0x42

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x46

    .line 351
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ieran"

    const/16 v5, 0x42

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x47

    .line 352
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iran"

    const/16 v5, 0x42

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x48

    .line 353
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00edan"

    const/16 v5, 0x42

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x49

    .line 354
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ar\u00edan"

    const/16 v5, 0x48

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x4a

    .line 355
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "er\u00edan"

    const/16 v5, 0x48

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x4b

    .line 356
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ir\u00edan"

    const/16 v5, 0x48

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    .line 357
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v12

    const/16 v0, 0x4d

    .line 358
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ien"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x4e

    .line 359
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "arien"

    const/16 v5, 0x4d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x4f

    .line 360
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "irien"

    const/16 v5, 0x4d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x50

    .line 361
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aren"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x51

    .line 362
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eren"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x52

    .line 363
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iren"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x53

    .line 364
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e0ren"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x54

    .line 365
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efren"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x55

    .line 366
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "asen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x56

    .line 367
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iesen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x57

    .line 368
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "assen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x58

    .line 369
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "essen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x59

    .line 370
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "issen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5a

    .line 371
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9ssen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5b

    .line 372
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efssen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5c

    .line 373
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "esquen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5d

    .line 374
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "isquen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5e

    .line 375
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efsquen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x5f

    .line 376
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aven"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x60

    .line 377
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ixen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x61

    .line 378
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eixen"

    const/16 v5, 0x60

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x62

    .line 379
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efxen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x63

    .line 380
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efen"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x64

    .line 381
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "in"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x65

    .line 382
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "inin"

    const/16 v5, 0x64

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x66

    .line 383
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "sin"

    const/16 v5, 0x64

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x67

    .line 384
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "isin"

    const/16 v5, 0x66

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x68

    .line 385
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "assin"

    const/16 v5, 0x66

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x69

    .line 386
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "essin"

    const/16 v5, 0x66

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x6a

    .line 387
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "issin"

    const/16 v5, 0x66

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x6b

    .line 388
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efssin"

    const/16 v5, 0x66

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x6c

    .line 389
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "esquin"

    const/16 v5, 0x64

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x6d

    .line 390
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eixin"

    const/16 v5, 0x64

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x6e

    .line 391
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aron"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x6f

    .line 392
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ieron"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x70

    .line 393
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e1n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x71

    .line 394
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e1n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x72

    .line 395
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e1n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x73

    .line 396
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00efn"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x74

    .line 397
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ado"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x75

    .line 398
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ido"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x76

    .line 399
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ando"

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x77

    .line 400
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iendo"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x78

    .line 401
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "io"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x79

    .line 402
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ixo"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x7a

    .line 403
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eixo"

    const/16 v5, 0x79

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x7b

    .line 404
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00efxo"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x7c

    .line 405
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itzo"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x7d

    .line 406
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x7e

    .line 407
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "tzar"

    const/16 v5, 0x7d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x7f

    .line 408
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x80

    .line 409
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eixer"

    const/16 v5, 0x7f

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x81

    .line 410
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x82

    .line 411
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ador"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x83

    .line 412
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "as"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x84

    .line 413
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "abas"

    const/16 v5, 0x83

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x85

    .line 414
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "adas"

    const/16 v5, 0x83

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x86

    .line 415
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "idas"

    const/16 v5, 0x83

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x87

    .line 416
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aras"

    const/16 v5, 0x83

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x88

    .line 417
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ieras"

    const/16 v5, 0x83

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x89

    .line 418
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00edas"

    const/16 v5, 0x83

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8a

    .line 419
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ar\u00edas"

    const/16 v5, 0x89

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8b

    .line 420
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "er\u00edas"

    const/16 v5, 0x89

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8c

    .line 421
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ir\u00edas"

    const/16 v5, 0x89

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x8d

    .line 422
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ids"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 423
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "es"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v11

    const/16 v0, 0x8f

    .line 424
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ades"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x90

    .line 425
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ides"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x91

    .line 426
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "udes"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x92

    .line 427
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efdes"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x93

    .line 428
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "atges"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x94

    .line 429
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ies"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x95

    .line 430
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aries"

    const/16 v5, 0x94

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x96

    .line 431
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "iries"

    const/16 v5, 0x94

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x97

    .line 432
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ares"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x98

    .line 433
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ires"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x99

    .line 434
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "adores"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9a

    .line 435
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efres"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9b

    .line 436
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ases"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9c

    .line 437
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ieses"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9d

    .line 438
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "asses"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9e

    .line 439
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "esses"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9f

    .line 440
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "isses"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa0

    .line 441
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efsses"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa1

    .line 442
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ques"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa2

    .line 443
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "esques"

    const/16 v5, 0xa1

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa3

    .line 444
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efsques"

    const/16 v5, 0xa1

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa4

    .line 445
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aves"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa5

    .line 446
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ixes"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa6

    .line 447
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eixes"

    const/16 v5, 0xa5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa7

    .line 448
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efxes"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa8

    .line 449
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efes"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v5, v11

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xa9

    .line 450
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abais"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xaa

    .line 451
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "arais"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xab

    .line 452
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ierais"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xac

    .line 453
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edais"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xad

    .line 454
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ar\u00edais"

    const/16 v5, 0xac

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xae

    .line 455
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "er\u00edais"

    const/16 v5, 0xac

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xaf

    .line 456
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ir\u00edais"

    const/16 v5, 0xac

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xb0

    .line 457
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aseis"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb1

    .line 458
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ieseis"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb2

    .line 459
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asteis"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb3

    .line 460
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isteis"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb4

    .line 461
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "inis"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb5

    .line 462
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sis"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xb6

    .line 463
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "isis"

    const/16 v5, 0xb5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb7

    .line 464
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "assis"

    const/16 v5, 0xb5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb8

    .line 465
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "essis"

    const/16 v5, 0xb5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb9

    .line 466
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "issis"

    const/16 v5, 0xb5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xba

    .line 467
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efssis"

    const/16 v5, 0xb5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xbb

    .line 468
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "esquis"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xbc

    .line 469
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eixis"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xbd

    .line 470
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itzis"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xbe

    .line 471
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1is"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xbf

    .line 472
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e9is"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc0

    .line 473
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e9is"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc1

    .line 474
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e9is"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc2

    .line 475
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ams"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc3

    .line 476
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ados"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc4

    .line 477
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "idos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc5

    .line 478
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xc6

    .line 479
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1bamos"

    const/16 v5, 0xc5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc7

    .line 480
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1ramos"

    const/16 v5, 0xc5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc8

    .line 481
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "i\u00e9ramos"

    const/16 v5, 0xc5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc9

    .line 482
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00edamos"

    const/16 v5, 0xc5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xca

    .line 483
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ar\u00edamos"

    const/16 v5, 0xc9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xcb

    .line 484
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "er\u00edamos"

    const/16 v5, 0xc9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xcc

    .line 485
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ir\u00edamos"

    const/16 v5, 0xc9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xcd

    .line 486
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aremos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xce

    .line 487
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eremos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xcf

    .line 488
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iremos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd0

    .line 489
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1semos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd1

    .line 490
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00e9semos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd2

    .line 491
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imos"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd3

    .line 492
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "adors"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd4

    .line 493
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ass"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xd5

    .line 494
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "erass"

    const/16 v5, 0xd4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xd6

    .line 495
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ess"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd7

    .line 496
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ats"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd8

    .line 497
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "its"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd9

    .line 498
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ents"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xda

    .line 499
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e0s"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xdb

    .line 500
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ar\u00e0s"

    const/16 v5, 0xda

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xdc

    .line 501
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ir\u00e0s"

    const/16 v5, 0xda

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xdd

    .line 502
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e1s"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xde

    .line 503
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e1s"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xdf

    .line 504
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e1s"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe0

    .line 505
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9s"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xe1

    .line 506
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ar\u00e9s"

    const/16 v5, 0xe0

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xe2

    .line 507
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eds"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe3

    .line 508
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00efs"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe4

    .line 509
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "at"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe5

    .line 510
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "it"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe6

    .line 511
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ant"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe7

    .line 512
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ent"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe8

    .line 513
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "int"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe9

    .line 514
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ut"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xea

    .line 515
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eft"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xeb

    .line 516
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "au"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xec

    .line 517
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "erau"

    const/16 v5, 0xeb

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xed

    .line 518
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ieu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xee

    .line 519
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ineu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xef

    .line 520
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "areu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf0

    .line 521
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ireu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf1

    .line 522
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e0reu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf2

    .line 523
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edreu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf3

    .line 524
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asseu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf4

    .line 525
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "esseu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xf5

    .line 526
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eresseu"

    const/16 v5, 0xf4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xf6

    .line 527
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e0sseu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf7

    .line 528
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9sseu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf8

    .line 529
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "igueu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf9

    .line 530
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00efgueu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xfa

    .line 531
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e0veu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xfb

    .line 532
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1veu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xfc

    .line 533
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itzeu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xfd

    .line 534
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eceu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xfe

    .line 535
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ir\u00eceu"

    const/16 v5, 0xfd

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xff

    .line 536
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edeu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x100

    .line 537
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ar\u00edeu"

    const/16 v5, 0xff

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x101

    .line 538
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ir\u00edeu"

    const/16 v5, 0xff

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x102

    .line 539
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "assiu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x103

    .line 540
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issiu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x104

    .line 541
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e0ssiu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x105

    .line 542
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e8ssiu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x106

    .line 543
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9ssiu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x107

    .line 544
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edssiu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x108

    .line 545
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00efu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x109

    .line 546
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ix"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x10a

    .line 547
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eix"

    const/16 v5, 0x109

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x10b

    .line 548
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00efx"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x10c

    .line 549
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x10d

    .line 550
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00e0"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x10e

    .line 551
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e0"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x10f

    .line 552
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e0"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x110

    .line 553
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itz\u00e0"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x111

    .line 554
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e1"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x112

    .line 555
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e1"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x113

    .line 556
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e1"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x114

    .line 557
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e8"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x115

    .line 558
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e9"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x116

    .line 559
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e9"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x117

    .line 560
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e9"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x118

    .line 561
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ed"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x119

    .line 562
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00ef"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x11a

    .line 563
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u00f3"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 280
    sput-object v9, Lorg/tartarus/snowball/ext/CatalanStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 566
    const/16 v0, 0x16

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    .line 567
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v13

    .line 568
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v10

    const/4 v6, 0x2

    .line 569
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x3

    .line 570
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00efn"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x4

    .line 571
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "o"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x5

    .line 572
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x6

    .line 573
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "s"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v0, 0x7

    .line 574
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "is"

    const/4 v5, 0x6

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8

    .line 575
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "os"

    const/4 v5, 0x6

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9

    .line 576
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00efs"

    const/4 v5, 0x6

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xa

    .line 577
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "it"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb

    .line 578
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc

    .line 579
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iu"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd

    .line 580
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iqu"

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe

    .line 581
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf

    .line 582
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e0"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x10

    .line 583
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x11

    .line 584
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x12

    .line 585
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ec"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x13

    .line 586
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ed"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x14

    .line 587
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ef"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x15

    .line 588
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f3"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/CatalanStemmer;->methodObject:Lorg/tartarus/snowball/ext/CatalanStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 566
    sput-object v9, Lorg/tartarus/snowball/ext/CatalanStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 591
    const/16 v0, 0x14

    new-array v0, v0, [C

    const/16 v1, 0x11

    aput-char v1, v0, v13

    const/16 v1, 0x41

    aput-char v1, v0, v10

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x80

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x81

    aput-char v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x51

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/4 v2, 0x6

    aput-char v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0xa

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/CatalanStemmer;->g_v:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/CatalanStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/CatalanStemmer;

    .prologue
    .line 597
    iget v0, p1, Lorg/tartarus/snowball/ext/CatalanStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->I_p2:I

    .line 598
    iget v0, p1, Lorg/tartarus/snowball/ext/CatalanStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->I_p1:I

    .line 599
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 600
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 785
    iget v0, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 787
    const/4 v0, 0x0

    .line 789
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 793
    iget v0, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 795
    const/4 v0, 0x0

    .line 797
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_attached_pronoun()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 804
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->ket:I

    .line 806
    sget-object v2, Lorg/tartarus/snowball/ext/CatalanStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x27

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/CatalanStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 807
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 827
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 812
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->bra:I

    .line 813
    packed-switch v0, :pswitch_data_0

    .line 827
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 819
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 824
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_del()V

    goto :goto_1

    .line 813
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_cleaning()Z
    .locals 4

    .prologue
    .line 690
    :goto_0
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 694
    .local v1, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->bra:I

    .line 696
    sget-object v2, Lorg/tartarus/snowball/ext/CatalanStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0xd

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/CatalanStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 697
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 778
    :cond_0
    :pswitch_0
    iput v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 781
    const/4 v2, 0x1

    return v2

    .line 702
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->ket:I

    .line 703
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 709
    :pswitch_1
    const-string v2, "a"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 714
    :pswitch_2
    const-string v2, "a"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 719
    :pswitch_3
    const-string v2, "e"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 724
    :pswitch_4
    const-string v2, "e"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 729
    :pswitch_5
    const-string v2, "i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 734
    :pswitch_6
    const-string v2, "i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 739
    :pswitch_7
    const-string v2, "o"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 744
    :pswitch_8
    const-string v2, "o"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 749
    :pswitch_9
    const-string v2, "u"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 754
    :pswitch_a
    const-string v2, "u"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 759
    :pswitch_b
    const-string v2, "i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 764
    :pswitch_c
    const-string v2, "."

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 769
    :pswitch_d
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 773
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    goto :goto_0

    .line 703
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 5

    .prologue
    const/16 v4, 0xfc

    const/16 v3, 0x61

    .line 605
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    iput v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->I_p1:I

    .line 606
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    iput v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->I_p2:I

    .line 608
    iget v0, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 615
    .local v0, "v_1":I
    :goto_0
    sget-object v1, Lorg/tartarus/snowball/ext/CatalanStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/CatalanStemmer;->in_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_3

    .line 621
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    if-lt v1, v2, :cond_1

    .line 680
    :cond_0
    :goto_1
    iput v0, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 681
    const/4 v1, 0x1

    return v1

    .line 625
    :cond_1
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    goto :goto_0

    .line 641
    :cond_2
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 631
    :cond_3
    sget-object v1, Lorg/tartarus/snowball/ext/CatalanStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/CatalanStemmer;->out_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_4

    .line 637
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    if-lt v1, v2, :cond_2

    goto :goto_1

    .line 644
    :cond_4
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->I_p1:I

    .line 649
    :goto_2
    sget-object v1, Lorg/tartarus/snowball/ext/CatalanStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/CatalanStemmer;->in_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_6

    .line 655
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    if-ge v1, v2, :cond_0

    .line 659
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    goto :goto_2

    .line 675
    :cond_5
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 665
    :cond_6
    sget-object v1, Lorg/tartarus/snowball/ext/CatalanStemmer;->g_v:[C

    invoke-virtual {p0, v1, v3, v4}, Lorg/tartarus/snowball/ext/CatalanStemmer;->out_grouping([CII)Z

    move-result v1

    if-nez v1, :cond_7

    .line 671
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    if-lt v1, v2, :cond_5

    goto :goto_1

    .line 678
    :cond_7
    iget v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->I_p2:I

    goto :goto_1
.end method

.method private r_residual_suffix()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 944
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->ket:I

    .line 946
    sget-object v2, Lorg/tartarus/snowball/ext/CatalanStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x16

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/CatalanStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 947
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 977
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 952
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->bra:I

    .line 953
    packed-switch v0, :pswitch_data_0

    .line 977
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 959
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 964
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_del()V

    goto :goto_1

    .line 969
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 974
    const-string v1, "ic"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 953
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_standard_suffix()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 834
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->ket:I

    .line 836
    sget-object v2, Lorg/tartarus/snowball/ext/CatalanStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0xc8

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/CatalanStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 837
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 897
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 842
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->bra:I

    .line 843
    packed-switch v0, :pswitch_data_0

    .line 897
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 849
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 854
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_del()V

    goto :goto_1

    .line 859
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 864
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_del()V

    goto :goto_1

    .line 869
    :pswitch_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 874
    const-string v1, "log"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 879
    :pswitch_4
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 884
    const-string v1, "ic"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 889
    :pswitch_5
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 894
    const-string v1, "c"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 843
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private r_verb_suffix()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 904
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->ket:I

    .line 906
    sget-object v2, Lorg/tartarus/snowball/ext/CatalanStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x11b

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/CatalanStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 907
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 937
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 912
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->bra:I

    .line 913
    packed-switch v0, :pswitch_data_0

    .line 937
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 919
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 924
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_del()V

    goto :goto_1

    .line 929
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_R2()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 934
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->slice_del()V

    goto :goto_1

    .line 913
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1061
    instance-of v0, p1, Lorg/tartarus/snowball/ext/CatalanStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1066
    const-class v0, Lorg/tartarus/snowball/ext/CatalanStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 8

    .prologue
    .line 990
    iget v0, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 993
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_mark_regions()Z

    move-result v6

    if-nez v6, :cond_0

    .line 998
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 1000
    iget v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit_backward:I

    iget v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    iput v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 1003
    iget v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 1006
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_attached_pronoun()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1011
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 1013
    iget v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 1018
    .local v2, "v_3":I
    iget v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 1021
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_standard_suffix()Z

    move-result v6

    if-nez v6, :cond_2

    .line 1027
    iget v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 1029
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_verb_suffix()Z

    move-result v6

    if-nez v6, :cond_2

    .line 1035
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 1037
    iget v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    sub-int v4, v6, v7

    .line 1040
    .local v4, "v_5":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_residual_suffix()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1045
    :cond_3
    iget v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit:I

    sub-int/2addr v6, v4

    iput v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 1046
    iget v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->limit_backward:I

    iput v6, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 1047
    iget v5, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 1050
    .local v5, "v_6":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/CatalanStemmer;->r_cleaning()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1055
    :cond_4
    iput v5, p0, Lorg/tartarus/snowball/ext/CatalanStemmer;->cursor:I

    .line 1056
    const/4 v6, 0x1

    return v6
.end method
