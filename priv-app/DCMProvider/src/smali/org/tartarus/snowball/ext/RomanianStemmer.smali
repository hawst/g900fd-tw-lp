.class public Lorg/tartarus/snowball/ext/RomanianStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "RomanianStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private B_standard_suffix_removed:Z

.field private I_p1:I

.field private I_p2:I

.field private I_pV:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/RomanianStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    .line 19
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "I"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "U"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 25
    const/16 v0, 0x10

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ea"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a\u0163ia"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aua"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 29
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iua"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 30
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a\u0163ie"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 31
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ele"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ile"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iile"

    const/4 v2, 0x6

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iei"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atei"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ii"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ului"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ul"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "elor"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ilor"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iilor"

    const/16 v2, 0xe

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 25
    sput-object v6, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 44
    const/16 v0, 0x2e

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icala"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iciva"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ativa"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itiva"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icale"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a\u0163iune"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u0163iune"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atoare"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itoare"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0103toare"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icitate"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abilitate"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ibilitate"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivitate"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icive"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 60
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ative"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itive"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icali"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atori"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icatori"

    const/16 v2, 0x12

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itori"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0103tori"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icitati"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abilitati"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 69
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivitati"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 70
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icivi"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 71
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ativi"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itivi"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icit\u0103i"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abilit\u0103i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 75
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivit\u0103i"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 76
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icit\u0103\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 77
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abilit\u0103\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivit\u0103\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 79
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ical"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ator"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icator"

    const/16 v2, 0x23

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itor"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x26

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0103tor"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x27

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iciv"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x28

    .line 85
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ativ"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x29

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itiv"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2a

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ical\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2b

    .line 88
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iciv\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2c

    .line 89
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ativ\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2d

    .line 90
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itiv\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 44
    sput-object v6, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 93
    const/16 v0, 0x3e

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ica"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 95
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abila"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ibila"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 97
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oasa"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 98
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ata"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ita"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "anta"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 101
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ista"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uta"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 103
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iva"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 104
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 105
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ice"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abile"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ibile"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isme"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 109
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iune"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oase"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 111
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ate"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itate"

    const/16 v2, 0x11

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ite"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ante"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 115
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iste"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 116
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ute"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ive"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 118
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ici"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 119
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abili"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ibili"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 121
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iuni"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 122
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "atori"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 123
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 124
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ati"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 125
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itati"

    const/16 v2, 0x1e

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 126
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 127
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "anti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 128
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isti"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 129
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 130
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u015fti"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 131
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x26

    .line 132
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "it\u0103i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x27

    .line 133
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "o\u015fi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x28

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "it\u0103\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x29

    .line 135
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abil"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2a

    .line 136
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ibil"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2b

    .line 137
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ism"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2c

    .line 138
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ator"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2d

    .line 139
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "os"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2e

    .line 140
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "at"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2f

    .line 141
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "it"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x30

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ant"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x31

    .line 143
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ist"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x32

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ut"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x33

    .line 145
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iv"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x34

    .line 146
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x35

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abil\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x36

    .line 148
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ibil\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x37

    .line 149
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oas\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x38

    .line 150
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "at\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x39

    .line 151
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "it\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3a

    .line 152
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ant\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3b

    .line 153
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ist\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3c

    .line 154
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ut\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3d

    .line 155
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iv\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 93
    sput-object v6, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 158
    const/16 v0, 0x5e

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 159
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ea"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 160
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ia"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 161
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "esc"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 162
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0103sc"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 163
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ind"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 164
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2nd"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 165
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "are"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 166
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ere"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 167
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ire"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 168
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2re"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 169
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "se"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 170
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ase"

    const/16 v2, 0xa

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 171
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sese"

    const/16 v2, 0xa

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 172
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ise"

    const/16 v2, 0xa

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 173
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "use"

    const/16 v2, 0xa

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 174
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2se"

    const/16 v2, 0xa

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 175
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e\u015fte"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 176
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0103\u015fte"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 177
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eze"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 178
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ai"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 179
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eai"

    const/16 v2, 0x13

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 180
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iai"

    const/16 v2, 0x13

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 181
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sei"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 182
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e\u015fti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 183
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0103\u015fti"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 184
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ui"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 185
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ezi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 186
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 187
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a\u015fi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 188
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "se\u015fi"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 189
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ase\u015fi"

    const/16 v2, 0x1d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 190
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sese\u015fi"

    const/16 v2, 0x1d

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 191
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ise\u015fi"

    const/16 v2, 0x1d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 192
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "use\u015fi"

    const/16 v2, 0x1d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 193
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2se\u015fi"

    const/16 v2, 0x1d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 194
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u015fi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 195
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "u\u015fi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 196
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2\u015fi"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x26

    .line 197
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x27

    .line 198
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ea\u0163i"

    const/16 v2, 0x26

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x28

    .line 199
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ia\u0163i"

    const/16 v2, 0x26

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x29

    .line 200
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2a

    .line 201
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2b

    .line 202
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2c

    .line 203
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u0103\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2d

    .line 204
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ser\u0103\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2e

    .line 205
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aser\u0103\u0163i"

    const/16 v2, 0x2d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2f

    .line 206
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "seser\u0103\u0163i"

    const/16 v2, 0x2d

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x30

    .line 207
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iser\u0103\u0163i"

    const/16 v2, 0x2d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x31

    .line 208
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "user\u0103\u0163i"

    const/16 v2, 0x2d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x32

    .line 209
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2ser\u0103\u0163i"

    const/16 v2, 0x2d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x33

    .line 210
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u0103\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x34

    .line 211
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ur\u0103\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x35

    .line 212
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2r\u0103\u0163i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x36

    .line 213
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "am"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x37

    .line 214
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eam"

    const/16 v2, 0x36

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x38

    .line 215
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iam"

    const/16 v2, 0x36

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x39

    .line 216
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "em"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3a

    .line 217
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asem"

    const/16 v2, 0x39

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3b

    .line 218
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sesem"

    const/16 v2, 0x39

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3c

    .line 219
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isem"

    const/16 v2, 0x39

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3d

    .line 220
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "usem"

    const/16 v2, 0x39

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3e

    .line 221
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2sem"

    const/16 v2, 0x39

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3f

    .line 222
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "im"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x40

    .line 223
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2m"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x41

    .line 224
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0103m"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x42

    .line 225
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u0103m"

    const/16 v2, 0x41

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x43

    .line 226
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ser\u0103m"

    const/16 v2, 0x41

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x44

    .line 227
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aser\u0103m"

    const/16 v2, 0x43

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x45

    .line 228
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "seser\u0103m"

    const/16 v2, 0x43

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x46

    .line 229
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iser\u0103m"

    const/16 v2, 0x43

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x47

    .line 230
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "user\u0103m"

    const/16 v2, 0x43

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x48

    .line 231
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2ser\u0103m"

    const/16 v2, 0x43

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x49

    .line 232
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u0103m"

    const/16 v2, 0x41

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4a

    .line 233
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ur\u0103m"

    const/16 v2, 0x41

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4b

    .line 234
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2r\u0103m"

    const/16 v2, 0x41

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4c

    .line 235
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "au"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4d

    .line 236
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eau"

    const/16 v2, 0x4c

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4e

    .line 237
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iau"

    const/16 v2, 0x4c

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4f

    .line 238
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "indu"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x50

    .line 239
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2ndu"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x51

    .line 240
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ez"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x52

    .line 241
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "easc\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x53

    .line 242
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x54

    .line 243
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ser\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x55

    .line 244
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aser\u0103"

    const/16 v2, 0x54

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x56

    .line 245
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "seser\u0103"

    const/16 v2, 0x54

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x57

    .line 246
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iser\u0103"

    const/16 v2, 0x54

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x58

    .line 247
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "user\u0103"

    const/16 v2, 0x54

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x59

    .line 248
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2ser\u0103"

    const/16 v2, 0x54

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5a

    .line 249
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5b

    .line 250
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ur\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5c

    .line 251
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2r\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5d

    .line 252
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eaz\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 158
    sput-object v6, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 255
    const/4 v0, 0x5

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 256
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 257
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 258
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ie"

    const/4 v2, 0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 259
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 260
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u0103"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->methodObject:Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 255
    sput-object v6, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 263
    const/16 v0, 0x15

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/4 v2, 0x2

    aput-char v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x20

    aput-char v2, v0, v1

    const/16 v1, 0x14

    const/4 v2, 0x4

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/RomanianStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/RomanianStemmer;

    .prologue
    .line 271
    iget-boolean v0, p1, Lorg/tartarus/snowball/ext/RomanianStemmer;->B_standard_suffix_removed:Z

    iput-boolean v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->B_standard_suffix_removed:Z

    .line 272
    iget v0, p1, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_p2:I

    .line 273
    iget v0, p1, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_p1:I

    .line 274
    iget v0, p1, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_pV:I

    iput v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_pV:I

    .line 275
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 276
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 616
    iget v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 618
    const/4 v0, 0x0

    .line 620
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 624
    iget v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 626
    const/4 v0, 0x0

    .line 628
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_RV()Z
    .locals 2

    .prologue
    .line 608
    iget v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_pV:I

    iget v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 610
    const/4 v0, 0x0

    .line 612
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_combo_suffix()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 709
    iget v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 712
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->ket:I

    .line 714
    sget-object v4, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x2e

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/RomanianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 715
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 764
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 720
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->bra:I

    .line 722
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_R1()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 727
    packed-switch v0, :pswitch_data_0

    .line 762
    :goto_1
    iput-boolean v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->B_standard_suffix_removed:Z

    .line 763
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    sub-int/2addr v2, v1

    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    move v2, v3

    .line 764
    goto :goto_0

    .line 733
    :pswitch_1
    const-string v2, "abil"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 738
    :pswitch_2
    const-string v2, "ibil"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 743
    :pswitch_3
    const-string v2, "iv"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 748
    :pswitch_4
    const-string v2, "ic"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 753
    :pswitch_5
    const-string v2, "at"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 758
    :pswitch_6
    const-string v2, "it"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 727
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 9

    .prologue
    const/16 v8, 0x103

    const/16 v7, 0x61

    .line 361
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_pV:I

    .line 362
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_p1:I

    .line 363
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_p2:I

    .line 365
    iget v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 370
    .local v0, "v_1":I
    iget v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 373
    .local v1, "v_2":I
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_3

    .line 429
    :cond_0
    iput v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 431
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_6

    .line 479
    :cond_1
    :goto_0
    iput v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 481
    iget v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 488
    .local v4, "v_8":I
    :goto_1
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_c

    .line 494
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    if-lt v5, v6, :cond_a

    .line 553
    :cond_2
    :goto_2
    iput v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 554
    const/4 v5, 0x1

    return v5

    .line 379
    .end local v4    # "v_8":I
    :cond_3
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 382
    .local v2, "v_3":I
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_5

    .line 404
    :goto_3
    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 406
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->in_grouping([CII)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 414
    :goto_4
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 420
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    if-ge v5, v6, :cond_0

    .line 424
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    goto :goto_4

    .line 400
    :cond_4
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 390
    :cond_5
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 396
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    if-lt v5, v6, :cond_4

    goto :goto_3

    .line 437
    .end local v2    # "v_3":I
    :cond_6
    iget v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 440
    .local v3, "v_6":I
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_9

    .line 462
    :goto_5
    iput v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 464
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->in_grouping([CII)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 469
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    if-ge v5, v6, :cond_1

    .line 473
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 477
    .end local v3    # "v_6":I
    :cond_7
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_pV:I

    goto :goto_0

    .line 458
    .restart local v3    # "v_6":I
    :cond_8
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 448
    :cond_9
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 454
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    if-lt v5, v6, :cond_8

    goto :goto_5

    .line 498
    .end local v3    # "v_6":I
    .restart local v4    # "v_8":I
    :cond_a
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    goto/16 :goto_1

    .line 514
    :cond_b
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 504
    :cond_c
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_d

    .line 510
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    if-lt v5, v6, :cond_b

    goto/16 :goto_2

    .line 517
    :cond_d
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_p1:I

    .line 522
    :goto_6
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_f

    .line 528
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    if-ge v5, v6, :cond_2

    .line 532
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    goto :goto_6

    .line 548
    :cond_e
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 538
    :cond_f
    sget-object v5, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_10

    .line 544
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    if-lt v5, v6, :cond_e

    goto/16 :goto_2

    .line 551
    :cond_10
    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_p2:I

    goto/16 :goto_2
.end method

.method private r_postlude()Z
    .locals 4

    .prologue
    .line 563
    :goto_0
    iget v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 567
    .local v1, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->bra:I

    .line 569
    sget-object v2, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/RomanianStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 570
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 601
    :cond_0
    :pswitch_0
    iput v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 604
    const/4 v2, 0x1

    return v2

    .line 575
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->ket:I

    .line 576
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 582
    :pswitch_1
    const-string v2, "i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 587
    :pswitch_2
    const-string v2, "u"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 592
    :pswitch_3
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 596
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    goto :goto_0

    .line 576
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_prelude()Z
    .locals 8

    .prologue
    const/16 v7, 0x103

    const/16 v6, 0x61

    const/4 v5, 0x1

    .line 286
    :goto_0
    iget v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 291
    .local v0, "v_1":I
    :goto_1
    iget v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 294
    .local v1, "v_2":I
    sget-object v3, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v3, v6, v7}, Lorg/tartarus/snowball/ext/RomanianStemmer;->in_grouping([CII)Z

    move-result v3

    if-nez v3, :cond_1

    .line 339
    :cond_0
    iput v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 340
    iget v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    if-lt v3, v4, :cond_4

    .line 348
    iput v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 351
    return v5

    .line 299
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->bra:I

    .line 302
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 306
    .local v2, "v_3":I
    const-string v3, "u"

    invoke-virtual {p0, v5, v3}, Lorg/tartarus/snowball/ext/RomanianStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 320
    :cond_2
    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 323
    const-string v3, "i"

    invoke-virtual {p0, v5, v3}, Lorg/tartarus/snowball/ext/RomanianStemmer;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 328
    iget v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->ket:I

    .line 329
    sget-object v3, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v3, v6, v7}, Lorg/tartarus/snowball/ext/RomanianStemmer;->in_grouping([CII)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 334
    const-string v3, "I"

    invoke-virtual {p0, v3}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    .line 336
    :goto_2
    iput v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    goto :goto_0

    .line 311
    :cond_3
    iget v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->ket:I

    .line 312
    sget-object v3, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    invoke-virtual {p0, v3, v6, v7}, Lorg/tartarus/snowball/ext/RomanianStemmer;->in_grouping([CII)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 317
    const-string v3, "U"

    invoke-virtual {p0, v3}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 344
    .end local v2    # "v_3":I
    :cond_4
    iget v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    goto :goto_1
.end method

.method private r_standard_suffix()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 772
    iput-boolean v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->B_standard_suffix_removed:Z

    .line 776
    :cond_0
    iget v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 779
    .local v1, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_combo_suffix()Z

    move-result v4

    if-nez v4, :cond_0

    .line 785
    iget v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 789
    iget v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->ket:I

    .line 791
    sget-object v4, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x3e

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/RomanianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 792
    .local v0, "among_var":I
    if-nez v0, :cond_2

    .line 832
    :cond_1
    :goto_0
    :pswitch_0
    return v2

    .line 797
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->bra:I

    .line 799
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_R2()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 804
    packed-switch v0, :pswitch_data_0

    .line 831
    :goto_1
    iput-boolean v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->B_standard_suffix_removed:Z

    move v2, v3

    .line 832
    goto :goto_0

    .line 810
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_del()V

    goto :goto_1

    .line 815
    :pswitch_2
    const-string/jumbo v4, "\u0163"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/RomanianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 820
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->bra:I

    .line 822
    const-string v2, "t"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 827
    :pswitch_3
    const-string v2, "ist"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 804
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_step_0()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 636
    iget v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->ket:I

    .line 638
    sget-object v3, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/16 v4, 0x10

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/RomanianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 639
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 702
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 644
    :cond_1
    iget v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->bra:I

    .line 646
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_R1()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 650
    packed-switch v0, :pswitch_data_0

    .line 702
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 656
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_del()V

    goto :goto_1

    .line 661
    :pswitch_2
    const-string v2, "a"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 666
    :pswitch_3
    const-string v2, "e"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 671
    :pswitch_4
    const-string v2, "i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 677
    :pswitch_5
    iget v3, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iget v4, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    sub-int v1, v3, v4

    .line 680
    .local v1, "v_1":I
    const/4 v3, 0x2

    const-string v4, "ab"

    invoke-virtual {p0, v3, v4}, Lorg/tartarus/snowball/ext/RomanianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 686
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    sub-int/2addr v2, v1

    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 689
    const-string v2, "i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 694
    .end local v1    # "v_1":I
    :pswitch_6
    const-string v2, "at"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 699
    :pswitch_7
    const-string v2, "a\u0163i"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 650
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private r_verb_suffix()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 841
    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 843
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_pV:I

    if-ge v6, v7, :cond_0

    .line 897
    :goto_0
    return v4

    .line 847
    :cond_0
    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->I_pV:I

    iput v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 848
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit_backward:I

    .line 849
    .local v2, "v_2":I
    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit_backward:I

    .line 850
    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 853
    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->ket:I

    .line 855
    sget-object v6, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/16 v7, 0x5e

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/RomanianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 856
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 858
    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit_backward:I

    goto :goto_0

    .line 862
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->bra:I

    .line 863
    packed-switch v0, :pswitch_data_0

    .line 896
    :goto_1
    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit_backward:I

    move v4, v5

    .line 897
    goto :goto_0

    .line 865
    :pswitch_0
    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit_backward:I

    goto :goto_0

    .line 871
    :pswitch_1
    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 873
    .local v3, "v_3":I
    sget-object v6, Lorg/tartarus/snowball/ext/RomanianStemmer;->g_v:[C

    const/16 v7, 0x61

    const/16 v8, 0x103

    invoke-virtual {p0, v6, v7, v8}, Lorg/tartarus/snowball/ext/RomanianStemmer;->out_grouping_b([CII)Z

    move-result v6

    if-nez v6, :cond_2

    .line 879
    iget v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 881
    const-string v6, "u"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/RomanianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 883
    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit_backward:I

    goto :goto_0

    .line 888
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_del()V

    goto :goto_1

    .line 893
    .end local v3    # "v_3":I
    :pswitch_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_del()V

    goto :goto_1

    .line 863
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_vowel_suffix()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 904
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->ket:I

    .line 906
    sget-object v2, Lorg/tartarus/snowball/ext/RomanianStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x5

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/RomanianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 907
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 927
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 912
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->bra:I

    .line 914
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_RV()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 918
    packed-switch v0, :pswitch_data_0

    .line 927
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 924
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->slice_del()V

    goto :goto_1

    .line 918
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1033
    instance-of v0, p1, Lorg/tartarus/snowball/ext/RomanianStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1038
    const-class v0, Lorg/tartarus/snowball/ext/RomanianStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 10

    .prologue
    .line 942
    iget v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 945
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_prelude()Z

    move-result v8

    if-nez v8, :cond_0

    .line 950
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 952
    iget v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 955
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_mark_regions()Z

    move-result v8

    if-nez v8, :cond_1

    .line 960
    :cond_1
    iput v1, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 962
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    iput v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit_backward:I

    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iput v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 965
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    sub-int v2, v8, v9

    .line 968
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_step_0()Z

    move-result v8

    if-nez v8, :cond_2

    .line 973
    :cond_2
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    sub-int/2addr v8, v2

    iput v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 975
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    sub-int v3, v8, v9

    .line 978
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_standard_suffix()Z

    move-result v8

    if-nez v8, :cond_3

    .line 983
    :cond_3
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    sub-int/2addr v8, v3

    iput v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 985
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    sub-int v4, v8, v9

    .line 990
    .local v4, "v_5":I
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    sub-int v5, v8, v9

    .line 993
    .local v5, "v_6":I
    iget-boolean v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->B_standard_suffix_removed:Z

    if-nez v8, :cond_4

    .line 999
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    sub-int/2addr v8, v5

    iput v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 1001
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_verb_suffix()Z

    move-result v8

    if-nez v8, :cond_4

    .line 1007
    :cond_4
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    sub-int/2addr v8, v4

    iput v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 1009
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    iget v9, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    sub-int v6, v8, v9

    .line 1012
    .local v6, "v_7":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_vowel_suffix()Z

    move-result v8

    if-nez v8, :cond_5

    .line 1017
    :cond_5
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit:I

    sub-int/2addr v8, v6

    iput v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 1018
    iget v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->limit_backward:I

    iput v8, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 1019
    iget v7, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 1022
    .local v7, "v_8":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/RomanianStemmer;->r_postlude()Z

    move-result v8

    if-nez v8, :cond_6

    .line 1027
    :cond_6
    iput v7, p0, Lorg/tartarus/snowball/ext/RomanianStemmer;->cursor:I

    .line 1028
    const/4 v8, 0x1

    return v8
.end method
