.class public Lorg/tartarus/snowball/ext/HungarianStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "HungarianStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_10:[Lorg/tartarus/snowball/Among;

.field private static final a_11:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final a_6:[Lorg/tartarus/snowball/Among;

.field private static final a_7:[Lorg/tartarus/snowball/Among;

.field private static final a_8:[Lorg/tartarus/snowball/Among;

.field private static final a_9:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, -0x1

    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/HungarianStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    .line 19
    const/16 v0, 0x8

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "cs"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v12

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dzs"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gy"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 23
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ly"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v13

    const/4 v7, 0x4

    .line 24
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ny"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 25
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "sz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ty"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zs"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 30
    new-array v6, v11, [Lorg/tartarus/snowball/Among;

    .line 31
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v12

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v11

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 30
    sput-object v6, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 35
    const/16 v0, 0x17

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    .line 36
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "bb"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v12

    .line 37
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "cc"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 38
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "dd"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ff"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v13

    const/4 v7, 0x4

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "gg"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "jj"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 42
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kk"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 43
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ll"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 44
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mm"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nn"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "pp"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "rr"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ccs"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 49
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ss"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 50
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zzs"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tt"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "vv"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ggy"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lly"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nny"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "tty"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ssz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "zz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 35
    sput-object v6, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 61
    new-array v6, v11, [Lorg/tartarus/snowball/Among;

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "al"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v12

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "el"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v11

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 61
    sput-object v6, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 66
    const/16 v0, 0x2c

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ba"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v12

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ra"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v10

    .line 69
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "be"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v11

    .line 70
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "re"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v13

    const/4 v6, 0x4

    .line 71
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ig"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x5

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nak"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x6

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nek"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/4 v6, 0x7

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "val"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x8

    .line 75
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "vel"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x9

    .line 76
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ul"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xa

    .line 77
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n\u00e1l"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xb

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n\u00e9l"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xc

    .line 79
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "b\u00f3l"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xd

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "r\u00f3l"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xe

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u00f3l"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0xf

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "b\u00f5l"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x10

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "r\u00f5l"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x11

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t\u00f5l"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x12

    .line 85
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00fcl"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x13

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x14

    .line 87
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "an"

    const/16 v5, 0x13

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x15

    .line 88
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ban"

    const/16 v5, 0x14

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x16

    .line 89
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "en"

    const/16 v5, 0x13

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x17

    .line 90
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ben"

    const/16 v5, 0x16

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x18

    .line 91
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "k\u00e9ppen"

    const/16 v5, 0x16

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x19

    .line 92
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "on"

    const/16 v5, 0x13

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1a

    .line 93
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00f6n"

    const/16 v5, 0x13

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x1b

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "k\u00e9pp"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1c

    .line 95
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "kor"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1d

    .line 96
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "t"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x1e

    .line 97
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "at"

    const/16 v5, 0x1d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1f

    .line 98
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "et"

    const/16 v5, 0x1d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x20

    .line 99
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "k\u00e9nt"

    const/16 v5, 0x1d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x21

    .line 100
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ank\u00e9nt"

    const/16 v5, 0x20

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x22

    .line 101
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "enk\u00e9nt"

    const/16 v5, 0x20

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x23

    .line 102
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "onk\u00e9nt"

    const/16 v5, 0x20

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x24

    .line 103
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ot"

    const/16 v5, 0x1d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x25

    .line 104
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9rt"

    const/16 v5, 0x1d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x26

    .line 105
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00f6t"

    const/16 v5, 0x1d

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v2

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x27

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "hez"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x28

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "hoz"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x29

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "h\u00f6z"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x2a

    .line 109
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "v\u00e1"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x2b

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "v\u00e9"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 66
    sput-object v9, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 113
    new-array v6, v13, [Lorg/tartarus/snowball/Among;

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v11

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v12

    .line 115
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9n"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 116
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1nk\u00e9nt"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v13

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v11

    .line 113
    sput-object v6, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 119
    const/4 v0, 0x6

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "stul"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v11

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v12

    .line 121
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "astul"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v10

    .line 122
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1stul"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    move v6, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v11

    .line 123
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "st\u00fcl"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v11

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v13

    const/4 v0, 0x4

    .line 124
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "est\u00fcl"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v13

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x5

    .line 125
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9st\u00fcl"

    const/4 v6, 0x4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    .line 119
    sput-object v9, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    .line 128
    new-array v6, v11, [Lorg/tartarus/snowball/Among;

    .line 129
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v12

    .line 130
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v11

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v10

    .line 128
    sput-object v6, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    .line 133
    const/4 v0, 0x7

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "k"

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v12

    .line 135
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ak"

    const/4 v6, 0x4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v10

    .line 136
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ek"

    const/4 v6, 0x6

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v11

    .line 137
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ok"

    const/4 v6, 0x5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v13

    const/4 v0, 0x4

    .line 138
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1k"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x5

    .line 139
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9k"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    move v6, v11

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x6

    .line 140
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00f6k"

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    move v6, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    .line 133
    sput-object v9, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    .line 143
    const/16 v0, 0xc

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9i"

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v12

    .line 145
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1\u00e9i"

    const/4 v6, 0x6

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v10

    .line 146
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9\u00e9i"

    const/4 v6, 0x5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v11

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9"

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v13

    const/4 v0, 0x4

    .line 148
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "k\u00e9"

    const/4 v6, 0x4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x5

    .line 149
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ak\u00e9"

    const/4 v5, 0x4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x6

    .line 150
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ek\u00e9"

    const/4 v5, 0x4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x7

    .line 151
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ok\u00e9"

    const/4 v5, 0x4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8

    .line 152
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1k\u00e9"

    const/4 v5, 0x4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9

    .line 153
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9k\u00e9"

    const/4 v5, 0x4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v11

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa

    .line 154
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00f6k\u00e9"

    const/4 v5, 0x4

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb

    .line 155
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9\u00e9"

    const/16 v6, 0x8

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    .line 143
    sput-object v9, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_9:[Lorg/tartarus/snowball/Among;

    .line 158
    const/16 v0, 0x1f

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    .line 159
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a"

    const/16 v3, 0x12

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v12

    .line 160
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ja"

    const/16 v6, 0x11

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v10

    .line 161
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "d"

    const/16 v3, 0x10

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v11

    .line 162
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ad"

    const/16 v6, 0xd

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v13

    const/4 v0, 0x4

    .line 163
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ed"

    const/16 v6, 0xd

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x5

    .line 164
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "od"

    const/16 v6, 0xd

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x6

    .line 165
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1d"

    const/16 v6, 0xe

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x7

    .line 166
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9d"

    const/16 v6, 0xf

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x8

    .line 167
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00f6d"

    const/16 v6, 0xd

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x9

    .line 168
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const/16 v3, 0x12

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xa

    .line 169
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "je"

    const/16 v5, 0x9

    const/16 v6, 0x11

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xb

    .line 170
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "nk"

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xc

    .line 171
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "unk"

    const/16 v5, 0xb

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd

    .line 172
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1nk"

    const/16 v5, 0xb

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v11

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xe

    .line 173
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9nk"

    const/16 v5, 0xb

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xf

    .line 174
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00fcnk"

    const/16 v5, 0xb

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x10

    .line 175
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uk"

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x11

    .line 176
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "juk"

    const/16 v5, 0x10

    const/4 v6, 0x7

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x12

    .line 177
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1juk"

    const/16 v5, 0x11

    const/4 v6, 0x5

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x13

    .line 178
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00fck"

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x14

    .line 179
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "j\u00fck"

    const/16 v5, 0x13

    const/4 v6, 0x7

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x15

    .line 180
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9j\u00fck"

    const/16 v5, 0x14

    const/4 v6, 0x6

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x16

    .line 181
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "m"

    const/16 v3, 0xc

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x17

    .line 182
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "am"

    const/16 v5, 0x16

    const/16 v6, 0x9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x18

    .line 183
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "em"

    const/16 v5, 0x16

    const/16 v6, 0x9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x19

    .line 184
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "om"

    const/16 v5, 0x16

    const/16 v6, 0x9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1a

    .line 185
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1m"

    const/16 v5, 0x16

    const/16 v6, 0xa

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1b

    .line 186
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9m"

    const/16 v5, 0x16

    const/16 v6, 0xb

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x1c

    .line 187
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "o"

    const/16 v3, 0x12

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1d

    .line 188
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1"

    const/16 v3, 0x13

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x1e

    .line 189
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9"

    const/16 v3, 0x14

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 158
    sput-object v9, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_10:[Lorg/tartarus/snowball/Among;

    .line 192
    const/16 v0, 0x2a

    new-array v9, v0, [Lorg/tartarus/snowball/Among;

    .line 193
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "id"

    const/16 v3, 0xa

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v12

    .line 194
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aid"

    const/16 v6, 0x9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v10

    .line 195
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "jaid"

    const/4 v6, 0x6

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v11

    .line 196
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eid"

    const/16 v6, 0x9

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v13

    const/4 v0, 0x4

    .line 197
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "jeid"

    const/4 v6, 0x6

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v13

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x5

    .line 198
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1id"

    const/4 v6, 0x7

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v0, 0x6

    .line 199
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9id"

    const/16 v6, 0x8

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/4 v6, 0x7

    .line 200
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i"

    const/16 v3, 0xf

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x8

    .line 201
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ai"

    const/4 v5, 0x7

    const/16 v6, 0xe

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x9

    .line 202
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "jai"

    const/16 v5, 0x8

    const/16 v6, 0xb

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xa

    .line 203
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "ei"

    const/4 v5, 0x7

    const/16 v6, 0xe

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xb

    .line 204
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "jei"

    const/16 v5, 0xa

    const/16 v6, 0xb

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xc

    .line 205
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1i"

    const/4 v5, 0x7

    const/16 v6, 0xc

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0xd

    .line 206
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9i"

    const/4 v5, 0x7

    const/16 v6, 0xd

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0xe

    .line 207
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "itek"

    const/16 v3, 0x18

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0xf

    .line 208
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eitek"

    const/16 v5, 0xe

    const/16 v6, 0x15

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x10

    .line 209
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "jeitek"

    const/16 v5, 0xf

    const/16 v6, 0x14

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x11

    .line 210
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9itek"

    const/16 v5, 0xe

    const/16 v6, 0x17

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x12

    .line 211
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ik"

    const/16 v3, 0x1d

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x13

    .line 212
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aik"

    const/16 v5, 0x12

    const/16 v6, 0x1a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x14

    .line 213
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "jaik"

    const/16 v5, 0x13

    const/16 v6, 0x19

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x15

    .line 214
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eik"

    const/16 v5, 0x12

    const/16 v6, 0x1a

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x16

    .line 215
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "jeik"

    const/16 v5, 0x15

    const/16 v6, 0x19

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x17

    .line 216
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1ik"

    const/16 v5, 0x12

    const/16 v6, 0x1b

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x18

    .line 217
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9ik"

    const/16 v5, 0x12

    const/16 v6, 0x1c

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x19

    .line 218
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ink"

    const/16 v3, 0x14

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x1a

    .line 219
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "aink"

    const/16 v5, 0x19

    const/16 v6, 0x11

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1b

    .line 220
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "jaink"

    const/16 v5, 0x1a

    const/16 v6, 0x10

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1c

    .line 221
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "eink"

    const/16 v5, 0x19

    const/16 v6, 0x11

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1d

    .line 222
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "jeink"

    const/16 v5, 0x1c

    const/16 v6, 0x10

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1e

    .line 223
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e1ink"

    const/16 v5, 0x19

    const/16 v6, 0x12

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v0, 0x1f

    .line 224
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string/jumbo v4, "\u00e9ink"

    const/16 v5, 0x19

    const/16 v6, 0x13

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x20

    .line 225
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aitok"

    const/16 v3, 0x15

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v0, 0x21

    .line 226
    new-instance v3, Lorg/tartarus/snowball/Among;

    const-string v4, "jaitok"

    const/16 v5, 0x20

    const/16 v6, 0x14

    const-string v7, ""

    sget-object v8, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v3 .. v8}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v3, v9, v0

    const/16 v6, 0x22

    .line 227
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1itok"

    const/16 v3, 0x16

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x23

    .line 228
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "im"

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x24

    .line 229
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aim"

    const/16 v2, 0x23

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x25

    .line 230
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "jaim"

    const/16 v2, 0x24

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x26

    .line 231
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eim"

    const/16 v2, 0x23

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x27

    .line 232
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "jeim"

    const/16 v2, 0x26

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x28

    .line 233
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1im"

    const/16 v2, 0x23

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v11

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    const/16 v6, 0x29

    .line 234
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9im"

    const/16 v2, 0x23

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/HungarianStemmer;->methodObject:Lorg/tartarus/snowball/ext/HungarianStemmer;

    move v3, v13

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v9, v6

    .line 192
    sput-object v9, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_11:[Lorg/tartarus/snowball/Among;

    .line 237
    const/16 v0, 0x14

    new-array v0, v0, [C

    const/16 v1, 0x11

    aput-char v1, v0, v12

    const/16 v1, 0x41

    aput-char v1, v0, v10

    const/16 v1, 0x10

    aput-char v1, v0, v11

    const/16 v1, 0x10

    aput-char v10, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x34

    aput-char v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0xe

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/HungarianStemmer;->g_v:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/HungarianStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/HungarianStemmer;

    .prologue
    .line 242
    iget v0, p1, Lorg/tartarus/snowball/ext/HungarianStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->I_p1:I

    .line 243
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 244
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 332
    iget v0, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 334
    const/4 v0, 0x0

    .line 336
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_case()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 465
    iget v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->ket:I

    .line 467
    sget-object v1, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    const/16 v2, 0x2c

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 485
    :cond_0
    :goto_0
    return v0

    .line 472
    :cond_1
    iget v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->bra:I

    .line 474
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_R1()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 479
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    .line 481
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_v_ending()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 485
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_case_other()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 532
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->ket:I

    .line 534
    sget-object v2, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x6

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 535
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 570
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 540
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->bra:I

    .line 542
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 546
    packed-switch v0, :pswitch_data_0

    .line 570
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 552
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 557
    :pswitch_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 562
    :pswitch_3
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 567
    :pswitch_4
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 546
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private r_case_special()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 492
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->ket:I

    .line 494
    sget-object v2, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 495
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 525
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 500
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->bra:I

    .line 502
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 506
    packed-switch v0, :pswitch_data_0

    .line 525
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 512
    :pswitch_1
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 517
    :pswitch_2
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 522
    :pswitch_3
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 506
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_double()Z
    .locals 3

    .prologue
    .line 378
    iget v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    sub-int v0, v1, v2

    .line 380
    .local v0, "v_1":I
    sget-object v1, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v2, 0x17

    invoke-virtual {p0, v1, v2}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v1

    if-nez v1, :cond_0

    .line 382
    const/4 v1, 0x0

    .line 385
    :goto_0
    return v1

    .line 384
    :cond_0
    iget v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    sub-int/2addr v1, v0

    iput v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 385
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private r_factive()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 577
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->ket:I

    .line 579
    sget-object v2, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 580
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 618
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 585
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->bra:I

    .line 587
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 591
    packed-switch v0, :pswitch_data_0

    .line 612
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    .line 614
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_undouble()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 618
    const/4 v1, 0x1

    goto :goto_0

    .line 597
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_double()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 605
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_double()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 591
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_instrum()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 418
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->ket:I

    .line 420
    sget-object v2, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 421
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 459
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 426
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->bra:I

    .line 428
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 432
    packed-switch v0, :pswitch_data_0

    .line 453
    :cond_2
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    .line 455
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_undouble()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 459
    const/4 v1, 0x1

    goto :goto_0

    .line 438
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_double()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 446
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_double()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 432
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/16 v7, 0xfc

    const/16 v6, 0x61

    .line 251
    iget v4, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iput v4, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->I_p1:I

    .line 254
    iget v0, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 257
    .local v0, "v_1":I
    sget-object v4, Lorg/tartarus/snowball/ext/HungarianStemmer;->g_v:[C

    invoke-virtual {p0, v4, v6, v7}, Lorg/tartarus/snowball/ext/HungarianStemmer;->in_grouping([CII)Z

    move-result v4

    if-nez v4, :cond_2

    .line 303
    :cond_0
    :goto_0
    iput v0, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 305
    sget-object v4, Lorg/tartarus/snowball/ext/HungarianStemmer;->g_v:[C

    invoke-virtual {p0, v4, v6, v7}, Lorg/tartarus/snowball/ext/HungarianStemmer;->out_grouping([CII)Z

    move-result v4

    if-nez v4, :cond_6

    .line 328
    :goto_1
    return v3

    .line 278
    .local v1, "v_2":I
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 264
    .end local v1    # "v_2":I
    :cond_2
    iget v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 266
    .restart local v1    # "v_2":I
    sget-object v4, Lorg/tartarus/snowball/ext/HungarianStemmer;->g_v:[C

    invoke-virtual {p0, v4, v6, v7}, Lorg/tartarus/snowball/ext/HungarianStemmer;->out_grouping([CII)Z

    move-result v4

    if-nez v4, :cond_3

    .line 273
    iput v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 274
    iget v4, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    if-lt v4, v5, :cond_1

    goto :goto_0

    .line 270
    :cond_3
    iput v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 282
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 285
    .local v2, "v_3":I
    sget-object v4, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x8

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v4

    if-nez v4, :cond_4

    .line 291
    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 293
    iget v4, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    if-ge v4, v5, :cond_0

    .line 297
    iget v3, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 300
    :cond_4
    iget v3, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->I_p1:I

    .line 328
    .end local v1    # "v_2":I
    .end local v2    # "v_3":I
    :goto_2
    const/4 v3, 0x1

    goto :goto_1

    .line 323
    :cond_5
    iget v4, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 313
    :cond_6
    sget-object v4, Lorg/tartarus/snowball/ext/HungarianStemmer;->g_v:[C

    invoke-virtual {p0, v4, v6, v7}, Lorg/tartarus/snowball/ext/HungarianStemmer;->in_grouping([CII)Z

    move-result v4

    if-nez v4, :cond_7

    .line 319
    iget v4, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    if-lt v4, v5, :cond_5

    goto :goto_1

    .line 326
    :cond_7
    iget v3, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->I_p1:I

    goto :goto_2
.end method

.method private r_owned()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 685
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->ket:I

    .line 687
    sget-object v2, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_9:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0xc

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 688
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 748
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 693
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->bra:I

    .line 695
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 699
    packed-switch v0, :pswitch_data_0

    .line 748
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 705
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 710
    :pswitch_2
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 715
    :pswitch_3
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 720
    :pswitch_4
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 725
    :pswitch_5
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 730
    :pswitch_6
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 735
    :pswitch_7
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 740
    :pswitch_8
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 745
    :pswitch_9
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 699
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private r_plur_owner()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 880
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->ket:I

    .line 882
    sget-object v2, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_11:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x2a

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 883
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 1043
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 888
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->bra:I

    .line 890
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 894
    packed-switch v0, :pswitch_data_0

    .line 1043
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 900
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 905
    :pswitch_2
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 910
    :pswitch_3
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 915
    :pswitch_4
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 920
    :pswitch_5
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 925
    :pswitch_6
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 930
    :pswitch_7
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 935
    :pswitch_8
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 940
    :pswitch_9
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 945
    :pswitch_a
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 950
    :pswitch_b
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 955
    :pswitch_c
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 960
    :pswitch_d
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 965
    :pswitch_e
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 970
    :pswitch_f
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 975
    :pswitch_10
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 980
    :pswitch_11
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 985
    :pswitch_12
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 990
    :pswitch_13
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 995
    :pswitch_14
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 1000
    :pswitch_15
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 1005
    :pswitch_16
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1010
    :pswitch_17
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1015
    :pswitch_18
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 1020
    :pswitch_19
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 1025
    :pswitch_1a
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 1030
    :pswitch_1b
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1035
    :pswitch_1c
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1040
    :pswitch_1d
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto/16 :goto_1

    .line 894
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method private r_plural()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 625
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->ket:I

    .line 627
    sget-object v2, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x7

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 628
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 678
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 633
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->bra:I

    .line 635
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 639
    packed-switch v0, :pswitch_data_0

    .line 678
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 645
    :pswitch_1
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 650
    :pswitch_2
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 655
    :pswitch_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 660
    :pswitch_4
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 665
    :pswitch_5
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 670
    :pswitch_6
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 675
    :pswitch_7
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 639
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private r_sing_owner()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 755
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->ket:I

    .line 757
    sget-object v2, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_10:[Lorg/tartarus/snowball/Among;

    const/16 v3, 0x1f

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 758
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 873
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 763
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->bra:I

    .line 765
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 769
    packed-switch v0, :pswitch_data_0

    .line 873
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 775
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 780
    :pswitch_2
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 785
    :pswitch_3
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 790
    :pswitch_4
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 795
    :pswitch_5
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 800
    :pswitch_6
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 805
    :pswitch_7
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 810
    :pswitch_8
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 815
    :pswitch_9
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 820
    :pswitch_a
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 825
    :pswitch_b
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 830
    :pswitch_c
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 835
    :pswitch_d
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 840
    :pswitch_e
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 845
    :pswitch_f
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 850
    :pswitch_10
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 855
    :pswitch_11
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 860
    :pswitch_12
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    goto :goto_1

    .line 865
    :pswitch_13
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 870
    :pswitch_14
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 769
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method private r_undouble()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 391
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit_backward:I

    if-gt v2, v3, :cond_1

    .line 411
    :cond_0
    :goto_0
    return v1

    .line 395
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 397
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->ket:I

    .line 400
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    add-int/lit8 v0, v2, -0x1

    .line 401
    .local v0, "c":I
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit_backward:I

    if-gt v2, v0, :cond_0

    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    if-gt v0, v2, :cond_0

    .line 405
    iput v0, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 408
    iget v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v1, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->bra:I

    .line 410
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_del()V

    .line 411
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private r_v_ending()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 343
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->ket:I

    .line 345
    sget-object v2, Lorg/tartarus/snowball/ext/HungarianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/HungarianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 346
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 371
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 351
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->bra:I

    .line 353
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_R1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 357
    packed-switch v0, :pswitch_data_0

    .line 371
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 363
    :pswitch_1
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 368
    :pswitch_2
    const-string v1, "e"

    invoke-virtual {p0, v1}, Lorg/tartarus/snowball/ext/HungarianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 357
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1167
    instance-of v0, p1, Lorg/tartarus/snowball/ext/HungarianStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1172
    const-class v0, Lorg/tartarus/snowball/ext/HungarianStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 12

    .prologue
    .line 1060
    iget v0, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1063
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_mark_regions()Z

    move-result v10

    if-nez v10, :cond_0

    .line 1068
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1070
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit_backward:I

    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1073
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    sub-int v2, v10, v11

    .line 1076
    .local v2, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_instrum()Z

    move-result v10

    if-nez v10, :cond_1

    .line 1081
    :cond_1
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    sub-int/2addr v10, v2

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1083
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    sub-int v3, v10, v11

    .line 1086
    .local v3, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_case()Z

    move-result v10

    if-nez v10, :cond_2

    .line 1091
    :cond_2
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    sub-int/2addr v10, v3

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1093
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    sub-int v4, v10, v11

    .line 1096
    .local v4, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_case_special()Z

    move-result v10

    if-nez v10, :cond_3

    .line 1101
    :cond_3
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    sub-int/2addr v10, v4

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1103
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    sub-int v5, v10, v11

    .line 1106
    .local v5, "v_5":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_case_other()Z

    move-result v10

    if-nez v10, :cond_4

    .line 1111
    :cond_4
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    sub-int/2addr v10, v5

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1113
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    sub-int v6, v10, v11

    .line 1116
    .local v6, "v_6":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_factive()Z

    move-result v10

    if-nez v10, :cond_5

    .line 1121
    :cond_5
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    sub-int/2addr v10, v6

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1123
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    sub-int v7, v10, v11

    .line 1126
    .local v7, "v_7":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_owned()Z

    move-result v10

    if-nez v10, :cond_6

    .line 1131
    :cond_6
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    sub-int/2addr v10, v7

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1133
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    sub-int v8, v10, v11

    .line 1136
    .local v8, "v_8":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_sing_owner()Z

    move-result v10

    if-nez v10, :cond_7

    .line 1141
    :cond_7
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    sub-int/2addr v10, v8

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1143
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    sub-int v9, v10, v11

    .line 1146
    .local v9, "v_9":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_plur_owner()Z

    move-result v10

    if-nez v10, :cond_8

    .line 1151
    :cond_8
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    sub-int/2addr v10, v9

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1153
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    sub-int v1, v10, v11

    .line 1156
    .local v1, "v_10":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/HungarianStemmer;->r_plural()Z

    move-result v10

    if-nez v10, :cond_9

    .line 1161
    :cond_9
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit:I

    sub-int/2addr v10, v1

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    .line 1162
    iget v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->limit_backward:I

    iput v10, p0, Lorg/tartarus/snowball/ext/HungarianStemmer;->cursor:I

    const/4 v10, 0x1

    return v10
.end method
