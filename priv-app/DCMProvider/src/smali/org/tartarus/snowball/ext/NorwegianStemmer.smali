.class public Lorg/tartarus/snowball/ext/NorwegianStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "NorwegianStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final g_s_ending:[C

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_x:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/16 v13, 0x10

    const/16 v12, 0xe

    const/4 v11, 0x2

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/NorwegianStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    .line 19
    const/16 v0, 0x1d

    new-array v10, v0, [Lorg/tartarus/snowball/Among;

    const/4 v6, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v3

    .line 22
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ede"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v3

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v11

    const/4 v0, 0x3

    .line 23
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ande"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v3

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x4

    .line 24
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ende"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v3

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x5

    .line 25
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ane"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v3

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x6

    .line 26
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ene"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v3

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x7

    .line 27
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "hetene"

    const/4 v6, 0x6

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x8

    .line 28
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "erte"

    const/4 v7, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x9

    .line 29
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "en"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xa

    .line 30
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "heten"

    const/16 v6, 0x9

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0xb

    .line 31
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xc

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0xd

    .line 33
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "heter"

    const/16 v6, 0xc

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 34
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "s"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v2

    move v7, v11

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v12

    const/16 v0, 0xf

    .line 35
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "as"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    .line 36
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "es"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v13

    const/16 v0, 0x11

    .line 37
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "edes"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x12

    .line 38
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "endes"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x13

    .line 39
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "enes"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x14

    .line 40
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "hetenes"

    const/16 v6, 0x13

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x15

    .line 41
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ens"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x16

    .line 42
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "hetens"

    const/16 v6, 0x15

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x17

    .line 43
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ers"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x18

    .line 44
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ets"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v12

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x19

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "et"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v0, 0x1a

    .line 46
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "het"

    const/16 v6, 0x19

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v0, 0x1b

    .line 47
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "ert"

    const/4 v7, 0x3

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/16 v6, 0x1c

    .line 48
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ast"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 19
    sput-object v10, Lorg/tartarus/snowball/ext/NorwegianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 51
    new-array v0, v11, [Lorg/tartarus/snowball/Among;

    const/4 v1, 0x0

    .line 52
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "dt"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v2

    move v7, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v1

    .line 53
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string/jumbo v5, "vt"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v2

    move v7, v2

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v0, v3

    .line 51
    sput-object v0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 56
    const/16 v0, 0xb

    new-array v10, v0, [Lorg/tartarus/snowball/Among;

    const/4 v6, 0x0

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "leg"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 58
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eleg"

    const/4 v6, 0x0

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v3

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ig"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v11

    const/4 v0, 0x3

    .line 60
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "eig"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x4

    .line 61
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "lig"

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v0, 0x5

    .line 62
    new-instance v4, Lorg/tartarus/snowball/Among;

    const-string v5, "elig"

    const/4 v6, 0x4

    const-string v8, ""

    sget-object v9, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v4, v10, v0

    const/4 v6, 0x6

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "els"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/4 v6, 0x7

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "lov"

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x8

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "elov"

    const/4 v2, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0x9

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "slov"

    const/4 v2, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    const/16 v6, 0xa

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "hetslov"

    const/16 v2, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/NorwegianStemmer;->methodObject:Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v10, v6

    .line 56
    sput-object v10, Lorg/tartarus/snowball/ext/NorwegianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 70
    const/16 v0, 0x13

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/16 v1, 0x41

    aput-char v1, v0, v3

    aput-char v13, v0, v11

    const/4 v1, 0x3

    aput-char v3, v0, v1

    const/16 v1, 0x30

    aput-char v1, v0, v13

    const/16 v1, 0x12

    const/16 v2, 0x80

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->g_v:[C

    .line 72
    const/4 v0, 0x4

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->g_s_ending:[C

    return-void

    :array_0
    .array-data 2
        0x77s
        0x7ds
        0x95s
        0x1s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/NorwegianStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/NorwegianStemmer;

    .prologue
    .line 78
    iget v0, p1, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_x:I

    iput v0, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_x:I

    .line 79
    iget v0, p1, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    .line 80
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 81
    return-void
.end method

.method private r_consonant_pair()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 232
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    sub-int v0, v4, v5

    .line 235
    .local v0, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 237
    .local v1, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    if-ge v4, v5, :cond_1

    .line 268
    :cond_0
    :goto_0
    return v3

    .line 241
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 242
    iget v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    .line 243
    .local v2, "v_3":I
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    .line 244
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 247
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->ket:I

    .line 249
    sget-object v4, Lorg/tartarus/snowball/ext/NorwegianStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v5, 0x2

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v4

    if-nez v4, :cond_2

    .line 251
    iput v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    goto :goto_0

    .line 255
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->bra:I

    .line 256
    iput v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    .line 257
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    sub-int/2addr v4, v0

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 259
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    if-le v4, v5, :cond_0

    .line 263
    iget v3, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 265
    iget v3, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->bra:I

    .line 267
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->slice_del()V

    .line 268
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private r_main_suffix()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 159
    iget v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 161
    .local v1, "v_1":I
    iget v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iget v7, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    if-ge v6, v7, :cond_1

    .line 223
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 165
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    iput v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 166
    iget v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    .line 167
    .local v2, "v_2":I
    iget v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    .line 168
    iget v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 171
    iget v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->ket:I

    .line 173
    sget-object v6, Lorg/tartarus/snowball/ext/NorwegianStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/16 v7, 0x1d

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 174
    .local v0, "among_var":I
    if-nez v0, :cond_2

    .line 176
    iput v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    goto :goto_0

    .line 180
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->bra:I

    .line 181
    iput v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    .line 182
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v4, v5

    .line 223
    goto :goto_0

    .line 188
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->slice_del()V

    goto :goto_1

    .line 194
    :pswitch_2
    iget v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 196
    .local v3, "v_3":I
    sget-object v6, Lorg/tartarus/snowball/ext/NorwegianStemmer;->g_s_ending:[C

    const/16 v7, 0x62

    const/16 v8, 0x7a

    invoke-virtual {p0, v6, v7, v8}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->in_grouping_b([CII)Z

    move-result v6

    if-nez v6, :cond_3

    .line 202
    iget v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 205
    const-string v6, "k"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 209
    sget-object v6, Lorg/tartarus/snowball/ext/NorwegianStemmer;->g_v:[C

    const/16 v7, 0x61

    const/16 v8, 0xf8

    invoke-virtual {p0, v6, v7, v8}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->out_grouping_b([CII)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 215
    :cond_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->slice_del()V

    goto :goto_1

    .line 220
    .end local v3    # "v_3":I
    :pswitch_3
    const-string v4, "er"

    invoke-virtual {p0, v4}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_mark_regions()Z
    .locals 8

    .prologue
    const/16 v7, 0xf8

    const/16 v6, 0x61

    const/4 v3, 0x0

    .line 87
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    .line 89
    iget v1, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 93
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    add-int/lit8 v0, v4, 0x3

    .line 94
    .local v0, "c":I
    if-ltz v0, :cond_0

    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    if-le v0, v4, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v3

    .line 98
    :cond_1
    iput v0, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 101
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_x:I

    .line 102
    iput v1, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 106
    :goto_1
    iget v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 108
    .local v2, "v_2":I
    sget-object v4, Lorg/tartarus/snowball/ext/NorwegianStemmer;->g_v:[C

    invoke-virtual {p0, v4, v6, v7}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->in_grouping([CII)Z

    move-result v4

    if-nez v4, :cond_2

    .line 115
    iput v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 116
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    if-ge v4, v5, :cond_0

    .line 120
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    goto :goto_1

    .line 112
    :cond_2
    iput v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 126
    :goto_2
    sget-object v4, Lorg/tartarus/snowball/ext/NorwegianStemmer;->g_v:[C

    invoke-virtual {p0, v4, v6, v7}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->out_grouping([CII)Z

    move-result v4

    if-nez v4, :cond_3

    .line 132
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    if-ge v4, v5, :cond_0

    .line 136
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    goto :goto_2

    .line 139
    :cond_3
    iget v3, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v3, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    .line 143
    iget v3, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_x:I

    if-lt v3, v4, :cond_4

    .line 149
    :goto_3
    const/4 v3, 0x1

    goto :goto_0

    .line 147
    :cond_4
    iget v3, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_x:I

    iput v3, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    goto :goto_3
.end method

.method private r_other_suffix()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 277
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 279
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    if-ge v4, v5, :cond_0

    .line 309
    :goto_0
    :pswitch_0
    return v3

    .line 283
    :cond_0
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->I_p1:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 284
    iget v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    .line 285
    .local v2, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    .line 286
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 289
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->ket:I

    .line 291
    sget-object v4, Lorg/tartarus/snowball/ext/NorwegianStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0xb

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 292
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 294
    iput v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    goto :goto_0

    .line 298
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->bra:I

    .line 299
    iput v2, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    .line 300
    packed-switch v0, :pswitch_data_0

    .line 309
    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    .line 306
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->slice_del()V

    goto :goto_1

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 367
    instance-of v0, p1, Lorg/tartarus/snowball/ext/NorwegianStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 372
    const-class v0, Lorg/tartarus/snowball/ext/NorwegianStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 6

    .prologue
    .line 320
    iget v0, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 323
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->r_mark_regions()Z

    move-result v4

    if-nez v4, :cond_0

    .line 328
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 330
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 333
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 336
    .local v1, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->r_main_suffix()Z

    move-result v4

    if-nez v4, :cond_1

    .line 341
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 343
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    sub-int v2, v4, v5

    .line 346
    .local v2, "v_3":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->r_consonant_pair()Z

    move-result v4

    if-nez v4, :cond_2

    .line 351
    :cond_2
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    sub-int/2addr v4, v2

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 353
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    sub-int v3, v4, v5

    .line 356
    .local v3, "v_4":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/NorwegianStemmer;->r_other_suffix()Z

    move-result v4

    if-nez v4, :cond_3

    .line 361
    :cond_3
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    .line 362
    iget v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->limit_backward:I

    iput v4, p0, Lorg/tartarus/snowball/ext/NorwegianStemmer;->cursor:I

    const/4 v4, 0x1

    return v4
.end method
