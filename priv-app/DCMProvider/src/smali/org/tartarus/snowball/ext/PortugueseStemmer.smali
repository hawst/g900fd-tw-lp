.class public Lorg/tartarus/snowball/ext/PortugueseStemmer;
.super Lorg/tartarus/snowball/SnowballProgram;
.source "PortugueseStemmer.java"


# static fields
.field private static final a_0:[Lorg/tartarus/snowball/Among;

.field private static final a_1:[Lorg/tartarus/snowball/Among;

.field private static final a_2:[Lorg/tartarus/snowball/Among;

.field private static final a_3:[Lorg/tartarus/snowball/Among;

.field private static final a_4:[Lorg/tartarus/snowball/Among;

.field private static final a_5:[Lorg/tartarus/snowball/Among;

.field private static final a_6:[Lorg/tartarus/snowball/Among;

.field private static final a_7:[Lorg/tartarus/snowball/Among;

.field private static final a_8:[Lorg/tartarus/snowball/Among;

.field private static final g_v:[C

.field private static final methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private I_p1:I

.field private I_p2:I

.field private I_pV:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 17
    new-instance v0, Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct {v0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;-><init>()V

    sput-object v0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    .line 19
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 20
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 21
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e3"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 22
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f5"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 19
    sput-object v6, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    .line 25
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 26
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 27
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a~"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 28
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "o~"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 25
    sput-object v6, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    .line 31
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 32
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 33
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ad"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 34
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "os"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 35
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iv"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 31
    sput-object v6, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    .line 38
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 39
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ante"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 40
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "avel"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 41
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edvel"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 38
    sput-object v6, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    .line 44
    const/4 v0, 0x3

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 45
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ic"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 46
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "abil"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 47
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iv"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 44
    sput-object v6, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    .line 50
    const/16 v0, 0x2d

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 51
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ica"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 52
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e2ncia"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 53
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eancia"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 54
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ira"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 55
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "adora"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 56
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osa"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 57
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ista"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 58
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iva"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 59
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eza"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 60
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "log\u00eda"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 61
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "idade"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 62
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ante"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 63
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "mente"

    const/4 v2, -0x1

    const/4 v3, 0x6

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 64
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amente"

    const/16 v2, 0xc

    const/4 v3, 0x5

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 65
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1vel"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 66
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edvel"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 67
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uci\u00f3n"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 68
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ico"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 69
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ismo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 70
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "oso"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 71
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amento"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 72
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imento"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 73
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivo"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 74
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a\u00e7a~o"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 75
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ador"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 76
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icas"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 77
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eancias"

    const/4 v2, -0x1

    const/4 v3, 0x4

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 78
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iras"

    const/4 v2, -0x1

    const/16 v3, 0x9

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 79
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "adoras"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 80
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osas"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 81
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "istas"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 82
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivas"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 83
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ezas"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 84
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "log\u00edas"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 85
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "idades"

    const/4 v2, -0x1

    const/4 v3, 0x7

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 86
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "uciones"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 87
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "adores"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 88
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "antes"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x26

    .line 89
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a\u00e7o~es"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x27

    .line 90
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "icos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x28

    .line 91
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ismos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x29

    .line 92
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "osos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2a

    .line 93
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amentos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2b

    .line 94
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imentos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2c

    .line 95
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ivos"

    const/4 v2, -0x1

    const/16 v3, 0x8

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 50
    sput-object v6, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    .line 98
    const/16 v0, 0x78

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 99
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ada"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 100
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ida"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 101
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ia"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 102
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aria"

    const/4 v2, 0x2

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 103
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eria"

    const/4 v2, 0x2

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 104
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iria"

    const/4 v2, 0x2

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 105
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ara"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    .line 106
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "era"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    .line 107
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ira"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    .line 108
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ava"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    .line 109
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asse"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    .line 110
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "esse"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    .line 111
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isse"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    .line 112
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aste"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    .line 113
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "este"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    .line 114
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iste"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    .line 115
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ei"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    .line 116
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "arei"

    const/16 v2, 0x10

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    .line 117
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erei"

    const/16 v2, 0x10

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x13

    .line 118
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irei"

    const/16 v2, 0x10

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x14

    .line 119
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "am"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x15

    .line 120
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iam"

    const/16 v2, 0x14

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x16

    .line 121
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ariam"

    const/16 v2, 0x15

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x17

    .line 122
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eriam"

    const/16 v2, 0x15

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x18

    .line 123
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iriam"

    const/16 v2, 0x15

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x19

    .line 124
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aram"

    const/16 v2, 0x14

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1a

    .line 125
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eram"

    const/16 v2, 0x14

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1b

    .line 126
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iram"

    const/16 v2, 0x14

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1c

    .line 127
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "avam"

    const/16 v2, 0x14

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1d

    .line 128
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "em"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    .line 129
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "arem"

    const/16 v2, 0x1d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1f

    .line 130
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erem"

    const/16 v2, 0x1d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x20

    .line 131
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irem"

    const/16 v2, 0x1d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x21

    .line 132
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "assem"

    const/16 v2, 0x1d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x22

    .line 133
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "essem"

    const/16 v2, 0x1d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x23

    .line 134
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "issem"

    const/16 v2, 0x1d

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x24

    .line 135
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ado"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x25

    .line 136
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ido"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x26

    .line 137
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ando"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x27

    .line 138
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "endo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x28

    .line 139
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "indo"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x29

    .line 140
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ara~o"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2a

    .line 141
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "era~o"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2b

    .line 142
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ira~o"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2c

    .line 143
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2d

    .line 144
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2e

    .line 145
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x2f

    .line 146
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "as"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x30

    .line 147
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "adas"

    const/16 v2, 0x2f

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x31

    .line 148
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "idas"

    const/16 v2, 0x2f

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x32

    .line 149
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ias"

    const/16 v2, 0x2f

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x33

    .line 150
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "arias"

    const/16 v2, 0x32

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x34

    .line 151
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erias"

    const/16 v2, 0x32

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x35

    .line 152
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irias"

    const/16 v2, 0x32

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x36

    .line 153
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aras"

    const/16 v2, 0x2f

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x37

    .line 154
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eras"

    const/16 v2, 0x2f

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x38

    .line 155
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iras"

    const/16 v2, 0x2f

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x39

    .line 156
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "avas"

    const/16 v2, 0x2f

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3a

    .line 157
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "es"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3b

    .line 158
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ardes"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3c

    .line 159
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "erdes"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3d

    .line 160
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irdes"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3e

    .line 161
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ares"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x3f

    .line 162
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eres"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x40

    .line 163
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ires"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x41

    .line 164
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "asses"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x42

    .line 165
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "esses"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x43

    .line 166
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "isses"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x44

    .line 167
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "astes"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x45

    .line 168
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "estes"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x46

    .line 169
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "istes"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x47

    .line 170
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "is"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x48

    .line 171
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ais"

    const/16 v2, 0x47

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x49

    .line 172
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eis"

    const/16 v2, 0x47

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4a

    .line 173
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "areis"

    const/16 v2, 0x49

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4b

    .line 174
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ereis"

    const/16 v2, 0x49

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4c

    .line 175
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ireis"

    const/16 v2, 0x49

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4d

    .line 176
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1reis"

    const/16 v2, 0x49

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4e

    .line 177
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9reis"

    const/16 v2, 0x49

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x4f

    .line 178
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edreis"

    const/16 v2, 0x49

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x50

    .line 179
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1sseis"

    const/16 v2, 0x49

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x51

    .line 180
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9sseis"

    const/16 v2, 0x49

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x52

    .line 181
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edsseis"

    const/16 v2, 0x49

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x53

    .line 182
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1veis"

    const/16 v2, 0x49

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x54

    .line 183
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edeis"

    const/16 v2, 0x49

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x55

    .line 184
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00edeis"

    const/16 v2, 0x54

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x56

    .line 185
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00edeis"

    const/16 v2, 0x54

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x57

    .line 186
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00edeis"

    const/16 v2, 0x54

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x58

    .line 187
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ados"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x59

    .line 188
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "idos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5a

    .line 189
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "amos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5b

    .line 190
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1ramos"

    const/16 v2, 0x5a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5c

    .line 191
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9ramos"

    const/16 v2, 0x5a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5d

    .line 192
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edramos"

    const/16 v2, 0x5a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5e

    .line 193
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1vamos"

    const/16 v2, 0x5a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x5f

    .line 194
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edamos"

    const/16 v2, 0x5a

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x60

    .line 195
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00edamos"

    const/16 v2, 0x5f

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x61

    .line 196
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00edamos"

    const/16 v2, 0x5f

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x62

    .line 197
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00edamos"

    const/16 v2, 0x5f

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x63

    .line 198
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "emos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x64

    .line 199
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "aremos"

    const/16 v2, 0x63

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x65

    .line 200
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eremos"

    const/16 v2, 0x63

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x66

    .line 201
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iremos"

    const/16 v2, 0x63

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x67

    .line 202
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1ssemos"

    const/16 v2, 0x63

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x68

    .line 203
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00eassemos"

    const/16 v2, 0x63

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x69

    .line 204
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00edssemos"

    const/16 v2, 0x63

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x6a

    .line 205
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "imos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x6b

    .line 206
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "armos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x6c

    .line 207
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ermos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x6d

    .line 208
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "irmos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x6e

    .line 209
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1mos"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x6f

    .line 210
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e1s"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x70

    .line 211
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e1s"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x71

    .line 212
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e1s"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x72

    .line 213
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "eu"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x73

    .line 214
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "iu"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x74

    .line 215
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ou"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x75

    .line 216
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ar\u00e1"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x76

    .line 217
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "er\u00e1"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/16 v7, 0x77

    .line 218
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "ir\u00e1"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 98
    sput-object v6, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    .line 221
    const/4 v0, 0x7

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 222
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "a"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 223
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "i"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 224
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "o"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 225
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "os"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 226
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e1"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    .line 227
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ed"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    .line 228
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00f3"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 221
    sput-object v6, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    .line 231
    const/4 v0, 0x4

    new-array v6, v0, [Lorg/tartarus/snowball/Among;

    const/4 v7, 0x0

    .line 232
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string v1, "e"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    .line 233
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e7"

    const/4 v2, -0x1

    const/4 v3, 0x2

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    .line 234
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00e9"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    .line 235
    new-instance v0, Lorg/tartarus/snowball/Among;

    const-string/jumbo v1, "\u00ea"

    const/4 v2, -0x1

    const/4 v3, 0x1

    const-string v4, ""

    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->methodObject:Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-direct/range {v0 .. v5}, Lorg/tartarus/snowball/Among;-><init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V

    aput-object v0, v6, v7

    .line 231
    sput-object v6, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    .line 238
    const/16 v0, 0x14

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x11

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x10

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/4 v2, 0x3

    aput-char v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x13

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0xc

    aput-char v2, v0, v1

    const/16 v1, 0x13

    const/4 v2, 0x2

    aput-char v2, v0, v1

    sput-object v0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/tartarus/snowball/SnowballProgram;-><init>()V

    return-void
.end method

.method private copy_from(Lorg/tartarus/snowball/ext/PortugueseStemmer;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/ext/PortugueseStemmer;

    .prologue
    .line 245
    iget v0, p1, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_p2:I

    iput v0, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_p2:I

    .line 246
    iget v0, p1, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_p1:I

    iput v0, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_p1:I

    .line 247
    iget v0, p1, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_pV:I

    iput v0, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_pV:I

    .line 248
    invoke-super {p0, p1}, Lorg/tartarus/snowball/SnowballProgram;->copy_from(Lorg/tartarus/snowball/SnowballProgram;)V

    .line 249
    return-void
.end method

.method private r_R1()Z
    .locals 2

    .prologue
    .line 563
    iget v0, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_p1:I

    iget v1, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 565
    const/4 v0, 0x0

    .line 567
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_R2()Z
    .locals 2

    .prologue
    .line 571
    iget v0, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_p2:I

    iget v1, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 573
    const/4 v0, 0x0

    .line 575
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_RV()Z
    .locals 2

    .prologue
    .line 555
    iget v0, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_pV:I

    iget v1, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    if-le v0, v1, :cond_0

    .line 557
    const/4 v0, 0x0

    .line 559
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private r_mark_regions()Z
    .locals 9

    .prologue
    const/16 v8, 0xfa

    const/16 v7, 0x61

    .line 308
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_pV:I

    .line 309
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_p1:I

    .line 310
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_p2:I

    .line 312
    iget v0, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 317
    .local v0, "v_1":I
    iget v1, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 320
    .local v1, "v_2":I
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_3

    .line 376
    :cond_0
    iput v1, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 378
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_6

    .line 426
    :cond_1
    :goto_0
    iput v0, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 428
    iget v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 435
    .local v4, "v_8":I
    :goto_1
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_c

    .line 441
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    if-lt v5, v6, :cond_a

    .line 500
    :cond_2
    :goto_2
    iput v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 501
    const/4 v5, 0x1

    return v5

    .line 326
    .end local v4    # "v_8":I
    :cond_3
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 329
    .local v2, "v_3":I
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_5

    .line 351
    :goto_3
    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 353
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->in_grouping([CII)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 361
    :goto_4
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 367
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    if-ge v5, v6, :cond_0

    .line 371
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto :goto_4

    .line 347
    :cond_4
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 337
    :cond_5
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 343
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    if-lt v5, v6, :cond_4

    goto :goto_3

    .line 384
    .end local v2    # "v_3":I
    :cond_6
    iget v3, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 387
    .local v3, "v_6":I
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_9

    .line 409
    :goto_5
    iput v3, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 411
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->in_grouping([CII)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 416
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    if-ge v5, v6, :cond_1

    .line 420
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 424
    .end local v3    # "v_6":I
    :cond_7
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_pV:I

    goto :goto_0

    .line 405
    .restart local v3    # "v_6":I
    :cond_8
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 395
    :cond_9
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_7

    .line 401
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    if-lt v5, v6, :cond_8

    goto :goto_5

    .line 445
    .end local v3    # "v_6":I
    .restart local v4    # "v_8":I
    :cond_a
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto/16 :goto_1

    .line 461
    :cond_b
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 451
    :cond_c
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_d

    .line 457
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    if-lt v5, v6, :cond_b

    goto/16 :goto_2

    .line 464
    :cond_d
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_p1:I

    .line 469
    :goto_6
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->in_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_f

    .line 475
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    if-ge v5, v6, :cond_2

    .line 479
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto :goto_6

    .line 495
    :cond_e
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 485
    :cond_f
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->g_v:[C

    invoke-virtual {p0, v5, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->out_grouping([CII)Z

    move-result v5

    if-nez v5, :cond_10

    .line 491
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    if-lt v5, v6, :cond_e

    goto/16 :goto_2

    .line 498
    :cond_10
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_p2:I

    goto/16 :goto_2
.end method

.method private r_postlude()Z
    .locals 4

    .prologue
    .line 510
    :goto_0
    iget v1, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 514
    .local v1, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 516
    sget-object v2, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_1:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 517
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 548
    :cond_0
    :pswitch_0
    iput v1, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 551
    const/4 v2, 0x1

    return v2

    .line 522
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 523
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 529
    :pswitch_1
    const-string/jumbo v2, "\u00e3"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 534
    :pswitch_2
    const-string/jumbo v2, "\u00f5"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 539
    :pswitch_3
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 543
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto :goto_0

    .line 523
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_prelude()Z
    .locals 4

    .prologue
    .line 257
    :goto_0
    iget v1, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 261
    .local v1, "v_1":I
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 263
    sget-object v2, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_0:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->find_among([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 264
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 295
    :cond_0
    :pswitch_0
    iput v1, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 298
    const/4 v2, 0x1

    return v2

    .line 269
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 270
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 276
    :pswitch_1
    const-string v2, "a~"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 281
    :pswitch_2
    const-string v2, "o~"

    invoke-virtual {p0, v2}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 286
    :pswitch_3
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    if-ge v2, v3, :cond_0

    .line 290
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private r_residual_form()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 912
    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 914
    sget-object v6, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_8:[Lorg/tartarus/snowball/Among;

    const/4 v7, 0x4

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 915
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 989
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 920
    :cond_1
    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 921
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v4, v5

    .line 989
    goto :goto_0

    .line 927
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_RV()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 932
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    .line 934
    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 937
    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v1, v6, v7

    .line 941
    .local v1, "v_1":I
    const-string v6, "u"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 957
    :cond_2
    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v6, v1

    iput v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 960
    const-string v6, "i"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 965
    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 967
    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v3, v6, v7

    .line 969
    .local v3, "v_3":I
    const-string v6, "c"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 973
    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v6, v3

    iput v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 976
    .end local v3    # "v_3":I
    :goto_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_RV()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 981
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    goto :goto_1

    .line 946
    :cond_3
    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 948
    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v2, v6, v7

    .line 950
    .local v2, "v_2":I
    const-string v6, "g"

    invoke-virtual {p0, v5, v6}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 954
    iget v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v6, v2

    iput v6, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto :goto_2

    .line 986
    .end local v1    # "v_1":I
    .end local v2    # "v_2":I
    :pswitch_2
    const-string v4, "c"

    invoke-virtual {p0, v4}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 921
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private r_residual_suffix()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 879
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 881
    sget-object v2, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_7:[Lorg/tartarus/snowball/Among;

    const/4 v3, 0x7

    invoke-virtual {p0, v2, v3}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 882
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 902
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 887
    :cond_1
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 888
    packed-switch v0, :pswitch_data_0

    .line 902
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 894
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_RV()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 899
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    goto :goto_1

    .line 888
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private r_standard_suffix()Z
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 586
    iget v7, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 588
    sget-object v7, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_5:[Lorg/tartarus/snowball/Among;

    const/16 v8, 0x2d

    invoke-virtual {p0, v7, v8}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 589
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 831
    :cond_0
    :goto_0
    :pswitch_0
    return v5

    .line 594
    :cond_1
    iget v7, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v7, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 595
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v5, v6

    .line 831
    goto :goto_0

    .line 601
    :pswitch_1
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 606
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    goto :goto_1

    .line 611
    :pswitch_2
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 616
    const-string v5, "log"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 621
    :pswitch_3
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 626
    const-string v5, "u"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 631
    :pswitch_4
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 636
    const-string v5, "ente"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 641
    :pswitch_5
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R1()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 646
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    .line 648
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v1, v5, v7

    .line 652
    .local v1, "v_1":I
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 654
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_2:[Lorg/tartarus/snowball/Among;

    const/4 v7, 0x4

    invoke-virtual {p0, v5, v7}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 655
    if-nez v0, :cond_2

    .line 657
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto :goto_1

    .line 661
    :cond_2
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 663
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_3

    .line 665
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto :goto_1

    .line 669
    :cond_3
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    .line 670
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 672
    :pswitch_6
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto :goto_1

    .line 677
    :pswitch_7
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 679
    const-string v5, "at"

    invoke-virtual {p0, v9, v5}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 681
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto :goto_1

    .line 685
    :cond_4
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 687
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_5

    .line 689
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v1

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto/16 :goto_1

    .line 693
    :cond_5
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    goto/16 :goto_1

    .line 701
    .end local v1    # "v_1":I
    :pswitch_8
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 706
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    .line 708
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v2, v5, v7

    .line 712
    .local v2, "v_2":I
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 714
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_3:[Lorg/tartarus/snowball/Among;

    invoke-virtual {p0, v5, v10}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 715
    if-nez v0, :cond_6

    .line 717
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto/16 :goto_1

    .line 721
    :cond_6
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 722
    packed-switch v0, :pswitch_data_2

    goto/16 :goto_1

    .line 724
    :pswitch_9
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto/16 :goto_1

    .line 729
    :pswitch_a
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_7

    .line 731
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v2

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto/16 :goto_1

    .line 735
    :cond_7
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    goto/16 :goto_1

    .line 743
    .end local v2    # "v_2":I
    :pswitch_b
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 748
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    .line 750
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v3, v5, v7

    .line 754
    .local v3, "v_3":I
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 756
    sget-object v5, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_4:[Lorg/tartarus/snowball/Among;

    invoke-virtual {p0, v5, v10}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 757
    if-nez v0, :cond_8

    .line 759
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto/16 :goto_1

    .line 763
    :cond_8
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 764
    packed-switch v0, :pswitch_data_3

    goto/16 :goto_1

    .line 766
    :pswitch_c
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto/16 :goto_1

    .line 771
    :pswitch_d
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_9

    .line 773
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v3

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto/16 :goto_1

    .line 777
    :cond_9
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    goto/16 :goto_1

    .line 785
    .end local v3    # "v_3":I
    :pswitch_e
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 790
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    .line 792
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v7, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v4, v5, v7

    .line 796
    .local v4, "v_4":I
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 798
    const-string v5, "at"

    invoke-virtual {p0, v9, v5}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 800
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto/16 :goto_1

    .line 804
    :cond_a
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 806
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_R2()Z

    move-result v5

    if-nez v5, :cond_b

    .line 808
    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v5, v4

    iput v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto/16 :goto_1

    .line 812
    :cond_b
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    goto/16 :goto_1

    .line 818
    .end local v4    # "v_4":I
    :pswitch_f
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_RV()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 823
    const-string v7, "e"

    invoke-virtual {p0, v6, v7}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 828
    const-string v5, "ir"

    invoke-virtual {p0, v5}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_from(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 595
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_b
        :pswitch_e
        :pswitch_f
    .end packed-switch

    .line 670
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 722
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 764
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private r_verb_suffix()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 839
    iget v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v1, v4, v5

    .line 841
    .local v1, "v_1":I
    iget v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iget v5, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_pV:I

    if-ge v4, v5, :cond_0

    .line 872
    :goto_0
    return v3

    .line 845
    :cond_0
    iget v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->I_pV:I

    iput v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 846
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit_backward:I

    .line 847
    .local v2, "v_2":I
    iget v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit_backward:I

    .line 848
    iget v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v4, v1

    iput v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 851
    iget v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 853
    sget-object v4, Lorg/tartarus/snowball/ext/PortugueseStemmer;->a_6:[Lorg/tartarus/snowball/Among;

    const/16 v5, 0x78

    invoke-virtual {p0, v4, v5}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->find_among_b([Lorg/tartarus/snowball/Among;I)I

    move-result v0

    .line 854
    .local v0, "among_var":I
    if-nez v0, :cond_1

    .line 856
    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit_backward:I

    goto :goto_0

    .line 860
    :cond_1
    iget v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v4, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 861
    packed-switch v0, :pswitch_data_0

    .line 871
    :goto_1
    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit_backward:I

    .line 872
    const/4 v3, 0x1

    goto :goto_0

    .line 863
    :pswitch_0
    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit_backward:I

    goto :goto_0

    .line 868
    :pswitch_1
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    goto :goto_1

    .line 861
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1125
    instance-of v0, p1, Lorg/tartarus/snowball/ext/PortugueseStemmer;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1130
    const-class v0, Lorg/tartarus/snowball/ext/PortugueseStemmer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public stem()Z
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 1006
    iget v0, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1009
    .local v0, "v_1":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_prelude()Z

    move-result v10

    if-nez v10, :cond_0

    .line 1014
    :cond_0
    iput v0, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1016
    iget v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1019
    .local v2, "v_2":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_mark_regions()Z

    move-result v10

    if-nez v10, :cond_1

    .line 1024
    :cond_1
    iput v2, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1026
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit_backward:I

    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1029
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v3, v10, v11

    .line 1034
    .local v3, "v_3":I
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v4, v10, v11

    .line 1038
    .local v4, "v_4":I
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v5, v10, v11

    .line 1042
    .local v5, "v_5":I
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v6, v10, v11

    .line 1045
    .local v6, "v_6":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_standard_suffix()Z

    move-result v10

    if-nez v10, :cond_5

    .line 1051
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v10, v6

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1053
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_verb_suffix()Z

    move-result v10

    if-nez v10, :cond_5

    .line 1091
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v10, v4

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1093
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_residual_suffix()Z

    move-result v10

    if-nez v10, :cond_2

    .line 1099
    :cond_2
    :goto_0
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v10, v3

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1101
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v9, v10, v11

    .line 1104
    .local v9, "v_9":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_residual_form()Z

    move-result v10

    if-nez v10, :cond_3

    .line 1109
    :cond_3
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v10, v9

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1110
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit_backward:I

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1111
    iget v1, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1114
    .local v1, "v_10":I
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_postlude()Z

    move-result v10

    if-nez v10, :cond_4

    .line 1119
    :cond_4
    iput v1, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1120
    return v12

    .line 1058
    .end local v1    # "v_10":I
    .end local v9    # "v_9":I
    :cond_5
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v10, v5

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1060
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v7, v10, v11

    .line 1064
    .local v7, "v_7":I
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->ket:I

    .line 1066
    const-string v10, "i"

    invoke-virtual {p0, v12, v10}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 1088
    :cond_6
    :goto_1
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v10, v7

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    goto :goto_0

    .line 1071
    :cond_7
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->bra:I

    .line 1073
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    iget v11, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    sub-int v8, v10, v11

    .line 1075
    .local v8, "v_8":I
    const-string v10, "c"

    invoke-virtual {p0, v12, v10}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1079
    iget v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->limit:I

    sub-int/2addr v10, v8

    iput v10, p0, Lorg/tartarus/snowball/ext/PortugueseStemmer;->cursor:I

    .line 1081
    invoke-direct {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->r_RV()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1086
    invoke-virtual {p0}, Lorg/tartarus/snowball/ext/PortugueseStemmer;->slice_del()V

    goto :goto_1
.end method
