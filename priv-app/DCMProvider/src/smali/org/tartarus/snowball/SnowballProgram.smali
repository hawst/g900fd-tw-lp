.class public abstract Lorg/tartarus/snowball/SnowballProgram;
.super Ljava/lang/Object;
.source "SnowballProgram.java"


# static fields
.field private static final EMPTY_ARGS:[Ljava/lang/Object;


# instance fields
.field protected bra:I

.field private current:[C

.field protected cursor:I

.field protected ket:I

.field protected limit:I

.field protected limit_backward:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lorg/tartarus/snowball/SnowballProgram;->EMPTY_ARGS:[Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/16 v0, 0x8

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    .line 55
    const-string v0, ""

    invoke-virtual {p0, v0}, Lorg/tartarus/snowball/SnowballProgram;->setCurrent(Ljava/lang/String;)V

    .line 56
    return-void
.end method


# virtual methods
.method protected assign_to(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 3
    .param p1, "s"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v2, 0x0

    .line 474
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 475
    iget-object v0, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    invoke-virtual {p1, v0, v2, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 476
    return-object p1
.end method

.method protected copy_from(Lorg/tartarus/snowball/SnowballProgram;)V
    .locals 1
    .param p1, "other"    # Lorg/tartarus/snowball/SnowballProgram;

    .prologue
    .line 133
    iget-object v0, p1, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iput-object v0, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    .line 134
    iget v0, p1, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 135
    iget v0, p1, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    .line 136
    iget v0, p1, Lorg/tartarus/snowball/SnowballProgram;->limit_backward:I

    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->limit_backward:I

    .line 137
    iget v0, p1, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    .line 138
    iget v0, p1, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    .line 139
    return-void
.end method

.method protected eq_s(ILjava/lang/CharSequence;)Z
    .locals 4
    .param p1, "s_size"    # I
    .param p2, "s"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v1, 0x0

    .line 233
    iget v2, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    sub-int/2addr v2, v3

    if-ge v2, p1, :cond_1

    .line 239
    :cond_0
    :goto_0
    return v1

    .line 235
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ne v0, p1, :cond_2

    .line 238
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/2addr v1, p1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 239
    const/4 v1, 0x1

    goto :goto_0

    .line 236
    :cond_2
    iget-object v2, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/2addr v3, v0

    aget-char v2, v2, v3

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-ne v2, v3, :cond_0

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected eq_s_b(ILjava/lang/CharSequence;)Z
    .locals 4
    .param p1, "s_size"    # I
    .param p2, "s"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v1, 0x0

    .line 244
    iget v2, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->limit_backward:I

    sub-int/2addr v2, v3

    if-ge v2, p1, :cond_1

    .line 250
    :cond_0
    :goto_0
    return v1

    .line 246
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ne v0, p1, :cond_2

    .line 249
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    sub-int/2addr v1, p1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 250
    const/4 v1, 0x1

    goto :goto_0

    .line 247
    :cond_2
    iget-object v2, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    sub-int/2addr v3, p1

    add-int/2addr v3, v0

    aget-char v2, v2, v3

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-ne v2, v3, :cond_0

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected eq_v(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;

    .prologue
    .line 255
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lorg/tartarus/snowball/SnowballProgram;->eq_s(ILjava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method protected eq_v_b(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;

    .prologue
    .line 260
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lorg/tartarus/snowball/SnowballProgram;->eq_s_b(ILjava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method protected find_among([Lorg/tartarus/snowball/Among;I)I
    .locals 20
    .param p1, "v"    # [Lorg/tartarus/snowball/Among;
    .param p2, "v_size"    # I

    .prologue
    .line 265
    const/4 v9, 0x0

    .line 266
    .local v9, "i":I
    move/from16 v11, p2

    .line 268
    .local v11, "j":I
    move-object/from16 v0, p0

    iget v2, v0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 269
    .local v2, "c":I
    move-object/from16 v0, p0

    iget v13, v0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    .line 271
    .local v13, "l":I
    const/4 v4, 0x0

    .line 272
    .local v4, "common_i":I
    const/4 v5, 0x0

    .line 274
    .local v5, "common_j":I
    const/4 v8, 0x0

    .line 277
    .local v8, "first_key_inspected":Z
    :cond_0
    :goto_0
    sub-int v17, v11, v9

    shr-int/lit8 v17, v17, 0x1

    add-int v12, v9, v17

    .line 278
    .local v12, "k":I
    const/4 v6, 0x0

    .line 279
    .local v6, "diff":I
    if-ge v4, v5, :cond_3

    move v3, v4

    .line 280
    .local v3, "common":I
    :goto_1
    aget-object v16, p1, v12

    .line 282
    .local v16, "w":Lorg/tartarus/snowball/Among;
    move v10, v3

    .local v10, "i2":I
    :goto_2
    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->s_size:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v10, v0, :cond_4

    .line 291
    :cond_1
    :goto_3
    if-gez v6, :cond_6

    .line 292
    move v11, v12

    .line 293
    move v5, v3

    .line 298
    :goto_4
    sub-int v17, v11, v9

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-gt v0, v1, :cond_0

    .line 299
    if-lez v9, :cond_7

    .line 311
    :cond_2
    aget-object v16, p1, v9

    .line 312
    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->s_size:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v4, v0, :cond_9

    .line 313
    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->s_size:I

    move/from16 v17, v0

    add-int v17, v17, v2

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 314
    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/tartarus/snowball/Among;->method:Ljava/lang/reflect/Method;

    move-object/from16 v17, v0

    if-nez v17, :cond_8

    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->result:I

    move/from16 v17, v0

    .line 330
    :goto_5
    return v17

    .end local v3    # "common":I
    .end local v10    # "i2":I
    .end local v16    # "w":Lorg/tartarus/snowball/Among;
    :cond_3
    move v3, v5

    .line 279
    goto :goto_1

    .line 283
    .restart local v3    # "common":I
    .restart local v10    # "i2":I
    .restart local v16    # "w":Lorg/tartarus/snowball/Among;
    :cond_4
    add-int v17, v2, v3

    move/from16 v0, v17

    if-ne v0, v13, :cond_5

    .line 284
    const/4 v6, -0x1

    .line 285
    goto :goto_3

    .line 287
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    move-object/from16 v17, v0

    add-int v18, v2, v3

    aget-char v17, v17, v18

    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/tartarus/snowball/Among;->s:[C

    move-object/from16 v18, v0

    aget-char v18, v18, v10

    sub-int v6, v17, v18

    .line 288
    if-nez v6, :cond_1

    .line 289
    add-int/lit8 v3, v3, 0x1

    .line 282
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 295
    :cond_6
    move v9, v12

    .line 296
    move v4, v3

    goto :goto_4

    .line 300
    :cond_7
    if-eq v11, v9, :cond_2

    .line 306
    if-nez v8, :cond_2

    .line 307
    const/4 v8, 0x1

    .line 276
    goto :goto_0

    .line 317
    :cond_8
    :try_start_0
    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/tartarus/snowball/Among;->method:Ljava/lang/reflect/Method;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/tartarus/snowball/Among;->methodobject:Lorg/tartarus/snowball/SnowballProgram;

    move-object/from16 v18, v0

    sget-object v19, Lorg/tartarus/snowball/SnowballProgram;->EMPTY_ARGS:[Ljava/lang/Object;

    invoke-virtual/range {v17 .. v19}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .line 318
    .local v15, "resobj":Ljava/lang/Object;
    invoke-virtual {v15}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    const-string v18, "true"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v14

    .line 326
    .end local v15    # "resobj":Ljava/lang/Object;
    .local v14, "res":Z
    :goto_6
    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->s_size:I

    move/from16 v17, v0

    add-int v17, v17, v2

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 327
    if-eqz v14, :cond_9

    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->result:I

    move/from16 v17, v0

    goto :goto_5

    .line 319
    .end local v14    # "res":Z
    :catch_0
    move-exception v7

    .line 320
    .local v7, "e":Ljava/lang/reflect/InvocationTargetException;
    const/4 v14, 0x0

    .restart local v14    # "res":Z
    goto :goto_6

    .line 322
    .end local v7    # "e":Ljava/lang/reflect/InvocationTargetException;
    .end local v14    # "res":Z
    :catch_1
    move-exception v7

    .line 323
    .local v7, "e":Ljava/lang/IllegalAccessException;
    const/4 v14, 0x0

    .restart local v14    # "res":Z
    goto :goto_6

    .line 329
    .end local v7    # "e":Ljava/lang/IllegalAccessException;
    .end local v14    # "res":Z
    :cond_9
    move-object/from16 v0, v16

    iget v9, v0, Lorg/tartarus/snowball/Among;->substring_i:I

    .line 330
    if-gez v9, :cond_2

    const/16 v17, 0x0

    goto :goto_5
.end method

.method protected find_among_b([Lorg/tartarus/snowball/Among;I)I
    .locals 20
    .param p1, "v"    # [Lorg/tartarus/snowball/Among;
    .param p2, "v_size"    # I

    .prologue
    .line 337
    const/4 v9, 0x0

    .line 338
    .local v9, "i":I
    move/from16 v11, p2

    .line 340
    .local v11, "j":I
    move-object/from16 v0, p0

    iget v2, v0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 341
    .local v2, "c":I
    move-object/from16 v0, p0

    iget v13, v0, Lorg/tartarus/snowball/SnowballProgram;->limit_backward:I

    .line 343
    .local v13, "lb":I
    const/4 v4, 0x0

    .line 344
    .local v4, "common_i":I
    const/4 v5, 0x0

    .line 346
    .local v5, "common_j":I
    const/4 v8, 0x0

    .line 349
    .local v8, "first_key_inspected":Z
    :cond_0
    :goto_0
    sub-int v17, v11, v9

    shr-int/lit8 v17, v17, 0x1

    add-int v12, v9, v17

    .line 350
    .local v12, "k":I
    const/4 v6, 0x0

    .line 351
    .local v6, "diff":I
    if-ge v4, v5, :cond_3

    move v3, v4

    .line 352
    .local v3, "common":I
    :goto_1
    aget-object v16, p1, v12

    .line 354
    .local v16, "w":Lorg/tartarus/snowball/Among;
    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->s_size:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    sub-int v10, v17, v3

    .local v10, "i2":I
    :goto_2
    if-gez v10, :cond_4

    .line 363
    :cond_1
    :goto_3
    if-gez v6, :cond_6

    .line 364
    move v11, v12

    .line 365
    move v5, v3

    .line 370
    :goto_4
    sub-int v17, v11, v9

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-gt v0, v1, :cond_0

    .line 371
    if-lez v9, :cond_7

    .line 378
    :cond_2
    aget-object v16, p1, v9

    .line 379
    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->s_size:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v4, v0, :cond_9

    .line 380
    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->s_size:I

    move/from16 v17, v0

    sub-int v17, v2, v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 381
    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/tartarus/snowball/Among;->method:Ljava/lang/reflect/Method;

    move-object/from16 v17, v0

    if-nez v17, :cond_8

    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->result:I

    move/from16 v17, v0

    .line 398
    :goto_5
    return v17

    .end local v3    # "common":I
    .end local v10    # "i2":I
    .end local v16    # "w":Lorg/tartarus/snowball/Among;
    :cond_3
    move v3, v5

    .line 351
    goto :goto_1

    .line 355
    .restart local v3    # "common":I
    .restart local v10    # "i2":I
    .restart local v16    # "w":Lorg/tartarus/snowball/Among;
    :cond_4
    sub-int v17, v2, v3

    move/from16 v0, v17

    if-ne v0, v13, :cond_5

    .line 356
    const/4 v6, -0x1

    .line 357
    goto :goto_3

    .line 359
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    move-object/from16 v17, v0

    add-int/lit8 v18, v2, -0x1

    sub-int v18, v18, v3

    aget-char v17, v17, v18

    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/tartarus/snowball/Among;->s:[C

    move-object/from16 v18, v0

    aget-char v18, v18, v10

    sub-int v6, v17, v18

    .line 360
    if-nez v6, :cond_1

    .line 361
    add-int/lit8 v3, v3, 0x1

    .line 354
    add-int/lit8 v10, v10, -0x1

    goto :goto_2

    .line 367
    :cond_6
    move v9, v12

    .line 368
    move v4, v3

    goto :goto_4

    .line 372
    :cond_7
    if-eq v11, v9, :cond_2

    .line 373
    if-nez v8, :cond_2

    .line 374
    const/4 v8, 0x1

    .line 348
    goto :goto_0

    .line 385
    :cond_8
    :try_start_0
    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/tartarus/snowball/Among;->method:Ljava/lang/reflect/Method;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/tartarus/snowball/Among;->methodobject:Lorg/tartarus/snowball/SnowballProgram;

    move-object/from16 v18, v0

    sget-object v19, Lorg/tartarus/snowball/SnowballProgram;->EMPTY_ARGS:[Ljava/lang/Object;

    invoke-virtual/range {v17 .. v19}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .line 386
    .local v15, "resobj":Ljava/lang/Object;
    invoke-virtual {v15}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    const-string v18, "true"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v14

    .line 394
    .end local v15    # "resobj":Ljava/lang/Object;
    .local v14, "res":Z
    :goto_6
    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->s_size:I

    move/from16 v17, v0

    sub-int v17, v2, v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 395
    if-eqz v14, :cond_9

    move-object/from16 v0, v16

    iget v0, v0, Lorg/tartarus/snowball/Among;->result:I

    move/from16 v17, v0

    goto :goto_5

    .line 387
    .end local v14    # "res":Z
    :catch_0
    move-exception v7

    .line 388
    .local v7, "e":Ljava/lang/reflect/InvocationTargetException;
    const/4 v14, 0x0

    .restart local v14    # "res":Z
    goto :goto_6

    .line 390
    .end local v7    # "e":Ljava/lang/reflect/InvocationTargetException;
    .end local v14    # "res":Z
    :catch_1
    move-exception v7

    .line 391
    .local v7, "e":Ljava/lang/IllegalAccessException;
    const/4 v14, 0x0

    .restart local v14    # "res":Z
    goto :goto_6

    .line 397
    .end local v7    # "e":Ljava/lang/IllegalAccessException;
    .end local v14    # "res":Z
    :cond_9
    move-object/from16 v0, v16

    iget v9, v0, Lorg/tartarus/snowball/Among;->substring_i:I

    .line 398
    if-gez v9, :cond_2

    const/16 v17, 0x0

    goto :goto_5
.end method

.method public getCurrent()Ljava/lang/String;
    .locals 4

    .prologue
    .line 78
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    const/4 v2, 0x0

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public getCurrentBuffer()[C
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    return-object v0
.end method

.method public getCurrentBufferLength()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    return v0
.end method

.method protected in_grouping([CII)Z
    .locals 5
    .param p1, "s"    # [C
    .param p2, "min"    # I
    .param p3, "max"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 143
    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    if-lt v3, v4, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v1

    .line 144
    :cond_1
    iget-object v3, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    aget-char v0, v3, v4

    .line 145
    .local v0, "ch":C
    if-gt v0, p3, :cond_0

    if-lt v0, p2, :cond_0

    .line 146
    sub-int v3, v0, p2

    int-to-char v0, v3

    .line 147
    shr-int/lit8 v3, v0, 0x3

    aget-char v3, p1, v3

    and-int/lit8 v4, v0, 0x7

    shl-int v4, v2, v4

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    .line 148
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    move v1, v2

    .line 149
    goto :goto_0
.end method

.method protected in_grouping_b([CII)Z
    .locals 5
    .param p1, "s"    # [C
    .param p2, "min"    # I
    .param p3, "max"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 154
    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->limit_backward:I

    if-gt v3, v4, :cond_1

    .line 160
    :cond_0
    :goto_0
    return v1

    .line 155
    :cond_1
    iget-object v3, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v4, v4, -0x1

    aget-char v0, v3, v4

    .line 156
    .local v0, "ch":C
    if-gt v0, p3, :cond_0

    if-lt v0, p2, :cond_0

    .line 157
    sub-int v3, v0, p2

    int-to-char v0, v3

    .line 158
    shr-int/lit8 v3, v0, 0x3

    aget-char v3, p1, v3

    and-int/lit8 v4, v0, 0x7

    shl-int v4, v2, v4

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    .line 159
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    move v1, v2

    .line 160
    goto :goto_0
.end method

.method protected in_range(II)Z
    .locals 4
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    const/4 v1, 0x0

    .line 197
    iget v2, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    if-lt v2, v3, :cond_1

    .line 201
    :cond_0
    :goto_0
    return v1

    .line 198
    :cond_1
    iget-object v2, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    aget-char v0, v2, v3

    .line 199
    .local v0, "ch":C
    if-gt v0, p2, :cond_0

    if-lt v0, p1, :cond_0

    .line 200
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 201
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected in_range_b(II)Z
    .locals 4
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    const/4 v1, 0x0

    .line 206
    iget v2, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->limit_backward:I

    if-gt v2, v3, :cond_1

    .line 210
    :cond_0
    :goto_0
    return v1

    .line 207
    :cond_1
    iget-object v2, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v3, v3, -0x1

    aget-char v0, v2, v3

    .line 208
    .local v0, "ch":C
    if-gt v0, p2, :cond_0

    if-lt v0, p1, :cond_0

    .line 209
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 210
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected insert(IILjava/lang/CharSequence;)V
    .locals 2
    .param p1, "c_bra"    # I
    .param p2, "c_ket"    # I
    .param p3, "s"    # Ljava/lang/CharSequence;

    .prologue
    .line 457
    invoke-virtual {p0, p1, p2, p3}, Lorg/tartarus/snowball/SnowballProgram;->replace_s(IILjava/lang/CharSequence;)I

    move-result v0

    .line 458
    .local v0, "adjustment":I
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    if-gt p1, v1, :cond_0

    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    .line 459
    :cond_0
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    if-gt p1, v1, :cond_1

    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    .line 460
    :cond_1
    return-void
.end method

.method protected out_grouping([CII)Z
    .locals 5
    .param p1, "s"    # [C
    .param p2, "min"    # I
    .param p3, "max"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 165
    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    if-lt v3, v4, :cond_1

    .line 176
    :cond_0
    :goto_0
    return v1

    .line 166
    :cond_1
    iget-object v3, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    aget-char v0, v3, v4

    .line 167
    .local v0, "ch":C
    if-gt v0, p3, :cond_2

    if-ge v0, p2, :cond_3

    .line 168
    :cond_2
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    move v1, v2

    .line 169
    goto :goto_0

    .line 171
    :cond_3
    sub-int v3, v0, p2

    int-to-char v0, v3

    .line 172
    shr-int/lit8 v3, v0, 0x3

    aget-char v3, p1, v3

    and-int/lit8 v4, v0, 0x7

    shl-int v4, v2, v4

    and-int/2addr v3, v4

    if-nez v3, :cond_0

    .line 173
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    move v1, v2

    .line 174
    goto :goto_0
.end method

.method protected out_grouping_b([CII)Z
    .locals 5
    .param p1, "s"    # [C
    .param p2, "min"    # I
    .param p3, "max"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 181
    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->limit_backward:I

    if-gt v3, v4, :cond_1

    .line 192
    :cond_0
    :goto_0
    return v1

    .line 182
    :cond_1
    iget-object v3, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v4, v4, -0x1

    aget-char v0, v3, v4

    .line 183
    .local v0, "ch":C
    if-gt v0, p3, :cond_2

    if-ge v0, p2, :cond_3

    .line 184
    :cond_2
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    move v1, v2

    .line 185
    goto :goto_0

    .line 187
    :cond_3
    sub-int v3, v0, p2

    int-to-char v0, v3

    .line 188
    shr-int/lit8 v3, v0, 0x3

    aget-char v3, p1, v3

    and-int/lit8 v4, v0, 0x7

    shl-int v4, v2, v4

    and-int/2addr v3, v4

    if-nez v3, :cond_0

    .line 189
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    move v1, v2

    .line 190
    goto :goto_0
.end method

.method protected out_range(II)Z
    .locals 4
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    const/4 v1, 0x0

    .line 215
    iget v2, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    if-lt v2, v3, :cond_1

    .line 219
    :cond_0
    :goto_0
    return v1

    .line 216
    :cond_1
    iget-object v2, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    aget-char v0, v2, v3

    .line 217
    .local v0, "ch":C
    if-gt v0, p2, :cond_2

    if-ge v0, p1, :cond_0

    .line 218
    :cond_2
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 219
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected out_range_b(II)Z
    .locals 4
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    const/4 v1, 0x0

    .line 224
    iget v2, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->limit_backward:I

    if-gt v2, v3, :cond_1

    .line 228
    :cond_0
    :goto_0
    return v1

    .line 225
    :cond_1
    iget-object v2, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v3, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v3, v3, -0x1

    aget-char v0, v2, v3

    .line 226
    .local v0, "ch":C
    if-gt v0, p2, :cond_2

    if-ge v0, p1, :cond_0

    .line 227
    :cond_2
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 228
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected replace_s(IILjava/lang/CharSequence;)I
    .locals 8
    .param p1, "c_bra"    # I
    .param p2, "c_ket"    # I
    .param p3, "s"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v6, 0x0

    .line 406
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    sub-int v5, p2, p1

    sub-int v0, v4, v5

    .line 407
    .local v0, "adjustment":I
    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    add-int v3, v4, v0

    .line 409
    .local v3, "newLength":I
    iget-object v4, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    array-length v4, v4

    if-le v3, v4, :cond_0

    .line 410
    const/4 v4, 0x2

    invoke-static {v3, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v4

    new-array v2, v4, [C

    .line 411
    .local v2, "newBuffer":[C
    iget-object v4, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v5, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    invoke-static {v4, v6, v2, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 412
    iput-object v2, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    .line 416
    .end local v2    # "newBuffer":[C
    :cond_0
    if-eqz v0, :cond_1

    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    if-ge p2, v4, :cond_1

    .line 417
    iget-object v4, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget-object v5, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v6

    add-int/2addr v6, p1

    .line 418
    iget v7, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    sub-int/2addr v7, p2

    .line 417
    invoke-static {v4, p2, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 423
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lt v1, v4, :cond_3

    .line 426
    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    add-int/2addr v4, v0

    iput v4, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    .line 427
    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    if-lt v4, p2, :cond_4

    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    add-int/2addr v4, v0

    iput v4, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 429
    :cond_2
    :goto_1
    return v0

    .line 424
    :cond_3
    iget-object v4, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    add-int v5, p1, v1

    invoke-interface {p3, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    aput-char v6, v4, v5

    .line 423
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 428
    :cond_4
    iget v4, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    if-le v4, p1, :cond_2

    iput p1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    goto :goto_1
.end method

.method public setCurrent(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    .line 66
    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 67
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    .line 68
    iput v1, p0, Lorg/tartarus/snowball/SnowballProgram;->limit_backward:I

    .line 69
    iget v0, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    .line 70
    iget v0, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    .line 71
    return-void
.end method

.method public setCurrent([CI)V
    .locals 1
    .param p1, "text"    # [C
    .param p2, "length"    # I

    .prologue
    const/4 v0, 0x0

    .line 87
    iput-object p1, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    .line 88
    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    .line 89
    iput p2, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    .line 90
    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->limit_backward:I

    .line 91
    iget v0, p0, Lorg/tartarus/snowball/SnowballProgram;->cursor:I

    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    .line 92
    iget v0, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    iput v0, p0, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    .line 93
    return-void
.end method

.method protected slice_check()V
    .locals 3

    .prologue
    .line 433
    iget v0, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    if-ltz v0, :cond_0

    .line 434
    iget v0, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    if-gt v0, v1, :cond_0

    .line 435
    iget v0, p0, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    if-le v0, v1, :cond_1

    .line 436
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "faulty slice operation: bra="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",ket="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",limit="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/tartarus/snowball/SnowballProgram;->limit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 444
    :cond_1
    return-void
.end method

.method protected slice_del()V
    .locals 1

    .prologue
    .line 452
    const-string v0, ""

    invoke-virtual {p0, v0}, Lorg/tartarus/snowball/SnowballProgram;->slice_from(Ljava/lang/CharSequence;)V

    .line 453
    return-void
.end method

.method protected slice_from(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;

    .prologue
    .line 447
    invoke-virtual {p0}, Lorg/tartarus/snowball/SnowballProgram;->slice_check()V

    .line 448
    iget v0, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    invoke-virtual {p0, v0, v1, p1}, Lorg/tartarus/snowball/SnowballProgram;->replace_s(IILjava/lang/CharSequence;)I

    .line 449
    return-void
.end method

.method protected slice_to(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 3
    .param p1, "s"    # Ljava/lang/StringBuilder;

    .prologue
    .line 465
    invoke-virtual {p0}, Lorg/tartarus/snowball/SnowballProgram;->slice_check()V

    .line 466
    iget v1, p0, Lorg/tartarus/snowball/SnowballProgram;->ket:I

    iget v2, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    sub-int v0, v1, v2

    .line 467
    .local v0, "len":I
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 468
    iget-object v1, p0, Lorg/tartarus/snowball/SnowballProgram;->current:[C

    iget v2, p0, Lorg/tartarus/snowball/SnowballProgram;->bra:I

    invoke-virtual {p1, v1, v2, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 469
    return-object p1
.end method

.method public abstract stem()Z
.end method
