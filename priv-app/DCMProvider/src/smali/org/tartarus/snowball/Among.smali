.class public Lorg/tartarus/snowball/Among;
.super Ljava/lang/Object;
.source "Among.java"


# static fields
.field private static final EMPTY_PARAMS:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final method:Ljava/lang/reflect/Method;

.field public final methodobject:Lorg/tartarus/snowball/SnowballProgram;

.field public final result:I

.field public final s:[C

.field public final s_size:I

.field public final substring_i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Class;

    sput-object v0, Lorg/tartarus/snowball/Among;->EMPTY_PARAMS:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;Lorg/tartarus/snowball/SnowballProgram;)V
    .locals 3
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "substring_i"    # I
    .param p3, "result"    # I
    .param p4, "methodname"    # Ljava/lang/String;
    .param p5, "methodobject"    # Lorg/tartarus/snowball/SnowballProgram;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    iput v1, p0, Lorg/tartarus/snowball/Among;->s_size:I

    .line 51
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    iput-object v1, p0, Lorg/tartarus/snowball/Among;->s:[C

    .line 52
    iput p2, p0, Lorg/tartarus/snowball/Among;->substring_i:I

    .line 53
    iput p3, p0, Lorg/tartarus/snowball/Among;->result:I

    .line 54
    iput-object p5, p0, Lorg/tartarus/snowball/Among;->methodobject:Lorg/tartarus/snowball/SnowballProgram;

    .line 55
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 56
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/tartarus/snowball/Among;->method:Ljava/lang/reflect/Method;

    .line 65
    :goto_0
    return-void

    .line 59
    :cond_0
    :try_start_0
    invoke-virtual {p5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 60
    sget-object v2, Lorg/tartarus/snowball/Among;->EMPTY_PARAMS:[Ljava/lang/Class;

    invoke-virtual {v1, p4, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 59
    iput-object v1, p0, Lorg/tartarus/snowball/Among;->method:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
