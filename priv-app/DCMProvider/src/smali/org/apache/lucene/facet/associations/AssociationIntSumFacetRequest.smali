.class public Lorg/apache/lucene/facet/associations/AssociationIntSumFacetRequest;
.super Lorg/apache/lucene/facet/search/FacetRequest;
.source "AssociationIntSumFacetRequest.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V
    .locals 0
    .param p1, "path"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "num"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/search/FacetRequest;-><init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V

    .line 38
    return-void
.end method


# virtual methods
.method public getFacetArraysSource()Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->INT:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    return-object v0
.end method

.method public getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D
    .locals 2
    .param p1, "arrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p2, "ordinal"    # I

    .prologue
    .line 47
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetArrays;->getIntArray()[I

    move-result-object v0

    aget v0, v0, p2

    int-to-double v0, v0

    return-wide v0
.end method
