.class public interface abstract Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;
.super Ljava/lang/Object;
.source "TaxonomyWriterCache.java"


# virtual methods
.method public abstract clear()V
.end method

.method public abstract close()V
.end method

.method public abstract get(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
.end method

.method public abstract isFull()Z
.end method

.method public abstract put(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)Z
.end method
