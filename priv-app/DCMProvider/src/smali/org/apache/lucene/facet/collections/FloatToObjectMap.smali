.class public Lorg/apache/lucene/facet/collections/FloatToObjectMap;
.super Ljava/lang/Object;
.source "FloatToObjectMap.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;,
        Lorg/apache/lucene/facet/collections/FloatToObjectMap$KeyIterator;,
        Lorg/apache/lucene/facet/collections/FloatToObjectMap$ValueIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static defaultCapacity:I


# instance fields
.field baseHash:[I

.field private capacity:I

.field private firstEmpty:I

.field private hashFactor:I

.field keys:[F

.field next:[I

.field private prev:I

.field private size:I

.field values:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 159
    const/16 v0, 0x10

    sput v0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->defaultCapacity:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 213
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    sget v0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->defaultCapacity:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;-><init>(I)V

    .line 214
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1, "capacity"    # I

    .prologue
    .line 223
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    const/16 v2, 0x10

    iput v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    .line 226
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    if-lt v2, p1, :cond_0

    .line 233
    iget v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    add-int/lit8 v0, v2, 0x1

    .line 235
    .local v0, "arrayLength":I
    new-array v2, v0, [Ljava/lang/Object;

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->values:[Ljava/lang/Object;

    .line 236
    new-array v2, v0, [F

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->keys:[F

    .line 237
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    .line 240
    iget v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    shl-int/lit8 v1, v2, 0x1

    .line 242
    .local v1, "baseHashSize":I
    new-array v2, v1, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    .line 246
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->hashFactor:I

    .line 248
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    .line 250
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->clear()V

    .line 251
    return-void

    .line 228
    .end local v0    # "arrayLength":I
    .end local v1    # "baseHashSize":I
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    goto :goto_0
.end method

.method private findForRemove(FI)I
    .locals 3
    .param p1, "key"    # F
    .param p2, "baseHashIndex"    # I

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    const/4 v1, 0x0

    .line 385
    iput v1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->prev:I

    .line 386
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    aget v0, v2, p2

    .line 389
    .local v0, "index":I
    :goto_0
    if-nez v0, :cond_1

    .line 402
    iput v1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->prev:I

    move v0, v1

    .line 403
    .end local v0    # "index":I
    :cond_0
    return v0

    .line 391
    .restart local v0    # "index":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->keys:[F

    aget v2, v2, v0

    cmpl-float v2, v2, p1

    if-eqz v2, :cond_0

    .line 396
    iput v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->prev:I

    .line 397
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    aget v0, v2, v0

    goto :goto_0
.end method

.method private getBaseHashAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private prvt_put(FLjava/lang/Object;)V
    .locals 4
    .param p1, "key"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FTT;)V"
        }
    .end annotation

    .prologue
    .line 267
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    .local p2, "e":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->calcBaseHashIndex(F)I

    move-result v0

    .line 270
    .local v0, "hashIndex":I
    iget v1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->firstEmpty:I

    .line 273
    .local v1, "objectIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->firstEmpty:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->firstEmpty:I

    .line 274
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->values:[Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 275
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->keys:[F

    aput p1, v2, v1

    .line 278
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 279
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    aput v1, v2, v0

    .line 282
    iget v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    .line 283
    return-void
.end method


# virtual methods
.method protected calcBaseHashIndex(F)I
    .locals 2
    .param p1, "key"    # F

    .prologue
    .line 289
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    invoke-static {p1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->hashFactor:I

    and-int/2addr v0, v1

    return v0
.end method

.method public clear()V
    .locals 5

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    const/4 v4, 0x0

    .line 297
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 300
    iput v4, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    .line 305
    const/4 v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->firstEmpty:I

    .line 309
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    if-lt v0, v2, :cond_0

    .line 314
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    aput v4, v2, v3

    .line 315
    return-void

    .line 310
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput v1, v2, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public containsKey(F)Z
    .locals 1
    .param p1, "key"    # F

    .prologue
    .line 325
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->find(F)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 338
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 344
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 339
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 340
    .local v1, "object":Ljava/lang/Object;, "TT;"
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 341
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    const/4 v5, 0x0

    .line 612
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/facet/collections/FloatToObjectMap;

    .line 613
    .local v2, "that":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    invoke-virtual {v2}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size()I

    move-result v6

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size()I

    move-result v7

    if-eq v6, v7, :cond_1

    .line 632
    :cond_0
    :goto_0
    return v5

    .line 617
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->keyIterator()Lorg/apache/lucene/facet/collections/FloatIterator;

    move-result-object v0

    .line 618
    .local v0, "it":Lorg/apache/lucene/facet/collections/FloatIterator;
    :cond_2
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/FloatIterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 632
    const/4 v5, 0x1

    goto :goto_0

    .line 619
    :cond_3
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/FloatIterator;->next()F

    move-result v1

    .line 620
    .local v1, "key":F
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->containsKey(F)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 624
    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->get(F)Ljava/lang/Object;

    move-result-object v3

    .line 625
    .local v3, "v1":Ljava/lang/Object;, "TT;"
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->get(F)Ljava/lang/Object;

    move-result-object v4

    .line 626
    .local v4, "v2":Ljava/lang/Object;, "TT;"
    if-nez v3, :cond_4

    if-nez v4, :cond_0

    .line 627
    :cond_4
    if-eqz v3, :cond_5

    if-eqz v4, :cond_0

    .line 628
    :cond_5
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    goto :goto_0
.end method

.method protected find(F)I
    .locals 3
    .param p1, "key"    # F

    .prologue
    .line 354
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->calcBaseHashIndex(F)I

    move-result v0

    .line 357
    .local v0, "baseHashIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    aget v1, v2, v0

    .line 360
    .local v1, "localIndex":I
    :goto_0
    if-nez v1, :cond_1

    .line 372
    const/4 v1, 0x0

    .end local v1    # "localIndex":I
    :cond_0
    return v1

    .line 362
    .restart local v1    # "localIndex":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->keys:[F

    aget v2, v2, v1

    cmpl-float v2, v2, p1

    if-eqz v2, :cond_0

    .line 367
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    aget v1, v2, v1

    goto :goto_0
.end method

.method public get(F)Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    .prologue
    .line 415
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->values:[Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->find(F)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected grow()V
    .locals 5

    .prologue
    .line 424
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    new-instance v2, Lorg/apache/lucene/facet/collections/FloatToObjectMap;

    .line 425
    iget v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    mul-int/lit8 v3, v3, 0x2

    .line 424
    invoke-direct {v2, v3}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;-><init>(I)V

    .line 430
    .local v2, "that":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    new-instance v1, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;

    invoke-direct {v1, p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/FloatToObjectMap;)V

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>.IndexIterator;"
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 436
    iget v3, v2, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    .line 437
    iget v3, v2, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    .line 438
    iget v3, v2, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->firstEmpty:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->firstEmpty:I

    .line 439
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->values:[Ljava/lang/Object;

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->values:[Ljava/lang/Object;

    .line 440
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->keys:[F

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->keys:[F

    .line 441
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    .line 442
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    .line 443
    iget v3, v2, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->hashFactor:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->hashFactor:I

    .line 444
    return-void

    .line 431
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->next()I

    move-result v0

    .line 432
    .local v0, "index":I
    iget-object v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->keys:[F

    aget v3, v3, v0

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->values:[Ljava/lang/Object;

    aget-object v4, v4, v0

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->prvt_put(FLjava/lang/Object;)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 606
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 451
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    iget v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 459
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    new-instance v0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$ValueIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap$ValueIterator;-><init>(Lorg/apache/lucene/facet/collections/FloatToObjectMap;)V

    return-object v0
.end method

.method public keyIterator()Lorg/apache/lucene/facet/collections/FloatIterator;
    .locals 1

    .prologue
    .line 464
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    new-instance v0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$KeyIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap$KeyIterator;-><init>(Lorg/apache/lucene/facet/collections/FloatToObjectMap;)V

    return-object v0
.end method

.method public put(FLjava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "key"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FTT;)TT;"
        }
    .end annotation

    .prologue
    .line 485
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    .local p2, "e":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->find(F)I

    move-result v0

    .line 488
    .local v0, "index":I
    if-eqz v0, :cond_0

    .line 490
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->values:[Ljava/lang/Object;

    aget-object v1, v2, v0

    .line 491
    .local v1, "old":Ljava/lang/Object;, "TT;"
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->values:[Ljava/lang/Object;

    aput-object p2, v2, v0

    .line 505
    .end local v1    # "old":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object v1

    .line 496
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    iget v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->capacity:I

    if-ne v2, v3, :cond_1

    .line 498
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->grow()V

    .line 503
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->prvt_put(FLjava/lang/Object;)V

    .line 505
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public remove(F)Ljava/lang/Object;
    .locals 5
    .param p1, "key"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    .prologue
    .line 517
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->calcBaseHashIndex(F)I

    move-result v0

    .line 518
    .local v0, "baseHashIndex":I
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->findForRemove(FI)I

    move-result v1

    .line 519
    .local v1, "index":I
    if-eqz v1, :cond_1

    .line 522
    iget v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->prev:I

    if-nez v2, :cond_0

    .line 523
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    aget v3, v3, v1

    aput v3, v2, v0

    .line 526
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->prev:I

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    aget v4, v4, v1

    aput v4, v2, v3

    .line 527
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->firstEmpty:I

    aput v3, v2, v1

    .line 528
    iput v1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->firstEmpty:I

    .line 529
    iget v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    .line 530
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->values:[Ljava/lang/Object;

    aget-object v2, v2, v1

    .line 533
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 540
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    iget v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 4

    .prologue
    .line 549
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    const/4 v2, -0x1

    .line 550
    .local v2, "j":I
    iget v3, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->size:I

    new-array v0, v3, [Ljava/lang/Object;

    .line 553
    .local v0, "array":[Ljava/lang/Object;
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 556
    return-object v0

    .line 554
    :cond_0
    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v0, v2

    goto :goto_0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1, "a"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 571
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    const/4 v1, 0x0

    .line 573
    .local v1, "j":I
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 574
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 578
    :cond_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 579
    const/4 v2, 0x0

    aput-object v2, p1, v1

    .line 582
    :cond_1
    return-object p1

    .line 575
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, p1, v1

    .line 574
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 587
    .local p0, "this":Lorg/apache/lucene/facet/collections/FloatToObjectMap;, "Lorg/apache/lucene/facet/collections/FloatToObjectMap<TT;>;"
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 588
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/16 v3, 0x7b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 589
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->keyIterator()Lorg/apache/lucene/facet/collections/FloatIterator;

    move-result-object v1

    .line 590
    .local v1, "keyIterator":Lorg/apache/lucene/facet/collections/FloatIterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/FloatIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 600
    const/16 v3, 0x7d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 601
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 591
    :cond_1
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/FloatIterator;->next()F

    move-result v0

    .line 592
    .local v0, "key":F
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 593
    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 594
    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->get(F)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 595
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/FloatIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 596
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 597
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
