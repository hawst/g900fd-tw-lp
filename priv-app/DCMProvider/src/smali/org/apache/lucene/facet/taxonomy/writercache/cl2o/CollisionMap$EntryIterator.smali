.class Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;
.super Ljava/lang/Object;
.source "CollisionMap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EntryIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;",
        ">;"
    }
.end annotation


# instance fields
.field ents:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

.field index:I

.field next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

.field final synthetic this$0:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;I)V
    .locals 3
    .param p2, "entries"    # [Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    .param p3, "size"    # I

    .prologue
    .line 188
    iput-object p1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;->this$0:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    iput-object p2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;->ents:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 190
    move-object v2, p2

    .line 191
    .local v2, "t":[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    array-length v0, v2

    .line 192
    .local v0, "i":I
    const/4 v1, 0x0

    .line 193
    .local v1, "n":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    if-eqz p3, :cond_1

    .line 194
    :cond_0
    if-lez v0, :cond_1

    add-int/lit8 v0, v0, -0x1

    aget-object v1, v2, v0

    if-eqz v1, :cond_0

    .line 198
    :cond_1
    iput-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;->next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 199
    iput v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;->index:I

    .line 200
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;->next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;->next()Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    .locals 5

    .prologue
    .line 209
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;->next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 210
    .local v0, "e":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    if-nez v0, :cond_0

    new-instance v4, Ljava/util/NoSuchElementException;

    invoke-direct {v4}, Ljava/util/NoSuchElementException;-><init>()V

    throw v4

    .line 212
    :cond_0
    iget-object v2, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 213
    .local v2, "n":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;->ents:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 214
    .local v3, "t":[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;->index:I

    .line 215
    .local v1, "i":I
    :goto_0
    if-nez v2, :cond_1

    if-gtz v1, :cond_2

    .line 218
    :cond_1
    iput v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;->index:I

    .line 219
    iput-object v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;->next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 220
    return-object v0

    .line 216
    :cond_2
    add-int/lit8 v1, v1, -0x1

    aget-object v2, v3, v1

    goto :goto_0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 225
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
