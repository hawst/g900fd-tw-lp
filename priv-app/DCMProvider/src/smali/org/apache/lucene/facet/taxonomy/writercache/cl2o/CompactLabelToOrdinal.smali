.class public Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;
.super Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/LabelToOrdinal;
.source "CompactLabelToOrdinal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;
    }
.end annotation


# static fields
.field private static final COLLISION:I = -0x5

.field public static final DefaultLoadFactor:F = 0.15f

.field static final TERMINATOR_CHAR:C = '\uffff'


# instance fields
.field private capacity:I

.field private collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

.field private hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

.field private labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

.field private loadFactor:F

.field private threshold:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/LabelToOrdinal;-><init>()V

    .line 76
    return-void
.end method

.method public constructor <init>(IFI)V
    .locals 4
    .param p1, "initialCapacity"    # I
    .param p2, "loadFactor"    # F
    .param p3, "numHashArrays"    # I

    .prologue
    .line 78
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/LabelToOrdinal;-><init>()V

    .line 81
    new-array v0, p3, [Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    .line 83
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    int-to-double v2, p3

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->determineCapacity(II)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->capacity:I

    .line 85
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->init()V

    .line 86
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;-><init>(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->counter:I

    .line 89
    iput p2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->loadFactor:F

    .line 91
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->loadFactor:F

    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->capacity:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->threshold:I

    .line 92
    return-void
.end method

.method private addLabel(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;Lorg/apache/lucene/facet/taxonomy/CategoryPath;II)Z
    .locals 4
    .param p1, "a"    # Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;
    .param p2, "label"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p3, "hash"    # I
    .param p4, "ordinal"    # I

    .prologue
    .line 203
    iget-object v2, p1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    array-length v2, v2

    invoke-static {p3, v2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->indexFor(II)I

    move-result v0

    .line 204
    .local v0, "index":I
    iget-object v2, p1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    aget v1, v2, v0

    .line 206
    .local v1, "offset":I
    if-nez v1, :cond_0

    .line 207
    iget-object v2, p1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    invoke-virtual {v3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->length()I

    move-result v3

    aput v3, v2, v0

    .line 208
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    invoke-static {p2, v2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CategoryPathUtils;->serialize(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V

    .line 209
    iget-object v2, p1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->cids:[I

    aput p4, v2, v0

    .line 210
    const/4 v2, 0x1

    .line 213
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private addLabelOffset(III)V
    .locals 3
    .param p1, "hash"    # I
    .param p2, "cid"    # I
    .param p3, "knownOffset"    # I

    .prologue
    .line 217
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 224
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    invoke-virtual {v1, p1, p3, p2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->addLabelOffset(III)V

    .line 226
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    invoke-virtual {v1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->size()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->threshold:I

    if-le v1, v2, :cond_0

    .line 227
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->grow()V

    .line 229
    :cond_0
    return-void

    .line 218
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    aget-object v1, v1, v0

    invoke-direct {p0, v1, p1, p2, p3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->addLabelOffsetToHashArray(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;III)Z

    move-result v1

    .line 219
    if-nez v1, :cond_0

    .line 217
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private addLabelOffsetToHashArray(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;III)Z
    .locals 3
    .param p1, "a"    # Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;
    .param p2, "hash"    # I
    .param p3, "ordinal"    # I
    .param p4, "knownOffset"    # I

    .prologue
    .line 234
    iget-object v2, p1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    array-length v2, v2

    invoke-static {p2, v2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->indexFor(II)I

    move-result v0

    .line 235
    .local v0, "index":I
    iget-object v2, p1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    aget v1, v2, v0

    .line 237
    .local v1, "offset":I
    if-nez v1, :cond_0

    .line 238
    iget-object v2, p1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    aput p4, v2, v0

    .line 239
    iget-object v2, p1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->cids:[I

    aput p3, v2, v0

    .line 240
    const/4 v2, 0x1

    .line 243
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static determineCapacity(II)I
    .locals 1
    .param p0, "minCapacity"    # I
    .param p1, "initialCapacity"    # I

    .prologue
    .line 95
    move v0, p0

    .line 96
    .local v0, "capacity":I
    :goto_0
    if-lt v0, p1, :cond_0

    .line 99
    return v0

    .line 97
    :cond_0
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getOrdinal(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)I
    .locals 4
    .param p1, "a"    # Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;
    .param p2, "label"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p3, "hash"    # I

    .prologue
    const/4 v2, -0x2

    .line 247
    if-nez p2, :cond_1

    .line 261
    :cond_0
    :goto_0
    return v2

    .line 251
    :cond_1
    iget-object v3, p1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    array-length v3, v3

    invoke-static {p3, v3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->indexFor(II)I

    move-result v0

    .line 252
    .local v0, "index":I
    iget-object v3, p1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    aget v1, v3, v0

    .line 253
    .local v1, "offset":I
    if-eqz v1, :cond_0

    .line 257
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    invoke-static {p2, v2, v1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CategoryPathUtils;->equalsToSerialized(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 258
    iget-object v2, p1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->cids:[I

    aget v2, v2, v0

    goto :goto_0

    .line 261
    :cond_2
    const/4 v2, -0x5

    goto :goto_0
.end method

.method private grow()V
    .locals 22

    .prologue
    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    aget-object v17, v18, v19

    .line 152
    .local v17, "temp":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    add-int/lit8 v6, v18, -0x1

    .local v6, "i":I
    :goto_0
    if-gtz v6, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->capacity:I

    move/from16 v18, v0

    mul-int/lit8 v18, v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->capacity:I

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    new-instance v20, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->capacity:I

    move/from16 v21, v0

    invoke-direct/range {v20 .. v21}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;-><init>(I)V

    aput-object v20, v18, v19

    .line 159
    const/4 v6, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v6, v0, :cond_1

    .line 181
    const/4 v6, 0x0

    :goto_2
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v6, v0, :cond_6

    .line 189
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    .line 190
    .local v12, "oldCollisionMap":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;
    new-instance v18, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    invoke-virtual {v12}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->capacity()I

    move-result v19

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    move-object/from16 v20, v0

    invoke-direct/range {v18 .. v20}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;-><init>(ILorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V

    .line 190
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    .line 192
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->capacity:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->loadFactor:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->threshold:I

    .line 194
    invoke-virtual {v12}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->entryIterator()Ljava/util/Iterator;

    move-result-object v7

    .line 195
    .local v7, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;>;"
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_8

    .line 200
    return-void

    .line 153
    .end local v7    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;>;"
    .end local v12    # "oldCollisionMap":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v19, v0

    add-int/lit8 v20, v6, -0x1

    aget-object v19, v19, v20

    aput-object v19, v18, v6

    .line 152
    add-int/lit8 v6, v6, -0x1

    goto/16 :goto_0

    .line 160
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v18, v0

    aget-object v18, v18, v6

    move-object/from16 v0, v18

    iget-object v14, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    .line 161
    .local v14, "sourceOffsetArray":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v18, v0

    aget-object v18, v18, v6

    move-object/from16 v0, v18

    iget-object v13, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->cids:[I

    .line 163
    .local v13, "sourceCidsArray":[I
    const/4 v9, 0x0

    .local v9, "k":I
    :goto_4
    array-length v0, v14

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v9, v0, :cond_2

    .line 159
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 165
    :cond_2
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_5
    if-ge v8, v6, :cond_3

    aget v18, v14, v9

    if-nez v18, :cond_4

    .line 163
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 166
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v18, v0

    aget-object v18, v18, v8

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    move-object/from16 v16, v0

    .line 167
    .local v16, "targetOffsetArray":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v18, v0

    aget-object v18, v18, v8

    move-object/from16 v0, v18

    iget-object v15, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->cids:[I

    .line 170
    .local v15, "targetCidsArray":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    move-object/from16 v18, v0

    aget v19, v14, v9

    .line 169
    invoke-static/range {v18 .. v19}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->stringHashCode(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;I)I

    move-result v18

    .line 171
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v19, v0

    .line 169
    invoke-static/range {v18 .. v19}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->indexFor(II)I

    move-result v10

    .line 172
    .local v10, "newIndex":I
    aget v18, v16, v10

    if-nez v18, :cond_5

    .line 173
    aget v18, v14, v9

    aput v18, v16, v10

    .line 174
    aget v18, v13, v9

    aput v18, v15, v10

    .line 175
    const/16 v18, 0x0

    aput v18, v14, v9

    .line 165
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 182
    .end local v8    # "j":I
    .end local v9    # "k":I
    .end local v10    # "newIndex":I
    .end local v13    # "sourceCidsArray":[I
    .end local v14    # "sourceOffsetArray":[I
    .end local v15    # "targetCidsArray":[I
    .end local v16    # "targetOffsetArray":[I
    :cond_6
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->offsets:[I

    move-object/from16 v18, v0

    aget v11, v18, v6

    .line 183
    .local v11, "offset":I
    if-lez v11, :cond_7

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v11}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->stringHashCode(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;I)I

    move-result v5

    .line 185
    .local v5, "hash":I
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->cids:[I

    move-object/from16 v18, v0

    aget v18, v18, v6

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v5, v1, v11}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->addLabelOffset(III)V

    .line 181
    .end local v5    # "hash":I
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    .line 196
    .end local v11    # "offset":I
    .restart local v7    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;>;"
    .restart local v12    # "oldCollisionMap":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;
    :cond_8
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 197
    .local v4, "e":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    move-object/from16 v18, v0

    iget v0, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->offset:I

    move/from16 v19, v0

    invoke-static/range {v18 .. v19}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->stringHashCode(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;I)I

    move-result v18

    .line 198
    iget v0, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->cid:I

    move/from16 v19, v0

    iget v0, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->offset:I

    move/from16 v20, v0

    .line 197
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->addLabelOffset(III)V

    goto/16 :goto_3
.end method

.method static indexFor(II)I
    .locals 1
    .param p0, "h"    # I
    .param p1, "length"    # I

    .prologue
    .line 266
    add-int/lit8 v0, p1, -0x1

    and-int/2addr v0, p0

    return v0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 103
    new-instance v2, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    invoke-direct {v2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;-><init>()V

    iput-object v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    .line 104
    sget-object v2, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->EMPTY:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    invoke-static {v2, v3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CategoryPathUtils;->serialize(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V

    .line 106
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->capacity:I

    .line 107
    .local v0, "c":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 111
    return-void

    .line 108
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    new-instance v3, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    invoke-direct {v3, v0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;-><init>(I)V

    aput-object v3, v2, v1

    .line 109
    div-int/lit8 v0, v0, 0x2

    .line 107
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static open(Ljava/io/File;FI)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;
    .locals 18
    .param p0, "file"    # Ljava/io/File;
    .param p1, "loadFactor"    # F
    .param p2, "numHashArrays"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 362
    new-instance v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;

    invoke-direct {v8}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;-><init>()V

    .line 363
    .local v8, "l2o":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;
    move/from16 v0, p1

    iput v0, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->loadFactor:F

    .line 364
    move/from16 v0, p2

    new-array v14, v0, [Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    iput-object v14, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    .line 366
    const/4 v4, 0x0

    .line 368
    .local v4, "dis":Ljava/io/DataInputStream;
    :try_start_0
    new-instance v5, Ljava/io/DataInputStream;

    new-instance v14, Ljava/io/BufferedInputStream;

    .line 369
    new-instance v15, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v14, v15}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 368
    invoke-direct {v5, v14}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 374
    .end local v4    # "dis":Ljava/io/DataInputStream;
    .local v5, "dis":Ljava/io/DataInputStream;
    :try_start_1
    invoke-virtual {v5}, Ljava/io/DataInputStream;->readInt()I

    move-result v14

    iput v14, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->counter:I

    .line 376
    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    .line 377
    iget-object v0, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    .line 376
    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    double-to-int v14, v14

    .line 377
    iget v15, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->counter:I

    .line 376
    invoke-static {v14, v15}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->determineCapacity(II)I

    move-result v14

    iput v14, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->capacity:I

    .line 378
    invoke-direct {v8}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->init()V

    .line 381
    invoke-static {v5}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->open(Ljava/io/InputStream;)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    move-result-object v14

    iput-object v14, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    .line 383
    new-instance v14, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    iget-object v15, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    invoke-direct {v14, v15}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;-><init>(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V

    iput-object v14, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    .line 389
    const/4 v2, 0x0

    .line 392
    .local v2, "cid":I
    const/4 v12, 0x1

    .line 393
    .local v12, "offset":I
    move v9, v12

    .local v9, "lastStartOffset":I
    move v13, v12

    .line 397
    .end local v12    # "offset":I
    .local v13, "offset":I
    :goto_0
    iget-object v14, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    invoke-virtual {v14}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->length()I
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v14

    if-lt v13, v14, :cond_1

    .line 423
    if-eqz v5, :cond_0

    .line 424
    invoke-virtual {v5}, Ljava/io/DataInputStream;->close()V

    .line 428
    :cond_0
    iget v14, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->loadFactor:F

    iget v15, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->capacity:I

    int-to-float v15, v15

    mul-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->threshold:I

    .line 429
    return-object v8

    .line 401
    :cond_1
    :try_start_2
    iget-object v14, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    add-int/lit8 v12, v13, 0x1

    .end local v13    # "offset":I
    .restart local v12    # "offset":I
    invoke-virtual {v14, v13}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->charAt(I)C

    move-result v14

    int-to-short v11, v14

    .line 402
    .local v11, "length":I
    move v6, v11

    .line 403
    .local v6, "hash":I
    if-eqz v11, :cond_2

    .line 404
    const/4 v7, 0x0

    .local v7, "i":I
    move v13, v12

    .end local v12    # "offset":I
    .restart local v13    # "offset":I
    :goto_1
    if-lt v7, v11, :cond_3

    move v12, v13

    .line 412
    .end local v7    # "i":I
    .end local v13    # "offset":I
    .restart local v12    # "offset":I
    :cond_2
    ushr-int/lit8 v14, v6, 0x14

    ushr-int/lit8 v15, v6, 0xc

    xor-int/2addr v14, v15

    xor-int/2addr v6, v14

    .line 413
    ushr-int/lit8 v14, v6, 0x7

    xor-int/2addr v14, v6

    ushr-int/lit8 v15, v6, 0x4

    xor-int v6, v14, v15

    .line 415
    invoke-direct {v8, v6, v2, v9}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->addLabelOffset(III)V

    .line 416
    add-int/lit8 v2, v2, 0x1

    .line 417
    move v9, v12

    move v13, v12

    .end local v12    # "offset":I
    .restart local v13    # "offset":I
    goto :goto_0

    .line 405
    .restart local v7    # "i":I
    :cond_3
    iget-object v14, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    add-int/lit8 v12, v13, 0x1

    .end local v13    # "offset":I
    .restart local v12    # "offset":I
    invoke-virtual {v14, v13}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->charAt(I)C

    move-result v14

    int-to-short v10, v14

    .line 406
    .local v10, "len":I
    mul-int/lit8 v14, v6, 0x1f

    iget-object v15, v8, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    add-int v16, v12, v10

    move/from16 v0, v16

    invoke-virtual {v15, v12, v0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Object;->hashCode()I
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v15

    add-int v6, v14, v15

    .line 407
    add-int/2addr v12, v10

    .line 404
    add-int/lit8 v7, v7, 0x1

    move v13, v12

    .end local v12    # "offset":I
    .restart local v13    # "offset":I
    goto :goto_1

    .line 420
    .end local v2    # "cid":I
    .end local v5    # "dis":Ljava/io/DataInputStream;
    .end local v6    # "hash":I
    .end local v7    # "i":I
    .end local v9    # "lastStartOffset":I
    .end local v10    # "len":I
    .end local v11    # "length":I
    .end local v13    # "offset":I
    .restart local v4    # "dis":Ljava/io/DataInputStream;
    :catch_0
    move-exception v3

    .line 421
    .local v3, "cnfe":Ljava/lang/ClassNotFoundException;
    :goto_2
    :try_start_3
    new-instance v14, Ljava/io/IOException;

    const-string v15, "Invalid file format. Cannot deserialize."

    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 422
    .end local v3    # "cnfe":Ljava/lang/ClassNotFoundException;
    :catchall_0
    move-exception v14

    .line 423
    :goto_3
    if-eqz v4, :cond_4

    .line 424
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V

    .line 426
    :cond_4
    throw v14

    .line 422
    .end local v4    # "dis":Ljava/io/DataInputStream;
    .restart local v5    # "dis":Ljava/io/DataInputStream;
    :catchall_1
    move-exception v14

    move-object v4, v5

    .end local v5    # "dis":Ljava/io/DataInputStream;
    .restart local v4    # "dis":Ljava/io/DataInputStream;
    goto :goto_3

    .line 420
    .end local v4    # "dis":Ljava/io/DataInputStream;
    .restart local v5    # "dis":Ljava/io/DataInputStream;
    :catch_1
    move-exception v3

    move-object v4, v5

    .end local v5    # "dis":Ljava/io/DataInputStream;
    .restart local v4    # "dis":Ljava/io/DataInputStream;
    goto :goto_2
.end method

.method static stringHashCode(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    .locals 3
    .param p0, "label"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    .line 284
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->hashCode()I

    move-result v0

    .line 286
    .local v0, "hash":I
    ushr-int/lit8 v1, v0, 0x14

    ushr-int/lit8 v2, v0, 0xc

    xor-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 287
    ushr-int/lit8 v1, v0, 0x7

    xor-int/2addr v1, v0

    ushr-int/lit8 v2, v0, 0x4

    xor-int v0, v1, v2

    .line 289
    return v0
.end method

.method static stringHashCode(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;I)I
    .locals 3
    .param p0, "labelRepository"    # Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    .param p1, "offset"    # I

    .prologue
    .line 294
    invoke-static {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CategoryPathUtils;->hashCodeOfSerialized(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;I)I

    move-result v0

    .line 295
    .local v0, "hash":I
    ushr-int/lit8 v1, v0, 0x14

    ushr-int/lit8 v2, v0, 0xc

    xor-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 296
    ushr-int/lit8 v1, v0, 0x7

    xor-int/2addr v1, v0

    ushr-int/lit8 v2, v0, 0x4

    xor-int v0, v1, v2

    .line 297
    return v0
.end method


# virtual methods
.method public addLabel(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V
    .locals 6
    .param p1, "label"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "ordinal"    # I

    .prologue
    .line 115
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    invoke-virtual {v3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->size()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->threshold:I

    if-le v3, v4, :cond_0

    .line 116
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->grow()V

    .line 119
    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->stringHashCode(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v0

    .line 120
    .local v0, "hash":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    array-length v3, v3

    if-lt v1, v3, :cond_1

    .line 126
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    invoke-virtual {v3, p1, v0, p2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->addLabel(Lorg/apache/lucene/facet/taxonomy/CategoryPath;II)I

    move-result v2

    .line 127
    .local v2, "prevVal":I
    if-eq v2, p2, :cond_2

    .line 128
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Label already exists: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x2f

    invoke-virtual {p1, v5}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " prev ordinal "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 121
    .end local v2    # "prevVal":I
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    aget-object v3, v3, v1

    invoke-direct {p0, v3, p1, v0, p2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->addLabel(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;Lorg/apache/lucene/facet/taxonomy/CategoryPath;II)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 130
    :cond_2
    return-void

    .line 120
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method flush(Ljava/io/File;)V
    .locals 4
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 434
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 437
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-direct {v2, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 439
    .local v2, "os":Ljava/io/BufferedOutputStream;
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 440
    .local v0, "dos":Ljava/io/DataOutputStream;
    iget v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->counter:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 443
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->flush(Ljava/io/OutputStream;)V

    .line 446
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 449
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 451
    return-void

    .line 448
    .end local v0    # "dos":Ljava/io/DataOutputStream;
    .end local v2    # "os":Ljava/io/BufferedOutputStream;
    :catchall_0
    move-exception v3

    .line 449
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 450
    throw v3
.end method

.method getMemoryUsage()I
    .locals 8

    .prologue
    .line 328
    const/4 v3, 0x0

    .line 329
    .local v3, "memoryUsage":I
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    if-eqz v4, :cond_0

    .line 331
    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v6, :cond_3

    .line 336
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    if-eqz v4, :cond_1

    .line 338
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    iget v1, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockSize:I

    .line 340
    .local v1, "blockSize":I
    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v0, v4, 0x4

    .line 341
    .local v0, "actualBlockSize":I
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    iget-object v4, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blocks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/2addr v4, v0

    add-int/2addr v3, v4

    .line 342
    add-int/lit8 v3, v3, 0x8

    .line 344
    .end local v0    # "actualBlockSize":I
    .end local v1    # "blockSize":I
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    if-eqz v4, :cond_2

    .line 345
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    invoke-virtual {v4}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->getMemoryUsage()I

    move-result v4

    add-int/2addr v3, v4

    .line 347
    :cond_2
    return v3

    .line 331
    :cond_3
    aget-object v2, v5, v4

    .line 333
    .local v2, "ha":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;
    iget v7, v2, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;->capacity:I

    mul-int/lit8 v7, v7, 0x2

    mul-int/lit8 v7, v7, 0x4

    add-int/lit8 v7, v7, 0x4

    add-int/2addr v3, v7

    .line 331
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    .locals 4
    .param p1, "label"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    .line 134
    if-nez p1, :cond_1

    .line 135
    const/4 v2, -0x2

    .line 146
    :cond_0
    :goto_0
    return v2

    .line 138
    :cond_1
    invoke-static {p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->stringHashCode(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v0

    .line 139
    .local v0, "hash":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    array-length v3, v3

    if-lt v1, v3, :cond_2

    .line 146
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    invoke-virtual {v3, p1, v0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->get(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)I

    move-result v2

    goto :goto_0

    .line 140
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->hashArrays:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;

    aget-object v3, v3, v1

    invoke-direct {p0, v3, p1, v0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->getOrdinal(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal$HashArray;Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)I

    move-result v2

    .line 141
    .local v2, "ord":I
    const/4 v3, -0x5

    if-ne v2, v3, :cond_0

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public sizeOfMap()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->collisionMap:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->size()I

    move-result v0

    return v0
.end method
