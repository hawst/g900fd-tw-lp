.class Lorg/apache/lucene/facet/complements/TotalFacetCounts$1;
.super Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;
.source "TotalFacetCounts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/complements/TotalFacetCounts;->compute(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)Lorg/apache/lucene/facet/complements/TotalFacetCounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$counts:[[I

.field private final synthetic val$facetIndexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;[[ILorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 0
    .param p1, "$anonymous0"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p2, "$anonymous1"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "$anonymous2"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .prologue
    .line 1
    iput-object p4, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$1;->val$counts:[[I

    iput-object p5, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$1;->val$facetIndexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 162
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V

    return-void
.end method


# virtual methods
.method protected getCategoryListMap(Lorg/apache/lucene/facet/search/FacetArrays;I)Ljava/util/HashMap;
    .locals 5
    .param p1, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p2, "partition"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/search/FacetArrays;",
            "I)",
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/facet/search/CategoryListIterator;",
            "Lorg/apache/lucene/facet/search/Aggregator;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    new-instance v0, Lorg/apache/lucene/facet/search/CountingAggregator;

    iget-object v3, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$1;->val$counts:[[I

    aget-object v3, v3, p2

    invoke-direct {v0, v3}, Lorg/apache/lucene/facet/search/CountingAggregator;-><init>([I)V

    .line 168
    .local v0, "aggregator":Lorg/apache/lucene/facet/search/Aggregator;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 169
    .local v2, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/facet/search/CategoryListIterator;Lorg/apache/lucene/facet/search/Aggregator;>;"
    iget-object v3, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$1;->val$facetIndexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-virtual {v3}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getAllCategoryListParams()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 172
    return-object v2

    .line 169
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 170
    .local v1, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    invoke-virtual {v1, p2}, Lorg/apache/lucene/facet/params/CategoryListParams;->createCategoryListIterator(I)Lorg/apache/lucene/facet/search/CategoryListIterator;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
