.class public final Lorg/apache/lucene/facet/encoding/UniqueValuesIntEncoder;
.super Lorg/apache/lucene/facet/encoding/IntEncoderFilter;
.source "UniqueValuesIntEncoder.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/encoding/IntEncoder;)V
    .locals 0
    .param p1, "encoder"    # Lorg/apache/lucene/facet/encoding/IntEncoder;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/encoding/IntEncoderFilter;-><init>(Lorg/apache/lucene/facet/encoding/IntEncoder;)V

    .line 36
    return-void
.end method


# virtual methods
.method public createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/facet/encoding/UniqueValuesIntEncoder;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/encoding/IntEncoder;->createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;

    move-result-object v0

    return-object v0
.end method

.method public encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 7
    .param p1, "values"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "buf"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 40
    iget-object v5, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v6, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    aget v3, v5, v6

    .line 41
    .local v3, "prev":I
    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int/lit8 v1, v5, 0x1

    .line 42
    .local v1, "idx":I
    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v6, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v4, v5, v6

    .line 43
    .local v4, "upto":I
    move v0, v1

    .local v0, "i":I
    move v2, v1

    .end local v1    # "idx":I
    .local v2, "idx":I
    :goto_0
    if-lt v0, v4, :cond_0

    .line 49
    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    sub-int v5, v2, v5

    iput v5, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 50
    iget-object v5, p0, Lorg/apache/lucene/facet/encoding/UniqueValuesIntEncoder;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    invoke-virtual {v5, p1, p2}, Lorg/apache/lucene/facet/encoding/IntEncoder;->encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 51
    return-void

    .line 44
    :cond_0
    iget-object v5, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v5, v5, v0

    if-eq v5, v3, :cond_1

    .line 45
    iget-object v5, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    iget-object v6, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v6, v6, v0

    aput v6, v5, v2

    .line 46
    iget-object v5, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v3, v5, v0

    .line 43
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_0

    :cond_1
    move v1, v2

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unique("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/facet/encoding/UniqueValuesIntEncoder;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
