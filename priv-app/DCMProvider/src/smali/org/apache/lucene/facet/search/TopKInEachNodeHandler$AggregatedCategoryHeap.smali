.class Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategoryHeap;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "TopKInEachNodeHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AggregatedCategoryHeap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;",
        ">;"
    }
.end annotation


# instance fields
.field private merger:Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;


# direct methods
.method public constructor <init>(ILorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;)V
    .locals 0
    .param p1, "size"    # I
    .param p2, "merger"    # Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;

    .prologue
    .line 502
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 503
    iput-object p2, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategoryHeap;->merger:Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;

    .line 504
    return-void
.end method


# virtual methods
.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;

    check-cast p2, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategoryHeap;->lessThan(Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;)Z

    move-result v0

    return v0
.end method

.method protected lessThan(Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;)Z
    .locals 7
    .param p1, "arg1"    # Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;
    .param p2, "arg2"    # Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;

    .prologue
    .line 508
    iget-object v0, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategoryHeap;->merger:Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;

    iget v1, p2, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;->ordinal:I

    iget-wide v2, p2, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;->value:D

    iget v4, p1, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;->ordinal:I

    iget-wide v5, p1, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;->value:D

    invoke-virtual/range {v0 .. v6}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;->leftGoesNow(IDID)Z

    move-result v0

    return v0
.end method
