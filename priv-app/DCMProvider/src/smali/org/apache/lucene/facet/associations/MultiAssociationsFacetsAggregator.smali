.class public Lorg/apache/lucene/facet/associations/MultiAssociationsFacetsAggregator;
.super Ljava/lang/Object;
.source "MultiAssociationsFacetsAggregator.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/FacetsAggregator;


# instance fields
.field private final aggregators:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetsAggregator;",
            ">;"
        }
    .end annotation
.end field

.field private final categoryAggregators:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            "Lorg/apache/lucene/facet/search/FacetsAggregator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            "Lorg/apache/lucene/facet/search/FacetsAggregator;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "aggregators":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/search/FacetsAggregator;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lorg/apache/lucene/facet/associations/MultiAssociationsFacetsAggregator;->categoryAggregators:Ljava/util/Map;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 64
    .local v0, "aggsClasses":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<+Lorg/apache/lucene/facet/search/FacetsAggregator;>;Lorg/apache/lucene/facet/search/FacetsAggregator;>;"
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 67
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lorg/apache/lucene/facet/associations/MultiAssociationsFacetsAggregator;->aggregators:Ljava/util/List;

    .line 68
    return-void

    .line 64
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/search/FacetsAggregator;

    .line 65
    .local v1, "fa":Lorg/apache/lucene/facet/search/FacetsAggregator;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 3
    .param p1, "matchingDocs"    # Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    .param p2, "clp"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v1, p0, Lorg/apache/lucene/facet/associations/MultiAssociationsFacetsAggregator;->aggregators:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 75
    return-void

    .line 72
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetsAggregator;

    .line 73
    .local v0, "fa":Lorg/apache/lucene/facet/search/FacetsAggregator;
    invoke-interface {v0, p1, p2, p3}, Lorg/apache/lucene/facet/search/FacetsAggregator;->aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V

    goto :goto_0
.end method

.method public requiresDocScores()Z
    .locals 3

    .prologue
    .line 84
    iget-object v1, p0, Lorg/apache/lucene/facet/associations/MultiAssociationsFacetsAggregator;->aggregators:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 89
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 84
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetsAggregator;

    .line 85
    .local v0, "fa":Lorg/apache/lucene/facet/search/FacetsAggregator;
    invoke-interface {v0}, Lorg/apache/lucene/facet/search/FacetsAggregator;->requiresDocScores()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public rollupValues(Lorg/apache/lucene/facet/search/FacetRequest;I[I[ILorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 6
    .param p1, "fr"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p2, "ordinal"    # I
    .param p3, "children"    # [I
    .param p4, "siblings"    # [I
    .param p5, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/facet/associations/MultiAssociationsFacetsAggregator;->categoryAggregators:Ljava/util/Map;

    iget-object v1, p1, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetsAggregator;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lorg/apache/lucene/facet/search/FacetsAggregator;->rollupValues(Lorg/apache/lucene/facet/search/FacetRequest;I[I[ILorg/apache/lucene/facet/search/FacetArrays;)V

    .line 80
    return-void
.end method
