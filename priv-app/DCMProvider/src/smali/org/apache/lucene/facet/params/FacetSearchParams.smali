.class public Lorg/apache/lucene/facet/params/FacetSearchParams;
.super Ljava/lang/Object;
.source "FacetSearchParams.java"


# instance fields
.field public final facetRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetRequest;",
            ">;"
        }
    .end annotation
.end field

.field public final indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "facetRequests":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetRequest;>;"
    sget-object v0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->DEFAULT:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/facet/params/FacetSearchParams;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Ljava/util/List;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Ljava/util/List;)V
    .locals 2
    .param p1, "indexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/params/FacetIndexingParams;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p2, "facetRequests":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetRequest;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 75
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "at least one FacetRequest must be defined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_1
    iput-object p2, p0, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    .line 78
    iput-object p1, p0, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 79
    return-void
.end method

.method public varargs constructor <init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;[Lorg/apache/lucene/facet/search/FacetRequest;)V
    .locals 1
    .param p1, "indexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p2, "facetRequests"    # [Lorg/apache/lucene/facet/search/FacetRequest;

    .prologue
    .line 66
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/params/FacetSearchParams;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Ljava/util/List;)V

    .line 67
    return-void
.end method

.method public varargs constructor <init>([Lorg/apache/lucene/facet/search/FacetRequest;)V
    .locals 2
    .param p1, "facetRequests"    # [Lorg/apache/lucene/facet/search/FacetRequest;

    .prologue
    .line 48
    sget-object v0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->DEFAULT:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/facet/params/FacetSearchParams;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Ljava/util/List;)V

    .line 49
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0xa

    .line 83
    const-string v0, "  "

    .line 84
    .local v0, "INDENT":Ljava/lang/String;
    const/16 v1, 0xa

    .line 86
    .local v1, "NEWLINE":C
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "IndexingParams: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 87
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "FacetRequests:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    iget-object v4, p0, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 94
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 90
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 91
    .local v2, "facetRequest":Lorg/apache/lucene/facet/search/FacetRequest;
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
