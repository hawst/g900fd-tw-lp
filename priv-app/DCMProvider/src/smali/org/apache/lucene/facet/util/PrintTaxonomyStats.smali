.class public Lorg/apache/lucene/facet/util/PrintTaxonomyStats;
.super Ljava/lang/Object;
.source "PrintTaxonomyStats.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static countAllChildren(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;I)I
    .locals 4
    .param p0, "r"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p1, "ord"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    const/4 v1, 0x0

    .line 78
    .local v1, "count":I
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getChildren(I)Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;

    move-result-object v2

    .line 80
    .local v2, "it":Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;
    :goto_0
    invoke-virtual {v2}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;->next()I

    move-result v0

    .local v0, "child":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 83
    return v1

    .line 81
    :cond_0
    invoke-static {p0, v0}, Lorg/apache/lucene/facet/util/PrintTaxonomyStats;->countAllChildren(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v3

    goto :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 8
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 37
    const/4 v3, 0x0

    .line 38
    .local v3, "printTree":Z
    const/4 v2, 0x0

    .line 39
    .local v2, "path":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, p0

    if-lt v1, v5, :cond_1

    .line 46
    array-length v7, p0

    if-eqz v3, :cond_3

    const/4 v5, 0x2

    :goto_1
    if-eq v7, v5, :cond_0

    .line 47
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "\nUsage: java -classpath ... org.apache.lucene.facet.util.PrintTaxonomyStats [-printTree] /path/to/taxononmy/index\n"

    invoke-virtual {v5, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 48
    invoke-static {v6}, Ljava/lang/System;->exit(I)V

    .line 50
    :cond_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v0

    .line 51
    .local v0, "dir":Lorg/apache/lucene/store/Directory;
    new-instance v4, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    invoke-direct {v4, v0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;-><init>(Lorg/apache/lucene/store/Directory;)V

    .line 52
    .local v4, "r":Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v4, v5, v3}, Lorg/apache/lucene/facet/util/PrintTaxonomyStats;->printStats(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Ljava/io/PrintStream;Z)V

    .line 53
    invoke-virtual {v4}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->close()V

    .line 54
    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 55
    return-void

    .line 40
    .end local v0    # "dir":Lorg/apache/lucene/store/Directory;
    .end local v4    # "r":Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    :cond_1
    aget-object v5, p0, v1

    const-string v7, "-printTree"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 41
    const/4 v3, 0x1

    .line 39
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    :cond_2
    aget-object v2, p0, v1

    goto :goto_2

    :cond_3
    move v5, v6

    .line 46
    goto :goto_1
.end method

.method private static printAllChildren(Ljava/io/PrintStream;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;ILjava/lang/String;I)V
    .locals 4
    .param p0, "out"    # Ljava/io/PrintStream;
    .param p1, "r"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p2, "ord"    # I
    .param p3, "indent"    # Ljava/lang/String;
    .param p4, "depth"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p1, p2}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getChildren(I)Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;

    move-result-object v1

    .line 89
    .local v1, "it":Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;->next()I

    move-result v0

    .local v0, "child":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 93
    return-void

    .line 90
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, v0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getPath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v3

    iget-object v3, v3, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v3, v3, p4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 91
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v3, p4, 0x1

    invoke-static {p0, p1, v0, v2, v3}, Lorg/apache/lucene/facet/util/PrintTaxonomyStats;->printAllChildren(Ljava/io/PrintStream;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;ILjava/lang/String;I)V

    goto :goto_0
.end method

.method public static printStats(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Ljava/io/PrintStream;Z)V
    .locals 8
    .param p0, "r"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p1, "out"    # Ljava/io/PrintStream;
    .param p2, "printTree"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    .line 58
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getSize()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " total categories."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 60
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getChildren(I)Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;

    move-result-object v3

    .line 62
    .local v3, "it":Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;
    :cond_0
    :goto_0
    invoke-virtual {v3}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;->next()I

    move-result v0

    .local v0, "child":I
    if-ne v0, v7, :cond_1

    .line 74
    return-void

    .line 63
    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getChildren(I)Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;

    move-result-object v1

    .line 64
    .local v1, "chilrenIt":Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;
    const/4 v4, 0x0

    .line 65
    .local v4, "numImmediateChildren":I
    :goto_1
    invoke-virtual {v1}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;->next()I

    move-result v5

    if-ne v5, v7, :cond_2

    .line 68
    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getPath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v2

    .line 69
    .local v2, "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " immediate children; "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p0, v0}, Lorg/apache/lucene/facet/util/PrintTaxonomyStats;->countAllChildren(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " total categories"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 70
    if-eqz p2, :cond_0

    .line 71
    const-string v5, "  "

    const/4 v6, 0x1

    invoke-static {p1, p0, v0, v5, v6}, Lorg/apache/lucene/facet/util/PrintTaxonomyStats;->printAllChildren(Ljava/io/PrintStream;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;ILjava/lang/String;I)V

    goto :goto_0

    .line 66
    .end local v2    # "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
