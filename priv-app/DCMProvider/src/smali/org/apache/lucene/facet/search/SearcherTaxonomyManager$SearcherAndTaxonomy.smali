.class public Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;
.super Ljava/lang/Object;
.source "SearcherTaxonomyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearcherAndTaxonomy"
.end annotation


# instance fields
.field public final searcher:Lorg/apache/lucene/search/IndexSearcher;

.field public final taxonomyReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;)V
    .locals 0
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .param p2, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 52
    iput-object p2, p0, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    .line 53
    return-void
.end method
