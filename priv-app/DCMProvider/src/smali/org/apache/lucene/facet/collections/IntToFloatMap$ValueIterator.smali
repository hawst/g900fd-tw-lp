.class final Lorg/apache/lucene/facet/collections/IntToFloatMap$ValueIterator;
.super Ljava/lang/Object;
.source "IntToFloatMap.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/FloatIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/IntToFloatMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ValueIterator"
.end annotation


# instance fields
.field private iterator:Lorg/apache/lucene/facet/collections/IntIterator;

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/IntToFloatMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/collections/IntToFloatMap;)V
    .locals 1

    .prologue
    .line 136
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToFloatMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToFloatMap$IndexIterator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/collections/IntToFloatMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToFloatMap;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    .line 136
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()F
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToFloatMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->values:[F

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->remove()V

    .line 151
    return-void
.end method
