.class public final Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;
.super Ljava/lang/Object;
.source "TotalFacetCountsCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;
    }
.end annotation


# static fields
.field public static final DEFAULT_CACHE_SIZE:I = 0x2

.field private static final singleton:Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;


# instance fields
.field private cache:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;",
            "Lorg/apache/lucene/facet/complements/TotalFacetCounts;",
            ">;"
        }
    .end annotation
.end field

.field private lruKeys:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;",
            ">;"
        }
    .end annotation
.end field

.field private maxCacheSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;

    invoke-direct {v0}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;-><init>()V

    sput-object v0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->singleton:Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->cache:Ljava/util/concurrent/ConcurrentHashMap;

    .line 72
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->lruKeys:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 74
    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->maxCacheSize:I

    .line 78
    return-void
.end method

.method private declared-synchronized computeAndCache(Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;)Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    .locals 4
    .param p1, "key"    # Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->cache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    .line 156
    .local v0, "tfc":Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    if-nez v0, :cond_0

    .line 157
    iget-object v1, p1, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->indexReader:Lorg/apache/lucene/index/IndexReader;

    iget-object v2, p1, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->taxonomy:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v3, p1, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->facetIndexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->compute(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->lruKeys:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 159
    iget-object v1, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->cache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    invoke-direct {p0}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->trimCache()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    :cond_0
    monitor-exit p0

    return-object v0

    .line 155
    .end local v0    # "tfc":Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public static getSingleton()Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->singleton:Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;

    return-object v0
.end method

.method private markRecentlyUsed(Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;)V
    .locals 1
    .param p1, "key"    # Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->lruKeys:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->lruKeys:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 133
    return-void
.end method

.method private declared-synchronized trimCache()V
    .locals 3

    .prologue
    .line 137
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->cache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->maxCacheSize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt v1, v2, :cond_0

    .line 147
    monitor-exit p0

    return-void

    .line 138
    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->lruKeys:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;

    .line 139
    .local v0, "key":Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;
    if-nez v0, :cond_1

    .line 141
    iget-object v1, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->cache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keys()Ljava/util/Enumeration;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "key":Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;
    check-cast v0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;

    .line 145
    .restart local v0    # "key":Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->cache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 137
    .end local v0    # "key":Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->cache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 273
    iget-object v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->lruKeys:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    monitor-exit p0

    return-void

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getCacheSize()I
    .locals 1

    .prologue
    .line 280
    iget v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->maxCacheSize:I

    return v0
.end method

.method public getTotalCounts(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    .locals 3
    .param p1, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "taxonomy"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p3, "facetIndexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    new-instance v0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V

    .line 99
    .local v0, "key":Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;
    iget-object v2, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->cache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    .line 100
    .local v1, "tfc":Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    if-eqz v1, :cond_0

    .line 101
    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->markRecentlyUsed(Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;)V

    .line 104
    .end local v1    # "tfc":Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    :goto_0
    return-object v1

    .restart local v1    # "tfc":Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    :cond_0
    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->computeAndCache(Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;)Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    move-result-object v1

    goto :goto_0
.end method

.method public declared-synchronized load(Ljava/io/File;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 5
    .param p1, "inputFile"    # Ljava/io/File;
    .param p2, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "taxonomy"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p4, "facetIndexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v2

    if-nez v2, :cond_1

    .line 186
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exepecting an existing readable file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 188
    :cond_1
    :try_start_1
    new-instance v0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;

    invoke-direct {v0, p2, p3, p4}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V

    .line 189
    .local v0, "key":Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;
    invoke-static {p1, p3, p4}, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->loadFromFile(Ljava/io/File;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    move-result-object v1

    .line 190
    .local v1, "tfc":Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    iget-object v2, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->cache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    invoke-direct {p0}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->trimCache()V

    .line 192
    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->markRecentlyUsed(Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 193
    monitor-exit p0

    return-void
.end method

.method public setCacheSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 292
    const/4 v1, 0x1

    if-ge p1, v1, :cond_0

    const/4 p1, 0x1

    .line 293
    :cond_0
    iget v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->maxCacheSize:I

    .line 294
    .local v0, "origSize":I
    iput p1, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->maxCacheSize:I

    .line 295
    iget v1, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->maxCacheSize:I

    if-ge v1, v0, :cond_1

    .line 296
    invoke-direct {p0}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->trimCache()V

    .line 298
    :cond_1
    return-void
.end method

.method public store(Ljava/io/File;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 5
    .param p1, "outputFile"    # Ljava/io/File;
    .param p2, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "taxonomy"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p4, "facetIndexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 219
    .local v0, "parentFile":Ljava/io/File;
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 220
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-nez v2, :cond_2

    .line 222
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exepecting a writable file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 224
    :cond_2
    invoke-virtual {p0, p2, p3, p4}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->getTotalCounts(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    move-result-object v1

    .line 225
    .local v1, "tfc":Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    invoke-static {p1, v1}, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->storeToFile(Ljava/io/File;Lorg/apache/lucene/facet/complements/TotalFacetCounts;)V

    .line 226
    return-void
.end method
