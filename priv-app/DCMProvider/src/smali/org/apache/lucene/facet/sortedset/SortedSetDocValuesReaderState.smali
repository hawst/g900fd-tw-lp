.class public final Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;
.super Ljava/lang/Object;
.source "SortedSetDocValuesReaderState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;
    }
.end annotation


# static fields
.field public static final FACET_FIELD_EXTENSION:Ljava/lang/String; = "_sorted_doc_values"


# instance fields
.field private final field:Ljava/lang/String;

.field private final prefixToOrdRange:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;",
            ">;"
        }
    .end annotation
.end field

.field final separator:C

.field final separatorRegex:Ljava/lang/String;

.field private final topReader:Lorg/apache/lucene/index/AtomicReader;

.field private final valueCount:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/index/IndexReader;)V
    .locals 12
    .param p1, "fip"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->prefixToOrdRange:Ljava/util/Map;

    .line 91
    new-instance v6, Ljava/lang/StringBuilder;

    const/4 v7, 0x0

    invoke-virtual {p1, v7}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v7

    iget-object v7, v7, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "_sorted_doc_values"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->field:Ljava/lang/String;

    .line 92
    invoke-virtual {p1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getFacetDelimChar()C

    move-result v6

    iput-char v6, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->separator:C

    .line 93
    iget-char v6, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->separator:C

    invoke-static {v6}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->separatorRegex:Ljava/lang/String;

    .line 97
    instance-of v6, p2, Lorg/apache/lucene/index/AtomicReader;

    if-eqz v6, :cond_0

    .line 98
    check-cast p2, Lorg/apache/lucene/index/AtomicReader;

    .end local p2    # "reader":Lorg/apache/lucene/index/IndexReader;
    iput-object p2, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->topReader:Lorg/apache/lucene/index/AtomicReader;

    .line 102
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->topReader:Lorg/apache/lucene/index/AtomicReader;

    iget-object v7, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->field:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v1

    .line 103
    .local v1, "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    if-nez v1, :cond_1

    .line 104
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "field \""

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->field:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\" was not indexed with SortedSetDocValues"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 100
    .end local v1    # "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    .restart local p2    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    new-instance v6, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;

    check-cast p2, Lorg/apache/lucene/index/CompositeReader;

    .end local p2    # "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-direct {v6, p2}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;-><init>(Lorg/apache/lucene/index/CompositeReader;)V

    iput-object v6, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->topReader:Lorg/apache/lucene/index/AtomicReader;

    goto :goto_0

    .line 106
    .restart local v1    # "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    :cond_1
    invoke-virtual {v1}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v6

    const-wide/32 v8, 0x7fffffff

    cmp-long v6, v6, v8

    if-lez v6, :cond_2

    .line 107
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "can only handle valueCount < Integer.MAX_VALUE; got "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 109
    :cond_2
    invoke-virtual {v1}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v6

    long-to-int v6, v6

    iput v6, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->valueCount:I

    .line 114
    const/4 v2, 0x0

    .line 115
    .local v2, "lastDim":Ljava/lang/String;
    const/4 v5, -0x1

    .line 116
    .local v5, "startOrd":I
    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 122
    .local v4, "spare":Lorg/apache/lucene/util/BytesRef;
    const/4 v3, 0x0

    .local v3, "ord":I
    :goto_1
    iget v6, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->valueCount:I

    if-lt v3, v6, :cond_4

    .line 137
    if-eqz v2, :cond_3

    .line 138
    iget-object v6, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->prefixToOrdRange:Ljava/util/Map;

    new-instance v7, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;

    iget v8, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->valueCount:I

    add-int/lit8 v8, v8, -0x1

    invoke-direct {v7, v5, v8}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;-><init>(II)V

    invoke-interface {v6, v2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    :cond_3
    return-void

    .line 123
    :cond_4
    int-to-long v6, v3

    invoke-virtual {v1, v6, v7, v4}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupOrd(JLorg/apache/lucene/util/BytesRef;)V

    .line 124
    invoke-virtual {v4}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->separatorRegex:Ljava/lang/String;

    invoke-virtual {v6, v7, v11}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "components":[Ljava/lang/String;
    array-length v6, v0

    if-eq v6, v11, :cond_5

    .line 126
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "this class can only handle 2 level hierarchy (dim/value); got: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 128
    :cond_5
    aget-object v6, v0, v10

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 129
    if-eqz v2, :cond_6

    .line 130
    iget-object v6, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->prefixToOrdRange:Ljava/util/Map;

    new-instance v7, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;

    add-int/lit8 v8, v3, -0x1

    invoke-direct {v7, v5, v8}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;-><init>(II)V

    invoke-interface {v6, v2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    :cond_6
    move v5, v3

    .line 133
    aget-object v2, v0, v10

    .line 122
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    sget-object v0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->DEFAULT:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/index/IndexReader;)V

    .line 84
    return-void
.end method


# virtual methods
.method getDocValues()Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->topReader:Lorg/apache/lucene/index/AtomicReader;

    iget-object v1, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v0

    return-object v0
.end method

.method getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->field:Ljava/lang/String;

    return-object v0
.end method

.method getOrdRange(Ljava/lang/String;)Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;
    .locals 1
    .param p1, "dim"    # Ljava/lang/String;

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->prefixToOrdRange:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;

    return-object v0
.end method

.method getSize()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->valueCount:I

    return v0
.end method
