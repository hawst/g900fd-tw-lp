.class public Lorg/apache/lucene/facet/collections/IntHashSet;
.super Ljava/lang/Object;
.source "IntHashSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;,
        Lorg/apache/lucene/facet/collections/IntHashSet$KeyIterator;
    }
.end annotation


# static fields
.field private static defaultCapacity:I


# instance fields
.field baseHash:[I

.field private capacity:I

.field private firstEmpty:I

.field private hashFactor:I

.field keys:[I

.field next:[I

.field private prev:I

.field private size:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    const/16 v0, 0x10

    sput v0, Lorg/apache/lucene/facet/collections/IntHashSet;->defaultCapacity:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 174
    sget v0, Lorg/apache/lucene/facet/collections/IntHashSet;->defaultCapacity:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/collections/IntHashSet;-><init>(I)V

    .line 175
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1, "capacity"    # I

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    const/16 v2, 0x10

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    .line 187
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    if-lt v2, p1, :cond_0

    .line 194
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    add-int/lit8 v0, v2, 0x1

    .line 196
    .local v0, "arrayLength":I
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->keys:[I

    .line 197
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    .line 200
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    shl-int/lit8 v1, v2, 0x1

    .line 202
    .local v1, "baseHashSize":I
    new-array v2, v1, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    .line 206
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->hashFactor:I

    .line 208
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    .line 210
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntHashSet;->clear()V

    .line 211
    return-void

    .line 189
    .end local v0    # "arrayLength":I
    .end local v1    # "baseHashSize":I
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    goto :goto_0
.end method

.method private findForRemove(II)I
    .locals 3
    .param p1, "key"    # I
    .param p2, "baseHashIndex"    # I

    .prologue
    const/4 v1, 0x0

    .line 324
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->prev:I

    .line 325
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    aget v0, v2, p2

    .line 328
    .local v0, "index":I
    :goto_0
    if-nez v0, :cond_1

    .line 341
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->prev:I

    move v0, v1

    .line 342
    .end local v0    # "index":I
    :cond_0
    return v0

    .line 330
    .restart local v0    # "index":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->keys:[I

    aget v2, v2, v0

    if-eq v2, p1, :cond_0

    .line 335
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->prev:I

    .line 336
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    aget v0, v2, v0

    goto :goto_0
.end method

.method private getBaseHashAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private prvt_add(I)V
    .locals 4
    .param p1, "key"    # I

    .prologue
    .line 225
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntHashSet;->calcBaseHashIndex(I)I

    move-result v0

    .line 228
    .local v0, "hashIndex":I
    iget v1, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->firstEmpty:I

    .line 231
    .local v1, "objectIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->firstEmpty:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->firstEmpty:I

    .line 232
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->keys:[I

    aput p1, v2, v1

    .line 235
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 236
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    aput v1, v2, v0

    .line 239
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    .line 240
    return-void
.end method


# virtual methods
.method public add(I)Z
    .locals 4
    .param p1, "value"    # I

    .prologue
    const/4 v3, 0x1

    .line 412
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntHashSet;->find(I)I

    move-result v0

    .line 415
    .local v0, "index":I
    if-eqz v0, :cond_0

    .line 429
    :goto_0
    return v3

    .line 420
    :cond_0
    iget v1, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    iget v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    if-ne v1, v2, :cond_1

    .line 422
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntHashSet;->grow()V

    .line 427
    :cond_1
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/collections/IntHashSet;->prvt_add(I)V

    goto :goto_0
.end method

.method protected calcBaseHashIndex(I)I
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 247
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->hashFactor:I

    and-int/2addr v0, p1

    return v0
.end method

.method public clear()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 255
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 258
    iput v4, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    .line 263
    const/4 v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->firstEmpty:I

    .line 267
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    if-lt v0, v2, :cond_0

    .line 272
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    aput v4, v2, v3

    .line 273
    return-void

    .line 268
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput v1, v2, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public contains(I)Z
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 283
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntHashSet;->find(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected find(I)I
    .locals 3
    .param p1, "key"    # I

    .prologue
    .line 293
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntHashSet;->calcBaseHashIndex(I)I

    move-result v0

    .line 296
    .local v0, "baseHashIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    aget v1, v2, v0

    .line 299
    .local v1, "localIndex":I
    :goto_0
    if-nez v1, :cond_1

    .line 311
    const/4 v1, 0x0

    .end local v1    # "localIndex":I
    :cond_0
    return v1

    .line 301
    .restart local v1    # "localIndex":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->keys:[I

    aget v2, v2, v1

    if-eq v2, p1, :cond_0

    .line 306
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    aget v1, v2, v1

    goto :goto_0
.end method

.method protected grow()V
    .locals 4

    .prologue
    .line 350
    new-instance v2, Lorg/apache/lucene/facet/collections/IntHashSet;

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Lorg/apache/lucene/facet/collections/IntHashSet;-><init>(I)V

    .line 355
    .local v2, "that":Lorg/apache/lucene/facet/collections/IntHashSet;
    new-instance v1, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;

    invoke-direct {v1, p0}, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/IntHashSet;)V

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 366
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->capacity:I

    .line 367
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    .line 368
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntHashSet;->firstEmpty:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->firstEmpty:I

    .line 369
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntHashSet;->keys:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->keys:[I

    .line 370
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    .line 371
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    .line 372
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntHashSet;->hashFactor:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->hashFactor:I

    .line 373
    return-void

    .line 356
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->next()I

    move-result v0

    .line 357
    .local v0, "index":I
    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->keys:[I

    aget v3, v3, v0

    invoke-direct {v2, v3}, Lorg/apache/lucene/facet/collections/IntHashSet;->prvt_add(I)V

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 380
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Lorg/apache/lucene/facet/collections/IntIterator;
    .locals 1

    .prologue
    .line 387
    new-instance v0, Lorg/apache/lucene/facet/collections/IntHashSet$KeyIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/IntHashSet$KeyIterator;-><init>(Lorg/apache/lucene/facet/collections/IntHashSet;)V

    return-object v0
.end method

.method public remove(I)Z
    .locals 5
    .param p1, "value"    # I

    .prologue
    .line 442
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntHashSet;->calcBaseHashIndex(I)I

    move-result v0

    .line 443
    .local v0, "baseHashIndex":I
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/collections/IntHashSet;->findForRemove(II)I

    move-result v1

    .line 444
    .local v1, "index":I
    if-eqz v1, :cond_1

    .line 447
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->prev:I

    if-nez v2, :cond_0

    .line 448
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    aget v3, v3, v1

    aput v3, v2, v0

    .line 451
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->prev:I

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    aget v4, v4, v1

    aput v4, v2, v3

    .line 452
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->firstEmpty:I

    aput v3, v2, v1

    .line 453
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->firstEmpty:I

    .line 454
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    .line 455
    const/4 v2, 0x1

    .line 458
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 465
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    return v0
.end method

.method public toArray()[I
    .locals 4

    .prologue
    .line 474
    const/4 v2, -0x1

    .line 475
    .local v2, "j":I
    iget v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    new-array v0, v3, [I

    .line 478
    .local v0, "array":[I
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntHashSet;->iterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v1

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/IntIterator;
    :goto_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 481
    return-object v0

    .line 479
    :cond_0
    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v3

    aput v3, v0, v2

    goto :goto_0
.end method

.method public toArray([I)[I
    .locals 4
    .param p1, "a"    # [I

    .prologue
    .line 496
    const/4 v1, 0x0

    .line 497
    .local v1, "j":I
    array-length v2, p1

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    if-ge v2, v3, :cond_0

    .line 498
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->size:I

    new-array p1, v2, [I

    .line 501
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntHashSet;->iterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v0

    .local v0, "iterator":Lorg/apache/lucene/facet/collections/IntIterator;
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 502
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 505
    :cond_1
    return-object p1

    .line 503
    :cond_2
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v2

    aput v2, p1, v1

    .line 502
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public toHashString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 529
    const-string v5, "\n"

    .line 530
    .local v5, "string":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 532
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    array-length v6, v6

    if-lt v0, v6, :cond_0

    .line 546
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 533
    :cond_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 534
    .local v3, "sb2":Ljava/lang/StringBuffer;
    const/4 v4, 0x0

    .line 535
    .local v4, "shouldAppend":Z
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, ".\t"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 536
    iget-object v6, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    aget v1, v6, v0

    .local v1, "index":I
    :goto_1
    if-nez v1, :cond_2

    .line 540
    if-eqz v4, :cond_1

    .line 541
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 542
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 532
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 537
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " -> "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->keys:[I

    aget v7, v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "@"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 538
    const/4 v4, 0x1

    .line 536
    iget-object v6, p0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    aget v1, v6, v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 514
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 515
    .local v1, "sb":Ljava/lang/StringBuffer;
    const/16 v2, 0x7b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 516
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntHashSet;->iterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v0

    .line 517
    .local v0, "iterator":Lorg/apache/lucene/facet/collections/IntIterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 524
    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 525
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 518
    :cond_1
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 519
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 520
    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 521
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
