.class Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "MatchingDocsAsScoredDocIDs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    .line 117
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    # getter for: Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->this$0:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;
    invoke-static {v0}, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->access$0(Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;)Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;

    move-result-object v0

    iget v0, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;->size:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget v0, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    iget-object v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v1, v1, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v1, v1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    iget v1, v1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    add-int/2addr v0, v1

    return v0
.end method

.method public nextDoc()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const v1, 0x7fffffff

    const/4 v4, -0x1

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-boolean v0, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->done:Z

    if-eqz v0, :cond_3

    move v0, v1

    .line 147
    :goto_0
    return v0

    .line 126
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v0, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->mdIter:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->done:Z

    move v0, v1

    .line 128
    goto :goto_0

    .line 130
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v0, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->mdIter:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iput-object v0, v2, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v2, v2, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v2, v2, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v2}, Lorg/apache/lucene/util/FixedBitSet;->length()I

    move-result v2

    iput v2, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->currentLength:I

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    const/4 v2, 0x0

    iput v2, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    .line 134
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget v0, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget v2, v2, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->currentLength:I

    if-ge v0, v2, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v2, v2, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v2, v2, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget v3, v3, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v2

    iput v2, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    if-ne v2, v4, :cond_5

    .line 135
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iput-object v5, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    .line 125
    :cond_3
    :goto_1
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v0, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget v1, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    .line 142
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget v0, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    iget-object v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget v1, v1, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->currentLength:I

    if-ge v0, v1, :cond_4

    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v1, v1, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v1, v1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget v2, v2, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    if-ne v1, v4, :cond_6

    .line 143
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iput-object v5, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    .line 144
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->nextDoc()I

    move-result v0

    goto/16 :goto_0

    .line 137
    :cond_5
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iput v4, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    goto :goto_1

    .line 147
    :cond_6
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget v0, v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->doc:I

    iget-object v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2$1;->this$1:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    iget-object v1, v1, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v1, v1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    iget v1, v1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    add-int/2addr v0, v1

    goto/16 :goto_0
.end method
