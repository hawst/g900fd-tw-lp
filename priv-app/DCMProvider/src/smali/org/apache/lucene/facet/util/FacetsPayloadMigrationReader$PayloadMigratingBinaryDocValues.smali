.class Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "FacetsPayloadMigrationReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PayloadMigratingBinaryDocValues"
.end annotation


# instance fields
.field private curDocID:I

.field private dpe:Lorg/apache/lucene/index/DocsAndPositionsEnum;

.field private fields:Lorg/apache/lucene/index/Fields;

.field private lastRequestedDocID:I

.field private term:Lorg/apache/lucene/index/Term;

.field final synthetic this$0:Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader;Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p2, "fields"    # Lorg/apache/lucene/index/Fields;
    .param p3, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 105
    iput-object p1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->this$0:Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    .line 83
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->curDocID:I

    .line 106
    iput-object p2, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->fields:Lorg/apache/lucene/index/Fields;

    .line 107
    iput-object p3, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->term:Lorg/apache/lucene/index/Term;

    .line 108
    invoke-direct {p0}, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->getDPE()Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->dpe:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 109
    iget-object v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->dpe:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    if-nez v1, :cond_0

    .line 110
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->curDocID:I

    .line 118
    :goto_0
    return-void

    .line 113
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->dpe:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->curDocID:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private getDPE()Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 7

    .prologue
    .line 88
    const/4 v0, 0x0

    .line 89
    .local v0, "dpe":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->fields:Lorg/apache/lucene/index/Fields;

    if-eqz v4, :cond_0

    .line 90
    iget-object v4, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->fields:Lorg/apache/lucene/index/Fields;

    iget-object v5, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v5}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v3

    .line 91
    .local v3, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v3, :cond_0

    .line 92
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v2

    .line 93
    .local v2, "te":Lorg/apache/lucene/index/TermsEnum;
    iget-object v4, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 95
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-virtual {v2, v4, v5, v6}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 99
    .end local v2    # "te":Lorg/apache/lucene/index/TermsEnum;
    .end local v3    # "terms":Lorg/apache/lucene/index/Terms;
    :cond_0
    return-object v0

    .line 100
    :catch_0
    move-exception v1

    .line 101
    .local v1, "ioe":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 2
    .param p1, "docID"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 127
    :try_start_0
    iget v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->lastRequestedDocID:I

    if-gt p1, v1, :cond_0

    .line 128
    invoke-direct {p0}, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->getDPE()Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->dpe:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 129
    iget-object v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->dpe:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    if-nez v1, :cond_1

    .line 130
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->curDocID:I

    .line 135
    :cond_0
    :goto_0
    iput p1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->lastRequestedDocID:I

    .line 136
    iget v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->curDocID:I

    if-le v1, p1, :cond_2

    .line 138
    const/4 v1, 0x0

    iput v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 155
    :goto_1
    return-void

    .line 132
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->dpe:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->curDocID:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 152
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 142
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_1
    iget v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->curDocID:I

    if-ge v1, p1, :cond_3

    .line 143
    iget-object v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->dpe:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->curDocID:I

    .line 144
    iget v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->curDocID:I

    if-eq v1, p1, :cond_3

    .line 145
    const/4 v1, 0x0

    iput v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_1

    .line 150
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->dpe:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    .line 151
    iget-object v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;->dpe:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    invoke-virtual {p2, v1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
