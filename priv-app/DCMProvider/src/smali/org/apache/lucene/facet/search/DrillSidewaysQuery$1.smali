.class Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;
.super Lorg/apache/lucene/search/Weight;
.source "DrillSidewaysQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

.field private final synthetic val$baseWeight:Lorg/apache/lucene/search/Weight;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/search/DrillSidewaysQuery;Lorg/apache/lucene/search/Weight;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->this$0:Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    iput-object p2, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->val$baseWeight:Lorg/apache/lucene/search/Weight;

    .line 76
    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->val$baseWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->this$0:Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->baseQuery:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->val$baseWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Weight;->getValueForNormalization()F

    move-result v0

    return v0
.end method

.method public normalize(FF)V
    .locals 1
    .param p1, "norm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->val$baseWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Weight;->normalize(FF)V

    .line 95
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 17
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->this$0:Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    iget-object v3, v3, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownTerms:[[Lorg/apache/lucene/index/Term;

    array-length v3, v3

    new-array v8, v3, [Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    .line 109
    .local v8, "dims":[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;
    const/16 v16, 0x0

    .line 110
    .local v16, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v12, 0x0

    .line 111
    .local v12, "lastField":Ljava/lang/String;
    const/4 v13, 0x0

    .line 112
    .local v13, "nullCount":I
    const/4 v9, 0x0

    .local v9, "dim":I
    :goto_0
    array-length v3, v8

    if-lt v9, v3, :cond_0

    .line 138
    const/4 v3, 0x1

    if-le v13, v3, :cond_7

    .line 139
    const/4 v3, 0x0

    .line 153
    :goto_1
    return-object v3

    .line 113
    :cond_0
    new-instance v3, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    invoke-direct {v3}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;-><init>()V

    aput-object v3, v8, v9

    .line 114
    aget-object v3, v8, v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->this$0:Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    iget-object v4, v4, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillSidewaysCollectors:[Lorg/apache/lucene/search/Collector;

    aget-object v4, v4, v9

    iput-object v4, v3, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->sidewaysCollector:Lorg/apache/lucene/search/Collector;

    .line 115
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->this$0:Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    iget-object v3, v3, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownTerms:[[Lorg/apache/lucene/index/Term;

    aget-object v3, v3, v9

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v10

    .line 116
    .local v10, "field":Ljava/lang/String;
    aget-object v3, v8, v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->this$0:Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    iget-object v4, v4, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownTerms:[[Lorg/apache/lucene/index/Term;

    aget-object v4, v4, v9

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->dim:Ljava/lang/String;

    .line 117
    if-eqz v12, :cond_1

    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 118
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v14

    .line 119
    .local v14, "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v14, v10}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v15

    .line 120
    .local v15, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v15, :cond_2

    .line 121
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v16

    .line 123
    :cond_2
    move-object v12, v10

    .line 125
    .end local v14    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .end local v15    # "terms":Lorg/apache/lucene/index/Terms;
    :cond_3
    if-nez v16, :cond_5

    .line 126
    add-int/lit8 v13, v13, 0x1

    .line 112
    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 129
    :cond_5
    aget-object v3, v8, v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->this$0:Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    iget-object v4, v4, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownTerms:[[Lorg/apache/lucene/index/Term;

    aget-object v4, v4, v9

    array-length v4, v4

    new-array v4, v4, [Lorg/apache/lucene/index/DocsEnum;

    iput-object v4, v3, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->docsEnums:[Lorg/apache/lucene/index/DocsEnum;

    .line 130
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->this$0:Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    iget-object v3, v3, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownTerms:[[Lorg/apache/lucene/index/Term;

    aget-object v3, v3, v9

    array-length v3, v3

    if-ge v11, v3, :cond_4

    .line 131
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->this$0:Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    iget-object v3, v3, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownTerms:[[Lorg/apache/lucene/index/Term;

    aget-object v3, v3, v9

    aget-object v3, v3, v11

    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 132
    aget-object v3, v8, v9

    aget-object v4, v8, v9

    iget v4, v4, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->freq:I

    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, v3, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->freq:I

    .line 133
    aget-object v3, v8, v9

    iget-object v3, v3, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->docsEnums:[Lorg/apache/lucene/index/DocsEnum;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v4

    aput-object v4, v3, v11

    .line 130
    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 143
    .end local v10    # "field":Ljava/lang/String;
    .end local v11    # "i":I
    :cond_7
    invoke-static {v8}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 147
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->val$baseWeight:Lorg/apache/lucene/search/Weight;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p4

    invoke-virtual {v3, v0, v1, v4, v2}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v6

    .line 149
    .local v6, "baseScorer":Lorg/apache/lucene/search/Scorer;
    if-nez v6, :cond_8

    .line 150
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 153
    :cond_8
    new-instance v3, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;

    .line 155
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;->this$0:Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    iget-object v7, v4, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 153
    invoke-direct/range {v3 .. v8}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;-><init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Collector;[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;)V

    goto/16 :goto_1
.end method

.method public scoresDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return v0
.end method
