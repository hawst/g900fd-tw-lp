.class Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap$1;
.super Ljava/lang/Object;
.source "ResultSortUtils.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->pop()Lorg/apache/lucene/facet/search/FacetResultNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/facet/search/FacetResultNode;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap$1;->this$1:Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/search/FacetResultNode;

    check-cast p2, Lorg/apache/lucene/facet/search/FacetResultNode;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap$1;->compare(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/facet/search/FacetResultNode;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/facet/search/FacetResultNode;)I
    .locals 6
    .param p1, "o1"    # Lorg/apache/lucene/facet/search/FacetResultNode;
    .param p2, "o2"    # Lorg/apache/lucene/facet/search/FacetResultNode;

    .prologue
    .line 122
    iget-wide v2, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    iget-wide v4, p2, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    .line 123
    .local v0, "value":I
    if-nez v0, :cond_0

    .line 124
    iget v1, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    iget v2, p2, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    sub-int v0, v1, v2

    .line 126
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap$1;->this$1:Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;

    iget-boolean v1, v1, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->accending:Z

    if-eqz v1, :cond_1

    .line 127
    neg-int v0, v0

    .line 129
    :cond_1
    return v0
.end method
