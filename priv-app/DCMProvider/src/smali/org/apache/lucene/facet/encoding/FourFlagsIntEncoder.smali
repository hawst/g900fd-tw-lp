.class public Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;
.super Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;
.source "FourFlagsIntEncoder.java"


# static fields
.field private static final ENCODE_TABLE:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 56
    new-array v0, v3, [[B

    const/4 v1, 0x0

    .line 57
    new-array v2, v3, [B

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 58
    new-array v2, v3, [B

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 59
    new-array v2, v3, [B

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 60
    new-array v2, v3, [B

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    .line 56
    sput-object v0, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->ENCODE_TABLE:[[B

    .line 61
    return-void

    .line 58
    nop

    :array_0
    .array-data 1
        0x1t
        0x4t
        0x10t
        0x40t
    .end array-data

    .line 59
    :array_1
    .array-data 1
        0x2t
        0x8t
        0x20t
        -0x80t
    .end array-data

    .line 60
    :array_2
    .array-data 1
        0x3t
        0xct
        0x30t
        -0x40t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;-><init>(I)V

    .line 65
    return-void
.end method


# virtual methods
.method public createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lorg/apache/lucene/facet/encoding/FourFlagsIntDecoder;

    invoke-direct {v0}, Lorg/apache/lucene/facet/encoding/FourFlagsIntDecoder;-><init>()V

    return-object v0
.end method

.method public encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 7
    .param p1, "values"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "buf"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 69
    const/4 v3, 0x0

    iput v3, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v3, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 70
    iget v3, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v1, v3, v4

    .line 71
    .local v1, "upto":I
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_1

    .line 87
    iget-byte v3, p0, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->ordinal:B

    if-eqz v3, :cond_0

    .line 88
    invoke-virtual {p0, p2}, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->encodeChunk(Lorg/apache/lucene/util/BytesRef;)V

    .line 90
    :cond_0
    return-void

    .line 72
    :cond_1
    iget-object v3, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v2, v3, v0

    .line 73
    .local v2, "value":I
    const/4 v3, 0x3

    if-gt v2, v3, :cond_3

    .line 74
    iget v3, p0, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->indicator:I

    sget-object v4, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->ENCODE_TABLE:[[B

    aget-object v4, v4, v2

    iget-byte v5, p0, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->ordinal:B

    aget-byte v4, v4, v5

    or-int/2addr v3, v4

    iput v3, p0, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->indicator:I

    .line 78
    :goto_1
    iget-byte v3, p0, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->ordinal:B

    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    iput-byte v3, p0, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->ordinal:B

    .line 81
    iget-byte v3, p0, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->ordinal:B

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 82
    invoke-virtual {p0, p2}, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->encodeChunk(Lorg/apache/lucene/util/BytesRef;)V

    .line 71
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->encodeQueue:Lorg/apache/lucene/util/IntsRef;

    iget-object v3, v3, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->encodeQueue:Lorg/apache/lucene/util/IntsRef;

    iget v5, v4, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v6, v5, 0x1

    iput v6, v4, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v4, v2, -0x4

    aput v4, v3, v5

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    const-string v0, "FourFlags(VInt)"

    return-object v0
.end method
