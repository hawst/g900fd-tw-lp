.class public Lorg/apache/lucene/facet/index/DrillDownStream;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "DrillDownStream.java"


# instance fields
.field private final categories:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;"
        }
    .end annotation
.end field

.field private current:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

.field private final indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

.field private isParent:Z

.field private final termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 1
    .param p2, "indexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;",
            "Lorg/apache/lucene/facet/params/FacetIndexingParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    invoke-direct {p0}, Lorg/apache/lucene/analysis/TokenStream;-><init>()V

    .line 43
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/index/DrillDownStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 44
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->categories:Ljava/util/Iterator;

    .line 45
    iput-object p2, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 46
    return-void
.end method


# virtual methods
.method protected addAdditionalAttributes(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Z)V
    .locals 0
    .param p1, "category"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "isParent"    # Z

    .prologue
    .line 51
    return-void
.end method

.method public final incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    iget-object v1, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->current:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget v1, v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v1, :cond_1

    .line 56
    iget-object v1, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->categories:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 73
    :goto_0
    return v1

    .line 59
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->categories:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iput-object v1, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->current:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 60
    iget-object v1, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v4, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->current:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v4}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->fullPathLength()I

    move-result v4

    invoke-interface {v1, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    .line 61
    iput-boolean v2, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->isParent:Z

    .line 66
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    iget-object v2, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->current:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget-object v4, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->drillDownTermText(Lorg/apache/lucene/facet/taxonomy/CategoryPath;[C)I

    move-result v0

    .line 67
    .local v0, "nChars":I
    iget-object v1, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 68
    iget-object v1, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->current:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget-boolean v2, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->isParent:Z

    invoke-virtual {p0, v1, v2}, Lorg/apache/lucene/facet/index/DrillDownStream;->addAdditionalAttributes(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Z)V

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->current:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget-object v2, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->current:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget v2, v2, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->subpath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->current:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 72
    iput-boolean v3, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->isParent:Z

    move v1, v3

    .line 73
    goto :goto_0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->categories:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iput-object v0, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->current:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 79
    iget-object v0, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v1, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->current:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->fullPathLength()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/facet/index/DrillDownStream;->isParent:Z

    .line 81
    return-void
.end method
