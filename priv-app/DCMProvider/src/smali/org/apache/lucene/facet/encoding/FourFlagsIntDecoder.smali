.class public Lorg/apache/lucene/facet/encoding/FourFlagsIntDecoder;
.super Lorg/apache/lucene/facet/encoding/IntDecoder;
.source "FourFlagsIntDecoder.java"


# static fields
.field private static final DECODE_TABLE:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 34
    const/16 v2, 0x100

    const/4 v3, 0x4

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[B

    sput-object v2, Lorg/apache/lucene/facet/encoding/FourFlagsIntDecoder;->DECODE_TABLE:[[B

    .line 38
    const/16 v0, 0x100

    .local v0, "i":I
    :cond_0
    if-nez v0, :cond_1

    .line 45
    return-void

    .line 39
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 40
    const/4 v1, 0x4

    .local v1, "j":I
    :goto_0
    if-eqz v1, :cond_0

    .line 41
    add-int/lit8 v1, v1, -0x1

    .line 42
    sget-object v2, Lorg/apache/lucene/facet/encoding/FourFlagsIntDecoder;->DECODE_TABLE:[[B

    aget-object v2, v2, v0

    shl-int/lit8 v3, v1, 0x1

    ushr-int v3, v0, v3

    and-int/lit8 v3, v3, 0x3

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/IntDecoder;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V
    .locals 13
    .param p1, "buf"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "values"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 49
    const/4 v10, 0x0

    iput v10, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v10, p2, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 50
    iget v10, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v11, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v8, v10, v11

    .line 51
    .local v8, "upto":I
    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .local v4, "offset":I
    move v5, v4

    .line 52
    .end local v4    # "offset":I
    .local v5, "offset":I
    :goto_0
    if-lt v5, v8, :cond_1

    move v4, v5

    .line 85
    .end local v5    # "offset":I
    .restart local v4    # "offset":I
    :cond_0
    return-void

    .line 54
    .end local v4    # "offset":I
    .restart local v5    # "offset":I
    :cond_1
    iget-object v10, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "offset":I
    .restart local v4    # "offset":I
    aget-byte v10, v10, v5

    and-int/lit16 v3, v10, 0xff

    .line 55
    .local v3, "indicator":I
    const/4 v6, 0x0

    .line 57
    .local v6, "ordinal":I
    iget v10, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v1, v10, 0x4

    .line 58
    .local v1, "capacityNeeded":I
    iget-object v10, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v10, v10

    if-ge v10, v1, :cond_5

    .line 59
    invoke-virtual {p2, v1}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    move v7, v6

    .line 62
    .end local v6    # "ordinal":I
    .local v7, "ordinal":I
    :goto_1
    const/4 v10, 0x4

    if-ne v7, v10, :cond_2

    move v5, v4

    .end local v4    # "offset":I
    .restart local v5    # "offset":I
    goto :goto_0

    .line 63
    .end local v5    # "offset":I
    .restart local v4    # "offset":I
    :cond_2
    sget-object v10, Lorg/apache/lucene/facet/encoding/FourFlagsIntDecoder;->DECODE_TABLE:[[B

    aget-object v10, v10, v3

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "ordinal":I
    .restart local v6    # "ordinal":I
    aget-byte v2, v10, v7

    .line 64
    .local v2, "decodeVal":B
    if-nez v2, :cond_4

    .line 65
    if-eq v4, v8, :cond_0

    .line 70
    const/4 v9, 0x0

    .line 72
    .local v9, "value":I
    :goto_2
    iget-object v10, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "offset":I
    .restart local v5    # "offset":I
    aget-byte v0, v10, v4

    .line 73
    .local v0, "b":B
    if-ltz v0, :cond_3

    .line 74
    iget-object v10, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v11, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v12, v11, 0x1

    iput v12, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    shl-int/lit8 v12, v9, 0x7

    or-int/2addr v12, v0

    add-int/lit8 v12, v12, 0x4

    aput v12, v10, v11

    move v7, v6

    .end local v6    # "ordinal":I
    .restart local v7    # "ordinal":I
    move v4, v5

    .line 75
    .end local v5    # "offset":I
    .restart local v4    # "offset":I
    goto :goto_1

    .line 77
    .end local v4    # "offset":I
    .end local v7    # "ordinal":I
    .restart local v5    # "offset":I
    .restart local v6    # "ordinal":I
    :cond_3
    shl-int/lit8 v10, v9, 0x7

    and-int/lit8 v11, v0, 0x7f

    or-int v9, v10, v11

    move v4, v5

    .line 71
    .end local v5    # "offset":I
    .restart local v4    # "offset":I
    goto :goto_2

    .line 81
    .end local v0    # "b":B
    .end local v9    # "value":I
    :cond_4
    iget-object v10, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v11, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v12, v11, 0x1

    iput v12, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    aput v2, v10, v11

    .end local v2    # "decodeVal":B
    :cond_5
    move v7, v6

    .end local v6    # "ordinal":I
    .restart local v7    # "ordinal":I
    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    const-string v0, "FourFlags(VInt)"

    return-object v0
.end method
