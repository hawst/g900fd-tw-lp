.class final Lorg/apache/lucene/facet/index/CountingListBuilder$NoPartitionsOrdinalsEncoder;
.super Lorg/apache/lucene/facet/index/CountingListBuilder$OrdinalsEncoder;
.source "CountingListBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/index/CountingListBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NoPartitionsOrdinalsEncoder"
.end annotation


# instance fields
.field private final encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

.field private final name:Ljava/lang/String;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/params/CategoryListParams;)V
    .locals 1
    .param p1, "categoryListParams"    # Lorg/apache/lucene/facet/params/CategoryListParams;

    .prologue
    .line 62
    invoke-direct {p0}, Lorg/apache/lucene/facet/index/CountingListBuilder$OrdinalsEncoder;-><init>()V

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$NoPartitionsOrdinalsEncoder;->name:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Lorg/apache/lucene/facet/params/CategoryListParams;->createEncoder()Lorg/apache/lucene/facet/encoding/IntEncoder;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$NoPartitionsOrdinalsEncoder;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    .line 64
    return-void
.end method


# virtual methods
.method public encode(Lorg/apache/lucene/util/IntsRef;)Ljava/util/Map;
    .locals 2
    .param p1, "ordinals"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/IntsRef;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 69
    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    iget-object v1, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$NoPartitionsOrdinalsEncoder;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    invoke-virtual {v1, p1, v0}, Lorg/apache/lucene/facet/encoding/IntEncoder;->encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 70
    const-string v1, ""

    invoke-static {v1, v0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method
