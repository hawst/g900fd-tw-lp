.class public Lorg/apache/lucene/facet/sampling/RandomSampler;
.super Lorg/apache/lucene/facet/sampling/Sampler;
.source "RandomSampler.java"


# instance fields
.field private final random:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/facet/sampling/Sampler;-><init>()V

    .line 36
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/sampling/RandomSampler;->random:Ljava/util/Random;

    .line 37
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/sampling/SamplingParams;Ljava/util/Random;)V
    .locals 0
    .param p1, "params"    # Lorg/apache/lucene/facet/sampling/SamplingParams;
    .param p2, "random"    # Ljava/util/Random;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/sampling/Sampler;-><init>(Lorg/apache/lucene/facet/sampling/SamplingParams;)V

    .line 41
    iput-object p2, p0, Lorg/apache/lucene/facet/sampling/RandomSampler;->random:Ljava/util/Random;

    .line 42
    return-void
.end method


# virtual methods
.method protected createSample(Lorg/apache/lucene/facet/search/ScoredDocIDs;II)Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;
    .locals 16
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .param p2, "actualSize"    # I
    .param p3, "sampleSetSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    move/from16 v0, p3

    new-array v9, v0, [I

    .line 47
    .local v9, "sample":[I
    mul-int/lit8 v12, p2, 0x2

    div-int v6, v12, p3

    .line 48
    .local v6, "maxStep":I
    move/from16 v7, p2

    .line 49
    .local v7, "remaining":I
    invoke-interface/range {p1 .. p1}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;

    move-result-object v4

    .line 50
    .local v4, "it":Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
    const/4 v2, 0x0

    .line 52
    .local v2, "i":I
    :goto_0
    array-length v12, v9

    if-ge v2, v12, :cond_0

    sub-int v12, p3, v6

    sub-int/2addr v12, v2

    if-gt v7, v12, :cond_1

    .line 62
    :cond_0
    :goto_1
    array-length v12, v9

    if-lt v2, v12, :cond_3

    .line 66
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils;->createScoredDocIDsSubset(Lorg/apache/lucene/facet/search/ScoredDocIDs;[I)Lorg/apache/lucene/facet/search/ScoredDocIDs;

    move-result-object v10

    .line 67
    .local v10, "sampleRes":Lorg/apache/lucene/facet/search/ScoredDocIDs;
    new-instance v8, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;

    move/from16 v0, p3

    int-to-double v12, v0

    move/from16 v0, p2

    int-to-double v14, v0

    div-double/2addr v12, v14

    invoke-direct {v8, v10, v12, v13}, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;-><init>(Lorg/apache/lucene/facet/search/ScoredDocIDs;D)V

    .line 68
    .local v8, "res":Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;
    return-object v8

    .line 53
    .end local v8    # "res":Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;
    .end local v10    # "sampleRes":Lorg/apache/lucene/facet/search/ScoredDocIDs;
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/facet/sampling/RandomSampler;->random:Ljava/util/Random;

    invoke-virtual {v12, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v12

    add-int/lit8 v11, v12, 0x1

    .line 55
    .local v11, "skipStep":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    if-lt v5, v11, :cond_2

    .line 59
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    invoke-interface {v4}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getDocID()I

    move-result v12

    aput v12, v9, v2

    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 56
    :cond_2
    invoke-interface {v4}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->next()Z

    .line 57
    add-int/lit8 v7, v7, -0x1

    .line 55
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 63
    .end local v5    # "j":I
    .end local v11    # "skipStep":I
    :cond_3
    invoke-interface {v4}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->next()Z

    .line 64
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .restart local v3    # "i":I
    invoke-interface {v4}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getDocID()I

    move-result v12

    aput v12, v9, v2

    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_1
.end method
