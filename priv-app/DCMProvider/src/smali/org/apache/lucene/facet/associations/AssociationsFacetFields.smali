.class public Lorg/apache/lucene/facet/associations/AssociationsFacetFields;
.super Lorg/apache/lucene/facet/index/FacetFields;
.source "AssociationsFacetFields.java"


# static fields
.field private static final DRILL_DOWN_TYPE:Lorg/apache/lucene/document/FieldType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/document/TextField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0, v1}, Lorg/apache/lucene/document/FieldType;-><init>(Lorg/apache/lucene/document/FieldType;)V

    sput-object v0, Lorg/apache/lucene/facet/associations/AssociationsFacetFields;->DRILL_DOWN_TYPE:Lorg/apache/lucene/document/FieldType;

    .line 52
    sget-object v0, Lorg/apache/lucene/facet/associations/AssociationsFacetFields;->DRILL_DOWN_TYPE:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 53
    sget-object v0, Lorg/apache/lucene/facet/associations/AssociationsFacetFields;->DRILL_DOWN_TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 54
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;)V
    .locals 0
    .param p1, "taxonomyWriter"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/index/FacetFields;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 0
    .param p1, "taxonomyWriter"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;
    .param p2, "params"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/index/FacetFields;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V

    .line 77
    return-void
.end method


# virtual methods
.method public addFields(Lorg/apache/lucene/document/Document;Ljava/lang/Iterable;)V
    .locals 3
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/document/Document;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    .local p2, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    instance-of v0, p2, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "categories must be of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 119
    const-class v2, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 118
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_0
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/facet/index/FacetFields;->addFields(Lorg/apache/lucene/document/Document;Ljava/lang/Iterable;)V

    .line 122
    return-void
.end method

.method protected createCategoryListMapping(Ljava/lang/Iterable;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    .line 84
    .local v0, "categoryAssociations":Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 85
    .local v1, "categoryLists":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/facet/params/CategoryListParams;Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 95
    return-object v1

    .line 85
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 87
    .local v4, "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    iget-object v6, p0, Lorg/apache/lucene/facet/associations/AssociationsFacetFields;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-virtual {v6, v4}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v2

    .line 88
    .local v2, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    .line 89
    .local v3, "clpContainer":Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;
    if-nez v3, :cond_1

    .line 90
    new-instance v3, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    .end local v3    # "clpContainer":Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;
    invoke-direct {v3}, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;-><init>()V

    .line 91
    .restart local v3    # "clpContainer":Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    :cond_1
    invoke-virtual {v0, v4}, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;->getAssociation(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/associations/CategoryAssociation;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;->setAssociation(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/associations/CategoryAssociation;)V

    goto :goto_0
.end method

.method protected drillDownFieldType()Lorg/apache/lucene/document/FieldType;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lorg/apache/lucene/facet/associations/AssociationsFacetFields;->DRILL_DOWN_TYPE:Lorg/apache/lucene/document/FieldType;

    return-object v0
.end method

.method protected getCategoryListData(Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/util/IntsRef;Ljava/lang/Iterable;)Ljava/util/Map;
    .locals 2
    .param p1, "categoryListParams"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p2, "ordinals"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            "Lorg/apache/lucene/util/IntsRef;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    .local p3, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    new-instance v0, Lorg/apache/lucene/facet/associations/AssociationsListBuilder;

    move-object v1, p3

    check-cast v1, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/associations/AssociationsListBuilder;-><init>(Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;)V

    .line 102
    .local v0, "associations":Lorg/apache/lucene/facet/associations/AssociationsListBuilder;
    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/facet/associations/AssociationsListBuilder;->build(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method

.method protected getDrillDownStream(Ljava/lang/Iterable;)Lorg/apache/lucene/facet/index/DrillDownStream;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)",
            "Lorg/apache/lucene/facet/index/DrillDownStream;"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    new-instance v0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;

    check-cast p1, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    .end local p1    # "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    iget-object v1, p0, Lorg/apache/lucene/facet/associations/AssociationsFacetFields;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;-><init>(Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V

    return-object v0
.end method
