.class Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "ScoredDocIdsUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private next:I

.field final synthetic this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;

    .line 284
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 285
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->next:I

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 289
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->next:I

    if-gt p1, v0, :cond_0

    .line 290
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->next:I

    add-int/lit8 p1, v0, 0x1

    .line 292
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;

    # getter for: Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;->this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;
    invoke-static {v0}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;->access$0(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;)Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;

    move-result-object v0

    iget v0, v0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;->maxDoc:I

    if-lt p1, v0, :cond_1

    const p1, 0x7fffffff

    .end local p1    # "target":I
    :cond_1
    iput p1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->next:I

    return p1
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;

    # getter for: Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;->this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;
    invoke-static {v0}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;->access$0(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;)Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;

    move-result-object v0

    iget v0, v0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;->maxDoc:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->next:I

    return v0
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    .line 302
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->next:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->next:I

    iget-object v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;

    # getter for: Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;->this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;
    invoke-static {v1}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;->access$0(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1;)Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;

    move-result-object v1

    iget v1, v1, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;->maxDoc:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$1$1;->next:I

    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method
