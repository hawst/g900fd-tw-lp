.class public Lorg/apache/lucene/facet/search/FacetsAccumulator;
.super Ljava/lang/Object;
.source "FacetsAccumulator.java"


# instance fields
.field public final facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

.field public final indexReader:Lorg/apache/lucene/index/IndexReader;

.field public searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

.field public final taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V
    .locals 1
    .param p1, "searchParams"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p2, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/facet/search/FacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 1
    .param p1, "searchParams"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p2, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p4, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    if-nez p4, :cond_0

    .line 101
    new-instance p4, Lorg/apache/lucene/facet/search/FacetArrays;

    .end local p4    # "facetArrays":Lorg/apache/lucene/facet/search/FacetArrays;
    invoke-virtual {p3}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getSize()I

    move-result v0

    invoke-direct {p4, v0}, Lorg/apache/lucene/facet/search/FacetArrays;-><init>(I)V

    .line 103
    .restart local p4    # "facetArrays":Lorg/apache/lucene/facet/search/FacetArrays;
    :cond_0
    iput-object p4, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    .line 104
    iput-object p2, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->indexReader:Lorg/apache/lucene/index/IndexReader;

    .line 105
    iput-object p3, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .line 106
    iput-object p1, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    .line 107
    return-void
.end method

.method public static create(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/FacetsAccumulator;
    .locals 3
    .param p0, "fsp"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p1, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "taxoReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .prologue
    .line 71
    iget-object v1, p0, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-virtual {v1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getPartitionSize()I

    move-result v1

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 72
    new-instance v1, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

    invoke-direct {v1, p0, p1, p2}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V

    .line 81
    :goto_0
    return-object v1

    .line 75
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 81
    new-instance v1, Lorg/apache/lucene/facet/search/FacetsAccumulator;

    invoke-direct {v1, p0, p1, p2}, Lorg/apache/lucene/facet/search/FacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V

    goto :goto_0

    .line 75
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 76
    .local v0, "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    instance-of v2, v0, Lorg/apache/lucene/facet/search/CountFacetRequest;

    if-nez v2, :cond_1

    .line 77
    new-instance v1, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

    invoke-direct {v1, p0, p1, p2}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V

    goto :goto_0
.end method

.method protected static emptyResult(ILorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/search/FacetResult;
    .locals 4
    .param p0, "ordinal"    # I
    .param p1, "fr"    # Lorg/apache/lucene/facet/search/FacetRequest;

    .prologue
    .line 86
    new-instance v0, Lorg/apache/lucene/facet/search/FacetResultNode;

    const-wide/16 v2, 0x0

    invoke-direct {v0, p0, v2, v3}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    .line 87
    .local v0, "root":Lorg/apache/lucene/facet/search/FacetResultNode;
    iget-object v1, p1, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iput-object v1, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 88
    new-instance v1, Lorg/apache/lucene/facet/search/FacetResult;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v0, v2}, Lorg/apache/lucene/facet/search/FacetResult;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    return-object v1
.end method


# virtual methods
.method public accumulate(Ljava/util/List;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResult;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    .local p1, "matchingDocs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/FacetsAccumulator;->getAggregator()Lorg/apache/lucene/facet/search/FacetsAggregator;

    move-result-object v0

    .line 168
    .local v0, "aggregator":Lorg/apache/lucene/facet/search/FacetsAggregator;
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/FacetsAccumulator;->getCategoryLists()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_1

    .line 174
    iget-object v5, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-virtual {v5}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getParallelTaxonomyArrays()Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;

    move-result-object v6

    .line 177
    .local v6, "arrays":Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;
    invoke-virtual {v6}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;->children()[I

    move-result-object v3

    .line 178
    .local v3, "children":[I
    invoke-virtual {v6}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;->siblings()[I

    move-result-object v4

    .line 179
    .local v4, "siblings":[I
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 180
    .local v11, "res":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    iget-object v5, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v5, v5, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 199
    return-object v11

    .line 168
    .end local v3    # "children":[I
    .end local v4    # "siblings":[I
    .end local v6    # "arrays":Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;
    .end local v11    # "res":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 169
    .local v7, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    .line 170
    .local v9, "md":Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    iget-object v13, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    invoke-interface {v0, v9, v7, v13}, Lorg/apache/lucene/facet/search/FacetsAggregator;->aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V

    goto :goto_1

    .line 180
    .end local v7    # "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    .end local v9    # "md":Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    .restart local v3    # "children":[I
    .restart local v4    # "siblings":[I
    .restart local v6    # "arrays":Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;
    .restart local v11    # "res":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    :cond_2
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 181
    .local v1, "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    iget-object v5, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v13, v1, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v5, v13}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v2

    .line 182
    .local v2, "rootOrd":I
    const/4 v5, -0x1

    if-ne v2, v5, :cond_3

    .line 184
    invoke-static {v2, v1}, Lorg/apache/lucene/facet/search/FacetsAccumulator;->emptyResult(ILorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/search/FacetResult;

    move-result-object v5

    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 187
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v5, v5, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    iget-object v13, v1, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v5, v13}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v7

    .line 188
    .restart local v7    # "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    iget-object v5, v1, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget v5, v5, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-lez v5, :cond_4

    .line 189
    iget-object v5, v1, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget-object v5, v5, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    const/4 v13, 0x0

    aget-object v5, v5, v13

    invoke-virtual {v7, v5}, Lorg/apache/lucene/facet/params/CategoryListParams;->getOrdinalPolicy(Ljava/lang/String;)Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    move-result-object v10

    .line 190
    .local v10, "ordinalPolicy":Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    sget-object v5, Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;->NO_PARENTS:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    if-ne v10, v5, :cond_4

    .line 192
    iget-object v5, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    invoke-interface/range {v0 .. v5}, Lorg/apache/lucene/facet/search/FacetsAggregator;->rollupValues(Lorg/apache/lucene/facet/search/FacetRequest;I[I[ILorg/apache/lucene/facet/search/FacetArrays;)V

    .line 196
    .end local v10    # "ordinalPolicy":Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    :cond_4
    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/search/FacetsAccumulator;->createFacetResultsHandler(Lorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/search/FacetResultsHandler;

    move-result-object v8

    .line 197
    .local v8, "frh":Lorg/apache/lucene/facet/search/FacetResultsHandler;
    invoke-virtual {v8}, Lorg/apache/lucene/facet/search/FacetResultsHandler;->compute()Lorg/apache/lucene/facet/search/FacetResult;

    move-result-object v5

    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected createFacetResultsHandler(Lorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/search/FacetResultsHandler;
    .locals 4
    .param p1, "fr"    # Lorg/apache/lucene/facet/search/FacetRequest;

    .prologue
    .line 128
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetRequest;->getDepth()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetRequest;->getSortOrder()Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;->DESCENDING:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    if-ne v1, v2, :cond_1

    .line 129
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetRequest;->getFacetArraysSource()Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    move-result-object v0

    .line 130
    .local v0, "fas":Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->INT:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    if-ne v0, v1, :cond_0

    .line 131
    new-instance v1, Lorg/apache/lucene/facet/search/IntFacetResultsHandler;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    invoke-direct {v1, v2, p1, v3}, Lorg/apache/lucene/facet/search/IntFacetResultsHandler;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 142
    .end local v0    # "fas":Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
    :goto_0
    return-object v1

    .line 134
    .restart local v0    # "fas":Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
    :cond_0
    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->FLOAT:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    if-ne v0, v1, :cond_1

    .line 135
    new-instance v1, Lorg/apache/lucene/facet/search/FloatFacetResultsHandler;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    invoke-direct {v1, v2, p1, v3}, Lorg/apache/lucene/facet/search/FloatFacetResultsHandler;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V

    goto :goto_0

    .line 139
    .end local v0    # "fas":Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetRequest;->getResultMode()Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;->PER_NODE_IN_TREE:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    if-ne v1, v2, :cond_2

    .line 140
    new-instance v1, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    invoke-direct {v1, v2, p1, v3}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V

    goto :goto_0

    .line 142
    :cond_2
    new-instance v1, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    invoke-direct {v1, v2, p1, v3}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V

    goto :goto_0
.end method

.method public getAggregator()Lorg/apache/lucene/facet/search/FacetsAggregator;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    invoke-static {v0}, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;->verifySearchParams(Lorg/apache/lucene/facet/params/FacetSearchParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    new-instance v0, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;

    invoke-direct {v0}, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;-><init>()V

    .line 119
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/facet/search/CountingFacetsAggregator;

    invoke-direct {v0}, Lorg/apache/lucene/facet/search/CountingFacetsAggregator;-><init>()V

    goto :goto_0
.end method

.method protected getCategoryLists()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v2, v2, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-virtual {v2}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getAllCategoryListParams()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 147
    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v2, v2, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 154
    :cond_0
    return-object v0

    .line 150
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 151
    .local v0, "clps":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/facet/params/CategoryListParams;>;"
    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v2, v2, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 152
    .local v1, "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v3, v3, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    iget-object v4, v1, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
