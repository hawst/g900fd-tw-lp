.class Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$1;
.super Ljava/lang/Object;
.source "SortedSetDocValuesAccumulator.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/FacetsAggregator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->getAggregator()Lorg/apache/lucene/facet/search/FacetsAggregator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$1;->this$0:Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 18
    .param p1, "matchingDocs"    # Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    .param p2, "clp"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v13}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$1;->this$0:Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;

    iget-object v14, v14, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->field:Ljava/lang/String;

    invoke-virtual {v13, v14}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v11

    .line 94
    .local v11, "segValues":Lorg/apache/lucene/index/SortedSetDocValues;
    if-nez v11, :cond_1

    .line 158
    :cond_0
    return-void

    .line 98
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/facet/search/FacetArrays;->getIntArray()[I

    move-result-object v3

    .line 99
    .local v3, "counts":[I
    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v13}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v13

    invoke-virtual {v13}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v5

    .line 100
    .local v5, "maxDoc":I
    sget-boolean v13, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->$assertionsDisabled:Z

    if-nez v13, :cond_2

    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v13}, Lorg/apache/lucene/util/FixedBitSet;->length()I

    move-result v13

    if-eq v5, v13, :cond_2

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 102
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$1;->this$0:Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;

    iget-object v13, v13, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->dv:Lorg/apache/lucene/index/SortedSetDocValues;

    instance-of v13, v13, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;

    if-eqz v13, :cond_9

    .line 103
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$1;->this$0:Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;

    iget-object v13, v13, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->dv:Lorg/apache/lucene/index/SortedSetDocValues;

    check-cast v13, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;

    iget-object v8, v13, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    .line 104
    .local v8, "ordinalMap":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    iget v10, v13, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    .line 106
    .local v10, "segOrd":I
    invoke-virtual {v11}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v14

    long-to-int v6, v14

    .line 108
    .local v6, "numSegOrds":I
    move-object/from16 v0, p1

    iget v13, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->totalHits:I

    div-int/lit8 v14, v6, 0xa

    if-ge v13, v14, :cond_4

    .line 110
    const/4 v4, 0x0

    .line 111
    .local v4, "doc":I
    :goto_0
    if-ge v4, v5, :cond_0

    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v13, v4}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v4

    const/4 v13, -0x1

    if-eq v4, v13, :cond_0

    .line 112
    invoke-virtual {v11, v4}, Lorg/apache/lucene/index/SortedSetDocValues;->setDocument(I)V

    .line 113
    invoke-virtual {v11}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v14

    long-to-int v12, v14

    .line 114
    .local v12, "term":I
    :goto_1
    int-to-long v14, v12

    const-wide/16 v16, -0x1

    cmp-long v13, v14, v16

    if-nez v13, :cond_3

    .line 118
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 115
    :cond_3
    int-to-long v14, v12

    invoke-virtual {v8, v10, v14, v15}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getGlobalOrd(IJ)J

    move-result-wide v14

    long-to-int v13, v14

    aget v14, v3, v13

    add-int/lit8 v14, v14, 0x1

    aput v14, v3, v13

    .line 116
    invoke-virtual {v11}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v14

    long-to-int v12, v14

    goto :goto_1

    .line 123
    .end local v4    # "doc":I
    .end local v12    # "term":I
    :cond_4
    new-array v9, v6, [I

    .line 124
    .local v9, "segCounts":[I
    const/4 v4, 0x0

    .line 125
    .restart local v4    # "doc":I
    :goto_2
    if-ge v4, v5, :cond_5

    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v13, v4}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v4

    const/4 v13, -0x1

    if-ne v4, v13, :cond_7

    .line 136
    :cond_5
    const/4 v7, 0x0

    .local v7, "ord":I
    :goto_3
    if-ge v7, v6, :cond_0

    .line 137
    aget v2, v9, v7

    .line 138
    .local v2, "count":I
    if-eqz v2, :cond_6

    .line 139
    int-to-long v14, v7

    invoke-virtual {v8, v10, v14, v15}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getGlobalOrd(IJ)J

    move-result-wide v14

    long-to-int v13, v14

    aget v14, v3, v13

    add-int/2addr v14, v2

    aput v14, v3, v13

    .line 136
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 126
    .end local v2    # "count":I
    .end local v7    # "ord":I
    :cond_7
    invoke-virtual {v11, v4}, Lorg/apache/lucene/index/SortedSetDocValues;->setDocument(I)V

    .line 127
    invoke-virtual {v11}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v14

    long-to-int v12, v14

    .line 128
    .restart local v12    # "term":I
    :goto_4
    int-to-long v14, v12

    const-wide/16 v16, -0x1

    cmp-long v13, v14, v16

    if-nez v13, :cond_8

    .line 132
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 129
    :cond_8
    aget v13, v9, v12

    add-int/lit8 v13, v13, 0x1

    aput v13, v9, v12

    .line 130
    invoke-virtual {v11}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v14

    long-to-int v12, v14

    goto :goto_4

    .line 147
    .end local v4    # "doc":I
    .end local v6    # "numSegOrds":I
    .end local v8    # "ordinalMap":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    .end local v9    # "segCounts":[I
    .end local v10    # "segOrd":I
    .end local v12    # "term":I
    :cond_9
    const/4 v4, 0x0

    .line 148
    .restart local v4    # "doc":I
    :goto_5
    if-ge v4, v5, :cond_0

    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v13, v4}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v4

    const/4 v13, -0x1

    if-eq v4, v13, :cond_0

    .line 149
    invoke-virtual {v11, v4}, Lorg/apache/lucene/index/SortedSetDocValues;->setDocument(I)V

    .line 150
    invoke-virtual {v11}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v14

    long-to-int v12, v14

    .line 151
    .restart local v12    # "term":I
    :goto_6
    int-to-long v14, v12

    const-wide/16 v16, -0x1

    cmp-long v13, v14, v16

    if-nez v13, :cond_a

    .line 155
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 152
    :cond_a
    aget v13, v3, v12

    add-int/lit8 v13, v13, 0x1

    aput v13, v3, v12

    .line 153
    invoke-virtual {v11}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v14

    long-to-int v12, v14

    goto :goto_6
.end method

.method public requiresDocScores()Z
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    return v0
.end method

.method public rollupValues(Lorg/apache/lucene/facet/search/FacetRequest;I[I[ILorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 0
    .param p1, "fr"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p2, "ordinal"    # I
    .param p3, "children"    # [I
    .param p4, "siblings"    # [I
    .param p5, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 165
    return-void
.end method
