.class Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesProducer;
.super Lorg/apache/lucene/codecs/DocValuesProducer;
.source "Facet42DocValuesProducer.java"


# instance fields
.field private final fields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/facet/codecs/facet42/Facet42BinaryDocValues;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/SegmentReadState;)V
    .locals 9
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesProducer;-><init>()V

    .line 38
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesProducer;->fields:Ljava/util/Map;

    .line 41
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    const-string v6, "fdv"

    invoke-static {v4, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "fileName":Ljava/lang/String;
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v4, v1, v5}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 43
    .local v2, "in":Lorg/apache/lucene/store/IndexInput;
    const/4 v3, 0x0

    .line 45
    .local v3, "success":Z
    :try_start_0
    const-string v4, "FacetsDocValues"

    .line 46
    const/4 v5, 0x0

    .line 47
    const/4 v6, 0x0

    .line 45
    invoke-static {v2, v4, v5, v6}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 48
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 49
    .local v0, "fieldNumber":I
    :goto_0
    const/4 v4, -0x1

    if-ne v0, v4, :cond_0

    .line 53
    const/4 v3, 0x1

    .line 55
    if-eqz v3, :cond_2

    new-array v4, v8, [Ljava/io/Closeable;

    .line 56
    aput-object v2, v4, v7

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 61
    :goto_1
    return-void

    .line 50
    :cond_0
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesProducer;->fields:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lorg/apache/lucene/facet/codecs/facet42/Facet42BinaryDocValues;

    invoke-direct {v6, v2}, Lorg/apache/lucene/facet/codecs/facet42/Facet42BinaryDocValues;-><init>(Lorg/apache/lucene/store/DataInput;)V

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 54
    .end local v0    # "fieldNumber":I
    :catchall_0
    move-exception v4

    .line 55
    if-eqz v3, :cond_1

    new-array v5, v8, [Ljava/io/Closeable;

    .line 56
    aput-object v2, v5, v7

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 60
    :goto_2
    throw v4

    .line 57
    :cond_1
    new-array v5, v8, [Ljava/io/Closeable;

    .line 58
    aput-object v2, v5, v7

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_2

    .line 57
    .restart local v0    # "fieldNumber":I
    :cond_2
    new-array v4, v8, [Ljava/io/Closeable;

    .line 58
    aput-object v2, v4, v7

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    return-void
.end method

.method public getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 2
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesProducer;->fields:Ljava/util/Map;

    iget v1, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/BinaryDocValues;

    return-object v0
.end method

.method public getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 2
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FacetsDocValues only implements binary"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSorted(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 2
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FacetsDocValues only implements binary"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 2
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FacetsDocValues only implements binary"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
