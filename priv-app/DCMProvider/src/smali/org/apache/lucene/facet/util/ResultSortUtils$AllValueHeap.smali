.class Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;
.super Ljava/lang/Object;
.source "ResultSortUtils.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/Heap;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/util/ResultSortUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AllValueHeap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/lucene/facet/search/Heap",
        "<",
        "Lorg/apache/lucene/facet/search/FacetResultNode;",
        ">;"
    }
.end annotation


# instance fields
.field final accending:Z

.field private isReady:Z

.field private resultNodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "accending"    # Z

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->resultNodes:Ljava/util/ArrayList;

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->isReady:Z

    .line 107
    iput-boolean p1, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->accending:Z

    .line 108
    return-void
.end method


# virtual methods
.method public bridge synthetic add(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/search/FacetResultNode;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->add(Lorg/apache/lucene/facet/search/FacetResultNode;)Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v0

    return-object v0
.end method

.method public add(Lorg/apache/lucene/facet/search/FacetResultNode;)Lorg/apache/lucene/facet/search/FacetResultNode;
    .locals 1
    .param p1, "frn"    # Lorg/apache/lucene/facet/search/FacetResultNode;

    .prologue
    .line 154
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->resultNodes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    const/4 v0, 0x0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->resultNodes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 161
    return-void
.end method

.method public bridge synthetic insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/search/FacetResultNode;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->insertWithOverflow(Lorg/apache/lucene/facet/search/FacetResultNode;)Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v0

    return-object v0
.end method

.method public insertWithOverflow(Lorg/apache/lucene/facet/search/FacetResultNode;)Lorg/apache/lucene/facet/search/FacetResultNode;
    .locals 1
    .param p1, "node"    # Lorg/apache/lucene/facet/search/FacetResultNode;

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->resultNodes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic pop()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->pop()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v0

    return-object v0
.end method

.method public pop()Lorg/apache/lucene/facet/search/FacetResultNode;
    .locals 2

    .prologue
    .line 118
    iget-boolean v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->isReady:Z

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->resultNodes:Ljava/util/ArrayList;

    new-instance v1, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap$1;

    invoke-direct {v1, p0}, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap$1;-><init>(Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->isReady:Z

    .line 135
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->resultNodes:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetResultNode;

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->resultNodes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic top()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->top()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v0

    return-object v0
.end method

.method public top()Lorg/apache/lucene/facet/search/FacetResultNode;
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->resultNodes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 146
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;->resultNodes:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 149
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
