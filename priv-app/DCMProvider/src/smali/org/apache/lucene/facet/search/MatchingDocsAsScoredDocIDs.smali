.class public Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;
.super Ljava/lang/Object;
.source "MatchingDocsAsScoredDocIDs.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/ScoredDocIDs;


# instance fields
.field final matchingDocs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;",
            ">;"
        }
    .end annotation
.end field

.field final size:I


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "matchingDocs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;->matchingDocs:Ljava/util/List;

    .line 42
    const/4 v1, 0x0

    .line 43
    .local v1, "totalSize":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 46
    iput v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;->size:I

    .line 47
    return-void

    .line 43
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    .line 44
    .local v0, "md":Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    iget v3, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->totalHits:I

    add-int/2addr v1, v3

    goto :goto_0
.end method


# virtual methods
.method public getDocIDs()Lorg/apache/lucene/search/DocIdSet;
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$2;-><init>(Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;)V

    return-object v0
.end method

.method public iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;-><init>(Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;->size:I

    return v0
.end method
