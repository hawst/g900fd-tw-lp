.class final Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;
.super Ljava/lang/Object;
.source "ScoredDocIdsUtils.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/ScoredDocIDs;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/util/ScoredDocIdsUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AllLiveDocsScoredDocIDs"
.end annotation


# instance fields
.field final maxDoc:I

.field final reader:Lorg/apache/lucene/index/IndexReader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 359
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;->maxDoc:I

    .line 360
    iput-object p1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 361
    return-void
.end method


# virtual methods
.method public getDocIDs()Lorg/apache/lucene/search/DocIdSet;
    .locals 1

    .prologue
    .line 370
    new-instance v0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;-><init>(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;)V

    return-object v0
.end method

.method public iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
    .locals 3

    .prologue
    .line 417
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;->getDocIDs()Lorg/apache/lucene/search/DocIdSet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 418
    .local v1, "iter":Lorg/apache/lucene/search/DocIdSetIterator;
    new-instance v2, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$2;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$2;-><init>(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;Lorg/apache/lucene/search/DocIdSetIterator;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 439
    .end local v1    # "iter":Lorg/apache/lucene/search/DocIdSetIterator;
    :catch_0
    move-exception v0

    .line 441
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public size()I
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v0

    return v0
.end method
