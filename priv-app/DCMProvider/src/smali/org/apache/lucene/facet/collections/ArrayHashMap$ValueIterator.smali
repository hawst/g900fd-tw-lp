.class final Lorg/apache/lucene/facet/collections/ArrayHashMap$ValueIterator;
.super Ljava/lang/Object;
.source "ArrayHashMap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/ArrayHashMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ValueIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private iterator:Lorg/apache/lucene/facet/collections/IntIterator;

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/ArrayHashMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/collections/ArrayHashMap;)V
    .locals 1

    .prologue
    .line 126
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/ArrayHashMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    new-instance v0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/ArrayHashMap;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    .line 126
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/ArrayHashMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->values:[Ljava/lang/Object;

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->remove()V

    .line 142
    return-void
.end method
