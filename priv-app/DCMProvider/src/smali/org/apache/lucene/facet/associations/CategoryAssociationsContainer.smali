.class public Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;
.super Ljava/lang/Object;
.source "CategoryAssociationsContainer.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
        ">;"
    }
.end annotation


# instance fields
.field private final categoryAssociations:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            "Lorg/apache/lucene/facet/associations/CategoryAssociation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;->categoryAssociations:Ljava/util/HashMap;

    .line 26
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;->categoryAssociations:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 57
    return-void
.end method

.method public getAssociation(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/associations/CategoryAssociation;
    .locals 1
    .param p1, "category"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;->categoryAssociations:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/associations/CategoryAssociation;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;->categoryAssociations:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public setAssociation(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/associations/CategoryAssociation;)V
    .locals 1
    .param p1, "category"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "association"    # Lorg/apache/lucene/facet/associations/CategoryAssociation;

    .prologue
    .line 38
    iget-object v0, p0, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;->categoryAssociations:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;->categoryAssociations:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
