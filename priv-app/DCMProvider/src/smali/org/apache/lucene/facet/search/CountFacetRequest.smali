.class public Lorg/apache/lucene/facet/search/CountFacetRequest;
.super Lorg/apache/lucene/facet/search/FacetRequest;
.source "CountFacetRequest.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V
    .locals 0
    .param p1, "path"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "num"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/search/FacetRequest;-><init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V

    .line 33
    return-void
.end method


# virtual methods
.method public createAggregator(ZLorg/apache/lucene/facet/search/FacetArrays;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/Aggregator;
    .locals 2
    .param p1, "useComplements"    # Z
    .param p2, "arrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p3, "taxonomy"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .prologue
    .line 38
    invoke-virtual {p2}, Lorg/apache/lucene/facet/search/FacetArrays;->getIntArray()[I

    move-result-object v0

    .line 39
    .local v0, "a":[I
    if-eqz p1, :cond_0

    .line 40
    new-instance v1, Lorg/apache/lucene/facet/complements/ComplementCountingAggregator;

    invoke-direct {v1, v0}, Lorg/apache/lucene/facet/complements/ComplementCountingAggregator;-><init>([I)V

    .line 42
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lorg/apache/lucene/facet/search/CountingAggregator;

    invoke-direct {v1, v0}, Lorg/apache/lucene/facet/search/CountingAggregator;-><init>([I)V

    goto :goto_0
.end method

.method public getFacetArraysSource()Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->INT:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    return-object v0
.end method

.method public getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D
    .locals 2
    .param p1, "arrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p2, "ordinal"    # I

    .prologue
    .line 47
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetArrays;->getIntArray()[I

    move-result-object v0

    aget v0, v0, p2

    int-to-double v0, v0

    return-wide v0
.end method
