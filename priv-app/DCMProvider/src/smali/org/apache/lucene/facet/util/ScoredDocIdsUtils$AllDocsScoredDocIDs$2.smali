.class Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$2;
.super Ljava/lang/Object;
.source "ScoredDocIdsUtils.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;->iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;

.field private final synthetic val$iter:Lorg/apache/lucene/search/DocIdSetIterator;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$2;->this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;

    iput-object p2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$2;->val$iter:Lorg/apache/lucene/search/DocIdSetIterator;

    .line 318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDocID()I
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$2;->val$iter:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v0

    return v0
.end method

.method public getScore()F
    .locals 1

    .prologue
    .line 331
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public next()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 322
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs$2;->val$iter:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const v3, 0x7fffffff

    if-eq v2, v3, :cond_0

    const/4 v1, 0x1

    .line 325
    :cond_0
    :goto_0
    return v1

    .line 323
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0
.end method
