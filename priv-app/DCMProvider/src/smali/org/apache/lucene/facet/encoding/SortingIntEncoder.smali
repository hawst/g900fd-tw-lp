.class public final Lorg/apache/lucene/facet/encoding/SortingIntEncoder;
.super Lorg/apache/lucene/facet/encoding/IntEncoderFilter;
.source "SortingIntEncoder.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/encoding/IntEncoder;)V
    .locals 0
    .param p1, "encoder"    # Lorg/apache/lucene/facet/encoding/IntEncoder;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/encoding/IntEncoderFilter;-><init>(Lorg/apache/lucene/facet/encoding/IntEncoder;)V

    .line 36
    return-void
.end method


# virtual methods
.method public createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/facet/encoding/SortingIntEncoder;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/encoding/IntEncoder;->createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;

    move-result-object v0

    return-object v0
.end method

.method public encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 4
    .param p1, "values"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "buf"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 40
    iget-object v0, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v2, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v3, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->sort([III)V

    .line 41
    iget-object v0, p0, Lorg/apache/lucene/facet/encoding/SortingIntEncoder;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/facet/encoding/IntEncoder;->encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 42
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Sorting("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/facet/encoding/SortingIntEncoder;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
