.class public final Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;
.super Ljava/lang/Object;
.source "OrdinalsCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/OrdinalsCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CachedOrds"
.end annotation


# instance fields
.field public final offsets:[I

.field public final ordinals:[I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/BinaryDocValues;ILorg/apache/lucene/facet/params/CategoryListParams;)V
    .locals 12
    .param p1, "dv"    # Lorg/apache/lucene/index/BinaryDocValues;
    .param p2, "maxDoc"    # I
    .param p3, "clp"    # Lorg/apache/lucene/facet/params/CategoryListParams;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 64
    .local v0, "buf":Lorg/apache/lucene/util/BytesRef;
    add-int/lit8 v8, p2, 0x1

    new-array v8, v8, [I

    iput-object v8, p0, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;->offsets:[I

    .line 65
    new-array v4, p2, [I

    .line 68
    .local v4, "ords":[I
    const/4 v5, 0x0

    .line 69
    .local v5, "totOrds":I
    invoke-virtual {p3}, Lorg/apache/lucene/facet/params/CategoryListParams;->createEncoder()Lorg/apache/lucene/facet/encoding/IntEncoder;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/lucene/facet/encoding/IntEncoder;->createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;

    move-result-object v1

    .line 70
    .local v1, "decoder":Lorg/apache/lucene/facet/encoding/IntDecoder;
    new-instance v7, Lorg/apache/lucene/util/IntsRef;

    const/16 v8, 0x20

    invoke-direct {v7, v8}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    .line 71
    .local v7, "values":Lorg/apache/lucene/util/IntsRef;
    const/4 v2, 0x0

    .local v2, "docID":I
    :goto_0
    if-lt v2, p2, :cond_0

    .line 85
    iget-object v8, p0, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;->offsets:[I

    aput v5, v8, p2

    .line 88
    int-to-double v8, v5

    array-length v10, v4

    int-to-double v10, v10

    div-double/2addr v8, v10

    const-wide v10, 0x3feccccccccccccdL    # 0.9

    cmpg-double v8, v8, v10

    if-gez v8, :cond_4

    .line 89
    new-array v8, v5, [I

    iput-object v8, p0, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;->ordinals:[I

    .line 90
    const/4 v8, 0x0

    iget-object v9, p0, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;->ordinals:[I

    const/4 v10, 0x0

    invoke-static {v4, v8, v9, v10, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    :goto_1
    return-void

    .line 72
    :cond_0
    iget-object v8, p0, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;->offsets:[I

    aput v5, v8, v2

    .line 73
    invoke-virtual {p1, v2, v0}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 74
    iget v8, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lez v8, :cond_2

    .line 76
    invoke-virtual {v1, v0, v7}, Lorg/apache/lucene/facet/encoding/IntDecoder;->decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V

    .line 77
    iget v8, v7, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v8, v5

    array-length v9, v4

    if-lt v8, v9, :cond_1

    .line 78
    iget v8, v7, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v8, v5

    add-int/lit8 v8, v8, 0x1

    invoke-static {v4, v8}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v4

    .line 80
    :cond_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget v8, v7, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v3, v8, :cond_3

    .line 71
    .end local v3    # "i":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 81
    .restart local v3    # "i":I
    :cond_3
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "totOrds":I
    .local v6, "totOrds":I
    iget-object v8, v7, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v8, v8, v3

    aput v8, v4, v5

    .line 80
    add-int/lit8 v3, v3, 0x1

    move v5, v6

    .end local v6    # "totOrds":I
    .restart local v5    # "totOrds":I
    goto :goto_2

    .line 92
    .end local v3    # "i":I
    :cond_4
    iput-object v4, p0, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;->ordinals:[I

    goto :goto_1
.end method
