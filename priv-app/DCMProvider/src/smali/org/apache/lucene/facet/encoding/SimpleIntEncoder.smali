.class public final Lorg/apache/lucene/facet/encoding/SimpleIntEncoder;
.super Lorg/apache/lucene/facet/encoding/IntEncoder;
.source "SimpleIntEncoder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/IntEncoder;-><init>()V

    return-void
.end method


# virtual methods
.method public createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lorg/apache/lucene/facet/encoding/SimpleIntDecoder;

    invoke-direct {v0}, Lorg/apache/lucene/facet/encoding/SimpleIntDecoder;-><init>()V

    return-object v0
.end method

.method public encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 7
    .param p1, "values"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "buf"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 32
    const/4 v4, 0x0

    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 34
    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    mul-int/lit8 v0, v4, 0x4

    .line 35
    .local v0, "bytesNeeded":I
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v4, v4

    if-ge v4, v0, :cond_0

    .line 36
    invoke-virtual {p2, v0}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 39
    :cond_0
    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v2, v4, v5

    .line 40
    .local v2, "upto":I
    iget v1, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 47
    return-void

    .line 41
    :cond_1
    iget-object v4, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v3, v4, v1

    .line 42
    .local v3, "value":I
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    ushr-int/lit8 v6, v3, 0x18

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 43
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    shr-int/lit8 v6, v3, 0x10

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 44
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    shr-int/lit8 v6, v3, 0x8

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 45
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int/lit16 v6, v3, 0xff

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 40
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string v0, "Simple"

    return-object v0
.end method
