.class Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "ScoredDocIdsUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private next:I

.field final synthetic this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1;

.field private final synthetic val$docids:[I

.field private final synthetic val$size:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1;I[I)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1;

    iput p2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->val$size:I

    iput-object p3, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->val$docids:[I

    .line 141
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 143
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->next:I

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 3
    .param p1, "target"    # I

    .prologue
    .line 147
    :cond_0
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->next:I

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->val$size:I

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->val$docids:[I

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->next:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->next:I

    aget v0, v0, v1

    if-lt v0, p1, :cond_0

    .line 149
    :cond_1
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->next:I

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->val$size:I

    if-ne v0, v1, :cond_2

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->val$docids:[I

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->next:I

    aget v0, v0, v1

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 167
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->val$size:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->val$docids:[I

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->next:I

    aget v0, v0, v1

    return v0
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    .line 159
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->next:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->next:I

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->val$size:I

    if-lt v0, v1, :cond_0

    .line 160
    const v0, 0x7fffffff

    .line 162
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->val$docids:[I

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1$1;->next:I

    aget v0, v0, v1

    goto :goto_0
.end method
