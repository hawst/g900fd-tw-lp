.class Lorg/apache/lucene/facet/search/DrillSidewaysQuery;
.super Lorg/apache/lucene/search/Query;
.source "DrillSidewaysQuery.java"


# instance fields
.field final baseQuery:Lorg/apache/lucene/search/Query;

.field final drillDownCollector:Lorg/apache/lucene/search/Collector;

.field final drillDownTerms:[[Lorg/apache/lucene/index/Term;

.field final drillSidewaysCollectors:[Lorg/apache/lucene/search/Collector;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Collector;[Lorg/apache/lucene/search/Collector;[[Lorg/apache/lucene/index/Term;)V
    .locals 0
    .param p1, "baseQuery"    # Lorg/apache/lucene/search/Query;
    .param p2, "drillDownCollector"    # Lorg/apache/lucene/search/Collector;
    .param p3, "drillSidewaysCollectors"    # [Lorg/apache/lucene/search/Collector;
    .param p4, "drillDownTerms"    # [[Lorg/apache/lucene/index/Term;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 44
    iput-object p1, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->baseQuery:Lorg/apache/lucene/search/Query;

    .line 45
    iput-object p2, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    .line 46
    iput-object p3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillSidewaysCollectors:[Lorg/apache/lucene/search/Collector;

    .line 47
    iput-object p4, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownTerms:[[Lorg/apache/lucene/index/Term;

    .line 48
    return-void
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 2
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v1, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->baseQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    .line 76
    .local v0, "baseWeight":Lorg/apache/lucene/search/Weight;
    new-instance v1, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/facet/search/DrillSidewaysQuery$1;-><init>(Lorg/apache/lucene/facet/search/DrillSidewaysQuery;Lorg/apache/lucene/search/Weight;)V

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 176
    if-ne p0, p1, :cond_1

    .line 188
    :cond_0
    :goto_0
    return v1

    .line 177
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    .line 178
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 179
    check-cast v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    .line 180
    .local v0, "other":Lorg/apache/lucene/facet/search/DrillSidewaysQuery;
    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->baseQuery:Lorg/apache/lucene/search/Query;

    if-nez v3, :cond_4

    .line 181
    iget-object v3, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->baseQuery:Lorg/apache/lucene/search/Query;

    if-eqz v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 182
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->baseQuery:Lorg/apache/lucene/search/Query;

    iget-object v4, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->baseQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 183
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    if-nez v3, :cond_6

    .line 184
    iget-object v3, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    if-eqz v3, :cond_7

    move v1, v2

    goto :goto_0

    .line 185
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    iget-object v4, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    goto :goto_0

    .line 186
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownTerms:[[Lorg/apache/lucene/index/Term;

    iget-object v4, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownTerms:[[Lorg/apache/lucene/index/Term;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    goto :goto_0

    .line 187
    :cond_8
    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillSidewaysCollectors:[Lorg/apache/lucene/search/Collector;

    iget-object v4, v0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillSidewaysCollectors:[Lorg/apache/lucene/search/Collector;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 164
    const/16 v0, 0x1f

    .line 165
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v1

    .line 166
    .local v1, "result":I
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->baseQuery:Lorg/apache/lucene/search/Query;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 167
    mul-int/lit8 v2, v1, 0x1f

    .line 168
    iget-object v4, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    if-nez v4, :cond_1

    .line 167
    :goto_1
    add-int v1, v2, v3

    .line 169
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownTerms:[[Lorg/apache/lucene/index/Term;

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v1, v2, v3

    .line 170
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillSidewaysCollectors:[Lorg/apache/lucene/search/Collector;

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v1, v2, v3

    .line 171
    return v1

    .line 166
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->baseQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v2

    goto :goto_0

    .line 168
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->baseQuery:Lorg/apache/lucene/search/Query;

    .line 59
    .local v0, "newQuery":Lorg/apache/lucene/search/Query;
    :goto_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 60
    .local v1, "rewrittenQuery":Lorg/apache/lucene/search/Query;
    if-ne v1, v0, :cond_0

    .line 65
    iget-object v2, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->baseQuery:Lorg/apache/lucene/search/Query;

    if-ne v0, v2, :cond_1

    .line 68
    .end local p0    # "this":Lorg/apache/lucene/facet/search/DrillSidewaysQuery;
    :goto_1
    return-object p0

    .line 63
    .restart local p0    # "this":Lorg/apache/lucene/facet/search/DrillSidewaysQuery;
    :cond_0
    move-object v0, v1

    .line 58
    goto :goto_0

    .line 68
    :cond_1
    new-instance v2, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    iget-object v4, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillSidewaysCollectors:[Lorg/apache/lucene/search/Collector;

    iget-object v5, p0, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;->drillDownTerms:[[Lorg/apache/lucene/index/Term;

    invoke-direct {v2, v0, v3, v4, v5}, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Collector;[Lorg/apache/lucene/search/Collector;[[Lorg/apache/lucene/index/Term;)V

    move-object p0, v2

    goto :goto_1
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 52
    const-string v0, "DrillSidewaysQuery"

    return-object v0
.end method
