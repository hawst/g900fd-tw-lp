.class public Lorg/apache/lucene/facet/search/PerCategoryListAggregator;
.super Ljava/lang/Object;
.source "PerCategoryListAggregator.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/FacetsAggregator;


# instance fields
.field private final aggregators:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            "Lorg/apache/lucene/facet/search/FacetsAggregator;",
            ">;"
        }
    .end annotation
.end field

.field private final fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;


# direct methods
.method public constructor <init>(Ljava/util/Map;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 0
    .param p2, "fip"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            "Lorg/apache/lucene/facet/search/FacetsAggregator;",
            ">;",
            "Lorg/apache/lucene/facet/params/FacetIndexingParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "aggregators":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetsAggregator;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/facet/search/PerCategoryListAggregator;->aggregators:Ljava/util/Map;

    .line 38
    iput-object p2, p0, Lorg/apache/lucene/facet/search/PerCategoryListAggregator;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 39
    return-void
.end method


# virtual methods
.method public aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 1
    .param p1, "matchingDocs"    # Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    .param p2, "clp"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/facet/search/PerCategoryListAggregator;->aggregators:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetsAggregator;

    invoke-interface {v0, p1, p2, p3}, Lorg/apache/lucene/facet/search/FacetsAggregator;->aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 44
    return-void
.end method

.method public requiresDocScores()Z
    .locals 3

    .prologue
    .line 54
    iget-object v1, p0, Lorg/apache/lucene/facet/search/PerCategoryListAggregator;->aggregators:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 59
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 54
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetsAggregator;

    .line 55
    .local v0, "aggregator":Lorg/apache/lucene/facet/search/FacetsAggregator;
    invoke-interface {v0}, Lorg/apache/lucene/facet/search/FacetsAggregator;->requiresDocScores()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public rollupValues(Lorg/apache/lucene/facet/search/FacetRequest;I[I[ILorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 7
    .param p1, "fr"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p2, "ordinal"    # I
    .param p3, "children"    # [I
    .param p4, "siblings"    # [I
    .param p5, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/facet/search/PerCategoryListAggregator;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    iget-object v1, p1, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v6

    .line 49
    .local v6, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    iget-object v0, p0, Lorg/apache/lucene/facet/search/PerCategoryListAggregator;->aggregators:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetsAggregator;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lorg/apache/lucene/facet/search/FacetsAggregator;->rollupValues(Lorg/apache/lucene/facet/search/FacetRequest;I[I[ILorg/apache/lucene/facet/search/FacetArrays;)V

    .line 50
    return-void
.end method
