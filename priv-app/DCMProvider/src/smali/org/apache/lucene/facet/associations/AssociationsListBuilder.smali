.class public Lorg/apache/lucene/facet/associations/AssociationsListBuilder;
.super Ljava/lang/Object;
.source "AssociationsListBuilder.java"

# interfaces
.implements Lorg/apache/lucene/facet/index/CategoryListBuilder;


# instance fields
.field private final associations:Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

.field private final output:Lorg/apache/lucene/store/ByteArrayDataOutput;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;)V
    .locals 1
    .param p1, "associations"    # Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lorg/apache/lucene/store/ByteArrayDataOutput;

    invoke-direct {v0}, Lorg/apache/lucene/store/ByteArrayDataOutput;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/associations/AssociationsListBuilder;->output:Lorg/apache/lucene/store/ByteArrayDataOutput;

    .line 46
    iput-object p1, p0, Lorg/apache/lucene/facet/associations/AssociationsListBuilder;->associations:Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    .line 47
    return-void
.end method


# virtual methods
.method public build(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Iterable;)Ljava/util/Map;
    .locals 13
    .param p1, "ordinals"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/IntsRef;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    .local p2, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 52
    .local v6, "res":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;>;"
    const/4 v3, 0x0

    .line 53
    .local v3, "idx":I
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 86
    return-object v6

    .line 53
    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 55
    .local v2, "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    iget-object v8, p0, Lorg/apache/lucene/facet/associations/AssociationsListBuilder;->associations:Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    invoke-virtual {v8, v2}, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;->getAssociation(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/associations/CategoryAssociation;

    move-result-object v0

    .line 57
    .local v0, "association":Lorg/apache/lucene/facet/associations/CategoryAssociation;
    if-nez v0, :cond_1

    .line 60
    add-int/lit8 v3, v3, 0x1

    .line 61
    goto :goto_0

    .line 64
    :cond_1
    invoke-interface {v0}, Lorg/apache/lucene/facet/associations/CategoryAssociation;->getCategoryListID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/BytesRef;

    .line 65
    .local v1, "bytes":Lorg/apache/lucene/util/BytesRef;
    if-nez v1, :cond_2

    .line 66
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    .end local v1    # "bytes":Lorg/apache/lucene/util/BytesRef;
    const/16 v8, 0x20

    invoke-direct {v1, v8}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 67
    .restart local v1    # "bytes":Lorg/apache/lucene/util/BytesRef;
    invoke-interface {v0}, Lorg/apache/lucene/facet/associations/CategoryAssociation;->getCategoryListID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    :cond_2
    invoke-interface {v0}, Lorg/apache/lucene/facet/associations/CategoryAssociation;->maxBytesNeeded()I

    move-result v8

    add-int/lit8 v8, v8, 0x4

    iget v9, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v5, v8, v9

    .line 71
    .local v5, "maxBytesNeeded":I
    iget-object v8, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v8, v8

    if-ge v8, v5, :cond_3

    .line 72
    invoke-virtual {v1, v5}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 76
    :cond_3
    iget-object v8, p0, Lorg/apache/lucene/facet/associations/AssociationsListBuilder;->output:Lorg/apache/lucene/store/ByteArrayDataOutput;

    iget-object v9, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v10, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget-object v11, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v11, v11

    iget v12, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v11, v12

    invoke-virtual {v8, v9, v10, v11}, Lorg/apache/lucene/store/ByteArrayDataOutput;->reset([BII)V

    .line 77
    iget-object v8, p0, Lorg/apache/lucene/facet/associations/AssociationsListBuilder;->output:Lorg/apache/lucene/store/ByteArrayDataOutput;

    iget-object v9, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "idx":I
    .local v4, "idx":I
    aget v9, v9, v3

    invoke-virtual {v8, v9}, Lorg/apache/lucene/store/ByteArrayDataOutput;->writeInt(I)V

    .line 80
    iget-object v8, p0, Lorg/apache/lucene/facet/associations/AssociationsListBuilder;->output:Lorg/apache/lucene/store/ByteArrayDataOutput;

    invoke-interface {v0, v8}, Lorg/apache/lucene/facet/associations/CategoryAssociation;->serialize(Lorg/apache/lucene/store/ByteArrayDataOutput;)V

    .line 83
    iget-object v8, p0, Lorg/apache/lucene/facet/associations/AssociationsListBuilder;->output:Lorg/apache/lucene/store/ByteArrayDataOutput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/ByteArrayDataOutput;->getPosition()I

    move-result v8

    iput v8, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    move v3, v4

    .end local v4    # "idx":I
    .restart local v3    # "idx":I
    goto :goto_0
.end method
