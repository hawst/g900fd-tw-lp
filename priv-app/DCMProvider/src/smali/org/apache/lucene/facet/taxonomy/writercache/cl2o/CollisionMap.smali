.class public Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;
.super Ljava/lang/Object;
.source "CollisionMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;,
        Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;
    }
.end annotation


# instance fields
.field private capacity:I

.field private entries:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

.field private labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

.field private loadFactor:F

.field private size:I

.field private threshold:I


# direct methods
.method private constructor <init>(IFLorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V
    .locals 2
    .param p1, "initialCapacity"    # I
    .param p2, "loadFactor"    # F
    .param p3, "labelRepository"    # Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    .line 66
    iput p2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->loadFactor:F

    .line 67
    const/4 v0, 0x2

    invoke-static {v0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->determineCapacity(II)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->capacity:I

    .line 69
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->capacity:I

    new-array v0, v0, [Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->entries:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 70
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->capacity:I

    int-to-float v0, v0

    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->loadFactor:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->threshold:I

    .line 71
    return-void
.end method

.method constructor <init>(ILorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V
    .locals 1
    .param p1, "initialCapacity"    # I
    .param p2, "labelRepository"    # Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    .prologue
    .line 61
    const/high16 v0, 0x3f400000    # 0.75f

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;-><init>(IFLorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V

    .line 62
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V
    .locals 2
    .param p1, "labelRepository"    # Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    .prologue
    .line 57
    const/16 v0, 0x4000

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {p0, v0, v1, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;-><init>(IFLorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V

    .line 58
    return-void
.end method

.method private addEntry(IIII)V
    .locals 3
    .param p1, "offset"    # I
    .param p2, "cid"    # I
    .param p3, "hash"    # I
    .param p4, "bucketIndex"    # I

    .prologue
    .line 146
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->entries:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    aget-object v0, v1, p4

    .line 147
    .local v0, "e":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->entries:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    new-instance v2, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    invoke-direct {v2, p1, p2, p3, v0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;-><init>(IIILorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;)V

    aput-object v2, v1, p4

    .line 148
    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->size:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->size:I

    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->threshold:I

    if-lt v1, v2, :cond_0

    .line 149
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->grow()V

    .line 151
    :cond_0
    return-void
.end method

.method private grow()V
    .locals 10

    .prologue
    .line 82
    iget v8, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->capacity:I

    mul-int/lit8 v4, v8, 0x2

    .line 83
    .local v4, "newCapacity":I
    new-array v5, v4, [Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 84
    .local v5, "newEntries":[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    iget-object v7, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->entries:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 86
    .local v7, "src":[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    array-length v8, v7

    if-lt v3, v8, :cond_0

    .line 101
    iput v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->capacity:I

    .line 102
    iput-object v5, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->entries:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 103
    iget v8, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->capacity:I

    int-to-float v8, v8

    iget v9, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->loadFactor:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->threshold:I

    .line 104
    return-void

    .line 87
    :cond_0
    aget-object v0, v7, v3

    .line 88
    .local v0, "e":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    if-eqz v0, :cond_2

    .line 89
    const/4 v8, 0x0

    aput-object v8, v7, v3

    .line 91
    :cond_1
    iget-object v6, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 92
    .local v6, "next":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    iget v1, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->hash:I

    .line 93
    .local v1, "hash":I
    invoke-static {v1, v4}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->indexFor(II)I

    move-result v2

    .line 94
    .local v2, "i":I
    aget-object v8, v5, v2

    iput-object v8, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .line 95
    aput-object v0, v5, v2

    .line 96
    move-object v0, v6

    .line 97
    if-nez v0, :cond_1

    .line 86
    .end local v1    # "hash":I
    .end local v2    # "i":I
    .end local v6    # "next":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method static indexFor(II)I
    .locals 1
    .param p0, "h"    # I
    .param p1, "length"    # I

    .prologue
    .line 161
    add-int/lit8 v0, p1, -0x1

    and-int/2addr v0, p0

    return v0
.end method


# virtual methods
.method public addLabel(Lorg/apache/lucene/facet/taxonomy/CategoryPath;II)I
    .locals 5
    .param p1, "label"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "hash"    # I
    .param p3, "cid"    # I

    .prologue
    .line 121
    iget v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->capacity:I

    invoke-static {p2, v3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->indexFor(II)I

    move-result v0

    .line 122
    .local v0, "bucketIndex":I
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->entries:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    aget-object v1, v3, v0

    .local v1, "e":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    :goto_0
    if-nez v1, :cond_0

    .line 129
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    invoke-virtual {v3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->length()I

    move-result v2

    .line 130
    .local v2, "offset":I
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    invoke-static {p1, v3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CategoryPathUtils;->serialize(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V

    .line 131
    invoke-direct {p0, v2, p3, p2, v0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->addEntry(IIII)V

    .line 132
    .end local v2    # "offset":I
    .end local p3    # "cid":I
    :goto_1
    return p3

    .line 123
    .restart local p3    # "cid":I
    :cond_0
    iget v3, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->hash:I

    if-ne v3, p2, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    iget v4, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->offset:I

    invoke-static {p1, v3, v4}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CategoryPathUtils;->equalsToSerialized(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    iget p3, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->cid:I

    goto :goto_1

    .line 122
    :cond_1
    iget-object v1, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    goto :goto_0
.end method

.method public addLabelOffset(III)V
    .locals 2
    .param p1, "hash"    # I
    .param p2, "offset"    # I
    .param p3, "cid"    # I

    .prologue
    .line 141
    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->capacity:I

    invoke-static {p1, v1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->indexFor(II)I

    move-result v0

    .line 142
    .local v0, "bucketIndex":I
    invoke-direct {p0, p2, p3, p1, v0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->addEntry(IIII)V

    .line 143
    return-void
.end method

.method public capacity()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->capacity:I

    return v0
.end method

.method entryIterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;

    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->entries:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->size:I

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$EntryIterator;-><init>(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;I)V

    return-object v0
.end method

.method public get(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)I
    .locals 4
    .param p1, "label"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "hash"    # I

    .prologue
    .line 107
    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->capacity:I

    invoke-static {p2, v2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->indexFor(II)I

    move-result v0

    .line 108
    .local v0, "bucketIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->entries:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    aget-object v1, v2, v0

    .line 110
    .local v1, "e":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    :goto_0
    if-eqz v1, :cond_0

    iget v2, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->hash:I

    if-ne p2, v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->labelRepository:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    iget v3, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->offset:I

    invoke-static {p1, v2, v3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CategoryPathUtils;->equalsToSerialized(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 113
    :cond_0
    if-nez v1, :cond_2

    .line 114
    const/4 v2, -0x2

    .line 117
    :goto_1
    return v2

    .line 111
    :cond_1
    iget-object v1, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    goto :goto_0

    .line 117
    :cond_2
    iget v2, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->cid:I

    goto :goto_1
.end method

.method getMemoryUsage()I
    .locals 6

    .prologue
    .line 169
    const/4 v2, 0x0

    .line 170
    .local v2, "memoryUsage":I
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->entries:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    if-eqz v3, :cond_0

    .line 171
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->entries:[Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_1

    .line 180
    :cond_0
    return v2

    .line 171
    :cond_1
    aget-object v0, v4, v3

    .line 172
    .local v0, "e":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    if-eqz v0, :cond_2

    .line 173
    add-int/lit8 v2, v2, 0x10

    .line 174
    iget-object v1, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    .local v1, "ee":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    :goto_1
    if-nez v1, :cond_3

    .line 171
    .end local v1    # "ee":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 175
    .restart local v1    # "ee":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;
    :cond_3
    add-int/lit8 v2, v2, 0x10

    .line 174
    iget-object v1, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;->next:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap$Entry;

    goto :goto_1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CollisionMap;->size:I

    return v0
.end method
