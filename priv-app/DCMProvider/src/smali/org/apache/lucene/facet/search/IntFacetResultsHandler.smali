.class public final Lorg/apache/lucene/facet/search/IntFacetResultsHandler;
.super Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;
.source "IntFacetResultsHandler.java"


# instance fields
.field private final values:[I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 1
    .param p1, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p2, "facetRequest"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 38
    invoke-virtual {p3}, Lorg/apache/lucene/facet/search/FacetArrays;->getIntArray()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/search/IntFacetResultsHandler;->values:[I

    .line 39
    return-void
.end method


# virtual methods
.method protected final addSiblings(I[ILorg/apache/lucene/util/PriorityQueue;)I
    .locals 8
    .param p1, "ordinal"    # I
    .param p2, "siblings"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[I",
            "Lorg/apache/lucene/util/PriorityQueue",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 48
    .local p3, "pq":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    invoke-virtual {p3}, Lorg/apache/lucene/util/PriorityQueue;->top()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 49
    .local v1, "top":Lorg/apache/lucene/facet/search/FacetResultNode;
    const/4 v0, 0x0

    .line 50
    .local v0, "numResults":I
    :goto_0
    const/4 v3, -0x1

    if-ne p1, v3, :cond_0

    .line 62
    return v0

    .line 51
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/facet/search/IntFacetResultsHandler;->values:[I

    aget v2, v3, p1

    .line 52
    .local v2, "value":I
    if-lez v2, :cond_1

    .line 53
    add-int/lit8 v0, v0, 0x1

    .line 54
    int-to-double v4, v2

    iget-wide v6, v1, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    cmpl-double v3, v4, v6

    if-lez v3, :cond_1

    .line 55
    int-to-double v4, v2

    iput-wide v4, v1, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    .line 56
    iput p1, v1, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    .line 57
    invoke-virtual {p3}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "top":Lorg/apache/lucene/facet/search/FacetResultNode;
    check-cast v1, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 60
    .restart local v1    # "top":Lorg/apache/lucene/facet/search/FacetResultNode;
    :cond_1
    aget p1, p2, p1

    goto :goto_0
.end method

.method protected final addSiblings(I[ILjava/util/ArrayList;)V
    .locals 4
    .param p1, "ordinal"    # I
    .param p2, "siblings"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[I",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    .local p3, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    :goto_0
    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    .line 76
    return-void

    .line 68
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/search/IntFacetResultsHandler;->values:[I

    aget v1, v2, p1

    .line 69
    .local v1, "value":I
    if-lez v1, :cond_1

    .line 70
    new-instance v0, Lorg/apache/lucene/facet/search/FacetResultNode;

    int-to-double v2, v1

    invoke-direct {v0, p1, v2, v3}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    .line 71
    .local v0, "node":Lorg/apache/lucene/facet/search/FacetResultNode;
    iget-object v2, p0, Lorg/apache/lucene/facet/search/IntFacetResultsHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getPath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v2

    iput-object v2, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 72
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    .end local v0    # "node":Lorg/apache/lucene/facet/search/FacetResultNode;
    :cond_1
    aget p1, p2, p1

    goto :goto_0
.end method

.method protected final valueOf(I)D
    .locals 2
    .param p1, "ordinal"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/facet/search/IntFacetResultsHandler;->values:[I

    aget v0, v0, p1

    int-to-double v0, v0

    return-wide v0
.end method
