.class public Lorg/apache/lucene/facet/collections/ObjectToFloatMap;
.super Ljava/lang/Object;
.source "ObjectToFloatMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/collections/ObjectToFloatMap$IndexIterator;,
        Lorg/apache/lucene/facet/collections/ObjectToFloatMap$KeyIterator;,
        Lorg/apache/lucene/facet/collections/ObjectToFloatMap$ValueIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static defaultCapacity:I


# instance fields
.field baseHash:[I

.field private capacity:I

.field private firstEmpty:I

.field private hashFactor:I

.field keys:[Ljava/lang/Object;

.field next:[I

.field private prev:I

.field private size:I

.field values:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 159
    const/16 v0, 0x10

    sput v0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->defaultCapacity:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 213
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    sget v0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->defaultCapacity:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;-><init>(I)V

    .line 214
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1, "capacity"    # I

    .prologue
    .line 223
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    const/16 v2, 0x10

    iput v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    .line 226
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    if-lt v2, p1, :cond_0

    .line 233
    iget v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    add-int/lit8 v0, v2, 0x1

    .line 235
    .local v0, "arrayLength":I
    new-array v2, v0, [F

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->values:[F

    .line 236
    new-array v2, v0, [Ljava/lang/Object;

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->keys:[Ljava/lang/Object;

    .line 237
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    .line 240
    iget v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    shl-int/lit8 v1, v2, 0x1

    .line 242
    .local v1, "baseHashSize":I
    new-array v2, v1, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->baseHash:[I

    .line 246
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->hashFactor:I

    .line 248
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    .line 250
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->clear()V

    .line 251
    return-void

    .line 228
    .end local v0    # "arrayLength":I
    .end local v1    # "baseHashSize":I
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    goto :goto_0
.end method

.method private findForRemove(Ljava/lang/Object;I)I
    .locals 3
    .param p2, "baseHashIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I)I"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    const/4 v1, 0x0

    .line 386
    iput v1, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->prev:I

    .line 387
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->baseHash:[I

    aget v0, v2, p2

    .line 390
    .local v0, "index":I
    :goto_0
    if-nez v0, :cond_1

    .line 403
    iput v1, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->prev:I

    move v0, v1

    .line 404
    .end local v0    # "index":I
    :cond_0
    return v0

    .line 392
    .restart local v0    # "index":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->keys:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 397
    iput v0, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->prev:I

    .line 398
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    aget v0, v2, v0

    goto :goto_0
.end method

.method private getBaseHashAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 471
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->baseHash:[I

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private prvt_put(Ljava/lang/Object;F)V
    .locals 4
    .param p2, "e"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;F)V"
        }
    .end annotation

    .prologue
    .line 267
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->calcBaseHashIndex(Ljava/lang/Object;)I

    move-result v0

    .line 270
    .local v0, "hashIndex":I
    iget v1, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->firstEmpty:I

    .line 273
    .local v1, "objectIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->firstEmpty:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->firstEmpty:I

    .line 274
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->values:[F

    aput p2, v2, v1

    .line 275
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->keys:[Ljava/lang/Object;

    aput-object p1, v2, v1

    .line 278
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->baseHash:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 279
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->baseHash:[I

    aput v1, v2, v0

    .line 282
    iget v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    .line 283
    return-void
.end method


# virtual methods
.method protected calcBaseHashIndex(Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    .line 289
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->hashFactor:I

    and-int/2addr v0, v1

    return v0
.end method

.method public clear()V
    .locals 5

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    const/4 v4, 0x0

    .line 297
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->baseHash:[I

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 300
    iput v4, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    .line 302
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->values:[F

    const/high16 v3, 0x7fc00000    # NaNf

    aput v3, v2, v4

    .line 307
    const/4 v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->firstEmpty:I

    .line 311
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    if-lt v0, v2, :cond_0

    .line 316
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    aput v4, v2, v3

    .line 317
    return-void

    .line 312
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput v1, v2, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)Z"
        }
    .end annotation

    .prologue
    .line 327
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->find(Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(F)Z
    .locals 2
    .param p1, "o"    # F

    .prologue
    .line 340
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->iterator()Lorg/apache/lucene/facet/collections/FloatIterator;

    move-result-object v0

    .local v0, "iterator":Lorg/apache/lucene/facet/collections/FloatIterator;
    :cond_0
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/FloatIterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 345
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 341
    :cond_1
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/FloatIterator;->next()F

    move-result v1

    cmpl-float v1, p1, v1

    if-nez v1, :cond_0

    .line 342
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    const/4 v5, 0x0

    .line 607
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;

    .line 608
    .local v2, "that":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    invoke-virtual {v2}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size()I

    move-result v6

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size()I

    move-result v7

    if-eq v6, v7, :cond_0

    .line 621
    :goto_0
    return v5

    .line 612
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->keyIterator()Ljava/util/Iterator;

    move-result-object v0

    .line 613
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<TK;>;"
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 621
    const/4 v5, 0x1

    goto :goto_0

    .line 614
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 615
    .local v1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->get(Ljava/lang/Object;)F

    move-result v3

    .line 616
    .local v3, "v1":F
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->get(Ljava/lang/Object;)F

    move-result v4

    .line 617
    .local v4, "v2":F
    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_0
.end method

.method protected find(Ljava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    .line 355
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->calcBaseHashIndex(Ljava/lang/Object;)I

    move-result v0

    .line 358
    .local v0, "baseHashIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->baseHash:[I

    aget v1, v2, v0

    .line 361
    .local v1, "localIndex":I
    :goto_0
    if-nez v1, :cond_1

    .line 373
    const/4 v1, 0x0

    .end local v1    # "localIndex":I
    :cond_0
    return v1

    .line 363
    .restart local v1    # "localIndex":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->keys:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 368
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    aget v1, v2, v1

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)F
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)F"
        }
    .end annotation

    .prologue
    .line 415
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->values:[F

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->find(Ljava/lang/Object;)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method protected grow()V
    .locals 5

    .prologue
    .line 424
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    new-instance v2, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;

    .line 425
    iget v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    mul-int/lit8 v3, v3, 0x2

    .line 424
    invoke-direct {v2, v3}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;-><init>(I)V

    .line 430
    .local v2, "that":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    new-instance v1, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$IndexIterator;

    invoke-direct {v1, p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/ObjectToFloatMap;)V

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/ObjectToFloatMap$IndexIterator;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>.IndexIterator;"
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$IndexIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 436
    iget v3, v2, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    .line 437
    iget v3, v2, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    .line 438
    iget v3, v2, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->firstEmpty:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->firstEmpty:I

    .line 439
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->values:[F

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->values:[F

    .line 440
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->keys:[Ljava/lang/Object;

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->keys:[Ljava/lang/Object;

    .line 441
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    .line 442
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->baseHash:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->baseHash:[I

    .line 443
    iget v3, v2, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->hashFactor:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->hashFactor:I

    .line 444
    return-void

    .line 431
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$IndexIterator;->next()I

    move-result v0

    .line 432
    .local v0, "index":I
    iget-object v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->keys:[Ljava/lang/Object;

    aget-object v3, v3, v0

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->values:[F

    aget v4, v4, v0

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->prvt_put(Ljava/lang/Object;F)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 601
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 451
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    iget v0, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Lorg/apache/lucene/facet/collections/FloatIterator;
    .locals 1

    .prologue
    .line 458
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    new-instance v0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$ValueIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$ValueIterator;-><init>(Lorg/apache/lucene/facet/collections/ObjectToFloatMap;)V

    return-object v0
.end method

.method public keyIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 463
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    new-instance v0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$KeyIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$KeyIterator;-><init>(Lorg/apache/lucene/facet/collections/ObjectToFloatMap;)V

    return-object v0
.end method

.method public put(Ljava/lang/Object;F)F
    .locals 4
    .param p2, "e"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;F)F"
        }
    .end annotation

    .prologue
    .line 483
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->find(Ljava/lang/Object;)I

    move-result v0

    .line 486
    .local v0, "index":I
    if-eqz v0, :cond_0

    .line 488
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->values:[F

    aget v1, v2, v0

    .line 489
    .local v1, "old":F
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->values:[F

    aput p2, v2, v0

    .line 503
    .end local v1    # "old":F
    :goto_0
    return v1

    .line 494
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    iget v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->capacity:I

    if-ne v2, v3, :cond_1

    .line 496
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->grow()V

    .line 501
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->prvt_put(Ljava/lang/Object;F)V

    .line 503
    const/high16 v1, 0x7fc00000    # NaNf

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)F
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)F"
        }
    .end annotation

    .prologue
    .line 514
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->calcBaseHashIndex(Ljava/lang/Object;)I

    move-result v0

    .line 515
    .local v0, "baseHashIndex":I
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->findForRemove(Ljava/lang/Object;I)I

    move-result v1

    .line 516
    .local v1, "index":I
    if-eqz v1, :cond_1

    .line 519
    iget v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->prev:I

    if-nez v2, :cond_0

    .line 520
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->baseHash:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    aget v3, v3, v1

    aput v3, v2, v0

    .line 523
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->prev:I

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    aget v4, v4, v1

    aput v4, v2, v3

    .line 524
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->firstEmpty:I

    aput v3, v2, v1

    .line 525
    iput v1, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->firstEmpty:I

    .line 526
    iget v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    .line 527
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->values:[F

    aget v2, v2, v1

    .line 530
    :goto_0
    return v2

    :cond_1
    const/high16 v2, 0x7fc00000    # NaNf

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 537
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    iget v0, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    return v0
.end method

.method public toArray()[F
    .locals 4

    .prologue
    .line 546
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    const/4 v2, -0x1

    .line 547
    .local v2, "j":I
    iget v3, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->size:I

    new-array v0, v3, [F

    .line 550
    .local v0, "array":[F
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->iterator()Lorg/apache/lucene/facet/collections/FloatIterator;

    move-result-object v1

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/FloatIterator;
    :goto_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/FloatIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 553
    return-object v0

    .line 551
    :cond_0
    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/FloatIterator;->next()F

    move-result v3

    aput v3, v0, v2

    goto :goto_0
.end method

.method public toArray([F)[F
    .locals 3
    .param p1, "a"    # [F

    .prologue
    .line 567
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    const/4 v1, 0x0

    .line 569
    .local v1, "j":I
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->iterator()Lorg/apache/lucene/facet/collections/FloatIterator;

    move-result-object v0

    .local v0, "iterator":Lorg/apache/lucene/facet/collections/FloatIterator;
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 570
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/FloatIterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 573
    :cond_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 574
    const/high16 v2, 0x7fc00000    # NaNf

    aput v2, p1, v1

    .line 577
    :cond_1
    return-object p1

    .line 571
    :cond_2
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/FloatIterator;->next()F

    move-result v2

    aput v2, p1, v1

    .line 570
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 582
    .local p0, "this":Lorg/apache/lucene/facet/collections/ObjectToFloatMap;, "Lorg/apache/lucene/facet/collections/ObjectToFloatMap<TK;>;"
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 583
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/16 v3, 0x7b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 584
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->keyIterator()Ljava/util/Iterator;

    move-result-object v1

    .line 585
    .local v1, "keyIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TK;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 595
    const/16 v3, 0x7d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 596
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 586
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 587
    .local v0, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 588
    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 589
    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->get(Ljava/lang/Object;)F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 590
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 591
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 592
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
