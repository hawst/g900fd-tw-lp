.class public abstract Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;
.super Lorg/apache/lucene/facet/encoding/IntEncoder;
.source "ChunksIntEncoder.java"


# instance fields
.field protected final encodeQueue:Lorg/apache/lucene/util/IntsRef;

.field protected indicator:I

.field protected ordinal:B


# direct methods
.method protected constructor <init>(I)V
    .locals 1
    .param p1, "chunkSize"    # I

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/IntEncoder;-><init>()V

    .line 57
    iput v0, p0, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;->indicator:I

    .line 60
    iput-byte v0, p0, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;->ordinal:B

    .line 63
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v0, p1}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;->encodeQueue:Lorg/apache/lucene/util/IntsRef;

    .line 64
    return-void
.end method


# virtual methods
.method protected encodeChunk(Lorg/apache/lucene/util/BytesRef;)V
    .locals 10
    .param p1, "buf"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/high16 v9, 0xfe00000

    const/high16 v8, -0x10000000

    const v7, 0x1fc000

    const/4 v6, 0x0

    .line 72
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;->encodeQueue:Lorg/apache/lucene/util/IntsRef;

    iget v4, v4, Lorg/apache/lucene/util/IntsRef;->length:I

    mul-int/lit8 v4, v4, 0x4

    add-int v1, v3, v4

    .line 73
    .local v1, "maxBytesRequired":I
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v3, v3

    if-ge v3, v1, :cond_0

    .line 74
    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 77
    :cond_0
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v5, p0, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;->indicator:I

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 78
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;->encodeQueue:Lorg/apache/lucene/util/IntsRef;

    iget v3, v3, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v0, v3, :cond_1

    .line 110
    iput-byte v6, p0, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;->ordinal:B

    .line 111
    iput v6, p0, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;->indicator:I

    .line 112
    iget-object v3, p0, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;->encodeQueue:Lorg/apache/lucene/util/IntsRef;

    iput v6, v3, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 113
    return-void

    .line 81
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;->encodeQueue:Lorg/apache/lucene/util/IntsRef;

    iget-object v3, v3, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v2, v3, v0

    .line 82
    .local v2, "value":I
    and-int/lit8 v3, v2, -0x80

    if-nez v3, :cond_2

    .line 83
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-byte v5, v2

    aput-byte v5, v3, v4

    .line 84
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 78
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :cond_2
    and-int/lit16 v3, v2, -0x4000

    if-nez v3, :cond_3

    .line 86
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int/lit16 v5, v2, 0x3f80

    shr-int/lit8 v5, v5, 0x7

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 87
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x1

    and-int/lit8 v5, v2, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 88
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v3, v3, 0x2

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_1

    .line 89
    :cond_3
    const/high16 v3, -0x200000

    and-int/2addr v3, v2

    if-nez v3, :cond_4

    .line 90
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int v5, v2, v7

    shr-int/lit8 v5, v5, 0xe

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 91
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x1

    and-int/lit16 v5, v2, 0x3f80

    shr-int/lit8 v5, v5, 0x7

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 92
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x2

    and-int/lit8 v5, v2, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 93
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v3, v3, 0x3

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_1

    .line 94
    :cond_4
    and-int v3, v2, v8

    if-nez v3, :cond_5

    .line 95
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int v5, v2, v9

    shr-int/lit8 v5, v5, 0x15

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 96
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x1

    and-int v5, v2, v7

    shr-int/lit8 v5, v5, 0xe

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 97
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x2

    and-int/lit16 v5, v2, 0x3f80

    shr-int/lit8 v5, v5, 0x7

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 98
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x3

    and-int/lit8 v5, v2, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 99
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v3, v3, 0x4

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    goto/16 :goto_1

    .line 101
    :cond_5
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int v5, v2, v8

    shr-int/lit8 v5, v5, 0x1c

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 102
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x1

    and-int v5, v2, v9

    shr-int/lit8 v5, v5, 0x15

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 103
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x2

    and-int v5, v2, v7

    shr-int/lit8 v5, v5, 0xe

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 104
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x3

    and-int/lit16 v5, v2, 0x3f80

    shr-int/lit8 v5, v5, 0x7

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 105
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x4

    and-int/lit8 v5, v2, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 106
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v3, v3, 0x5

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    goto/16 :goto_1
.end method
