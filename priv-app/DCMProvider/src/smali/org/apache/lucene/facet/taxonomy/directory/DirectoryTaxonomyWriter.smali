.class public Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;
.super Ljava/lang/Object;
.source "DirectoryTaxonomyWriter.java"

# interfaces
.implements Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;,
        Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$MemoryOrdinalMap;,
        Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;,
        Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final INDEX_EPOCH:Ljava/lang/String; = "index.epoch"


# instance fields
.field private final cache:Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

.field private volatile cacheIsComplete:Z

.field private final cacheMisses:Ljava/util/concurrent/atomic/AtomicInteger;

.field private cacheMissesUntilFill:I

.field private delimiter:C

.field private final dir:Lorg/apache/lucene/store/Directory;

.field private fullPathField:Lorg/apache/lucene/document/Field;

.field private indexEpoch:J

.field private final indexWriter:Lorg/apache/lucene/index/IndexWriter;

.field private volatile initializedReaderManager:Z

.field private volatile isClosed:Z

.field private volatile nextID:I

.field private parentStream:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;

.field private parentStreamField:Lorg/apache/lucene/document/Field;

.field private readerManager:Lorg/apache/lucene/index/ReaderManager;

.field private shouldFillCache:Z

.field private volatile shouldRefreshReaderManager:Z

.field private volatile taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    const-class v0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->$assertionsDisabled:Z

    .line 97
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;)V
    .locals 1
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 347
    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE_OR_APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)V

    .line 348
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)V
    .locals 1
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "openMode"    # Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 331
    invoke-static {}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->defaultTaxonomyWriterCache()Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;)V

    .line 332
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;)V
    .locals 10
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "openMode"    # Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    .param p3, "cache"    # Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v4, v8}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheMisses:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 107
    const/16 v4, 0x1f

    iput-char v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->delimiter:C

    .line 108
    new-instance v4, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;

    const-string v5, "p"

    invoke-direct {v4, v5}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->parentStream:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;

    .line 111
    const/16 v4, 0xb

    iput v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheMissesUntilFill:I

    .line 112
    iput-boolean v9, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->shouldFillCache:Z

    .line 117
    iput-boolean v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initializedReaderManager:Z

    .line 131
    iput-boolean v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->isClosed:Z

    .line 207
    iput-object p1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->dir:Lorg/apache/lucene/store/Directory;

    .line 208
    invoke-virtual {p0, p2}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->createIndexWriterConfig(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v1

    .line 209
    .local v1, "config":Lorg/apache/lucene/index/IndexWriterConfig;
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p0, v4, v1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->openIndexWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)Lorg/apache/lucene/index/IndexWriter;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    .line 212
    sget-boolean v4, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexWriter;->getConfig()Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMergePolicy()Lorg/apache/lucene/index/MergePolicy;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/lucene/index/TieredMergePolicy;

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    .line 213
    const-string v5, "for preserving category docids, merging none-adjacent segments is not allowed"

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 217
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriterConfig;->getOpenMode()Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    move-result-object p2

    .line 218
    invoke-static {p1}, Lorg/apache/lucene/index/DirectoryReader;->indexExists(Lorg/apache/lucene/store/Directory;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 219
    iput-wide v6, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexEpoch:J

    .line 231
    :goto_0
    sget-object v4, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    if-ne p2, v4, :cond_1

    .line 232
    iget-wide v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexEpoch:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexEpoch:J

    .line 235
    :cond_1
    new-instance v3, Lorg/apache/lucene/document/FieldType;

    sget-object v4, Lorg/apache/lucene/document/TextField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-direct {v3, v4}, Lorg/apache/lucene/document/FieldType;-><init>(Lorg/apache/lucene/document/FieldType;)V

    .line 236
    .local v3, "ft":Lorg/apache/lucene/document/FieldType;
    invoke-virtual {v3, v9}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    .line 237
    new-instance v4, Lorg/apache/lucene/document/Field;

    const-string v5, "$payloads$"

    iget-object v6, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->parentStream:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;

    invoke-direct {v4, v5, v6, v3}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/document/FieldType;)V

    iput-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->parentStreamField:Lorg/apache/lucene/document/Field;

    .line 238
    new-instance v4, Lorg/apache/lucene/document/StringField;

    const-string v5, "$full_path$"

    const-string v6, ""

    sget-object v7, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/lucene/document/StringField;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V

    iput-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->fullPathField:Lorg/apache/lucene/document/Field;

    .line 240
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexWriter;->maxDoc()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->nextID:I

    .line 242
    if-nez p3, :cond_2

    .line 243
    invoke-static {}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->defaultTaxonomyWriterCache()Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    move-result-object p3

    .line 245
    :cond_2
    iput-object p3, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    .line 247
    iget v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->nextID:I

    if-nez v4, :cond_6

    .line 248
    iput-boolean v9, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheIsComplete:Z

    .line 251
    sget-object v4, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->EMPTY:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->addCategory(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    .line 261
    :goto_1
    return-void

    .line 221
    .end local v3    # "ft":Lorg/apache/lucene/document/FieldType;
    :cond_3
    const/4 v2, 0x0

    .line 222
    .local v2, "epochStr":Ljava/lang/String;
    invoke-static {p1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readCommitData(Lorg/apache/lucene/store/Directory;)Ljava/util/Map;

    move-result-object v0

    .line 223
    .local v0, "commitData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_4

    .line 224
    const-string v4, "index.epoch"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "epochStr":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 228
    .restart local v2    # "epochStr":Ljava/lang/String;
    :cond_4
    if-nez v2, :cond_5

    move-wide v4, v6

    :goto_2
    iput-wide v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexEpoch:J

    goto :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    goto :goto_2

    .line 259
    .end local v0    # "commitData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "epochStr":Ljava/lang/String;
    .restart local v3    # "ft":Lorg/apache/lucene/document/FieldType;
    :cond_6
    iput-boolean v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheIsComplete:Z

    goto :goto_1
.end method

.method private addCategoryDocument(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)I
    .locals 5
    .param p1, "categoryPath"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "parent"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 530
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->parentStream:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;

    add-int/lit8 v3, p2, 0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;->set(I)V

    .line 531
    new-instance v0, Lorg/apache/lucene/document/Document;

    invoke-direct {v0}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 532
    .local v0, "d":Lorg/apache/lucene/document/Document;
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->parentStreamField:Lorg/apache/lucene/document/Field;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 534
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->fullPathField:Lorg/apache/lucene/document/Field;

    iget-char v3, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->delimiter:C

    invoke-virtual {p1, v3}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/document/Field;->setStringValue(Ljava/lang/String;)V

    .line 535
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->fullPathField:Lorg/apache/lucene/document/Field;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 540
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/IndexWriter;->addDocument(Ljava/lang/Iterable;)V

    .line 541
    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->nextID:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->nextID:I

    .line 544
    .local v1, "id":I
    iput-boolean v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->shouldRefreshReaderManager:Z

    .line 547
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->getTaxoArrays()Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    move-result-object v2

    invoke-virtual {v2, v1, p2}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->add(II)Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    .line 551
    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->addToCache(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V

    .line 553
    return v1
.end method

.method private addToCache(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V
    .locals 1
    .param p1, "categoryPath"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 592
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    invoke-interface {v0, p1, p2}, Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;->put(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->refreshReaderManager()V

    .line 599
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheIsComplete:Z

    .line 601
    :cond_0
    return-void
.end method

.method private combinedCommitData(Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 626
    .local p1, "commitData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 627
    .local v0, "m":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 628
    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 630
    :cond_0
    const-string v1, "index.epoch"

    iget-wide v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexEpoch:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 631
    return-object v0
.end method

.method public static defaultTaxonomyWriterCache()Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;
    .locals 4

    .prologue
    .line 343
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;

    const/16 v1, 0x400

    const v2, 0x3e19999a    # 0.15f

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;-><init>(IFI)V

    return-object v0
.end method

.method private doClose()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 365
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 366
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->isClosed:Z

    .line 367
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->closeResources()V

    .line 368
    return-void
.end method

.method private getTaxoArrays()Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 749
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    if-nez v2, :cond_1

    .line 750
    monitor-enter p0

    .line 751
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    if-nez v2, :cond_0

    .line 752
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initReaderManager()V

    .line 753
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ReaderManager;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DirectoryReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 758
    .local v0, "reader":Lorg/apache/lucene/index/DirectoryReader;
    :try_start_1
    new-instance v1, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    invoke-direct {v1, v0}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    .line 759
    .local v1, "tmpArrays":Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    iput-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 761
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/ReaderManager;->release(Ljava/lang/Object;)V

    .line 750
    .end local v0    # "reader":Lorg/apache/lucene/index/DirectoryReader;
    .end local v1    # "tmpArrays":Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    :cond_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 766
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    return-object v2

    .line 760
    .restart local v0    # "reader":Lorg/apache/lucene/index/DirectoryReader;
    :catchall_0
    move-exception v2

    .line 761
    :try_start_3
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/ReaderManager;->release(Ljava/lang/Object;)V

    .line 762
    throw v2

    .line 750
    .end local v0    # "reader":Lorg/apache/lucene/index/DirectoryReader;
    :catchall_1
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method

.method private initReaderManager()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 312
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initializedReaderManager:Z

    if-nez v0, :cond_1

    .line 313
    monitor-enter p0

    .line 315
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->ensureOpen()V

    .line 316
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initializedReaderManager:Z

    if-nez v0, :cond_0

    .line 317
    new-instance v0, Lorg/apache/lucene/index/ReaderManager;

    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/ReaderManager;-><init>(Lorg/apache/lucene/index/IndexWriter;Z)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    .line 318
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->shouldRefreshReaderManager:Z

    .line 319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initializedReaderManager:Z

    .line 313
    :cond_0
    monitor-exit p0

    .line 323
    :cond_1
    return-void

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private internalAddCategory(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    .locals 5
    .param p1, "cp"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 490
    iget v3, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-le v3, v4, :cond_1

    .line 491
    iget v3, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v3}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->subpath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v2

    .line 492
    .local v2, "parentPath":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    invoke-virtual {p0, v2}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->findCategory(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v1

    .line 493
    .local v1, "parent":I
    if-gez v1, :cond_0

    .line 494
    invoke-direct {p0, v2}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->internalAddCategory(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v1

    .line 501
    .end local v2    # "parentPath":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    :cond_0
    :goto_0
    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->addCategoryDocument(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)I

    move-result v0

    .line 503
    .local v0, "id":I
    return v0

    .line 496
    .end local v0    # "id":I
    .end local v1    # "parent":I
    :cond_1
    iget v3, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-ne v3, v4, :cond_2

    .line 497
    const/4 v1, 0x0

    .line 498
    .restart local v1    # "parent":I
    goto :goto_0

    .line 499
    .end local v1    # "parent":I
    :cond_2
    const/4 v1, -0x1

    .restart local v1    # "parent":I
    goto :goto_0
.end method

.method private declared-synchronized perhapsFillCache()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 687
    monitor-enter p0

    :try_start_0
    iget-object v10, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheMisses:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v10

    iget v11, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheMissesUntilFill:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-ge v10, v11, :cond_1

    .line 746
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 691
    :cond_1
    :try_start_1
    iget-boolean v10, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->shouldFillCache:Z

    if-eqz v10, :cond_0

    .line 695
    const/4 v10, 0x0

    iput-boolean v10, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->shouldFillCache:Z

    .line 697
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initReaderManager()V

    .line 699
    const/4 v0, 0x0

    .line 700
    .local v0, "aborted":Z
    iget-object v10, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v10}, Lorg/apache/lucene/index/ReaderManager;->acquire()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/DirectoryReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 702
    .local v4, "reader":Lorg/apache/lucene/index/DirectoryReader;
    const/4 v8, 0x0

    .line 703
    .local v8, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v3, 0x0

    .line 704
    .local v3, "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    :try_start_2
    invoke-virtual {v4}, Lorg/apache/lucene/index/DirectoryReader;->leaves()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v11

    if-nez v11, :cond_3

    .line 732
    :goto_1
    :try_start_3
    iget-object v10, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v10, v4}, Lorg/apache/lucene/index/ReaderManager;->release(Ljava/lang/Object;)V

    .line 735
    if-eqz v0, :cond_8

    :goto_2
    iput-boolean v9, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheIsComplete:Z

    .line 736
    iget-boolean v9, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheIsComplete:Z

    if-eqz v9, :cond_0

    .line 737
    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 741
    :try_start_4
    iget-object v9, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v9}, Lorg/apache/lucene/index/ReaderManager;->close()V

    .line 742
    const/4 v9, 0x0

    iput-object v9, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    .line 743
    const/4 v9, 0x0

    iput-boolean v9, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initializedReaderManager:Z

    .line 737
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v9

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 687
    .end local v0    # "aborted":Z
    .end local v3    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    .end local v4    # "reader":Lorg/apache/lucene/index/DirectoryReader;
    .end local v8    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :catchall_1
    move-exception v9

    monitor-exit p0

    throw v9

    .line 704
    .restart local v0    # "aborted":Z
    .restart local v3    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    .restart local v4    # "reader":Lorg/apache/lucene/index/DirectoryReader;
    .restart local v8    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_3
    :try_start_6
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 705
    .local v2, "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v11

    const-string v12, "$full_path$"

    invoke-virtual {v11, v12}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v7

    .line 706
    .local v7, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v7, :cond_5

    .line 707
    invoke-virtual {v7, v8}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v8

    .line 708
    :cond_4
    invoke-virtual {v8}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v11

    if-nez v11, :cond_6

    .line 727
    :cond_5
    :goto_3
    if-eqz v0, :cond_2

    goto :goto_1

    .line 709
    :cond_6
    iget-object v11, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    invoke-interface {v11}, Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;->isFull()Z

    move-result v11

    if-nez v11, :cond_7

    .line 710
    invoke-virtual {v8}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    .line 716
    .local v6, "t":Lorg/apache/lucene/util/BytesRef;
    new-instance v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v11

    iget-char v12, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->delimiter:C

    invoke-direct {v1, v11, v12}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>(Ljava/lang/String;C)V

    .line 717
    .local v1, "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v3, v12}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v3

    .line 718
    iget-object v11, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v12

    iget v13, v2, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    add-int/2addr v12, v13

    invoke-interface {v11, v1, v12}, Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;->put(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)Z

    move-result v5

    .line 719
    .local v5, "res":Z
    sget-boolean v11, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->$assertionsDisabled:Z

    if-nez v11, :cond_4

    if-eqz v5, :cond_4

    new-instance v9, Ljava/lang/AssertionError;

    const-string v10, "entries should not have been evicted from the cache"

    invoke-direct {v9, v10}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 731
    .end local v1    # "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .end local v2    # "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v5    # "res":Z
    .end local v6    # "t":Lorg/apache/lucene/util/BytesRef;
    .end local v7    # "terms":Lorg/apache/lucene/index/Terms;
    :catchall_2
    move-exception v9

    .line 732
    :try_start_7
    iget-object v10, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v10, v4}, Lorg/apache/lucene/index/ReaderManager;->release(Ljava/lang/Object;)V

    .line 733
    throw v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 722
    .restart local v2    # "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    .restart local v7    # "terms":Lorg/apache/lucene/index/Terms;
    :cond_7
    const/4 v0, 0x1

    .line 723
    goto :goto_3

    .line 735
    .end local v2    # "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v7    # "terms":Lorg/apache/lucene/index/Terms;
    :cond_8
    const/4 v9, 0x1

    goto :goto_2
.end method

.method private static readCommitData(Lorg/apache/lucene/store/Directory;)Ljava/util/Map;
    .locals 2
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    new-instance v0, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v0}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 138
    .local v0, "infos":Lorg/apache/lucene/index/SegmentInfos;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 139
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method

.method private declared-synchronized refreshReaderManager()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 611
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->shouldRefreshReaderManager:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initializedReaderManager:Z

    if-eqz v0, :cond_0

    .line 612
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v0}, Lorg/apache/lucene/index/ReaderManager;->maybeRefresh()Z

    .line 613
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->shouldRefreshReaderManager:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 615
    :cond_0
    monitor-exit p0

    return-void

    .line 611
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static unlock(Lorg/apache/lucene/store/Directory;)V
    .locals 0
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    invoke-static {p0}, Lorg/apache/lucene/index/IndexWriter;->unlock(Lorg/apache/lucene/store/Directory;)V

    .line 173
    return-void
.end method


# virtual methods
.method public addCategory(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    .locals 2
    .param p1, "categoryPath"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 453
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->ensureOpen()V

    .line 456
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    invoke-interface {v1, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;->get(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v0

    .line 457
    .local v0, "res":I
    if-gez v0, :cond_1

    .line 459
    monitor-enter p0

    .line 460
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->findCategory(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v0

    .line 461
    if-gez v0, :cond_0

    .line 468
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->internalAddCategory(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v0

    .line 459
    :cond_0
    monitor-exit p0

    .line 472
    :cond_1
    return v0

    .line 459
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public addTaxonomy(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;)V
    .locals 15
    .param p1, "taxoDir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "map"    # Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 791
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->ensureOpen()V

    .line 792
    invoke-static/range {p1 .. p1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v7

    .line 794
    .local v7, "r":Lorg/apache/lucene/index/DirectoryReader;
    :try_start_0
    invoke-virtual {v7}, Lorg/apache/lucene/index/DirectoryReader;->numDocs()I

    move-result v8

    .line 795
    .local v8, "size":I
    move-object/from16 v6, p2

    .line 796
    .local v6, "ordinalMap":Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;
    invoke-interface {v6, v8}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;->setSize(I)V

    .line 797
    const/4 v1, 0x0

    .line 798
    .local v1, "base":I
    const/4 v9, 0x0

    .line 799
    .local v9, "te":Lorg/apache/lucene/index/TermsEnum;
    const/4 v4, 0x0

    .line 800
    .local v4, "docs":Lorg/apache/lucene/index/DocsEnum;
    invoke-virtual {v7}, Lorg/apache/lucene/index/DirectoryReader;->leaves()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_0

    .line 813
    invoke-interface {v6}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;->addDone()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 815
    invoke-virtual {v7}, Lorg/apache/lucene/index/DirectoryReader;->close()V

    .line 817
    return-void

    .line 800
    :cond_0
    :try_start_1
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 801
    .local v3, "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    .line 802
    .local v0, "ar":Lorg/apache/lucene/index/AtomicReader;
    const-string v13, "$full_path$"

    invoke-virtual {v0, v13}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v10

    .line 803
    .local v10, "terms":Lorg/apache/lucene/index/Terms;
    invoke-virtual {v10, v9}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v9

    .line 804
    :goto_1
    invoke-virtual {v9}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v13

    if-nez v13, :cond_1

    .line 811
    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v13

    add-int/2addr v1, v13

    goto :goto_0

    .line 805
    :cond_1
    invoke-virtual {v9}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v13

    invoke-virtual {v13}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v11

    .line 806
    .local v11, "value":Ljava/lang/String;
    new-instance v2, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget-char v13, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->delimiter:C

    invoke-direct {v2, v11, v13}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>(Ljava/lang/String;C)V

    .line 807
    .local v2, "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    invoke-virtual {p0, v2}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->addCategory(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v5

    .line 808
    .local v5, "ordinal":I
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v9, v13, v4, v14}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v4

    .line 809
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v13

    add-int/2addr v13, v1

    invoke-interface {v6, v13, v5}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;->addMapping(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 814
    .end local v0    # "ar":Lorg/apache/lucene/index/AtomicReader;
    .end local v1    # "base":I
    .end local v2    # "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .end local v3    # "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v4    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v5    # "ordinal":I
    .end local v6    # "ordinalMap":Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;
    .end local v8    # "size":I
    .end local v9    # "te":Lorg/apache/lucene/index/TermsEnum;
    .end local v10    # "terms":Lorg/apache/lucene/index/Terms;
    .end local v11    # "value":Ljava/lang/String;
    :catchall_0
    move-exception v12

    .line 815
    invoke-virtual {v7}, Lorg/apache/lucene/index/DirectoryReader;->close()V

    .line 816
    throw v12
.end method

.method public declared-synchronized close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 357
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->isClosed:Z

    if-nez v0, :cond_0

    .line 358
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter;->getCommitData()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->combinedCommitData(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->setCommitData(Ljava/util/Map;)V

    .line 359
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->commit()V

    .line 360
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->doClose()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 362
    :cond_0
    monitor-exit p0

    return-void

    .line 357
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized closeResources()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 378
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initializedReaderManager:Z

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v0}, Lorg/apache/lucene/index/ReaderManager;->close()V

    .line 380
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    .line 381
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initializedReaderManager:Z

    .line 383
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    if-eqz v0, :cond_1

    .line 384
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    invoke-interface {v0}, Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    :cond_1
    monitor-exit p0

    return-void

    .line 378
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized commit()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 619
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->ensureOpen()V

    .line 620
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter;->getCommitData()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->combinedCommitData(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->setCommitData(Ljava/util/Map;)V

    .line 621
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 622
    monitor-exit p0

    return-void

    .line 619
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected createIndexWriterConfig(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 3
    .param p1, "openMode"    # Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    .prologue
    .line 305
    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_43:Lorg/apache/lucene/util/Version;

    .line 306
    const/4 v2, 0x0

    .line 305
    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 306
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    .line 307
    new-instance v1, Lorg/apache/lucene/index/LogByteSizeMergePolicy;

    invoke-direct {v1}, Lorg/apache/lucene/index/LogByteSizeMergePolicy;-><init>()V

    .line 306
    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriterConfig;->setMergePolicy(Lorg/apache/lucene/index/MergePolicy;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    .line 305
    return-object v0
.end method

.method protected final ensureOpen()V
    .locals 2

    .prologue
    .line 511
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->isClosed:Z

    if-eqz v0, :cond_0

    .line 512
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "The taxonomy writer has already been closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 514
    :cond_0
    return-void
.end method

.method protected declared-synchronized findCategory(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    .locals 11
    .param p1, "categoryPath"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 396
    monitor-enter p0

    :try_start_0
    iget-object v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    invoke-interface {v8, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;->get(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v5

    .line 397
    .local v5, "res":I
    if-gez v5, :cond_0

    iget-boolean v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheIsComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v8, :cond_2

    :cond_0
    move v2, v5

    .line 448
    :cond_1
    :goto_0
    monitor-exit p0

    return v2

    .line 401
    :cond_2
    :try_start_1
    iget-object v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheMisses:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 409
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->perhapsFillCache()V

    .line 410
    iget-object v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    invoke-interface {v8, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;->get(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v5

    .line 411
    if-gez v5, :cond_3

    iget-boolean v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheIsComplete:Z

    if-eqz v8, :cond_4

    :cond_3
    move v2, v5

    .line 414
    goto :goto_0

    .line 421
    :cond_4
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initReaderManager()V

    .line 423
    const/4 v2, -0x1

    .line 424
    .local v2, "doc":I
    iget-object v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v8}, Lorg/apache/lucene/index/ReaderManager;->acquire()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/DirectoryReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 426
    .local v4, "reader":Lorg/apache/lucene/index/DirectoryReader;
    :try_start_2
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    iget-char v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->delimiter:C

    invoke-virtual {p1, v8}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    .line 427
    .local v0, "catTerm":Lorg/apache/lucene/util/BytesRef;
    const/4 v7, 0x0

    .line 428
    .local v7, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v3, 0x0

    .line 429
    .local v3, "docs":Lorg/apache/lucene/index/DocsEnum;
    invoke-virtual {v4}, Lorg/apache/lucene/index/DirectoryReader;->leaves()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v9

    if-nez v9, :cond_6

    .line 443
    :goto_1
    :try_start_3
    iget-object v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v8, v4}, Lorg/apache/lucene/index/ReaderManager;->release(Ljava/lang/Object;)V

    .line 445
    if-lez v2, :cond_1

    .line 446
    invoke-direct {p0, p1, v2}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->addToCache(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 396
    .end local v0    # "catTerm":Lorg/apache/lucene/util/BytesRef;
    .end local v2    # "doc":I
    .end local v3    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v4    # "reader":Lorg/apache/lucene/index/DirectoryReader;
    .end local v5    # "res":I
    .end local v7    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 429
    .restart local v0    # "catTerm":Lorg/apache/lucene/util/BytesRef;
    .restart local v2    # "doc":I
    .restart local v3    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .restart local v4    # "reader":Lorg/apache/lucene/index/DirectoryReader;
    .restart local v5    # "res":I
    .restart local v7    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_6
    :try_start_4
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 430
    .local v1, "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v9

    const-string v10, "$full_path$"

    invoke-virtual {v9, v10}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v6

    .line 431
    .local v6, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v6, :cond_5

    .line 432
    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v7

    .line 433
    const/4 v9, 0x1

    invoke-virtual {v7, v0, v9}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 435
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v3, v9}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v3

    .line 437
    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v8

    iget v9, v1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    add-int v2, v8, v9

    .line 438
    goto :goto_1

    .line 442
    .end local v0    # "catTerm":Lorg/apache/lucene/util/BytesRef;
    .end local v1    # "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v3    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v6    # "terms":Lorg/apache/lucene/index/Terms;
    .end local v7    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :catchall_1
    move-exception v8

    .line 443
    :try_start_5
    iget-object v9, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->readerManager:Lorg/apache/lucene/index/ReaderManager;

    invoke-virtual {v9, v4}, Lorg/apache/lucene/index/ReaderManager;->release(Ljava/lang/Object;)V

    .line 444
    throw v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public getCommitData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 641
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->getCommitData()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->combinedCommitData(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 981
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->dir:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method final getInternalIndexWriter()Lorg/apache/lucene/index/IndexWriter;
    .locals 1

    .prologue
    .line 992
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    return-object v0
.end method

.method public getParent(I)I
    .locals 4
    .param p1, "ordinal"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 771
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->ensureOpen()V

    .line 775
    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->nextID:I

    if-lt p1, v1, :cond_0

    .line 776
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v2, "requested ordinal is bigger than the largest ordinal in the taxonomy"

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 779
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->getTaxoArrays()Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents()[I

    move-result-object v0

    .line 780
    .local v0, "parents":[I
    sget-boolean v1, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    array-length v1, v0

    if-lt p1, v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requested ordinal ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "); parents.length ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") !"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 781
    :cond_1
    aget v1, v0, p1

    return v1
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 657
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->ensureOpen()V

    .line 658
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->nextID:I

    return v0
.end method

.method public final getTaxonomyEpoch()J
    .locals 2

    .prologue
    .line 1001
    iget-wide v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexEpoch:J

    return-wide v0
.end method

.method protected openIndexWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)Lorg/apache/lucene/index/IndexWriter;
    .locals 1
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "config"    # Lorg/apache/lucene/index/IndexWriterConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 282
    new-instance v0, Lorg/apache/lucene/index/IndexWriter;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    return-object v0
.end method

.method public declared-synchronized prepareCommit()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 650
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->ensureOpen()V

    .line 651
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter;->getCommitData()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->combinedCommitData(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->setCommitData(Ljava/util/Map;)V

    .line 652
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->prepareCommit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 653
    monitor-exit p0

    return-void

    .line 650
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized replaceTaxonomy(Lorg/apache/lucene/store/Directory;)V
    .locals 4
    .param p1, "taxoDir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 962
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->deleteAll()V

    .line 963
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    const/4 v1, 0x1

    new-array v1, v1, [Lorg/apache/lucene/store/Directory;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->addIndexes([Lorg/apache/lucene/store/Directory;)V

    .line 964
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->shouldRefreshReaderManager:Z

    .line 965
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->initReaderManager()V

    .line 966
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->refreshReaderManager()V

    .line 967
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->maxDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->nextID:I

    .line 971
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;

    invoke-interface {v0}, Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;->clear()V

    .line 972
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheIsComplete:Z

    .line 973
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->shouldFillCache:Z

    .line 976
    iget-wide v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexEpoch:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexEpoch:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 977
    monitor-exit p0

    return-void

    .line 962
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized rollback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 949
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->ensureOpen()V

    .line 950
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->rollback()V

    .line 951
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->doClose()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 952
    monitor-exit p0

    return-void

    .line 949
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setCacheMissesUntilFill(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 679
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->ensureOpen()V

    .line 680
    iput p1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->cacheMissesUntilFill:I

    .line 681
    return-void
.end method

.method public setCommitData(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 636
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->combinedCommitData(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->setCommitData(Ljava/util/Map;)V

    .line 637
    return-void
.end method

.method public setDelimiter(C)V
    .locals 0
    .param p1, "delimiter"    # C

    .prologue
    .line 155
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->ensureOpen()V

    .line 156
    iput-char p1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->delimiter:C

    .line 157
    return-void
.end method
