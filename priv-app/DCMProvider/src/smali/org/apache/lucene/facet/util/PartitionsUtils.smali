.class public final Lorg/apache/lucene/facet/util/PartitionsUtils;
.super Ljava/lang/Object;
.source "PartitionsUtils.java"


# static fields
.field public static final PART_NAME_PREFIX:Ljava/lang/String; = "$part"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final partitionName(I)Ljava/lang/String;
    .locals 2
    .param p0, "partition"    # I

    .prologue
    .line 63
    if-nez p0, :cond_0

    .line 66
    const-string v0, ""

    .line 68
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "$part"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final partitionNameByOrdinal(Lorg/apache/lucene/facet/params/FacetIndexingParams;I)Ljava/lang/String;
    .locals 2
    .param p0, "iParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p1, "ordinal"    # I

    .prologue
    .line 55
    invoke-static {p0, p1}, Lorg/apache/lucene/facet/util/PartitionsUtils;->partitionNumber(Lorg/apache/lucene/facet/params/FacetIndexingParams;I)I

    move-result v0

    .line 56
    .local v0, "partition":I
    invoke-static {v0}, Lorg/apache/lucene/facet/util/PartitionsUtils;->partitionName(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static final partitionNumber(Lorg/apache/lucene/facet/params/FacetIndexingParams;I)I
    .locals 1
    .param p0, "iParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p1, "ordinal"    # I

    .prologue
    .line 48
    invoke-virtual {p0}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getPartitionSize()I

    move-result v0

    div-int v0, p1, v0

    return v0
.end method

.method public static final partitionSize(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)I
    .locals 2
    .param p0, "indexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p1, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .prologue
    .line 38
    invoke-virtual {p0}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getPartitionSize()I

    move-result v0

    invoke-virtual {p1}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getSize()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method
