.class public Lorg/apache/lucene/facet/search/SumScoreFacetRequest;
.super Lorg/apache/lucene/facet/search/FacetRequest;
.source "SumScoreFacetRequest.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/facet/search/SumScoreFacetRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/search/SumScoreFacetRequest;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V
    .locals 0
    .param p1, "path"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "num"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/search/FacetRequest;-><init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V

    .line 34
    return-void
.end method


# virtual methods
.method public createAggregator(ZLorg/apache/lucene/facet/search/FacetArrays;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/Aggregator;
    .locals 2
    .param p1, "useComplements"    # Z
    .param p2, "arrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p3, "taxonomy"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .prologue
    .line 38
    sget-boolean v0, Lorg/apache/lucene/facet/search/SumScoreFacetRequest;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "complements are not supported by this FacetRequest"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 39
    :cond_0
    new-instance v0, Lorg/apache/lucene/facet/search/ScoringAggregator;

    invoke-virtual {p2}, Lorg/apache/lucene/facet/search/FacetArrays;->getFloatArray()[F

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/search/ScoringAggregator;-><init>([F)V

    return-object v0
.end method

.method public getFacetArraysSource()Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->FLOAT:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    return-object v0
.end method

.method public getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D
    .locals 2
    .param p1, "arrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p2, "ordinal"    # I

    .prologue
    .line 44
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetArrays;->getFloatArray()[F

    move-result-object v0

    aget v0, v0, p2

    float-to-double v0, v0

    return-wide v0
.end method
