.class public Lorg/apache/lucene/facet/search/ScoringAggregator;
.super Ljava/lang/Object;
.source "ScoringAggregator.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/Aggregator;


# instance fields
.field private final hashCode:I

.field private final scoreArray:[F


# direct methods
.method public constructor <init>([F)V
    .locals 1
    .param p1, "counterArray"    # [F

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/facet/search/ScoringAggregator;->scoreArray:[F

    .line 38
    iget-object v0, p0, Lorg/apache/lucene/facet/search/ScoringAggregator;->scoreArray:[F

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lorg/apache/lucene/facet/search/ScoringAggregator;->hashCode:I

    .line 39
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/search/ScoringAggregator;->scoreArray:[F

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public aggregate(IFLorg/apache/lucene/util/IntsRef;)V
    .locals 4
    .param p1, "docID"    # I
    .param p2, "score"    # F
    .param p3, "ordinals"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v0, v1, :cond_0

    .line 46
    return-void

    .line 44
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/facet/search/ScoringAggregator;->scoreArray:[F

    iget-object v2, p3, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v2, v2, v0

    aget v3, v1, v2

    add-float/2addr v3, p2

    aput v3, v1, v2

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 50
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 53
    check-cast v0, Lorg/apache/lucene/facet/search/ScoringAggregator;

    .line 54
    .local v0, "that":Lorg/apache/lucene/facet/search/ScoringAggregator;
    iget-object v2, v0, Lorg/apache/lucene/facet/search/ScoringAggregator;->scoreArray:[F

    iget-object v3, p0, Lorg/apache/lucene/facet/search/ScoringAggregator;->scoreArray:[F

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lorg/apache/lucene/facet/search/ScoringAggregator;->hashCode:I

    return v0
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Z
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    const/4 v0, 0x1

    return v0
.end method
