.class final Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;
.super Ljava/lang/Object;
.source "IntHashSet.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/IntIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/IntHashSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "IndexIterator"
.end annotation


# instance fields
.field private baseHashIndex:I

.field private index:I

.field private lastIndex:I

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/IntHashSet;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/collections/IntHashSet;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 58
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntHashSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->baseHashIndex:I

    .line 46
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->index:I

    .line 51
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->lastIndex:I

    .line 59
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->baseHashIndex:I

    :goto_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->baseHashIndex:I

    iget-object v1, p1, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 65
    :cond_0
    return-void

    .line 60
    :cond_1
    iget-object v0, p1, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->baseHashIndex:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->index:I

    .line 61
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->index:I

    if-nez v0, :cond_0

    .line 59
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->baseHashIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->baseHashIndex:I

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->index:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()I
    .locals 2

    .prologue
    .line 75
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->index:I

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->lastIndex:I

    .line 78
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntHashSet;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntHashSet;->next:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->index:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->index:I

    .line 83
    :goto_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->index:I

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->baseHashIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->baseHashIndex:I

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntHashSet;

    iget-object v1, v1, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 87
    :cond_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->lastIndex:I

    return v0

    .line 84
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntHashSet;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntHashSet;->baseHash:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->baseHashIndex:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->index:I

    goto :goto_0
.end method

.method public remove()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntHashSet;

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntHashSet;

    iget-object v1, v1, Lorg/apache/lucene/facet/collections/IntHashSet;->keys:[I

    iget v2, p0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;->lastIndex:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/collections/IntHashSet;->remove(I)Z

    .line 93
    return-void
.end method
