.class public final Lorg/apache/lucene/facet/encoding/DGapIntDecoder;
.super Lorg/apache/lucene/facet/encoding/IntDecoder;
.source "DGapIntDecoder.java"


# instance fields
.field private final decoder:Lorg/apache/lucene/facet/encoding/IntDecoder;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/encoding/IntDecoder;)V
    .locals 0
    .param p1, "decoder"    # Lorg/apache/lucene/facet/encoding/IntDecoder;

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/IntDecoder;-><init>()V

    .line 34
    iput-object p1, p0, Lorg/apache/lucene/facet/encoding/DGapIntDecoder;->decoder:Lorg/apache/lucene/facet/encoding/IntDecoder;

    .line 35
    return-void
.end method


# virtual methods
.method public decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V
    .locals 4
    .param p1, "buf"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "values"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 39
    iget-object v2, p0, Lorg/apache/lucene/facet/encoding/DGapIntDecoder;->decoder:Lorg/apache/lucene/facet/encoding/IntDecoder;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/facet/encoding/IntDecoder;->decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V

    .line 40
    const/4 v1, 0x0

    .line 41
    .local v1, "prev":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v0, v2, :cond_0

    .line 45
    return-void

    .line 42
    :cond_0
    iget-object v2, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v3, v2, v0

    add-int/2addr v3, v1

    aput v3, v2, v0

    .line 43
    iget-object v2, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v1, v2, v0

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DGap("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/facet/encoding/DGapIntDecoder;->decoder:Lorg/apache/lucene/facet/encoding/IntDecoder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
