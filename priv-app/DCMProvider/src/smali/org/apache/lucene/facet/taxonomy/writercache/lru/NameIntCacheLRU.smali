.class Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;
.super Ljava/lang/Object;
.source "NameIntCacheLRU.java"


# instance fields
.field private cache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private maxCacheSize:I

.field nHits:J

.field nMisses:J


# direct methods
.method constructor <init>(I)V
    .locals 2
    .param p1, "maxCacheSize"    # I

    .prologue
    const-wide/16 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-wide v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->nMisses:J

    .line 37
    iput-wide v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->nHits:J

    .line 41
    iput p1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->maxCacheSize:I

    .line 42
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->createCache(I)V

    .line 43
    return-void
.end method

.method private createCache(I)V
    .locals 4
    .param p1, "maxSize"    # I

    .prologue
    const/16 v3, 0x3e8

    const v2, 0x3f333333    # 0.7f

    .line 54
    const v0, 0x7fffffff

    if-ge p1, v0, :cond_0

    .line 55
    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v3, v2, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->cache:Ljava/util/HashMap;

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3, v2}, Ljava/util/HashMap;-><init>(IF)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->cache:Ljava/util/HashMap;

    goto :goto_0
.end method

.method private isCacheFull()Z
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->cache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->maxCacheSize:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->cache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 100
    return-void
.end method

.method get(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Ljava/lang/Integer;
    .locals 6
    .param p1, "name"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    const-wide/16 v4, 0x1

    .line 62
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->cache:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->key(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 63
    .local v0, "res":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 64
    iget-wide v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->nMisses:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->nMisses:J

    .line 68
    :goto_0
    return-object v0

    .line 66
    :cond_0
    iget-wide v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->nHits:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->nHits:J

    goto :goto_0
.end method

.method public getMaxSize()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->maxCacheSize:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->cache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method key(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Ljava/lang/Object;
    .locals 0
    .param p1, "name"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    .line 73
    return-object p1
.end method

.method key(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)Ljava/lang/Object;
    .locals 1
    .param p1, "name"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "prefixLen"    # I

    .prologue
    .line 77
    invoke-virtual {p1, p2}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->subpath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v0

    return-object v0
.end method

.method makeRoomLRU()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 114
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->isCacheFull()Z

    move-result v4

    if-nez v4, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v3

    .line 117
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->cache:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    iget v5, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->maxCacheSize:I

    mul-int/lit8 v5, v5, 0x2

    div-int/lit8 v5, v5, 0x3

    sub-int v2, v4, v5

    .line 118
    .local v2, "n":I
    if-lez v2, :cond_0

    .line 121
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->cache:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 122
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Object;>;"
    const/4 v0, 0x0

    .line 123
    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 128
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 124
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 125
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method put(Lorg/apache/lucene/facet/taxonomy/CategoryPath;ILjava/lang/Integer;)Z
    .locals 2
    .param p1, "name"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "prefixLen"    # I
    .param p3, "val"    # Ljava/lang/Integer;

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->cache:Ljava/util/HashMap;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->key(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->isCacheFull()Z

    move-result v0

    return v0
.end method

.method put(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Ljava/lang/Integer;)Z
    .locals 2
    .param p1, "name"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "val"    # Ljava/lang/Integer;

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->cache:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->key(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->isCacheFull()Z

    move-result v0

    return v0
.end method

.method stats()Ljava/lang/String;
    .locals 4

    .prologue
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#miss="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->nMisses:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " #hit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->nHits:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
