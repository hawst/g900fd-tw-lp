.class Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;
.super Lorg/apache/lucene/facet/search/FacetRequest;
.source "Sampler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/sampling/Sampler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OverSampledFacetRequest"
.end annotation


# instance fields
.field final orig:Lorg/apache/lucene/facet/search/FacetRequest;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/search/FacetRequest;I)V
    .locals 1
    .param p1, "orig"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p2, "num"    # I

    .prologue
    .line 220
    iget-object v0, p1, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-direct {p0, v0, p2}, Lorg/apache/lucene/facet/search/FacetRequest;-><init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V

    .line 221
    iput-object p1, p0, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;->orig:Lorg/apache/lucene/facet/search/FacetRequest;

    .line 222
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetRequest;->getDepth()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;->setDepth(I)V

    .line 223
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetRequest;->getNumLabel()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;->setNumLabel(I)V

    .line 224
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetRequest;->getResultMode()Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;->setResultMode(Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;)V

    .line 225
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetRequest;->getSortOrder()Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;->setSortOrder(Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;)V

    .line 226
    return-void
.end method


# virtual methods
.method public createAggregator(ZLorg/apache/lucene/facet/search/FacetArrays;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/Aggregator;
    .locals 1
    .param p1, "useComplements"    # Z
    .param p2, "arrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p3, "taxonomy"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    iget-object v0, p0, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;->orig:Lorg/apache/lucene/facet/search/FacetRequest;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/facet/search/FacetRequest;->createAggregator(ZLorg/apache/lucene/facet/search/FacetArrays;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/Aggregator;

    move-result-object v0

    return-object v0
.end method

.method public getFacetArraysSource()Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;->orig:Lorg/apache/lucene/facet/search/FacetRequest;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/search/FacetRequest;->getFacetArraysSource()Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    move-result-object v0

    return-object v0
.end method

.method public getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D
    .locals 2
    .param p1, "arrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p2, "idx"    # I

    .prologue
    .line 241
    iget-object v0, p0, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;->orig:Lorg/apache/lucene/facet/search/FacetRequest;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/facet/search/FacetRequest;->getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D

    move-result-wide v0

    return-wide v0
.end method
