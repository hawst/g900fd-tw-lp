.class final Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;
.super Ljava/lang/Object;
.source "IntToIntMap.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/IntIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/IntToIntMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "IndexIterator"
.end annotation


# instance fields
.field private baseHashIndex:I

.field private index:I

.field private lastIndex:I

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/IntToIntMap;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/collections/IntToIntMap;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 65
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToIntMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->baseHashIndex:I

    .line 53
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->index:I

    .line 58
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->lastIndex:I

    .line 66
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->baseHashIndex:I

    :goto_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->baseHashIndex:I

    iget-object v1, p1, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 72
    :cond_0
    return-void

    .line 67
    :cond_1
    iget-object v0, p1, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->baseHashIndex:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->index:I

    .line 68
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->index:I

    if-nez v0, :cond_0

    .line 66
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->baseHashIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->baseHashIndex:I

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->index:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()I
    .locals 2

    .prologue
    .line 82
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->index:I

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->lastIndex:I

    .line 85
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToIntMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->index:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->index:I

    .line 90
    :goto_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->index:I

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->baseHashIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->baseHashIndex:I

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToIntMap;

    iget-object v1, v1, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 94
    :cond_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->lastIndex:I

    return v0

    .line 91
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToIntMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->baseHashIndex:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->index:I

    goto :goto_0
.end method

.method public remove()V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToIntMap;

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToIntMap;

    iget-object v1, v1, Lorg/apache/lucene/facet/collections/IntToIntMap;->keys:[I

    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->lastIndex:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/collections/IntToIntMap;->remove(I)I

    .line 100
    return-void
.end method
