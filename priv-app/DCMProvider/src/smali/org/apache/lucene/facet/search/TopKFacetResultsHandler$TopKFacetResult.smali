.class Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;
.super Lorg/apache/lucene/facet/search/FacetResult;
.source "TopKFacetResultsHandler.java"

# interfaces
.implements Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TopKFacetResult"
.end annotation


# instance fields
.field private heap:Lorg/apache/lucene/facet/search/Heap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/facet/search/Heap",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V
    .locals 0
    .param p1, "facetRequest"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p2, "facetResultNode"    # Lorg/apache/lucene/facet/search/FacetResultNode;
    .param p3, "totalFacets"    # I

    .prologue
    .line 261
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/facet/search/FacetResult;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    .line 262
    return-void
.end method


# virtual methods
.method public getHeap()Lorg/apache/lucene/facet/search/Heap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/facet/search/Heap",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    iget-object v0, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;->heap:Lorg/apache/lucene/facet/search/Heap;

    return-object v0
.end method

.method public setHeap(Lorg/apache/lucene/facet/search/Heap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/search/Heap",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 276
    .local p1, "heap":Lorg/apache/lucene/facet/search/Heap;, "Lorg/apache/lucene/facet/search/Heap<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    iput-object p1, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;->heap:Lorg/apache/lucene/facet/search/Heap;

    .line 277
    return-void
.end method
