.class public Lorg/apache/lucene/facet/search/OrdinalsCache;
.super Ljava/lang/Object;
.source "OrdinalsCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;
    }
.end annotation


# static fields
.field private static final intsCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/BinaryDocValues;",
            "Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lorg/apache/lucene/facet/search/OrdinalsCache;->intsCache:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getCachedOrds(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/facet/params/CategoryListParams;)Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;
    .locals 5
    .param p0, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p1, "clp"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    const-class v3, Lorg/apache/lucene/facet/search/OrdinalsCache;

    monitor-enter v3

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v4, p1, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 107
    .local v1, "dv":Lorg/apache/lucene/index/BinaryDocValues;
    if-nez v1, :cond_1

    .line 108
    const/4 v0, 0x0

    .line 115
    :cond_0
    :goto_0
    monitor-exit v3

    return-object v0

    .line 110
    :cond_1
    :try_start_1
    sget-object v2, Lorg/apache/lucene/facet/search/OrdinalsCache;->intsCache:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;

    .line 111
    .local v0, "ci":Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;
    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;

    .end local v0    # "ci":Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    invoke-direct {v0, v1, v2, p1}, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;-><init>(Lorg/apache/lucene/index/BinaryDocValues;ILorg/apache/lucene/facet/params/CategoryListParams;)V

    .line 113
    .restart local v0    # "ci":Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;
    sget-object v2, Lorg/apache/lucene/facet/search/OrdinalsCache;->intsCache:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 106
    .end local v0    # "ci":Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;
    .end local v1    # "dv":Lorg/apache/lucene/index/BinaryDocValues;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method
