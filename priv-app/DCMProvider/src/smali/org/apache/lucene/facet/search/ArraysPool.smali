.class public final Lorg/apache/lucene/facet/search/ArraysPool;
.super Ljava/lang/Object;
.source "ArraysPool.java"


# instance fields
.field public final arrayLength:I

.field private final floatsPool:Ljava/util/concurrent/ArrayBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ArrayBlockingQueue",
            "<[F>;"
        }
    .end annotation
.end field

.field private final intsPool:Ljava/util/concurrent/ArrayBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ArrayBlockingQueue",
            "<[I>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(II)V
    .locals 2
    .param p1, "arrayLength"    # I
    .param p2, "maxArrays"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    if-nez p2, :cond_0

    .line 54
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 55
    const-string v1, "maxArrays cannot be 0 - don\'t use this class if you don\'t intend to pool arrays"

    .line 54
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    iput p1, p0, Lorg/apache/lucene/facet/search/ArraysPool;->arrayLength:I

    .line 58
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, p2}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/ArraysPool;->intsPool:Ljava/util/concurrent/ArrayBlockingQueue;

    .line 59
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, p2}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/ArraysPool;->floatsPool:Ljava/util/concurrent/ArrayBlockingQueue;

    .line 60
    return-void
.end method


# virtual methods
.method public final allocateFloatArray()[F
    .locals 2

    .prologue
    .line 80
    iget-object v1, p0, Lorg/apache/lucene/facet/search/ArraysPool;->floatsPool:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ArrayBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    .line 81
    .local v0, "arr":[F
    if-nez v0, :cond_0

    .line 82
    iget v1, p0, Lorg/apache/lucene/facet/search/ArraysPool;->arrayLength:I

    new-array v0, v1, [F

    .line 85
    .end local v0    # "arr":[F
    :goto_0
    return-object v0

    .line 84
    .restart local v0    # "arr":[F
    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    goto :goto_0
.end method

.method public final allocateIntArray()[I
    .locals 2

    .prologue
    .line 67
    iget-object v1, p0, Lorg/apache/lucene/facet/search/ArraysPool;->intsPool:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ArrayBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 68
    .local v0, "arr":[I
    if-nez v0, :cond_0

    .line 69
    iget v1, p0, Lorg/apache/lucene/facet/search/ArraysPool;->arrayLength:I

    new-array v0, v1, [I

    .line 72
    .end local v0    # "arr":[I
    :goto_0
    return-object v0

    .line 71
    .restart local v0    # "arr":[I
    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    goto :goto_0
.end method

.method public final free([F)V
    .locals 1
    .param p1, "arr"    # [F

    .prologue
    .line 104
    if-eqz p1, :cond_0

    .line 106
    iget-object v0, p0, Lorg/apache/lucene/facet/search/ArraysPool;->floatsPool:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ArrayBlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 108
    :cond_0
    return-void
.end method

.method public final free([I)V
    .locals 1
    .param p1, "arr"    # [I

    .prologue
    .line 93
    if-eqz p1, :cond_0

    .line 95
    iget-object v0, p0, Lorg/apache/lucene/facet/search/ArraysPool;->intsPool:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ArrayBlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 97
    :cond_0
    return-void
.end method
