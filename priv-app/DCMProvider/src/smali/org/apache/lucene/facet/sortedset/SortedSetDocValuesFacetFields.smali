.class public Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesFacetFields;
.super Lorg/apache/lucene/facet/index/FacetFields;
.source "SortedSetDocValuesFacetFields.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->DEFAULT:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesFacetFields;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 2
    .param p1, "fip"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/facet/index/FacetFields;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V

    .line 54
    invoke-virtual {p1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getPartitionSize()I

    move-result v0

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "partitions are not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method public addFields(Lorg/apache/lucene/document/Document;Ljava/lang/Iterable;)V
    .locals 12
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/document/Document;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    .local p2, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    if-nez p2, :cond_0

    .line 62
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "categories should not be null"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 65
    :cond_0
    invoke-virtual {p0, p2}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesFacetFields;->createCategoryListMapping(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v0

    .line 66
    .local v0, "categoryLists":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/facet/params/CategoryListParams;Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 84
    return-void

    .line 66
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 68
    .local v6, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/facet/params/CategoryListParams;Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 69
    .local v1, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v9, v1, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "_sorted_doc_values"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 72
    .local v5, "dvField":Ljava/lang/String;
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Iterable;

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_2

    .line 80
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Iterable;

    invoke-virtual {p0, v7}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesFacetFields;->getDrillDownStream(Ljava/lang/Iterable;)Lorg/apache/lucene/facet/index/DrillDownStream;

    move-result-object v4

    .line 81
    .local v4, "drillDownStream":Lorg/apache/lucene/facet/index/DrillDownStream;
    new-instance v3, Lorg/apache/lucene/document/Field;

    iget-object v7, v1, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesFacetFields;->drillDownFieldType()Lorg/apache/lucene/document/FieldType;

    move-result-object v9

    invoke-direct {v3, v7, v4, v9}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/document/FieldType;)V

    .line 82
    .local v3, "drillDown":Lorg/apache/lucene/document/Field;
    invoke-virtual {p1, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_0

    .line 72
    .end local v3    # "drillDown":Lorg/apache/lucene/document/Field;
    .end local v4    # "drillDownStream":Lorg/apache/lucene/facet/index/DrillDownStream;
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 73
    .local v2, "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    iget v9, v2, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    const/4 v10, 0x2

    if-eq v9, v10, :cond_3

    .line 74
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "only flat facets (dimension + label) are currently supported; got "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 76
    :cond_3
    new-instance v9, Lorg/apache/lucene/document/SortedSetDocValuesField;

    new-instance v10, Lorg/apache/lucene/util/BytesRef;

    iget-object v11, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesFacetFields;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-virtual {v11}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getFacetDelimChar()C

    move-result v11

    invoke-virtual {v2, v11}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {v9, v5, v10}, Lorg/apache/lucene/document/SortedSetDocValuesField;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    invoke-virtual {p1, v9}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_1
.end method
