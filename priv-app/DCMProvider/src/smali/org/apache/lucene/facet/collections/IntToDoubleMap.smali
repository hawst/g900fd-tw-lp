.class public Lorg/apache/lucene/facet/collections/IntToDoubleMap;
.super Ljava/lang/Object;
.source "IntToDoubleMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;,
        Lorg/apache/lucene/facet/collections/IntToDoubleMap$KeyIterator;,
        Lorg/apache/lucene/facet/collections/IntToDoubleMap$ValueIterator;
    }
.end annotation


# static fields
.field public static final GROUND:D = NaN

.field private static defaultCapacity:I


# instance fields
.field baseHash:[I

.field private capacity:I

.field private firstEmpty:I

.field private hashFactor:I

.field keys:[I

.field next:[I

.field private prev:I

.field private size:I

.field values:[D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    const/16 v0, 0x10

    sput v0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->defaultCapacity:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 211
    sget v0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->defaultCapacity:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;-><init>(I)V

    .line 212
    return-void
.end method

.method public constructor <init>(I)V
    .locals 6
    .param p1, "capacity"    # I

    .prologue
    const/4 v3, 0x0

    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    const/16 v2, 0x10

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    .line 224
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    if-lt v2, p1, :cond_0

    .line 231
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    add-int/lit8 v0, v2, 0x1

    .line 233
    .local v0, "arrayLength":I
    new-array v2, v0, [D

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->values:[D

    .line 234
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->keys:[I

    .line 235
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    .line 238
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    shl-int/lit8 v1, v2, 0x1

    .line 240
    .local v1, "baseHashSize":I
    new-array v2, v1, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    .line 242
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->values:[D

    const-wide/high16 v4, 0x7ff8000000000000L    # NaN

    aput-wide v4, v2, v3

    .line 246
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->hashFactor:I

    .line 248
    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    .line 250
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->clear()V

    .line 251
    return-void

    .line 226
    .end local v0    # "arrayLength":I
    .end local v1    # "baseHashSize":I
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    goto :goto_0
.end method

.method private findForRemove(II)I
    .locals 3
    .param p1, "key"    # I
    .param p2, "baseHashIndex"    # I

    .prologue
    const/4 v1, 0x0

    .line 385
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->prev:I

    .line 386
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    aget v0, v2, p2

    .line 389
    .local v0, "index":I
    :goto_0
    if-nez v0, :cond_1

    .line 402
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->prev:I

    move v0, v1

    .line 403
    .end local v0    # "index":I
    :cond_0
    return v0

    .line 391
    .restart local v0    # "index":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->keys:[I

    aget v2, v2, v0

    if-eq v2, p1, :cond_0

    .line 396
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->prev:I

    .line 397
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    aget v0, v2, v0

    goto :goto_0
.end method

.method private getBaseHashAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private prvt_put(ID)V
    .locals 4
    .param p1, "key"    # I
    .param p2, "v"    # D

    .prologue
    .line 267
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 270
    .local v0, "hashIndex":I
    iget v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->firstEmpty:I

    .line 273
    .local v1, "objectIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->firstEmpty:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->firstEmpty:I

    .line 274
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->values:[D

    aput-wide p2, v2, v1

    .line 275
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->keys:[I

    aput p1, v2, v1

    .line 278
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 279
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    aput v1, v2, v0

    .line 282
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    .line 283
    return-void
.end method


# virtual methods
.method protected calcBaseHashIndex(I)I
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 290
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->hashFactor:I

    and-int/2addr v0, p1

    return v0
.end method

.method public clear()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 298
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 301
    iput v4, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    .line 306
    const/4 v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->firstEmpty:I

    .line 310
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    if-lt v0, v2, :cond_0

    .line 315
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    aput v4, v2, v3

    .line 316
    return-void

    .line 311
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput v1, v2, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public containsKey(I)Z
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 326
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->find(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(D)Z
    .locals 5
    .param p1, "value"    # D

    .prologue
    .line 338
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->iterator()Lorg/apache/lucene/facet/collections/DoubleIterator;

    move-result-object v2

    .local v2, "iterator":Lorg/apache/lucene/facet/collections/DoubleIterator;
    :cond_0
    invoke-interface {v2}, Lorg/apache/lucene/facet/collections/DoubleIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 344
    const/4 v3, 0x0

    :goto_0
    return v3

    .line 339
    :cond_1
    invoke-interface {v2}, Lorg/apache/lucene/facet/collections/DoubleIterator;->next()D

    move-result-wide v0

    .line 340
    .local v0, "d":D
    cmpl-double v3, v0, p1

    if-nez v3, :cond_0

    .line 341
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 611
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/facet/collections/IntToDoubleMap;

    .line 612
    .local v2, "that":Lorg/apache/lucene/facet/collections/IntToDoubleMap;
    invoke-virtual {v2}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size()I

    move-result v8

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size()I

    move-result v9

    if-eq v8, v9, :cond_1

    .line 629
    :cond_0
    :goto_0
    return v3

    .line 616
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v0

    .line 617
    .local v0, "it":Lorg/apache/lucene/facet/collections/IntIterator;
    :cond_2
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 629
    const/4 v3, 0x1

    goto :goto_0

    .line 618
    :cond_3
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    .line 619
    .local v1, "key":I
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->containsKey(I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 623
    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->get(I)D

    move-result-wide v4

    .line 624
    .local v4, "v1":D
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->get(I)D

    move-result-wide v6

    .line 625
    .local v6, "v2":D
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Double;->compare(DD)I

    move-result v8

    if-eqz v8, :cond_2

    goto :goto_0
.end method

.method protected find(I)I
    .locals 3
    .param p1, "key"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 357
    .local v0, "baseHashIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    aget v1, v2, v0

    .line 360
    .local v1, "localIndex":I
    :goto_0
    if-nez v1, :cond_1

    .line 372
    const/4 v1, 0x0

    .end local v1    # "localIndex":I
    :cond_0
    return v1

    .line 362
    .restart local v1    # "localIndex":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->keys:[I

    aget v2, v2, v1

    if-eq v2, p1, :cond_0

    .line 367
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    aget v1, v2, v1

    goto :goto_0
.end method

.method public get(I)D
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 414
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->values:[D

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->find(I)I

    move-result v1

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method protected grow()V
    .locals 6

    .prologue
    .line 422
    new-instance v2, Lorg/apache/lucene/facet/collections/IntToDoubleMap;

    .line 423
    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    mul-int/lit8 v3, v3, 0x2

    .line 422
    invoke-direct {v2, v3}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;-><init>(I)V

    .line 428
    .local v2, "that":Lorg/apache/lucene/facet/collections/IntToDoubleMap;
    new-instance v1, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;

    invoke-direct {v1, p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToDoubleMap;)V

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 434
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    .line 435
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    .line 436
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->firstEmpty:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->firstEmpty:I

    .line 437
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->values:[D

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->values:[D

    .line 438
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->keys:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->keys:[I

    .line 439
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    .line 440
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    .line 441
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->hashFactor:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->hashFactor:I

    .line 442
    return-void

    .line 429
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->next()I

    move-result v0

    .line 430
    .local v0, "index":I
    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->keys:[I

    aget v3, v3, v0

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->values:[D

    aget-wide v4, v4, v0

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->prvt_put(ID)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 606
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 449
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Lorg/apache/lucene/facet/collections/DoubleIterator;
    .locals 1

    .prologue
    .line 456
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$ValueIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap$ValueIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToDoubleMap;)V

    return-object v0
.end method

.method public keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;
    .locals 1

    .prologue
    .line 461
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$KeyIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap$KeyIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToDoubleMap;)V

    return-object v0
.end method

.method public put(ID)D
    .locals 6
    .param p1, "key"    # I
    .param p2, "v"    # D

    .prologue
    .line 481
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->find(I)I

    move-result v0

    .line 484
    .local v0, "index":I
    if-eqz v0, :cond_0

    .line 486
    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->values:[D

    aget-wide v2, v1, v0

    .line 487
    .local v2, "old":D
    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->values:[D

    aput-wide p2, v1, v0

    .line 501
    .end local v2    # "old":D
    :goto_0
    return-wide v2

    .line 492
    :cond_0
    iget v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    iget v4, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->capacity:I

    if-ne v1, v4, :cond_1

    .line 494
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->grow()V

    .line 499
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->prvt_put(ID)V

    .line 501
    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    goto :goto_0
.end method

.method public remove(I)D
    .locals 5
    .param p1, "key"    # I

    .prologue
    .line 512
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 513
    .local v0, "baseHashIndex":I
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->findForRemove(II)I

    move-result v1

    .line 514
    .local v1, "index":I
    if-eqz v1, :cond_1

    .line 517
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->prev:I

    if-nez v2, :cond_0

    .line 518
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    aget v3, v3, v1

    aput v3, v2, v0

    .line 521
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->prev:I

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    aget v4, v4, v1

    aput v4, v2, v3

    .line 522
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->firstEmpty:I

    aput v3, v2, v1

    .line 523
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->firstEmpty:I

    .line 524
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    .line 525
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->values:[D

    aget-wide v2, v2, v1

    .line 528
    :goto_0
    return-wide v2

    :cond_1
    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 535
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    return v0
.end method

.method public toArray()[D
    .locals 6

    .prologue
    .line 544
    const/4 v2, -0x1

    .line 545
    .local v2, "j":I
    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size:I

    new-array v0, v3, [D

    .line 548
    .local v0, "array":[D
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->iterator()Lorg/apache/lucene/facet/collections/DoubleIterator;

    move-result-object v1

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/DoubleIterator;
    :goto_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/DoubleIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 551
    return-object v0

    .line 549
    :cond_0
    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/DoubleIterator;->next()D

    move-result-wide v4

    aput-wide v4, v0, v2

    goto :goto_0
.end method

.method public toArray([D)[D
    .locals 4
    .param p1, "a"    # [D

    .prologue
    .line 568
    const/4 v1, 0x0

    .line 569
    .local v1, "j":I
    array-length v2, p1

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 570
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->size()I

    move-result v2

    new-array p1, v2, [D

    .line 574
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->iterator()Lorg/apache/lucene/facet/collections/DoubleIterator;

    move-result-object v0

    .local v0, "iterator":Lorg/apache/lucene/facet/collections/DoubleIterator;
    :goto_0
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/DoubleIterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 578
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 579
    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    aput-wide v2, p1, v1

    .line 582
    :cond_1
    return-object p1

    .line 575
    :cond_2
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/DoubleIterator;->next()D

    move-result-wide v2

    aput-wide v2, p1, v1

    .line 574
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 587
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 588
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/16 v3, 0x7b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 589
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v1

    .line 590
    .local v1, "keyIterator":Lorg/apache/lucene/facet/collections/IntIterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 600
    const/16 v3, 0x7d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 601
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 591
    :cond_1
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v0

    .line 592
    .local v0, "key":I
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 593
    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 594
    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->get(I)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    .line 595
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 596
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 597
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
