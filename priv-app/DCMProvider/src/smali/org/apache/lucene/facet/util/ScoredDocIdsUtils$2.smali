.class Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;
.super Ljava/lang/Object;
.source "ScoredDocIdsUtils.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/ScoredDocIDs;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/util/ScoredDocIdsUtils;->createScoredDocIds(Lorg/apache/lucene/search/DocIdSet;I)Lorg/apache/lucene/facet/search/ScoredDocIDs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private size:I

.field private final synthetic val$docIdSet:Lorg/apache/lucene/search/DocIdSet;

.field private final synthetic val$maxDoc:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/DocIdSet;I)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;->val$docIdSet:Lorg/apache/lucene/search/DocIdSet;

    iput p2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;->val$maxDoc:I

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;->size:I

    return-void
.end method


# virtual methods
.method public getDocIDs()Lorg/apache/lucene/search/DocIdSet;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;->val$docIdSet:Lorg/apache/lucene/search/DocIdSet;

    return-object v0
.end method

.method public iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 220
    iget-object v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;->val$docIdSet:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v1}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    .line 221
    .local v0, "docIterator":Lorg/apache/lucene/search/DocIdSetIterator;
    new-instance v1, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2$1;-><init>(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;Lorg/apache/lucene/search/DocIdSetIterator;)V

    return-object v1
.end method

.method public size()I
    .locals 4

    .prologue
    .line 242
    iget v2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;->size:I

    if-gez v2, :cond_0

    .line 245
    :try_start_0
    new-instance v1, Lorg/apache/lucene/util/OpenBitSetDISI;

    iget-object v2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;->val$docIdSet:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;->val$maxDoc:I

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/util/OpenBitSetDISI;-><init>(Lorg/apache/lucene/search/DocIdSetIterator;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    .local v1, "openBitSetDISI":Lorg/apache/lucene/util/OpenBitSetDISI;
    invoke-virtual {v1}, Lorg/apache/lucene/util/OpenBitSetDISI;->cardinality()J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;->size:I

    .line 251
    .end local v1    # "openBitSetDISI":Lorg/apache/lucene/util/OpenBitSetDISI;
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;->size:I

    return v2

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method
