.class public Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;
.super Ljava/lang/Object;
.source "TopKInEachNodeHandler.java"

# interfaces
.implements Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IntermediateFacetResultWithHash"
.end annotation


# instance fields
.field facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

.field isRootNodeIncluded:Z

.field protected mapToAACOs:Lorg/apache/lucene/facet/collections/IntToObjectMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/facet/collections/IntToObjectMap",
            "<",
            "Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;",
            ">;"
        }
    .end annotation
.end field

.field rootNodeValue:D

.field totalNumOfFacetsConsidered:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/collections/IntToObjectMap;)V
    .locals 3
    .param p1, "facetReq"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/search/FacetRequest;",
            "Lorg/apache/lucene/facet/collections/IntToObjectMap",
            "<",
            "Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "mapToAACOs":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;>;"
    const/4 v2, 0x0

    .line 593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 595
    iput-object p2, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->mapToAACOs:Lorg/apache/lucene/facet/collections/IntToObjectMap;

    .line 596
    iput-object p1, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    .line 597
    iput-boolean v2, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->isRootNodeIncluded:Z

    .line 598
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->rootNodeValue:D

    .line 599
    iput v2, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->totalNumOfFacetsConsidered:I

    .line 600
    return-void
.end method


# virtual methods
.method public getFacetRequest()Lorg/apache/lucene/facet/search/FacetRequest;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    return-object v0
.end method
