.class public final Lorg/apache/lucene/facet/search/DrillDownQuery;
.super Lorg/apache/lucene/search/Query;
.source "DrillDownQuery.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final drillDownDims:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

.field private final query:Lorg/apache/lucene/search/BooleanQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lorg/apache/lucene/facet/search/DrillDownQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/search/DrillDownQuery;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 1
    .param p1, "fip"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .prologue
    .line 124
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/search/DrillDownQuery;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/Query;)V

    .line 125
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/BooleanQuery;Ljava/util/Map;)V
    .locals 1
    .param p1, "fip"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p2, "query"    # Lorg/apache/lucene/search/BooleanQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/params/FacetIndexingParams;",
            "Lorg/apache/lucene/search/BooleanQuery;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p3, "drillDownDims":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 65
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    .line 70
    iput-object p1, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 71
    invoke-virtual {p2}, Lorg/apache/lucene/search/BooleanQuery;->clone()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    .line 72
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/Query;)V
    .locals 2
    .param p1, "fip"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p2, "baseQuery"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 133
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 65
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    .line 134
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    .line 135
    if-eqz p2, :cond_0

    .line 136
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, p2, v1}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 138
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 139
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/Query;Ljava/util/List;)V
    .locals 5
    .param p1, "fip"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p2, "baseQuery"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/params/FacetIndexingParams;",
            "Lorg/apache/lucene/search/Query;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Query;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p3, "clauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 65
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    .line 94
    iput-object p1, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 95
    new-instance v1, Lorg/apache/lucene/search/BooleanQuery;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    iput-object v1, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    .line 96
    if-eqz p2, :cond_0

    .line 97
    iget-object v1, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, p2, v2}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 99
    :cond_0
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 103
    return-void

    .line 99
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/Query;

    .line 100
    .local v0, "clause":Lorg/apache/lucene/search/Query;
    iget-object v2, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v0, v3}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 101
    iget-object v2, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/search/DrillDownQuery;->getDim(Lorg/apache/lucene/search/Query;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/facet/search/DrillDownQuery;)V
    .locals 5
    .param p1, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p2, "other"    # Lorg/apache/lucene/facet/search/DrillDownQuery;

    .prologue
    .line 76
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 65
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v2, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    .line 77
    new-instance v2, Lorg/apache/lucene/search/BooleanQuery;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    iput-object v2, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    .line 79
    iget-object v2, p2, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v2}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v0

    .line 80
    .local v0, "clauses":[Lorg/apache/lucene/search/BooleanClause;
    array-length v2, v0

    iget-object v3, p2, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 81
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "cannot apply filter unless baseQuery isn\'t null; pass ConstantScoreQuery instead"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 83
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/facet/search/DrillDownQuery;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    array-length v2, v0

    iget-object v3, p2, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    if-eq v2, v3, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    array-length v4, v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " vs "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 84
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    iget-object v3, p2, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 85
    iget-object v2, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    new-instance v3, Lorg/apache/lucene/search/FilteredQuery;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v4

    invoke-direct {v3, v4, p1}, Lorg/apache/lucene/search/FilteredQuery;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)V

    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 86
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-lt v1, v2, :cond_2

    .line 89
    iget-object v2, p2, Lorg/apache/lucene/facet/search/DrillDownQuery;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    iput-object v2, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 90
    return-void

    .line 87
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    aget-object v3, v0, v1

    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static term(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/index/Term;
    .locals 5
    .param p0, "iParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p1, "path"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v1

    .line 59
    .local v1, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    invoke-virtual {p1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->fullPathLength()I

    move-result v2

    new-array v0, v2, [C

    .line 60
    .local v0, "buffer":[C
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->drillDownTermText(Lorg/apache/lucene/facet/taxonomy/CategoryPath;[C)I

    .line 61
    new-instance v2, Lorg/apache/lucene/index/Term;

    iget-object v3, v1, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method


# virtual methods
.method public varargs add([Lorg/apache/lucene/facet/taxonomy/CategoryPath;)V
    .locals 10
    .param p1, "paths"    # [Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 147
    aget-object v5, p1, v6

    iget v5, v5, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v5, :cond_0

    .line 148
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "all CategoryPaths must have length > 0"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 150
    :cond_0
    aget-object v5, p1, v6

    iget-object v5, v5, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v2, v5, v6

    .line 151
    .local v2, "dim":Ljava/lang/String;
    iget-object v5, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 152
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "dimension \'"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\' was already added"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 154
    :cond_1
    array-length v5, p1

    if-ne v5, v7, :cond_2

    .line 155
    new-instance v4, Lorg/apache/lucene/search/TermQuery;

    iget-object v5, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    aget-object v6, p1, v6

    invoke-static {v5, v6}, Lorg/apache/lucene/facet/search/DrillDownQuery;->term(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/index/Term;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 170
    .local v4, "q":Lorg/apache/lucene/search/Query;
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    iget-object v6, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    new-instance v3, Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-direct {v3, v4}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Query;)V

    .line 173
    .local v3, "drillDownQuery":Lorg/apache/lucene/search/ConstantScoreQuery;
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lorg/apache/lucene/search/ConstantScoreQuery;->setBoost(F)V

    .line 174
    iget-object v5, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    sget-object v6, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v5, v3, v6}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 175
    return-void

    .line 157
    .end local v3    # "drillDownQuery":Lorg/apache/lucene/search/ConstantScoreQuery;
    .end local v4    # "q":Lorg/apache/lucene/search/Query;
    :cond_2
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0, v7}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 158
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    array-length v7, p1

    move v5, v6

    :goto_1
    if-lt v5, v7, :cond_3

    .line 168
    move-object v4, v0

    .restart local v4    # "q":Lorg/apache/lucene/search/Query;
    goto :goto_0

    .line 158
    .end local v4    # "q":Lorg/apache/lucene/search/Query;
    :cond_3
    aget-object v1, p1, v5

    .line 159
    .local v1, "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    iget v8, v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v8, :cond_4

    .line 160
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "all CategoryPaths must have length > 0"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 162
    :cond_4
    iget-object v8, v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v8, v8, v6

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 163
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "multiple (OR\'d) drill-down paths must be under same dimension; got \'"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 164
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' and \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v6, v8, v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 163
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 166
    :cond_5
    new-instance v8, Lorg/apache/lucene/search/TermQuery;

    iget-object v9, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-static {v9, v1}, Lorg/apache/lucene/facet/search/DrillDownQuery;->term(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/index/Term;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    sget-object v9, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v8, v9}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 158
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public clone()Lorg/apache/lucene/facet/search/DrillDownQuery;
    .locals 4

    .prologue
    .line 179
    new-instance v0, Lorg/apache/lucene/facet/search/DrillDownQuery;

    iget-object v1, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/facet/search/DrillDownQuery;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/BooleanQuery;Ljava/util/Map;)V

    return-object v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/DrillDownQuery;->clone()Lorg/apache/lucene/facet/search/DrillDownQuery;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 191
    instance-of v2, p1, Lorg/apache/lucene/facet/search/DrillDownQuery;

    if-nez v2, :cond_1

    .line 196
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 195
    check-cast v0, Lorg/apache/lucene/facet/search/DrillDownQuery;

    .line 196
    .local v0, "other":Lorg/apache/lucene/facet/search/DrillDownQuery;
    iget-object v2, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    iget-object v3, v0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/BooleanQuery;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-super {p0, v0}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method getBooleanQuery()Lorg/apache/lucene/search/BooleanQuery;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    return-object v0
.end method

.method getDim(Lorg/apache/lucene/search/Query;)Ljava/lang/String;
    .locals 4
    .param p1, "clause"    # Lorg/apache/lucene/search/Query;

    .prologue
    const/4 v3, 0x0

    .line 106
    sget-boolean v1, Lorg/apache/lucene/facet/search/DrillDownQuery;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/lucene/search/ConstantScoreQuery;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 107
    :cond_0
    check-cast p1, Lorg/apache/lucene/search/ConstantScoreQuery;

    .end local p1    # "clause":Lorg/apache/lucene/search/Query;
    invoke-virtual {p1}, Lorg/apache/lucene/search/ConstantScoreQuery;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object p1

    .line 108
    .restart local p1    # "clause":Lorg/apache/lucene/search/Query;
    sget-boolean v1, Lorg/apache/lucene/facet/search/DrillDownQuery;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    instance-of v1, p1, Lorg/apache/lucene/search/TermQuery;

    if-nez v1, :cond_1

    instance-of v1, p1, Lorg/apache/lucene/search/BooleanQuery;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 110
    :cond_1
    instance-of v1, p1, Lorg/apache/lucene/search/TermQuery;

    if-eqz v1, :cond_2

    .line 111
    check-cast p1, Lorg/apache/lucene/search/TermQuery;

    .end local p1    # "clause":Lorg/apache/lucene/search/Query;
    invoke-virtual {p1}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v0

    .line 115
    .local v0, "term":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-virtual {v1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getFacetDelimChar()C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    return-object v1

    .line 113
    .end local v0    # "term":Ljava/lang/String;
    .restart local p1    # "clause":Lorg/apache/lucene/search/Query;
    :cond_2
    check-cast p1, Lorg/apache/lucene/search/BooleanQuery;

    .end local p1    # "clause":Lorg/apache/lucene/search/Query;
    invoke-virtual {p1}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v1

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/TermQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "term":Ljava/lang/String;
    goto :goto_0
.end method

.method getDims()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->drillDownDims:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 184
    const/16 v0, 0x1f

    .line 185
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v1

    .line 186
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v3}, Lorg/apache/lucene/search/BooleanQuery;->hashCode()I

    move-result v3

    add-int/2addr v2, v3

    return v2
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "r"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanQuery;->clauses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 205
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no base query or drill-down categories given"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    return-object v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillDownQuery;->query:Lorg/apache/lucene/search/BooleanQuery;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/BooleanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
