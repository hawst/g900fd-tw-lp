.class final enum Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;
.super Ljava/lang/Enum;
.source "RepeatableSampler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/sampling/RepeatableSampler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Algorithm"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

.field public static final enum HASHING:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

.field public static final enum TRAVERSAL:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 354
    new-instance v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    const-string v1, "TRAVERSAL"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;-><init>(Ljava/lang/String;I)V

    .line 364
    sput-object v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;->TRAVERSAL:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    .line 366
    new-instance v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    const-string v1, "HASHING"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;-><init>(Ljava/lang/String;I)V

    .line 372
    sput-object v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;->HASHING:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    .line 352
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    sget-object v1, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;->TRAVERSAL:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;->HASHING:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;->ENUM$VALUES:[Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 352
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;->ENUM$VALUES:[Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
