.class public final Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$MemoryOrdinalMap;
.super Ljava/lang/Object;
.source "DirectoryTaxonomyWriter.java"

# interfaces
.implements Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MemoryOrdinalMap"
.end annotation


# instance fields
.field map:[I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addDone()V
    .locals 0

    .prologue
    .line 875
    return-void
.end method

.method public addMapping(II)V
    .locals 1
    .param p1, "origOrdinal"    # I
    .param p2, "newOrdinal"    # I

    .prologue
    .line 872
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$MemoryOrdinalMap;->map:[I

    aput p2, v0, p1

    .line 873
    return-void
.end method

.method public getMap()[I
    .locals 1

    .prologue
    .line 878
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$MemoryOrdinalMap;->map:[I

    return-object v0
.end method

.method public setSize(I)V
    .locals 1
    .param p1, "taxonomySize"    # I

    .prologue
    .line 868
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$MemoryOrdinalMap;->map:[I

    .line 869
    return-void
.end method
