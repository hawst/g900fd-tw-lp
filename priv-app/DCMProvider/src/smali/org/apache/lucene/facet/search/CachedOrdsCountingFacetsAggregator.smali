.class public Lorg/apache/lucene/facet/search/CachedOrdsCountingFacetsAggregator;
.super Lorg/apache/lucene/facet/search/IntRollupFacetsAggregator;
.source "CachedOrdsCountingFacetsAggregator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/facet/search/IntRollupFacetsAggregator;-><init>()V

    return-void
.end method


# virtual methods
.method public aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 9
    .param p1, "matchingDocs"    # Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    .param p2, "clp"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    iget-object v7, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-static {v7, p2}, Lorg/apache/lucene/facet/search/OrdinalsCache;->getCachedOrds(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/facet/params/CategoryListParams;)Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;

    move-result-object v5

    .line 38
    .local v5, "ords":Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;
    if-nez v5, :cond_1

    .line 52
    :cond_0
    return-void

    .line 41
    :cond_1
    invoke-virtual {p3}, Lorg/apache/lucene/facet/search/FacetArrays;->getIntArray()[I

    move-result-object v0

    .line 42
    .local v0, "counts":[I
    const/4 v1, 0x0

    .line 43
    .local v1, "doc":I
    iget-object v7, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v7}, Lorg/apache/lucene/util/FixedBitSet;->length()I

    move-result v4

    .line 44
    .local v4, "length":I
    :goto_0
    if-ge v1, v4, :cond_0

    iget-object v7, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v7, v1}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v1

    const/4 v7, -0x1

    if-eq v1, v7, :cond_0

    .line 45
    iget-object v7, v5, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;->offsets:[I

    aget v6, v7, v1

    .line 46
    .local v6, "start":I
    iget-object v7, v5, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;->offsets:[I

    add-int/lit8 v8, v1, 0x1

    aget v2, v7, v8

    .line 47
    .local v2, "end":I
    move v3, v6

    .local v3, "i":I
    :goto_1
    if-lt v3, v2, :cond_2

    .line 50
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 48
    :cond_2
    iget-object v7, v5, Lorg/apache/lucene/facet/search/OrdinalsCache$CachedOrds;->ordinals:[I

    aget v7, v7, v3

    aget v8, v0, v7

    add-int/lit8 v8, v8, 0x1

    aput v8, v0, v7

    .line 47
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method
