.class public Lorg/apache/lucene/facet/collections/IntArray;
.super Ljava/lang/Object;
.source "IntArray.java"


# instance fields
.field private data:[I

.field private shouldSort:Z

.field private size:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/collections/IntArray;->init(Z)V

    .line 51
    return-void
.end method

.method private init(Z)V
    .locals 2
    .param p1, "realloc"    # Z

    .prologue
    const/4 v1, 0x0

    .line 54
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    .line 55
    if-eqz p1, :cond_0

    .line 56
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    .line 58
    :cond_0
    iput-boolean v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->shouldSort:Z

    .line 59
    return-void
.end method


# virtual methods
.method public addToArray(I)V
    .locals 4
    .param p1, "value"    # I

    .prologue
    const/4 v3, 0x0

    .line 149
    iget v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 150
    iget v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [I

    .line 151
    .local v0, "newArray":[I
    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    iget v2, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 152
    iput-object v0, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    .line 154
    .end local v0    # "newArray":[I
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    iget v2, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    aput p1, v1, v2

    .line 155
    iget v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    .line 156
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->shouldSort:Z

    .line 157
    return-void
.end method

.method public clear(Z)V
    .locals 0
    .param p1, "resize"    # Z

    .prologue
    .line 249
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/collections/IntArray;->init(Z)V

    .line 250
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 166
    instance-of v4, p1, Lorg/apache/lucene/facet/collections/IntArray;

    if-nez v4, :cond_1

    .line 185
    :cond_0
    :goto_0
    return v3

    :cond_1
    move-object v0, p1

    .line 170
    check-cast v0, Lorg/apache/lucene/facet/collections/IntArray;

    .line 171
    .local v0, "array":Lorg/apache/lucene/facet/collections/IntArray;
    iget v4, v0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    iget v5, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-ne v4, v5, :cond_0

    .line 175
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntArray;->sort()V

    .line 176
    invoke-virtual {v0}, Lorg/apache/lucene/facet/collections/IntArray;->sort()V

    .line 178
    const/4 v1, 0x1

    .line 180
    .local v1, "equal":Z
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    .local v2, "i":I
    :goto_1
    if-lez v2, :cond_2

    if-nez v1, :cond_3

    :cond_2
    move v3, v1

    .line 185
    goto :goto_0

    .line 181
    :cond_3
    add-int/lit8 v2, v2, -0x1

    .line 182
    iget-object v4, v0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v4, v4, v2

    iget-object v5, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v5, v5, v2

    if-ne v4, v5, :cond_4

    const/4 v1, 0x1

    :goto_2
    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_2
.end method

.method public get(I)I
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 217
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-lt p1, v0, :cond_0

    .line 218
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 220
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v0, v0, p1

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 203
    const/4 v0, 0x0

    .line 204
    .local v0, "hash":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-lt v1, v2, :cond_0

    .line 207
    return v0

    .line 205
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v2, v2, v1

    mul-int/lit8 v3, v0, 0x1f

    xor-int v0, v2, v3

    .line 204
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public intersect(Lorg/apache/lucene/facet/collections/IntArray;)V
    .locals 6
    .param p1, "other"    # Lorg/apache/lucene/facet/collections/IntArray;

    .prologue
    .line 86
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntArray;->sort()V

    .line 87
    invoke-virtual {p1}, Lorg/apache/lucene/facet/collections/IntArray;->sort()V

    .line 89
    const/4 v0, 0x0

    .line 90
    .local v0, "myIndex":I
    const/4 v3, 0x0

    .line 91
    .local v3, "otherIndex":I
    const/4 v1, 0x0

    .line 92
    .local v1, "newSize":I
    iget v4, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    iget v5, p1, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-le v4, v5, :cond_b

    .line 93
    :cond_0
    :goto_0
    iget v4, p1, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-ge v3, v4, :cond_1

    iget v4, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-lt v0, v4, :cond_3

    .line 129
    :cond_1
    :goto_1
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    .line 130
    return-void

    .line 96
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 94
    :cond_3
    iget v4, p1, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-ge v3, v4, :cond_4

    .line 95
    iget-object v4, p1, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v4, v4, v3

    iget-object v5, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v5, v5, v0

    .line 94
    if-lt v4, v5, :cond_2

    .line 98
    :cond_4
    iget v4, p1, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-eq v3, v4, :cond_1

    .line 101
    :goto_2
    iget v4, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-ge v0, v4, :cond_5

    iget-object v4, p1, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v4, v4, v3

    iget-object v5, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v5, v5, v0

    if-gt v4, v5, :cond_6

    .line 104
    :cond_5
    iget-object v4, p1, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v4, v4, v3

    iget-object v5, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v5, v5, v0

    if-ne v4, v5, :cond_0

    .line 105
    iget-object v4, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "newSize":I
    .local v2, "newSize":I
    iget-object v5, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v5, v5, v0

    aput v5, v4, v1

    .line 106
    add-int/lit8 v3, v3, 0x1

    .line 107
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .end local v2    # "newSize":I
    .restart local v1    # "newSize":I
    goto :goto_0

    .line 102
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 113
    :cond_7
    add-int/lit8 v0, v0, 0x1

    .line 112
    :cond_8
    iget v4, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-ge v0, v4, :cond_9

    iget-object v4, p1, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v4, v4, v3

    iget-object v5, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v5, v5, v0

    if-gt v4, v5, :cond_7

    .line 115
    :cond_9
    iget v4, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-eq v0, v4, :cond_1

    .line 118
    :goto_3
    iget v4, p1, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-ge v3, v4, :cond_a

    .line 119
    iget-object v4, p1, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v4, v4, v3

    iget-object v5, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v5, v5, v0

    .line 118
    if-lt v4, v5, :cond_c

    .line 122
    :cond_a
    iget-object v4, p1, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v4, v4, v3

    iget-object v5, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v5, v5, v0

    if-ne v4, v5, :cond_b

    .line 123
    iget-object v4, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "newSize":I
    .restart local v2    # "newSize":I
    iget-object v5, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v5, v5, v0

    aput v5, v4, v1

    .line 124
    add-int/lit8 v3, v3, 0x1

    .line 125
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .line 111
    .end local v2    # "newSize":I
    .restart local v1    # "newSize":I
    :cond_b
    iget v4, p1, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-ge v3, v4, :cond_1

    iget v4, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-lt v0, v4, :cond_8

    goto/16 :goto_1

    .line 120
    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method public intersect(Lorg/apache/lucene/facet/collections/IntHashSet;)V
    .locals 4
    .param p1, "set"    # Lorg/apache/lucene/facet/collections/IntHashSet;

    .prologue
    .line 69
    const/4 v1, 0x0

    .line 70
    .local v1, "newSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-lt v0, v2, :cond_0

    .line 76
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    .line 77
    return-void

    .line 71
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v2, v2, v0

    invoke-virtual {p1, v2}, Lorg/apache/lucene/facet/collections/IntHashSet;->contains(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 72
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 73
    add-int/lit8 v1, v1, 0x1

    .line 70
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public set(II)V
    .locals 1
    .param p1, "idx"    # I
    .param p2, "value"    # I

    .prologue
    .line 224
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-lt p1, v0, :cond_0

    .line 225
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 227
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aput p2, v0, p1

    .line 228
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    return v0
.end method

.method public sort()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 192
    iget-boolean v0, p0, Lorg/apache/lucene/facet/collections/IntArray;->shouldSort:Z

    if-eqz v0, :cond_0

    .line 193
    iput-boolean v2, p0, Lorg/apache/lucene/facet/collections/IntArray;->shouldSort:Z

    .line 194
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    invoke-static {v0, v2, v1}, Ljava/util/Arrays;->sort([III)V

    .line 196
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 235
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 236
    .local v1, "s":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntArray;->size:I

    if-lt v0, v2, :cond_0

    .line 239
    return-object v1

    .line 237
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntArray;->data:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
