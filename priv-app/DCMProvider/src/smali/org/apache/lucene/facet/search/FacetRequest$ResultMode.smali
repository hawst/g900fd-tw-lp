.class public final enum Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;
.super Ljava/lang/Enum;
.source "FacetRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/FacetRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResultMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

.field public static final enum GLOBAL_FLAT:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

.field public static final enum PER_NODE_IN_TREE:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    new-instance v0, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    const-string v1, "PER_NODE_IN_TREE"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;-><init>(Ljava/lang/String;I)V

    .line 47
    sput-object v0, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;->PER_NODE_IN_TREE:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    .line 49
    new-instance v0, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    const-string v1, "GLOBAL_FLAT"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;-><init>(Ljava/lang/String;I)V

    .line 50
    sput-object v0, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;->GLOBAL_FLAT:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    .line 45
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;->PER_NODE_IN_TREE:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;->GLOBAL_FLAT:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;->ENUM$VALUES:[Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;->ENUM$VALUES:[Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
