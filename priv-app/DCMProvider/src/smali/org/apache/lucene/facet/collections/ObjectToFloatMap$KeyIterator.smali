.class final Lorg/apache/lucene/facet/collections/ObjectToFloatMap$KeyIterator;
.super Ljava/lang/Object;
.source "ObjectToFloatMap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/ObjectToFloatMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "KeyIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TK;>;"
    }
.end annotation


# instance fields
.field private iterator:Lorg/apache/lucene/facet/collections/IntIterator;

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/ObjectToFloatMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/collections/ObjectToFloatMap;)V
    .locals 1

    .prologue
    .line 112
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$KeyIterator;->this$0:Lorg/apache/lucene/facet/collections/ObjectToFloatMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$IndexIterator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/ObjectToFloatMap;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    .line 112
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$KeyIterator;->this$0:Lorg/apache/lucene/facet/collections/ObjectToFloatMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap;->keys:[Ljava/lang/Object;

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ObjectToFloatMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->remove()V

    .line 128
    return-void
.end method
