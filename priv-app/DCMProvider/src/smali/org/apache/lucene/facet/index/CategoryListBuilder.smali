.class public interface abstract Lorg/apache/lucene/facet/index/CategoryListBuilder;
.super Ljava/lang/Object;
.source "CategoryListBuilder.java"


# virtual methods
.method public abstract build(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Iterable;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/IntsRef;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
