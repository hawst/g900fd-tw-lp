.class public Lorg/apache/lucene/facet/params/PerDimensionIndexingParams;
.super Lorg/apache/lucene/facet/params/FacetIndexingParams;
.source "PerDimensionIndexingParams.java"


# instance fields
.field private final clParamsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "paramsMap":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/params/CategoryListParams;>;"
    sget-object v0, Lorg/apache/lucene/facet/params/PerDimensionIndexingParams;->DEFAULT_CATEGORY_LIST_PARAMS:Lorg/apache/lucene/facet/params/CategoryListParams;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/params/PerDimensionIndexingParams;-><init>(Ljava/util/Map;Lorg/apache/lucene/facet/params/CategoryListParams;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Lorg/apache/lucene/facet/params/CategoryListParams;)V
    .locals 5
    .param p2, "categoryListParams"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            ">;",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "paramsMap":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/params/CategoryListParams;>;"
    invoke-direct {p0, p2}, Lorg/apache/lucene/facet/params/FacetIndexingParams;-><init>(Lorg/apache/lucene/facet/params/CategoryListParams;)V

    .line 66
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/facet/params/PerDimensionIndexingParams;->clParamsMap:Ljava/util/Map;

    .line 67
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    return-void

    .line 67
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 68
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/params/CategoryListParams;>;"
    iget-object v3, p0, Lorg/apache/lucene/facet/params/PerDimensionIndexingParams;->clParamsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget-object v1, v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v4, v1, v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/params/CategoryListParams;

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public getAllCategoryListParams()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/apache/lucene/facet/params/PerDimensionIndexingParams;->clParamsMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 75
    .local v0, "vals":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/params/CategoryListParams;>;"
    iget-object v1, p0, Lorg/apache/lucene/facet/params/PerDimensionIndexingParams;->clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    return-object v0
.end method

.method public getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;
    .locals 4
    .param p1, "category"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    .line 87
    if-eqz p1, :cond_0

    .line 88
    iget-object v1, p0, Lorg/apache/lucene/facet/params/PerDimensionIndexingParams;->clParamsMap:Ljava/util/Map;

    iget-object v2, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 89
    .local v0, "clParams":Lorg/apache/lucene/facet/params/CategoryListParams;
    if-eqz v0, :cond_0

    .line 93
    .end local v0    # "clParams":Lorg/apache/lucene/facet/params/CategoryListParams;
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/params/PerDimensionIndexingParams;->clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    goto :goto_0
.end method
