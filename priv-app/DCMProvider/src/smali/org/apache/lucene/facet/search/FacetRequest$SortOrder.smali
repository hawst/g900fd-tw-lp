.class public final enum Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;
.super Ljava/lang/Enum;
.source "FacetRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/FacetRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SortOrder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ASCENDING:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

.field public static final enum DESCENDING:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    const-string v1, "ASCENDING"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;->ASCENDING:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    new-instance v0, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    const-string v1, "DESCENDING"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;->DESCENDING:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;->ASCENDING:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;->DESCENDING:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;->ENUM$VALUES:[Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;->ENUM$VALUES:[Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
