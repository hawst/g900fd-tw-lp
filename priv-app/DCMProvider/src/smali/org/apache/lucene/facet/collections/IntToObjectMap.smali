.class public Lorg/apache/lucene/facet/collections/IntToObjectMap;
.super Ljava/lang/Object;
.source "IntToObjectMap.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/collections/IntToObjectMap$IndexIterator;,
        Lorg/apache/lucene/facet/collections/IntToObjectMap$KeyIterator;,
        Lorg/apache/lucene/facet/collections/IntToObjectMap$ValueIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static defaultCapacity:I


# instance fields
.field baseHash:[I

.field private capacity:I

.field private firstEmpty:I

.field private hashFactor:I

.field keys:[I

.field next:[I

.field private prev:I

.field private size:I

.field values:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158
    const/16 v0, 0x10

    sput v0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->defaultCapacity:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 212
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    sget v0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->defaultCapacity:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;-><init>(I)V

    .line 213
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1, "capacity"    # I

    .prologue
    .line 222
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 223
    const/16 v2, 0x10

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    .line 225
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    if-lt v2, p1, :cond_0

    .line 232
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    add-int/lit8 v0, v2, 0x1

    .line 234
    .local v0, "arrayLength":I
    new-array v2, v0, [Ljava/lang/Object;

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->values:[Ljava/lang/Object;

    .line 235
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->keys:[I

    .line 236
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    .line 239
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    shl-int/lit8 v1, v2, 0x1

    .line 241
    .local v1, "baseHashSize":I
    new-array v2, v1, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->baseHash:[I

    .line 245
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->hashFactor:I

    .line 247
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    .line 249
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->clear()V

    .line 250
    return-void

    .line 227
    .end local v0    # "arrayLength":I
    .end local v1    # "baseHashSize":I
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    goto :goto_0
.end method

.method private findForRemove(II)I
    .locals 3
    .param p1, "key"    # I
    .param p2, "baseHashIndex"    # I

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    const/4 v1, 0x0

    .line 385
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->prev:I

    .line 386
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->baseHash:[I

    aget v0, v2, p2

    .line 389
    .local v0, "index":I
    :goto_0
    if-nez v0, :cond_1

    .line 402
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->prev:I

    move v0, v1

    .line 403
    .end local v0    # "index":I
    :cond_0
    return v0

    .line 391
    .restart local v0    # "index":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->keys:[I

    aget v2, v2, v0

    if-eq v2, p1, :cond_0

    .line 396
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->prev:I

    .line 397
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    aget v0, v2, v0

    goto :goto_0
.end method

.method private getBaseHashAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->baseHash:[I

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private prvt_put(ILjava/lang/Object;)V
    .locals 4
    .param p1, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 266
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    .local p2, "e":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 269
    .local v0, "hashIndex":I
    iget v1, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->firstEmpty:I

    .line 272
    .local v1, "objectIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->firstEmpty:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->firstEmpty:I

    .line 273
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->values:[Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 274
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->keys:[I

    aput p1, v2, v1

    .line 277
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->baseHash:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 278
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->baseHash:[I

    aput v1, v2, v0

    .line 281
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    .line 282
    return-void
.end method


# virtual methods
.method protected calcBaseHashIndex(I)I
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 289
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->hashFactor:I

    and-int/2addr v0, p1

    return v0
.end method

.method public clear()V
    .locals 5

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    const/4 v4, 0x0

    .line 297
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->baseHash:[I

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 300
    iput v4, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    .line 305
    const/4 v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->firstEmpty:I

    .line 309
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    if-lt v0, v2, :cond_0

    .line 314
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    aput v4, v2, v3

    .line 315
    return-void

    .line 310
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput v1, v2, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public containsKey(I)Z
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 325
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->find(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 338
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 344
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 339
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 340
    .local v1, "object":Ljava/lang/Object;, "TT;"
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 341
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    const/4 v5, 0x0

    .line 612
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/facet/collections/IntToObjectMap;

    .line 613
    .local v2, "that":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    invoke-virtual {v2}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size()I

    move-result v6

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size()I

    move-result v7

    if-eq v6, v7, :cond_1

    .line 632
    :cond_0
    :goto_0
    return v5

    .line 617
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v0

    .line 618
    .local v0, "it":Lorg/apache/lucene/facet/collections/IntIterator;
    :cond_2
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 632
    const/4 v5, 0x1

    goto :goto_0

    .line 619
    :cond_3
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    .line 620
    .local v1, "key":I
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 624
    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 625
    .local v3, "v1":Ljava/lang/Object;, "TT;"
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 626
    .local v4, "v2":Ljava/lang/Object;, "TT;"
    if-nez v3, :cond_4

    if-nez v4, :cond_0

    .line 627
    :cond_4
    if-eqz v3, :cond_5

    if-eqz v4, :cond_0

    .line 628
    :cond_5
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    goto :goto_0
.end method

.method protected find(I)I
    .locals 3
    .param p1, "key"    # I

    .prologue
    .line 354
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 357
    .local v0, "baseHashIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->baseHash:[I

    aget v1, v2, v0

    .line 360
    .local v1, "localIndex":I
    :goto_0
    if-nez v1, :cond_1

    .line 372
    const/4 v1, 0x0

    .end local v1    # "localIndex":I
    :cond_0
    return v1

    .line 362
    .restart local v1    # "localIndex":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->keys:[I

    aget v2, v2, v1

    if-eq v2, p1, :cond_0

    .line 367
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    aget v1, v2, v1

    goto :goto_0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 415
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->values:[Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->find(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected grow()V
    .locals 5

    .prologue
    .line 424
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    new-instance v2, Lorg/apache/lucene/facet/collections/IntToObjectMap;

    .line 425
    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    mul-int/lit8 v3, v3, 0x2

    .line 424
    invoke-direct {v2, v3}, Lorg/apache/lucene/facet/collections/IntToObjectMap;-><init>(I)V

    .line 430
    .local v2, "that":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    new-instance v1, Lorg/apache/lucene/facet/collections/IntToObjectMap$IndexIterator;

    invoke-direct {v1, p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToObjectMap;)V

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/IntToObjectMap$IndexIterator;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>.IndexIterator;"
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/IntToObjectMap$IndexIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 436
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    .line 437
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    .line 438
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToObjectMap;->firstEmpty:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->firstEmpty:I

    .line 439
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToObjectMap;->values:[Ljava/lang/Object;

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->values:[Ljava/lang/Object;

    .line 440
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToObjectMap;->keys:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->keys:[I

    .line 441
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    .line 442
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToObjectMap;->baseHash:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->baseHash:[I

    .line 443
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToObjectMap;->hashFactor:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->hashFactor:I

    .line 444
    return-void

    .line 431
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/IntToObjectMap$IndexIterator;->next()I

    move-result v0

    .line 432
    .local v0, "index":I
    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->keys:[I

    aget v3, v3, v0

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->values:[Ljava/lang/Object;

    aget-object v4, v4, v0

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->prvt_put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 606
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 451
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 459
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToObjectMap$ValueIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap$ValueIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToObjectMap;)V

    return-object v0
.end method

.method public keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;
    .locals 1

    .prologue
    .line 464
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToObjectMap$KeyIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap$KeyIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToObjectMap;)V

    return-object v0
.end method

.method public put(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)TT;"
        }
    .end annotation

    .prologue
    .line 485
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    .local p2, "e":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->find(I)I

    move-result v0

    .line 488
    .local v0, "index":I
    if-eqz v0, :cond_0

    .line 490
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->values:[Ljava/lang/Object;

    aget-object v1, v2, v0

    .line 491
    .local v1, "old":Ljava/lang/Object;, "TT;"
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->values:[Ljava/lang/Object;

    aput-object p2, v2, v0

    .line 505
    .end local v1    # "old":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object v1

    .line 496
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->capacity:I

    if-ne v2, v3, :cond_1

    .line 498
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->grow()V

    .line 503
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->prvt_put(ILjava/lang/Object;)V

    .line 505
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 5
    .param p1, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 517
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 518
    .local v0, "baseHashIndex":I
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->findForRemove(II)I

    move-result v1

    .line 519
    .local v1, "index":I
    if-eqz v1, :cond_1

    .line 522
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->prev:I

    if-nez v2, :cond_0

    .line 523
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->baseHash:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    aget v3, v3, v1

    aput v3, v2, v0

    .line 526
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->prev:I

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    aget v4, v4, v1

    aput v4, v2, v3

    .line 527
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->firstEmpty:I

    aput v3, v2, v1

    .line 528
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->firstEmpty:I

    .line 529
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    .line 530
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->values:[Ljava/lang/Object;

    aget-object v2, v2, v1

    .line 533
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 540
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 4

    .prologue
    .line 549
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    const/4 v2, -0x1

    .line 550
    .local v2, "j":I
    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->size:I

    new-array v0, v3, [Ljava/lang/Object;

    .line 553
    .local v0, "array":[Ljava/lang/Object;
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 556
    return-object v0

    .line 554
    :cond_0
    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v0, v2

    goto :goto_0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1, "a"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 571
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    const/4 v1, 0x0

    .line 573
    .local v1, "j":I
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 574
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 578
    :cond_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 579
    const/4 v2, 0x0

    aput-object v2, p1, v1

    .line 582
    :cond_1
    return-object p1

    .line 575
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, p1, v1

    .line 574
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 587
    .local p0, "this":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<TT;>;"
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 588
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/16 v3, 0x7b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 589
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v1

    .line 590
    .local v1, "keyIterator":Lorg/apache/lucene/facet/collections/IntIterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 600
    const/16 v3, 0x7d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 601
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 591
    :cond_1
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v0

    .line 592
    .local v0, "key":I
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 593
    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 594
    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 595
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 596
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 597
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
