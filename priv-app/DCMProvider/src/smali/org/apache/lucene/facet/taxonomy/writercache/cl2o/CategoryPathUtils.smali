.class Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CategoryPathUtils;
.super Ljava/lang/Object;
.source "CategoryPathUtils.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static equalsToSerialized(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;I)Z
    .locals 8
    .param p0, "cp"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p1, "charBlockArray"    # Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    .param p2, "offset"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 61
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "offset":I
    .local v3, "offset":I
    invoke-virtual {p1, p2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->charAt(I)C

    move-result v2

    .line 62
    .local v2, "n":I
    iget v6, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-eq v6, v2, :cond_0

    move p2, v3

    .line 79
    .end local v3    # "offset":I
    .restart local p2    # "offset":I
    :goto_0
    return v4

    .line 65
    .end local p2    # "offset":I
    .restart local v3    # "offset":I
    :cond_0
    iget v6, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v6, :cond_1

    move p2, v3

    .end local v3    # "offset":I
    .restart local p2    # "offset":I
    move v4, v5

    .line 66
    goto :goto_0

    .line 69
    .end local p2    # "offset":I
    .restart local v3    # "offset":I
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    move p2, v3

    .end local v3    # "offset":I
    .restart local p2    # "offset":I
    :goto_1
    iget v6, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-lt v0, v6, :cond_2

    move v4, v5

    .line 79
    goto :goto_0

    .line 70
    :cond_2
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "offset":I
    .restart local v3    # "offset":I
    invoke-virtual {p1, p2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->charAt(I)C

    move-result v6

    int-to-short v1, v6

    .line 71
    .local v1, "len":I
    iget-object v6, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-eq v1, v6, :cond_3

    move p2, v3

    .line 72
    .end local v3    # "offset":I
    .restart local p2    # "offset":I
    goto :goto_0

    .line 74
    .end local p2    # "offset":I
    .restart local v3    # "offset":I
    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v6, v6, v0

    add-int v7, v3, v1

    invoke-virtual {p1, v3, v7}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    move p2, v3

    .line 75
    .end local v3    # "offset":I
    .restart local p2    # "offset":I
    goto :goto_0

    .line 77
    .end local p2    # "offset":I
    .restart local v3    # "offset":I
    :cond_4
    add-int p2, v3, v1

    .line 69
    .end local v3    # "offset":I
    .restart local p2    # "offset":I
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static hashCodeOfSerialized(Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;I)I
    .locals 7
    .param p0, "charBlockArray"    # Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    .param p1, "offset"    # I

    .prologue
    .line 42
    add-int/lit8 v4, p1, 0x1

    .end local p1    # "offset":I
    .local v4, "offset":I
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->charAt(I)C

    move-result v5

    int-to-short v3, v5

    .line 43
    .local v3, "length":I
    if-nez v3, :cond_0

    .line 44
    const/4 v0, 0x0

    move p1, v4

    .line 53
    .end local v4    # "offset":I
    .restart local p1    # "offset":I
    :goto_0
    return v0

    .line 47
    .end local p1    # "offset":I
    .restart local v4    # "offset":I
    :cond_0
    move v0, v3

    .line 48
    .local v0, "hash":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v3, :cond_1

    move p1, v4

    .line 53
    .end local v4    # "offset":I
    .restart local p1    # "offset":I
    goto :goto_0

    .line 49
    .end local p1    # "offset":I
    .restart local v4    # "offset":I
    :cond_1
    add-int/lit8 p1, v4, 0x1

    .end local v4    # "offset":I
    .restart local p1    # "offset":I
    invoke-virtual {p0, v4}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->charAt(I)C

    move-result v5

    int-to-short v2, v5

    .line 50
    .local v2, "len":I
    mul-int/lit8 v5, v0, 0x1f

    add-int v6, p1, v2

    invoke-virtual {p0, p1, v6}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->hashCode()I

    move-result v6

    add-int v0, v5, v6

    .line 51
    add-int/2addr p1, v2

    .line 48
    add-int/lit8 v1, v1, 0x1

    move v4, p1

    .end local p1    # "offset":I
    .restart local v4    # "offset":I
    goto :goto_1
.end method

.method public static serialize(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;)V
    .locals 2
    .param p0, "cp"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p1, "charBlockArray"    # Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    .prologue
    .line 27
    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    int-to-char v1, v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->append(C)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    .line 28
    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v1, :cond_1

    .line 35
    :cond_0
    return-void

    .line 31
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-ge v0, v1, :cond_0

    .line 32
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->append(C)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    .line 33
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p1, v1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->append(Ljava/lang/String;)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
