.class public Lorg/apache/lucene/facet/codecs/facet42/Facet42Codec;
.super Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;
.source "Facet42Codec.java"


# instance fields
.field private final facetFields:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final facetsDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

.field private final lucene42DVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->DEFAULT:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/codecs/facet42/Facet42Codec;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 4
    .param p1, "fip"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .prologue
    .line 62
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;-><init>()V

    .line 48
    const-string v1, "Facet42"

    invoke-static {v1}, Lorg/apache/lucene/codecs/DocValuesFormat;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/DocValuesFormat;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42Codec;->facetsDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    .line 49
    const-string v1, "Lucene42"

    invoke-static {v1}, Lorg/apache/lucene/codecs/DocValuesFormat;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/DocValuesFormat;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42Codec;->lucene42DVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    .line 63
    invoke-virtual {p1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getPartitionSize()I

    move-result v1

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 64
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "this Codec does not support partitions"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 66
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42Codec;->facetFields:Ljava/util/Set;

    .line 67
    invoke-virtual {p1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getAllCategoryListParams()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 70
    return-void

    .line 67
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 68
    .local v0, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    iget-object v2, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42Codec;->facetFields:Ljava/util/Set;

    iget-object v3, v0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public getDocValuesFormatForField(Ljava/lang/String;)Lorg/apache/lucene/codecs/DocValuesFormat;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42Codec;->facetFields:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42Codec;->facetsDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    .line 77
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42Codec;->lucene42DVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    goto :goto_0
.end method
