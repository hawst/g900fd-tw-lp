.class public final Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;
.super Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;
.source "AdaptiveFacetsAccumulator.java"


# instance fields
.field private sampler:Lorg/apache/lucene/facet/sampling/Sampler;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V
    .locals 1
    .param p1, "searchParams"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p2, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V

    .line 44
    new-instance v0, Lorg/apache/lucene/facet/sampling/RandomSampler;

    invoke-direct {v0}, Lorg/apache/lucene/facet/sampling/RandomSampler;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    .line 53
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 1
    .param p1, "searchParams"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p2, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p4, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 44
    new-instance v0, Lorg/apache/lucene/facet/sampling/RandomSampler;

    invoke-direct {v0}, Lorg/apache/lucene/facet/sampling/RandomSampler;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    .line 64
    return-void
.end method

.method private appropriateFacetCountingAccumulator(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;
    .locals 5
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;

    .prologue
    .line 91
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->mayComplement()Z

    move-result v1

    if-nez v1, :cond_1

    .line 104
    .end local p0    # "this":Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;
    :cond_0
    :goto_0
    return-object p0

    .line 98
    .restart local p0    # "this":Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/facet/sampling/Sampler;->shouldSample(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    new-instance v0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;

    iget-object v1, p0, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->indexReader:Lorg/apache/lucene/index/IndexReader;

    iget-object v4, p0, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;-><init>(Lorg/apache/lucene/facet/sampling/Sampler;Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V

    .line 103
    .local v0, "samplingAccumulator":Lorg/apache/lucene/facet/sampling/SamplingAccumulator;
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->getComplementThreshold()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->setComplementThreshold(D)V

    move-object p0, v0

    .line 104
    goto :goto_0
.end method


# virtual methods
.method public accumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Ljava/util/List;
    .locals 2
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/search/ScoredDocIDs;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResult;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->appropriateFacetCountingAccumulator(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

    move-result-object v0

    .line 78
    .local v0, "delegee":Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;
    if-ne v0, p0, :cond_0

    .line 79
    invoke-super {p0, p1}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->accumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Ljava/util/List;

    move-result-object v1

    .line 82
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->accumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public final getSampler()Lorg/apache/lucene/facet/sampling/Sampler;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    return-object v0
.end method

.method public setSampler(Lorg/apache/lucene/facet/sampling/Sampler;)V
    .locals 0
    .param p1, "sampler"    # Lorg/apache/lucene/facet/sampling/Sampler;

    .prologue
    .line 71
    iput-object p1, p0, Lorg/apache/lucene/facet/search/AdaptiveFacetsAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    .line 72
    return-void
.end method
