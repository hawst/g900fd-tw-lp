.class final Lorg/apache/lucene/facet/collections/IntToObjectMap$KeyIterator;
.super Ljava/lang/Object;
.source "IntToObjectMap.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/IntIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/IntToObjectMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "KeyIterator"
.end annotation


# instance fields
.field private iterator:Lorg/apache/lucene/facet/collections/IntIterator;

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/IntToObjectMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/collections/IntToObjectMap;)V
    .locals 1

    .prologue
    .line 111
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$KeyIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToObjectMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToObjectMap$IndexIterator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/collections/IntToObjectMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToObjectMap;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    .line 111
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()I
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$KeyIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToObjectMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->keys:[I

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->remove()V

    .line 126
    return-void
.end method
