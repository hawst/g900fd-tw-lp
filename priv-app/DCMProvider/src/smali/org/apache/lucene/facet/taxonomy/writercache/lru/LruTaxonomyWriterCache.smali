.class public Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;
.super Ljava/lang/Object;
.source "LruTaxonomyWriterCache.java"

# interfaces
.implements Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache$LRUType;
    }
.end annotation


# instance fields
.field private cache:Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "cacheSize"    # I

    .prologue
    .line 46
    sget-object v0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache$LRUType;->LRU_HASHED:Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache$LRUType;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;-><init>(ILorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache$LRUType;)V

    .line 47
    return-void
.end method

.method public constructor <init>(ILorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache$LRUType;)V
    .locals 1
    .param p1, "cacheSize"    # I
    .param p2, "lruType"    # Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache$LRUType;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    sget-object v0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache$LRUType;->LRU_HASHED:Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache$LRUType;

    if-ne p2, v0, :cond_0

    .line 57
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameHashIntCacheLRU;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameHashIntCacheLRU;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_0
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    monitor-exit p0

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->clear()V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    .locals 2
    .param p1, "categoryPath"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    .line 81
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->get(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 82
    .local v0, "res":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 83
    const/4 v1, -0x1

    .line 86
    :goto_0
    monitor-exit p0

    return v1

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_0

    .line 81
    .end local v0    # "res":Ljava/lang/Integer;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized isFull()Z
    .locals 2

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->getSize()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;

    invoke-virtual {v1}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->getMaxSize()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized put(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)Z
    .locals 3
    .param p1, "categoryPath"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "ordinal"    # I

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, p1, v2}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->put(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Ljava/lang/Integer;)Z

    move-result v0

    .line 99
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    .line 100
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/lru/LruTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;

    invoke-virtual {v1}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->makeRoomLRU()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    :cond_0
    monitor-exit p0

    return v0

    .line 91
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
