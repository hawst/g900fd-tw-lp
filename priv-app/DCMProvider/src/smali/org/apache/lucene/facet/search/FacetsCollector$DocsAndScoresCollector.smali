.class final Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;
.super Lorg/apache/lucene/facet/search/FacetsCollector;
.source "FacetsCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/FacetsCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DocsAndScoresCollector"
.end annotation


# instance fields
.field private bits:Lorg/apache/lucene/util/FixedBitSet;

.field private context:Lorg/apache/lucene/index/AtomicReaderContext;

.field private scorer:Lorg/apache/lucene/search/Scorer;

.field private scores:[F

.field private totalHits:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/search/FacetsAccumulator;)V
    .locals 0
    .param p1, "accumulator"    # Lorg/apache/lucene/facet/search/FacetsAccumulator;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/search/FacetsCollector;-><init>(Lorg/apache/lucene/facet/search/FacetsAccumulator;)V

    .line 55
    return-void
.end method


# virtual methods
.method public final acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public final collect(I)V
    .locals 4
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 74
    iget-object v1, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 75
    iget v1, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->totalHits:I

    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->scores:[F

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 76
    iget v1, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->totalHits:I

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [F

    .line 77
    .local v0, "newScores":[F
    iget-object v1, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->scores:[F

    iget v2, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->totalHits:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 78
    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->scores:[F

    .line 80
    .end local v0    # "newScores":[F
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->scores:[F

    iget v2, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->totalHits:I

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v3

    aput v3, v1, v2

    .line 81
    iget v1, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->totalHits:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->totalHits:I

    .line 82
    return-void
.end method

.method protected final doSetNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->matchingDocs:Ljava/util/List;

    new-instance v1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    iget v4, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->totalHits:I

    iget-object v5, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->scores:[F

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;-><init>(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/FixedBitSet;I[F)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    .line 95
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->totalHits:I

    .line 96
    const/16 v0, 0x40

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->scores:[F

    .line 97
    iput-object p1, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 98
    return-void
.end method

.method protected final finish()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 59
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->matchingDocs:Ljava/util/List;

    new-instance v1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    iget v4, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->totalHits:I

    iget-object v5, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->scores:[F

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;-><init>(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/FixedBitSet;I[F)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    iput-object v6, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    .line 62
    iput-object v6, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->scores:[F

    .line 63
    iput-object v6, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 65
    :cond_0
    return-void
.end method

.method public final setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    iput-object p1, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 87
    return-void
.end method
