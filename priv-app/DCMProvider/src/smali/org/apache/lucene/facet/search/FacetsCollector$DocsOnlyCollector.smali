.class final Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;
.super Lorg/apache/lucene/facet/search/FacetsCollector;
.source "FacetsCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/FacetsCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DocsOnlyCollector"
.end annotation


# instance fields
.field private bits:Lorg/apache/lucene/util/FixedBitSet;

.field private context:Lorg/apache/lucene/index/AtomicReaderContext;

.field private totalHits:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/search/FacetsAccumulator;)V
    .locals 0
    .param p1, "accumulator"    # Lorg/apache/lucene/facet/search/FacetsAccumulator;

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/search/FacetsCollector;-><init>(Lorg/apache/lucene/facet/search/FacetsAccumulator;)V

    .line 110
    return-void
.end method


# virtual methods
.method public final acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x1

    return v0
.end method

.method public final collect(I)V
    .locals 1
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    iget v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->totalHits:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->totalHits:I

    .line 129
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 130
    return-void
.end method

.method protected final doSetNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->matchingDocs:Ljava/util/List;

    new-instance v1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    iget v4, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->totalHits:I

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;-><init>(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/FixedBitSet;I[F)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    .line 141
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->totalHits:I

    .line 142
    iput-object p1, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 143
    return-void
.end method

.method protected final finish()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 114
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->matchingDocs:Ljava/util/List;

    new-instance v1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    iget v4, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->totalHits:I

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;-><init>(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/FixedBitSet;I[F)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    iput-object v5, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->bits:Lorg/apache/lucene/util/FixedBitSet;

    .line 117
    iput-object v5, p0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 119
    :cond_0
    return-void
.end method

.method public final setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    return-void
.end method
