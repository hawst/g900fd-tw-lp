.class Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;
.super Ljava/lang/Object;
.source "TakmiSampleFixer.java"

# interfaces
.implements Lorg/apache/lucene/facet/sampling/SampleFixer;


# instance fields
.field private indexReader:Lorg/apache/lucene/index/IndexReader;

.field private searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

.field private taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetSearchParams;)V
    .locals 0
    .param p1, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p3, "searchParams"    # Lorg/apache/lucene/facet/params/FacetSearchParams;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->indexReader:Lorg/apache/lucene/index/IndexReader;

    .line 57
    iput-object p2, p0, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .line 58
    iput-object p3, p0, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    .line 59
    return-void
.end method

.method private static advance(Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;I)Z
    .locals 1
    .param p0, "iterator"    # Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
    .param p1, "targetDoc"    # I

    .prologue
    .line 175
    :cond_0
    invoke-interface {p0}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->next()Z

    move-result v0

    if-nez v0, :cond_1

    .line 180
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 176
    :cond_1
    invoke-interface {p0}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getDocID()I

    move-result v0

    if-lt v0, p1, :cond_0

    .line 177
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static countIntersection(Lorg/apache/lucene/index/DocsEnum;Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;)I
    .locals 5
    .param p0, "p1"    # Lorg/apache/lucene/index/DocsEnum;
    .param p1, "p2"    # Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const v4, 0x7fffffff

    .line 127
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v3

    if-ne v3, v4, :cond_1

    .line 161
    :cond_0
    return v0

    .line 130
    :cond_1
    invoke-interface {p1}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->next()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 134
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v1

    .line 135
    .local v1, "d1":I
    invoke-interface {p1}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getDocID()I

    move-result v2

    .line 137
    .local v2, "d2":I
    const/4 v0, 0x0

    .line 139
    .local v0, "count":I
    :goto_0
    if-ne v1, v2, :cond_2

    .line 140
    add-int/lit8 v0, v0, 0x1

    .line 141
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v3

    if-eq v3, v4, :cond_0

    .line 144
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v1

    .line 145
    invoke-static {p1, v1}, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->advance(Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 148
    invoke-interface {p1}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getDocID()I

    move-result v2

    .line 149
    goto :goto_0

    :cond_2
    if-ge v1, v2, :cond_3

    .line 150
    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/DocsEnum;->advance(I)I

    move-result v3

    if-eq v3, v4, :cond_0

    .line 153
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v1

    .line 154
    goto :goto_0

    .line 155
    :cond_3
    invoke-static {p1, v1}, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->advance(Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 158
    invoke-interface {p1}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getDocID()I

    move-result v2

    .line 138
    goto :goto_0
.end method

.method private fixResultNode(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/facet/search/ScoredDocIDs;)V
    .locals 3
    .param p1, "facetResNode"    # Lorg/apache/lucene/facet/search/FacetResultNode;
    .param p2, "docIds"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->recount(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/facet/search/ScoredDocIDs;)V

    .line 79
    iget-object v1, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 82
    return-void

    .line 79
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 80
    .local v0, "frn":Lorg/apache/lucene/facet/search/FacetResultNode;
    invoke-direct {p0, v0, p2}, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->fixResultNode(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/facet/search/ScoredDocIDs;)V

    goto :goto_0
.end method

.method private recount(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/facet/search/ScoredDocIDs;)V
    .locals 8
    .param p1, "fresNode"    # Lorg/apache/lucene/facet/search/FacetResultNode;
    .param p2, "docIds"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v4, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    if-nez v4, :cond_0

    .line 104
    iget-object v4, p0, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget v5, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getPath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v4

    iput-object v4, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 106
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 108
    .local v0, "catPath":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    iget-object v4, p0, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v4, v4, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-static {v4, v0}, Lorg/apache/lucene/facet/search/DrillDownQuery;->term(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/index/Term;

    move-result-object v1

    .line 110
    .local v1, "drillDownTerm":Lorg/apache/lucene/index/Term;
    iget-object v4, p0, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->indexReader:Lorg/apache/lucene/index/IndexReader;

    invoke-static {v4}, Lorg/apache/lucene/index/MultiFields;->getLiveDocs(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/util/Bits;

    move-result-object v2

    .line 111
    .local v2, "liveDocs":Lorg/apache/lucene/util/Bits;
    iget-object v4, p0, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->indexReader:Lorg/apache/lucene/index/IndexReader;

    .line 112
    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    .line 113
    const/4 v7, 0x0

    .line 111
    invoke-static {v4, v2, v5, v6, v7}, Lorg/apache/lucene/index/MultiFields;->getTermDocsEnum(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v4

    .line 113
    invoke-interface {p2}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;

    move-result-object v5

    .line 111
    invoke-static {v4, v5}, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->countIntersection(Lorg/apache/lucene/index/DocsEnum;Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;)I

    move-result v3

    .line 114
    .local v3, "updatedCount":I
    int-to-double v4, v3

    iput-wide v4, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    .line 115
    return-void
.end method


# virtual methods
.method public fixResult(Lorg/apache/lucene/facet/search/ScoredDocIDs;Lorg/apache/lucene/facet/search/FacetResult;)V
    .locals 1
    .param p1, "origDocIds"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .param p2, "fres"    # Lorg/apache/lucene/facet/search/FacetResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-virtual {p2}, Lorg/apache/lucene/facet/search/FacetResult;->getFacetResultNode()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v0

    .line 65
    .local v0, "topRes":Lorg/apache/lucene/facet/search/FacetResultNode;
    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;->fixResultNode(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/facet/search/ScoredDocIDs;)V

    .line 66
    return-void
.end method
