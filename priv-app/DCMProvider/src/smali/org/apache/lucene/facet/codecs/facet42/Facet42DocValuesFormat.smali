.class public final Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesFormat;
.super Lorg/apache/lucene/codecs/DocValuesFormat;
.source "Facet42DocValuesFormat.java"


# static fields
.field public static final CODEC:Ljava/lang/String; = "FacetsDocValues"

.field public static final EXTENSION:Ljava/lang/String; = "fdv"

.field public static final VERSION_CURRENT:I

.field public static final VERSION_START:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "Facet42"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/DocValuesFormat;-><init>(Ljava/lang/String;)V

    .line 51
    return-void
.end method


# virtual methods
.method public fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    new-instance v0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;-><init>(Lorg/apache/lucene/index/SegmentWriteState;)V

    return-object v0
.end method

.method public fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesProducer;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesProducer;-><init>(Lorg/apache/lucene/index/SegmentReadState;)V

    return-object v0
.end method
