.class public interface abstract Lorg/apache/lucene/facet/search/FacetsAggregator;
.super Ljava/lang/Object;
.source "FacetsAggregator.java"


# virtual methods
.method public abstract aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract requiresDocScores()Z
.end method

.method public abstract rollupValues(Lorg/apache/lucene/facet/search/FacetRequest;I[I[ILorg/apache/lucene/facet/search/FacetArrays;)V
.end method
