.class Lorg/apache/lucene/facet/search/DrillSidewaysScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "DrillSidewaysScorer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CHUNK:I = 0x800

.field private static final MASK:I = 0x7ff


# instance fields
.field private final baseScorer:Lorg/apache/lucene/search/Scorer;

.field private collectDocID:I

.field private collectScore:F

.field private final context:Lorg/apache/lucene/index/AtomicReaderContext;

.field private final dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

.field private final drillDownCollector:Lorg/apache/lucene/search/Collector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->$assertionsDisabled:Z

    .line 45
    return-void

    .line 31
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Collector;[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;)V
    .locals 1
    .param p1, "w"    # Lorg/apache/lucene/search/Weight;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "baseScorer"    # Lorg/apache/lucene/search/Scorer;
    .param p4, "drillDownCollector"    # Lorg/apache/lucene/search/Collector;
    .param p5, "dims"    # [Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectDocID:I

    .line 53
    iput-object p5, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    .line 54
    iput-object p2, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 55
    iput-object p3, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    .line 56
    iput-object p4, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    .line 57
    return-void
.end method

.method private collectHit(Lorg/apache/lucene/search/Collector;[Lorg/apache/lucene/search/Collector;)V
    .locals 3
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "sidewaysCollectors"    # [Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 393
    iget v1, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectDocID:I

    invoke-virtual {p1, v1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 394
    iget-object v1, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    iget v2, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectDocID:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 401
    const/4 v0, 0x0

    .local v0, "dim":I
    :goto_0
    array-length v1, p2

    if-lt v0, v1, :cond_0

    .line 404
    return-void

    .line 402
    :cond_0
    aget-object v1, p2, v0

    iget v2, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectDocID:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 401
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private collectNearMiss([Lorg/apache/lucene/search/Collector;I)V
    .locals 2
    .param p1, "sidewaysCollectors"    # [Lorg/apache/lucene/search/Collector;
    .param p2, "dim"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 410
    aget-object v0, p1, p2

    iget v1, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectDocID:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 411
    return-void
.end method

.method private doBaseAdvanceScoring(Lorg/apache/lucene/search/Collector;[[Lorg/apache/lucene/index/DocsEnum;[Lorg/apache/lucene/search/Collector;)V
    .locals 11
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "docsEnums"    # [[Lorg/apache/lucene/index/DocsEnum;
    .param p3, "sidewaysCollectors"    # [Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, -0x1

    .line 337
    iget-object v6, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v6}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    .line 339
    .local v1, "docID":I
    iget-object v6, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    array-length v5, v6

    .line 341
    .local v5, "numDims":I
    :goto_0
    const v6, 0x7fffffff

    if-ne v1, v6, :cond_0

    .line 386
    return-void

    .line 342
    :cond_0
    const/4 v3, -0x1

    .line 343
    .local v3, "failedDim":I
    const/4 v0, 0x0

    .local v0, "dim":I
    :goto_1
    if-lt v0, v5, :cond_1

    .line 372
    iput v1, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectDocID:I

    .line 376
    iget-object v6, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v6}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v6

    iput v6, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectScore:F

    .line 378
    if-ne v3, v10, :cond_8

    .line 379
    invoke-direct {p0, p1, p3}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectHit(Lorg/apache/lucene/search/Collector;[Lorg/apache/lucene/search/Collector;)V

    .line 384
    :goto_2
    iget-object v6, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v6}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v1

    goto :goto_0

    .line 346
    :cond_1
    const/4 v4, 0x0

    .line 347
    .local v4, "found":Z
    aget-object v7, p2, v0

    array-length v8, v7

    const/4 v6, 0x0

    :goto_3
    if-lt v6, v8, :cond_2

    .line 359
    :goto_4
    if-nez v4, :cond_7

    .line 360
    if-eq v3, v10, :cond_6

    .line 364
    iget-object v6, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v6}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v1

    .line 365
    goto :goto_0

    .line 347
    :cond_2
    aget-object v2, v7, v6

    .line 348
    .local v2, "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    if-nez v2, :cond_4

    .line 347
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 351
    :cond_4
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v9

    if-ge v9, v1, :cond_5

    .line 352
    invoke-virtual {v2, v1}, Lorg/apache/lucene/index/DocsEnum;->advance(I)I

    .line 354
    :cond_5
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v9

    if-ne v9, v1, :cond_3

    .line 355
    const/4 v4, 0x1

    .line 356
    goto :goto_4

    .line 367
    .end local v2    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    :cond_6
    move v3, v0

    .line 343
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 381
    .end local v4    # "found":Z
    :cond_8
    invoke-direct {p0, p3, v3}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectNearMiss([Lorg/apache/lucene/search/Collector;I)V

    goto :goto_2
.end method

.method private doDrillDownAdvanceScoring(Lorg/apache/lucene/search/Collector;[[Lorg/apache/lucene/index/DocsEnum;[Lorg/apache/lucene/search/Collector;)V
    .locals 27
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "docsEnums"    # [[Lorg/apache/lucene/index/DocsEnum;
    .param p3, "sidewaysCollectors"    # [Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v14

    .line 129
    .local v14, "maxDoc":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v17, v0

    .line 136
    .local v17, "numDims":I
    const/16 v22, 0x800

    move/from16 v0, v22

    new-array v12, v0, [I

    .line 137
    .local v12, "filledSlots":[I
    const/16 v22, 0x800

    move/from16 v0, v22

    new-array v8, v0, [I

    .line 138
    .local v8, "docIDs":[I
    const/16 v22, 0x800

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v18, v0

    .line 139
    .local v18, "scores":[F
    const/16 v22, 0x800

    move/from16 v0, v22

    new-array v15, v0, [I

    .line 140
    .local v15, "missingDims":[I
    const/16 v22, 0x800

    move/from16 v0, v22

    new-array v4, v0, [I

    .line 142
    .local v4, "counts":[I
    const/16 v22, 0x0

    const/16 v23, -0x1

    aput v23, v8, v22

    .line 143
    const/16 v16, 0x800

    .line 145
    .local v16, "nextChunkStart":I
    new-instance v19, Lorg/apache/lucene/util/FixedBitSet;

    const/16 v22, 0x800

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 156
    .local v19, "seen":Lorg/apache/lucene/util/FixedBitSet;
    :goto_0
    const/16 v22, 0x0

    aget-object v23, p2, v22

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v24, v0

    const/16 v22, 0x0

    :goto_1
    move/from16 v0, v22

    move/from16 v1, v24

    if-lt v0, v1, :cond_2

    .line 183
    const/16 v22, 0x1

    aget-object v23, p2, v22

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v24, v0

    const/16 v22, 0x0

    :goto_2
    move/from16 v0, v22

    move/from16 v1, v24

    if-lt v0, v1, :cond_6

    .line 230
    const/4 v10, 0x0

    .line 231
    .local v10, "filledCount":I
    const/16 v21, 0x0

    .local v21, "slot0":I
    move v11, v10

    .line 232
    .end local v10    # "filledCount":I
    .local v11, "filledCount":I
    :goto_3
    const/16 v22, 0x800

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v21

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_b

    .line 259
    :cond_0
    const/16 v22, 0x0

    const/16 v23, 0x800

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/FixedBitSet;->clear(II)V

    .line 261
    if-nez v11, :cond_10

    .line 262
    move/from16 v0, v16

    if-lt v0, v14, :cond_f

    .line 328
    :cond_1
    return-void

    .line 156
    .end local v11    # "filledCount":I
    .end local v21    # "slot0":I
    :cond_2
    aget-object v9, v23, v22

    .line 157
    .local v9, "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    if-nez v9, :cond_4

    .line 156
    :cond_3
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 160
    :cond_4
    invoke-virtual {v9}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v7

    .line 161
    .local v7, "docID":I
    :goto_4
    move/from16 v0, v16

    if-ge v7, v0, :cond_3

    .line 162
    and-int/lit16 v0, v7, 0x7ff

    move/from16 v20, v0

    .line 164
    .local v20, "slot":I
    aget v25, v8, v20

    move/from16 v0, v25

    if-eq v0, v7, :cond_5

    .line 165
    invoke-virtual/range {v19 .. v20}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 170
    aput v7, v8, v20

    .line 171
    const/16 v25, 0x1

    aput v25, v15, v20

    .line 172
    const/16 v25, 0x1

    aput v25, v4, v20

    .line 175
    :cond_5
    invoke-virtual {v9}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v7

    goto :goto_4

    .line 183
    .end local v7    # "docID":I
    .end local v9    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    .end local v20    # "slot":I
    :cond_6
    aget-object v9, v23, v22

    .line 184
    .restart local v9    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    if-nez v9, :cond_8

    .line 183
    :cond_7
    add-int/lit8 v22, v22, 0x1

    goto :goto_2

    .line 187
    :cond_8
    invoke-virtual {v9}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v7

    .line 188
    .restart local v7    # "docID":I
    :goto_5
    move/from16 v0, v16

    if-ge v7, v0, :cond_7

    .line 189
    and-int/lit16 v0, v7, 0x7ff

    move/from16 v20, v0

    .line 191
    .restart local v20    # "slot":I
    aget v25, v8, v20

    move/from16 v0, v25

    if-eq v0, v7, :cond_9

    .line 193
    invoke-virtual/range {v19 .. v20}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 197
    aput v7, v8, v20

    .line 198
    const/16 v25, 0x0

    aput v25, v15, v20

    .line 199
    const/16 v25, 0x1

    aput v25, v4, v20

    .line 217
    :goto_6
    invoke-virtual {v9}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v7

    goto :goto_5

    .line 203
    :cond_9
    aget v25, v15, v20

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_a

    .line 204
    const/16 v25, 0x2

    aput v25, v15, v20

    .line 205
    const/16 v25, 0x2

    aput v25, v4, v20

    goto :goto_6

    .line 210
    :cond_a
    const/16 v25, 0x1

    aput v25, v4, v20

    goto :goto_6

    .line 233
    .end local v7    # "docID":I
    .end local v9    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    .end local v20    # "slot":I
    .restart local v11    # "filledCount":I
    .restart local v21    # "slot0":I
    :cond_b
    aget v5, v8, v21

    .line 234
    .local v5, "ddDocID":I
    sget-boolean v22, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->$assertionsDisabled:Z

    if-nez v22, :cond_c

    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v5, v0, :cond_c

    new-instance v22, Ljava/lang/AssertionError;

    invoke-direct/range {v22 .. v22}, Ljava/lang/AssertionError;-><init>()V

    throw v22

    .line 236
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v3

    .line 237
    .local v3, "baseDocID":I
    if-ge v3, v5, :cond_d

    .line 238
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v3

    .line 240
    :cond_d
    if-ne v3, v5, :cond_e

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v22

    aput v22, v18, v21

    .line 245
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "filledCount":I
    .restart local v10    # "filledCount":I
    aput v21, v12, v11

    .line 246
    aget v22, v4, v21

    add-int/lit8 v22, v22, 0x1

    aput v22, v4, v21

    .line 257
    :goto_7
    add-int/lit8 v21, v21, 0x1

    move v11, v10

    .end local v10    # "filledCount":I
    .restart local v11    # "filledCount":I
    goto/16 :goto_3

    .line 251
    :cond_e
    const/16 v22, -0x1

    aput v22, v8, v21

    move v10, v11

    .end local v11    # "filledCount":I
    .restart local v10    # "filledCount":I
    goto :goto_7

    .line 265
    .end local v3    # "baseDocID":I
    .end local v5    # "ddDocID":I
    .end local v10    # "filledCount":I
    .restart local v11    # "filledCount":I
    :cond_f
    move/from16 v0, v16

    add-int/lit16 v0, v0, 0x800

    move/from16 v16, v0

    .line 266
    goto/16 :goto_0

    .line 271
    :cond_10
    const/4 v6, 0x2

    .local v6, "dim":I
    :goto_8
    move/from16 v0, v17

    if-lt v6, v0, :cond_11

    .line 308
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_9
    if-lt v13, v11, :cond_17

    .line 322
    move/from16 v0, v16

    if-ge v0, v14, :cond_1

    .line 326
    move/from16 v0, v16

    add-int/lit16 v0, v0, 0x800

    move/from16 v16, v0

    .line 147
    goto/16 :goto_0

    .line 275
    .end local v13    # "i":I
    :cond_11
    aget-object v23, p2, v6

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v24, v0

    const/16 v22, 0x0

    :goto_a
    move/from16 v0, v22

    move/from16 v1, v24

    if-lt v0, v1, :cond_12

    .line 271
    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    .line 275
    :cond_12
    aget-object v9, v23, v22

    .line 276
    .restart local v9    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    if-nez v9, :cond_14

    .line 275
    :cond_13
    add-int/lit8 v22, v22, 0x1

    goto :goto_a

    .line 279
    :cond_14
    invoke-virtual {v9}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v7

    .line 280
    .restart local v7    # "docID":I
    :goto_b
    move/from16 v0, v16

    if-ge v7, v0, :cond_13

    .line 281
    and-int/lit16 v0, v7, 0x7ff

    move/from16 v20, v0

    .line 282
    .restart local v20    # "slot":I
    aget v25, v8, v20

    move/from16 v0, v25

    if-ne v0, v7, :cond_15

    aget v25, v4, v20

    move/from16 v0, v25

    if-lt v0, v6, :cond_15

    .line 285
    aget v25, v15, v20

    move/from16 v0, v25

    if-lt v0, v6, :cond_16

    .line 289
    add-int/lit8 v25, v6, 0x1

    aput v25, v15, v20

    .line 290
    add-int/lit8 v25, v6, 0x2

    aput v25, v4, v20

    .line 299
    :cond_15
    :goto_c
    invoke-virtual {v9}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v7

    goto :goto_b

    .line 295
    :cond_16
    add-int/lit8 v25, v6, 0x1

    aput v25, v4, v20

    goto :goto_c

    .line 309
    .end local v7    # "docID":I
    .end local v9    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    .end local v20    # "slot":I
    .restart local v13    # "i":I
    :cond_17
    aget v20, v12, v13

    .line 310
    .restart local v20    # "slot":I
    aget v22, v8, v20

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectDocID:I

    .line 311
    aget v22, v18, v20

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectScore:F

    .line 315
    aget v22, v4, v20

    add-int/lit8 v23, v17, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_19

    .line 316
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectHit(Lorg/apache/lucene/search/Collector;[Lorg/apache/lucene/search/Collector;)V

    .line 308
    :cond_18
    :goto_d
    add-int/lit8 v13, v13, 0x1

    goto :goto_9

    .line 317
    :cond_19
    aget v22, v4, v20

    move/from16 v0, v22

    move/from16 v1, v17

    if-ne v0, v1, :cond_18

    .line 318
    aget v22, v15, v20

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectNearMiss([Lorg/apache/lucene/search/Collector;I)V

    goto :goto_d
.end method

.method private doUnionScoring(Lorg/apache/lucene/search/Collector;[[Lorg/apache/lucene/index/DocsEnum;[Lorg/apache/lucene/search/Collector;)V
    .locals 22
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "docsEnums"    # [[Lorg/apache/lucene/index/DocsEnum;
    .param p3, "sidewaysCollectors"    # [Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v12

    .line 419
    .local v12, "maxDoc":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v15, v0

    .line 422
    .local v15, "numDims":I
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v10, v0, [I

    .line 423
    .local v10, "filledSlots":[I
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v6, v0, [I

    .line 424
    .local v6, "docIDs":[I
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v16, v0

    .line 425
    .local v16, "scores":[F
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v13, v0, [I

    .line 426
    .local v13, "missingDims":[I
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v3, v0, [I

    .line 428
    .local v3, "counts":[I
    const/16 v18, 0x0

    const/16 v19, -0x1

    aput v19, v6, v18

    .line 434
    const/16 v14, 0x800

    .line 440
    .local v14, "nextChunkStart":I
    :goto_0
    const/4 v8, 0x0

    .line 441
    .local v8, "filledCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v5

    .local v5, "docID":I
    move v9, v8

    .line 445
    .end local v8    # "filledCount":I
    .local v9, "filledCount":I
    :goto_1
    if-lt v5, v14, :cond_1

    .line 462
    if-nez v9, :cond_4

    .line 463
    if-lt v14, v12, :cond_3

    .line 590
    :cond_0
    return-void

    .line 446
    :cond_1
    and-int/lit16 v0, v5, 0x7ff

    move/from16 v17, v0

    .line 452
    .local v17, "slot":I
    sget-boolean v18, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->$assertionsDisabled:Z

    if-nez v18, :cond_2

    aget v18, v6, v17

    move/from16 v0, v18

    if-ne v0, v5, :cond_2

    new-instance v18, Ljava/lang/AssertionError;

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "slot="

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " docID="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v18

    .line 453
    :cond_2
    aput v5, v6, v17

    .line 454
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v18

    aput v18, v16, v17

    .line 455
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "filledCount":I
    .restart local v8    # "filledCount":I
    aput v17, v10, v9

    .line 456
    const/16 v18, 0x0

    aput v18, v13, v17

    .line 457
    const/16 v18, 0x1

    aput v18, v3, v17

    .line 459
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v5

    move v9, v8

    .end local v8    # "filledCount":I
    .restart local v9    # "filledCount":I
    goto :goto_1

    .line 466
    .end local v17    # "slot":I
    :cond_3
    add-int/lit16 v14, v14, 0x800

    .line 467
    goto :goto_0

    .line 475
    :cond_4
    const/16 v18, 0x0

    aget-object v19, p2, v18

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    const/16 v18, 0x0

    :goto_2
    move/from16 v0, v18

    move/from16 v1, v20

    if-lt v0, v1, :cond_5

    .line 496
    const/4 v4, 0x1

    .local v4, "dim":I
    :goto_3
    if-lt v4, v15, :cond_9

    .line 567
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_4
    if-lt v11, v9, :cond_f

    .line 584
    if-ge v14, v12, :cond_0

    .line 588
    add-int/lit16 v14, v14, 0x800

    .line 436
    goto/16 :goto_0

    .line 475
    .end local v4    # "dim":I
    .end local v11    # "i":I
    :cond_5
    aget-object v7, v19, v18

    .line 476
    .local v7, "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    if-nez v7, :cond_7

    .line 475
    :cond_6
    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    .line 479
    :cond_7
    invoke-virtual {v7}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v5

    .line 483
    :goto_5
    if-ge v5, v14, :cond_6

    .line 484
    and-int/lit16 v0, v5, 0x7ff

    move/from16 v17, v0

    .line 485
    .restart local v17    # "slot":I
    aget v21, v6, v17

    move/from16 v0, v21

    if-ne v0, v5, :cond_8

    .line 489
    const/16 v21, 0x1

    aput v21, v13, v17

    .line 490
    const/16 v21, 0x2

    aput v21, v3, v17

    .line 492
    :cond_8
    invoke-virtual {v7}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v5

    goto :goto_5

    .line 500
    .end local v7    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    .end local v17    # "slot":I
    .restart local v4    # "dim":I
    :cond_9
    aget-object v19, p2, v4

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    const/16 v18, 0x0

    :goto_6
    move/from16 v0, v18

    move/from16 v1, v20

    if-lt v0, v1, :cond_a

    .line 496
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 500
    :cond_a
    aget-object v7, v19, v18

    .line 501
    .restart local v7    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    if-nez v7, :cond_c

    .line 500
    :cond_b
    add-int/lit8 v18, v18, 0x1

    goto :goto_6

    .line 504
    :cond_c
    invoke-virtual {v7}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v5

    .line 508
    :goto_7
    if-ge v5, v14, :cond_b

    .line 509
    and-int/lit16 v0, v5, 0x7ff

    move/from16 v17, v0

    .line 510
    .restart local v17    # "slot":I
    aget v21, v6, v17

    move/from16 v0, v21

    if-ne v0, v5, :cond_d

    aget v21, v3, v17

    move/from16 v0, v21

    if-lt v0, v4, :cond_d

    .line 514
    aget v21, v13, v17

    move/from16 v0, v21

    if-lt v0, v4, :cond_e

    .line 518
    add-int/lit8 v21, v4, 0x1

    aput v21, v13, v17

    .line 519
    add-int/lit8 v21, v4, 0x2

    aput v21, v3, v17

    .line 527
    :cond_d
    :goto_8
    invoke-virtual {v7}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v5

    goto :goto_7

    .line 524
    :cond_e
    add-int/lit8 v21, v4, 0x1

    aput v21, v3, v17

    goto :goto_8

    .line 568
    .end local v7    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    .end local v17    # "slot":I
    .restart local v11    # "i":I
    :cond_f
    aget v17, v10, v11

    .line 569
    .restart local v17    # "slot":I
    aget v18, v6, v17

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectDocID:I

    .line 570
    aget v18, v16, v17

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectScore:F

    .line 575
    aget v18, v3, v17

    add-int/lit8 v19, v15, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_11

    .line 577
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectHit(Lorg/apache/lucene/search/Collector;[Lorg/apache/lucene/search/Collector;)V

    .line 567
    :cond_10
    :goto_9
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_4

    .line 578
    :cond_11
    aget v18, v3, v17

    move/from16 v0, v18

    if-ne v0, v15, :cond_10

    .line 580
    aget v18, v13, v17

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectNearMiss([Lorg/apache/lucene/search/Collector;I)V

    goto :goto_9
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 614
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 619
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 594
    iget v0, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectDocID:I

    return v0
.end method

.method public freq()I
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getChildren()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624
    new-instance v0, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v1, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    const-string v2, "MUST"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public nextDoc()I
    .locals 1

    .prologue
    .line 609
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public score()F
    .locals 1

    .prologue
    .line 599
    iget v0, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->collectScore:F

    return v0
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 14
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 66
    iget-object v8, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v8, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 67
    iget-object v8, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->drillDownCollector:Lorg/apache/lucene/search/Collector;

    iget-object v9, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v8, v9}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V

    .line 68
    iget-object v9, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    array-length v10, v9

    const/4 v8, 0x0

    :goto_0
    if-lt v8, v10, :cond_0

    .line 76
    sget-boolean v8, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->$assertionsDisabled:Z

    if-nez v8, :cond_1

    iget-object v8, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    if-nez v8, :cond_1

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 68
    :cond_0
    aget-object v1, v9, v8

    .line 69
    .local v1, "dim":Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;
    iget-object v11, v1, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->sidewaysCollector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v11, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 70
    iget-object v11, v1, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->sidewaysCollector:Lorg/apache/lucene/search/Collector;

    iget-object v12, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v11, v12}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V

    .line 68
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 79
    .end local v1    # "dim":Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;
    :cond_1
    iget-object v8, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->baseScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v8}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    .line 81
    .local v0, "baseDocID":I
    iget-object v10, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    array-length v11, v10

    const/4 v8, 0x0

    :goto_1
    if-lt v8, v11, :cond_2

    .line 89
    iget-object v8, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    array-length v6, v8

    .line 91
    .local v6, "numDims":I
    new-array v3, v6, [[Lorg/apache/lucene/index/DocsEnum;

    .line 92
    .local v3, "docsEnums":[[Lorg/apache/lucene/index/DocsEnum;
    new-array v7, v6, [Lorg/apache/lucene/search/Collector;

    .line 93
    .local v7, "sidewaysCollectors":[Lorg/apache/lucene/search/Collector;
    const/4 v5, 0x0

    .line 94
    .local v5, "maxFreq":I
    const/4 v1, 0x0

    .local v1, "dim":I
    :goto_2
    if-lt v1, v6, :cond_5

    .line 101
    iget-object v8, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v8}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v8

    add-int/lit8 v9, v0, 0x1

    div-int v4, v8, v9

    .line 113
    .local v4, "estBaseHitCount":I
    div-int/lit8 v8, v5, 0xa

    if-ge v4, v8, :cond_6

    .line 115
    invoke-direct {p0, p1, v3, v7}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->doBaseAdvanceScoring(Lorg/apache/lucene/search/Collector;[[Lorg/apache/lucene/index/DocsEnum;[Lorg/apache/lucene/search/Collector;)V

    .line 123
    :goto_3
    return-void

    .line 81
    .end local v1    # "dim":I
    .end local v3    # "docsEnums":[[Lorg/apache/lucene/index/DocsEnum;
    .end local v4    # "estBaseHitCount":I
    .end local v5    # "maxFreq":I
    .end local v6    # "numDims":I
    .end local v7    # "sidewaysCollectors":[Lorg/apache/lucene/search/Collector;
    :cond_2
    aget-object v1, v10, v8

    .line 82
    .local v1, "dim":Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;
    iget-object v12, v1, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->docsEnums:[Lorg/apache/lucene/index/DocsEnum;

    array-length v13, v12

    const/4 v9, 0x0

    :goto_4
    if-lt v9, v13, :cond_3

    .line 81
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 82
    :cond_3
    aget-object v2, v12, v9

    .line 83
    .local v2, "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    if-eqz v2, :cond_4

    .line 84
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    .line 82
    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 95
    .end local v2    # "docsEnum":Lorg/apache/lucene/index/DocsEnum;
    .local v1, "dim":I
    .restart local v3    # "docsEnums":[[Lorg/apache/lucene/index/DocsEnum;
    .restart local v5    # "maxFreq":I
    .restart local v6    # "numDims":I
    .restart local v7    # "sidewaysCollectors":[Lorg/apache/lucene/search/Collector;
    :cond_5
    iget-object v8, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    aget-object v8, v8, v1

    iget-object v8, v8, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->docsEnums:[Lorg/apache/lucene/index/DocsEnum;

    aput-object v8, v3, v1

    .line 96
    iget-object v8, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    aget-object v8, v8, v1

    iget-object v8, v8, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->sidewaysCollector:Lorg/apache/lucene/search/Collector;

    aput-object v8, v7, v1

    .line 97
    iget-object v8, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    aget-object v8, v8, v1

    iget v8, v8, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->freq:I

    invoke-static {v5, v8}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 116
    .restart local v4    # "estBaseHitCount":I
    :cond_6
    const/4 v8, 0x1

    if-le v6, v8, :cond_7

    iget-object v8, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->dims:[Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    const/4 v9, 0x1

    aget-object v8, v8, v9

    iget v8, v8, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->freq:I

    div-int/lit8 v9, v4, 0xa

    if-ge v8, v9, :cond_7

    .line 118
    invoke-direct {p0, p1, v3, v7}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->doDrillDownAdvanceScoring(Lorg/apache/lucene/search/Collector;[[Lorg/apache/lucene/index/DocsEnum;[Lorg/apache/lucene/search/Collector;)V

    goto :goto_3

    .line 121
    :cond_7
    invoke-direct {p0, p1, v3, v7}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer;->doUnionScoring(Lorg/apache/lucene/search/Collector;[[Lorg/apache/lucene/index/DocsEnum;[Lorg/apache/lucene/search/Collector;)V

    goto :goto_3
.end method
