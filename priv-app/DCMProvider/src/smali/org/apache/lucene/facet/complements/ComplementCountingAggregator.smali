.class public Lorg/apache/lucene/facet/complements/ComplementCountingAggregator;
.super Lorg/apache/lucene/facet/search/CountingAggregator;
.source "ComplementCountingAggregator.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/facet/complements/ComplementCountingAggregator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/complements/ComplementCountingAggregator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([I)V
    .locals 0
    .param p1, "counterArray"    # [I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/search/CountingAggregator;-><init>([I)V

    .line 34
    return-void
.end method


# virtual methods
.method public aggregate(IFLorg/apache/lucene/util/IntsRef;)V
    .locals 5
    .param p1, "docID"    # I
    .param p2, "score"    # F
    .param p3, "ordinals"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v0, v2, :cond_0

    .line 43
    return-void

    .line 39
    :cond_0
    iget-object v2, p3, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v1, v2, v0

    .line 40
    .local v1, "ord":I
    sget-boolean v2, Lorg/apache/lucene/facet/complements/ComplementCountingAggregator;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/facet/complements/ComplementCountingAggregator;->counterArray:[I

    aget v2, v2, v1

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "complement aggregation: count is about to become negative for ordinal "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 41
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/complements/ComplementCountingAggregator;->counterArray:[I

    aget v3, v2, v1

    add-int/lit8 v3, v3, -0x1

    aput v3, v2, v1

    .line 38
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
