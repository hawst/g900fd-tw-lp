.class public Lorg/apache/lucene/facet/index/CountingListBuilder;
.super Ljava/lang/Object;
.source "CountingListBuilder.java"

# interfaces
.implements Lorg/apache/lucene/facet/index/CategoryListBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/index/CountingListBuilder$NoPartitionsOrdinalsEncoder;,
        Lorg/apache/lucene/facet/index/CountingListBuilder$OrdinalsEncoder;,
        Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;
    }
.end annotation


# instance fields
.field private final clp:Lorg/apache/lucene/facet/params/CategoryListParams;

.field private final ordinalsEncoder:Lorg/apache/lucene/facet/index/CountingListBuilder$OrdinalsEncoder;

.field private final taxoWriter:Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;)V
    .locals 2
    .param p1, "categoryListParams"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p2, "indexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p3, "taxoWriter"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object p3, p0, Lorg/apache/lucene/facet/index/CountingListBuilder;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;

    .line 124
    iput-object p1, p0, Lorg/apache/lucene/facet/index/CountingListBuilder;->clp:Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 125
    invoke-virtual {p2}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getPartitionSize()I

    move-result v0

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 126
    new-instance v0, Lorg/apache/lucene/facet/index/CountingListBuilder$NoPartitionsOrdinalsEncoder;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/index/CountingListBuilder$NoPartitionsOrdinalsEncoder;-><init>(Lorg/apache/lucene/facet/params/CategoryListParams;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/index/CountingListBuilder;->ordinalsEncoder:Lorg/apache/lucene/facet/index/CountingListBuilder$OrdinalsEncoder;

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    new-instance v0, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;

    invoke-direct {v0, p2, p1}, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/params/CategoryListParams;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/index/CountingListBuilder;->ordinalsEncoder:Lorg/apache/lucene/facet/index/CountingListBuilder$OrdinalsEncoder;

    goto :goto_0
.end method


# virtual methods
.method public build(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Iterable;)Ljava/util/Map;
    .locals 10
    .param p1, "ordinals"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/IntsRef;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    .local p2, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    iget v6, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 147
    .local v6, "upto":I
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 148
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v6, :cond_0

    .line 167
    iget-object v7, p0, Lorg/apache/lucene/facet/index/CountingListBuilder;->ordinalsEncoder:Lorg/apache/lucene/facet/index/CountingListBuilder$OrdinalsEncoder;

    invoke-virtual {v7, p1}, Lorg/apache/lucene/facet/index/CountingListBuilder$OrdinalsEncoder;->encode(Lorg/apache/lucene/util/IntsRef;)Ljava/util/Map;

    move-result-object v7

    return-object v7

    .line 149
    :cond_0
    iget-object v7, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v4, v7, v1

    .line 150
    .local v4, "ordinal":I
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 151
    .local v0, "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    iget-object v7, p0, Lorg/apache/lucene/facet/index/CountingListBuilder;->clp:Lorg/apache/lucene/facet/params/CategoryListParams;

    iget-object v8, v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Lorg/apache/lucene/facet/params/CategoryListParams;->getOrdinalPolicy(Ljava/lang/String;)Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    move-result-object v3

    .line 152
    .local v3, "op":Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    sget-object v7, Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;->NO_PARENTS:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    if-eq v3, v7, :cond_1

    .line 154
    iget-object v7, p0, Lorg/apache/lucene/facet/index/CountingListBuilder;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;

    invoke-interface {v7, v4}, Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;->getParent(I)I

    move-result v5

    .line 155
    .local v5, "parent":I
    if-lez v5, :cond_1

    .line 157
    :goto_1
    if-gtz v5, :cond_2

    .line 161
    sget-object v7, Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;->ALL_BUT_DIMENSION:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    if-ne v3, v7, :cond_1

    .line 162
    iget v7, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 148
    .end local v5    # "parent":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 158
    .restart local v5    # "parent":I
    :cond_2
    iget-object v7, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v8, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v9, v8, 0x1

    iput v9, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    aput v5, v7, v8

    .line 159
    iget-object v7, p0, Lorg/apache/lucene/facet/index/CountingListBuilder;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;

    invoke-interface {v7, v5}, Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;->getParent(I)I

    move-result v5

    goto :goto_1
.end method
