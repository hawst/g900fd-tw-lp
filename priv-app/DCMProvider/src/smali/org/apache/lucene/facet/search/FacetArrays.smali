.class public Lorg/apache/lucene/facet/search/FacetArrays;
.super Ljava/lang/Object;
.source "FacetArrays.java"


# instance fields
.field public final arrayLength:I

.field private floats:[F

.field private ints:[I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "arrayLength"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput p1, p0, Lorg/apache/lucene/facet/search/FacetArrays;->arrayLength:I

    .line 46
    return-void
.end method


# virtual methods
.method protected doFree([F[I)V
    .locals 0
    .param p1, "floats"    # [F
    .param p2, "ints"    # [I

    .prologue
    .line 57
    return-void
.end method

.method public final free()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetArrays;->floats:[F

    iget-object v1, p0, Lorg/apache/lucene/facet/search/FacetArrays;->ints:[I

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/facet/search/FacetArrays;->doFree([F[I)V

    .line 65
    iput-object v2, p0, Lorg/apache/lucene/facet/search/FacetArrays;->ints:[I

    .line 66
    iput-object v2, p0, Lorg/apache/lucene/facet/search/FacetArrays;->floats:[F

    .line 67
    return-void
.end method

.method public final getFloatArray()[F
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetArrays;->floats:[F

    if-nez v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/FacetArrays;->newFloatArray()[F

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetArrays;->floats:[F

    .line 80
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetArrays;->floats:[F

    return-object v0
.end method

.method public final getIntArray()[I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetArrays;->ints:[I

    if-nez v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/FacetArrays;->newIntArray()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetArrays;->ints:[I

    .line 73
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetArrays;->ints:[I

    return-object v0
.end method

.method protected newFloatArray()[F
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lorg/apache/lucene/facet/search/FacetArrays;->arrayLength:I

    new-array v0, v0, [F

    return-object v0
.end method

.method protected newIntArray()[I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lorg/apache/lucene/facet/search/FacetArrays;->arrayLength:I

    new-array v0, v0, [I

    return-object v0
.end method
