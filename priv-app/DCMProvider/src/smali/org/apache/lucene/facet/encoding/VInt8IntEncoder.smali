.class public final Lorg/apache/lucene/facet/encoding/VInt8IntEncoder;
.super Lorg/apache/lucene/facet/encoding/IntEncoder;
.source "VInt8IntEncoder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/IntEncoder;-><init>()V

    return-void
.end method


# virtual methods
.method public createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lorg/apache/lucene/facet/encoding/VInt8IntDecoder;

    invoke-direct {v0}, Lorg/apache/lucene/facet/encoding/VInt8IntDecoder;-><init>()V

    return-object v0
.end method

.method public encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 10
    .param p1, "values"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "buf"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/high16 v9, 0xfe00000

    const/high16 v8, -0x10000000

    const v7, 0x1fc000

    .line 54
    const/4 v4, 0x0

    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 55
    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    mul-int/lit8 v1, v4, 0x5

    .line 56
    .local v1, "maxBytesNeeded":I
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v4, v4

    if-ge v4, v1, :cond_0

    .line 57
    invoke-virtual {p2, v1}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 60
    :cond_0
    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v2, v4, v5

    .line 61
    .local v2, "upto":I
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_1

    .line 92
    return-void

    .line 64
    :cond_1
    iget-object v4, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v3, v4, v0

    .line 65
    .local v3, "value":I
    and-int/lit8 v4, v3, -0x80

    if-nez v4, :cond_2

    .line 66
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-byte v6, v3

    aput-byte v6, v4, v5

    .line 67
    iget v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 61
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_2
    and-int/lit16 v4, v3, -0x4000

    if-nez v4, :cond_3

    .line 69
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int/lit16 v6, v3, 0x3f80

    shr-int/lit8 v6, v6, 0x7

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 70
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x1

    and-int/lit8 v6, v3, 0x7f

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 71
    iget v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x2

    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_1

    .line 72
    :cond_3
    const/high16 v4, -0x200000

    and-int/2addr v4, v3

    if-nez v4, :cond_4

    .line 73
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int v6, v3, v7

    shr-int/lit8 v6, v6, 0xe

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 74
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x1

    and-int/lit16 v6, v3, 0x3f80

    shr-int/lit8 v6, v6, 0x7

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 75
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x2

    and-int/lit8 v6, v3, 0x7f

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 76
    iget v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x3

    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_1

    .line 77
    :cond_4
    and-int v4, v3, v8

    if-nez v4, :cond_5

    .line 78
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int v6, v3, v9

    shr-int/lit8 v6, v6, 0x15

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 79
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x1

    and-int v6, v3, v7

    shr-int/lit8 v6, v6, 0xe

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 80
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x2

    and-int/lit16 v6, v3, 0x3f80

    shr-int/lit8 v6, v6, 0x7

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 81
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x3

    and-int/lit8 v6, v3, 0x7f

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 82
    iget v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x4

    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto/16 :goto_1

    .line 84
    :cond_5
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int v6, v3, v8

    shr-int/lit8 v6, v6, 0x1c

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 85
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x1

    and-int v6, v3, v9

    shr-int/lit8 v6, v6, 0x15

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 86
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x2

    and-int v6, v3, v7

    shr-int/lit8 v6, v6, 0xe

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 87
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x3

    and-int/lit16 v6, v3, 0x3f80

    shr-int/lit8 v6, v6, 0x7

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 88
    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x4

    and-int/lit8 v6, v3, 0x7f

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 89
    iget v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, 0x5

    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto/16 :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    const-string v0, "VInt8"

    return-object v0
.end method
