.class public Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;
.super Lorg/apache/lucene/facet/search/FacetsAccumulator;
.source "StandardFacetsAccumulator.java"


# static fields
.field public static final DEFAULT_COMPLEMENT_THRESHOLD:D = 0.6

.field public static final DISABLE_COMPLEMENT:D = Infinity

.field public static final FORCE_COMPLEMENT:D

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private accumulateGuard:Ljava/lang/Object;

.field private complementThreshold:D

.field protected isUsingComplements:Z

.field protected maxPartitions:I

.field protected partitionSize:I

.field private totalFacetCounts:Lorg/apache/lucene/facet/complements/TotalFacetCounts;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->logger:Ljava/util/logging/Logger;

    .line 87
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V
    .locals 2
    .param p1, "searchParams"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p2, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .prologue
    .line 101
    new-instance v0, Lorg/apache/lucene/facet/search/FacetArrays;

    .line 102
    iget-object v1, p1, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-static {v1, p3}, Lorg/apache/lucene/facet/util/PartitionsUtils;->partitionSize(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)I

    move-result v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/search/FacetArrays;-><init>(I)V

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 4
    .param p1, "searchParams"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p2, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p4, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/facet/search/FacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->isUsingComplements:Z

    .line 111
    iget-object v0, p1, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-static {v0, p3}, Lorg/apache/lucene/facet/util/PartitionsUtils;->partitionSize(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->partitionSize:I

    .line 112
    iget-object v0, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getSize()I

    move-result v0

    int-to-double v0, v0

    iget v2, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->partitionSize:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->maxPartitions:I

    .line 113
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->accumulateGuard:Ljava/lang/Object;

    .line 114
    return-void
.end method

.method private final fillArraysForPartition(Lorg/apache/lucene/facet/search/ScoredDocIDs;Lorg/apache/lucene/facet/search/FacetArrays;I)V
    .locals 18
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .param p2, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p3, "partition"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->isUsingComplements:Z

    if-eqz v15, :cond_1

    .line 262
    invoke-interface/range {p1 .. p1}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->size()I

    move-result v15

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2, v15}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->initArraysByTotalCounts(Lorg/apache/lucene/facet/search/FacetArrays;II)V

    .line 267
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->getCategoryListMap(Lorg/apache/lucene/facet/search/FacetArrays;I)Ljava/util/HashMap;

    move-result-object v5

    .line 269
    .local v5, "categoryLists":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/facet/search/CategoryListIterator;Lorg/apache/lucene/facet/search/Aggregator;>;"
    new-instance v13, Lorg/apache/lucene/util/IntsRef;

    const/16 v15, 0x20

    invoke-direct {v13, v15}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    .line 270
    .local v13, "ordinals":Lorg/apache/lucene/util/IntsRef;
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-nez v16, :cond_2

    .line 312
    return-void

    .line 264
    .end local v5    # "categoryLists":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/facet/search/CategoryListIterator;Lorg/apache/lucene/facet/search/Aggregator;>;"
    .end local v13    # "ordinals":Lorg/apache/lucene/util/IntsRef;
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/facet/search/FacetArrays;->free()V

    goto :goto_0

    .line 270
    .restart local v5    # "categoryLists":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/facet/search/CategoryListIterator;Lorg/apache/lucene/facet/search/Aggregator;>;"
    .restart local v13    # "ordinals":Lorg/apache/lucene/util/IntsRef;
    :cond_2
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 271
    .local v9, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/facet/search/CategoryListIterator;Lorg/apache/lucene/facet/search/Aggregator;>;"
    invoke-interface/range {p1 .. p1}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;

    move-result-object v10

    .line 272
    .local v10, "iterator":Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/facet/search/CategoryListIterator;

    .line 273
    .local v4, "categoryListIter":Lorg/apache/lucene/facet/search/CategoryListIterator;
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/facet/search/Aggregator;

    .line 274
    .local v3, "aggregator":Lorg/apache/lucene/facet/search/Aggregator;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->indexReader:Lorg/apache/lucene/index/IndexReader;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 275
    .local v6, "contexts":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    const/4 v7, 0x0

    .line 276
    .local v7, "current":Lorg/apache/lucene/index/AtomicReaderContext;
    const/4 v12, -0x1

    .line 277
    .local v12, "maxDoc":I
    :cond_3
    :goto_1
    invoke-interface {v10}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->next()Z

    move-result v16

    if-eqz v16, :cond_0

    .line 278
    invoke-interface {v10}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getDocID()I

    move-result v8

    .line 279
    .local v8, "docID":I
    if-lt v8, v12, :cond_8

    .line 280
    const/4 v11, 0x0

    .line 282
    .local v11, "iteratorDone":Z
    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-nez v16, :cond_5

    .line 283
    new-instance v15, Ljava/lang/RuntimeException;

    const-string v16, "ScoredDocIDs contains documents outside this reader\'s segments !?"

    invoke-direct/range {v15 .. v16}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 285
    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "current":Lorg/apache/lucene/index/AtomicReaderContext;
    check-cast v7, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 286
    .restart local v7    # "current":Lorg/apache/lucene/index/AtomicReaderContext;
    iget v0, v7, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    move/from16 v16, v0

    invoke-virtual {v7}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v17

    add-int v12, v16, v17

    .line 287
    if-ge v8, v12, :cond_7

    .line 288
    invoke-interface {v4, v7}, Lorg/apache/lucene/facet/search/CategoryListIterator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Z

    move-result v14

    .line 289
    .local v14, "validSegment":Z
    invoke-interface {v3, v7}, Lorg/apache/lucene/facet/search/Aggregator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Z

    move-result v16

    and-int v14, v14, v16

    .line 290
    if-nez v14, :cond_7

    .line 291
    :goto_2
    if-ge v8, v12, :cond_6

    invoke-interface {v10}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->next()Z

    move-result v16

    if-nez v16, :cond_9

    .line 294
    :cond_6
    if-ge v8, v12, :cond_7

    .line 295
    const/4 v11, 0x1

    .line 281
    .end local v14    # "validSegment":Z
    :cond_7
    if-ge v8, v12, :cond_4

    .line 300
    if-nez v11, :cond_0

    .line 304
    .end local v11    # "iteratorDone":Z
    :cond_8
    iget v0, v7, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    move/from16 v16, v0

    sub-int v8, v8, v16

    .line 305
    invoke-interface {v4, v8, v13}, Lorg/apache/lucene/facet/search/CategoryListIterator;->getOrdinals(ILorg/apache/lucene/util/IntsRef;)V

    .line 306
    iget v0, v13, Lorg/apache/lucene/util/IntsRef;->length:I

    move/from16 v16, v0

    if-eqz v16, :cond_3

    .line 309
    invoke-interface {v10}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getScore()F

    move-result v16

    move/from16 v0, v16

    invoke-interface {v3, v8, v0, v13}, Lorg/apache/lucene/facet/search/Aggregator;->aggregate(IFLorg/apache/lucene/util/IntsRef;)V

    goto :goto_1

    .line 292
    .restart local v11    # "iteratorDone":Z
    .restart local v14    # "validSegment":Z
    :cond_9
    invoke-interface {v10}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getDocID()I

    move-result v8

    goto :goto_2
.end method

.method private final initArraysByTotalCounts(Lorg/apache/lucene/facet/search/FacetArrays;II)V
    .locals 8
    .param p1, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p2, "partition"    # I
    .param p3, "nAccumulatedDocs"    # I

    .prologue
    .line 316
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetArrays;->getIntArray()[I

    move-result-object v2

    .line 317
    .local v2, "intArray":[I
    iget-object v3, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->totalFacetCounts:Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    invoke-virtual {v3, v2, p2}, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->fillTotalCountsForPartition([II)V

    .line 318
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->getTotalCountsFactor()D

    move-result-wide v4

    .line 320
    .local v4, "totalCountsFactor":D
    const-wide v6, 0x3fefffeb074a771dL    # 0.99999

    cmpg-double v3, v4, v6

    if-gez v3, :cond_0

    .line 321
    add-int/lit8 v0, p3, 0x1

    .line 322
    .local v0, "delta":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-lt v1, v3, :cond_1

    .line 329
    .end local v0    # "delta":I
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 323
    .restart local v0    # "delta":I
    .restart local v1    # "i":I
    :cond_1
    aget v3, v2, v1

    int-to-double v6, v3

    mul-double/2addr v6, v4

    double-to-int v3, v6

    aput v3, v2, v1

    .line 326
    aget v3, v2, v1

    add-int/2addr v3, v0

    aput v3, v2, v1

    .line 322
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public accumulate(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResult;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 378
    .local p1, "matchingDocs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;>;"
    new-instance v0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->accumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public accumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Ljava/util/List;
    .locals 20
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/search/ScoredDocIDs;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResult;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->accumulateGuard:Ljava/lang/Object;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 125
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->shouldComplement(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Z

    move-result v15

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->isUsingComplements:Z

    .line 127
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->isUsingComplements:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v15, :cond_0

    .line 129
    :try_start_1
    invoke-static {}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->getSingleton()Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->indexReader:Lorg/apache/lucene/index/IndexReader;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    move-object/from16 v18, v0

    .line 130
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    move-object/from16 v19, v0

    .line 129
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v15, v0, v1, v2}, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;->getTotalCounts(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->totalFacetCounts:Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    .line 131
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->totalFacetCounts:Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    if-eqz v15, :cond_1

    .line 132
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->indexReader:Lorg/apache/lucene/index/IndexReader;

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils;->getComplementSet(Lorg/apache/lucene/facet/search/ScoredDocIDs;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/facet/search/ScoredDocIDs;
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object p1

    .line 160
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual/range {p0 .. p1}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->actualDocsToAccumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Lorg/apache/lucene/facet/search/ScoredDocIDs;

    move-result-object p1

    .line 162
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 165
    .local v6, "fr2tmpRes":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;>;"
    const/4 v11, 0x0

    .local v11, "part":I
    :goto_1
    :try_start_3
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->maxPartitions:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-lt v11, v15, :cond_4

    .line 191
    :try_start_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    invoke-virtual {v15}, Lorg/apache/lucene/facet/search/FacetArrays;->free()V

    .line 195
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 196
    .local v12, "res":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v15, v15, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-nez v17, :cond_8

    .line 210
    monitor-exit v16
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    return-object v12

    .line 134
    .end local v6    # "fr2tmpRes":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;>;"
    .end local v11    # "part":I
    .end local v12    # "res":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    :cond_1
    const/4 v15, 0x0

    :try_start_5
    move-object/from16 v0, p0

    iput-boolean v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->isUsingComplements:Z
    :try_end_5
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 136
    :catch_0
    move-exception v3

    .line 144
    .local v3, "e":Ljava/lang/UnsupportedOperationException;
    :try_start_6
    sget-object v15, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->logger:Ljava/util/logging/Logger;

    sget-object v17, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 145
    sget-object v15, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->logger:Ljava/util/logging/Logger;

    sget-object v17, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    const-string v18, "IndexReader used does not support completents: "

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 147
    :cond_2
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->isUsingComplements:Z

    goto :goto_0

    .line 122
    .end local v3    # "e":Ljava/lang/UnsupportedOperationException;
    :catchall_0
    move-exception v15

    monitor-exit v16
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v15

    .line 148
    :catch_1
    move-exception v3

    .line 149
    .local v3, "e":Ljava/io/IOException;
    :try_start_7
    sget-object v15, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->logger:Ljava/util/logging/Logger;

    sget-object v17, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 150
    sget-object v15, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->logger:Ljava/util/logging/Logger;

    sget-object v17, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    const-string v18, "Failed to load/calculate total counts (complement counting disabled): "

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 153
    :cond_3
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->isUsingComplements:Z

    goto :goto_0

    .line 154
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 156
    .local v3, "e":Ljava/lang/Exception;
    new-instance v15, Ljava/io/IOException;

    const-string v17, "PANIC: Got unexpected exception while trying to get/calculate total counts"

    move-object/from16 v0, v17

    invoke-direct {v15, v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v15
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 168
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v6    # "fr2tmpRes":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;>;"
    .restart local v11    # "part":I
    :cond_4
    :try_start_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15, v11}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->fillArraysForPartition(Lorg/apache/lucene/facet/search/ScoredDocIDs;Lorg/apache/lucene/facet/search/FacetArrays;I)V

    .line 170
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->partitionSize:I

    mul-int v9, v11, v15

    .line 176
    .local v9, "offset":I
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 177
    .local v8, "handledRequests":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/facet/search/FacetRequest;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v15, v15, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_5
    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-nez v17, :cond_6

    .line 165
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 177
    :cond_6
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 179
    .local v5, "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    invoke-virtual {v8, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 180
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->createFacetResultsHandler(Lorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;

    move-result-object v7

    .line 181
    .local v7, "frHndlr":Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;
    invoke-virtual {v7, v9}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;->fetchPartitionResult(I)Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;

    move-result-object v13

    .line 182
    .local v13, "res4fr":Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    invoke-virtual {v6, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;

    .line 183
    .local v10, "oldRes":Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    if-eqz v10, :cond_7

    .line 184
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v10, v17, v18

    const/16 v18, 0x1

    aput-object v13, v17, v18

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;->mergeResults([Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;)Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;

    move-result-object v13

    .line 186
    :cond_7
    invoke-virtual {v6, v5, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_3

    .line 190
    .end local v5    # "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    .end local v7    # "frHndlr":Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;
    .end local v8    # "handledRequests":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/facet/search/FacetRequest;>;"
    .end local v9    # "offset":I
    .end local v10    # "oldRes":Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    .end local v13    # "res4fr":Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    :catchall_1
    move-exception v15

    .line 191
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/facet/search/FacetArrays;->free()V

    .line 192
    throw v15

    .line 196
    .restart local v12    # "res":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    :cond_8
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 197
    .restart local v5    # "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->createFacetResultsHandler(Lorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;

    move-result-object v7

    .line 198
    .restart local v7    # "frHndlr":Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;
    invoke-virtual {v6, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;

    .line 199
    .local v14, "tmpResult":Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    if-nez v14, :cond_9

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    move-object/from16 v17, v0

    iget-object v0, v5, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v17

    move/from16 v0, v17

    invoke-static {v0, v5}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->emptyResult(ILorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/search/FacetResult;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 204
    :cond_9
    invoke-virtual {v7, v14}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;->renderFacetResult(Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;)Lorg/apache/lucene/facet/search/FacetResult;

    move-result-object v4

    .line 206
    .local v4, "facetRes":Lorg/apache/lucene/facet/search/FacetResult;
    invoke-virtual {v7, v4}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;->labelResult(Lorg/apache/lucene/facet/search/FacetResult;)V

    .line 207
    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_2
.end method

.method protected actualDocsToAccumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .locals 0
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    return-object p1
.end method

.method protected createFacetResultsHandler(Lorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;
    .locals 3
    .param p1, "fr"    # Lorg/apache/lucene/facet/search/FacetRequest;

    .prologue
    .line 226
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetRequest;->getResultMode()Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;->PER_NODE_IN_TREE:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    if-ne v0, v1, :cond_0

    .line 227
    new-instance v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;

    iget-object v1, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    invoke-direct {v0, v1, p1, v2}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 229
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;

    iget-object v1, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    invoke-direct {v0, v1, p1, v2}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V

    goto :goto_0
.end method

.method protected bridge synthetic createFacetResultsHandler(Lorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/search/FacetResultsHandler;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->createFacetResultsHandler(Lorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;

    move-result-object v0

    return-object v0
.end method

.method protected getCategoryListMap(Lorg/apache/lucene/facet/search/FacetArrays;I)Ljava/util/HashMap;
    .locals 9
    .param p1, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p2, "partition"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/search/FacetArrays;",
            "I)",
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/facet/search/CategoryListIterator;",
            "Lorg/apache/lucene/facet/search/Aggregator;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 356
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 358
    .local v1, "categoryLists":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/facet/search/CategoryListIterator;Lorg/apache/lucene/facet/search/Aggregator;>;"
    iget-object v6, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v4, v6, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 359
    .local v4, "indexingParams":Lorg/apache/lucene/facet/params/FacetIndexingParams;
    iget-object v6, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v6, v6, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 373
    return-object v1

    .line 359
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 360
    .local v3, "facetRequest":Lorg/apache/lucene/facet/search/FacetRequest;
    iget-boolean v7, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->isUsingComplements:Z

    iget-object v8, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-virtual {v3, v7, p1, v8}, Lorg/apache/lucene/facet/search/FacetRequest;->createAggregator(ZLorg/apache/lucene/facet/search/FacetArrays;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/Aggregator;

    move-result-object v0

    .line 362
    .local v0, "categoryAggregator":Lorg/apache/lucene/facet/search/Aggregator;
    iget-object v7, v3, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v4, v7}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v7

    invoke-virtual {v7, p2}, Lorg/apache/lucene/facet/params/CategoryListParams;->createCategoryListIterator(I)Lorg/apache/lucene/facet/search/CategoryListIterator;

    move-result-object v2

    .line 365
    .local v2, "cli":Lorg/apache/lucene/facet/search/CategoryListIterator;
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/facet/search/Aggregator;

    .line 367
    .local v5, "old":Lorg/apache/lucene/facet/search/Aggregator;
    if-eqz v5, :cond_0

    invoke-virtual {v5, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 368
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "Overriding existing category list with different aggregator"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public getComplementThreshold()D
    .locals 2

    .prologue
    .line 386
    iget-wide v0, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->complementThreshold:D

    return-wide v0
.end method

.method protected getTotalCountsFactor()D
    .locals 2

    .prologue
    .line 338
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    return-wide v0
.end method

.method public isUsingComplements()Z
    .locals 1

    .prologue
    .line 416
    iget-boolean v0, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->isUsingComplements:Z

    return v0
.end method

.method protected mayComplement()Z
    .locals 3

    .prologue
    .line 216
    iget-object v1, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v1, v1, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 221
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 216
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 217
    .local v0, "freq":Lorg/apache/lucene/facet/search/FacetRequest;
    instance-of v2, v0, Lorg/apache/lucene/facet/search/CountFacetRequest;

    if-nez v2, :cond_0

    .line 218
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setComplementThreshold(D)V
    .locals 1
    .param p1, "complementThreshold"    # D

    .prologue
    .line 411
    iput-wide p1, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->complementThreshold:D

    .line 412
    return-void
.end method

.method protected shouldComplement(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Z
    .locals 6
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;

    .prologue
    .line 251
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->mayComplement()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->size()I

    move-result v0

    int-to-double v0, v0

    iget-object v2, p0, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->indexReader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v2

    int-to-double v2, v2

    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->getComplementThreshold()D

    move-result-wide v4

    mul-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
