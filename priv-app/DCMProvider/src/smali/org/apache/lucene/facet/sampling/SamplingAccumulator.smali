.class public Lorg/apache/lucene/facet/sampling/SamplingAccumulator;
.super Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;
.source "SamplingAccumulator.java"


# instance fields
.field private final sampler:Lorg/apache/lucene/facet/sampling/Sampler;

.field private samplingRatio:D


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/sampling/Sampler;Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V
    .locals 2
    .param p1, "sampler"    # Lorg/apache/lucene/facet/sampling/Sampler;
    .param p2, "searchParams"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p3, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p4, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .prologue
    .line 74
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V

    .line 57
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->samplingRatio:D

    .line 75
    iput-object p1, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    .line 76
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/sampling/Sampler;Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 2
    .param p1, "sampler"    # Lorg/apache/lucene/facet/sampling/Sampler;
    .param p2, "searchParams"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p3, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p4, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p5, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 63
    invoke-direct {p0, p2, p3, p4, p5}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 57
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->samplingRatio:D

    .line 64
    iput-object p1, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    .line 65
    return-void
.end method


# virtual methods
.method public accumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Ljava/util/List;
    .locals 10
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/search/ScoredDocIDs;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResult;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v3, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    .line 82
    .local v3, "original":Lorg/apache/lucene/facet/params/FacetSearchParams;
    iget-object v5, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    invoke-virtual {v5, v3}, Lorg/apache/lucene/facet/sampling/Sampler;->overSampledSearchParams(Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/params/FacetSearchParams;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    .line 84
    invoke-super {p0, p1}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->accumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Ljava/util/List;

    move-result-object v4

    .line 86
    .local v4, "sampleRes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v0, "fixedRes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 103
    iput-object v3, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    .line 105
    return-object v0

    .line 87
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/search/FacetResult;

    .line 89
    .local v1, "fres":Lorg/apache/lucene/facet/search/FacetResult;
    invoke-virtual {v1}, Lorg/apache/lucene/facet/search/FacetResult;->getFacetRequest()Lorg/apache/lucene/facet/search/FacetRequest;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->createFacetResultsHandler(Lorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;

    move-result-object v2

    .line 91
    .local v2, "frh":Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;
    iget-object v6, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    iget-object v7, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->indexReader:Lorg/apache/lucene/index/IndexReader;

    iget-object v8, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v9, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    invoke-virtual {v6, v7, v8, v9}, Lorg/apache/lucene/facet/sampling/Sampler;->getSampleFixer(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/sampling/SampleFixer;

    move-result-object v6

    invoke-interface {v6, p1, v1}, Lorg/apache/lucene/facet/sampling/SampleFixer;->fixResult(Lorg/apache/lucene/facet/search/ScoredDocIDs;Lorg/apache/lucene/facet/search/FacetResult;)V

    .line 93
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;->rearrangeFacetResult(Lorg/apache/lucene/facet/search/FacetResult;)Lorg/apache/lucene/facet/search/FacetResult;

    move-result-object v1

    .line 96
    iget-object v6, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    invoke-virtual {v6, v1}, Lorg/apache/lucene/facet/sampling/Sampler;->trimResult(Lorg/apache/lucene/facet/search/FacetResult;)Lorg/apache/lucene/facet/search/FacetResult;

    move-result-object v1

    .line 99
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;->labelResult(Lorg/apache/lucene/facet/search/FacetResult;)V

    .line 100
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected actualDocsToAccumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .locals 4
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    iget-object v1, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/facet/sampling/Sampler;->getSampleSet(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;

    move-result-object v0

    .line 111
    .local v0, "sampleRes":Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;
    iget-wide v2, v0, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;->actualSampleRatio:D

    iput-wide v2, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->samplingRatio:D

    .line 112
    iget-object v1, v0, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;->docids:Lorg/apache/lucene/facet/search/ScoredDocIDs;

    return-object v1
.end method

.method protected getTotalCountsFactor()D
    .locals 4

    .prologue
    .line 117
    iget-wide v0, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->samplingRatio:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Total counts ratio unavailable because actualDocsToAccumulate() was not invoked"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/facet/sampling/SamplingAccumulator;->samplingRatio:D

    return-wide v0
.end method
