.class public Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;
.super Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;
.source "TopKInEachNodeHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;,
        Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;,
        Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;,
        Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategoryHeap;,
        Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AscValueACComparator;,
        Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$DescValueACComparator;,
        Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;,
        Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ResultNodeHeap;
    }
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 0
    .param p1, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p2, "facetRequest"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 67
    return-void
.end method

.method private countOnly(I[I[IIIIII)I
    .locals 13
    .param p1, "ordinal"    # I
    .param p2, "youngestChild"    # [I
    .param p3, "olderSibling"    # [I
    .param p4, "partitionSize"    # I
    .param p5, "offset"    # I
    .param p6, "endOffset"    # I
    .param p7, "currentDepth"    # I
    .param p8, "maxDepth"    # I

    .prologue
    .line 374
    const/4 v11, 0x0

    .line 375
    .local v11, "ret":I
    move/from16 v0, p5

    if-gt v0, p1, :cond_0

    .line 377
    const-wide/16 v4, 0x0

    iget-object v2, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    iget-object v6, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    rem-int v7, p1, p4

    invoke-virtual {v2, v6, v7}, Lorg/apache/lucene/facet/search/FacetRequest;->getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D

    move-result-wide v6

    cmpl-double v2, v4, v6

    if-eqz v2, :cond_0

    .line 378
    add-int/lit8 v11, v11, 0x1

    .line 382
    :cond_0
    move/from16 v0, p7

    move/from16 v1, p8

    if-lt v0, v1, :cond_1

    move v12, v11

    .line 395
    .end local v11    # "ret":I
    .local v12, "ret":I
    :goto_0
    return v12

    .line 386
    .end local v12    # "ret":I
    .restart local v11    # "ret":I
    :cond_1
    aget v3, p2, p1

    .line 387
    .local v3, "yc":I
    :goto_1
    move/from16 v0, p6

    if-ge v3, v0, :cond_2

    .line 390
    :goto_2
    const/4 v2, -0x1

    if-gt v3, v2, :cond_3

    move v12, v11

    .line 395
    .end local v11    # "ret":I
    .restart local v12    # "ret":I
    goto :goto_0

    .line 388
    .end local v12    # "ret":I
    .restart local v11    # "ret":I
    :cond_2
    aget v3, p3, v3

    goto :goto_1

    .line 392
    :cond_3
    add-int/lit8 v9, p7, 0x1

    move-object v2, p0

    move-object v4, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v10, p8

    invoke-direct/range {v2 .. v10}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->countOnly(I[I[IIIIII)I

    move-result v2

    add-int/2addr v11, v2

    .line 393
    aget v3, p3, v3

    goto :goto_2
.end method

.method private generateNode(IDLorg/apache/lucene/facet/collections/IntToObjectMap;)Lorg/apache/lucene/facet/search/FacetResultNode;
    .locals 8
    .param p1, "ordinal"    # I
    .param p2, "val"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ID",
            "Lorg/apache/lucene/facet/collections/IntToObjectMap",
            "<",
            "Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;",
            ">;)",
            "Lorg/apache/lucene/facet/search/FacetResultNode;"
        }
    .end annotation

    .prologue
    .line 713
    .local p4, "mapToAACOs":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;>;"
    new-instance v3, Lorg/apache/lucene/facet/search/FacetResultNode;

    invoke-direct {v3, p1, p2, p3}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    .line 714
    .local v3, "node":Lorg/apache/lucene/facet/search/FacetResultNode;
    invoke-virtual {p4, p1}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;

    .line 715
    .local v0, "aaco":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;
    if-nez v0, :cond_0

    .line 723
    :goto_0
    return-object v3

    .line 718
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 719
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    array-length v4, v4

    if-lt v1, v4, :cond_1

    .line 722
    iput-object v2, v3, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    goto :goto_0

    .line 720
    :cond_1
    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    aget v4, v4, v1

    iget-object v5, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->values:[D

    aget-wide v6, v5, v1

    invoke-direct {p0, v4, v6, v7, p4}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->generateNode(IDLorg/apache/lucene/facet/collections/IntToObjectMap;)Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 719
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getSuitableACComparator()Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/search/FacetRequest;->getSortOrder()Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;->ASCENDING:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    if-ne v0, v1, :cond_0

    .line 535
    new-instance v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AscValueACComparator;

    invoke-direct {v0}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AscValueACComparator;-><init>()V

    .line 537
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$DescValueACComparator;

    invoke-direct {v0}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$DescValueACComparator;-><init>()V

    goto :goto_0
.end method

.method private rearrangeChilrenOfNode(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/util/PriorityQueue;)V
    .locals 5
    .param p1, "node"    # Lorg/apache/lucene/facet/search/FacetResultNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            "Lorg/apache/lucene/util/PriorityQueue",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 681
    .local p2, "nodesHeap":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    invoke-virtual {p2}, Lorg/apache/lucene/util/PriorityQueue;->clear()V

    .line 682
    iget-object v3, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 685
    invoke-virtual {p2}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v1

    .line 686
    .local v1, "size":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 687
    .local v2, "subResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    :goto_1
    invoke-virtual {p2}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v3

    if-gtz v3, :cond_1

    .line 690
    iput-object v2, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    .line 691
    iget-object v3, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 695
    return-void

    .line 682
    .end local v1    # "size":I
    .end local v2    # "subResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 683
    .local v0, "frn":Lorg/apache/lucene/facet/search/FacetResultNode;
    invoke-virtual {p2, v0}, Lorg/apache/lucene/util/PriorityQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 688
    .end local v0    # "frn":Lorg/apache/lucene/facet/search/FacetResultNode;
    .restart local v1    # "size":I
    .restart local v2    # "subResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {p2}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/facet/search/FacetResultNode;

    invoke-virtual {v2, v4, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 691
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 692
    .restart local v0    # "frn":Lorg/apache/lucene/facet/search/FacetResultNode;
    invoke-direct {p0, v0, p2}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->rearrangeChilrenOfNode(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/util/PriorityQueue;)V

    goto :goto_2
.end method

.method private recursivelyLabel(Lorg/apache/lucene/facet/search/FacetResultNode;I)V
    .locals 4
    .param p1, "node"    # Lorg/apache/lucene/facet/search/FacetResultNode;
    .param p2, "numToLabel"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 651
    if-nez p1, :cond_1

    .line 664
    :cond_0
    :goto_0
    return-void

    .line 654
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget v3, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getPath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v2

    iput-object v2, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 657
    const/4 v1, 0x0

    .line 658
    .local v1, "numLabeled":I
    iget-object v2, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 659
    .local v0, "frn":Lorg/apache/lucene/facet/search/FacetResultNode;
    invoke-direct {p0, v0, p2}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->recursivelyLabel(Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    .line 660
    add-int/lit8 v1, v1, 0x1

    if-lt v1, p2, :cond_2

    goto :goto_0
.end method


# virtual methods
.method public fetchPartitionResult(I)Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    .locals 38
    .param p1, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    iget-object v5, v5, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v25

    .line 106
    .local v25, "rootNode":I
    const/4 v4, -0x1

    move/from16 v0, v25

    if-ne v0, v4, :cond_1

    .line 107
    const/16 v27, 0x0

    .line 344
    :cond_0
    :goto_0
    return-object v27

    .line 110
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    iget v4, v4, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-virtual {v5}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getSize()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 113
    .local v14, "K":I
    new-instance v13, Lorg/apache/lucene/facet/collections/IntToObjectMap;

    invoke-direct {v13}, Lorg/apache/lucene/facet/collections/IntToObjectMap;-><init>()V

    .line 115
    .local v13, "AACOsOfOnePartition":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    iget v8, v4, Lorg/apache/lucene/facet/search/FacetArrays;->arrayLength:I

    .line 118
    .local v8, "partitionSize":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    invoke-virtual {v4}, Lorg/apache/lucene/facet/search/FacetRequest;->getDepth()I

    move-result v12

    .line 120
    .local v12, "depth":I
    if-nez v12, :cond_2

    .line 122
    new-instance v27, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;

    .line 123
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    .line 122
    move-object/from16 v0, v27

    invoke-direct {v0, v4, v13}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/collections/IntToObjectMap;)V

    .line 124
    .local v27, "tempFRWH":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, p1

    invoke-virtual {v0, v1, v4, v2}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->isSelfPartition(ILorg/apache/lucene/facet/search/FacetArrays;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 125
    const/4 v4, 0x1

    move-object/from16 v0, v27

    iput-boolean v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->isRootNodeIncluded:Z

    .line 126
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    rem-int v9, v25, v8

    invoke-virtual {v4, v5, v9}, Lorg/apache/lucene/facet/search/FacetRequest;->getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D

    move-result-wide v4

    move-object/from16 v0, v27

    iput-wide v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->rootNodeValue:D

    goto :goto_0

    .line 131
    .end local v27    # "tempFRWH":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;
    :cond_2
    const/16 v4, 0x7ffc

    if-le v12, v4, :cond_3

    .line 132
    const/16 v12, 0x7ffc

    .line 135
    :cond_3
    add-int v10, p1, v8

    .line 136
    .local v10, "endOffset":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-virtual {v4}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getParallelTaxonomyArrays()Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;

    move-result-object v18

    .line 137
    .local v18, "childrenArray":Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;->children()[I

    move-result-object v6

    .line 138
    .local v6, "children":[I
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;->siblings()[I

    move-result-object v7

    .line 139
    .local v7, "siblings":[I
    const/16 v31, 0x0

    .line 146
    .local v31, "totalNumOfDescendantsConsidered":I
    new-instance v23, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategoryHeap;

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->getSuitableACComparator()Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-direct {v0, v14, v4}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategoryHeap;-><init>(ILorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;)V

    .line 149
    .local v23, "pq":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;>;"
    add-int/lit8 v4, v14, 0x2

    new-array v0, v4, [Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;

    move-object/from16 v24, v0

    .line 150
    .local v24, "reusables":[Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_1
    move-object/from16 v0, v24

    array-length v4, v0

    move/from16 v0, v20

    if-lt v0, v4, :cond_5

    .line 177
    add-int/lit8 v4, v12, 0x2

    new-array v0, v4, [I

    move-object/from16 v21, v0

    .line 178
    .local v21, "ordinalStack":[I
    const/4 v4, 0x0

    aput v25, v21, v4

    .line 179
    const/4 v11, 0x0

    .line 197
    .local v11, "localDepth":I
    add-int/lit8 v4, v12, 0x2

    new-array v0, v4, [[I

    move-object/from16 v17, v0

    .line 198
    .local v17, "bestSignlingsStack":[[I
    add-int/lit8 v4, v12, 0x2

    new-array v0, v4, [I

    move-object/from16 v26, v0

    .line 199
    .local v26, "siblingExplored":[I
    add-int/lit8 v4, v12, 0x2

    new-array v0, v4, [I

    move-object/from16 v19, v0

    .line 212
    .local v19, "firstToTheLeftOfPartition":[I
    add-int/lit8 v11, v11, 0x1

    aget v4, v6, v25

    aput v4, v21, v11

    .line 213
    const v4, 0x7fffffff

    aput v4, v26, v11

    .line 214
    const/4 v4, 0x0

    const/4 v5, -0x1

    aput v5, v26, v4

    .line 221
    :goto_2
    if-gtz v11, :cond_6

    .line 337
    new-instance v27, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;

    .line 338
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    .line 337
    move-object/from16 v0, v27

    invoke-direct {v0, v4, v13}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/collections/IntToObjectMap;)V

    .line 339
    .restart local v27    # "tempFRWH":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, p1

    invoke-virtual {v0, v1, v4, v2}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->isSelfPartition(ILorg/apache/lucene/facet/search/FacetArrays;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 340
    const/4 v4, 0x1

    move-object/from16 v0, v27

    iput-boolean v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->isRootNodeIncluded:Z

    .line 341
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    rem-int v9, v25, v8

    invoke-virtual {v4, v5, v9}, Lorg/apache/lucene/facet/search/FacetRequest;->getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D

    move-result-wide v4

    move-object/from16 v0, v27

    iput-wide v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->rootNodeValue:D

    .line 343
    :cond_4
    move/from16 v0, v31

    move-object/from16 v1, v27

    iput v0, v1, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->totalNumOfFacetsConsidered:I

    goto/16 :goto_0

    .line 151
    .end local v11    # "localDepth":I
    .end local v17    # "bestSignlingsStack":[[I
    .end local v19    # "firstToTheLeftOfPartition":[I
    .end local v21    # "ordinalStack":[I
    .end local v26    # "siblingExplored":[I
    .end local v27    # "tempFRWH":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;
    :cond_5
    new-instance v4, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;

    const/4 v5, 0x1

    const-wide/16 v36, 0x0

    move-wide/from16 v0, v36

    invoke-direct {v4, v5, v0, v1}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;-><init>(ID)V

    aput-object v4, v24, v20

    .line 150
    add-int/lit8 v20, v20, 0x1

    goto :goto_1

    .line 222
    .restart local v11    # "localDepth":I
    .restart local v17    # "bestSignlingsStack":[[I
    .restart local v19    # "firstToTheLeftOfPartition":[I
    .restart local v21    # "ordinalStack":[I
    .restart local v26    # "siblingExplored":[I
    :cond_6
    aget v28, v21, v11

    .line 223
    .local v28, "tosOrdinal":I
    const/4 v4, -0x1

    move/from16 v0, v28

    if-ne v0, v4, :cond_9

    .line 227
    add-int/lit8 v11, v11, -0x1

    .line 232
    aget v4, v26, v11

    if-gez v4, :cond_7

    .line 233
    aget v4, v21, v11

    aget v4, v7, v4

    aput v4, v21, v11

    goto :goto_2

    .line 238
    :cond_7
    aget v4, v26, v11

    add-int/lit8 v4, v4, -0x1

    aput v4, v26, v11

    .line 239
    aget v4, v26, v11

    const/4 v5, -0x1

    if-ne v4, v5, :cond_8

    .line 242
    aget v4, v19, v11

    aput v4, v21, v11

    goto :goto_2

    .line 246
    :cond_8
    aget-object v4, v17, v11

    aget v5, v26, v11

    aget v4, v4, v5

    aput v4, v21, v11

    goto :goto_2

    .line 255
    :cond_9
    aget v4, v26, v11

    const v5, 0x7fffffff

    if-ne v4, v5, :cond_f

    .line 258
    :goto_3
    move/from16 v0, v28

    if-ge v0, v10, :cond_a

    .line 263
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/util/PriorityQueue;->clear()V

    .line 266
    move-object/from16 v0, v24

    array-length v4, v0

    add-int/lit8 v29, v4, -0x1

    .local v29, "tosReuslables":I
    move/from16 v30, v29

    .line 268
    .end local v29    # "tosReuslables":I
    .local v30, "tosReuslables":I
    :goto_4
    move/from16 v0, v28

    move/from16 v1, p1

    if-ge v0, v1, :cond_b

    .line 296
    aput v28, v19, v11

    .line 297
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v15

    .line 298
    .local v15, "aaci":I
    new-array v0, v15, [I

    move-object/from16 v22, v0

    .line 299
    .local v22, "ords":[I
    new-array v0, v15, [D

    move-object/from16 v32, v0

    .local v32, "vals":[D
    move/from16 v29, v30

    .line 300
    .end local v30    # "tosReuslables":I
    .restart local v29    # "tosReuslables":I
    :goto_5
    if-gtz v15, :cond_d

    .line 309
    move-object/from16 v0, v22

    array-length v4, v0

    if-lez v4, :cond_e

    .line 310
    add-int/lit8 v4, v11, -0x1

    aget v4, v21, v4

    new-instance v5, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;

    move-object/from16 v0, v22

    move-object/from16 v1, v32

    invoke-direct {v5, v0, v1}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;-><init>([I[D)V

    invoke-virtual {v13, v4, v5}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    .line 311
    aput-object v22, v17, v11

    .line 312
    move-object/from16 v0, v22

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aput v4, v26, v11

    .line 313
    move-object/from16 v0, v22

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget v4, v22, v4

    aput v4, v21, v11

    goto/16 :goto_2

    .line 259
    .end local v15    # "aaci":I
    .end local v22    # "ords":[I
    .end local v29    # "tosReuslables":I
    .end local v32    # "vals":[D
    :cond_a
    aget v28, v7, v28

    goto :goto_3

    .line 270
    .restart local v30    # "tosReuslables":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    rem-int v9, v28, v8

    invoke-virtual {v4, v5, v9}, Lorg/apache/lucene/facet/search/FacetRequest;->getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D

    move-result-wide v34

    .line 271
    .local v34, "value":D
    const-wide/16 v4, 0x0

    cmpl-double v4, v34, v4

    if-eqz v4, :cond_11

    .line 272
    add-int/lit8 v31, v31, 0x1

    .line 275
    add-int/lit8 v29, v30, -0x1

    .end local v30    # "tosReuslables":I
    .restart local v29    # "tosReuslables":I
    aget-object v16, v24, v30

    .line 276
    .local v16, "ac":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;
    move/from16 v0, v28

    move-object/from16 v1, v16

    iput v0, v1, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;->ordinal:I

    .line 277
    move-wide/from16 v0, v34

    move-object/from16 v2, v16

    iput-wide v0, v2, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;->value:D

    .line 278
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/PriorityQueue;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "ac":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;
    check-cast v16, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;

    .line 279
    .restart local v16    # "ac":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;
    if-eqz v16, :cond_c

    .line 284
    add-int/lit8 v31, v31, -0x1

    .line 286
    move-object/from16 v0, v16

    iget v5, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;->ordinal:I

    move-object/from16 v4, p0

    move/from16 v9, p1

    .line 287
    invoke-direct/range {v4 .. v12}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->countOnly(I[I[IIIIII)I

    move-result v4

    add-int v31, v31, v4

    .line 288
    add-int/lit8 v29, v29, 0x1

    aput-object v16, v24, v29

    .line 291
    .end local v16    # "ac":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;
    :cond_c
    :goto_6
    aget v28, v7, v28

    move/from16 v30, v29

    .end local v29    # "tosReuslables":I
    .restart local v30    # "tosReuslables":I
    goto/16 :goto_4

    .line 301
    .end local v30    # "tosReuslables":I
    .end local v34    # "value":D
    .restart local v15    # "aaci":I
    .restart local v22    # "ords":[I
    .restart local v29    # "tosReuslables":I
    .restart local v32    # "vals":[D
    :cond_d
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;

    .line 302
    .restart local v16    # "ac":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;
    add-int/lit8 v15, v15, -0x1

    move-object/from16 v0, v16

    iget v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;->ordinal:I

    aput v4, v22, v15

    .line 303
    move-object/from16 v0, v16

    iget-wide v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;->value:D

    aput-wide v4, v32, v15

    .line 304
    add-int/lit8 v29, v29, 0x1

    aput-object v16, v24, v29

    goto/16 :goto_5

    .line 318
    .end local v16    # "ac":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AggregatedCategory;
    :cond_e
    aput v28, v21, v11

    .line 319
    const/4 v4, -0x1

    aput v4, v26, v11

    goto/16 :goto_2

    .line 327
    .end local v15    # "aaci":I
    .end local v22    # "ords":[I
    .end local v29    # "tosReuslables":I
    .end local v32    # "vals":[D
    :cond_f
    if-lt v11, v12, :cond_10

    .line 329
    add-int/lit8 v11, v11, 0x1

    const/4 v4, -0x1

    aput v4, v21, v11

    goto/16 :goto_2

    .line 332
    :cond_10
    add-int/lit8 v11, v11, 0x1

    aget v4, v6, v28

    aput v4, v21, v11

    .line 333
    const v4, 0x7fffffff

    aput v4, v26, v11

    goto/16 :goto_2

    .restart local v30    # "tosReuslables":I
    .restart local v34    # "value":D
    :cond_11
    move/from16 v29, v30

    .end local v30    # "tosReuslables":I
    .restart local v29    # "tosReuslables":I
    goto :goto_6
.end method

.method public labelResult(Lorg/apache/lucene/facet/search/FacetResult;)V
    .locals 2
    .param p1, "facetResult"    # Lorg/apache/lucene/facet/search/FacetResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 643
    if-nez p1, :cond_0

    .line 648
    :goto_0
    return-void

    .line 646
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetResult;->getFacetResultNode()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v0

    .line 647
    .local v0, "rootNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    iget-object v1, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    invoke-virtual {v1}, Lorg/apache/lucene/facet/search/FacetRequest;->getNumLabel()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->recursivelyLabel(Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    goto :goto_0
.end method

.method public varargs mergeResults([Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;)Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    .locals 26
    .param p1, "tmpResults"    # [Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;

    .prologue
    .line 407
    move-object/from16 v0, p1

    array-length v3, v0

    if-nez v3, :cond_1

    .line 408
    const/16 v23, 0x0

    .line 495
    :cond_0
    :goto_0
    return-object v23

    .line 411
    :cond_1
    const/4 v10, 0x0

    .line 413
    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p1

    array-length v3, v0

    if-ge v10, v3, :cond_2

    aget-object v3, p1, v10

    if-eqz v3, :cond_3

    .line 414
    :cond_2
    move-object/from16 v0, p1

    array-length v3, v0

    if-ne v10, v3, :cond_4

    .line 416
    const/16 v23, 0x0

    goto :goto_0

    .line 413
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 420
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    iget v9, v3, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    .line 421
    .local v9, "K":I
    add-int/lit8 v11, v10, 0x1

    .end local v10    # "i":I
    .local v11, "i":I
    aget-object v23, p1, v10

    check-cast v23, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;

    .local v23, "tmpToReturn":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;
    move v10, v11

    .line 424
    .end local v11    # "i":I
    .restart local v10    # "i":I
    :goto_2
    move-object/from16 v0, p1

    array-length v3, v0

    if-ge v10, v3, :cond_0

    .line 425
    aget-object v18, p1, v10

    check-cast v18, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;

    .line 426
    .local v18, "tfr":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;
    move-object/from16 v0, v23

    iget v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->totalNumOfFacetsConsidered:I

    move-object/from16 v0, v18

    iget v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->totalNumOfFacetsConsidered:I

    add-int/2addr v3, v4

    move-object/from16 v0, v23

    iput v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->totalNumOfFacetsConsidered:I

    .line 427
    move-object/from16 v0, v18

    iget-boolean v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->isRootNodeIncluded:Z

    if-eqz v3, :cond_5

    .line 428
    const/4 v3, 0x1

    move-object/from16 v0, v23

    iput-boolean v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->isRootNodeIncluded:Z

    .line 429
    move-object/from16 v0, v18

    iget-wide v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->rootNodeValue:D

    move-object/from16 v0, v23

    iput-wide v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->rootNodeValue:D

    .line 432
    :cond_5
    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->mapToAACOs:Lorg/apache/lucene/facet/collections/IntToObjectMap;

    move-object/from16 v25, v0

    .line 433
    .local v25, "tmpToReturnMapToACCOs":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;>;"
    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->mapToAACOs:Lorg/apache/lucene/facet/collections/IntToObjectMap;

    move-object/from16 v21, v0

    .line 434
    .local v21, "tfrMapToACCOs":Lorg/apache/lucene/facet/collections/IntToObjectMap;, "Lorg/apache/lucene/facet/collections/IntToObjectMap<Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;>;"
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v20

    .line 436
    .local v20, "tfrIntIterator":Lorg/apache/lucene/facet/collections/IntIterator;
    :goto_3
    invoke-interface/range {v20 .. v20}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_6

    .line 424
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 437
    :cond_6
    invoke-interface/range {v20 .. v20}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v22

    .line 438
    .local v22, "tfrkey":I
    const/16 v24, 0x0

    .line 439
    .local v24, "tmpToReturnAACO":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;
    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->get(I)Ljava/lang/Object;

    move-result-object v24

    .end local v24    # "tmpToReturnAACO":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;
    check-cast v24, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;

    .restart local v24    # "tmpToReturnAACO":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;
    if-nez v24, :cond_7

    .line 442
    invoke-virtual/range {v21 .. v22}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1, v3}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 445
    :cond_7
    invoke-virtual/range {v21 .. v22}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;

    .line 446
    .local v19, "tfrAACO":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;
    move-object/from16 v0, v19

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    array-length v3, v3

    move-object/from16 v0, v24

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    array-length v4, v4

    add-int v15, v3, v4

    .line 447
    .local v15, "resLength":I
    if-ge v9, v15, :cond_8

    .line 448
    move v15, v9

    .line 450
    :cond_8
    new-array v0, v15, [I

    move-object/from16 v16, v0

    .line 451
    .local v16, "resOrds":[I
    new-array v0, v15, [D

    move-object/from16 v17, v0

    .line 452
    .local v17, "resVals":[D
    const/4 v14, 0x0

    .line 453
    .local v14, "indexIntoTmpToReturn":I
    const/4 v13, 0x0

    .line 454
    .local v13, "indexIntoTFR":I
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->getSuitableACComparator()Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;

    move-result-object v2

    .line 455
    .local v2, "merger":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;
    const/4 v12, 0x0

    .local v12, "indexIntoRes":I
    :goto_4
    if-lt v12, v15, :cond_9

    .line 489
    new-instance v3, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v3, v0, v1}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;-><init>([I[D)V

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1, v3}, Lorg/apache/lucene/facet/collections/IntToObjectMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 456
    :cond_9
    move-object/from16 v0, v24

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    array-length v3, v3

    if-lt v14, v3, :cond_a

    .line 459
    move-object/from16 v0, v19

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    aget v3, v3, v13

    aput v3, v16, v12

    .line 460
    move-object/from16 v0, v19

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->values:[D

    aget-wide v4, v3, v13

    aput-wide v4, v17, v12

    .line 461
    add-int/lit8 v13, v13, 0x1

    .line 455
    :goto_5
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 464
    :cond_a
    move-object/from16 v0, v19

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    array-length v3, v3

    if-lt v13, v3, :cond_b

    .line 466
    move-object/from16 v0, v24

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    aget v3, v3, v14

    aput v3, v16, v12

    .line 467
    move-object/from16 v0, v24

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->values:[D

    aget-wide v4, v3, v14

    aput-wide v4, v17, v12

    .line 468
    add-int/lit8 v14, v14, 0x1

    .line 469
    goto :goto_5

    .line 472
    :cond_b
    move-object/from16 v0, v24

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    aget v3, v3, v14

    .line 473
    move-object/from16 v0, v24

    iget-object v4, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->values:[D

    aget-wide v4, v4, v14

    .line 474
    move-object/from16 v0, v19

    iget-object v6, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    aget v6, v6, v13

    .line 475
    move-object/from16 v0, v19

    iget-object v7, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->values:[D

    aget-wide v7, v7, v13

    .line 472
    invoke-virtual/range {v2 .. v8}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;->leftGoesNow(IDID)Z

    move-result v3

    .line 475
    if-eqz v3, :cond_c

    .line 476
    move-object/from16 v0, v24

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    aget v3, v3, v14

    aput v3, v16, v12

    .line 477
    move-object/from16 v0, v24

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->values:[D

    aget-wide v4, v3, v14

    aput-wide v4, v17, v12

    .line 478
    add-int/lit8 v14, v14, 0x1

    .line 479
    goto :goto_5

    .line 480
    :cond_c
    move-object/from16 v0, v19

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->ordinals:[I

    aget v3, v3, v13

    aput v3, v16, v12

    .line 481
    move-object/from16 v0, v19

    iget-object v3, v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$AACO;->values:[D

    aget-wide v4, v3, v13

    aput-wide v4, v17, v12

    .line 482
    add-int/lit8 v13, v13, 0x1

    goto :goto_5
.end method

.method public rearrangeFacetResult(Lorg/apache/lucene/facet/search/FacetResult;)Lorg/apache/lucene/facet/search/FacetResult;
    .locals 4
    .param p1, "facetResult"    # Lorg/apache/lucene/facet/search/FacetResult;

    .prologue
    .line 674
    new-instance v0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ResultNodeHeap;

    iget-object v2, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    iget v2, v2, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    invoke-direct {p0}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->getSuitableACComparator()Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$ResultNodeHeap;-><init>(ILorg/apache/lucene/facet/search/TopKInEachNodeHandler$ACComparator;)V

    .line 675
    .local v0, "nodesHeap":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetResult;->getFacetResultNode()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v1

    .line 676
    .local v1, "topFrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->rearrangeChilrenOfNode(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/util/PriorityQueue;)V

    .line 677
    return-object p1
.end method

.method public renderFacetResult(Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;)Lorg/apache/lucene/facet/search/FacetResult;
    .locals 8
    .param p1, "tmpResult"    # Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 699
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;

    .line 700
    .local v2, "tmp":Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;
    iget-object v3, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v6, p0, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    iget-object v6, v6, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v3, v6}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v0

    .line 701
    .local v0, "ordinal":I
    if-eqz v2, :cond_0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    .line 702
    :cond_0
    const/4 v3, 0x0

    .line 709
    :goto_0
    return-object v3

    .line 704
    :cond_1
    const-wide/high16 v4, 0x7ff8000000000000L    # NaN

    .line 705
    .local v4, "value":D
    iget-boolean v3, v2, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->isRootNodeIncluded:Z

    if-eqz v3, :cond_2

    .line 706
    iget-wide v4, v2, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->rootNodeValue:D

    .line 708
    :cond_2
    iget-object v3, v2, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->mapToAACOs:Lorg/apache/lucene/facet/collections/IntToObjectMap;

    invoke-direct {p0, v0, v4, v5, v3}, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler;->generateNode(IDLorg/apache/lucene/facet/collections/IntToObjectMap;)Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v1

    .line 709
    .local v1, "root":Lorg/apache/lucene/facet/search/FacetResultNode;
    new-instance v3, Lorg/apache/lucene/facet/search/FacetResult;

    iget-object v6, v2, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    iget v7, v2, Lorg/apache/lucene/facet/search/TopKInEachNodeHandler$IntermediateFacetResultWithHash;->totalNumOfFacetsConsidered:I

    invoke-direct {v3, v6, v1, v7}, Lorg/apache/lucene/facet/search/FacetResult;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    goto :goto_0
.end method
