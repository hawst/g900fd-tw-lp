.class public Lorg/apache/lucene/facet/search/FacetResultNode;
.super Ljava/lang/Object;
.source "FacetResultNode.java"


# static fields
.field public static final EMPTY_SUB_RESULTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

.field public ordinal:I

.field public subResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;"
        }
    .end annotation
.end field

.field public value:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/facet/search/FacetResultNode;->EMPTY_SUB_RESULTS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(ID)V
    .locals 2
    .param p1, "ordinal"    # I
    .param p2, "value"    # D

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    sget-object v0, Lorg/apache/lucene/facet/search/FacetResultNode;->EMPTY_SUB_RESULTS:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    .line 71
    iput p1, p0, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    .line 72
    iput-wide p2, p0, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    .line 73
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    const-string v0, ""

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/search/FacetResultNode;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 83
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    if-nez v2, :cond_0

    .line 84
    const-string v2, "not labeled (ordinal="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    :goto_0
    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 92
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 86
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v2}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 89
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 90
    .local v1, "sub":Lorg/apache/lucene/facet/search/FacetResultNode;
    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/apache/lucene/facet/search/FacetResultNode;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
