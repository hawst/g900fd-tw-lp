.class public Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;
.super Lorg/apache/lucene/facet/search/FacetsAccumulator;
.source "SortedSetDocValuesAccumulator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$TopCountPQ;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final dv:Lorg/apache/lucene/index/SortedSetDocValues;

.field final field:Ljava/lang/String;

.field final state:Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;)V
    .locals 8
    .param p1, "fsp"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p2, "state"    # Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 60
    new-instance v3, Lorg/apache/lucene/facet/search/FacetArrays;

    invoke-virtual {p2}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->getDocValues()Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/facet/search/FacetArrays;-><init>(I)V

    invoke-direct {p0, p1, v7, v7, v3}, Lorg/apache/lucene/facet/search/FacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 61
    iput-object p2, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->state:Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;

    .line 62
    invoke-virtual {p2}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->getField()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->field:Ljava/lang/String;

    .line 63
    invoke-virtual {p2}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->getDocValues()Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->dv:Lorg/apache/lucene/index/SortedSetDocValues;

    .line 66
    iget-object v3, p1, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 83
    return-void

    .line 66
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 67
    .local v2, "request":Lorg/apache/lucene/facet/search/FacetRequest;
    instance-of v4, v2, Lorg/apache/lucene/facet/search/CountFacetRequest;

    if-nez v4, :cond_2

    .line 68
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "this collector only supports CountFacetRequest; got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 70
    :cond_2
    iget-object v4, v2, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget v4, v4, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-eq v4, v6, :cond_3

    .line 71
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "this collector only supports depth 1 CategoryPath; got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 73
    :cond_3
    invoke-virtual {v2}, Lorg/apache/lucene/facet/search/FacetRequest;->getDepth()I

    move-result v4

    if-eq v4, v6, :cond_4

    .line 74
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "this collector only supports depth=1; got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/apache/lucene/facet/search/FacetRequest;->getDepth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 76
    :cond_4
    iget-object v4, v2, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget-object v4, v4, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v0, v4, v5

    .line 78
    .local v0, "dim":Ljava/lang/String;
    invoke-virtual {p2, v0}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->getOrdRange(Ljava/lang/String;)Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;

    move-result-object v1

    .line 79
    .local v1, "ordRange":Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;
    if-nez v1, :cond_0

    .line 80
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dim \""

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\" does not exist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method public accumulate(Ljava/util/List;)Ljava/util/List;
    .locals 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResult;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 195
    .local p1, "matchingDocs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;>;"
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->getAggregator()Lorg/apache/lucene/facet/search/FacetsAggregator;

    move-result-object v4

    .line 196
    .local v4, "aggregator":Lorg/apache/lucene/facet/search/FacetsAggregator;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->getCategoryLists()Ljava/util/Set;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :cond_0
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-nez v26, :cond_1

    .line 203
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 205
    .local v21, "results":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lorg/apache/lucene/facet/search/FacetArrays;->getIntArray()[I

    move-result-object v9

    .line 207
    .local v9, "counts":[I
    new-instance v24, Lorg/apache/lucene/util/BytesRef;

    invoke-direct/range {v24 .. v24}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 209
    .local v24, "scratch":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :goto_0
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-nez v25, :cond_2

    .line 304
    return-object v21

    .line 196
    .end local v9    # "counts":[I
    .end local v21    # "results":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    .end local v24    # "scratch":Lorg/apache/lucene/util/BytesRef;
    :cond_1
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 197
    .local v8, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :goto_1
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_0

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    .line 198
    .local v13, "md":Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v4, v13, v8, v0}, Lorg/apache/lucene/facet/search/FacetsAggregator;->aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V

    goto :goto_1

    .line 209
    .end local v8    # "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    .end local v13    # "md":Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    .restart local v9    # "counts":[I
    .restart local v21    # "results":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    .restart local v24    # "scratch":Lorg/apache/lucene/util/BytesRef;
    :cond_2
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 210
    .local v20, "request":Lorg/apache/lucene/facet/search/FacetRequest;
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    move-object/from16 v25, v0

    const/16 v27, 0x0

    aget-object v10, v25, v27

    .line 211
    .local v10, "dim":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->state:Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->getOrdRange(Ljava/lang/String;)Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;

    move-result-object v18

    .line 213
    .local v18, "ordRange":Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;
    sget-boolean v25, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->$assertionsDisabled:Z

    if-nez v25, :cond_3

    if-nez v18, :cond_3

    new-instance v25, Ljava/lang/AssertionError;

    invoke-direct/range {v25 .. v25}, Ljava/lang/AssertionError;-><init>()V

    throw v25

    .line 215
    :cond_3
    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    move/from16 v25, v0

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;->end:I

    move/from16 v27, v0

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;->start:I

    move/from16 v28, v0

    sub-int v27, v27, v28

    add-int/lit8 v27, v27, 0x1

    move/from16 v0, v25

    move/from16 v1, v27

    if-lt v0, v1, :cond_7

    .line 217
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 218
    .local v15, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    const/4 v11, 0x0

    .line 219
    .local v11, "dimCount":I
    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;->start:I

    move/from16 v17, v0

    .local v17, "ord":I
    :goto_2
    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;->end:I

    move/from16 v25, v0

    move/from16 v0, v17

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    .line 230
    new-instance v25, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$2;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$2;-><init>(Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;)V

    move-object/from16 v0, v25

    invoke-static {v15, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    move-object/from16 v25, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Lorg/apache/lucene/facet/params/CategoryListParams;->getOrdinalPolicy(Ljava/lang/String;)Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    move-result-object v16

    .line 244
    .local v16, "op":Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    sget-object v25, Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;->ALL_BUT_DIMENSION:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_4

    .line 245
    const/4 v11, 0x0

    .line 248
    :cond_4
    new-instance v23, Lorg/apache/lucene/facet/search/FacetResultNode;

    const/16 v25, -0x1

    int-to-double v0, v11

    move-wide/from16 v28, v0

    move-object/from16 v0, v23

    move/from16 v1, v25

    move-wide/from16 v2, v28

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    .line 249
    .local v23, "rootNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    new-instance v25, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v10, v27, v28

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    iput-object v0, v1, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 250
    move-object/from16 v0, v23

    iput-object v15, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    .line 251
    new-instance v25, Lorg/apache/lucene/facet/search/FacetResult;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v27

    move-object/from16 v0, v25

    move-object/from16 v1, v20

    move-object/from16 v2, v23

    move/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/facet/search/FacetResult;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 221
    .end local v16    # "op":Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    .end local v23    # "rootNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    :cond_5
    aget v25, v9, v17

    if-eqz v25, :cond_6

    .line 222
    aget v25, v9, v17

    add-int v11, v11, v25

    .line 223
    new-instance v14, Lorg/apache/lucene/facet/search/FacetResultNode;

    aget v25, v9, v17

    move/from16 v0, v25

    int-to-double v0, v0

    move-wide/from16 v28, v0

    move/from16 v0, v17

    move-wide/from16 v1, v28

    invoke-direct {v14, v0, v1, v2}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    .line 224
    .local v14, "node":Lorg/apache/lucene/facet/search/FacetResultNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->dv:Lorg/apache/lucene/index/SortedSetDocValues;

    move-object/from16 v25, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v28, v0

    move-object/from16 v0, v25

    move-wide/from16 v1, v28

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupOrd(JLorg/apache/lucene/util/BytesRef;)V

    .line 225
    new-instance v25, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->state:Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->separatorRegex:Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x2

    invoke-virtual/range {v27 .. v29}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, v25

    iput-object v0, v14, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 226
    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    .end local v14    # "node":Lorg/apache/lucene/facet/search/FacetResultNode;
    :cond_6
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_2

    .line 255
    .end local v11    # "dimCount":I
    .end local v15    # "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    .end local v17    # "ord":I
    :cond_7
    new-instance v19, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$TopCountPQ;

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    move/from16 v25, v0

    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$TopCountPQ;-><init>(I)V

    .line 257
    .local v19, "q":Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$TopCountPQ;
    const/4 v5, 0x0

    .line 260
    .local v5, "bottomCount":I
    const/4 v11, 0x0

    .line 261
    .restart local v11    # "dimCount":I
    const/4 v6, 0x0

    .line 262
    .local v6, "childCount":I
    const/16 v22, 0x0

    .line 263
    .local v22, "reuse":Lorg/apache/lucene/facet/search/FacetResultNode;
    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;->start:I

    move/from16 v17, v0

    .restart local v17    # "ord":I
    :goto_3
    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState$OrdRange;->end:I

    move/from16 v25, v0

    move/from16 v0, v17

    move/from16 v1, v25

    if-le v0, v1, :cond_9

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    move-object/from16 v25, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Lorg/apache/lucene/facet/params/CategoryListParams;->getOrdinalPolicy(Ljava/lang/String;)Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    move-result-object v16

    .line 286
    .restart local v16    # "op":Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    sget-object v25, Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;->ALL_BUT_DIMENSION:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_8

    .line 287
    const/4 v11, 0x0

    .line 290
    :cond_8
    new-instance v23, Lorg/apache/lucene/facet/search/FacetResultNode;

    const/16 v25, -0x1

    int-to-double v0, v11

    move-wide/from16 v28, v0

    move-object/from16 v0, v23

    move/from16 v1, v25

    move-wide/from16 v2, v28

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    .line 291
    .restart local v23    # "rootNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    new-instance v25, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v10, v27, v28

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    iput-object v0, v1, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 293
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$TopCountPQ;->size()I

    move-result v25

    move/from16 v0, v25

    new-array v7, v0, [Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 294
    .local v7, "childNodes":[Lorg/apache/lucene/facet/search/FacetResultNode;
    array-length v0, v7

    move/from16 v25, v0

    add-int/lit8 v12, v25, -0x1

    .local v12, "i":I
    :goto_4
    if-gez v12, :cond_c

    .line 299
    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    iput-object v0, v1, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    .line 301
    new-instance v25, Lorg/apache/lucene/facet/search/FacetResult;

    move-object/from16 v0, v25

    move-object/from16 v1, v20

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2, v6}, Lorg/apache/lucene/facet/search/FacetResult;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 265
    .end local v7    # "childNodes":[Lorg/apache/lucene/facet/search/FacetResultNode;
    .end local v12    # "i":I
    .end local v16    # "op":Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    .end local v23    # "rootNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    :cond_9
    aget v25, v9, v17

    if-lez v25, :cond_a

    .line 266
    add-int/lit8 v6, v6, 0x1

    .line 267
    aget v25, v9, v17

    move/from16 v0, v25

    if-le v0, v5, :cond_a

    .line 268
    aget v25, v9, v17

    add-int v11, v11, v25

    .line 270
    if-nez v22, :cond_b

    .line 271
    new-instance v22, Lorg/apache/lucene/facet/search/FacetResultNode;

    .end local v22    # "reuse":Lorg/apache/lucene/facet/search/FacetResultNode;
    aget v25, v9, v17

    move/from16 v0, v25

    int-to-double v0, v0

    move-wide/from16 v28, v0

    move-object/from16 v0, v22

    move/from16 v1, v17

    move-wide/from16 v2, v28

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    .line 276
    .restart local v22    # "reuse":Lorg/apache/lucene/facet/search/FacetResultNode;
    :goto_5
    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$TopCountPQ;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    .end local v22    # "reuse":Lorg/apache/lucene/facet/search/FacetResultNode;
    check-cast v22, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 277
    .restart local v22    # "reuse":Lorg/apache/lucene/facet/search/FacetResultNode;
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$TopCountPQ;->size()I

    move-result v25

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    move/from16 v27, v0

    move/from16 v0, v25

    move/from16 v1, v27

    if-ne v0, v1, :cond_a

    .line 278
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$TopCountPQ;->top()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lorg/apache/lucene/facet/search/FacetResultNode;

    move-object/from16 v0, v25

    iget-wide v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    move-wide/from16 v28, v0

    move-wide/from16 v0, v28

    double-to-int v5, v0

    .line 263
    :cond_a
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_3

    .line 273
    :cond_b
    move/from16 v0, v17

    move-object/from16 v1, v22

    iput v0, v1, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    .line 274
    aget v25, v9, v17

    move/from16 v0, v25

    int-to-double v0, v0

    move-wide/from16 v28, v0

    move-wide/from16 v0, v28

    move-object/from16 v2, v22

    iput-wide v0, v2, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    goto :goto_5

    .line 295
    .restart local v7    # "childNodes":[Lorg/apache/lucene/facet/search/FacetResultNode;
    .restart local v12    # "i":I
    .restart local v16    # "op":Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    .restart local v23    # "rootNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    :cond_c
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$TopCountPQ;->pop()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lorg/apache/lucene/facet/search/FacetResultNode;

    aput-object v25, v7, v12

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->dv:Lorg/apache/lucene/index/SortedSetDocValues;

    move-object/from16 v25, v0

    aget-object v27, v7, v12

    move-object/from16 v0, v27

    iget v0, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v28, v0

    move-object/from16 v0, v25

    move-wide/from16 v1, v28

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupOrd(JLorg/apache/lucene/util/BytesRef;)V

    .line 297
    aget-object v25, v7, v12

    new-instance v27, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;->state:Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesReaderState;->separatorRegex:Ljava/lang/String;

    move-object/from16 v29, v0

    const/16 v30, 0x2

    invoke-virtual/range {v28 .. v30}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    iput-object v0, v1, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 294
    add-int/lit8 v12, v12, -0x1

    goto/16 :goto_4
.end method

.method public getAggregator()Lorg/apache/lucene/facet/search/FacetsAggregator;
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator$1;-><init>(Lorg/apache/lucene/facet/sortedset/SortedSetDocValuesAccumulator;)V

    return-object v0
.end method
