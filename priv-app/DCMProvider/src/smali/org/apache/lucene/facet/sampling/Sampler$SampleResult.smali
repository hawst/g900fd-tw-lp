.class public final Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;
.super Ljava/lang/Object;
.source "Sampler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/sampling/Sampler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SampleResult"
.end annotation


# instance fields
.field public final actualSampleRatio:D

.field public final docids:Lorg/apache/lucene/facet/search/ScoredDocIDs;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/facet/search/ScoredDocIDs;D)V
    .locals 0
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .param p2, "actualSampleRatio"    # D

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-object p1, p0, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;->docids:Lorg/apache/lucene/facet/search/ScoredDocIDs;

    .line 131
    iput-wide p2, p0, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;->actualSampleRatio:D

    .line 132
    return-void
.end method
