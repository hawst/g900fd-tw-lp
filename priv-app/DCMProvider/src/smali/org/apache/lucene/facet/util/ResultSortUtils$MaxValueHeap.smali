.class Lorg/apache/lucene/facet/util/ResultSortUtils$MaxValueHeap;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "ResultSortUtils.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/Heap;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/util/ResultSortUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MaxValueHeap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/facet/search/FacetResultNode;",
        ">;",
        "Lorg/apache/lucene/facet/search/Heap",
        "<",
        "Lorg/apache/lucene/facet/search/FacetResultNode;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 81
    return-void
.end method


# virtual methods
.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/search/FacetResultNode;

    check-cast p2, Lorg/apache/lucene/facet/search/FacetResultNode;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/facet/util/ResultSortUtils$MaxValueHeap;->lessThan(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/facet/search/FacetResultNode;)Z

    move-result v0

    return v0
.end method

.method protected lessThan(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/facet/search/FacetResultNode;)Z
    .locals 9
    .param p1, "arg0"    # Lorg/apache/lucene/facet/search/FacetResultNode;
    .param p2, "arg1"    # Lorg/apache/lucene/facet/search/FacetResultNode;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 85
    iget-wide v0, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    .line 86
    .local v0, "value0":D
    iget-wide v2, p2, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    .line 88
    .local v2, "value1":D
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v4

    .line 89
    .local v4, "valueCompare":I
    if-nez v4, :cond_2

    .line 90
    iget v7, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    iget v8, p2, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    if-le v7, v8, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v5

    :cond_1
    move v5, v6

    .line 90
    goto :goto_0

    .line 93
    :cond_2
    if-gtz v4, :cond_0

    move v5, v6

    goto :goto_0
.end method
