.class public Lorg/apache/lucene/facet/params/FacetIndexingParams;
.super Ljava/lang/Object;
.source "FacetIndexingParams.java"


# static fields
.field public static final DEFAULT:Lorg/apache/lucene/facet/params/FacetIndexingParams;

.field protected static final DEFAULT_CATEGORY_LIST_PARAMS:Lorg/apache/lucene/facet/params/CategoryListParams;

.field public static final DEFAULT_FACET_DELIM_CHAR:C = '\u001f'


# instance fields
.field protected final clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

.field private final partitionSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lorg/apache/lucene/facet/params/CategoryListParams;

    invoke-direct {v0}, Lorg/apache/lucene/facet/params/CategoryListParams;-><init>()V

    sput-object v0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->DEFAULT_CATEGORY_LIST_PARAMS:Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 52
    new-instance v0, Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {v0}, Lorg/apache/lucene/facet/params/FacetIndexingParams;-><init>()V

    sput-object v0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->DEFAULT:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 61
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->DEFAULT_CATEGORY_LIST_PARAMS:Lorg/apache/lucene/facet/params/CategoryListParams;

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/params/FacetIndexingParams;-><init>(Lorg/apache/lucene/facet/params/CategoryListParams;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/params/CategoryListParams;)V
    .locals 1
    .param p1, "categoryListParams"    # Lorg/apache/lucene/facet/params/CategoryListParams;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->partitionSize:I

    .line 78
    iput-object p1, p0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 79
    return-void
.end method


# virtual methods
.method public drillDownTermText(Lorg/apache/lucene/facet/taxonomy/CategoryPath;[C)I
    .locals 2
    .param p1, "path"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "buffer"    # [C

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-virtual {p0}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getFacetDelimChar()C

    move-result v1

    invoke-virtual {p1, p2, v0, v1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->copyFullPath([CIC)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 145
    if-ne p0, p1, :cond_1

    .line 146
    const/4 v3, 0x1

    .line 169
    :cond_0
    :goto_0
    return v3

    .line 148
    :cond_1
    if-eqz p1, :cond_0

    .line 151
    instance-of v4, p1, Lorg/apache/lucene/facet/params/FacetIndexingParams;

    if-eqz v4, :cond_0

    move-object v1, p1

    .line 154
    check-cast v1, Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 155
    .local v1, "other":Lorg/apache/lucene/facet/params/FacetIndexingParams;
    iget-object v4, p0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    if-nez v4, :cond_3

    .line 156
    iget-object v4, v1, Lorg/apache/lucene/facet/params/FacetIndexingParams;->clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    if-nez v4, :cond_0

    .line 162
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 166
    invoke-virtual {p0}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getAllCategoryListParams()Ljava/util/List;

    move-result-object v0

    .line 167
    .local v0, "cLs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/params/CategoryListParams;>;"
    invoke-virtual {v1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getAllCategoryListParams()Ljava/util/List;

    move-result-object v2

    .line 169
    .local v2, "otherCLs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/params/CategoryListParams;>;"
    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0

    .line 159
    .end local v0    # "cLs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/params/CategoryListParams;>;"
    .end local v2    # "otherCLs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/params/CategoryListParams;>;"
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    iget-object v5, v1, Lorg/apache/lucene/facet/params/FacetIndexingParams;->clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/facet/params/CategoryListParams;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0
.end method

.method public getAllCategoryListParams()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;
    .locals 1
    .param p1, "category"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    return-object v0
.end method

.method public getFacetDelimChar()C
    .locals 1

    .prologue
    .line 177
    const/16 v0, 0x1f

    return v0
.end method

.method public getPartitionSize()I
    .locals 1

    .prologue
    .line 118
    const v0, 0x7fffffff

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 131
    const/16 v1, 0x1f

    .line 132
    .local v1, "prime":I
    const/4 v2, 0x1

    .line 133
    .local v2, "result":I
    iget-object v3, p0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v2, v3, 0x1f

    .line 134
    mul-int/lit8 v3, v2, 0x1f

    const v4, 0x7fffffff

    add-int v2, v3, v4

    .line 136
    invoke-virtual {p0}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getAllCategoryListParams()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 140
    return v2

    .line 133
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->clParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    invoke-virtual {v3}, Lorg/apache/lucene/facet/params/CategoryListParams;->hashCode()I

    move-result v3

    goto :goto_0

    .line 136
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 137
    .local v0, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    invoke-virtual {v0}, Lorg/apache/lucene/facet/params/CategoryListParams;->hashCode()I

    move-result v4

    xor-int/2addr v2, v4

    goto :goto_1
.end method
