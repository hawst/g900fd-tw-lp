.class public Lorg/apache/lucene/facet/sampling/RepeatableSampler;
.super Lorg/apache/lucene/facet/sampling/Sampler;
.source "RepeatableSampler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;,
        Lorg/apache/lucene/facet/sampling/RepeatableSampler$IntPriorityQueue;,
        Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;,
        Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;
    }
.end annotation


# static fields
.field private static final N_PRIMES:I = 0xfa0

.field private static final PHI_32:J = 0x9e3779b9L

.field private static final PHI_32I:J = 0x144cbc89L

.field private static final logger:Ljava/util/logging/Logger;

.field private static primes:[I

.field private static returnTimings:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xfa0

    const/4 v4, 0x0

    .line 37
    const-class v1, Lorg/apache/lucene/facet/sampling/RepeatableSampler;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v1

    sput-object v1, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->logger:Ljava/util/logging/Logger;

    .line 243
    new-array v1, v5, [I

    sput-object v1, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->primes:[I

    .line 245
    sget-object v1, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->primes:[I

    const/4 v2, 0x3

    aput v2, v1, v4

    .line 246
    const/4 v0, 0x1

    .local v0, "count":I
    :goto_0
    if-lt v0, v5, :cond_0

    .line 404
    sput-boolean v4, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->returnTimings:Z

    return-void

    .line 247
    :cond_0
    sget-object v1, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->primes:[I

    sget-object v2, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->primes:[I

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    invoke-static {v2}, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->findNextPrimeAfter(I)I

    move-result v2

    aput v2, v1, v0

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/sampling/SamplingParams;)V
    .locals 0
    .param p1, "params"    # Lorg/apache/lucene/facet/sampling/SamplingParams;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/sampling/Sampler;-><init>(Lorg/apache/lucene/facet/sampling/SamplingParams;)V

    .line 41
    return-void
.end method

.method private static findGoodStepSize(II)I
    .locals 4
    .param p0, "collectionSize"    # I
    .param p1, "sampleSize"    # I

    .prologue
    .line 200
    int-to-double v2, p0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 201
    .local v0, "i":I
    if-ge p1, v0, :cond_0

    .line 202
    div-int v0, p0, p1

    .line 205
    :cond_0
    invoke-static {v0}, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->findNextPrimeAfter(I)I

    move-result v0

    .line 206
    rem-int v1, p0, v0

    if-eqz v1, :cond_0

    .line 207
    return v0
.end method

.method private static findNextPrimeAfter(I)I
    .locals 6
    .param p0, "n"    # I

    .prologue
    .line 216
    rem-int/lit8 v3, p0, 0x2

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    add-int/2addr p0, v3

    .line 218
    :goto_1
    int-to-double v4, p0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 219
    .local v2, "sri":I
    const/4 v1, 0x0

    .local v1, "primeIndex":I
    :goto_2
    const/16 v3, 0xfa0

    if-lt v1, v3, :cond_2

    .line 228
    sget-object v3, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->primes:[I

    const/16 v4, 0xf9f

    aget v3, v3, v4

    add-int/lit8 v0, v3, 0x2

    .line 229
    .local v0, "p":I
    :goto_3
    if-le v0, v2, :cond_5

    .line 230
    :cond_0
    return p0

    .line 216
    .end local v0    # "p":I
    .end local v1    # "primeIndex":I
    .end local v2    # "sri":I
    :cond_1
    const/4 v3, 0x2

    goto :goto_0

    .line 220
    .restart local v1    # "primeIndex":I
    .restart local v2    # "sri":I
    :cond_2
    sget-object v3, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->primes:[I

    aget v0, v3, v1

    .line 221
    .restart local v0    # "p":I
    if-gt v0, v2, :cond_0

    .line 224
    rem-int v3, p0, v0

    if-nez v3, :cond_4

    .line 217
    :cond_3
    add-int/lit8 p0, p0, 0x2

    goto :goto_1

    .line 219
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 232
    :cond_5
    rem-int v3, p0, v0

    if-eqz v3, :cond_3

    .line 228
    add-int/lit8 v0, v0, 0x2

    goto :goto_3
.end method

.method private static repeatableSample(Lorg/apache/lucene/facet/search/ScoredDocIDs;II)[I
    .locals 2
    .param p0, "collection"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .param p1, "collectionSize"    # I
    .param p2, "sampleSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    .line 79
    sget-object v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;->HASHING:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    sget-object v1, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;->NO:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    .line 78
    invoke-static {p0, p1, p2, v0, v1}, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->repeatableSample(Lorg/apache/lucene/facet/search/ScoredDocIDs;IILorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;)[I

    move-result-object v0

    return-object v0
.end method

.method private static repeatableSample(Lorg/apache/lucene/facet/search/ScoredDocIDs;IILorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;)[I
    .locals 11
    .param p0, "collection"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .param p1, "collectionSize"    # I
    .param p2, "sampleSize"    # I
    .param p3, "algorithm"    # Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;
    .param p4, "sorted"    # Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 97
    if-nez p0, :cond_0

    .line 98
    new-instance v2, Ljava/io/IOException;

    const-string v3, "docIdSet is null"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 100
    :cond_0
    if-ge p2, v8, :cond_1

    .line 101
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sampleSize < 1 ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 103
    :cond_1
    if-ge p1, p2, :cond_2

    .line 104
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "collectionSize ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") less than sampleSize ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 106
    :cond_2
    new-array v0, p2, [I

    .line 107
    .local v0, "sample":[I
    const/4 v2, 0x4

    new-array v1, v2, [J

    .line 108
    .local v1, "times":[J
    sget-object v2, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;->TRAVERSAL:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    if-ne p3, v2, :cond_5

    .line 109
    invoke-static {p0, p1, v0, v1}, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->sample1(Lorg/apache/lucene/facet/search/ScoredDocIDs;I[I[J)V

    .line 115
    :goto_0
    sget-object v2, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;->YES:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    if-ne p4, v2, :cond_3

    .line 116
    invoke-static {v0}, Ljava/util/Arrays;->sort([I)V

    .line 118
    :cond_3
    sget-boolean v2, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->returnTimings:Z

    if-eqz v2, :cond_4

    .line 119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    aput-wide v2, v1, v10

    .line 120
    sget-object v2, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 121
    sget-object v2, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->logger:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Times: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-wide v4, v1, v8

    const/4 v6, 0x0

    aget-wide v6, v1, v6

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 122
    aget-wide v4, v1, v9

    aget-wide v6, v1, v8

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-wide v4, v1, v10

    aget-wide v6, v1, v9

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 121
    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    .line 125
    :cond_4
    return-object v0

    .line 110
    :cond_5
    sget-object v2, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;->HASHING:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Algorithm;

    if-ne p3, v2, :cond_6

    .line 111
    invoke-static {p0, p1, v0, v1}, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->sample2(Lorg/apache/lucene/facet/search/ScoredDocIDs;I[I[J)V

    goto :goto_0

    .line 113
    :cond_6
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid algorithm selection"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private static sample1(Lorg/apache/lucene/facet/search/ScoredDocIDs;I[I[J)V
    .locals 12
    .param p0, "collection"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .param p1, "collectionSize"    # I
    .param p2, "sample"    # [I
    .param p3, "times"    # [J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    invoke-interface {p0}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;

    move-result-object v2

    .line 151
    .local v2, "it":Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
    sget-boolean v8, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->returnTimings:Z

    if-eqz v8, :cond_0

    .line 152
    const/4 v8, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    aput-wide v10, p3, v8

    .line 154
    :cond_0
    array-length v7, p2

    .line 155
    .local v7, "sampleSize":I
    invoke-static {p1, v7}, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->findGoodStepSize(II)I

    move-result v4

    .line 156
    .local v4, "prime":I
    rem-int v3, v4, p1

    .line 157
    .local v3, "mod":I
    sget-boolean v8, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->returnTimings:Z

    if-eqz v8, :cond_1

    .line 158
    const/4 v8, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    aput-wide v10, p3, v8

    .line 160
    :cond_1
    const/4 v5, 0x0

    .line 161
    .local v5, "sampleCount":I
    const/4 v1, 0x0

    .local v1, "index":I
    move v6, v5

    .line 162
    .end local v5    # "sampleCount":I
    .local v6, "sampleCount":I
    :goto_0
    if-lt v6, v7, :cond_3

    .line 176
    sget-boolean v8, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->returnTimings:Z

    if-eqz v8, :cond_2

    .line 177
    const/4 v8, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    aput-wide v10, p3, v8

    .line 179
    :cond_2
    return-void

    .line 163
    :cond_3
    add-int v8, v1, v3

    if-ge v8, p1, :cond_6

    .line 164
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v3, :cond_5

    .line 174
    :cond_4
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "sampleCount":I
    .restart local v5    # "sampleCount":I
    invoke-interface {v2}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getDocID()I

    move-result v8

    aput v8, p2, v6

    move v6, v5

    .end local v5    # "sampleCount":I
    .restart local v6    # "sampleCount":I
    goto :goto_0

    .line 165
    :cond_5
    invoke-interface {v2}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->next()Z

    .line 164
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 168
    .end local v0    # "i":I
    :cond_6
    add-int v8, v1, v3

    sub-int v1, v8, p1

    .line 169
    invoke-interface {p0}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;

    move-result-object v2

    .line 170
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    if-ge v0, v1, :cond_4

    .line 171
    invoke-interface {v2}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->next()Z

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private static sample2(Lorg/apache/lucene/facet/search/ScoredDocIDs;I[I[J)V
    .locals 11
    .param p0, "collection"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .param p1, "collectionSize"    # I
    .param p2, "sample"    # [I
    .param p3, "times"    # [J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v10, 0x7fffffff

    .line 272
    sget-boolean v6, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->returnTimings:Z

    if-eqz v6, :cond_0

    .line 273
    const/4 v6, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    aput-wide v8, p3, v6

    .line 275
    :cond_0
    array-length v4, p2

    .line 276
    .local v4, "sampleSize":I
    new-instance v3, Lorg/apache/lucene/facet/sampling/RepeatableSampler$IntPriorityQueue;

    invoke-direct {v3, v4}, Lorg/apache/lucene/facet/sampling/RepeatableSampler$IntPriorityQueue;-><init>(I)V

    .line 281
    .local v3, "pq":Lorg/apache/lucene/facet/sampling/RepeatableSampler$IntPriorityQueue;
    invoke-interface {p0}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;

    move-result-object v1

    .line 282
    .local v1, "it":Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
    const/4 v2, 0x0

    .line 283
    .local v2, "mi":Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;
    :goto_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->next()Z

    move-result v6

    if-nez v6, :cond_3

    .line 290
    sget-boolean v6, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->returnTimings:Z

    if-eqz v6, :cond_1

    .line 291
    const/4 v6, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    aput-wide v8, p3, v6

    .line 296
    :cond_1
    invoke-virtual {v3}, Lorg/apache/lucene/facet/sampling/RepeatableSampler$IntPriorityQueue;->getHeap()[Ljava/lang/Object;

    move-result-object v0

    .line 297
    .local v0, "heap":[Ljava/lang/Object;
    const/4 v5, 0x0

    .local v5, "si":I
    :goto_1
    if-lt v5, v4, :cond_5

    .line 300
    sget-boolean v6, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->returnTimings:Z

    if-eqz v6, :cond_2

    .line 301
    const/4 v6, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    aput-wide v8, p3, v6

    .line 303
    :cond_2
    return-void

    .line 284
    .end local v0    # "heap":[Ljava/lang/Object;
    .end local v5    # "si":I
    :cond_3
    if-nez v2, :cond_4

    .line 285
    new-instance v2, Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;

    .end local v2    # "mi":Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;
    invoke-direct {v2}, Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;-><init>()V

    .line 287
    .restart local v2    # "mi":Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;
    :cond_4
    invoke-interface {v1}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getDocID()I

    move-result v6

    int-to-long v6, v6

    const-wide v8, 0x9e3779b9L

    mul-long/2addr v6, v8

    long-to-int v6, v6

    and-int/2addr v6, v10

    iput v6, v2, Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;->value:I

    .line 288
    invoke-virtual {v3, v2}, Lorg/apache/lucene/facet/sampling/RepeatableSampler$IntPriorityQueue;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "mi":Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;
    check-cast v2, Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;

    .restart local v2    # "mi":Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;
    goto :goto_0

    .line 298
    .restart local v0    # "heap":[Ljava/lang/Object;
    .restart local v5    # "si":I
    :cond_5
    add-int/lit8 v6, v5, 0x1

    aget-object v6, v0, v6

    check-cast v6, Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;

    iget v6, v6, Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;->value:I

    int-to-long v6, v6

    const-wide/32 v8, 0x144cbc89

    mul-long/2addr v6, v8

    long-to-int v6, v6

    and-int/2addr v6, v10

    aput v6, p2, v5

    .line 297
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected createSample(Lorg/apache/lucene/facet/search/ScoredDocIDs;II)Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;
    .locals 8
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .param p2, "actualSize"    # I
    .param p3, "sampleSetSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    const/4 v1, 0x0

    .line 48
    .local v1, "sampleSet":[I
    :try_start_0
    invoke-static {p1, p2, p3}, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->repeatableSample(Lorg/apache/lucene/facet/search/ScoredDocIDs;II)[I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 57
    invoke-static {p1, v1}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils;->createScoredDocIDsSubset(Lorg/apache/lucene/facet/search/ScoredDocIDs;[I)Lorg/apache/lucene/facet/search/ScoredDocIDs;

    move-result-object v2

    .line 59
    .local v2, "sampled":Lorg/apache/lucene/facet/search/ScoredDocIDs;
    sget-object v3, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 60
    sget-object v3, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "******************** "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    .line 62
    :cond_0
    new-instance v3, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;

    invoke-interface {v2}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->size()I

    move-result v4

    int-to-double v4, v4

    invoke-interface {p1}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->size()I

    move-result v6

    int-to-double v6, v6

    div-double/2addr v4, v6

    invoke-direct {v3, v2, v4, v5}, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;-><init>(Lorg/apache/lucene/facet/search/ScoredDocIDs;D)V

    .end local v2    # "sampled":Lorg/apache/lucene/facet/search/ScoredDocIDs;
    :goto_0
    return-object v3

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 52
    sget-object v3, Lorg/apache/lucene/facet/sampling/RepeatableSampler;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sampling failed: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - falling back to no sampling!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 54
    :cond_1
    new-instance v3, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-direct {v3, p1, v4, v5}, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;-><init>(Lorg/apache/lucene/facet/search/ScoredDocIDs;D)V

    goto :goto_0
.end method
