.class Lorg/apache/lucene/facet/sampling/RepeatableSampler$IntPriorityQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "RepeatableSampler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/sampling/RepeatableSampler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IntPriorityQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 324
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 325
    return-void
.end method


# virtual methods
.method public getHeap()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 334
    invoke-virtual {p0}, Lorg/apache/lucene/facet/sampling/RepeatableSampler$IntPriorityQueue;->getHeapArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;

    check-cast p2, Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/facet/sampling/RepeatableSampler$IntPriorityQueue;->lessThan(Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;)Z

    move-result v0

    return v0
.end method

.method public lessThan(Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;)Z
    .locals 2
    .param p1, "o1"    # Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;
    .param p2, "o2"    # Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;

    .prologue
    .line 344
    iget v0, p1, Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;->value:I

    iget v1, p2, Lorg/apache/lucene/facet/sampling/RepeatableSampler$MI;->value:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
