.class public abstract Lorg/apache/lucene/facet/search/FacetsCollector;
.super Lorg/apache/lucene/search/Collector;
.source "FacetsCollector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;,
        Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;,
        Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    }
.end annotation


# instance fields
.field private final accumulator:Lorg/apache/lucene/facet/search/FacetsAccumulator;

.field private cachedResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResult;",
            ">;"
        }
    .end annotation
.end field

.field protected final matchingDocs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/facet/search/FacetsAccumulator;)V
    .locals 1
    .param p1, "accumulator"    # Lorg/apache/lucene/facet/search/FacetsAccumulator;

    .prologue
    .line 190
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector;->matchingDocs:Ljava/util/List;

    .line 191
    iput-object p1, p0, Lorg/apache/lucene/facet/search/FacetsCollector;->accumulator:Lorg/apache/lucene/facet/search/FacetsAccumulator;

    .line 192
    return-void
.end method

.method public static create(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/FacetsCollector;
    .locals 1
    .param p0, "fsp"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .param p1, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "taxoReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .prologue
    .line 170
    invoke-static {p0, p1, p2}, Lorg/apache/lucene/facet/search/FacetsAccumulator;->create(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/FacetsAccumulator;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/facet/search/FacetsCollector;->create(Lorg/apache/lucene/facet/search/FacetsAccumulator;)Lorg/apache/lucene/facet/search/FacetsCollector;

    move-result-object v0

    return-object v0
.end method

.method public static create(Lorg/apache/lucene/facet/search/FacetsAccumulator;)Lorg/apache/lucene/facet/search/FacetsCollector;
    .locals 1
    .param p0, "accumulator"    # Lorg/apache/lucene/facet/search/FacetsAccumulator;

    .prologue
    .line 178
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/FacetsAccumulator;->getAggregator()Lorg/apache/lucene/facet/search/FacetsAggregator;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/lucene/facet/search/FacetsAggregator;->requiresDocScores()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    new-instance v0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/search/FacetsCollector$DocsAndScoresCollector;-><init>(Lorg/apache/lucene/facet/search/FacetsAccumulator;)V

    .line 181
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/search/FacetsCollector$DocsOnlyCollector;-><init>(Lorg/apache/lucene/facet/search/FacetsAccumulator;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract doSetNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract finish()V
.end method

.method public final getFacetResults()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResult;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector;->cachedResults:Ljava/util/List;

    if-nez v0, :cond_0

    .line 213
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/FacetsCollector;->finish()V

    .line 214
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector;->accumulator:Lorg/apache/lucene/facet/search/FacetsAccumulator;

    iget-object v1, p0, Lorg/apache/lucene/facet/search/FacetsCollector;->matchingDocs:Ljava/util/List;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/search/FacetsAccumulator;->accumulate(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector;->cachedResults:Ljava/util/List;

    .line 217
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector;->cachedResults:Ljava/util/List;

    return-object v0
.end method

.method public final getMatchingDocs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/FacetsCollector;->finish()V

    .line 226
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector;->matchingDocs:Ljava/util/List;

    return-object v0
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/FacetsCollector;->finish()V

    .line 236
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector;->matchingDocs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 237
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector;->cachedResults:Ljava/util/List;

    .line 238
    return-void
.end method

.method public final setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 245
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetsCollector;->cachedResults:Ljava/util/List;

    .line 246
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/search/FacetsCollector;->doSetNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)V

    .line 247
    return-void
.end method
