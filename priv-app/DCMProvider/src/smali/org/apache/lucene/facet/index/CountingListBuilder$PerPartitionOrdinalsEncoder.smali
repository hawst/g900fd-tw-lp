.class final Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;
.super Lorg/apache/lucene/facet/index/CountingListBuilder$OrdinalsEncoder;
.source "CountingListBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/index/CountingListBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PerPartitionOrdinalsEncoder"
.end annotation


# instance fields
.field private final categoryListParams:Lorg/apache/lucene/facet/params/CategoryListParams;

.field private final indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

.field private final partitionEncoder:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/facet/encoding/IntEncoder;",
            ">;"
        }
    .end annotation
.end field

.field private final partitionSize:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/params/CategoryListParams;)V
    .locals 1
    .param p1, "indexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p2, "categoryListParams"    # Lorg/apache/lucene/facet/params/CategoryListParams;

    .prologue
    .line 82
    invoke-direct {p0}, Lorg/apache/lucene/facet/index/CountingListBuilder$OrdinalsEncoder;-><init>()V

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;->partitionEncoder:Ljava/util/HashMap;

    .line 83
    iput-object p1, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 84
    iput-object p2, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;->categoryListParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 85
    invoke-virtual {p1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getPartitionSize()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;->partitionSize:I

    .line 86
    return-void
.end method


# virtual methods
.method public encode(Lorg/apache/lucene/util/IntsRef;)Ljava/util/HashMap;
    .locals 12
    .param p1, "ordinals"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/IntsRef;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 92
    .local v7, "partitionOrdinals":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lorg/apache/lucene/util/IntsRef;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v9, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v3, v9, :cond_0

    .line 104
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 105
    .local v6, "partitionBytes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;>;"
    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_2

    .line 112
    return-object v6

    .line 93
    .end local v6    # "partitionBytes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;>;"
    :cond_0
    iget-object v9, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v5, v9, v3

    .line 94
    .local v5, "ordinal":I
    iget-object v9, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-static {v9, v5}, Lorg/apache/lucene/facet/util/PartitionsUtils;->partitionNameByOrdinal(Lorg/apache/lucene/facet/params/FacetIndexingParams;I)Ljava/lang/String;

    move-result-object v4

    .line 95
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/util/IntsRef;

    .line 96
    .local v8, "partitionOrds":Lorg/apache/lucene/util/IntsRef;
    if-nez v8, :cond_1

    .line 97
    new-instance v8, Lorg/apache/lucene/util/IntsRef;

    .end local v8    # "partitionOrds":Lorg/apache/lucene/util/IntsRef;
    const/16 v9, 0x20

    invoke-direct {v8, v9}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    .line 98
    .restart local v8    # "partitionOrds":Lorg/apache/lucene/util/IntsRef;
    invoke-virtual {v7, v4, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v9, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;->partitionEncoder:Ljava/util/HashMap;

    iget-object v10, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;->categoryListParams:Lorg/apache/lucene/facet/params/CategoryListParams;

    invoke-virtual {v10}, Lorg/apache/lucene/facet/params/CategoryListParams;->createEncoder()Lorg/apache/lucene/facet/encoding/IntEncoder;

    move-result-object v10

    invoke-virtual {v9, v4, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    :cond_1
    iget-object v9, v8, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v10, v8, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v8, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v11, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;->partitionSize:I

    rem-int v11, v5, v11

    aput v11, v9, v10

    .line 92
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 105
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "ordinal":I
    .end local v8    # "partitionOrds":Lorg/apache/lucene/util/IntsRef;
    .restart local v6    # "partitionBytes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;>;"
    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 106
    .local v1, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/util/IntsRef;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 107
    .restart local v4    # "name":Ljava/lang/String;
    iget-object v9, p0, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;->partitionEncoder:Ljava/util/HashMap;

    invoke-virtual {v9, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/facet/encoding/IntEncoder;

    .line 108
    .local v2, "encoder":Lorg/apache/lucene/facet/encoding/IntEncoder;
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v9, 0x80

    invoke-direct {v0, v9}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 109
    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v2, v9, v0}, Lorg/apache/lucene/facet/encoding/IntEncoder;->encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 110
    invoke-virtual {v6, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public bridge synthetic encode(Lorg/apache/lucene/util/IntsRef;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/index/CountingListBuilder$PerPartitionOrdinalsEncoder;->encode(Lorg/apache/lucene/util/IntsRef;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method
