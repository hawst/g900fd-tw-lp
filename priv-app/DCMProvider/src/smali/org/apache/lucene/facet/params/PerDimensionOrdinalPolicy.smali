.class public Lorg/apache/lucene/facet/params/PerDimensionOrdinalPolicy;
.super Lorg/apache/lucene/facet/params/CategoryListParams;
.source "PerDimensionOrdinalPolicy.java"


# instance fields
.field private final defaultOP:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

.field private final policies:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "policies":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;>;"
    sget-object v0, Lorg/apache/lucene/facet/params/PerDimensionOrdinalPolicy;->DEFAULT_ORDINAL_POLICY:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/params/PerDimensionOrdinalPolicy;-><init>(Ljava/util/Map;Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;)V
    .locals 0
    .param p2, "defaultOP"    # Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;",
            ">;",
            "Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "policies":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;>;"
    invoke-direct {p0}, Lorg/apache/lucene/facet/params/CategoryListParams;-><init>()V

    .line 40
    iput-object p2, p0, Lorg/apache/lucene/facet/params/PerDimensionOrdinalPolicy;->defaultOP:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/facet/params/PerDimensionOrdinalPolicy;->policies:Ljava/util/Map;

    .line 42
    return-void
.end method


# virtual methods
.method public getOrdinalPolicy(Ljava/lang/String;)Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    .locals 2
    .param p1, "dimension"    # Ljava/lang/String;

    .prologue
    .line 46
    iget-object v1, p0, Lorg/apache/lucene/facet/params/PerDimensionOrdinalPolicy;->policies:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    .line 47
    .local v0, "op":Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/facet/params/PerDimensionOrdinalPolicy;->defaultOP:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    .end local v0    # "op":Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    :cond_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lorg/apache/lucene/facet/params/CategoryListParams;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " policies="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/facet/params/PerDimensionOrdinalPolicy;->policies:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
