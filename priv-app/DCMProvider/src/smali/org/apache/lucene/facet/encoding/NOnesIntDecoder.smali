.class public Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;
.super Lorg/apache/lucene/facet/encoding/FourFlagsIntDecoder;
.source "NOnesIntDecoder.java"


# instance fields
.field private final internalBuffer:Lorg/apache/lucene/util/IntsRef;

.field private final n:I


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/FourFlagsIntDecoder;-><init>()V

    .line 39
    iput p1, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->n:I

    .line 41
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    .line 42
    return-void
.end method


# virtual methods
.method public decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V
    .locals 7
    .param p1, "buf"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "values"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 46
    iput v4, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v4, p2, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 47
    iget-object v3, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iput v4, v3, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 48
    iget-object v3, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    invoke-super {p0, p1, v3}, Lorg/apache/lucene/facet/encoding/FourFlagsIntDecoder;->decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V

    .line 49
    iget-object v3, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v3, v3

    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget v4, v4, Lorg/apache/lucene/util/IntsRef;->length:I

    if-ge v3, v4, :cond_0

    .line 52
    iget-object v3, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget v3, v3, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->n:I

    mul-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {p2, v3}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 55
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget v3, v3, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v1, v3, :cond_1

    .line 79
    return-void

    .line 56
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget-object v3, v3, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v0, v3, v1

    .line 57
    .local v0, "decode":I
    if-ne v0, v6, :cond_4

    .line 58
    iget v3, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iget-object v4, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v4, v4

    if-ne v3, v4, :cond_2

    .line 59
    iget v3, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v3, v3, 0xa

    invoke-virtual {p2, v3}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 62
    :cond_2
    iget-object v3, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v4, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    aput v6, v3, v4

    .line 55
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    :cond_4
    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    .line 64
    iget v3, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->n:I

    add-int/2addr v3, v4

    iget-object v4, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v4, v4

    if-lt v3, v4, :cond_5

    .line 65
    iget v3, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->n:I

    add-int/2addr v3, v4

    invoke-virtual {p2, v3}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 68
    :cond_5
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    iget v3, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->n:I

    if-ge v2, v3, :cond_3

    .line 69
    iget-object v3, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v4, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    aput v6, v3, v4

    .line 68
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 72
    .end local v2    # "j":I
    :cond_6
    iget v3, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iget-object v4, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v4, v4

    if-ne v3, v4, :cond_7

    .line 73
    iget v3, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v3, v3, 0xa

    invoke-virtual {p2, v3}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 76
    :cond_7
    iget-object v3, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v4, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v5, v0, -0x1

    aput v5, v3, v4

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NOnes("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lorg/apache/lucene/facet/encoding/FourFlagsIntDecoder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
