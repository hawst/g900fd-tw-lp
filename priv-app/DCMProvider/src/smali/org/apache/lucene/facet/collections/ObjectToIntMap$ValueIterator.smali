.class final Lorg/apache/lucene/facet/collections/ObjectToIntMap$ValueIterator;
.super Ljava/lang/Object;
.source "ObjectToIntMap.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/IntIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/ObjectToIntMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ValueIterator"
.end annotation


# instance fields
.field private iterator:Lorg/apache/lucene/facet/collections/IntIterator;

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/ObjectToIntMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/collections/ObjectToIntMap;)V
    .locals 1

    .prologue
    .line 138
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/ObjectToIntMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/ObjectToIntMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    new-instance v0, Lorg/apache/lucene/facet/collections/ObjectToIntMap$IndexIterator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/collections/ObjectToIntMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/ObjectToIntMap;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/collections/ObjectToIntMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    .line 138
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ObjectToIntMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()I
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ObjectToIntMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/ObjectToIntMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/ObjectToIntMap;->values:[I

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/ObjectToIntMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ObjectToIntMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->remove()V

    .line 153
    return-void
.end method
