.class final Lorg/apache/lucene/facet/collections/IntToDoubleMap$ValueIterator;
.super Ljava/lang/Object;
.source "IntToDoubleMap.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/DoubleIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/IntToDoubleMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ValueIterator"
.end annotation


# instance fields
.field private iterator:Lorg/apache/lucene/facet/collections/IntIterator;

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/IntToDoubleMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/collections/IntToDoubleMap;)V
    .locals 1

    .prologue
    .line 136
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToDoubleMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToDoubleMap;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    .line 136
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()D
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToDoubleMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->values:[D

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->remove()V

    .line 151
    return-void
.end method
