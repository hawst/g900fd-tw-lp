.class public Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;
.super Lorg/apache/lucene/codecs/DocValuesConsumer;
.source "Facet42DocValuesConsumer.java"


# instance fields
.field final acceptableOverheadRatio:F

.field final maxDoc:I

.field final out:Lorg/apache/lucene/store/IndexOutput;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    const v0, 0x3e4ccccd    # 0.2f

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;-><init>(Lorg/apache/lucene/index/SegmentWriteState;F)V

    .line 41
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;F)V
    .locals 7
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "acceptableOverheadRatio"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer;-><init>()V

    .line 44
    iput p2, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->acceptableOverheadRatio:F

    .line 45
    const/4 v1, 0x0

    .line 47
    .local v1, "success":Z
    :try_start_0
    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    const-string v4, "fdv"

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "fileName":Ljava/lang/String;
    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v2, v0, v3}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->out:Lorg/apache/lucene/store/IndexOutput;

    .line 49
    iget-object v2, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->out:Lorg/apache/lucene/store/IndexOutput;

    const-string v3, "FacetsDocValues"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 50
    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->maxDoc:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    const/4 v1, 0x1

    .line 53
    if-nez v1, :cond_0

    new-array v2, v6, [Ljava/io/Closeable;

    .line 54
    aput-object p0, v2, v5

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 57
    :cond_0
    return-void

    .line 52
    .end local v0    # "fileName":Ljava/lang/String;
    :catchall_0
    move-exception v2

    .line 53
    if-nez v1, :cond_1

    new-array v3, v6, [Ljava/io/Closeable;

    .line 54
    aput-object p0, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 56
    :cond_1
    throw v2
.end method


# virtual methods
.method public addBinaryField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .locals 10
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    iget-object v5, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->out:Lorg/apache/lucene/store/IndexOutput;

    iget v6, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v5, v6}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 69
    const-wide/16 v2, 0x0

    .line 70
    .local v2, "totBytes":J
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 74
    const-wide/32 v6, 0x7fffffff

    cmp-long v5, v2, v6

    if-lez v5, :cond_1

    .line 75
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "too many facets in one segment: Facet42DocValues cannot handle more than 2 GB facet data per segment"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 70
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/BytesRef;

    .line 71
    .local v1, "v":Lorg/apache/lucene/util/BytesRef;
    iget v6, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-long v6, v6

    add-long/2addr v2, v6

    goto :goto_0

    .line 78
    .end local v1    # "v":Lorg/apache/lucene/util/BytesRef;
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->out:Lorg/apache/lucene/store/IndexOutput;

    long-to-int v6, v2

    invoke-virtual {v5, v6}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 80
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 84
    iget-object v5, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->out:Lorg/apache/lucene/store/IndexOutput;

    iget v6, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->maxDoc:I

    add-int/lit8 v6, v6, 0x1

    const-wide/16 v8, 0x1

    add-long/2addr v8, v2

    invoke-static {v8, v9}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v7

    iget v8, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->acceptableOverheadRatio:F

    invoke-static {v5, v6, v7, v8}, Lorg/apache/lucene/util/packed/PackedInts;->getWriter(Lorg/apache/lucene/store/DataOutput;IIF)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v4

    .line 86
    .local v4, "w":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    const/4 v0, 0x0

    .line 87
    .local v0, "address":I
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 91
    int-to-long v6, v0

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    .line 92
    invoke-virtual {v4}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->finish()V

    .line 93
    return-void

    .line 80
    .end local v0    # "address":I
    .end local v4    # "w":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/BytesRef;

    .line 81
    .restart local v1    # "v":Lorg/apache/lucene/util/BytesRef;
    iget-object v6, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->out:Lorg/apache/lucene/store/IndexOutput;

    iget-object v7, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v8, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v9, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v6, v7, v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    goto :goto_1

    .line 87
    .end local v1    # "v":Lorg/apache/lucene/util/BytesRef;
    .restart local v0    # "address":I
    .restart local v4    # "w":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/BytesRef;

    .line 88
    .restart local v1    # "v":Lorg/apache/lucene/util/BytesRef;
    int-to-long v6, v0

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    .line 89
    iget v6, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v0, v6

    goto :goto_2
.end method

.method public addNumericField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .locals 2
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FacetsDocValues can only handle binary fields"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addSortedField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .locals 2
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    .local p3, "docToOrd":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FacetsDocValues can only handle binary fields"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addSortedSetField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .locals 2
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    .local p3, "docToOrdCount":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    .local p4, "ords":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FacetsDocValues can only handle binary fields"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 107
    const/4 v0, 0x0

    .line 109
    .local v0, "success":Z
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->out:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    const/4 v0, 0x1

    .line 112
    if-eqz v0, :cond_1

    new-array v1, v3, [Ljava/io/Closeable;

    .line 113
    iget-object v2, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v4

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 118
    :goto_0
    return-void

    .line 111
    :catchall_0
    move-exception v1

    .line 112
    if-eqz v0, :cond_0

    new-array v2, v3, [Ljava/io/Closeable;

    .line 113
    iget-object v3, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 117
    :goto_1
    throw v1

    .line 114
    :cond_0
    new-array v2, v3, [Ljava/io/Closeable;

    .line 115
    iget-object v3, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    .line 114
    :cond_1
    new-array v1, v3, [Ljava/io/Closeable;

    .line 115
    iget-object v2, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42DocValuesConsumer;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v4

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method
