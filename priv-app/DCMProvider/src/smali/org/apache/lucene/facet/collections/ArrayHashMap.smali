.class public Lorg/apache/lucene/facet/collections/ArrayHashMap;
.super Ljava/lang/Object;
.source "ArrayHashMap.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;,
        Lorg/apache/lucene/facet/collections/ArrayHashMap$KeyIterator;,
        Lorg/apache/lucene/facet/collections/ArrayHashMap$ValueIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TV;>;"
    }
.end annotation


# static fields
.field private static final DEFAULT_CAPACITY:I = 0x10


# instance fields
.field baseHash:[I

.field private capacity:I

.field private firstEmpty:I

.field private hashFactor:I

.field keys:[Ljava/lang/Object;

.field next:[I

.field private prev:I

.field private size:I

.field values:[Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 191
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;-><init>(I)V

    .line 192
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1, "capacity"    # I

    .prologue
    .line 200
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    const/16 v2, 0x10

    iput v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    .line 202
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    if-lt v2, p1, :cond_0

    .line 209
    iget v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    add-int/lit8 v0, v2, 0x1

    .line 211
    .local v0, "arrayLength":I
    new-array v2, v0, [Ljava/lang/Object;

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->values:[Ljava/lang/Object;

    .line 212
    new-array v2, v0, [Ljava/lang/Object;

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->keys:[Ljava/lang/Object;

    .line 213
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    .line 216
    iget v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    shl-int/lit8 v1, v2, 0x1

    .line 218
    .local v1, "baseHashSize":I
    new-array v2, v1, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    .line 222
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->hashFactor:I

    .line 224
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    .line 226
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->clear()V

    .line 227
    return-void

    .line 204
    .end local v0    # "arrayLength":I
    .end local v1    # "baseHashSize":I
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    goto :goto_0
.end method

.method private findForRemove(Ljava/lang/Object;I)I
    .locals 3
    .param p2, "baseHashIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I)I"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    const/4 v1, 0x0

    .line 331
    iput v1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->prev:I

    .line 332
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    aget v0, v2, p2

    .line 335
    .local v0, "index":I
    :goto_0
    if-nez v0, :cond_1

    .line 348
    iput v1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->prev:I

    move v0, v1

    .end local v0    # "index":I
    :cond_0
    return v0

    .line 337
    .restart local v0    # "index":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->keys:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 342
    iput v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->prev:I

    .line 343
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    aget v0, v2, v0

    goto :goto_0
.end method

.method private getBaseHashAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 403
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private prvt_put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->calcBaseHashIndex(Ljava/lang/Object;)I

    move-result v0

    .line 239
    .local v0, "hashIndex":I
    iget v1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->firstEmpty:I

    .line 242
    .local v1, "objectIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->firstEmpty:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->firstEmpty:I

    .line 243
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->values:[Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 244
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->keys:[Ljava/lang/Object;

    aput-object p1, v2, v1

    .line 247
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 248
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    aput v1, v2, v0

    .line 251
    iget v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    .line 252
    return-void
.end method


# virtual methods
.method protected calcBaseHashIndex(Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    .line 256
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->hashFactor:I

    and-int/2addr v0, v1

    return v0
.end method

.method public clear()V
    .locals 5

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    const/4 v4, 0x0

    .line 262
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 265
    iput v4, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    .line 270
    const/4 v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->firstEmpty:I

    .line 274
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    if-lt v0, v2, :cond_0

    .line 279
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    aput v4, v2, v3

    .line 280
    return-void

    .line 275
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput v1, v2, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)Z"
        }
    .end annotation

    .prologue
    .line 284
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->find(Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 289
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TV;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 295
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 290
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 291
    .local v1, "object":Ljava/lang/Object;, "TV;"
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 292
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    const/4 v5, 0x0

    .line 536
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/facet/collections/ArrayHashMap;

    .line 537
    .local v2, "that":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    invoke-virtual {v2}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size()I

    move-result v6

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size()I

    move-result v7

    if-eq v6, v7, :cond_1

    .line 552
    :cond_0
    :goto_0
    return v5

    .line 541
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->keyIterator()Ljava/util/Iterator;

    move-result-object v0

    .line 542
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<TK;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 552
    const/4 v5, 0x1

    goto :goto_0

    .line 543
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 544
    .local v1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 545
    .local v3, "v1":Ljava/lang/Object;, "TV;"
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 546
    .local v4, "v2":Ljava/lang/Object;, "TV;"
    if-nez v3, :cond_4

    if-nez v4, :cond_0

    .line 547
    :cond_4
    if-eqz v3, :cond_5

    if-eqz v4, :cond_0

    .line 548
    :cond_5
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    goto :goto_0
.end method

.method protected find(Ljava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    .line 301
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->calcBaseHashIndex(Ljava/lang/Object;)I

    move-result v0

    .line 304
    .local v0, "baseHashIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    aget v1, v2, v0

    .line 307
    .local v1, "localIndex":I
    :goto_0
    if-nez v1, :cond_1

    .line 319
    const/4 v1, 0x0

    .end local v1    # "localIndex":I
    :cond_0
    return v1

    .line 309
    .restart local v1    # "localIndex":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->keys:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 314
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    aget v1, v2, v1

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 354
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->values:[Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->find(Ljava/lang/Object;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected grow()V
    .locals 5

    .prologue
    .line 363
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    new-instance v2, Lorg/apache/lucene/facet/collections/ArrayHashMap;

    iget v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Lorg/apache/lucene/facet/collections/ArrayHashMap;-><init>(I)V

    .line 368
    .local v2, "newmap":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    new-instance v1, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;

    invoke-direct {v1, p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/ArrayHashMap;)V

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>.IndexIterator;"
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 374
    iget v3, v2, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    .line 375
    iget v3, v2, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    .line 376
    iget v3, v2, Lorg/apache/lucene/facet/collections/ArrayHashMap;->firstEmpty:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->firstEmpty:I

    .line 377
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/ArrayHashMap;->values:[Ljava/lang/Object;

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->values:[Ljava/lang/Object;

    .line 378
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/ArrayHashMap;->keys:[Ljava/lang/Object;

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->keys:[Ljava/lang/Object;

    .line 379
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    .line 380
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    .line 381
    iget v3, v2, Lorg/apache/lucene/facet/collections/ArrayHashMap;->hashFactor:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->hashFactor:I

    .line 382
    return-void

    .line 369
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->next()I

    move-result v0

    .line 370
    .local v0, "index":I
    iget-object v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->keys:[Ljava/lang/Object;

    aget-object v3, v3, v0

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->values:[Ljava/lang/Object;

    aget-object v4, v4, v0

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->prvt_put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 530
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 386
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    iget v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 392
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    new-instance v0, Lorg/apache/lucene/facet/collections/ArrayHashMap$ValueIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap$ValueIterator;-><init>(Lorg/apache/lucene/facet/collections/ArrayHashMap;)V

    return-object v0
.end method

.method public keyIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 397
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    new-instance v0, Lorg/apache/lucene/facet/collections/ArrayHashMap$KeyIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap$KeyIterator;-><init>(Lorg/apache/lucene/facet/collections/ArrayHashMap;)V

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 416
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "e":Ljava/lang/Object;, "TV;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->find(Ljava/lang/Object;)I

    move-result v0

    .line 419
    .local v0, "index":I
    if-eqz v0, :cond_0

    .line 421
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->values:[Ljava/lang/Object;

    aget-object v1, v2, v0

    .line 422
    .local v1, "old":Ljava/lang/Object;, "TV;"
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->values:[Ljava/lang/Object;

    aput-object p2, v2, v0

    .line 436
    .end local v1    # "old":Ljava/lang/Object;, "TV;"
    :goto_0
    return-object v1

    .line 427
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    iget v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->capacity:I

    if-ne v2, v3, :cond_1

    .line 429
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->grow()V

    .line 434
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->prvt_put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 436
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 448
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->calcBaseHashIndex(Ljava/lang/Object;)I

    move-result v0

    .line 449
    .local v0, "baseHashIndex":I
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->findForRemove(Ljava/lang/Object;I)I

    move-result v1

    .line 450
    .local v1, "index":I
    if-eqz v1, :cond_1

    .line 453
    iget v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->prev:I

    if-nez v2, :cond_0

    .line 454
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    aget v3, v3, v1

    aput v3, v2, v0

    .line 457
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->prev:I

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    aget v4, v4, v1

    aput v4, v2, v3

    .line 458
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->firstEmpty:I

    aput v3, v2, v1

    .line 459
    iput v1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->firstEmpty:I

    .line 460
    iget v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    .line 461
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->values:[Ljava/lang/Object;

    aget-object v2, v2, v1

    .line 464
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 469
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    iget v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 4

    .prologue
    .line 478
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    const/4 v2, -0x1

    .line 479
    .local v2, "j":I
    iget v3, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->size:I

    new-array v0, v3, [Ljava/lang/Object;

    .line 482
    .local v0, "array":[Ljava/lang/Object;
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TV;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 485
    return-object v0

    .line 483
    :cond_0
    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v0, v2

    goto :goto_0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1, "a"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TV;)[TV;"
        }
    .end annotation

    .prologue
    .line 496
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    const/4 v1, 0x0

    .line 498
    .local v1, "j":I
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TV;>;"
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 499
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 502
    :cond_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 503
    const/4 v2, 0x0

    aput-object v2, p1, v1

    .line 506
    :cond_1
    return-object p1

    .line 500
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, p1, v1

    .line 499
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 511
    .local p0, "this":Lorg/apache/lucene/facet/collections/ArrayHashMap;, "Lorg/apache/lucene/facet/collections/ArrayHashMap<TK;TV;>;"
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 512
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/16 v3, 0x7b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 513
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->keyIterator()Ljava/util/Iterator;

    move-result-object v1

    .line 514
    .local v1, "keyIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<TK;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 524
    const/16 v3, 0x7d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 525
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 515
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 516
    .local v0, "key":Ljava/lang/Object;, "TK;"
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 517
    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 518
    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 519
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 520
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 521
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
