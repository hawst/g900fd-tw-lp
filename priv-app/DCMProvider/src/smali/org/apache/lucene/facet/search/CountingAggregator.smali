.class public Lorg/apache/lucene/facet/search/CountingAggregator;
.super Ljava/lang/Object;
.source "CountingAggregator.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/Aggregator;


# instance fields
.field protected counterArray:[I


# direct methods
.method public constructor <init>([I)V
    .locals 0
    .param p1, "counterArray"    # [I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/facet/search/CountingAggregator;->counterArray:[I

    .line 38
    return-void
.end method


# virtual methods
.method public aggregate(IFLorg/apache/lucene/util/IntsRef;)V
    .locals 4
    .param p1, "docID"    # I
    .param p2, "score"    # F
    .param p3, "ordinals"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v0, v1, :cond_0

    .line 45
    return-void

    .line 43
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/facet/search/CountingAggregator;->counterArray:[I

    iget-object v2, p3, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v2, v2, v0

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 49
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 52
    check-cast v0, Lorg/apache/lucene/facet/search/CountingAggregator;

    .line 53
    .local v0, "that":Lorg/apache/lucene/facet/search/CountingAggregator;
    iget-object v2, v0, Lorg/apache/lucene/facet/search/CountingAggregator;->counterArray:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/search/CountingAggregator;->counterArray:[I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/facet/search/CountingAggregator;->counterArray:[I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/search/CountingAggregator;->counterArray:[I

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Z
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method
