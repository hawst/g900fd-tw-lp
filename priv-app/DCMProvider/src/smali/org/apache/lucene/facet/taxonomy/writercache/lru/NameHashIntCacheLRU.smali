.class public Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameHashIntCacheLRU;
.super Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;
.source "NameHashIntCacheLRU.java"


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "maxCacheSize"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;-><init>(I)V

    .line 35
    return-void
.end method


# virtual methods
.method public bridge synthetic getMaxSize()I
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->getMaxSize()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getSize()I
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/lru/NameIntCacheLRU;->getSize()I

    move-result v0

    return v0
.end method

.method key(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Ljava/lang/Object;
    .locals 4
    .param p1, "name"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    .line 39
    new-instance v0, Ljava/lang/Long;

    invoke-virtual {p1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->longHashCode()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/lang/Long;-><init>(J)V

    return-object v0
.end method

.method key(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)Ljava/lang/Object;
    .locals 4
    .param p1, "name"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "prefixLen"    # I

    .prologue
    .line 44
    new-instance v0, Ljava/lang/Long;

    invoke-virtual {p1, p2}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->subpath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->longHashCode()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/lang/Long;-><init>(J)V

    return-object v0
.end method
