.class public Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;
.super Ljava/lang/Object;
.source "DrillSideways.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/DrillSideways;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DrillSidewaysResult"
.end annotation


# instance fields
.field public final facetResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResult;",
            ">;"
        }
    .end annotation
.end field

.field public hits:Lorg/apache/lucene/search/TopDocs;


# direct methods
.method constructor <init>(Ljava/util/List;Lorg/apache/lucene/search/TopDocs;)V
    .locals 0
    .param p2, "hits"    # Lorg/apache/lucene/search/TopDocs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResult;",
            ">;",
            "Lorg/apache/lucene/search/TopDocs;",
            ")V"
        }
    .end annotation

    .prologue
    .line 312
    .local p1, "facetResults":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313
    iput-object p1, p0, Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;->facetResults:Ljava/util/List;

    .line 314
    iput-object p2, p0, Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;->hits:Lorg/apache/lucene/search/TopDocs;

    .line 315
    return-void
.end method
