.class public Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;
.super Ljava/lang/Object;
.source "Cl2oTaxonomyWriterCache.java"

# interfaces
.implements Lorg/apache/lucene/facet/taxonomy/writercache/TaxonomyWriterCache;


# instance fields
.field private volatile cache:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;

.field private final initialCapcity:I

.field private final loadFactor:F

.field private final lock:Ljava/util/concurrent/locks/ReadWriteLock;

.field private final numHashArrays:I


# direct methods
.method public constructor <init>(IFI)V
    .locals 1
    .param p1, "initialCapcity"    # I
    .param p2, "loadFactor"    # F
    .param p3, "numHashArrays"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 43
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;-><init>(IFI)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;

    .line 44
    iput p1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->initialCapcity:I

    .line 45
    iput p3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->numHashArrays:I

    .line 46
    iput p2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->loadFactor:F

    .line 47
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 53
    :try_start_0
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;

    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->initialCapcity:I

    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->loadFactor:F

    iget v3, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->numHashArrays:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;-><init>(IFI)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 57
    return-void

    .line 54
    :catchall_0
    move-exception v0

    .line 55
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 56
    throw v0
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 61
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    monitor-exit p0

    return-void

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public get(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    .locals 2
    .param p1, "categoryPath"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 74
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 76
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 74
    return v0

    .line 75
    :catchall_0
    move-exception v0

    .line 76
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 77
    throw v0
.end method

.method public getMemoryUsage()I
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->getMemoryUsage()I

    move-result v0

    goto :goto_0
.end method

.method public isFull()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public put(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)Z
    .locals 2
    .param p1, "categoryPath"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "ordinal"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 84
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->cache:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CompactLabelToOrdinal;->addLabel(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 87
    const/4 v0, 0x0

    return v0

    .line 88
    :catchall_0
    move-exception v0

    .line 89
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/Cl2oTaxonomyWriterCache;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 90
    throw v0
.end method
