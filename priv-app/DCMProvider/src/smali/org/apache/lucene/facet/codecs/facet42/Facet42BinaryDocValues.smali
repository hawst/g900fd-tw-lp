.class Lorg/apache/lucene/facet/codecs/facet42/Facet42BinaryDocValues;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "Facet42BinaryDocValues.java"


# instance fields
.field private final addresses:Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field private final bytes:[B


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/DataInput;)V
    .locals 3
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    .line 33
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v0

    .line 34
    .local v0, "totBytes":I
    new-array v1, v0, [B

    iput-object v1, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42BinaryDocValues;->bytes:[B

    .line 35
    iget-object v1, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42BinaryDocValues;->bytes:[B

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 36
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts;->getReader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42BinaryDocValues;->addresses:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 37
    return-void
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 6
    .param p1, "docID"    # I
    .param p2, "ret"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 41
    iget-object v1, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42BinaryDocValues;->addresses:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    .line 42
    .local v0, "start":I
    iget-object v1, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42BinaryDocValues;->bytes:[B

    iput-object v1, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 43
    iput v0, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 44
    iget-object v1, p0, Lorg/apache/lucene/facet/codecs/facet42/Facet42BinaryDocValues;->addresses:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    add-int/lit8 v2, p1, 0x1

    invoke-interface {v1, v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    int-to-long v4, v0

    sub-long/2addr v2, v4

    long-to-int v1, v2

    iput v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 45
    return-void
.end method
