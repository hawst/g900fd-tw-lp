.class public Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;
.super Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;
.source "EightFlagsIntEncoder.java"


# static fields
.field private static final ENCODE_TABLE:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->ENCODE_TABLE:[B

    return-void

    :array_0
    .array-data 1
        0x1t
        0x2t
        0x4t
        0x8t
        0x10t
        0x20t
        0x40t
        -0x80t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/encoding/ChunksIntEncoder;-><init>(I)V

    .line 59
    return-void
.end method


# virtual methods
.method public createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lorg/apache/lucene/facet/encoding/EightFlagsIntDecoder;

    invoke-direct {v0}, Lorg/apache/lucene/facet/encoding/EightFlagsIntDecoder;-><init>()V

    return-object v0
.end method

.method public encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 7
    .param p1, "values"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "buf"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 63
    const/4 v3, 0x0

    iput v3, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v3, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 64
    iget v3, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v1, v3, v4

    .line 65
    .local v1, "upto":I
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_1

    .line 81
    iget-byte v3, p0, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->ordinal:B

    if-eqz v3, :cond_0

    .line 82
    invoke-virtual {p0, p2}, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->encodeChunk(Lorg/apache/lucene/util/BytesRef;)V

    .line 84
    :cond_0
    return-void

    .line 66
    :cond_1
    iget-object v3, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v2, v3, v0

    .line 67
    .local v2, "value":I
    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 68
    iget v3, p0, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->indicator:I

    sget-object v4, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->ENCODE_TABLE:[B

    iget-byte v5, p0, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->ordinal:B

    aget-byte v4, v4, v5

    or-int/2addr v3, v4

    iput v3, p0, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->indicator:I

    .line 72
    :goto_1
    iget-byte v3, p0, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->ordinal:B

    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    iput-byte v3, p0, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->ordinal:B

    .line 75
    iget-byte v3, p0, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->ordinal:B

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    .line 76
    invoke-virtual {p0, p2}, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->encodeChunk(Lorg/apache/lucene/util/BytesRef;)V

    .line 65
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->encodeQueue:Lorg/apache/lucene/util/IntsRef;

    iget-object v3, v3, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/EightFlagsIntEncoder;->encodeQueue:Lorg/apache/lucene/util/IntsRef;

    iget v5, v4, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v6, v5, 0x1

    iput v6, v4, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v4, v2, -0x2

    aput v4, v3, v5

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const-string v0, "EightFlags(VInt)"

    return-object v0
.end method
