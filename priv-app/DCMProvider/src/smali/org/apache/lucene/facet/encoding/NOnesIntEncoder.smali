.class public Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;
.super Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;
.source "NOnesIntEncoder.java"


# instance fields
.field private final internalBuffer:Lorg/apache/lucene/util/IntsRef;

.field private final n:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 62
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;-><init>()V

    .line 63
    iput p1, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->n:I

    .line 64
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v0, p1}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    .line 65
    return-void
.end method


# virtual methods
.method public createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;
    .locals 2

    .prologue
    .line 106
    new-instance v0, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;

    iget v1, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->n:I

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/encoding/NOnesIntDecoder;-><init>(I)V

    return-object v0
.end method

.method public encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 9
    .param p1, "values"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "buf"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v8, 0x1

    .line 69
    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    const/4 v5, 0x0

    iput v5, v4, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 71
    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    iget-object v5, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget-object v5, v5, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v5, v5

    if-le v4, v5, :cond_0

    .line 72
    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 75
    :cond_0
    const/4 v1, 0x0

    .line 76
    .local v1, "onesCounter":I
    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v2, v4, v5

    .line 77
    .local v2, "upto":I
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_1

    .line 97
    :goto_1
    if-gtz v1, :cond_5

    .line 101
    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    invoke-super {p0, v4, p2}, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 102
    return-void

    .line 78
    :cond_1
    iget-object v4, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v3, v4, v0

    .line 79
    .local v3, "value":I
    if-ne v3, v8, :cond_4

    .line 81
    add-int/lit8 v1, v1, 0x1

    iget v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->n:I

    if-ne v1, v4, :cond_2

    .line 82
    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget-object v4, v4, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v5, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget v6, v5, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v7, v6, 0x1

    iput v7, v5, Lorg/apache/lucene/util/IntsRef;->length:I

    const/4 v5, 0x2

    aput v5, v4, v6

    .line 83
    const/4 v1, 0x0

    .line 77
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :cond_3
    add-int/lit8 v1, v1, -0x1

    .line 89
    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget-object v4, v4, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v5, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget v6, v5, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v7, v6, 0x1

    iput v7, v5, Lorg/apache/lucene/util/IntsRef;->length:I

    aput v8, v4, v6

    .line 87
    :cond_4
    if-gtz v1, :cond_3

    .line 93
    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget-object v4, v4, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v5, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget v6, v5, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v7, v6, 0x1

    iput v7, v5, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v5, v3, 0x1

    aput v5, v4, v6

    goto :goto_2

    .line 98
    .end local v3    # "value":I
    :cond_5
    add-int/lit8 v1, v1, -0x1

    .line 99
    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget-object v4, v4, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v5, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->internalBuffer:Lorg/apache/lucene/util/IntsRef;

    iget v6, v5, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v7, v6, 0x1

    iput v7, v5, Lorg/apache/lucene/util/IntsRef;->length:I

    aput v8, v4, v6

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NOnes("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/facet/encoding/NOnesIntEncoder;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lorg/apache/lucene/facet/encoding/FourFlagsIntEncoder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
