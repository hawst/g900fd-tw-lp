.class Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
.super Ljava/lang/Object;
.source "CharBlockArray.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Appendable;
.implements Ljava/lang/CharSequence;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;
    }
.end annotation


# static fields
.field private static final DefaultBlockSize:I = 0x8000

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field blockSize:I

.field blocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;",
            ">;"
        }
    .end annotation
.end field

.field current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

.field length:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 59
    const v0, 0x8000

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;-><init>(I)V

    .line 60
    return-void
.end method

.method constructor <init>(I)V
    .locals 1
    .param p1, "blockSize"    # I

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blocks:Ljava/util/List;

    .line 64
    iput p1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockSize:I

    .line 65
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->addBlock()V

    .line 66
    return-void
.end method

.method private addBlock()V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockSize:I

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    .line 70
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blocks:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    return-void
.end method

.method public static open(Ljava/io/InputStream;)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    .locals 4
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 200
    const/4 v1, 0x0

    .line 202
    .local v1, "ois":Ljava/io/ObjectInputStream;
    :try_start_0
    new-instance v2, Ljava/io/ObjectInputStream;

    invoke-direct {v2, p0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    .end local v1    # "ois":Ljava/io/ObjectInputStream;
    .local v2, "ois":Ljava/io/ObjectInputStream;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 206
    .local v0, "a":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    if-eqz v2, :cond_0

    .line 207
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V

    .line 204
    :cond_0
    return-object v0

    .line 205
    .end local v0    # "a":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    .end local v2    # "ois":Ljava/io/ObjectInputStream;
    .restart local v1    # "ois":Ljava/io/ObjectInputStream;
    :catchall_0
    move-exception v3

    .line 206
    :goto_0
    if-eqz v1, :cond_1

    .line 207
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    .line 209
    :cond_1
    throw v3

    .line 205
    .end local v1    # "ois":Ljava/io/ObjectInputStream;
    .restart local v2    # "ois":Ljava/io/ObjectInputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "ois":Ljava/io/ObjectInputStream;
    .restart local v1    # "ois":Ljava/io/ObjectInputStream;
    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic append(C)Ljava/lang/Appendable;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->append(C)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->append(Ljava/lang/CharSequence;)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    move-result-object v0

    return-object v0
.end method

.method public append(C)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    .locals 4
    .param p1, "c"    # C

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockSize:I

    if-ne v0, v1, :cond_0

    .line 89
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->addBlock()V

    .line 91
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->chars:[C

    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget v2, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v1, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    aput-char p1, v0, v2

    .line 92
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->length:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->length:I

    .line 94
    return-object p0
.end method

.method public append(Ljava/lang/CharSequence;)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    .locals 2
    .param p1, "chars"    # Ljava/lang/CharSequence;

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    move-result-object v0

    return-object v0
.end method

.method public append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    .locals 3
    .param p1, "chars"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 99
    add-int v0, p2, p3

    .line 100
    .local v0, "end":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 103
    return-object p0

    .line 101
    :cond_0
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->append(C)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;

    .line 100
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public append(Ljava/lang/String;)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    .locals 7
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 129
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 130
    .local v1, "remain":I
    const/4 v0, 0x0

    .line 131
    .local v0, "offset":I
    :goto_0
    if-gtz v1, :cond_0

    .line 146
    iget v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->length:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->length:I

    .line 147
    return-object p0

    .line 132
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget v4, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    iget v5, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockSize:I

    if-ne v4, v5, :cond_1

    .line 133
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->addBlock()V

    .line 135
    :cond_1
    move v3, v1

    .line 136
    .local v3, "toCopy":I
    iget v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockSize:I

    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget v5, v5, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    sub-int v2, v4, v5

    .line 137
    .local v2, "remainingInBlock":I
    if-ge v2, v3, :cond_2

    .line 138
    move v3, v2

    .line 140
    :cond_2
    add-int v4, v0, v3

    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget-object v5, v5, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->chars:[C

    iget-object v6, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget v6, v6, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    invoke-virtual {p1, v0, v4, v5, v6}, Ljava/lang/String;->getChars(II[CI)V

    .line 141
    add-int/2addr v0, v3

    .line 142
    sub-int/2addr v1, v3

    .line 143
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget v5, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    add-int/2addr v5, v3

    iput v5, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    goto :goto_0
.end method

.method public append([CII)Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
    .locals 6
    .param p1, "chars"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 107
    move v0, p2

    .line 108
    .local v0, "offset":I
    move v1, p3

    .line 109
    .local v1, "remain":I
    :goto_0
    if-gtz v1, :cond_0

    .line 124
    iget v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->length:I

    add-int/2addr v4, p3

    iput v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->length:I

    .line 125
    return-object p0

    .line 110
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget v4, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    iget v5, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockSize:I

    if-ne v4, v5, :cond_1

    .line 111
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->addBlock()V

    .line 113
    :cond_1
    move v3, v1

    .line 114
    .local v3, "toCopy":I
    iget v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockSize:I

    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget v5, v5, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    sub-int v2, v4, v5

    .line 115
    .local v2, "remainingInBlock":I
    if-ge v2, v3, :cond_2

    .line 116
    move v3, v2

    .line 118
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget-object v4, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->chars:[C

    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget v5, v5, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    invoke-static {p1, v0, v4, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 119
    add-int/2addr v0, v3

    .line 120
    sub-int/2addr v1, v3

    .line 121
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->current:Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    iget v5, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    add-int/2addr v5, v3

    iput v5, v4, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    goto :goto_0
.end method

.method blockIndex(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 74
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockSize:I

    div-int v0, p1, v0

    return v0
.end method

.method public charAt(I)C
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 152
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blocks:Ljava/util/List;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockIndex(I)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    .line 153
    .local v0, "b":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;
    iget-object v1, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->chars:[C

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->indexInBlock(I)I

    move-result v2

    aget-char v1, v1, v2

    return v1
.end method

.method flush(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    const/4 v0, 0x0

    .line 189
    .local v0, "oos":Ljava/io/ObjectOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, p1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    .end local v0    # "oos":Ljava/io/ObjectOutputStream;
    .local v1, "oos":Ljava/io/ObjectOutputStream;
    :try_start_1
    invoke-virtual {v1, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 191
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 193
    if-eqz v1, :cond_0

    .line 194
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    .line 197
    :cond_0
    return-void

    .line 192
    .end local v1    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v0    # "oos":Ljava/io/ObjectOutputStream;
    :catchall_0
    move-exception v2

    .line 193
    :goto_0
    if-eqz v0, :cond_1

    .line 194
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V

    .line 196
    :cond_1
    throw v2

    .line 192
    .end local v0    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v1    # "oos":Ljava/io/ObjectOutputStream;
    :catchall_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v0    # "oos":Ljava/io/ObjectOutputStream;
    goto :goto_0
.end method

.method indexInBlock(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 78
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockSize:I

    rem-int v0, p1, v0

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->length:I

    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .locals 8
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 163
    sub-int v5, p2, p1

    .line 164
    .local v5, "remaining":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 165
    .local v6, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blockIndex(I)I

    move-result v1

    .line 166
    .local v1, "blockIdx":I
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->indexInBlock(I)I

    move-result v3

    .local v3, "indexInBlock":I
    move v2, v1

    .line 167
    .end local v1    # "blockIdx":I
    .local v2, "blockIdx":I
    :goto_0
    if-gtz v5, :cond_0

    .line 174
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 168
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blocks:Ljava/util/List;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "blockIdx":I
    .restart local v1    # "blockIdx":I
    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    .line 169
    .local v0, "b":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;
    iget v7, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    sub-int/2addr v7, v3

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 170
    .local v4, "numToAppend":I
    iget-object v7, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->chars:[C

    invoke-virtual {v6, v7, v3, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 171
    sub-int/2addr v5, v4

    .line 172
    const/4 v3, 0x0

    move v2, v1

    .end local v1    # "blockIdx":I
    .restart local v2    # "blockIdx":I
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 180
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;->blocks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 183
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 180
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;

    .line 181
    .local v0, "b":Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;
    iget-object v3, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->chars:[C

    const/4 v4, 0x0

    iget v5, v0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    invoke-virtual {v1, v3, v4, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
