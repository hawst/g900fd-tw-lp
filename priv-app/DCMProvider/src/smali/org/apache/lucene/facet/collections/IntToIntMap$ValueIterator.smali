.class final Lorg/apache/lucene/facet/collections/IntToIntMap$ValueIterator;
.super Ljava/lang/Object;
.source "IntToIntMap.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/IntIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/IntToIntMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ValueIterator"
.end annotation


# instance fields
.field private iterator:Lorg/apache/lucene/facet/collections/IntIterator;

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/IntToIntMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/collections/IntToIntMap;)V
    .locals 1

    .prologue
    .line 134
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToIntMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToIntMap;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    .line 134
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()I
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToIntMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntToIntMap;->values:[I

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->remove()V

    .line 149
    return-void
.end method
