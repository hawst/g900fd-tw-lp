.class Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;
.super Ljava/lang/Object;
.source "DrillSidewaysScorer.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/DrillSidewaysScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DocsEnumsAndFreq"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;",
        ">;"
    }
.end annotation


# instance fields
.field dim:Ljava/lang/String;

.field docsEnums:[Lorg/apache/lucene/index/DocsEnum;

.field freq:I

.field sidewaysCollector:Lorg/apache/lucene/search/Collector;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->compareTo(Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;

    .prologue
    .line 636
    iget v0, p0, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->freq:I

    iget v1, p1, Lorg/apache/lucene/facet/search/DrillSidewaysScorer$DocsEnumsAndFreq;->freq:I

    sub-int/2addr v0, v1

    return v0
.end method
