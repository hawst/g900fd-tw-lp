.class Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;
.super Ljava/lang/Object;
.source "ScoredDocIdsUtils.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/ScoredDocIDs;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/util/ScoredDocIdsUtils;->createScoredDocIDsSubset(Lorg/apache/lucene/facet/search/ScoredDocIDs;[I)Lorg/apache/lucene/facet/search/ScoredDocIDs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$docids:[I

.field private final synthetic val$scores:[F

.field private final synthetic val$size:I


# direct methods
.method constructor <init>(I[I[F)V
    .locals 0

    .prologue
    .line 1
    iput p1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;->val$size:I

    iput-object p2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;->val$docids:[I

    iput-object p3, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;->val$scores:[F

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDocIDs()Lorg/apache/lucene/search/DocIdSet;
    .locals 3

    .prologue
    .line 134
    new-instance v0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1;

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;->val$size:I

    iget-object v2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;->val$docids:[I

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$1;-><init>(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;I[I)V

    return-object v0
.end method

.method public iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
    .locals 4

    .prologue
    .line 176
    new-instance v0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;->val$size:I

    iget-object v2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;->val$scores:[F

    iget-object v3, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;->val$docids:[I

    invoke-direct {v0, p0, v1, v2, v3}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;-><init>(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;I[F[I)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;->val$size:I

    return v0
.end method
