.class public abstract Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;
.super Lorg/apache/lucene/facet/search/FacetResultsHandler;
.source "DepthOneFacetResultsHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler$FacetResultNodeQueue;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 2
    .param p1, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p2, "facetRequest"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/facet/search/FacetResultsHandler;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 64
    sget-boolean v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lorg/apache/lucene/facet/search/FacetRequest;->getDepth()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this handler only computes the top-K facets at depth 1"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 65
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p2, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    invoke-virtual {p2}, Lorg/apache/lucene/facet/search/FacetRequest;->getNumLabel()I

    move-result v1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this handler always labels all top-K results"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 66
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lorg/apache/lucene/facet/search/FacetRequest;->getSortOrder()Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;->DESCENDING:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this handler always sorts results in descending order"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 67
    :cond_2
    return-void
.end method


# virtual methods
.method protected abstract addSiblings(I[ILorg/apache/lucene/util/PriorityQueue;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[I",
            "Lorg/apache/lucene/util/PriorityQueue",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;)I"
        }
    .end annotation
.end method

.method protected abstract addSiblings(I[ILjava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[I",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final compute()Lorg/apache/lucene/facet/search/FacetResult;
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getParallelTaxonomyArrays()Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;

    move-result-object v4

    .line 91
    .local v4, "arrays":Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;
    invoke-virtual {v4}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;->children()[I

    move-result-object v6

    .line 92
    .local v6, "children":[I
    invoke-virtual {v4}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;->siblings()[I

    move-result-object v15

    .line 94
    .local v15, "siblings":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v14

    .line 96
    .local v14, "rootOrd":I
    new-instance v13, Lorg/apache/lucene/facet/search/FacetResultNode;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->valueOf(I)D

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-direct {v13, v14, v0, v1}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    .line 97
    .local v13, "root":Lorg/apache/lucene/facet/search/FacetResultNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v0, v13, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 98
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getSize()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_0

    .line 100
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v9, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    aget v5, v6, v14

    .line 102
    .local v5, "child":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v15, v9}, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->addSiblings(I[ILjava/util/ArrayList;)V

    .line 103
    new-instance v18, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler$1;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler$1;-><init>(Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;)V

    move-object/from16 v0, v18

    invoke-static {v9, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 114
    iput-object v9, v13, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    .line 115
    new-instance v18, Lorg/apache/lucene/facet/search/FacetResult;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    move-object/from16 v19, v0

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v13, v2}, Lorg/apache/lucene/facet/search/FacetResult;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    .line 135
    .end local v5    # "child":I
    .end local v9    # "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    :goto_0
    return-object v18

    .line 119
    :cond_0
    new-instance v11, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler$FacetResultNodeQueue;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v11, v0, v1}, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler$FacetResultNodeQueue;-><init>(IZ)V

    .line 120
    .local v11, "pq":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    aget v18, v6, v14

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1, v15, v11}, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->addSiblings(I[ILorg/apache/lucene/util/PriorityQueue;)I

    move-result v10

    .line 123
    .local v10, "numSiblings":I
    invoke-virtual {v11}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v12

    .line 124
    .local v12, "pqsize":I
    if-ge v10, v12, :cond_1

    move/from16 v16, v10

    .line 125
    .local v16, "size":I
    :goto_1
    sub-int v7, v12, v16

    .local v7, "i":I
    :goto_2
    if-gtz v7, :cond_2

    .line 128
    move/from16 v0, v16

    new-array v0, v0, [Lorg/apache/lucene/facet/search/FacetResultNode;

    move-object/from16 v17, v0

    .line 129
    .local v17, "subResults":[Lorg/apache/lucene/facet/search/FacetResultNode;
    add-int/lit8 v7, v16, -0x1

    :goto_3
    if-gez v7, :cond_3

    .line 134
    invoke-static/range {v17 .. v17}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    .line 135
    new-instance v18, Lorg/apache/lucene/facet/search/FacetResult;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v13, v10}, Lorg/apache/lucene/facet/search/FacetResult;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    goto :goto_0

    .end local v7    # "i":I
    .end local v16    # "size":I
    .end local v17    # "subResults":[Lorg/apache/lucene/facet/search/FacetResultNode;
    :cond_1
    move/from16 v16, v12

    .line 124
    goto :goto_1

    .line 125
    .restart local v7    # "i":I
    .restart local v16    # "size":I
    :cond_2
    invoke-virtual {v11}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    add-int/lit8 v7, v7, -0x1

    goto :goto_2

    .line 130
    .restart local v17    # "subResults":[Lorg/apache/lucene/facet/search/FacetResultNode;
    :cond_3
    invoke-virtual {v11}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 131
    .local v8, "node":Lorg/apache/lucene/facet/search/FacetResultNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    move-object/from16 v18, v0

    iget v0, v8, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getPath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v8, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 132
    aput-object v8, v17, v7

    .line 129
    add-int/lit8 v7, v7, -0x1

    goto :goto_3
.end method

.method protected abstract valueOf(I)D
.end method
