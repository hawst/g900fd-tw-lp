.class public Lorg/apache/lucene/facet/util/TaxonomyMergeUtils;
.super Ljava/lang/Object;
.source "TaxonomyMergeUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static merge(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 9
    .param p0, "srcIndexDir"    # Lorg/apache/lucene/store/Directory;
    .param p1, "srcTaxDir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "map"    # Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;
    .param p3, "destIndexWriter"    # Lorg/apache/lucene/index/IndexWriter;
    .param p4, "destTaxWriter"    # Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;
    .param p5, "params"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p4, p1, p2}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->addTaxonomy(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;)V

    .line 47
    invoke-interface {p2}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;->getMap()[I

    move-result-object v3

    .line 48
    .local v3, "ordinalMap":[I
    const/4 v6, -0x1

    invoke-static {p0, v6}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;I)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v4

    .line 49
    .local v4, "reader":Lorg/apache/lucene/index/DirectoryReader;
    invoke-virtual {v4}, Lorg/apache/lucene/index/DirectoryReader;->leaves()Ljava/util/List;

    move-result-object v1

    .line 50
    .local v1, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 51
    .local v2, "numReaders":I
    new-array v5, v2, [Lorg/apache/lucene/index/AtomicReader;

    .line 52
    .local v5, "wrappedLeaves":[Lorg/apache/lucene/index/AtomicReader;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_0

    .line 56
    const/4 v6, 0x1

    :try_start_0
    new-array v6, v6, [Lorg/apache/lucene/index/IndexReader;

    const/4 v7, 0x0

    new-instance v8, Lorg/apache/lucene/index/MultiReader;

    invoke-direct {v8, v5}, Lorg/apache/lucene/index/MultiReader;-><init>([Lorg/apache/lucene/index/IndexReader;)V

    aput-object v8, v6, v7

    invoke-virtual {p3, v6}, Lorg/apache/lucene/index/IndexWriter;->addIndexes([Lorg/apache/lucene/index/IndexReader;)V

    .line 59
    invoke-virtual {p4}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->commit()V

    .line 60
    invoke-virtual {p3}, Lorg/apache/lucene/index/IndexWriter;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    invoke-virtual {v4}, Lorg/apache/lucene/index/DirectoryReader;->close()V

    .line 64
    return-void

    .line 53
    :cond_0
    new-instance v7, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    invoke-direct {v7, v6, v3, p5}, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;-><init>(Lorg/apache/lucene/index/AtomicReader;[ILorg/apache/lucene/facet/params/FacetIndexingParams;)V

    aput-object v7, v5, v0

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v6

    .line 62
    invoke-virtual {v4}, Lorg/apache/lucene/index/DirectoryReader;->close()V

    .line 63
    throw v6
.end method
