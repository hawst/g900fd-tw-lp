.class public Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;
.super Lorg/apache/lucene/search/ReferenceManager;
.source "SearcherTaxonomyManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/ReferenceManager",
        "<",
        "Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;",
        ">;"
    }
.end annotation


# instance fields
.field private final searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

.field private final taxoEpoch:J

.field private final taxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexWriter;ZLorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;)V
    .locals 4
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "applyAllDeletes"    # Z
    .param p3, "searcherFactory"    # Lorg/apache/lucene/search/SearcherFactory;
    .param p4, "taxoWriter"    # Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;-><init>()V

    .line 63
    if-nez p3, :cond_0

    .line 64
    new-instance p3, Lorg/apache/lucene/search/SearcherFactory;

    .end local p3    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    invoke-direct {p3}, Lorg/apache/lucene/search/SearcherFactory;-><init>()V

    .line 66
    .restart local p3    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    :cond_0
    iput-object p3, p0, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    .line 67
    iput-object p4, p0, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    .line 68
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    invoke-direct {v0, p4}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;-><init>(Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;)V

    .line 69
    .local v0, "taxoReader":Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
    new-instance v1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;

    invoke-static {p1, p2}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v2

    invoke-static {p3, v2}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v2

    .line 70
    invoke-direct {v1, v2, v0}, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;-><init>(Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;)V

    .line 69
    iput-object v1, p0, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;->current:Ljava/lang/Object;

    .line 71
    invoke-virtual {p4}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->getTaxonomyEpoch()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;->taxoEpoch:J

    .line 72
    return-void
.end method


# virtual methods
.method protected bridge synthetic decRef(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;->decRef(Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;)V

    return-void
.end method

.method protected decRef(Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;)V
    .locals 1
    .param p1, "ref"    # Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 85
    iget-object v0, p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->decRef()V

    .line 86
    return-void
.end method

.method protected bridge synthetic refreshIfNeeded(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;->refreshIfNeeded(Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;)Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;

    move-result-object v0

    return-object v0
.end method

.method protected refreshIfNeeded(Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;)Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;
    .locals 8
    .param p1, "ref"    # Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    iget-object v3, p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v3}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    .line 106
    .local v1, "r":Lorg/apache/lucene/index/IndexReader;
    check-cast v1, Lorg/apache/lucene/index/DirectoryReader;

    .end local v1    # "r":Lorg/apache/lucene/index/IndexReader;
    invoke-static {v1}, Lorg/apache/lucene/index/DirectoryReader;->openIfChanged(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    .line 107
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    if-nez v0, :cond_0

    .line 108
    const/4 v3, 0x0

    .line 119
    :goto_0
    return-object v3

    .line 110
    :cond_0
    iget-object v3, p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    invoke-static {v3}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->openIfChanged(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    .line 111
    .local v2, "tr":Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
    if-nez v2, :cond_2

    .line 112
    iget-object v3, p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    invoke-virtual {v3}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->incRef()V

    .line 113
    iget-object v2, p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    .line 119
    :cond_1
    new-instance v3, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;

    iget-object v4, p0, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    invoke-static {v4, v0}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;-><init>(Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;)V

    goto :goto_0

    .line 114
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->getTaxonomyEpoch()J

    move-result-wide v4

    iget-wide v6, p0, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;->taxoEpoch:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/io/Closeable;

    const/4 v4, 0x0

    .line 115
    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 116
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "DirectoryTaxonomyWriter.replaceTaxonomy was called, which is not allowed when using SearcherTaxonomyManager"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method protected bridge synthetic tryIncRef(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager;->tryIncRef(Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;)Z

    move-result v0

    return v0
.end method

.method protected tryIncRef(Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;)Z
    .locals 1
    .param p1, "ref"    # Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->tryIncRef()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->tryIncRef()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    .line 94
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/facet/search/SearcherTaxonomyManager$SearcherAndTaxonomy;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 97
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
