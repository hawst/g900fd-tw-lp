.class public Lorg/apache/lucene/facet/collections/LRUHashMap;
.super Ljava/util/LinkedHashMap;
.source "LRUHashMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private maxSize:I


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .param p1, "maxSize"    # I

    .prologue
    .line 70
    .local p0, "this":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<TK;TV;>;"
    const/16 v0, 0x10

    const/high16 v1, 0x3f400000    # 0.75f

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    .line 71
    iput p1, p0, Lorg/apache/lucene/facet/collections/LRUHashMap;->maxSize:I

    .line 72
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/LRUHashMap;->clone()Lorg/apache/lucene/facet/collections/LRUHashMap;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/facet/collections/LRUHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/facet/collections/LRUHashMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 108
    .local p0, "this":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<TK;TV;>;"
    invoke-super {p0}, Ljava/util/LinkedHashMap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/collections/LRUHashMap;

    return-object v0
.end method

.method public getMaxSize()I
    .locals 1

    .prologue
    .line 78
    .local p0, "this":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<TK;TV;>;"
    iget v0, p0, Lorg/apache/lucene/facet/collections/LRUHashMap;->maxSize:I

    return v0
.end method

.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "this":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<TK;TV;>;"
    .local p1, "eldest":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<TK;TV;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/LRUHashMap;->size()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/facet/collections/LRUHashMap;->maxSize:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMaxSize(I)V
    .locals 0
    .param p1, "maxSize"    # I

    .prologue
    .line 92
    .local p0, "this":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<TK;TV;>;"
    iput p1, p0, Lorg/apache/lucene/facet/collections/LRUHashMap;->maxSize:I

    .line 93
    return-void
.end method
