.class public Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;
.super Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;
.source "TopKFacetResultsHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;
    }
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 0
    .param p1, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p2, "facetRequest"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 47
    return-void
.end method

.method private heapDescendants(ILorg/apache/lucene/facet/search/Heap;Lorg/apache/lucene/facet/search/FacetResultNode;I)I
    .locals 20
    .param p1, "ordinal"    # I
    .param p3, "parentResultNode"    # Lorg/apache/lucene/facet/search/FacetResultNode;
    .param p4, "offset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lorg/apache/lucene/facet/search/Heap",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            "I)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    .local p2, "pq":Lorg/apache/lucene/facet/search/Heap;, "Lorg/apache/lucene/facet/search/Heap<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v9, v0, Lorg/apache/lucene/facet/search/FacetArrays;->arrayLength:I

    .line 115
    .local v9, "partitionSize":I
    add-int v6, p4, v9

    .line 116
    .local v6, "endOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getParallelTaxonomyArrays()Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;

    move-result-object v3

    .line 117
    .local v3, "childrenArray":Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;
    invoke-virtual {v3}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;->children()[I

    move-result-object v2

    .line 118
    .local v2, "children":[I
    invoke-virtual {v3}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;->siblings()[I

    move-result-object v12

    .line 119
    .local v12, "siblings":[I
    const/4 v11, 0x0

    .line 120
    .local v11, "reusable":Lorg/apache/lucene/facet/search/FacetResultNode;
    const/4 v7, 0x0

    .line 121
    .local v7, "localDepth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/facet/search/FacetRequest;->getDepth()I

    move-result v5

    .line 122
    .local v5, "depth":I
    const/16 v17, 0x7fff

    move/from16 v0, v17

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v17

    add-int/lit8 v17, v17, 0x2

    move/from16 v0, v17

    new-array v8, v0, [I

    .line 123
    .local v8, "ordinalStack":[I
    const/4 v4, 0x0

    .line 127
    .local v4, "childrenCounter":I
    aget v16, v2, p1

    .line 128
    .local v16, "yc":I
    :goto_0
    move/from16 v0, v16

    if-ge v0, v6, :cond_0

    .line 133
    add-int/lit8 v7, v7, 0x1

    aput v16, v8, v7

    .line 147
    :goto_1
    if-gtz v7, :cond_1

    .line 189
    return v4

    .line 129
    :cond_0
    aget v16, v12, v16

    goto :goto_0

    .line 148
    :cond_1
    aget v13, v8, v7

    .line 149
    .local v13, "tosOrdinal":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v13, v0, :cond_2

    .line 152
    add-int/lit8 v7, v7, -0x1

    .line 154
    aget v17, v8, v7

    aget v17, v12, v17

    aput v17, v8, v7

    goto :goto_1

    .line 159
    :cond_2
    move/from16 v0, p4

    if-lt v13, v0, :cond_3

    .line 160
    rem-int v10, v13, v9

    .line 161
    .local v10, "relativeOrdinal":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v10}, Lorg/apache/lucene/facet/search/FacetRequest;->getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D

    move-result-wide v14

    .line 162
    .local v14, "value":D
    const-wide/16 v18, 0x0

    cmpl-double v17, v14, v18

    if-eqz v17, :cond_3

    invoke-static {v14, v15}, Ljava/lang/Double;->isNaN(D)Z

    move-result v17

    if-nez v17, :cond_3

    .line 164
    if-nez v11, :cond_4

    .line 165
    new-instance v11, Lorg/apache/lucene/facet/search/FacetResultNode;

    .end local v11    # "reusable":Lorg/apache/lucene/facet/search/FacetResultNode;
    invoke-direct {v11, v13, v14, v15}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    .line 173
    .restart local v11    # "reusable":Lorg/apache/lucene/facet/search/FacetResultNode;
    :goto_2
    add-int/lit8 v4, v4, 0x1

    .line 174
    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Lorg/apache/lucene/facet/search/Heap;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "reusable":Lorg/apache/lucene/facet/search/FacetResultNode;
    check-cast v11, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 177
    .end local v10    # "relativeOrdinal":I
    .end local v14    # "value":D
    .restart local v11    # "reusable":Lorg/apache/lucene/facet/search/FacetResultNode;
    :cond_3
    if-ge v7, v5, :cond_6

    .line 179
    aget v16, v2, v13

    .line 180
    :goto_3
    move/from16 v0, v16

    if-ge v0, v6, :cond_5

    .line 183
    add-int/lit8 v7, v7, 0x1

    aput v16, v8, v7

    goto :goto_1

    .line 168
    .restart local v10    # "relativeOrdinal":I
    .restart local v14    # "value":D
    :cond_4
    iput v13, v11, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    .line 169
    iput-wide v14, v11, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    .line 170
    iget-object v0, v11, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->clear()V

    .line 171
    const/16 v17, 0x0

    move-object/from16 v0, v17

    iput-object v0, v11, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    goto :goto_2

    .line 181
    .end local v10    # "relativeOrdinal":I
    .end local v14    # "value":D
    :cond_5
    aget v16, v12, v16

    goto :goto_3

    .line 185
    :cond_6
    add-int/lit8 v7, v7, 0x1

    const/16 v17, -0x1

    aput v17, v8, v7

    goto :goto_1
.end method


# virtual methods
.method public fetchPartitionResult(I)Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    .locals 11
    .param p1, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    const/4 v4, 0x0

    .line 54
    .local v4, "res":Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;
    iget-object v8, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v9, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    iget-object v9, v9, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v8, v9}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v1

    .line 55
    .local v1, "ordinal":I
    const/4 v8, -0x1

    if-eq v1, v8, :cond_1

    .line 56
    const-wide/16 v6, 0x0

    .line 57
    .local v6, "value":D
    iget-object v8, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    invoke-virtual {p0, v1, v8, p1}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->isSelfPartition(ILorg/apache/lucene/facet/search/FacetArrays;I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 58
    iget-object v8, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    iget v3, v8, Lorg/apache/lucene/facet/search/FacetArrays;->arrayLength:I

    .line 59
    .local v3, "partitionSize":I
    iget-object v8, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    iget-object v9, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    rem-int v10, v1, v3

    invoke-virtual {v8, v9, v10}, Lorg/apache/lucene/facet/search/FacetRequest;->getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D

    move-result-wide v6

    .line 62
    .end local v3    # "partitionSize":I
    :cond_0
    new-instance v2, Lorg/apache/lucene/facet/search/FacetResultNode;

    invoke-direct {v2, v1, v6, v7}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    .line 64
    .local v2, "parentResultNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    iget-object v8, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    invoke-static {v8}, Lorg/apache/lucene/facet/util/ResultSortUtils;->createSuitableHeap(Lorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/search/Heap;

    move-result-object v0

    .line 65
    .local v0, "heap":Lorg/apache/lucene/facet/search/Heap;, "Lorg/apache/lucene/facet/search/Heap<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    invoke-direct {p0, v1, v0, v2, p1}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->heapDescendants(ILorg/apache/lucene/facet/search/Heap;Lorg/apache/lucene/facet/search/FacetResultNode;I)I

    move-result v5

    .line 66
    .local v5, "totalFacets":I
    new-instance v4, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;

    .end local v4    # "res":Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;
    iget-object v8, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    invoke-direct {v4, v8, v2, v5}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    .line 67
    .restart local v4    # "res":Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;
    invoke-virtual {v4, v0}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;->setHeap(Lorg/apache/lucene/facet/search/Heap;)V

    .line 69
    .end local v0    # "heap":Lorg/apache/lucene/facet/search/Heap;, "Lorg/apache/lucene/facet/search/Heap<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    .end local v2    # "parentResultNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    .end local v5    # "totalFacets":I
    .end local v6    # "value":D
    :cond_1
    return-object v4
.end method

.method public labelResult(Lorg/apache/lucene/facet/search/FacetResult;)V
    .locals 6
    .param p1, "facetResult"    # Lorg/apache/lucene/facet/search/FacetResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 228
    if-eqz p1, :cond_0

    .line 229
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetResult;->getFacetResultNode()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v0

    .line 230
    .local v0, "facetResultNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    if-eqz v0, :cond_0

    .line 231
    iget-object v3, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget v4, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getPath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v3

    iput-object v3, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 232
    iget-object v3, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    invoke-virtual {v3}, Lorg/apache/lucene/facet/search/FacetRequest;->getNumLabel()I

    move-result v2

    .line 233
    .local v2, "num2label":I
    iget-object v3, v0, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 241
    .end local v0    # "facetResultNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    .end local v2    # "num2label":I
    :cond_0
    return-void

    .line 233
    .restart local v0    # "facetResultNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    .restart local v2    # "num2label":I
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 234
    .local v1, "frn":Lorg/apache/lucene/facet/search/FacetResultNode;
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_0

    .line 237
    iget-object v4, p0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget v5, v1, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getPath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v4

    iput-object v4, v1, Lorg/apache/lucene/facet/search/FacetResultNode;->label:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    goto :goto_0
.end method

.method public varargs mergeResults([Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;)Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    .locals 18
    .param p1, "tmpResults"    # [Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    iget-object v12, v12, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v11, v12}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v5

    .line 77
    .local v5, "ordinal":I
    new-instance v7, Lorg/apache/lucene/facet/search/FacetResultNode;

    const-wide/16 v12, 0x0

    invoke-direct {v7, v5, v12, v13}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    .line 79
    .local v7, "resNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    const/4 v10, 0x0

    .line 80
    .local v10, "totalFacets":I
    const/4 v3, 0x0

    .line 83
    .local v3, "heap":Lorg/apache/lucene/facet/search/Heap;, "Lorg/apache/lucene/facet/search/Heap<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    move-object/from16 v0, p1

    array-length v13, v0

    const/4 v11, 0x0

    move v12, v11

    :goto_0
    if-lt v12, v13, :cond_0

    .line 100
    new-instance v6, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    invoke-direct {v6, v11, v7, v10}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    .line 101
    .local v6, "res":Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;
    invoke-virtual {v6, v3}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;->setHeap(Lorg/apache/lucene/facet/search/Heap;)V

    .line 102
    return-object v6

    .line 83
    .end local v6    # "res":Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;
    :cond_0
    aget-object v8, p1, v12

    .local v8, "tmpFres":Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    move-object v2, v8

    .line 85
    check-cast v2, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;

    .line 86
    .local v2, "fres":Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;
    invoke-virtual {v2}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;->getNumValidDescendants()I

    move-result v11

    add-int/2addr v10, v11

    .line 88
    iget-wide v14, v7, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    invoke-virtual {v2}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;->getFacetResultNode()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v11

    iget-wide v0, v11, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    iput-wide v14, v7, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    .line 89
    invoke-virtual {v2}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;->getHeap()Lorg/apache/lucene/facet/search/Heap;

    move-result-object v9

    .line 90
    .local v9, "tmpHeap":Lorg/apache/lucene/facet/search/Heap;, "Lorg/apache/lucene/facet/search/Heap<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    if-nez v3, :cond_2

    .line 91
    move-object v3, v9

    .line 83
    :cond_1
    add-int/lit8 v11, v12, 0x1

    move v12, v11

    goto :goto_0

    .line 95
    :cond_2
    invoke-interface {v9}, Lorg/apache/lucene/facet/search/Heap;->size()I

    move-result v4

    .local v4, "i":I
    :goto_1
    if-lez v4, :cond_1

    .line 96
    invoke-interface {v9}, Lorg/apache/lucene/facet/search/Heap;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/facet/search/FacetResultNode;

    invoke-interface {v3, v11}, Lorg/apache/lucene/facet/search/Heap;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    add-int/lit8 v4, v4, -0x1

    goto :goto_1
.end method

.method public rearrangeFacetResult(Lorg/apache/lucene/facet/search/FacetResult;)Lorg/apache/lucene/facet/search/FacetResult;
    .locals 9
    .param p1, "facetResult"    # Lorg/apache/lucene/facet/search/FacetResult;

    .prologue
    .line 210
    move-object v3, p1

    check-cast v3, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;

    .line 211
    .local v3, "res":Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;
    invoke-virtual {v3}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;->getHeap()Lorg/apache/lucene/facet/search/Heap;

    move-result-object v1

    .line 212
    .local v1, "heap":Lorg/apache/lucene/facet/search/Heap;, "Lorg/apache/lucene/facet/search/Heap<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    invoke-interface {v1}, Lorg/apache/lucene/facet/search/Heap;->clear()V

    .line 213
    invoke-virtual {v3}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;->getFacetResultNode()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v6

    .line 214
    .local v6, "topFrn":Lorg/apache/lucene/facet/search/FacetResultNode;
    iget-object v7, v6, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 217
    invoke-interface {v1}, Lorg/apache/lucene/facet/search/Heap;->size()I

    move-result v4

    .line 218
    .local v4, "size":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 219
    .local v5, "subResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    invoke-interface {v1}, Lorg/apache/lucene/facet/search/Heap;->size()I

    move-result v2

    .local v2, "i":I
    :goto_1
    if-gtz v2, :cond_1

    .line 222
    iput-object v5, v6, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    .line 223
    return-object v3

    .line 214
    .end local v2    # "i":I
    .end local v4    # "size":I
    .end local v5    # "subResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 215
    .local v0, "frn":Lorg/apache/lucene/facet/search/FacetResultNode;
    invoke-interface {v1, v0}, Lorg/apache/lucene/facet/search/Heap;->add(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 220
    .end local v0    # "frn":Lorg/apache/lucene/facet/search/FacetResultNode;
    .restart local v2    # "i":I
    .restart local v4    # "size":I
    .restart local v5    # "subResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    :cond_1
    const/4 v8, 0x0

    invoke-interface {v1}, Lorg/apache/lucene/facet/search/Heap;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/facet/search/FacetResultNode;

    invoke-virtual {v5, v8, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 219
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method public renderFacetResult(Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;)Lorg/apache/lucene/facet/search/FacetResult;
    .locals 7
    .param p1, "tmpResult"    # Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;

    .prologue
    .line 194
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;

    .line 195
    .local v2, "res":Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;
    if-eqz v2, :cond_1

    .line 196
    invoke-virtual {v2}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;->getHeap()Lorg/apache/lucene/facet/search/Heap;

    move-result-object v0

    .line 197
    .local v0, "heap":Lorg/apache/lucene/facet/search/Heap;, "Lorg/apache/lucene/facet/search/Heap<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    invoke-virtual {v2}, Lorg/apache/lucene/facet/search/TopKFacetResultsHandler$TopKFacetResult;->getFacetResultNode()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v3

    .line 198
    .local v3, "resNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    iget-object v4, v3, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    sget-object v5, Lorg/apache/lucene/facet/search/FacetResultNode;->EMPTY_SUB_RESULTS:Ljava/util/List;

    if-ne v4, v5, :cond_0

    .line 199
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v3, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    .line 201
    :cond_0
    invoke-interface {v0}, Lorg/apache/lucene/facet/search/Heap;->size()I

    move-result v1

    .local v1, "i":I
    :goto_0
    if-gtz v1, :cond_2

    .line 205
    .end local v0    # "heap":Lorg/apache/lucene/facet/search/Heap;, "Lorg/apache/lucene/facet/search/Heap<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    .end local v1    # "i":I
    .end local v3    # "resNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    :cond_1
    return-object v2

    .line 202
    .restart local v0    # "heap":Lorg/apache/lucene/facet/search/Heap;, "Lorg/apache/lucene/facet/search/Heap<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    .restart local v1    # "i":I
    .restart local v3    # "resNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    :cond_2
    iget-object v5, v3, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v0}, Lorg/apache/lucene/facet/search/Heap;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/facet/search/FacetResultNode;

    invoke-interface {v5, v6, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 201
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method
