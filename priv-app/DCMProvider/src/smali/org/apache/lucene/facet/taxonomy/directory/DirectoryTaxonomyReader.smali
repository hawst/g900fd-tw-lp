.class public Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
.super Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
.source "DirectoryTaxonomyReader.java"


# static fields
.field private static final DEFAULT_CACHE_VALUE:I = 0xfa0

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/facet/collections/LRUHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;"
        }
    .end annotation
.end field

.field private delimiter:C

.field private final indexReader:Lorg/apache/lucene/index/DirectoryReader;

.field private ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/facet/collections/LRUHashMap",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private volatile taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

.field private final taxoEpoch:J

.field private final taxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->logger:Ljava/util/logging/Logger;

    .line 56
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;)V
    .locals 3
    .param p1, "taxoWriter"    # Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v2, 0xfa0

    .line 118
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;-><init>()V

    .line 68
    const/16 v0, 0x1f

    iput-char v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->delimiter:C

    .line 119
    iput-object p1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    .line 120
    invoke-virtual {p1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->getTaxonomyEpoch()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoEpoch:J

    .line 121
    invoke-virtual {p1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->getInternalIndexWriter()Lorg/apache/lucene/index/IndexWriter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->openIndexReader(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    .line 125
    new-instance v0, Lorg/apache/lucene/facet/collections/LRUHashMap;

    invoke-direct {v0, v2}, Lorg/apache/lucene/facet/collections/LRUHashMap;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    .line 126
    new-instance v0, Lorg/apache/lucene/facet/collections/LRUHashMap;

    invoke-direct {v0, v2}, Lorg/apache/lucene/facet/collections/LRUHashMap;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    .line 127
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/index/DirectoryReader;Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;Lorg/apache/lucene/facet/collections/LRUHashMap;Lorg/apache/lucene/facet/collections/LRUHashMap;Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;)V
    .locals 3
    .param p1, "indexReader"    # Lorg/apache/lucene/index/DirectoryReader;
    .param p2, "taxoWriter"    # Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;
    .param p5, "taxoArrays"    # Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/DirectoryReader;",
            "Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;",
            "Lorg/apache/lucene/facet/collections/LRUHashMap",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            "Ljava/lang/Integer;",
            ">;",
            "Lorg/apache/lucene/facet/collections/LRUHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;",
            "Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p3, "ordinalCache":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<Lorg/apache/lucene/facet/taxonomy/CategoryPath;Ljava/lang/Integer;>;"
    .local p4, "categoryCache":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<Ljava/lang/Integer;Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    const/16 v2, 0xfa0

    .line 75
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;-><init>()V

    .line 68
    const/16 v0, 0x1f

    iput-char v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->delimiter:C

    .line 78
    iput-object p1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    .line 79
    iput-object p2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    .line 80
    if-nez p2, :cond_2

    const-wide/16 v0, -0x1

    :goto_0
    iput-wide v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoEpoch:J

    .line 83
    if-nez p3, :cond_0

    new-instance p3, Lorg/apache/lucene/facet/collections/LRUHashMap;

    .end local p3    # "ordinalCache":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<Lorg/apache/lucene/facet/taxonomy/CategoryPath;Ljava/lang/Integer;>;"
    invoke-direct {p3, v2}, Lorg/apache/lucene/facet/collections/LRUHashMap;-><init>(I)V

    :cond_0
    iput-object p3, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    .line 84
    if-nez p4, :cond_1

    new-instance p4, Lorg/apache/lucene/facet/collections/LRUHashMap;

    .end local p4    # "categoryCache":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<Ljava/lang/Integer;Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    invoke-direct {p4, v2}, Lorg/apache/lucene/facet/collections/LRUHashMap;-><init>(I)V

    :cond_1
    iput-object p4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    .line 86
    if-eqz p5, :cond_3

    new-instance v0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    invoke-direct {v0, p1, p5}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;)V

    :goto_1
    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    .line 87
    return-void

    .line 80
    .restart local p3    # "ordinalCache":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<Lorg/apache/lucene/facet/taxonomy/CategoryPath;Ljava/lang/Integer;>;"
    .restart local p4    # "categoryCache":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<Ljava/lang/Integer;Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    :cond_2
    invoke-virtual {p2}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->getTaxonomyEpoch()J

    move-result-wide v0

    goto :goto_0

    .line 86
    .end local p3    # "ordinalCache":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<Lorg/apache/lucene/facet/taxonomy/CategoryPath;Ljava/lang/Integer;>;"
    .end local p4    # "categoryCache":Lorg/apache/lucene/facet/collections/LRUHashMap;, "Lorg/apache/lucene/facet/collections/LRUHashMap<Ljava/lang/Integer;Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;)V
    .locals 3
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v2, 0xfa0

    .line 99
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;-><init>()V

    .line 68
    const/16 v0, 0x1f

    iput-char v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->delimiter:C

    .line 100
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->openIndexReader(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    .line 102
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoEpoch:J

    .line 106
    new-instance v0, Lorg/apache/lucene/facet/collections/LRUHashMap;

    invoke-direct {v0, v2}, Lorg/apache/lucene/facet/collections/LRUHashMap;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    .line 107
    new-instance v0, Lorg/apache/lucene/facet/collections/LRUHashMap;

    invoke-direct {v0, v2}, Lorg/apache/lucene/facet/collections/LRUHashMap;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    .line 108
    return-void
.end method

.method private declared-synchronized initTaxoArrays()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    if-nez v1, :cond_0

    .line 134
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    .line 135
    .local v0, "tmpArrays":Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    .end local v0    # "tmpArrays":Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    :cond_0
    monitor-exit p0

    return-void

    .line 130
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method protected doClose()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 141
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->close()V

    .line 142
    iput-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    .line 144
    iput-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    .line 145
    iput-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    .line 146
    return-void
.end method

.method protected bridge synthetic doOpenIfChanged()Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->doOpenIfChanged()Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    move-result-object v0

    return-object v0
.end method

.method protected doOpenIfChanged()Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v0, 0x0

    .line 162
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ensureOpen()V

    .line 165
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    invoke-static {v2}, Lorg/apache/lucene/index/DirectoryReader;->openIfChanged(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v1

    .line 166
    .local v1, "r2":Lorg/apache/lucene/index/DirectoryReader;
    if-nez v1, :cond_1

    .line 204
    :cond_0
    :goto_0
    return-object v0

    .line 171
    :cond_1
    const/4 v7, 0x0

    .line 173
    .local v7, "success":Z
    const/4 v6, 0x0

    .line 174
    .local v6, "recreated":Z
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    if-nez v2, :cond_4

    .line 176
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DirectoryReader;->getIndexCommit()Lorg/apache/lucene/index/IndexCommit;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexCommit;->getUserData()Ljava/util/Map;

    move-result-object v2

    const-string v3, "index.epoch"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 177
    .local v8, "t1":Ljava/lang/String;
    invoke-virtual {v1}, Lorg/apache/lucene/index/DirectoryReader;->getIndexCommit()Lorg/apache/lucene/index/IndexCommit;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexCommit;->getUserData()Ljava/util/Map;

    move-result-object v2

    const-string v3, "index.epoch"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 178
    .local v9, "t2":Ljava/lang/String;
    if-nez v8, :cond_3

    .line 179
    if-eqz v9, :cond_2

    .line 180
    const/4 v6, 0x1

    .line 195
    .end local v8    # "t1":Ljava/lang/String;
    .end local v9    # "t2":Ljava/lang/String;
    :cond_2
    :goto_1
    if-eqz v6, :cond_5

    .line 198
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;-><init>(Lorg/apache/lucene/index/DirectoryReader;Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;Lorg/apache/lucene/facet/collections/LRUHashMap;Lorg/apache/lucene/facet/collections/LRUHashMap;Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    .local v0, "newtr":Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
    :goto_2
    const/4 v7, 0x1

    .line 206
    if-nez v7, :cond_0

    new-array v2, v11, [Ljava/io/Closeable;

    .line 207
    aput-object v1, v2, v10

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0

    .line 182
    .end local v0    # "newtr":Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
    .restart local v8    # "t1":Ljava/lang/String;
    .restart local v9    # "t2":Ljava/lang/String;
    :cond_3
    :try_start_1
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 185
    const/4 v6, 0x1

    .line 187
    goto :goto_1

    .line 189
    .end local v8    # "t1":Ljava/lang/String;
    .end local v9    # "t2":Ljava/lang/String;
    :cond_4
    iget-wide v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoEpoch:J

    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    invoke-virtual {v4}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;->getTaxonomyEpoch()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 190
    const/4 v6, 0x1

    goto :goto_1

    .line 200
    :cond_5
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;

    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoWriter:Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;

    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;-><init>(Lorg/apache/lucene/index/DirectoryReader;Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;Lorg/apache/lucene/facet/collections/LRUHashMap;Lorg/apache/lucene/facet/collections/LRUHashMap;Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v0    # "newtr":Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
    goto :goto_2

    .line 205
    .end local v0    # "newtr":Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;
    :catchall_0
    move-exception v2

    .line 206
    if-nez v7, :cond_6

    new-array v3, v11, [Ljava/io/Closeable;

    .line 207
    aput-object v1, v3, v10

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 209
    :cond_6
    throw v2
.end method

.method public getCommitUserData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ensureOpen()V

    .line 241
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->getIndexCommit()Lorg/apache/lucene/index/IndexCommit;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getUserData()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method getInternalIndexReader()Lorg/apache/lucene/index/DirectoryReader;
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ensureOpen()V

    .line 226
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    return-object v0
.end method

.method public getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    .locals 9
    .param p1, "cp"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 246
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ensureOpen()V

    .line 247
    iget v4, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v4, :cond_1

    move v2, v3

    .line 287
    :cond_0
    :goto_0
    return v2

    .line 252
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    monitor-enter v4

    .line 253
    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    invoke-virtual {v5, p1}, Lorg/apache/lucene/facet/collections/LRUHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 254
    .local v1, "res":Ljava/lang/Integer;
    if-eqz v1, :cond_3

    .line 255
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DirectoryReader;->maxDoc()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 259
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    monitor-exit v4

    goto :goto_0

    .line 252
    .end local v1    # "res":Ljava/lang/Integer;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 265
    .restart local v1    # "res":Ljava/lang/Integer;
    :cond_2
    :try_start_1
    monitor-exit v4

    const/4 v2, -0x1

    goto :goto_0

    .line 252
    :cond_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 272
    const/4 v2, -0x1

    .line 273
    .local v2, "ret":I
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    const/4 v5, 0x0

    const-string v6, "$full_path$"

    new-instance v7, Lorg/apache/lucene/util/BytesRef;

    iget-char v8, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->delimiter:C

    invoke-virtual {p1, v8}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v4, v5, v6, v7, v3}, Lorg/apache/lucene/index/MultiFields;->getTermDocsEnum(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    .line 274
    .local v0, "docs":Lorg/apache/lucene/index/DocsEnum;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v3

    const v4, 0x7fffffff

    if-eq v3, v4, :cond_0

    .line 275
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v2

    .line 282
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    monitor-enter v4

    .line 283
    :try_start_2
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p1, v5}, Lorg/apache/lucene/facet/collections/LRUHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    monitor-exit v4

    goto :goto_0

    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3
.end method

.method public getParallelTaxonomyArrays()Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ensureOpen()V

    .line 232
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    if-nez v0, :cond_0

    .line 233
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->initTaxoArrays()V

    .line 235
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->taxoArrays:Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    return-object v0
.end method

.method public getPath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .locals 6
    .param p1, "ordinal"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ensureOpen()V

    .line 298
    if-ltz p1, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DirectoryReader;->maxDoc()I

    move-result v4

    if-lt p1, v4, :cond_1

    .line 299
    :cond_0
    const/4 v2, 0x0

    .line 318
    :goto_0
    return-object v2

    .line 304
    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 305
    .local v0, "catIDInteger":Ljava/lang/Integer;
    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    monitor-enter v5

    .line 306
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    invoke-virtual {v4, v0}, Lorg/apache/lucene/facet/collections/LRUHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 307
    .local v2, "res":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    if-eqz v2, :cond_2

    .line 308
    monitor-exit v5

    goto :goto_0

    .line 305
    .end local v2    # "res":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v2    # "res":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    :cond_2
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 312
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/index/DirectoryReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v1

    .line 313
    .local v1, "doc":Lorg/apache/lucene/document/Document;
    new-instance v3, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    const-string v4, "$full_path$"

    invoke-virtual {v1, v4}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-char v5, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->delimiter:C

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>(Ljava/lang/String;C)V

    .line 314
    .local v3, "ret":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    monitor-enter v5

    .line 315
    :try_start_2
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    invoke-virtual {v4, v0, v3}, Lorg/apache/lucene/facet/collections/LRUHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    monitor-exit v5

    move-object v2, v3

    .line 318
    goto :goto_0

    .line 314
    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v4
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 323
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ensureOpen()V

    .line 324
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->numDocs()I

    move-result v0

    return v0
.end method

.method protected openIndexReader(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method protected openIndexReader(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    invoke-static {p1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method public setCacheSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 337
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ensureOpen()V

    .line 338
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    monitor-enter v1

    .line 339
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->categoryCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/facet/collections/LRUHashMap;->setMaxSize(I)V

    .line 338
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    monitor-enter v1

    .line 342
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ordinalCache:Lorg/apache/lucene/facet/collections/LRUHashMap;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/facet/collections/LRUHashMap;->setMaxSize(I)V

    .line 341
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 344
    return-void

    .line 338
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 341
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public setDelimiter(C)V
    .locals 0
    .param p1, "delimiter"    # C

    .prologue
    .line 357
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ensureOpen()V

    .line 358
    iput-char p1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->delimiter:C

    .line 359
    return-void
.end method

.method public toString(I)Ljava/lang/String;
    .locals 8
    .param p1, "max"    # I

    .prologue
    .line 362
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->ensureOpen()V

    .line 363
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 364
    .local v3, "sb":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->indexReader:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DirectoryReader;->maxDoc()I

    move-result v5

    invoke-static {p1, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 365
    .local v4, "upperl":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v4, :cond_0

    .line 383
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 367
    :cond_0
    :try_start_0
    invoke-virtual {p0, v2}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->getPath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-result-object v0

    .line 368
    .local v0, "category":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    if-nez v0, :cond_2

    .line 369
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ": NULL!! \n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    .end local v0    # "category":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 372
    .restart local v0    # "category":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    :cond_2
    iget v5, v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v5, :cond_3

    .line 373
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ": EMPTY STRING!! \n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 377
    .end local v0    # "category":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    :catch_0
    move-exception v1

    .line 378
    .local v1, "e":Ljava/io/IOException;
    sget-object v5, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v5, v6}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 379
    sget-object v5, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyReader;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 376
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "category":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    :cond_3
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
