.class public final Lorg/apache/lucene/facet/encoding/DGapVInt8IntDecoder;
.super Lorg/apache/lucene/facet/encoding/IntDecoder;
.source "DGapVInt8IntDecoder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/IntDecoder;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V
    .locals 9
    .param p1, "buf"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "values"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 34
    const/4 v6, 0x0

    iput v6, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v6, p2, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 39
    iget-object v6, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v6, v6

    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v6, v7, :cond_0

    .line 40
    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v6

    new-array v6, v6, [I

    iput-object v6, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 45
    :cond_0
    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v4, v6, v7

    .line 46
    .local v4, "upto":I
    const/4 v5, 0x0

    .line 47
    .local v5, "value":I
    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 48
    .local v1, "offset":I
    const/4 v3, 0x0

    .local v3, "prev":I
    move v2, v1

    .line 49
    .end local v1    # "offset":I
    .local v2, "offset":I
    :goto_0
    if-lt v2, v4, :cond_1

    .line 60
    return-void

    .line 50
    :cond_1
    iget-object v6, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    aget-byte v0, v6, v2

    .line 51
    .local v0, "b":B
    if-ltz v0, :cond_2

    .line 52
    iget-object v6, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v7, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    shl-int/lit8 v8, v5, 0x7

    or-int/2addr v8, v0

    add-int/2addr v8, v3

    aput v8, v6, v7

    .line 53
    const/4 v5, 0x0

    .line 54
    iget-object v6, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v7, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    aget v3, v6, v7

    .line 55
    iget v6, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    move v2, v1

    .line 56
    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    goto :goto_0

    .line 57
    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    :cond_2
    shl-int/lit8 v6, v5, 0x7

    and-int/lit8 v7, v0, 0x7f

    or-int v5, v6, v7

    move v2, v1

    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const-string v0, "DGapVInt8"

    return-object v0
.end method
