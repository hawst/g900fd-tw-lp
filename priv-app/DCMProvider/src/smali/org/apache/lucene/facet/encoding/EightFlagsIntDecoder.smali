.class public Lorg/apache/lucene/facet/encoding/EightFlagsIntDecoder;
.super Lorg/apache/lucene/facet/encoding/IntDecoder;
.source "EightFlagsIntDecoder.java"


# static fields
.field private static final DECODE_TABLE:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 34
    const/16 v2, 0x100

    const/16 v3, 0x8

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[B

    sput-object v2, Lorg/apache/lucene/facet/encoding/EightFlagsIntDecoder;->DECODE_TABLE:[[B

    .line 38
    const/16 v0, 0x100

    .local v0, "i":I
    :cond_0
    if-nez v0, :cond_1

    .line 45
    return-void

    .line 39
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 40
    const/16 v1, 0x8

    .local v1, "j":I
    :goto_0
    if-eqz v1, :cond_0

    .line 41
    add-int/lit8 v1, v1, -0x1

    .line 42
    sget-object v2, Lorg/apache/lucene/facet/encoding/EightFlagsIntDecoder;->DECODE_TABLE:[[B

    aget-object v2, v2, v0

    ushr-int v3, v0, v1

    and-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/IntDecoder;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V
    .locals 12
    .param p1, "buf"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "values"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 49
    const/4 v9, 0x0

    iput v9, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v9, p2, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 50
    iget v9, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v10, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v7, v9, v10

    .line 51
    .local v7, "upto":I
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .local v3, "offset":I
    move v4, v3

    .line 52
    .end local v3    # "offset":I
    .local v4, "offset":I
    :goto_0
    if-lt v4, v7, :cond_1

    move v3, v4

    .line 85
    .end local v4    # "offset":I
    .restart local v3    # "offset":I
    :cond_0
    return-void

    .line 54
    .end local v3    # "offset":I
    .restart local v4    # "offset":I
    :cond_1
    iget-object v9, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "offset":I
    .restart local v3    # "offset":I
    aget-byte v9, v9, v4

    and-int/lit16 v2, v9, 0xff

    .line 55
    .local v2, "indicator":I
    const/4 v5, 0x0

    .line 57
    .local v5, "ordinal":I
    iget v9, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v1, v9, 0x8

    .line 58
    .local v1, "capacityNeeded":I
    iget-object v9, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v9, v9

    if-ge v9, v1, :cond_5

    .line 59
    invoke-virtual {p2, v1}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    move v6, v5

    .line 63
    .end local v5    # "ordinal":I
    .local v6, "ordinal":I
    :goto_1
    const/16 v9, 0x8

    if-ne v6, v9, :cond_2

    move v4, v3

    .end local v3    # "offset":I
    .restart local v4    # "offset":I
    goto :goto_0

    .line 64
    .end local v4    # "offset":I
    .restart local v3    # "offset":I
    :cond_2
    sget-object v9, Lorg/apache/lucene/facet/encoding/EightFlagsIntDecoder;->DECODE_TABLE:[[B

    aget-object v9, v9, v2

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "ordinal":I
    .restart local v5    # "ordinal":I
    aget-byte v9, v9, v6

    if-nez v9, :cond_4

    .line 65
    if-eq v3, v7, :cond_0

    .line 70
    const/4 v8, 0x0

    .line 72
    .local v8, "value":I
    :goto_2
    iget-object v9, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "offset":I
    .restart local v4    # "offset":I
    aget-byte v0, v9, v3

    .line 73
    .local v0, "b":B
    if-ltz v0, :cond_3

    .line 74
    iget-object v9, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v10, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    shl-int/lit8 v11, v8, 0x7

    or-int/2addr v11, v0

    add-int/lit8 v11, v11, 0x2

    aput v11, v9, v10

    move v6, v5

    .end local v5    # "ordinal":I
    .restart local v6    # "ordinal":I
    move v3, v4

    .line 75
    .end local v4    # "offset":I
    .restart local v3    # "offset":I
    goto :goto_1

    .line 77
    .end local v3    # "offset":I
    .end local v6    # "ordinal":I
    .restart local v4    # "offset":I
    .restart local v5    # "ordinal":I
    :cond_3
    shl-int/lit8 v9, v8, 0x7

    and-int/lit8 v10, v0, 0x7f

    or-int v8, v9, v10

    move v3, v4

    .line 71
    .end local v4    # "offset":I
    .restart local v3    # "offset":I
    goto :goto_2

    .line 81
    .end local v0    # "b":B
    .end local v8    # "value":I
    :cond_4
    iget-object v9, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v10, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    const/4 v11, 0x1

    aput v11, v9, v10

    :cond_5
    move v6, v5

    .end local v5    # "ordinal":I
    .restart local v6    # "ordinal":I
    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    const-string v0, "EightFlags(VInt8)"

    return-object v0
.end method
