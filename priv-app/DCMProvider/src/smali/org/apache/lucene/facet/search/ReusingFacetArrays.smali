.class public Lorg/apache/lucene/facet/search/ReusingFacetArrays;
.super Lorg/apache/lucene/facet/search/FacetArrays;
.source "ReusingFacetArrays.java"


# instance fields
.field private final arraysPool:Lorg/apache/lucene/facet/search/ArraysPool;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/search/ArraysPool;)V
    .locals 1
    .param p1, "arraysPool"    # Lorg/apache/lucene/facet/search/ArraysPool;

    .prologue
    .line 31
    iget v0, p1, Lorg/apache/lucene/facet/search/ArraysPool;->arrayLength:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/search/FacetArrays;-><init>(I)V

    .line 32
    iput-object p1, p0, Lorg/apache/lucene/facet/search/ReusingFacetArrays;->arraysPool:Lorg/apache/lucene/facet/search/ArraysPool;

    .line 33
    return-void
.end method


# virtual methods
.method protected doFree([F[I)V
    .locals 1
    .param p1, "floats"    # [F
    .param p2, "ints"    # [I

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/facet/search/ReusingFacetArrays;->arraysPool:Lorg/apache/lucene/facet/search/ArraysPool;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/facet/search/ArraysPool;->free([F)V

    .line 48
    iget-object v0, p0, Lorg/apache/lucene/facet/search/ReusingFacetArrays;->arraysPool:Lorg/apache/lucene/facet/search/ArraysPool;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/facet/search/ArraysPool;->free([I)V

    .line 49
    return-void
.end method

.method protected newFloatArray()[F
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lorg/apache/lucene/facet/search/ReusingFacetArrays;->arraysPool:Lorg/apache/lucene/facet/search/ArraysPool;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/search/ArraysPool;->allocateFloatArray()[F

    move-result-object v0

    return-object v0
.end method

.method protected newIntArray()[I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lorg/apache/lucene/facet/search/ReusingFacetArrays;->arraysPool:Lorg/apache/lucene/facet/search/ArraysPool;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/search/ArraysPool;->allocateIntArray()[I

    move-result-object v0

    return-object v0
.end method
