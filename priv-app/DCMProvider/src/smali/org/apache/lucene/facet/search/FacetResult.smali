.class public Lorg/apache/lucene/facet/search/FacetResult;
.super Ljava/lang/Object;
.source "FacetResult.java"


# instance fields
.field private final facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

.field private final numValidDescendants:I

.field private final rootNode:Lorg/apache/lucene/facet/search/FacetResultNode;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V
    .locals 0
    .param p1, "facetRequest"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p2, "rootNode"    # Lorg/apache/lucene/facet/search/FacetResultNode;
    .param p3, "numValidDescendants"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lorg/apache/lucene/facet/search/FacetResult;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    .line 33
    iput-object p2, p0, Lorg/apache/lucene/facet/search/FacetResult;->rootNode:Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 34
    iput p3, p0, Lorg/apache/lucene/facet/search/FacetResult;->numValidDescendants:I

    .line 35
    return-void
.end method


# virtual methods
.method public final getFacetRequest()Lorg/apache/lucene/facet/search/FacetRequest;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetResult;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    return-object v0
.end method

.method public final getFacetResultNode()Lorg/apache/lucene/facet/search/FacetResultNode;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetResult;->rootNode:Lorg/apache/lucene/facet/search/FacetResultNode;

    return-object v0
.end method

.method public final getNumValidDescendants()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lorg/apache/lucene/facet/search/FacetResult;->numValidDescendants:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const-string v0, ""

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/search/FacetResult;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v0, ""

    .line 72
    .local v0, "nl":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetResult;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    if-eqz v2, :cond_0

    .line 73
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Request: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 74
    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetResult;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    invoke-virtual {v3}, Lorg/apache/lucene/facet/search/FacetRequest;->toString()Ljava/lang/String;

    move-result-object v3

    .line 73
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    const-string v0, "\n"

    .line 79
    :cond_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Num valid Descendants (up to specified depth): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 80
    iget v3, p0, Lorg/apache/lucene/facet/search/FacetResult;->numValidDescendants:I

    .line 79
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 81
    const-string v0, "\n"

    .line 84
    iget-object v2, p0, Lorg/apache/lucene/facet/search/FacetResult;->rootNode:Lorg/apache/lucene/facet/search/FacetResultNode;

    if-eqz v2, :cond_1

    .line 85
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetResult;->rootNode:Lorg/apache/lucene/facet/search/FacetResultNode;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/facet/search/FacetResultNode;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
