.class public Lorg/apache/lucene/facet/search/DrillSideways;
.super Ljava/lang/Object;
.source "DrillSideways.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final searcher:Lorg/apache/lucene/search/IndexSearcher;

.field protected final taxoReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lorg/apache/lucene/facet/search/DrillSideways;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/search/DrillSideways;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V
    .locals 0
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .param p2, "taxoReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lorg/apache/lucene/facet/search/DrillSideways;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 74
    iput-object p2, p0, Lorg/apache/lucene/facet/search/DrillSideways;->taxoReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .line 75
    return-void
.end method

.method private static moveDrillDownOnlyClauses(Lorg/apache/lucene/facet/search/DrillDownQuery;Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/DrillDownQuery;
    .locals 14
    .param p0, "in"    # Lorg/apache/lucene/facet/search/DrillDownQuery;
    .param p1, "fsp"    # Lorg/apache/lucene/facet/params/FacetSearchParams;

    .prologue
    .line 84
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 85
    .local v4, "facetDims":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v11, p1, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_2

    .line 92
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/DrillDownQuery;->getBooleanQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v0

    .line 93
    .local v0, "clauses":[Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/DrillDownQuery;->getDims()Ljava/util/Map;

    move-result-object v2

    .line 96
    .local v2, "drillDownDims":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    array-length v11, v0

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v12

    if-ne v11, v12, :cond_4

    .line 97
    const/4 v10, 0x0

    .line 106
    .local v10, "startClause":I
    :goto_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 107
    .local v8, "nonFacetClauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v3, "facetClauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    move v6, v10

    .local v6, "i":I
    :goto_2
    array-length v11, v0

    if-lt v6, v11, :cond_6

    .line 118
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_1

    .line 119
    new-instance v7, Lorg/apache/lucene/search/BooleanQuery;

    const/4 v11, 0x1

    invoke-direct {v7, v11}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 120
    .local v7, "newBaseQuery":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v11, 0x1

    if-ne v10, v11, :cond_0

    .line 122
    const/4 v11, 0x0

    aget-object v11, v0, v11

    invoke-virtual {v11}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v11

    sget-object v12, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v7, v11, v12}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 124
    :cond_0
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_8

    .line 128
    new-instance p0, Lorg/apache/lucene/facet/search/DrillDownQuery;

    .end local p0    # "in":Lorg/apache/lucene/facet/search/DrillDownQuery;
    iget-object v11, p1, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {p0, v11, v7, v3}, Lorg/apache/lucene/facet/search/DrillDownQuery;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/search/Query;Ljava/util/List;)V

    .line 131
    .end local v7    # "newBaseQuery":Lorg/apache/lucene/search/BooleanQuery;
    :cond_1
    return-object p0

    .line 85
    .end local v0    # "clauses":[Lorg/apache/lucene/search/BooleanClause;
    .end local v2    # "drillDownDims":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v3    # "facetClauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    .end local v6    # "i":I
    .end local v8    # "nonFacetClauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    .end local v10    # "startClause":I
    .restart local p0    # "in":Lorg/apache/lucene/facet/search/DrillDownQuery;
    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 86
    .local v5, "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    iget-object v12, v5, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget v12, v12, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v12, :cond_3

    .line 87
    new-instance v11, Ljava/lang/IllegalArgumentException;

    const-string v12, "all FacetRequests must have CategoryPath with length > 0"

    invoke-direct {v11, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 89
    :cond_3
    iget-object v12, v5, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget-object v12, v12, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    const/4 v13, 0x0

    aget-object v12, v12, v13

    invoke-interface {v4, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 99
    .end local v5    # "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    .restart local v0    # "clauses":[Lorg/apache/lucene/search/BooleanClause;
    .restart local v2    # "drillDownDims":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_4
    sget-boolean v11, Lorg/apache/lucene/facet/search/DrillSideways;->$assertionsDisabled:Z

    if-nez v11, :cond_5

    array-length v11, v0

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v12

    add-int/lit8 v12, v12, 0x1

    if-eq v11, v12, :cond_5

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11

    .line 100
    :cond_5
    const/4 v10, 0x1

    .restart local v10    # "startClause":I
    goto :goto_1

    .line 109
    .restart local v3    # "facetClauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    .restart local v6    # "i":I
    .restart local v8    # "nonFacetClauses":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    :cond_6
    aget-object v11, v0, v6

    invoke-virtual {v11}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v9

    .line 110
    .local v9, "q":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0, v9}, Lorg/apache/lucene/facet/search/DrillDownQuery;->getDim(Lorg/apache/lucene/search/Query;)Ljava/lang/String;

    move-result-object v1

    .line 111
    .local v1, "dim":Ljava/lang/String;
    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_7

    .line 112
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 114
    :cond_7
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 124
    .end local v1    # "dim":Ljava/lang/String;
    .end local v9    # "q":Lorg/apache/lucene/search/Query;
    .restart local v7    # "newBaseQuery":Lorg/apache/lucene/search/BooleanQuery;
    :cond_8
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/search/Query;

    .line 125
    .restart local v9    # "q":Lorg/apache/lucene/search/Query;
    sget-object v12, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v7, v9, v12}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_3
.end method


# virtual methods
.method protected getDrillDownAccumulator(Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/FacetsAccumulator;
    .locals 2
    .param p1, "fsp"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 294
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillSideways;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/facet/search/DrillSideways;->taxoReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-static {p1, v0, v1}, Lorg/apache/lucene/facet/search/FacetsAccumulator;->create(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/FacetsAccumulator;

    move-result-object v0

    return-object v0
.end method

.method protected getDrillSidewaysAccumulator(Ljava/lang/String;Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/FacetsAccumulator;
    .locals 2
    .param p1, "dim"    # Ljava/lang/String;
    .param p2, "fsp"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 300
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DrillSideways;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/facet/search/DrillSideways;->taxoReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-static {p2, v0, v1}, Lorg/apache/lucene/facet/search/FacetsAccumulator;->create(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/FacetsAccumulator;

    move-result-object v0

    return-object v0
.end method

.method public search(Lorg/apache/lucene/facet/search/DrillDownQuery;Lorg/apache/lucene/search/Collector;Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;
    .locals 34
    .param p1, "query"    # Lorg/apache/lucene/facet/search/DrillDownQuery;
    .param p2, "hitCollector"    # Lorg/apache/lucene/search/Collector;
    .param p3, "fsp"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillDownQuery;->fip:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    move-object/from16 v30, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    move-object/from16 v31, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_0

    .line 144
    new-instance v30, Ljava/lang/IllegalArgumentException;

    const-string v31, "DrillDownQuery\'s FacetIndexingParams should match FacetSearchParams\'"

    invoke-direct/range {v30 .. v31}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 147
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lorg/apache/lucene/facet/search/DrillSideways;->moveDrillDownOnlyClauses(Lorg/apache/lucene/facet/search/DrillDownQuery;Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/DrillDownQuery;

    move-result-object p1

    .line 149
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/facet/search/DrillDownQuery;->getDims()Ljava/util/Map;

    move-result-object v11

    .line 151
    .local v11, "drillDownDims":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v11}, Ljava/util/Map;->isEmpty()Z

    move-result v30

    if-eqz v30, :cond_1

    .line 153
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/search/DrillSideways;->getDrillDownAccumulator(Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/FacetsAccumulator;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/facet/search/FacetsCollector;->create(Lorg/apache/lucene/facet/search/FacetsAccumulator;)Lorg/apache/lucene/facet/search/FacetsCollector;

    move-result-object v4

    .line 154
    .local v4, "c":Lorg/apache/lucene/facet/search/FacetsCollector;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSideways;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    move-object/from16 v30, v0

    const/16 v31, 0x2

    move/from16 v0, v31

    new-array v0, v0, [Lorg/apache/lucene/search/Collector;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    aput-object p2, v31, v32

    const/16 v32, 0x1

    aput-object v4, v31, v32

    invoke-static/range {v31 .. v31}, Lorg/apache/lucene/search/MultiCollector;->wrap([Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, p1

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Collector;)V

    .line 155
    new-instance v30, Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;

    invoke-virtual {v4}, Lorg/apache/lucene/facet/search/FacetsCollector;->getFacetResults()Ljava/util/List;

    move-result-object v31

    const/16 v32, 0x0

    invoke-direct/range {v30 .. v32}, Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;-><init>(Ljava/util/List;Lorg/apache/lucene/search/TopDocs;)V

    .line 250
    .end local v4    # "c":Lorg/apache/lucene/facet/search/FacetsCollector;
    :goto_0
    return-object v30

    .line 158
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/facet/search/DrillDownQuery;->getBooleanQuery()Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v7

    .line 159
    .local v7, "ddq":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v7}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v5

    .line 163
    .local v5, "clauses":[Lorg/apache/lucene/search/BooleanClause;
    array-length v0, v5

    move/from16 v30, v0

    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_2

    .line 166
    new-instance v3, Lorg/apache/lucene/search/MatchAllDocsQuery;

    invoke-direct {v3}, Lorg/apache/lucene/search/MatchAllDocsQuery;-><init>()V

    .line 167
    .local v3, "baseQuery":Lorg/apache/lucene/search/Query;
    const/16 v29, 0x0

    .line 174
    .local v29, "startClause":I
    :goto_1
    array-length v0, v5

    move/from16 v30, v0

    sub-int v30, v30, v29

    move/from16 v0, v30

    new-array v13, v0, [[Lorg/apache/lucene/index/Term;

    .line 175
    .local v13, "drillDownTerms":[[Lorg/apache/lucene/index/Term;
    move/from16 v18, v29

    .local v18, "i":I
    :goto_2
    array-length v0, v5

    move/from16 v30, v0

    move/from16 v0, v18

    move/from16 v1, v30

    if-lt v0, v1, :cond_4

    .line 193
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/search/DrillSideways;->getDrillDownAccumulator(Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/FacetsAccumulator;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/facet/search/FacetsCollector;->create(Lorg/apache/lucene/facet/search/FacetsAccumulator;)Lorg/apache/lucene/facet/search/FacetsCollector;

    move-result-object v10

    .line 195
    .local v10, "drillDownCollector":Lorg/apache/lucene/facet/search/FacetsCollector;
    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v30

    move/from16 v0, v30

    new-array v14, v0, [Lorg/apache/lucene/facet/search/FacetsCollector;

    .line 197
    .local v14, "drillSidewaysCollectors":[Lorg/apache/lucene/facet/search/FacetsCollector;
    const/16 v19, 0x0

    .line 198
    .local v19, "idx":I
    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v30

    invoke-interface/range {v30 .. v30}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v30

    :goto_3
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v31

    if-nez v31, :cond_a

    .line 212
    new-instance v16, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;

    move-object/from16 v0, v16

    invoke-direct {v0, v3, v10, v14, v13}, Lorg/apache/lucene/facet/search/DrillSidewaysQuery;-><init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Collector;[Lorg/apache/lucene/search/Collector;[[Lorg/apache/lucene/index/Term;)V

    .line 214
    .local v16, "dsq":Lorg/apache/lucene/facet/search/DrillSidewaysQuery;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/facet/search/DrillSideways;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Collector;)V

    .line 216
    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v23

    .line 217
    .local v23, "numDims":I
    move/from16 v0, v23

    new-array v15, v0, [Ljava/util/List;

    .line 218
    .local v15, "drillSidewaysResults":[Ljava/util/List;
    const/4 v12, 0x0

    .line 220
    .local v12, "drillDownResults":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 221
    .local v22, "mergedResults":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v30

    move/from16 v0, v30

    new-array v0, v0, [I

    move-object/from16 v26, v0

    .line 222
    .local v26, "requestUpto":[I
    const/16 v18, 0x0

    :goto_4
    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    move-object/from16 v30, v0

    invoke-interface/range {v30 .. v30}, Ljava/util/List;->size()I

    move-result v30

    move/from16 v0, v18

    move/from16 v1, v30

    if-lt v0, v1, :cond_f

    .line 250
    new-instance v30, Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;

    const/16 v31, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, v22

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;-><init>(Ljava/util/List;Lorg/apache/lucene/search/TopDocs;)V

    goto/16 :goto_0

    .line 169
    .end local v3    # "baseQuery":Lorg/apache/lucene/search/Query;
    .end local v10    # "drillDownCollector":Lorg/apache/lucene/facet/search/FacetsCollector;
    .end local v12    # "drillDownResults":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    .end local v13    # "drillDownTerms":[[Lorg/apache/lucene/index/Term;
    .end local v14    # "drillSidewaysCollectors":[Lorg/apache/lucene/facet/search/FacetsCollector;
    .end local v15    # "drillSidewaysResults":[Ljava/util/List;
    .end local v16    # "dsq":Lorg/apache/lucene/facet/search/DrillSidewaysQuery;
    .end local v18    # "i":I
    .end local v19    # "idx":I
    .end local v22    # "mergedResults":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    .end local v23    # "numDims":I
    .end local v26    # "requestUpto":[I
    .end local v29    # "startClause":I
    :cond_2
    sget-boolean v30, Lorg/apache/lucene/facet/search/DrillSideways;->$assertionsDisabled:Z

    if-nez v30, :cond_3

    array-length v0, v5

    move/from16 v30, v0

    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v31

    add-int/lit8 v31, v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_3

    new-instance v30, Ljava/lang/AssertionError;

    invoke-direct/range {v30 .. v30}, Ljava/lang/AssertionError;-><init>()V

    throw v30

    .line 170
    :cond_3
    const/16 v30, 0x0

    aget-object v30, v5, v30

    invoke-virtual/range {v30 .. v30}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 171
    .restart local v3    # "baseQuery":Lorg/apache/lucene/search/Query;
    const/16 v29, 0x1

    .restart local v29    # "startClause":I
    goto/16 :goto_1

    .line 176
    .restart local v13    # "drillDownTerms":[[Lorg/apache/lucene/index/Term;
    .restart local v18    # "i":I
    :cond_4
    aget-object v30, v5, v18

    invoke-virtual/range {v30 .. v30}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v24

    .line 177
    .local v24, "q":Lorg/apache/lucene/search/Query;
    sget-boolean v30, Lorg/apache/lucene/facet/search/DrillSideways;->$assertionsDisabled:Z

    if-nez v30, :cond_5

    move-object/from16 v0, v24

    instance-of v0, v0, Lorg/apache/lucene/search/ConstantScoreQuery;

    move/from16 v30, v0

    if-nez v30, :cond_5

    new-instance v30, Ljava/lang/AssertionError;

    invoke-direct/range {v30 .. v30}, Ljava/lang/AssertionError;-><init>()V

    throw v30

    .line 178
    :cond_5
    check-cast v24, Lorg/apache/lucene/search/ConstantScoreQuery;

    .end local v24    # "q":Lorg/apache/lucene/search/Query;
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/search/ConstantScoreQuery;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v24

    .line 179
    .restart local v24    # "q":Lorg/apache/lucene/search/Query;
    sget-boolean v30, Lorg/apache/lucene/facet/search/DrillSideways;->$assertionsDisabled:Z

    if-nez v30, :cond_6

    move-object/from16 v0, v24

    instance-of v0, v0, Lorg/apache/lucene/search/TermQuery;

    move/from16 v30, v0

    if-nez v30, :cond_6

    move-object/from16 v0, v24

    instance-of v0, v0, Lorg/apache/lucene/search/BooleanQuery;

    move/from16 v30, v0

    if-nez v30, :cond_6

    new-instance v30, Ljava/lang/AssertionError;

    invoke-direct/range {v30 .. v30}, Ljava/lang/AssertionError;-><init>()V

    throw v30

    .line 180
    :cond_6
    move-object/from16 v0, v24

    instance-of v0, v0, Lorg/apache/lucene/search/TermQuery;

    move/from16 v30, v0

    if-eqz v30, :cond_8

    .line 181
    sub-int v30, v18, v29

    const/16 v31, 0x1

    move/from16 v0, v31

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    check-cast v24, Lorg/apache/lucene/search/TermQuery;

    .end local v24    # "q":Lorg/apache/lucene/search/Query;
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v33

    aput-object v33, v31, v32

    aput-object v31, v13, v30

    .line 175
    :cond_7
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_2

    .restart local v24    # "q":Lorg/apache/lucene/search/Query;
    :cond_8
    move-object/from16 v25, v24

    .line 183
    check-cast v25, Lorg/apache/lucene/search/BooleanQuery;

    .line 184
    .local v25, "q2":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual/range {v25 .. v25}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v6

    .line 185
    .local v6, "clauses2":[Lorg/apache/lucene/search/BooleanClause;
    sub-int v30, v18, v29

    array-length v0, v6

    move/from16 v31, v0

    move/from16 v0, v31

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v31, v0

    aput-object v31, v13, v30

    .line 186
    const/16 v21, 0x0

    .local v21, "j":I
    :goto_5
    array-length v0, v6

    move/from16 v30, v0

    move/from16 v0, v21

    move/from16 v1, v30

    if-ge v0, v1, :cond_7

    .line 187
    sget-boolean v30, Lorg/apache/lucene/facet/search/DrillSideways;->$assertionsDisabled:Z

    if-nez v30, :cond_9

    aget-object v30, v6, v21

    invoke-virtual/range {v30 .. v30}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v30

    move-object/from16 v0, v30

    instance-of v0, v0, Lorg/apache/lucene/search/TermQuery;

    move/from16 v30, v0

    if-nez v30, :cond_9

    new-instance v30, Ljava/lang/AssertionError;

    invoke-direct/range {v30 .. v30}, Ljava/lang/AssertionError;-><init>()V

    throw v30

    .line 188
    :cond_9
    sub-int v30, v18, v29

    aget-object v31, v13, v30

    aget-object v30, v6, v21

    invoke-virtual/range {v30 .. v30}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v30

    check-cast v30, Lorg/apache/lucene/search/TermQuery;

    invoke-virtual/range {v30 .. v30}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v30

    aput-object v30, v31, v21

    .line 186
    add-int/lit8 v21, v21, 0x1

    goto :goto_5

    .line 198
    .end local v6    # "clauses2":[Lorg/apache/lucene/search/BooleanClause;
    .end local v21    # "j":I
    .end local v24    # "q":Lorg/apache/lucene/search/Query;
    .end local v25    # "q2":Lorg/apache/lucene/search/BooleanQuery;
    .restart local v10    # "drillDownCollector":Lorg/apache/lucene/facet/search/FacetsCollector;
    .restart local v14    # "drillSidewaysCollectors":[Lorg/apache/lucene/facet/search/FacetsCollector;
    .restart local v19    # "idx":I
    :cond_a
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 199
    .local v8, "dim":Ljava/lang/String;
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 200
    .local v27, "requests":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetRequest;>;"
    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :cond_b
    :goto_6
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-nez v32, :cond_c

    .line 206
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->isEmpty()Z

    move-result v31

    if-eqz v31, :cond_e

    .line 207
    new-instance v30, Ljava/lang/IllegalArgumentException;

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "could not find FacetRequest for drill-sideways dimension \""

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "\""

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 200
    :cond_c
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 201
    .local v17, "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    sget-boolean v32, Lorg/apache/lucene/facet/search/DrillSideways;->$assertionsDisabled:Z

    if-nez v32, :cond_d

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget v0, v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    move/from16 v32, v0

    if-gtz v32, :cond_d

    new-instance v30, Ljava/lang/AssertionError;

    invoke-direct/range {v30 .. v30}, Ljava/lang/AssertionError;-><init>()V

    throw v30

    .line 202
    :cond_d
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    aget-object v32, v32, v33

    move-object/from16 v0, v32

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_b

    .line 203
    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 209
    .end local v17    # "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    :cond_e
    add-int/lit8 v20, v19, 0x1

    .end local v19    # "idx":I
    .local v20, "idx":I
    new-instance v31, Lorg/apache/lucene/facet/params/FacetSearchParams;

    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/params/FacetSearchParams;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Ljava/util/List;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-virtual {v0, v8, v1}, Lorg/apache/lucene/facet/search/DrillSideways;->getDrillSidewaysAccumulator(Ljava/lang/String;Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/FacetsAccumulator;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lorg/apache/lucene/facet/search/FacetsCollector;->create(Lorg/apache/lucene/facet/search/FacetsAccumulator;)Lorg/apache/lucene/facet/search/FacetsCollector;

    move-result-object v31

    aput-object v31, v14, v19

    move/from16 v19, v20

    .end local v20    # "idx":I
    .restart local v19    # "idx":I
    goto/16 :goto_3

    .line 223
    .end local v8    # "dim":Ljava/lang/String;
    .end local v27    # "requests":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetRequest;>;"
    .restart local v12    # "drillDownResults":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    .restart local v15    # "drillSidewaysResults":[Ljava/util/List;
    .restart local v16    # "dsq":Lorg/apache/lucene/facet/search/DrillSidewaysQuery;
    .restart local v22    # "mergedResults":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    .restart local v23    # "numDims":I
    .restart local v26    # "requestUpto":[I
    :cond_f
    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 224
    .restart local v17    # "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    sget-boolean v30, Lorg/apache/lucene/facet/search/DrillSideways;->$assertionsDisabled:Z

    if-nez v30, :cond_10

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    move/from16 v30, v0

    if-gtz v30, :cond_10

    new-instance v30, Ljava/lang/AssertionError;

    invoke-direct/range {v30 .. v30}, Ljava/lang/AssertionError;-><init>()V

    throw v30

    .line 225
    :cond_10
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    aget-object v30, v30, v31

    move-object/from16 v0, v30

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    .line 226
    .local v9, "dimIndex":Ljava/lang/Integer;
    if-nez v9, :cond_12

    .line 229
    if-nez v12, :cond_11

    .line 232
    invoke-virtual {v10}, Lorg/apache/lucene/facet/search/FacetsCollector;->getFacetResults()Ljava/util/List;

    move-result-object v12

    .line 234
    :cond_11
    move/from16 v0, v18

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lorg/apache/lucene/facet/search/FacetResult;

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    :goto_7
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_4

    .line 237
    :cond_12
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 238
    .local v8, "dim":I
    aget-object v28, v15, v8

    .line 239
    .local v28, "sidewaysResult":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    if-nez v28, :cond_13

    .line 242
    aget-object v30, v14, v8

    invoke-virtual/range {v30 .. v30}, Lorg/apache/lucene/facet/search/FacetsCollector;->getFacetResults()Ljava/util/List;

    move-result-object v28

    .line 243
    aput-object v28, v15, v8

    .line 245
    :cond_13
    aget v30, v26, v8

    move-object/from16 v0, v28

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lorg/apache/lucene/facet/search/FacetResult;

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    aget v30, v26, v8

    add-int/lit8 v30, v30, 0x1

    aput v30, v26, v8

    goto :goto_7
.end method

.method public search(Lorg/apache/lucene/facet/search/DrillDownQuery;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/FieldDoc;ILorg/apache/lucene/search/Sort;ZZLorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;
    .locals 11
    .param p1, "query"    # Lorg/apache/lucene/facet/search/DrillDownQuery;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "after"    # Lorg/apache/lucene/search/FieldDoc;
    .param p4, "topN"    # I
    .param p5, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p6, "doDocScores"    # Z
    .param p7, "doMaxScore"    # Z
    .param p8, "fsp"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 260
    if-eqz p2, :cond_0

    .line 261
    new-instance v9, Lorg/apache/lucene/facet/search/DrillDownQuery;

    invoke-direct {v9, p2, p1}, Lorg/apache/lucene/facet/search/DrillDownQuery;-><init>(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/facet/search/DrillDownQuery;)V

    .end local p1    # "query":Lorg/apache/lucene/facet/search/DrillDownQuery;
    .local v9, "query":Lorg/apache/lucene/facet/search/DrillDownQuery;
    move-object p1, v9

    .line 263
    .end local v9    # "query":Lorg/apache/lucene/facet/search/DrillDownQuery;
    .restart local p1    # "query":Lorg/apache/lucene/facet/search/DrillDownQuery;
    :cond_0
    if-eqz p5, :cond_1

    .line 265
    iget-object v1, p0, Lorg/apache/lucene/facet/search/DrillSideways;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v1

    invoke-static {p4, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 267
    const/4 v4, 0x1

    .line 270
    const/4 v7, 0x1

    move-object/from16 v1, p5

    move-object v3, p3

    move/from16 v5, p6

    move/from16 v6, p7

    .line 264
    invoke-static/range {v1 .. v7}, Lorg/apache/lucene/search/TopFieldCollector;->create(Lorg/apache/lucene/search/Sort;ILorg/apache/lucene/search/FieldDoc;ZZZZ)Lorg/apache/lucene/search/TopFieldCollector;

    move-result-object v8

    .line 271
    .local v8, "hitCollector":Lorg/apache/lucene/search/TopFieldCollector;
    move-object/from16 v0, p8

    invoke-virtual {p0, p1, v8, v0}, Lorg/apache/lucene/facet/search/DrillSideways;->search(Lorg/apache/lucene/facet/search/DrillDownQuery;Lorg/apache/lucene/search/Collector;Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;

    move-result-object v10

    .line 272
    .local v10, "r":Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;
    invoke-virtual {v8}, Lorg/apache/lucene/search/TopFieldCollector;->topDocs()Lorg/apache/lucene/search/TopDocs;

    move-result-object v1

    iput-object v1, v10, Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;->hits:Lorg/apache/lucene/search/TopDocs;

    .line 275
    .end local v8    # "hitCollector":Lorg/apache/lucene/search/TopFieldCollector;
    .end local v10    # "r":Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;
    :goto_0
    return-object v10

    :cond_1
    move-object/from16 v0, p8

    invoke-virtual {p0, p3, p1, p4, v0}, Lorg/apache/lucene/facet/search/DrillSideways;->search(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/facet/search/DrillDownQuery;ILorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;

    move-result-object v10

    goto :goto_0
.end method

.method public search(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/facet/search/DrillDownQuery;ILorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;
    .locals 4
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "query"    # Lorg/apache/lucene/facet/search/DrillDownQuery;
    .param p3, "topN"    # I
    .param p4, "fsp"    # Lorg/apache/lucene/facet/params/FacetSearchParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 285
    iget-object v2, p0, Lorg/apache/lucene/facet/search/DrillSideways;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v2}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v2

    invoke-static {p3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/search/TopScoreDocCollector;->create(ILorg/apache/lucene/search/ScoreDoc;Z)Lorg/apache/lucene/search/TopScoreDocCollector;

    move-result-object v0

    .line 286
    .local v0, "hitCollector":Lorg/apache/lucene/search/TopScoreDocCollector;
    invoke-virtual {p0, p2, v0, p4}, Lorg/apache/lucene/facet/search/DrillSideways;->search(Lorg/apache/lucene/facet/search/DrillDownQuery;Lorg/apache/lucene/search/Collector;Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;

    move-result-object v1

    .line 287
    .local v1, "r":Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;
    invoke-virtual {v0}, Lorg/apache/lucene/search/TopScoreDocCollector;->topDocs()Lorg/apache/lucene/search/TopDocs;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/lucene/facet/search/DrillSideways$DrillSidewaysResult;->hits:Lorg/apache/lucene/search/TopDocs;

    .line 288
    return-object v1
.end method
