.class Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;
.super Ljava/lang/Object;
.source "MatchingDocsAsScoredDocIDs.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;->iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

.field currentLength:I

.field doc:I

.field done:Z

.field final mdIter:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;",
            ">;"
        }
    .end annotation
.end field

.field scoresIdx:I

.field final synthetic this$0:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->this$0:Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iget-object v0, p1, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs;->matchingDocs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->mdIter:Ljava/util/Iterator;

    .line 55
    iput v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->scoresIdx:I

    .line 56
    iput v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    .line 59
    iput-boolean v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->done:Z

    return-void
.end method


# virtual methods
.method public getDocID()I
    .locals 2

    .prologue
    .line 100
    iget-boolean v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->done:Z

    if-eqz v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    iget-object v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v1, v1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    iget v1, v1, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getScore()F
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->scores:[F

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->scores:[F

    iget v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->scoresIdx:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->scoresIdx:I

    aget v0, v0, v1

    goto :goto_0
.end method

.method public next()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 63
    iget-boolean v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->done:Z

    if-eqz v0, :cond_3

    move v0, v1

    .line 90
    :goto_0
    return v0

    .line 68
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->mdIter:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    iput-boolean v2, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->done:Z

    move v0, v1

    .line 70
    goto :goto_0

    .line 72
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->mdIter:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iput-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v0}, Lorg/apache/lucene/util/FixedBitSet;->length()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->currentLength:I

    .line 74
    iput v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    .line 75
    iput v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->scoresIdx:I

    .line 77
    iget v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    iget v3, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->currentLength:I

    if-ge v0, v3, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    iget v3, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    invoke-virtual {v0, v3}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    if-ne v0, v4, :cond_5

    .line 78
    :cond_2
    iput-object v5, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    .line 67
    :cond_3
    :goto_1
    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    if-eqz v0, :cond_0

    .line 84
    iget v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    .line 85
    iget v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    iget v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->currentLength:I

    if-ge v0, v1, :cond_4

    iget-object v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    iget-object v0, v0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    iget v1, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    if-ne v0, v4, :cond_6

    .line 86
    :cond_4
    iput-object v5, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->current:Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;

    .line 87
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->next()Z

    move-result v0

    goto :goto_0

    .line 80
    :cond_5
    iput v4, p0, Lorg/apache/lucene/facet/search/MatchingDocsAsScoredDocIDs$1;->doc:I

    goto :goto_1

    :cond_6
    move v0, v2

    .line 90
    goto :goto_0
.end method
