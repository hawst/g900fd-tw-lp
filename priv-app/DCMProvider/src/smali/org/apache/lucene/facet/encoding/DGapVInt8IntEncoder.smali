.class public final Lorg/apache/lucene/facet/encoding/DGapVInt8IntEncoder;
.super Lorg/apache/lucene/facet/encoding/IntEncoder;
.source "DGapVInt8IntEncoder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/IntEncoder;-><init>()V

    return-void
.end method


# virtual methods
.method public createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lorg/apache/lucene/facet/encoding/DGapVInt8IntDecoder;

    invoke-direct {v0}, Lorg/apache/lucene/facet/encoding/DGapVInt8IntDecoder;-><init>()V

    return-object v0
.end method

.method public encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 11
    .param p1, "values"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "buf"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/high16 v10, 0xfe00000

    const/high16 v9, -0x10000000

    const v8, 0x1fc000

    .line 37
    const/4 v5, 0x0

    iput v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v5, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 38
    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    mul-int/lit8 v1, v5, 0x5

    .line 39
    .local v1, "maxBytesNeeded":I
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v5, v5

    if-ge v5, v1, :cond_0

    .line 40
    invoke-virtual {p2, v1}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 43
    :cond_0
    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v6, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v3, v5, v6

    .line 44
    .local v3, "upto":I
    const/4 v2, 0x0

    .line 45
    .local v2, "prev":I
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v0, "i":I
    :goto_0
    if-lt v0, v3, :cond_1

    .line 77
    return-void

    .line 48
    :cond_1
    iget-object v5, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v5, v5, v0

    sub-int v4, v5, v2

    .line 49
    .local v4, "value":I
    and-int/lit8 v5, v4, -0x80

    if-nez v5, :cond_2

    .line 50
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-byte v7, v4

    aput-byte v7, v5, v6

    .line 51
    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 75
    :goto_1
    iget-object v5, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v2, v5, v0

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_2
    and-int/lit16 v5, v4, -0x4000

    if-nez v5, :cond_3

    .line 53
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int/lit16 v7, v4, 0x3f80

    shr-int/lit8 v7, v7, 0x7

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 54
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, 0x1

    and-int/lit8 v7, v4, 0x7f

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 55
    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x2

    iput v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_1

    .line 56
    :cond_3
    const/high16 v5, -0x200000

    and-int/2addr v5, v4

    if-nez v5, :cond_4

    .line 57
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int v7, v4, v8

    shr-int/lit8 v7, v7, 0xe

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 58
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, 0x1

    and-int/lit16 v7, v4, 0x3f80

    shr-int/lit8 v7, v7, 0x7

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 59
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, 0x2

    and-int/lit8 v7, v4, 0x7f

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 60
    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x3

    iput v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_1

    .line 61
    :cond_4
    and-int v5, v4, v9

    if-nez v5, :cond_5

    .line 62
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int v7, v4, v10

    shr-int/lit8 v7, v7, 0x15

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 63
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, 0x1

    and-int v7, v4, v8

    shr-int/lit8 v7, v7, 0xe

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 64
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, 0x2

    and-int/lit16 v7, v4, 0x3f80

    shr-int/lit8 v7, v7, 0x7

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 65
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, 0x3

    and-int/lit8 v7, v4, 0x7f

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 66
    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x4

    iput v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto/16 :goto_1

    .line 68
    :cond_5
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    and-int v7, v4, v9

    shr-int/lit8 v7, v7, 0x1c

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 69
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, 0x1

    and-int v7, v4, v10

    shr-int/lit8 v7, v7, 0x15

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 70
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, 0x2

    and-int v7, v4, v8

    shr-int/lit8 v7, v7, 0xe

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 71
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, 0x3

    and-int/lit16 v7, v4, 0x3f80

    shr-int/lit8 v7, v7, 0x7

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 72
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v6, v6, 0x4

    and-int/lit8 v7, v4, 0x7f

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 73
    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v5, 0x5

    iput v5, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto/16 :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const-string v0, "DGapVInt8"

    return-object v0
.end method
