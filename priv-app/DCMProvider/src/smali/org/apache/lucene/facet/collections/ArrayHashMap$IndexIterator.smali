.class final Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;
.super Ljava/lang/Object;
.source "ArrayHashMap.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/IntIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/ArrayHashMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "IndexIterator"
.end annotation


# instance fields
.field private baseHashIndex:I

.field private index:I

.field private lastIndex:I

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/ArrayHashMap;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/collections/ArrayHashMap;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 59
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/ArrayHashMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->baseHashIndex:I

    .line 49
    iput v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->index:I

    .line 52
    iput v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->lastIndex:I

    .line 60
    iput v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->baseHashIndex:I

    :goto_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->baseHashIndex:I

    iget-object v1, p1, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 66
    :cond_0
    return-void

    .line 61
    :cond_1
    iget-object v0, p1, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->baseHashIndex:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->index:I

    .line 62
    iget v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->index:I

    if-nez v0, :cond_0

    .line 60
    iget v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->baseHashIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->baseHashIndex:I

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->index:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()I
    .locals 2

    .prologue
    .line 76
    iget v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->index:I

    iput v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->lastIndex:I

    .line 79
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/ArrayHashMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->next:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->index:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->index:I

    .line 84
    :goto_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->index:I

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->baseHashIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->baseHashIndex:I

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/ArrayHashMap;

    iget-object v1, v1, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 88
    :cond_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->lastIndex:I

    return v0

    .line 85
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/ArrayHashMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/ArrayHashMap;->baseHash:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->baseHashIndex:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->index:I

    goto :goto_0
.end method

.method public remove()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/ArrayHashMap;

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/ArrayHashMap;

    iget-object v1, v1, Lorg/apache/lucene/facet/collections/ArrayHashMap;->keys:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/lucene/facet/collections/ArrayHashMap$IndexIterator;->lastIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/collections/ArrayHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    return-void
.end method
