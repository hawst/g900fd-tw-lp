.class public abstract Lorg/apache/lucene/facet/search/IntRollupFacetsAggregator;
.super Ljava/lang/Object;
.source "IntRollupFacetsAggregator.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/FacetsAggregator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private rollupValues(I[I[I[I)I
    .locals 3
    .param p1, "ordinal"    # I
    .param p2, "children"    # [I
    .param p3, "siblings"    # [I
    .param p4, "values"    # [I

    .prologue
    .line 41
    const/4 v1, 0x0

    .line 42
    .local v1, "value":I
    :goto_0
    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    .line 49
    return v1

    .line 43
    :cond_0
    aget v0, p4, p1

    .line 44
    .local v0, "childValue":I
    aget v2, p2, p1

    invoke-direct {p0, v2, p2, p3, p4}, Lorg/apache/lucene/facet/search/IntRollupFacetsAggregator;->rollupValues(I[I[I[I)I

    move-result v2

    add-int/2addr v0, v2

    .line 45
    aput v0, p4, p1

    .line 46
    add-int/2addr v1, v0

    .line 47
    aget p1, p3, p1

    goto :goto_0
.end method


# virtual methods
.method public abstract aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final requiresDocScores()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public final rollupValues(Lorg/apache/lucene/facet/search/FacetRequest;I[I[ILorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 3
    .param p1, "fr"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p2, "ordinal"    # I
    .param p3, "children"    # [I
    .param p4, "siblings"    # [I
    .param p5, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 54
    invoke-virtual {p5}, Lorg/apache/lucene/facet/search/FacetArrays;->getIntArray()[I

    move-result-object v0

    .line 55
    .local v0, "values":[I
    aget v1, v0, p2

    aget v2, p3, p2

    invoke-direct {p0, v2, p3, p4, v0}, Lorg/apache/lucene/facet/search/IntRollupFacetsAggregator;->rollupValues(I[I[I[I)I

    move-result v2

    add-int/2addr v1, v2

    aput v1, v0, p2

    .line 56
    return-void
.end method
