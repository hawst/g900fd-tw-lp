.class final Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;
.super Ljava/lang/Object;
.source "IntToDoubleMap.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/IntIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/IntToDoubleMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "IndexIterator"
.end annotation


# instance fields
.field private baseHashIndex:I

.field private index:I

.field private lastIndex:I

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/IntToDoubleMap;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/collections/IntToDoubleMap;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 66
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToDoubleMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->baseHashIndex:I

    .line 54
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->index:I

    .line 59
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->lastIndex:I

    .line 67
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->baseHashIndex:I

    :goto_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->baseHashIndex:I

    iget-object v1, p1, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 73
    :cond_0
    return-void

    .line 68
    :cond_1
    iget-object v0, p1, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->baseHashIndex:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->index:I

    .line 69
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->index:I

    if-nez v0, :cond_0

    .line 67
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->baseHashIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->baseHashIndex:I

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->index:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()I
    .locals 2

    .prologue
    .line 83
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->index:I

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->lastIndex:I

    .line 86
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToDoubleMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->next:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->index:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->index:I

    .line 91
    :goto_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->index:I

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->baseHashIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->baseHashIndex:I

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToDoubleMap;

    iget-object v1, v1, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 95
    :cond_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->lastIndex:I

    return v0

    .line 92
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToDoubleMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->baseHash:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->baseHashIndex:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->index:I

    goto :goto_0
.end method

.method public remove()V
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToDoubleMap;

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToDoubleMap;

    iget-object v1, v1, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->keys:[I

    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToDoubleMap$IndexIterator;->lastIndex:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/collections/IntToDoubleMap;->remove(I)D

    .line 101
    return-void
.end method
