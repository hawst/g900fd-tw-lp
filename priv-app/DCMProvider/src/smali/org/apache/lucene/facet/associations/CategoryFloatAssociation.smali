.class public Lorg/apache/lucene/facet/associations/CategoryFloatAssociation;
.super Ljava/lang/Object;
.source "CategoryFloatAssociation.java"

# interfaces
.implements Lorg/apache/lucene/facet/associations/CategoryAssociation;


# static fields
.field public static final ASSOCIATION_LIST_ID:Ljava/lang/String; = "$assoc_float$"


# instance fields
.field private value:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method public constructor <init>(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lorg/apache/lucene/facet/associations/CategoryFloatAssociation;->value:F

    .line 38
    return-void
.end method


# virtual methods
.method public deserialize(Lorg/apache/lucene/store/ByteArrayDataInput;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/store/ByteArrayDataInput;

    .prologue
    .line 51
    invoke-virtual {p1}, Lorg/apache/lucene/store/ByteArrayDataInput;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/associations/CategoryFloatAssociation;->value:F

    .line 52
    return-void
.end method

.method public getCategoryListID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const-string v0, "$assoc_float$"

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lorg/apache/lucene/facet/associations/CategoryFloatAssociation;->value:F

    return v0
.end method

.method public maxBytesNeeded()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x4

    return v0
.end method

.method public serialize(Lorg/apache/lucene/store/ByteArrayDataOutput;)V
    .locals 3
    .param p1, "output"    # Lorg/apache/lucene/store/ByteArrayDataOutput;

    .prologue
    .line 43
    :try_start_0
    iget v1, p0, Lorg/apache/lucene/facet/associations/CategoryFloatAssociation;->value:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/ByteArrayDataOutput;->writeInt(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "unexpected exception writing to a byte[]"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/facet/associations/CategoryFloatAssociation;->value:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
