.class public Lorg/apache/lucene/facet/search/SumScoreFacetsAggregator;
.super Ljava/lang/Object;
.source "SumScoreFacetsAggregator.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/FacetsAggregator;


# instance fields
.field private final ordinals:Lorg/apache/lucene/util/IntsRef;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/SumScoreFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    .line 31
    return-void
.end method

.method private rollupScores(I[I[I[F)F
    .locals 3
    .param p1, "ordinal"    # I
    .param p2, "children"    # [I
    .param p3, "siblings"    # [I
    .param p4, "scores"    # [F

    .prologue
    .line 58
    const/4 v1, 0x0

    .line 59
    .local v1, "score":F
    :goto_0
    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    .line 66
    return v1

    .line 60
    :cond_0
    aget v0, p4, p1

    .line 61
    .local v0, "childScore":F
    aget v2, p2, p1

    invoke-direct {p0, v2, p2, p3, p4}, Lorg/apache/lucene/facet/search/SumScoreFacetsAggregator;->rollupScores(I[I[I[F)F

    move-result v2

    add-float/2addr v0, v2

    .line 62
    aput v0, p4, p1

    .line 63
    add-float/2addr v1, v0

    .line 64
    aget p1, p3, p1

    goto :goto_0
.end method


# virtual methods
.method public aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 11
    .param p1, "matchingDocs"    # Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    .param p2, "clp"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    const/4 v9, 0x0

    invoke-virtual {p2, v9}, Lorg/apache/lucene/facet/params/CategoryListParams;->createCategoryListIterator(I)Lorg/apache/lucene/facet/search/CategoryListIterator;

    move-result-object v0

    .line 38
    .local v0, "cli":Lorg/apache/lucene/facet/search/CategoryListIterator;
    iget-object v9, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-interface {v0, v9}, Lorg/apache/lucene/facet/search/CategoryListIterator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 55
    :cond_0
    return-void

    .line 42
    :cond_1
    const/4 v1, 0x0

    .line 43
    .local v1, "doc":I
    iget-object v9, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v9}, Lorg/apache/lucene/util/FixedBitSet;->length()I

    move-result v3

    .line 44
    .local v3, "length":I
    invoke-virtual {p3}, Lorg/apache/lucene/facet/search/FacetArrays;->getFloatArray()[F

    move-result-object v5

    .line 45
    .local v5, "scores":[F
    const/4 v6, 0x0

    .local v6, "scoresIdx":I
    move v7, v6

    .line 46
    .end local v6    # "scoresIdx":I
    .local v7, "scoresIdx":I
    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v9, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v9, v1}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v1

    const/4 v9, -0x1

    if-eq v1, v9, :cond_0

    .line 47
    iget-object v9, p0, Lorg/apache/lucene/facet/search/SumScoreFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    invoke-interface {v0, v1, v9}, Lorg/apache/lucene/facet/search/CategoryListIterator;->getOrdinals(ILorg/apache/lucene/util/IntsRef;)V

    .line 48
    iget-object v9, p0, Lorg/apache/lucene/facet/search/SumScoreFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    iget v9, v9, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget-object v10, p0, Lorg/apache/lucene/facet/search/SumScoreFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    iget v10, v10, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v8, v9, v10

    .line 49
    .local v8, "upto":I
    iget-object v9, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->scores:[F

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "scoresIdx":I
    .restart local v6    # "scoresIdx":I
    aget v4, v9, v7

    .line 50
    .local v4, "score":F
    iget-object v9, p0, Lorg/apache/lucene/facet/search/SumScoreFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    iget v2, v9, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v2, "i":I
    :goto_1
    if-lt v2, v8, :cond_2

    .line 53
    add-int/lit8 v1, v1, 0x1

    move v7, v6

    .end local v6    # "scoresIdx":I
    .restart local v7    # "scoresIdx":I
    goto :goto_0

    .line 51
    .end local v7    # "scoresIdx":I
    .restart local v6    # "scoresIdx":I
    :cond_2
    iget-object v9, p0, Lorg/apache/lucene/facet/search/SumScoreFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    iget-object v9, v9, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v9, v9, v2

    aget v10, v5, v9

    add-float/2addr v10, v4

    aput v10, v5, v9

    .line 50
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public requiresDocScores()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method

.method public rollupValues(Lorg/apache/lucene/facet/search/FacetRequest;I[I[ILorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 3
    .param p1, "fr"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p2, "ordinal"    # I
    .param p3, "children"    # [I
    .param p4, "siblings"    # [I
    .param p5, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 71
    invoke-virtual {p5}, Lorg/apache/lucene/facet/search/FacetArrays;->getFloatArray()[F

    move-result-object v0

    .line 72
    .local v0, "scores":[F
    aget v1, v0, p2

    aget v2, p3, p2

    invoke-direct {p0, v2, p3, p4, v0}, Lorg/apache/lucene/facet/search/SumScoreFacetsAggregator;->rollupScores(I[I[I[F)F

    move-result v2

    add-float/2addr v1, v2

    aput v1, v0, p2

    .line 73
    return-void
.end method
