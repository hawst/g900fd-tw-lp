.class public abstract Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/LabelToOrdinal;
.super Ljava/lang/Object;
.source "LabelToOrdinal.java"


# static fields
.field public static final INVALID_ORDINAL:I = -0x2


# instance fields
.field protected counter:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract addLabel(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V
.end method

.method public getMaxOrdinal()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/LabelToOrdinal;->counter:I

    return v0
.end method

.method public getNextOrdinal()I
    .locals 2

    .prologue
    .line 44
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/LabelToOrdinal;->counter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/LabelToOrdinal;->counter:I

    return v0
.end method

.method public abstract getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
.end method
