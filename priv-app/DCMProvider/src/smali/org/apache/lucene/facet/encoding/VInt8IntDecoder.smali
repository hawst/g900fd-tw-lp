.class public final Lorg/apache/lucene/facet/encoding/VInt8IntDecoder;
.super Lorg/apache/lucene/facet/encoding/IntDecoder;
.source "VInt8IntDecoder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/IntDecoder;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V
    .locals 8
    .param p1, "buf"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "values"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 34
    const/4 v5, 0x0

    iput v5, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v5, p2, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 39
    iget-object v5, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v5, v5

    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v5, v6, :cond_0

    .line 40
    iget v5, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    const/4 v6, 0x4

    invoke-static {v5, v6}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v5

    new-array v5, v5, [I

    iput-object v5, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 45
    :cond_0
    iget v5, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v3, v5, v6

    .line 46
    .local v3, "upto":I
    const/4 v4, 0x0

    .line 47
    .local v4, "value":I
    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .local v1, "offset":I
    move v2, v1

    .line 48
    .end local v1    # "offset":I
    .local v2, "offset":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 57
    return-void

    .line 49
    :cond_1
    iget-object v5, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    aget-byte v0, v5, v2

    .line 50
    .local v0, "b":B
    if-ltz v0, :cond_2

    .line 51
    iget-object v5, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v6, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    shl-int/lit8 v7, v4, 0x7

    or-int/2addr v7, v0

    aput v7, v5, v6

    .line 52
    const/4 v4, 0x0

    move v2, v1

    .line 53
    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    goto :goto_0

    .line 54
    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    :cond_2
    shl-int/lit8 v5, v4, 0x7

    and-int/lit8 v6, v0, 0x7f

    or-int v4, v5, v6

    move v2, v1

    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string v0, "VInt8"

    return-object v0
.end method
