.class Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "OrdinalMappingAtomicReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OrdinalMappingBinaryDocValues"
.end annotation


# instance fields
.field private final decoder:Lorg/apache/lucene/facet/encoding/IntDecoder;

.field private final delegate:Lorg/apache/lucene/index/BinaryDocValues;

.field private final encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

.field private final ordinals:Lorg/apache/lucene/util/IntsRef;

.field private final scratch:Lorg/apache/lucene/util/BytesRef;

.field final synthetic this$0:Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/index/BinaryDocValues;)V
    .locals 2
    .param p2, "clp"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p3, "delegate"    # Lorg/apache/lucene/index/BinaryDocValues;

    .prologue
    .line 116
    iput-object p1, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->this$0:Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    .line 112
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->ordinals:Lorg/apache/lucene/util/IntsRef;

    .line 114
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 117
    iput-object p3, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->delegate:Lorg/apache/lucene/index/BinaryDocValues;

    .line 118
    invoke-virtual {p2}, Lorg/apache/lucene/facet/params/CategoryListParams;->createEncoder()Lorg/apache/lucene/facet/encoding/IntEncoder;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    .line 119
    iget-object v0, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/encoding/IntEncoder;->createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->decoder:Lorg/apache/lucene/facet/encoding/IntDecoder;

    .line 120
    return-void
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 4
    .param p1, "docID"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 130
    iget-object v1, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->delegate:Lorg/apache/lucene/index/BinaryDocValues;

    iget-object v2, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, p1, v2}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 131
    iget-object v1, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v1, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lez v1, :cond_0

    .line 135
    iget-object v1, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->decoder:Lorg/apache/lucene/facet/encoding/IntDecoder;

    iget-object v2, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->ordinals:Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/facet/encoding/IntDecoder;->decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V

    .line 138
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->ordinals:Lorg/apache/lucene/util/IntsRef;

    iget v1, v1, Lorg/apache/lucene/util/IntsRef;->length:I

    if-lt v0, v1, :cond_1

    .line 142
    iget-object v1, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    iget-object v2, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->ordinals:Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v1, v2, p2}, Lorg/apache/lucene/facet/encoding/IntEncoder;->encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 144
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 139
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->ordinals:Lorg/apache/lucene/util/IntsRef;

    iget-object v1, v1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v2, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->this$0:Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;

    # getter for: Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;->ordinalMap:[I
    invoke-static {v2}, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;->access$0(Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;)[I

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;->ordinals:Lorg/apache/lucene/util/IntsRef;

    iget-object v3, v3, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v3, v3, v0

    aget v2, v2, v3

    aput v2, v1, v0

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
