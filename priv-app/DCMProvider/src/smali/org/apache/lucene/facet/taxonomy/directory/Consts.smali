.class abstract Lorg/apache/lucene/facet/taxonomy/directory/Consts;
.super Ljava/lang/Object;
.source "Consts.java"


# static fields
.field static final DEFAULT_DELIMITER:C = '\u001f'

.field static final FIELD_PAYLOADS:Ljava/lang/String; = "$payloads$"

.field static final FULL:Ljava/lang/String; = "$full_path$"

.field static final PAYLOAD_PARENT:Ljava/lang/String; = "p"

.field static final PAYLOAD_PARENT_BYTES_REF:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "p"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/facet/taxonomy/directory/Consts;->PAYLOAD_PARENT_BYTES_REF:Lorg/apache/lucene/util/BytesRef;

    .line 44
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
