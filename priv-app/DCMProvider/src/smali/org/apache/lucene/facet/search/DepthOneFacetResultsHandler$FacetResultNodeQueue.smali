.class Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler$FacetResultNodeQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "DepthOneFacetResultsHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FacetResultNodeQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/facet/search/FacetResultNode;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(IZ)V
    .locals 0
    .param p1, "maxSize"    # I
    .param p2, "prepopulate"    # Z

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/PriorityQueue;-><init>(IZ)V

    .line 45
    return-void
.end method


# virtual methods
.method protected bridge synthetic getSentinelObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler$FacetResultNodeQueue;->getSentinelObject()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v0

    return-object v0
.end method

.method protected getSentinelObject()Lorg/apache/lucene/facet/search/FacetResultNode;
    .locals 4

    .prologue
    .line 49
    new-instance v0, Lorg/apache/lucene/facet/search/FacetResultNode;

    const/4 v1, -0x1

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/facet/search/FacetResultNode;-><init>(ID)V

    return-object v0
.end method

.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/search/FacetResultNode;

    check-cast p2, Lorg/apache/lucene/facet/search/FacetResultNode;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/facet/search/DepthOneFacetResultsHandler$FacetResultNodeQueue;->lessThan(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/facet/search/FacetResultNode;)Z

    move-result v0

    return v0
.end method

.method protected lessThan(Lorg/apache/lucene/facet/search/FacetResultNode;Lorg/apache/lucene/facet/search/FacetResultNode;)Z
    .locals 6
    .param p1, "a"    # Lorg/apache/lucene/facet/search/FacetResultNode;
    .param p2, "b"    # Lorg/apache/lucene/facet/search/FacetResultNode;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 54
    iget-wide v2, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    iget-wide v4, p2, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v0

    .line 55
    :cond_1
    iget-wide v2, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    iget-wide v4, p2, Lorg/apache/lucene/facet/search/FacetResultNode;->value:D

    cmpl-double v2, v2, v4

    if-lez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 57
    :cond_2
    iget v2, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    iget v3, p2, Lorg/apache/lucene/facet/search/FacetResultNode;->ordinal:I

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method
