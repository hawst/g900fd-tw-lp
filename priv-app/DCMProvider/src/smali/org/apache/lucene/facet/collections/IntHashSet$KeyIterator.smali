.class final Lorg/apache/lucene/facet/collections/IntHashSet$KeyIterator;
.super Ljava/lang/Object;
.source "IntHashSet.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/IntIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/IntHashSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "KeyIterator"
.end annotation


# instance fields
.field private iterator:Lorg/apache/lucene/facet/collections/IntIterator;

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/IntHashSet;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/collections/IntHashSet;)V
    .locals 1

    .prologue
    .line 103
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/IntHashSet$KeyIterator;->this$0:Lorg/apache/lucene/facet/collections/IntHashSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    new-instance v0, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/collections/IntHashSet$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/IntHashSet;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    .line 103
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()I
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$KeyIterator;->this$0:Lorg/apache/lucene/facet/collections/IntHashSet;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntHashSet;->keys:[I

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntHashSet$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntHashSet$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->remove()V

    .line 118
    return-void
.end method
