.class public interface abstract Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
.super Ljava/lang/Object;
.source "ScoredDocIDsIterator.java"


# static fields
.field public static final DEFAULT_SCORE:F = 1.0f


# virtual methods
.method public abstract getDocID()I
.end method

.method public abstract getScore()F
.end method

.method public abstract next()Z
.end method
