.class Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "ScoredDocIdsUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final liveDocs:Lorg/apache/lucene/util/Bits;

.field private next:I

.field final synthetic this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;

    .line 379
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 380
    # getter for: Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;->this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;
    invoke-static {p1}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;->access$0(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;)Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-static {v0}, Lorg/apache/lucene/index/MultiFields;->getLiveDocs(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/util/Bits;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 381
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->next:I

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 385
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->next:I

    if-le p1, v0, :cond_0

    .line 386
    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->next:I

    .line 388
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->nextDoc()I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;

    # getter for: Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;->this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;
    invoke-static {v0}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;->access$0(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;)Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;

    move-result-object v0

    iget v0, v0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;->maxDoc:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 393
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->next:I

    return v0
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    .line 399
    :cond_0
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->next:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->next:I

    .line 400
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->next:I

    iget-object v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;

    # getter for: Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;->this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;
    invoke-static {v1}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;->access$0(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;)Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;

    move-result-object v1

    iget v1, v1, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;->maxDoc:I

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->next:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    :cond_1
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->next:I

    iget-object v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->this$2:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;

    # getter for: Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;->this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;
    invoke-static {v1}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;->access$0(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1;)Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;

    move-result-object v1

    iget v1, v1, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;->maxDoc:I

    if-ge v0, v1, :cond_2

    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs$1$1;->next:I

    :goto_0
    return v0

    :cond_2
    const v0, 0x7fffffff

    goto :goto_0
.end method
