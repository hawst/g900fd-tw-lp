.class final Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;
.super Ljava/lang/Object;
.source "CharBlockArray.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Block"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field final chars:[C

.field length:I


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-array v0, p1, [C

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->chars:[C

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/facet/taxonomy/writercache/cl2o/CharBlockArray$Block;->length:I

    .line 50
    return-void
.end method
