.class public Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;
.super Ljava/lang/Object;
.source "DocValuesCategoryListIterator.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/CategoryListIterator;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final bytes:Lorg/apache/lucene/util/BytesRef;

.field private current:Lorg/apache/lucene/index/BinaryDocValues;

.field private final decoder:Lorg/apache/lucene/facet/encoding/IntDecoder;

.field private final field:Ljava/lang/String;

.field private final hashCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/facet/encoding/IntDecoder;)V
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "decoder"    # Lorg/apache/lucene/facet/encoding/IntDecoder;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->field:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->decoder:Lorg/apache/lucene/facet/encoding/IntDecoder;

    .line 44
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->hashCode:I

    .line 45
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 54
    instance-of v2, p1, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;

    if-nez v2, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 57
    check-cast v0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;

    .line 58
    .local v0, "other":Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;
    iget v2, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->hashCode:I

    iget v3, v0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->hashCode:I

    if-ne v2, v3, :cond_0

    .line 63
    iget-object v1, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->field:Ljava/lang/String;

    iget-object v2, v0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getOrdinals(ILorg/apache/lucene/util/IntsRef;)V
    .locals 2
    .param p1, "docID"    # I
    .param p2, "ints"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    sget-boolean v0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->current:Lorg/apache/lucene/index/BinaryDocValues;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "don\'t call this if setNextReader returned false"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 75
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->current:Lorg/apache/lucene/index/BinaryDocValues;

    iget-object v1, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 76
    const/4 v0, 0x0

    iput v0, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 77
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lez v0, :cond_1

    .line 78
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->decoder:Lorg/apache/lucene/facet/encoding/IntDecoder;

    iget-object v1, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1, p2}, Lorg/apache/lucene/facet/encoding/IntDecoder;->decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V

    .line 80
    :cond_1
    return-void
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->hashCode:I

    return v0
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Z
    .locals 2
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->current:Lorg/apache/lucene/index/BinaryDocValues;

    .line 69
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->current:Lorg/apache/lucene/index/BinaryDocValues;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;->field:Ljava/lang/String;

    return-object v0
.end method
