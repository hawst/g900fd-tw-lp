.class Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;
.super Ljava/lang/Object;
.source "TotalFacetCountsCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/complements/TotalFacetCountsCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TFCKey"
.end annotation


# instance fields
.field private final clps:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            ">;"
        }
    .end annotation
.end field

.field final facetIndexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

.field private final hashCode:I

.field final indexReader:Lorg/apache/lucene/index/IndexReader;

.field private final nDels:I

.field final taxonomy:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 2
    .param p1, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "taxonomy"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p3, "facetIndexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .prologue
    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    iput-object p1, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->indexReader:Lorg/apache/lucene/index/IndexReader;

    .line 239
    iput-object p2, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->taxonomy:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .line 240
    iput-object p3, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->facetIndexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 241
    invoke-virtual {p3}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getAllCategoryListParams()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->clps:Ljava/lang/Iterable;

    .line 242
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->numDeletedDocs()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->nDels:I

    .line 243
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->hashCode()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->hashCode:I

    .line 244
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 253
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;

    .line 254
    .local v2, "o":Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;
    iget-object v3, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->indexReader:Lorg/apache/lucene/index/IndexReader;

    iget-object v5, v2, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->indexReader:Lorg/apache/lucene/index/IndexReader;

    if-ne v3, v5, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->taxonomy:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v5, v2, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->taxonomy:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    if-ne v3, v5, :cond_0

    iget v3, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->nDels:I

    iget v5, v2, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->nDels:I

    if-eq v3, v5, :cond_1

    :cond_0
    move v3, v4

    .line 264
    :goto_0
    return v3

    .line 257
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->clps:Ljava/lang/Iterable;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 258
    .local v0, "it1":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/facet/params/CategoryListParams;>;"
    iget-object v3, v2, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->clps:Ljava/lang/Iterable;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 259
    .local v1, "it2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/facet/params/CategoryListParams;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 264
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-ne v3, v5, :cond_5

    const/4 v3, 0x1

    goto :goto_0

    .line 260
    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/facet/params/CategoryListParams;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/lucene/facet/params/CategoryListParams;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v4

    .line 261
    goto :goto_0

    :cond_5
    move v3, v4

    .line 264
    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 248
    iget v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCountsCache$TFCKey;->hashCode:I

    return v0
.end method
