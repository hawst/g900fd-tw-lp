.class public Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader;
.super Lorg/apache/lucene/index/FilterAtomicReader;
.source "FacetsPayloadMigrationReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;
    }
.end annotation


# static fields
.field public static final PAYLOAD_TERM_TEXT:Ljava/lang/String; = "$fulltree$"


# instance fields
.field private final fieldTerms:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/AtomicReader;Ljava/util/Map;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/AtomicReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/AtomicReader;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p2, "fieldTerms":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/Term;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/FilterAtomicReader;-><init>(Lorg/apache/lucene/index/AtomicReader;)V

    .line 210
    iput-object p2, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader;->fieldTerms:Ljava/util/Map;

    .line 211
    return-void
.end method

.method public static buildFieldTermsMap(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/facet/params/FacetIndexingParams;)Ljava/util/Map;
    .locals 13
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p1, "fip"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            "Lorg/apache/lucene/facet/params/FacetIndexingParams;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    invoke-static {p0}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v3

    .line 178
    .local v3, "reader":Lorg/apache/lucene/index/DirectoryReader;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 179
    .local v2, "fieldTerms":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {v3}, Lorg/apache/lucene/index/DirectoryReader;->leaves()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 198
    invoke-virtual {v3}, Lorg/apache/lucene/index/DirectoryReader;->close()V

    .line 199
    return-object v2

    .line 179
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 180
    .local v1, "context":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {p1}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getAllCategoryListParams()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 181
    .local v0, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v10

    iget-object v11, v0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lorg/apache/lucene/index/AtomicReader;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v7

    .line 182
    .local v7, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v7, :cond_2

    .line 183
    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v4

    .line 184
    .local v4, "te":Lorg/apache/lucene/index/TermsEnum;
    const/4 v6, 0x0

    .line 185
    .local v6, "termBytes":Lorg/apache/lucene/util/BytesRef;
    :cond_3
    :goto_0
    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 186
    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v5

    .line 187
    .local v5, "term":Ljava/lang/String;
    const-string v10, "$fulltree$"

    invoke-virtual {v5, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 188
    const-string v10, "$fulltree$"

    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 189
    iget-object v10, v0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    new-instance v11, Lorg/apache/lucene/index/Term;

    iget-object v12, v0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-direct {v11, v12, v5}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 191
    :cond_4
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, v0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "$fulltree$"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v5, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Lorg/apache/lucene/index/Term;

    iget-object v12, v0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-direct {v11, v12, v5}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    iget-object v1, p0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader;->fieldTerms:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/Term;

    .line 216
    .local v0, "term":Lorg/apache/lucene/index/Term;
    if-nez v0, :cond_0

    .line 217
    invoke-super {p0, p1}, Lorg/apache/lucene/index/FilterAtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v1

    .line 221
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;

    invoke-virtual {p0}, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader$PayloadMigratingBinaryDocValues;-><init>(Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader;Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/index/Term;)V

    goto :goto_0
.end method

.method public getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 18

    .prologue
    .line 227
    invoke-super/range {p0 .. p0}, Lorg/apache/lucene/index/FilterAtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v14

    .line 228
    .local v14, "innerInfos":Lorg/apache/lucene/index/FieldInfos;
    new-instance v13, Ljava/util/ArrayList;

    invoke-virtual {v14}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v1

    invoke-direct {v13, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 231
    .local v13, "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/index/FieldInfo;>;"
    new-instance v15, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader;->fieldTerms:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v15, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 232
    .local v15, "leftoverFields":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v4, -0x1

    .line 233
    .local v4, "number":I
    invoke-virtual {v14}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v17

    move/from16 v16, v4

    .end local v4    # "number":I
    .local v16, "number":I
    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    invoke-virtual {v15}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v17

    move/from16 v4, v16

    .end local v16    # "number":I
    .restart local v4    # "number":I
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 250
    new-instance v3, Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/lucene/index/FieldInfo;

    invoke-direct {v3, v1}, Lorg/apache/lucene/index/FieldInfos;-><init>([Lorg/apache/lucene/index/FieldInfo;)V

    return-object v3

    .line 233
    .end local v4    # "number":I
    .restart local v16    # "number":I
    :cond_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/index/FieldInfo;

    .line 234
    .local v12, "info":Lorg/apache/lucene/index/FieldInfo;
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/facet/util/FacetsPayloadMigrationReader;->fieldTerms:Ljava/util/Map;

    iget-object v3, v12, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 236
    new-instance v1, Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v12, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    const/4 v3, 0x1

    iget v4, v12, Lorg/apache/lucene/index/FieldInfo;->number:I

    .line 237
    invoke-virtual {v12}, Lorg/apache/lucene/index/FieldInfo;->hasVectors()Z

    move-result v5

    invoke-virtual {v12}, Lorg/apache/lucene/index/FieldInfo;->omitsNorms()Z

    move-result v6

    invoke-virtual {v12}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v7

    .line 238
    invoke-virtual {v12}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v8

    sget-object v9, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 239
    invoke-virtual {v12}, Lorg/apache/lucene/index/FieldInfo;->getNormType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v10

    invoke-virtual {v12}, Lorg/apache/lucene/index/FieldInfo;->attributes()Ljava/util/Map;

    move-result-object v11

    invoke-direct/range {v1 .. v11}, Lorg/apache/lucene/index/FieldInfo;-><init>(Ljava/lang/String;ZIZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Ljava/util/Map;)V

    .line 236
    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    iget-object v1, v12, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v15, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 244
    :goto_2
    iget v1, v12, Lorg/apache/lucene/index/FieldInfo;->number:I

    move/from16 v0, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .end local v16    # "number":I
    .restart local v4    # "number":I
    move/from16 v16, v4

    .end local v4    # "number":I
    .restart local v16    # "number":I
    goto :goto_0

    .line 242
    :cond_1
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 246
    .end local v12    # "info":Lorg/apache/lucene/index/FieldInfo;
    .end local v16    # "number":I
    .restart local v4    # "number":I
    :cond_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 247
    .local v2, "field":Ljava/lang/String;
    new-instance v1, Lorg/apache/lucene/index/FieldInfo;

    const/4 v3, 0x0

    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 248
    const/4 v8, 0x0

    sget-object v9, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v1 .. v11}, Lorg/apache/lucene/index/FieldInfo;-><init>(Ljava/lang/String;ZIZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Ljava/util/Map;)V

    .line 247
    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
