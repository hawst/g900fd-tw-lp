.class public final Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;
.super Lorg/apache/lucene/facet/search/IntRollupFacetsAggregator;
.source "FastCountingFacetsAggregator.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final buf:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/facet/search/IntRollupFacetsAggregator;-><init>()V

    .line 41
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;->buf:Lorg/apache/lucene/util/BytesRef;

    .line 39
    return-void
.end method

.method static final verifySearchParams(Lorg/apache/lucene/facet/params/FacetSearchParams;)Z
    .locals 5
    .param p0, "fsp"    # Lorg/apache/lucene/facet/params/FacetSearchParams;

    .prologue
    .line 50
    iget-object v2, p0, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 57
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 50
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 51
    .local v1, "fr":Lorg/apache/lucene/facet/search/FacetRequest;
    iget-object v3, p0, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    iget-object v4, v1, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v0

    .line 52
    .local v0, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    invoke-virtual {v0}, Lorg/apache/lucene/facet/params/CategoryListParams;->createEncoder()Lorg/apache/lucene/facet/encoding/IntEncoder;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/facet/encoding/IntEncoder;->createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-class v4, Lorg/apache/lucene/facet/encoding/DGapVInt8IntDecoder;

    if-eq v3, v4, :cond_0

    .line 53
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 13
    .param p1, "matchingDocs"    # Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    .param p2, "clp"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    sget-boolean v10, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;->$assertionsDisabled:Z

    if-nez v10, :cond_0

    invoke-virtual {p2}, Lorg/apache/lucene/facet/params/CategoryListParams;->createEncoder()Lorg/apache/lucene/facet/encoding/IntEncoder;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/lucene/facet/encoding/IntEncoder;->createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    const-class v11, Lorg/apache/lucene/facet/encoding/DGapVInt8IntDecoder;

    if-eq v10, v11, :cond_0

    new-instance v10, Ljava/lang/AssertionError;

    .line 64
    const-string v11, "this aggregator assumes ordinals were encoded as dgap+vint"

    invoke-direct {v10, v11}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v10

    .line 66
    :cond_0
    iget-object v10, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v10}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v10

    iget-object v11, p2, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v3

    .line 67
    .local v3, "dv":Lorg/apache/lucene/index/BinaryDocValues;
    if-nez v3, :cond_2

    .line 96
    :cond_1
    return-void

    .line 71
    :cond_2
    iget-object v10, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v10}, Lorg/apache/lucene/util/FixedBitSet;->length()I

    move-result v4

    .line 72
    .local v4, "length":I
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/facet/search/FacetArrays;->getIntArray()[I

    move-result-object v1

    .line 73
    .local v1, "counts":[I
    const/4 v2, 0x0

    .line 74
    .local v2, "doc":I
    :goto_0
    if-ge v2, v4, :cond_1

    iget-object v10, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v10, v2}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v2

    const/4 v10, -0x1

    if-eq v2, v10, :cond_1

    .line 75
    iget-object v10, p0, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;->buf:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v2, v10}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 76
    iget-object v10, p0, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;->buf:Lorg/apache/lucene/util/BytesRef;

    iget v10, v10, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lez v10, :cond_3

    .line 78
    iget-object v10, p0, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;->buf:Lorg/apache/lucene/util/BytesRef;

    iget v10, v10, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v11, p0, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;->buf:Lorg/apache/lucene/util/BytesRef;

    iget v11, v11, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v9, v10, v11

    .line 79
    .local v9, "upto":I
    const/4 v7, 0x0

    .line 80
    .local v7, "ord":I
    iget-object v10, p0, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;->buf:Lorg/apache/lucene/util/BytesRef;

    iget v5, v10, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 81
    .local v5, "offset":I
    const/4 v8, 0x0

    .local v8, "prev":I
    move v6, v5

    .line 82
    .end local v5    # "offset":I
    .local v6, "offset":I
    :goto_1
    if-lt v6, v9, :cond_4

    .line 94
    .end local v6    # "offset":I
    .end local v7    # "ord":I
    .end local v8    # "prev":I
    .end local v9    # "upto":I
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 83
    .restart local v6    # "offset":I
    .restart local v7    # "ord":I
    .restart local v8    # "prev":I
    .restart local v9    # "upto":I
    :cond_4
    iget-object v10, p0, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;->buf:Lorg/apache/lucene/util/BytesRef;

    iget-object v10, v10, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "offset":I
    .restart local v5    # "offset":I
    aget-byte v0, v10, v6

    .line 84
    .local v0, "b":B
    if-ltz v0, :cond_6

    .line 85
    shl-int/lit8 v10, v7, 0x7

    or-int/2addr v10, v0

    add-int v7, v10, v8

    move v8, v7

    .line 86
    sget-boolean v10, Lorg/apache/lucene/facet/search/FastCountingFacetsAggregator;->$assertionsDisabled:Z

    if-nez v10, :cond_5

    array-length v10, v1

    if-lt v7, v10, :cond_5

    new-instance v10, Ljava/lang/AssertionError;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "ord="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " vs maxOrd="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    array-length v12, v1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v10

    .line 87
    :cond_5
    aget v10, v1, v7

    add-int/lit8 v10, v10, 0x1

    aput v10, v1, v7

    .line 88
    const/4 v7, 0x0

    move v6, v5

    .line 89
    .end local v5    # "offset":I
    .restart local v6    # "offset":I
    goto :goto_1

    .line 90
    .end local v6    # "offset":I
    .restart local v5    # "offset":I
    :cond_6
    shl-int/lit8 v10, v7, 0x7

    and-int/lit8 v11, v0, 0x7f

    or-int v7, v10, v11

    move v6, v5

    .end local v5    # "offset":I
    .restart local v6    # "offset":I
    goto :goto_1
.end method
