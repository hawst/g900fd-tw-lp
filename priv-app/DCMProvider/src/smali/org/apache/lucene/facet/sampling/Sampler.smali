.class public abstract Lorg/apache/lucene/facet/sampling/Sampler;
.super Ljava/lang/Object;
.source "Sampler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;,
        Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;
    }
.end annotation


# instance fields
.field protected final samplingParams:Lorg/apache/lucene/facet/sampling/SamplingParams;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lorg/apache/lucene/facet/sampling/SamplingParams;

    invoke-direct {v0}, Lorg/apache/lucene/facet/sampling/SamplingParams;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/sampling/Sampler;-><init>(Lorg/apache/lucene/facet/sampling/SamplingParams;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/sampling/SamplingParams;)V
    .locals 2
    .param p1, "params"    # Lorg/apache/lucene/facet/sampling/SamplingParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-virtual {p1}, Lorg/apache/lucene/facet/sampling/SamplingParams;->validate()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The provided SamplingParams are not valid!!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/facet/sampling/Sampler;->samplingParams:Lorg/apache/lucene/facet/sampling/SamplingParams;

    .line 68
    return-void
.end method

.method private trimSubResults(Lorg/apache/lucene/facet/search/FacetResultNode;I)V
    .locals 5
    .param p1, "node"    # Lorg/apache/lucene/facet/search/FacetResultNode;
    .param p2, "size"    # I

    .prologue
    .line 178
    iget-object v3, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    sget-object v4, Lorg/apache/lucene/facet/search/FacetResultNode;->EMPTY_SUB_RESULTS:Ljava/util/List;

    if-eq v3, v4, :cond_0

    iget-object v3, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 183
    .local v1, "trimmed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/facet/search/FacetResultNode;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    if-lt v0, p2, :cond_3

    .line 189
    :cond_2
    iput-object v1, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    goto :goto_0

    .line 184
    :cond_3
    iget-object v3, p1, Lorg/apache/lucene/facet/search/FacetResultNode;->subResults:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/facet/search/FacetResultNode;

    .line 185
    .local v2, "trimmedNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    invoke-direct {p0, v2, p2}, Lorg/apache/lucene/facet/sampling/Sampler;->trimSubResults(Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    .line 186
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected abstract createSample(Lorg/apache/lucene/facet/search/ScoredDocIDs;II)Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getSampleFixer(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/sampling/SampleFixer;
    .locals 1
    .param p1, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p3, "searchParams"    # Lorg/apache/lucene/facet/params/FacetSearchParams;

    .prologue
    .line 120
    new-instance v0, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/lucene/facet/sampling/TakmiSampleFixer;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetSearchParams;)V

    return-object v0
.end method

.method public getSampleSet(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;
    .locals 6
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/sampling/Sampler;->shouldSample(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    new-instance v2, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-direct {v2, p1, v4, v5}, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;-><init>(Lorg/apache/lucene/facet/search/ScoredDocIDs;D)V

    .line 100
    :goto_0
    return-object v2

    .line 95
    :cond_0
    invoke-interface {p1}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->size()I

    move-result v0

    .line 96
    .local v0, "actualSize":I
    int-to-double v2, v0

    iget-object v4, p0, Lorg/apache/lucene/facet/sampling/Sampler;->samplingParams:Lorg/apache/lucene/facet/sampling/SamplingParams;

    invoke-virtual {v4}, Lorg/apache/lucene/facet/sampling/SamplingParams;->getSampleRatio()D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 97
    .local v1, "sampleSetSize":I
    iget-object v2, p0, Lorg/apache/lucene/facet/sampling/Sampler;->samplingParams:Lorg/apache/lucene/facet/sampling/SamplingParams;

    invoke-virtual {v2}, Lorg/apache/lucene/facet/sampling/SamplingParams;->getMinSampleSize()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 98
    iget-object v2, p0, Lorg/apache/lucene/facet/sampling/Sampler;->samplingParams:Lorg/apache/lucene/facet/sampling/SamplingParams;

    invoke-virtual {v2}, Lorg/apache/lucene/facet/sampling/SamplingParams;->getMaxSampleSize()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 100
    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/facet/sampling/Sampler;->createSample(Lorg/apache/lucene/facet/search/ScoredDocIDs;II)Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;

    move-result-object v2

    goto :goto_0
.end method

.method public final getSamplingParams()Lorg/apache/lucene/facet/sampling/SamplingParams;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lorg/apache/lucene/facet/sampling/Sampler;->samplingParams:Lorg/apache/lucene/facet/sampling/SamplingParams;

    return-object v0
.end method

.method public overSampledSearchParams(Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/params/FacetSearchParams;
    .locals 10
    .param p1, "original"    # Lorg/apache/lucene/facet/params/FacetSearchParams;

    .prologue
    .line 196
    move-object v5, p1

    .line 198
    .local v5, "res":Lorg/apache/lucene/facet/params/FacetSearchParams;
    invoke-virtual {p0}, Lorg/apache/lucene/facet/sampling/Sampler;->getSamplingParams()Lorg/apache/lucene/facet/sampling/SamplingParams;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/facet/sampling/SamplingParams;->getOversampleFactor()D

    move-result-wide v2

    .line 199
    .local v2, "overSampleFactor":D
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v6, v2, v6

    if-lez v6, :cond_0

    .line 200
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 201
    .local v0, "facetRequests":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetRequest;>;"
    iget-object v6, p1, Lorg/apache/lucene/facet/params/FacetSearchParams;->facetRequests:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 205
    new-instance v5, Lorg/apache/lucene/facet/params/FacetSearchParams;

    .end local v5    # "res":Lorg/apache/lucene/facet/params/FacetSearchParams;
    iget-object v6, p1, Lorg/apache/lucene/facet/params/FacetSearchParams;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {v5, v6, v0}, Lorg/apache/lucene/facet/params/FacetSearchParams;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;Ljava/util/List;)V

    .line 207
    .end local v0    # "facetRequests":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetRequest;>;"
    .restart local v5    # "res":Lorg/apache/lucene/facet/params/FacetSearchParams;
    :cond_0
    return-object v5

    .line 201
    .restart local v0    # "facetRequests":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetRequest;>;"
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 202
    .local v1, "frq":Lorg/apache/lucene/facet/search/FacetRequest;
    iget v7, v1, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    int-to-double v8, v7

    mul-double/2addr v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v4, v8

    .line 203
    .local v4, "overSampledNumResults":I
    new-instance v7, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;

    invoke-direct {v7, v1, v4}, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;I)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public shouldSample(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Z
    .locals 2
    .param p1, "docIds"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;

    .prologue
    .line 74
    invoke-interface {p1}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->size()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/facet/sampling/Sampler;->samplingParams:Lorg/apache/lucene/facet/sampling/SamplingParams;

    invoke-virtual {v1}, Lorg/apache/lucene/facet/sampling/SamplingParams;->getSamplingThreshold()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public trimResult(Lorg/apache/lucene/facet/search/FacetResult;)Lorg/apache/lucene/facet/search/FacetResult;
    .locals 8
    .param p1, "facetResult"    # Lorg/apache/lucene/facet/search/FacetResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-virtual {p0}, Lorg/apache/lucene/facet/sampling/Sampler;->getSamplingParams()Lorg/apache/lucene/facet/sampling/SamplingParams;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/facet/sampling/SamplingParams;->getOversampleFactor()D

    move-result-wide v2

    .line 153
    .local v2, "overSampleFactor":D
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpg-double v6, v2, v6

    if-gtz v6, :cond_0

    .line 173
    .end local p1    # "facetResult":Lorg/apache/lucene/facet/search/FacetResult;
    :goto_0
    return-object p1

    .line 157
    .restart local p1    # "facetResult":Lorg/apache/lucene/facet/search/FacetResult;
    :cond_0
    const/4 v4, 0x0

    .line 160
    .local v4, "sampledFreq":Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetResult;->getFacetRequest()Lorg/apache/lucene/facet/search/FacetRequest;

    move-result-object v4

    .end local v4    # "sampledFreq":Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;
    check-cast v4, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    .restart local v4    # "sampledFreq":Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;
    iget-object v1, v4, Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;->orig:Lorg/apache/lucene/facet/search/FacetRequest;

    .line 170
    .local v1, "origFrq":Lorg/apache/lucene/facet/search/FacetRequest;
    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetResult;->getFacetResultNode()Lorg/apache/lucene/facet/search/FacetResultNode;

    move-result-object v5

    .line 171
    .local v5, "trimmedRootNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    iget v6, v1, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    invoke-direct {p0, v5, v6}, Lorg/apache/lucene/facet/sampling/Sampler;->trimSubResults(Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    .line 173
    new-instance v6, Lorg/apache/lucene/facet/search/FacetResult;

    invoke-virtual {p1}, Lorg/apache/lucene/facet/search/FacetResult;->getNumValidDescendants()I

    move-result v7

    invoke-direct {v6, v1, v5, v7}, Lorg/apache/lucene/facet/search/FacetResult;-><init>(Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetResultNode;I)V

    move-object p1, v6

    goto :goto_0

    .line 161
    .end local v1    # "origFrq":Lorg/apache/lucene/facet/search/FacetRequest;
    .end local v4    # "sampledFreq":Lorg/apache/lucene/facet/sampling/Sampler$OverSampledFacetRequest;
    .end local v5    # "trimmedRootNode":Lorg/apache/lucene/facet/search/FacetResultNode;
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    .line 163
    const-string v7, "It is only valid to call this method with result obtained for a facet request created through sampler.overSamlpingSearchParams()"

    .line 162
    invoke-direct {v6, v7, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
.end method
