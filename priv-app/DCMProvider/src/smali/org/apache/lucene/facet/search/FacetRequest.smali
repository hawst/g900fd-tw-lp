.class public abstract Lorg/apache/lucene/facet/search/FacetRequest;
.super Ljava/lang/Object;
.source "FacetRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;,
        Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;,
        Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;
    }
.end annotation


# static fields
.field public static final DEFAULT_DEPTH:I = 0x1

.field public static final DEFAULT_RESULT_MODE:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;


# instance fields
.field public final categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

.field private depth:I

.field private final hashCode:I

.field private numLabel:I

.field public final numResults:I

.field private resultMode:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

.field private sortOrder:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;->PER_NODE_IN_TREE:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    sput-object v0, Lorg/apache/lucene/facet/search/FacetRequest;->DEFAULT_RESULT_MODE:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V
    .locals 3
    .param p1, "path"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "numResults"    # I

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    sget-object v0, Lorg/apache/lucene/facet/search/FacetRequest;->DEFAULT_RESULT_MODE:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetRequest;->resultMode:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    .line 112
    if-gtz p2, :cond_0

    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "num results must be a positive (>0) number: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_0
    if-nez p1, :cond_1

    .line 116
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "category path cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 119
    iput p2, p0, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    .line 120
    iput p2, p0, Lorg/apache/lucene/facet/search/FacetRequest;->numLabel:I

    .line 121
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/search/FacetRequest;->depth:I

    .line 122
    sget-object v0, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;->DESCENDING:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    iput-object v0, p0, Lorg/apache/lucene/facet/search/FacetRequest;->sortOrder:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    .line 124
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->hashCode()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    xor-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/search/FacetRequest;->hashCode:I

    .line 125
    return-void
.end method


# virtual methods
.method public createAggregator(ZLorg/apache/lucene/facet/search/FacetArrays;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/search/Aggregator;
    .locals 2
    .param p1, "useComplements"    # Z
    .param p2, "arrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p3, "taxonomy"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this FacetRequest does not support this type of Aggregator anymore; you should override FacetsAccumulator to return the proper FacetsAggregator"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 149
    instance-of v2, p1, Lorg/apache/lucene/facet/search/FacetRequest;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 150
    check-cast v0, Lorg/apache/lucene/facet/search/FacetRequest;

    .line 151
    .local v0, "that":Lorg/apache/lucene/facet/search/FacetRequest;
    iget v2, v0, Lorg/apache/lucene/facet/search/FacetRequest;->hashCode:I

    iget v3, p0, Lorg/apache/lucene/facet/search/FacetRequest;->hashCode:I

    if-ne v2, v3, :cond_0

    .line 152
    iget-object v2, v0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    iget v2, v0, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    iget v3, p0, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    if-ne v2, v3, :cond_0

    .line 154
    iget v2, v0, Lorg/apache/lucene/facet/search/FacetRequest;->depth:I

    iget v3, p0, Lorg/apache/lucene/facet/search/FacetRequest;->depth:I

    if-ne v2, v3, :cond_0

    .line 155
    iget-object v2, v0, Lorg/apache/lucene/facet/search/FacetRequest;->resultMode:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    iget-object v3, p0, Lorg/apache/lucene/facet/search/FacetRequest;->resultMode:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    if-ne v2, v3, :cond_0

    .line 156
    iget v2, v0, Lorg/apache/lucene/facet/search/FacetRequest;->numLabel:I

    iget v3, p0, Lorg/apache/lucene/facet/search/FacetRequest;->numLabel:I

    if-ne v2, v3, :cond_0

    .line 151
    const/4 v1, 0x1

    .line 158
    .end local v0    # "that":Lorg/apache/lucene/facet/search/FacetRequest;
    :cond_0
    return v1
.end method

.method public final getDepth()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lorg/apache/lucene/facet/search/FacetRequest;->depth:I

    return v0
.end method

.method public abstract getFacetArraysSource()Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
.end method

.method public final getNumLabel()I
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lorg/apache/lucene/facet/search/FacetRequest;->numLabel:I

    return v0
.end method

.method public final getResultMode()Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetRequest;->resultMode:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    return-object v0
.end method

.method public final getSortOrder()Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/lucene/facet/search/FacetRequest;->sortOrder:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    return-object v0
.end method

.method public abstract getValueOf(Lorg/apache/lucene/facet/search/FacetArrays;I)D
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lorg/apache/lucene/facet/search/FacetRequest;->hashCode:I

    return v0
.end method

.method public setDepth(I)V
    .locals 0
    .param p1, "depth"    # I

    .prologue
    .line 245
    iput p1, p0, Lorg/apache/lucene/facet/search/FacetRequest;->depth:I

    .line 246
    return-void
.end method

.method public setNumLabel(I)V
    .locals 0
    .param p1, "numLabel"    # I

    .prologue
    .line 249
    iput p1, p0, Lorg/apache/lucene/facet/search/FacetRequest;->numLabel:I

    .line 250
    return-void
.end method

.method public setResultMode(Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;)V
    .locals 0
    .param p1, "resultMode"    # Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    .prologue
    .line 257
    iput-object p1, p0, Lorg/apache/lucene/facet/search/FacetRequest;->resultMode:Lorg/apache/lucene/facet/search/FacetRequest$ResultMode;

    .line 258
    return-void
.end method

.method public setSortOrder(Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;)V
    .locals 0
    .param p1, "sortOrder"    # Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    .prologue
    .line 261
    iput-object p1, p0, Lorg/apache/lucene/facet/search/FacetRequest;->sortOrder:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    .line 262
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 266
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/facet/search/FacetRequest;->categoryPath:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " nRes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " nLbl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/facet/search/FacetRequest;->numLabel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
