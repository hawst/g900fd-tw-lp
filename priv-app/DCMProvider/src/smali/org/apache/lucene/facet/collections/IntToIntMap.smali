.class public Lorg/apache/lucene/facet/collections/IntToIntMap;
.super Ljava/lang/Object;
.source "IntToIntMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;,
        Lorg/apache/lucene/facet/collections/IntToIntMap$KeyIterator;,
        Lorg/apache/lucene/facet/collections/IntToIntMap$ValueIterator;
    }
.end annotation


# static fields
.field public static final GROUD:I = -0x1

.field private static defaultCapacity:I


# instance fields
.field baseHash:[I

.field private capacity:I

.field private firstEmpty:I

.field private hashFactor:I

.field keys:[I

.field next:[I

.field private prev:I

.field private size:I

.field values:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    const/16 v0, 0x10

    sput v0, Lorg/apache/lucene/facet/collections/IntToIntMap;->defaultCapacity:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 209
    sget v0, Lorg/apache/lucene/facet/collections/IntToIntMap;->defaultCapacity:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/collections/IntToIntMap;-><init>(I)V

    .line 210
    return-void
.end method

.method public constructor <init>(I)V
    .locals 5
    .param p1, "capacity"    # I

    .prologue
    const/4 v4, 0x0

    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    const/16 v2, 0x10

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    .line 222
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    if-lt v2, p1, :cond_0

    .line 229
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    add-int/lit8 v0, v2, 0x1

    .line 231
    .local v0, "arrayLength":I
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->values:[I

    .line 232
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->keys:[I

    .line 233
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    .line 235
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->values:[I

    const/4 v3, -0x1

    aput v3, v2, v4

    .line 238
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    shl-int/lit8 v1, v2, 0x1

    .line 240
    .local v1, "baseHashSize":I
    new-array v2, v1, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    .line 244
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->hashFactor:I

    .line 246
    iput v4, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    .line 248
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToIntMap;->clear()V

    .line 249
    return-void

    .line 224
    .end local v0    # "arrayLength":I
    .end local v1    # "baseHashSize":I
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    goto :goto_0
.end method

.method private findForRemove(II)I
    .locals 3
    .param p1, "key"    # I
    .param p2, "baseHashIndex"    # I

    .prologue
    const/4 v1, 0x0

    .line 382
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->prev:I

    .line 383
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    aget v0, v2, p2

    .line 386
    .local v0, "index":I
    :goto_0
    if-nez v0, :cond_1

    .line 399
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->prev:I

    move v0, v1

    .line 400
    .end local v0    # "index":I
    :cond_0
    return v0

    .line 388
    .restart local v0    # "index":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->keys:[I

    aget v2, v2, v0

    if-eq v2, p1, :cond_0

    .line 393
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->prev:I

    .line 394
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    aget v0, v2, v0

    goto :goto_0
.end method

.method private getBaseHashAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private prvt_put(II)V
    .locals 4
    .param p1, "key"    # I
    .param p2, "e"    # I

    .prologue
    .line 265
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToIntMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 268
    .local v0, "hashIndex":I
    iget v1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->firstEmpty:I

    .line 271
    .local v1, "objectIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->firstEmpty:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->firstEmpty:I

    .line 272
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->values:[I

    aput p2, v2, v1

    .line 273
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->keys:[I

    aput p1, v2, v1

    .line 276
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 277
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    aput v1, v2, v0

    .line 280
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    .line 281
    return-void
.end method


# virtual methods
.method protected calcBaseHashIndex(I)I
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 287
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->hashFactor:I

    and-int/2addr v0, p1

    return v0
.end method

.method public clear()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 295
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 298
    iput v4, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    .line 303
    const/4 v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->firstEmpty:I

    .line 307
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    if-lt v0, v2, :cond_0

    .line 312
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    aput v4, v2, v3

    .line 313
    return-void

    .line 308
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput v1, v2, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public containsKey(I)Z
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 323
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToIntMap;->find(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(I)Z
    .locals 2
    .param p1, "v"    # I

    .prologue
    .line 336
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToIntMap;->iterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v0

    .local v0, "iterator":Lorg/apache/lucene/facet/collections/IntIterator;
    :cond_0
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 341
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 337
    :cond_1
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 338
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 601
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/facet/collections/IntToIntMap;

    .line 602
    .local v2, "that":Lorg/apache/lucene/facet/collections/IntToIntMap;
    invoke-virtual {v2}, Lorg/apache/lucene/facet/collections/IntToIntMap;->size()I

    move-result v6

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToIntMap;->size()I

    move-result v7

    if-eq v6, v7, :cond_1

    .line 620
    :cond_0
    :goto_0
    return v5

    .line 606
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToIntMap;->keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v0

    .line 607
    .local v0, "it":Lorg/apache/lucene/facet/collections/IntIterator;
    :cond_2
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 620
    const/4 v5, 0x1

    goto :goto_0

    .line 608
    :cond_3
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    .line 610
    .local v1, "key":I
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/IntToIntMap;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 614
    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/collections/IntToIntMap;->get(I)I

    move-result v3

    .line 615
    .local v3, "v1":I
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/IntToIntMap;->get(I)I

    move-result v4

    .line 616
    .local v4, "v2":I
    if-eq v3, v4, :cond_2

    goto :goto_0
.end method

.method protected find(I)I
    .locals 3
    .param p1, "key"    # I

    .prologue
    .line 351
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToIntMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 354
    .local v0, "baseHashIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    aget v1, v2, v0

    .line 357
    .local v1, "localIndex":I
    :goto_0
    if-nez v1, :cond_1

    .line 369
    const/4 v1, 0x0

    .end local v1    # "localIndex":I
    :cond_0
    return v1

    .line 359
    .restart local v1    # "localIndex":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->keys:[I

    aget v2, v2, v1

    if-eq v2, p1, :cond_0

    .line 364
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    aget v1, v2, v1

    goto :goto_0
.end method

.method public get(I)I
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 411
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->values:[I

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToIntMap;->find(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method protected grow()V
    .locals 5

    .prologue
    .line 419
    new-instance v2, Lorg/apache/lucene/facet/collections/IntToIntMap;

    .line 420
    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    mul-int/lit8 v3, v3, 0x2

    .line 419
    invoke-direct {v2, v3}, Lorg/apache/lucene/facet/collections/IntToIntMap;-><init>(I)V

    .line 425
    .local v2, "that":Lorg/apache/lucene/facet/collections/IntToIntMap;
    new-instance v1, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;

    invoke-direct {v1, p0}, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToIntMap;)V

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 431
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    .line 432
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    .line 433
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToIntMap;->firstEmpty:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->firstEmpty:I

    .line 434
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToIntMap;->values:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->values:[I

    .line 435
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToIntMap;->keys:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->keys:[I

    .line 436
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    .line 437
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    .line 438
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToIntMap;->hashFactor:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->hashFactor:I

    .line 439
    return-void

    .line 426
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/IntToIntMap$IndexIterator;->next()I

    move-result v0

    .line 427
    .local v0, "index":I
    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->keys:[I

    aget v3, v3, v0

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->values:[I

    aget v4, v4, v0

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/facet/collections/IntToIntMap;->prvt_put(II)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 596
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToIntMap;->size()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 446
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Lorg/apache/lucene/facet/collections/IntIterator;
    .locals 1

    .prologue
    .line 453
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToIntMap$ValueIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/IntToIntMap$ValueIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToIntMap;)V

    return-object v0
.end method

.method public keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;
    .locals 1

    .prologue
    .line 458
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToIntMap$KeyIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/IntToIntMap$KeyIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToIntMap;)V

    return-object v0
.end method

.method public put(II)I
    .locals 4
    .param p1, "key"    # I
    .param p2, "e"    # I

    .prologue
    .line 478
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToIntMap;->find(I)I

    move-result v0

    .line 481
    .local v0, "index":I
    if-eqz v0, :cond_0

    .line 483
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->values:[I

    aget v1, v2, v0

    .line 484
    .local v1, "old":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->values:[I

    aput p2, v2, v0

    .line 498
    .end local v1    # "old":I
    :goto_0
    return v1

    .line 489
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->capacity:I

    if-ne v2, v3, :cond_1

    .line 491
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToIntMap;->grow()V

    .line 496
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/collections/IntToIntMap;->prvt_put(II)V

    .line 498
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public remove(I)I
    .locals 5
    .param p1, "key"    # I

    .prologue
    .line 509
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToIntMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 510
    .local v0, "baseHashIndex":I
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/collections/IntToIntMap;->findForRemove(II)I

    move-result v1

    .line 511
    .local v1, "index":I
    if-eqz v1, :cond_1

    .line 514
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->prev:I

    if-nez v2, :cond_0

    .line 515
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->baseHash:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    aget v3, v3, v1

    aput v3, v2, v0

    .line 518
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->prev:I

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    aget v4, v4, v1

    aput v4, v2, v3

    .line 519
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->firstEmpty:I

    aput v3, v2, v1

    .line 520
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->firstEmpty:I

    .line 521
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    .line 522
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->values:[I

    aget v2, v2, v1

    .line 525
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 532
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    return v0
.end method

.method public toArray()[I
    .locals 4

    .prologue
    .line 541
    const/4 v2, -0x1

    .line 542
    .local v2, "j":I
    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    new-array v0, v3, [I

    .line 545
    .local v0, "array":[I
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToIntMap;->iterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v1

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/IntIterator;
    :goto_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 548
    return-object v0

    .line 546
    :cond_0
    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v3

    aput v3, v0, v2

    goto :goto_0
.end method

.method public toArray([I)[I
    .locals 4
    .param p1, "a"    # [I

    .prologue
    .line 563
    const/4 v1, 0x0

    .line 564
    .local v1, "j":I
    array-length v2, p1

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    if-ge v2, v3, :cond_0

    .line 565
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToIntMap;->size:I

    new-array p1, v2, [I

    .line 568
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToIntMap;->iterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v0

    .local v0, "iterator":Lorg/apache/lucene/facet/collections/IntIterator;
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 569
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 572
    :cond_1
    return-object p1

    .line 570
    :cond_2
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v2

    aput v2, p1, v1

    .line 569
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 577
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 578
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/16 v3, 0x7b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 579
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToIntMap;->keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v1

    .line 580
    .local v1, "keyIterator":Lorg/apache/lucene/facet/collections/IntIterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 590
    const/16 v3, 0x7d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 591
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 581
    :cond_1
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v0

    .line 582
    .local v0, "key":I
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 583
    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 584
    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/collections/IntToIntMap;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 585
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 586
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 587
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
