.class public Lorg/apache/lucene/facet/sampling/SamplingWrapper;
.super Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;
.source "SamplingWrapper.java"


# instance fields
.field private delegee:Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

.field private sampler:Lorg/apache/lucene/facet/sampling/Sampler;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;Lorg/apache/lucene/facet/sampling/Sampler;)V
    .locals 3
    .param p1, "delegee"    # Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;
    .param p2, "sampler"    # Lorg/apache/lucene/facet/sampling/Sampler;

    .prologue
    .line 46
    iget-object v0, p1, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    iget-object v1, p1, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->indexReader:Lorg/apache/lucene/index/IndexReader;

    iget-object v2, p1, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)V

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->delegee:Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

    .line 48
    iput-object p2, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    .line 49
    return-void
.end method


# virtual methods
.method public accumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Ljava/util/List;
    .locals 11
    .param p1, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/search/ScoredDocIDs;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResult;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v6, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->delegee:Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

    iget-object v3, v6, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    .line 55
    .local v3, "original":Lorg/apache/lucene/facet/params/FacetSearchParams;
    iget-object v6, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->delegee:Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

    iget-object v7, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    invoke-virtual {v7, v3}, Lorg/apache/lucene/facet/sampling/Sampler;->overSampledSearchParams(Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/params/FacetSearchParams;

    move-result-object v7

    iput-object v7, v6, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    .line 57
    iget-object v6, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    invoke-virtual {v6, p1}, Lorg/apache/lucene/facet/sampling/Sampler;->getSampleSet(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;

    move-result-object v5

    .line 59
    .local v5, "sampleSet":Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;
    iget-object v6, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->delegee:Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

    iget-object v7, v5, Lorg/apache/lucene/facet/sampling/Sampler$SampleResult;->docids:Lorg/apache/lucene/facet/search/ScoredDocIDs;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->accumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Ljava/util/List;

    move-result-object v4

    .line 61
    .local v4, "sampleRes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v0, "fixedRes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/search/FacetResult;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_0

    .line 77
    iget-object v6, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->delegee:Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

    iput-object v3, v6, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    .line 79
    return-object v0

    .line 62
    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/facet/search/FacetResult;

    .line 64
    .local v1, "fres":Lorg/apache/lucene/facet/search/FacetResult;
    invoke-virtual {v1}, Lorg/apache/lucene/facet/search/FacetResult;->getFacetRequest()Lorg/apache/lucene/facet/search/FacetRequest;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->createFacetResultsHandler(Lorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;

    move-result-object v2

    .line 66
    .local v2, "frh":Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;
    iget-object v7, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    iget-object v8, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->indexReader:Lorg/apache/lucene/index/IndexReader;

    iget-object v9, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    iget-object v10, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->searchParams:Lorg/apache/lucene/facet/params/FacetSearchParams;

    invoke-virtual {v7, v8, v9, v10}, Lorg/apache/lucene/facet/sampling/Sampler;->getSampleFixer(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetSearchParams;)Lorg/apache/lucene/facet/sampling/SampleFixer;

    move-result-object v7

    invoke-interface {v7, p1, v1}, Lorg/apache/lucene/facet/sampling/SampleFixer;->fixResult(Lorg/apache/lucene/facet/search/ScoredDocIDs;Lorg/apache/lucene/facet/search/FacetResult;)V

    .line 67
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;->rearrangeFacetResult(Lorg/apache/lucene/facet/search/FacetResult;)Lorg/apache/lucene/facet/search/FacetResult;

    move-result-object v1

    .line 70
    iget-object v7, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->sampler:Lorg/apache/lucene/facet/sampling/Sampler;

    invoke-virtual {v7, v1}, Lorg/apache/lucene/facet/sampling/Sampler;->trimResult(Lorg/apache/lucene/facet/search/FacetResult;)Lorg/apache/lucene/facet/search/FacetResult;

    move-result-object v1

    .line 73
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;->labelResult(Lorg/apache/lucene/facet/search/FacetResult;)V

    .line 74
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getComplementThreshold()D
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->delegee:Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->getComplementThreshold()D

    move-result-wide v0

    return-wide v0
.end method

.method public setComplementThreshold(D)V
    .locals 1
    .param p1, "complementThreshold"    # D

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/facet/sampling/SamplingWrapper;->delegee:Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->setComplementThreshold(D)V

    .line 90
    return-void
.end method
