.class Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2$1;
.super Ljava/lang/Object;
.source "ScoredDocIdsUtils.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;->iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;

.field private final synthetic val$docIterator:Lorg/apache/lucene/search/DocIdSetIterator;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2$1;->this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;

    iput-object p2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2$1;->val$docIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDocID()I
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2$1;->val$docIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v0

    return v0
.end method

.method public getScore()F
    .locals 1

    .prologue
    .line 232
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public next()Z
    .locals 3

    .prologue
    .line 225
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2$1;->val$docIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v1}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
