.class final enum Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;
.super Ljava/lang/Enum;
.source "TotalFacetCounts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/complements/TotalFacetCounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "CreationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum Computed:Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

.field public static final enum Loaded:Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 70
    new-instance v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    const-string v1, "Computed"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;->Computed:Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    new-instance v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    const-string v1, "Loaded"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;->Loaded:Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    sget-object v1, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;->Computed:Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;->Loaded:Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;->ENUM$VALUES:[Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;->ENUM$VALUES:[Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
