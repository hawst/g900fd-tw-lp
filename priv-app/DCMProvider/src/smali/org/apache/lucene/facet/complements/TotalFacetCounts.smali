.class public Lorg/apache/lucene/facet/complements/TotalFacetCounts;
.super Ljava/lang/Object;
.source "TotalFacetCounts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;
    }
.end annotation


# static fields
.field private static final DUMMY_REQ:Lorg/apache/lucene/facet/search/FacetRequest;

.field private static final atomicGen4Test:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field final createType4test:Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

.field private final facetIndexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

.field final gen4test:I

.field private final taxonomy:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

.field private totalCounts:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->atomicGen4Test:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 154
    new-instance v0, Lorg/apache/lucene/facet/search/CountFacetRequest;

    sget-object v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->EMPTY:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/search/CountFacetRequest;-><init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V

    sput-object v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->DUMMY_REQ:Lorg/apache/lucene/facet/search/FacetRequest;

    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;[[ILorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;)V
    .locals 1
    .param p1, "taxonomy"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p2, "facetIndexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .param p3, "counts"    # [[I
    .param p4, "createType4Test"    # Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->totalCounts:[[I

    .line 79
    iput-object p1, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->taxonomy:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .line 80
    iput-object p2, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->facetIndexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 81
    iput-object p3, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->totalCounts:[[I

    .line 82
    iput-object p4, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->createType4test:Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    .line 83
    sget-object v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->atomicGen4Test:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->gen4test:I

    .line 84
    return-void
.end method

.method static compute(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    .locals 7
    .param p0, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "taxonomy"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p2, "facetIndexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    invoke-static {p2, p1}, Lorg/apache/lucene/facet/util/PartitionsUtils;->partitionSize(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)I

    move-result v6

    .line 159
    .local v6, "partitionSize":I
    invoke-virtual {p1}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getSize()I

    move-result v2

    int-to-float v2, v2

    int-to-float v3, v6

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    filled-new-array {v2, v6}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[I

    .line 160
    .local v4, "counts":[[I
    new-instance v1, Lorg/apache/lucene/facet/params/FacetSearchParams;

    const/4 v2, 0x1

    new-array v2, v2, [Lorg/apache/lucene/facet/search/FacetRequest;

    const/4 v3, 0x0

    sget-object v5, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->DUMMY_REQ:Lorg/apache/lucene/facet/search/FacetRequest;

    aput-object v5, v2, v3

    invoke-direct {v1, p2, v2}, Lorg/apache/lucene/facet/params/FacetSearchParams;-><init>(Lorg/apache/lucene/facet/params/FacetIndexingParams;[Lorg/apache/lucene/facet/search/FacetRequest;)V

    .line 162
    .local v1, "newSearchParams":Lorg/apache/lucene/facet/params/FacetSearchParams;
    new-instance v0, Lorg/apache/lucene/facet/complements/TotalFacetCounts$1;

    move-object v2, p0

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/facet/complements/TotalFacetCounts$1;-><init>(Lorg/apache/lucene/facet/params/FacetSearchParams;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;[[ILorg/apache/lucene/facet/params/FacetIndexingParams;)V

    .line 175
    .local v0, "sfa":Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;
    const-wide/high16 v2, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->setComplementThreshold(D)V

    .line 176
    invoke-static {p0}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils;->createAllDocsScoredDocIDs(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/facet/search/ScoredDocIDs;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/lucene/facet/search/StandardFacetsAccumulator;->accumulate(Lorg/apache/lucene/facet/search/ScoredDocIDs;)Ljava/util/List;

    .line 177
    new-instance v2, Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    sget-object v3, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;->Computed:Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    invoke-direct {v2, p1, p2, v4, v3}, Lorg/apache/lucene/facet/complements/TotalFacetCounts;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;[[ILorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;)V

    return-object v2
.end method

.method static loadFromFile(Ljava/io/File;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;)Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    .locals 7
    .param p0, "inputFile"    # Ljava/io/File;
    .param p1, "taxonomy"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p2, "facetIndexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v5, Ljava/io/BufferedInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 116
    .local v1, "dis":Ljava/io/DataInputStream;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    new-array v0, v5, [[I

    .line 117
    .local v0, "counts":[[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_0

    .line 128
    new-instance v5, Lorg/apache/lucene/facet/complements/TotalFacetCounts;

    sget-object v6, Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;->Loaded:Lorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;

    invoke-direct {v5, p1, p2, v0, v6}, Lorg/apache/lucene/facet/complements/TotalFacetCounts;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/params/FacetIndexingParams;[[ILorg/apache/lucene/facet/complements/TotalFacetCounts$CreationType;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 128
    return-object v5

    .line 118
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    .line 119
    .local v4, "size":I
    if-gez v4, :cond_2

    .line 120
    const/4 v5, 0x0

    aput-object v5, v0, v2

    .line 117
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 122
    :cond_2
    new-array v5, v4, [I

    aput-object v5, v0, v2

    .line 123
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    if-ge v3, v4, :cond_1

    .line 124
    aget-object v5, v0, v2

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 129
    .end local v0    # "counts":[[I
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v4    # "size":I
    :catchall_0
    move-exception v5

    .line 130
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 131
    throw v5
.end method

.method static storeToFile(Ljava/io/File;Lorg/apache/lucene/facet/complements/TotalFacetCounts;)V
    .locals 9
    .param p0, "outputFile"    # Ljava/io/File;
    .param p1, "tfc"    # Lorg/apache/lucene/facet/complements/TotalFacetCounts;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 135
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/BufferedOutputStream;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 137
    .local v1, "dos":Ljava/io/DataOutputStream;
    :try_start_0
    iget-object v3, p1, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->totalCounts:[[I

    array-length v3, v3

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 138
    iget-object v6, p1, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->totalCounts:[[I

    array-length v7, v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v5, v4

    :goto_0
    if-lt v5, v7, :cond_0

    .line 149
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 151
    return-void

    .line 138
    :cond_0
    :try_start_1
    aget-object v0, v6, v5

    .line 139
    .local v0, "counts":[I
    if-nez v0, :cond_2

    .line 140
    const/4 v3, -0x1

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 138
    :cond_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 142
    :cond_2
    array-length v3, v0

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 143
    array-length v8, v0

    move v3, v4

    :goto_1
    if-ge v3, v8, :cond_1

    aget v2, v0, v3

    .line 144
    .local v2, "i":I
    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 148
    .end local v0    # "counts":[I
    .end local v2    # "i":I
    :catchall_0
    move-exception v3

    .line 149
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 150
    throw v3
.end method


# virtual methods
.method public fillTotalCountsForPartition([II)V
    .locals 5
    .param p1, "partitionArray"    # [I
    .param p2, "partition"    # I

    .prologue
    const/4 v4, 0x0

    .line 92
    array-length v2, p1

    .line 93
    .local v2, "partitionSize":I
    iget-object v3, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->totalCounts:[[I

    aget-object v0, v3, p2

    .line 94
    .local v0, "countArray":[I
    if-nez v0, :cond_0

    .line 95
    new-array v0, v2, [I

    .line 96
    iget-object v3, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->totalCounts:[[I

    aput-object v0, v3, p2

    .line 98
    :cond_0
    array-length v3, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 99
    .local v1, "length":I
    invoke-static {v0, v4, p1, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    return-void
.end method

.method public getTotalCount(I)I
    .locals 4
    .param p1, "ordinal"    # I

    .prologue
    .line 107
    iget-object v2, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->facetIndexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-static {v2, p1}, Lorg/apache/lucene/facet/util/PartitionsUtils;->partitionNumber(Lorg/apache/lucene/facet/params/FacetIndexingParams;I)I

    move-result v1

    .line 108
    .local v1, "partition":I
    iget-object v2, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->facetIndexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    iget-object v3, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->taxonomy:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-static {v2, v3}, Lorg/apache/lucene/facet/util/PartitionsUtils;->partitionSize(Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)I

    move-result v2

    rem-int v0, p1, v2

    .line 109
    .local v0, "offset":I
    iget-object v2, p0, Lorg/apache/lucene/facet/complements/TotalFacetCounts;->totalCounts:[[I

    aget-object v2, v2, v1

    aget v2, v2, v0

    return v2
.end method
