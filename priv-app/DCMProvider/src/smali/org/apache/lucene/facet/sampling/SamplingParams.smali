.class public Lorg/apache/lucene/facet/sampling/SamplingParams;
.super Ljava/lang/Object;
.source "SamplingParams.java"


# static fields
.field public static final DEFAULT_MAX_SAMPLE_SIZE:I = 0x2710

.field public static final DEFAULT_MIN_SAMPLE_SIZE:I = 0x64

.field public static final DEFAULT_OVERSAMPLE_FACTOR:D = 2.0

.field public static final DEFAULT_SAMPLE_RATIO:D = 0.01

.field public static final DEFAULT_SAMPLING_THRESHOLD:I = 0x124f8


# instance fields
.field private maxSampleSize:I

.field private minSampleSize:I

.field private oversampleFactor:D

.field private sampleRatio:D

.field private samplingThreshold:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/16 v0, 0x2710

    iput v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->maxSampleSize:I

    .line 58
    const/16 v0, 0x64

    iput v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->minSampleSize:I

    .line 59
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    iput-wide v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->sampleRatio:D

    .line 60
    const v0, 0x124f8

    iput v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->samplingThreshold:I

    .line 61
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iput-wide v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->oversampleFactor:D

    .line 25
    return-void
.end method


# virtual methods
.method public final getMaxSampleSize()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->maxSampleSize:I

    return v0
.end method

.method public final getMinSampleSize()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->minSampleSize:I

    return v0
.end method

.method public final getOversampleFactor()D
    .locals 2

    .prologue
    .line 158
    iget-wide v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->oversampleFactor:D

    return-wide v0
.end method

.method public final getSampleRatio()D
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->sampleRatio:D

    return-wide v0
.end method

.method public final getSamplingThreshold()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->samplingThreshold:I

    return v0
.end method

.method public setMaxSampleSize(I)V
    .locals 0
    .param p1, "maxSampleSize"    # I

    .prologue
    .line 103
    iput p1, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->maxSampleSize:I

    .line 104
    return-void
.end method

.method public setMinSampleSize(I)V
    .locals 0
    .param p1, "minSampleSize"    # I

    .prologue
    .line 112
    iput p1, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->minSampleSize:I

    .line 113
    return-void
.end method

.method public setOversampleFactor(D)V
    .locals 1
    .param p1, "oversampleFactor"    # D

    .prologue
    .line 166
    iput-wide p1, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->oversampleFactor:D

    .line 167
    return-void
.end method

.method public setSampleRatio(D)V
    .locals 1
    .param p1, "sampleRatio"    # D

    .prologue
    .line 121
    iput-wide p1, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->sampleRatio:D

    .line 122
    return-void
.end method

.method public setSamplingThreshold(I)V
    .locals 0
    .param p1, "samplingThreshold"    # I

    .prologue
    .line 129
    iput p1, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->samplingThreshold:I

    .line 130
    return-void
.end method

.method public validate()Z
    .locals 4

    .prologue
    .line 143
    iget v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->samplingThreshold:I

    iget v1, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->maxSampleSize:I

    if-lt v0, v1, :cond_0

    .line 144
    iget v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->maxSampleSize:I

    iget v1, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->minSampleSize:I

    if-lt v0, v1, :cond_0

    .line 145
    iget-wide v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->sampleRatio:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 146
    iget-wide v0, p0, Lorg/apache/lucene/facet/sampling/SamplingParams;->sampleRatio:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 142
    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
