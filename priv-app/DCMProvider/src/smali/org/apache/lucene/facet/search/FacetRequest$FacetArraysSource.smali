.class public final enum Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
.super Ljava/lang/Enum;
.source "FacetRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/FacetRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FacetArraysSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BOTH:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

.field public static final enum FLOAT:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

.field public static final enum INT:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    new-instance v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    const-string v1, "INT"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->INT:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    new-instance v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    const-string v1, "FLOAT"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->FLOAT:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    new-instance v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->BOTH:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->INT:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->FLOAT:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->BOTH:Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->ENUM$VALUES:[Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;->ENUM$VALUES:[Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/facet/search/FacetRequest$FacetArraysSource;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
