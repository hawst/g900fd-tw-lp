.class public Lorg/apache/lucene/facet/index/FacetFields;
.super Ljava/lang/Object;
.source "FacetFields.java"


# static fields
.field private static final DRILL_DOWN_TYPE:Lorg/apache/lucene/document/FieldType;


# instance fields
.field protected final indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

.field protected final taxonomyWriter:Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/document/TextField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0, v1}, Lorg/apache/lucene/document/FieldType;-><init>(Lorg/apache/lucene/document/FieldType;)V

    sput-object v0, Lorg/apache/lucene/facet/index/FacetFields;->DRILL_DOWN_TYPE:Lorg/apache/lucene/document/FieldType;

    .line 56
    sget-object v0, Lorg/apache/lucene/facet/index/FacetFields;->DRILL_DOWN_TYPE:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 57
    sget-object v0, Lorg/apache/lucene/facet/index/FacetFields;->DRILL_DOWN_TYPE:Lorg/apache/lucene/document/FieldType;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    .line 58
    sget-object v0, Lorg/apache/lucene/facet/index/FacetFields;->DRILL_DOWN_TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 59
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;)V
    .locals 1
    .param p1, "taxonomyWriter"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;

    .prologue
    .line 73
    sget-object v0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->DEFAULT:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/index/FacetFields;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 0
    .param p1, "taxonomyWriter"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;
    .param p2, "params"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lorg/apache/lucene/facet/index/FacetFields;->taxonomyWriter:Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;

    .line 86
    iput-object p2, p0, Lorg/apache/lucene/facet/index/FacetFields;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .line 87
    return-void
.end method


# virtual methods
.method protected addCountingListData(Lorg/apache/lucene/document/Document;Ljava/util/Map;Ljava/lang/String;)V
    .locals 5
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p3, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/document/Document;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 145
    .local p2, "categoriesData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;>;"
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    return-void

    .line 145
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 146
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;>;"
    new-instance v3, Lorg/apache/lucene/document/BinaryDocValuesField;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v3, v4, v1}, Lorg/apache/lucene/document/BinaryDocValuesField;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    invoke-virtual {p1, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_0
.end method

.method public addFields(Lorg/apache/lucene/document/Document;Ljava/lang/Iterable;)V
    .locals 18
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/document/Document;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    .local p2, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    if-nez p2, :cond_0

    .line 153
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "categories should not be null"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 162
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/index/FacetFields;->createCategoryListMapping(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v3

    .line 166
    .local v3, "categoryLists":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/facet/params/CategoryListParams;Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;>;"
    new-instance v12, Lorg/apache/lucene/util/IntsRef;

    const/16 v13, 0x20

    invoke-direct {v12, v13}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    .line 167
    .local v12, "ordinals":Lorg/apache/lucene/util/IntsRef;
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_1

    .line 192
    return-void

    .line 167
    :cond_1
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 168
    .local v8, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/facet/params/CategoryListParams;Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;>;"
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 169
    .local v4, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    iget-object v9, v4, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    .line 172
    .local v9, "field":Ljava/lang/String;
    const/4 v13, 0x0

    iput v13, v12, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 173
    const/4 v10, 0x0

    .line 174
    .local v10, "maxNumOrds":I
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Iterable;

    invoke-interface {v13}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_2

    .line 182
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Iterable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v12, v13}, Lorg/apache/lucene/facet/index/FacetFields;->getCategoryListData(Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/util/IntsRef;Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v2

    .line 185
    .local v2, "categoriesData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v9}, Lorg/apache/lucene/facet/index/FacetFields;->addCountingListData(Lorg/apache/lucene/document/Document;Ljava/util/Map;Ljava/lang/String;)V

    .line 188
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Iterable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/apache/lucene/facet/index/FacetFields;->getDrillDownStream(Ljava/lang/Iterable;)Lorg/apache/lucene/facet/index/DrillDownStream;

    move-result-object v7

    .line 189
    .local v7, "drillDownStream":Lorg/apache/lucene/facet/index/DrillDownStream;
    new-instance v6, Lorg/apache/lucene/document/Field;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/facet/index/FacetFields;->drillDownFieldType()Lorg/apache/lucene/document/FieldType;

    move-result-object v13

    invoke-direct {v6, v9, v7, v13}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/document/FieldType;)V

    .line 190
    .local v6, "drillDown":Lorg/apache/lucene/document/Field;
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_0

    .line 174
    .end local v2    # "categoriesData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;>;"
    .end local v6    # "drillDown":Lorg/apache/lucene/document/Field;
    .end local v7    # "drillDownStream":Lorg/apache/lucene/facet/index/DrillDownStream;
    :cond_2
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 175
    .local v5, "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/facet/index/FacetFields;->taxonomyWriter:Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;

    invoke-interface {v15, v5}, Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;->addCategory(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v11

    .line 176
    .local v11, "ordinal":I
    iget v15, v5, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    add-int/2addr v10, v15

    .line 177
    iget-object v15, v12, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v15, v15

    if-ge v15, v10, :cond_3

    .line 178
    invoke-virtual {v12, v10}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 180
    :cond_3
    iget-object v15, v12, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v0, v12, Lorg/apache/lucene/util/IntsRef;->length:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    iput v0, v12, Lorg/apache/lucene/util/IntsRef;->length:I

    aput v11, v15, v16

    goto :goto_1
.end method

.method protected createCategoryListMapping(Ljava/lang/Iterable;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    iget-object v4, p0, Lorg/apache/lucene/facet/index/FacetFields;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-virtual {v4}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getAllCategoryListParams()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 96
    iget-object v4, p0, Lorg/apache/lucene/facet/index/FacetFields;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v4

    invoke-static {v4, p1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    .line 110
    :cond_0
    return-object v0

    .line 99
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 100
    .local v0, "categoryLists":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/facet/params/CategoryListParams;Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 102
    .local v2, "cp":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    iget-object v5, p0, Lorg/apache/lucene/facet/index/FacetFields;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-virtual {v5, v2}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getCategoryListParams(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/params/CategoryListParams;

    move-result-object v1

    .line 103
    .local v1, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 104
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    if-nez v3, :cond_2

    .line 105
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .restart local v3    # "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    :cond_2
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected drillDownFieldType()Lorg/apache/lucene/document/FieldType;
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lorg/apache/lucene/facet/index/FacetFields;->DRILL_DOWN_TYPE:Lorg/apache/lucene/document/FieldType;

    return-object v0
.end method

.method protected getCategoryListData(Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/util/IntsRef;Ljava/lang/Iterable;)Ljava/util/Map;
    .locals 3
    .param p1, "categoryListParams"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p2, "ordinals"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            "Lorg/apache/lucene/util/IntsRef;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    .local p3, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    new-instance v0, Lorg/apache/lucene/facet/index/CountingListBuilder;

    iget-object v1, p0, Lorg/apache/lucene/facet/index/FacetFields;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    iget-object v2, p0, Lorg/apache/lucene/facet/index/FacetFields;->taxonomyWriter:Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/facet/index/CountingListBuilder;-><init>(Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/params/FacetIndexingParams;Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;)V

    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/facet/index/CountingListBuilder;->build(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected getDrillDownStream(Ljava/lang/Iterable;)Lorg/apache/lucene/facet/index/DrillDownStream;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
            ">;)",
            "Lorg/apache/lucene/facet/index/DrillDownStream;"
        }
    .end annotation

    .prologue
    .line 129
    .local p1, "categories":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/facet/taxonomy/CategoryPath;>;"
    new-instance v0, Lorg/apache/lucene/facet/index/DrillDownStream;

    iget-object v1, p0, Lorg/apache/lucene/facet/index/FacetFields;->indexingParams:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/facet/index/DrillDownStream;-><init>(Ljava/lang/Iterable;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V

    return-object v0
.end method
