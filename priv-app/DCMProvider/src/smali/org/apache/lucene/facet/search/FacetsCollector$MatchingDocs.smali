.class public final Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
.super Ljava/lang/Object;
.source "FacetsCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/search/FacetsCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MatchingDocs"
.end annotation


# instance fields
.field public final bits:Lorg/apache/lucene/util/FixedBitSet;

.field public final context:Lorg/apache/lucene/index/AtomicReaderContext;

.field public final scores:[F

.field public final totalHits:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/FixedBitSet;I[F)V
    .locals 0
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "bits"    # Lorg/apache/lucene/util/FixedBitSet;
    .param p3, "totalHits"    # I
    .param p4, "scores"    # [F

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput-object p1, p0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 159
    iput-object p2, p0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    .line 160
    iput-object p4, p0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->scores:[F

    .line 161
    iput p3, p0, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->totalHits:I

    .line 162
    return-void
.end method
