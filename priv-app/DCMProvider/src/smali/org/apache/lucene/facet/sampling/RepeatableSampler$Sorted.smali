.class final enum Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;
.super Ljava/lang/Enum;
.source "RepeatableSampler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/sampling/RepeatableSampler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Sorted"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

.field public static final enum NO:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

.field public static final enum YES:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 380
    new-instance v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    const-string v1, "YES"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;-><init>(Ljava/lang/String;I)V

    .line 383
    sput-object v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;->YES:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    .line 385
    new-instance v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    const-string v1, "NO"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;-><init>(Ljava/lang/String;I)V

    .line 388
    sput-object v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;->NO:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    .line 378
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    sget-object v1, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;->YES:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;->NO:Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;->ENUM$VALUES:[Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 378
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;->ENUM$VALUES:[Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/facet/sampling/RepeatableSampler$Sorted;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
