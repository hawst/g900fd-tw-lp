.class final Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;
.super Ljava/lang/Object;
.source "FloatToObjectMap.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/IntIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/FloatToObjectMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "IndexIterator"
.end annotation


# instance fields
.field private baseHashIndex:I

.field private index:I

.field private lastIndex:I

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/FloatToObjectMap;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/collections/FloatToObjectMap;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 67
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/FloatToObjectMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->baseHashIndex:I

    .line 55
    iput v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->index:I

    .line 60
    iput v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->lastIndex:I

    .line 68
    iput v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->baseHashIndex:I

    :goto_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->baseHashIndex:I

    iget-object v1, p1, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 74
    :cond_0
    return-void

    .line 69
    :cond_1
    iget-object v0, p1, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->baseHashIndex:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->index:I

    .line 70
    iget v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->index:I

    if-nez v0, :cond_0

    .line 68
    iget v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->baseHashIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->baseHashIndex:I

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->index:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()I
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->index:I

    iput v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->lastIndex:I

    .line 87
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/FloatToObjectMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->next:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->index:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->index:I

    .line 92
    :goto_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->index:I

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->baseHashIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->baseHashIndex:I

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/FloatToObjectMap;

    iget-object v1, v1, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 96
    :cond_0
    iget v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->lastIndex:I

    return v0

    .line 93
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/FloatToObjectMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->baseHash:[I

    iget v1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->baseHashIndex:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->index:I

    goto :goto_0
.end method

.method public remove()V
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/FloatToObjectMap;

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->this$0:Lorg/apache/lucene/facet/collections/FloatToObjectMap;

    iget-object v1, v1, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->keys:[F

    iget v2, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;->lastIndex:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->remove(F)Ljava/lang/Object;

    .line 102
    return-void
.end method
