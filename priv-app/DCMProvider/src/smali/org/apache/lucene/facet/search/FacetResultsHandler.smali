.class public abstract Lorg/apache/lucene/facet/search/FacetResultsHandler;
.super Ljava/lang/Object;
.source "FacetResultsHandler.java"


# instance fields
.field protected final facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

.field public final facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

.field public final taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 0
    .param p1, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p2, "facetRequest"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/lucene/facet/search/FacetResultsHandler;->taxonomyReader:Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    .line 39
    iput-object p2, p0, Lorg/apache/lucene/facet/search/FacetResultsHandler;->facetRequest:Lorg/apache/lucene/facet/search/FacetRequest;

    .line 40
    iput-object p3, p0, Lorg/apache/lucene/facet/search/FacetResultsHandler;->facetArrays:Lorg/apache/lucene/facet/search/FacetArrays;

    .line 41
    return-void
.end method


# virtual methods
.method public abstract compute()Lorg/apache/lucene/facet/search/FacetResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
