.class public Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;
.super Ljava/lang/Object;
.source "SumFloatAssociationFacetsAggregator.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/FacetsAggregator;


# instance fields
.field private final bytes:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 41
    return-void
.end method


# virtual methods
.method public aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 12
    .param p1, "matchingDocs"    # Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    .param p2, "clp"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v9, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v9}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p2, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "$assoc_float$"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v2

    .line 48
    .local v2, "dv":Lorg/apache/lucene/index/BinaryDocValues;
    if-nez v2, :cond_1

    .line 76
    :cond_0
    return-void

    .line 52
    :cond_1
    iget-object v9, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v9}, Lorg/apache/lucene/util/FixedBitSet;->length()I

    move-result v3

    .line 53
    .local v3, "length":I
    invoke-virtual {p3}, Lorg/apache/lucene/facet/search/FacetArrays;->getFloatArray()[F

    move-result-object v8

    .line 54
    .local v8, "values":[F
    const/4 v1, 0x0

    .line 55
    .local v1, "doc":I
    :cond_2
    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v9, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v9, v1}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v1

    const/4 v9, -0x1

    if-eq v1, v9, :cond_0

    .line 56
    iget-object v9, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, v1, v9}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 57
    iget-object v9, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v9, v9, Lorg/apache/lucene/util/BytesRef;->length:I

    if-eqz v9, :cond_2

    .line 62
    iget-object v9, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v9, v9, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v10, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v10, v10, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v0, v9, v10

    .line 63
    .local v0, "bytesUpto":I
    iget-object v9, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v5, v9, Lorg/apache/lucene/util/BytesRef;->offset:I

    .local v5, "pos":I
    move v6, v5

    .line 64
    .end local v5    # "pos":I
    .local v6, "pos":I
    :goto_1
    if-lt v6, v0, :cond_3

    .line 74
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 65
    :cond_3
    iget-object v9, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v9, v9, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "pos":I
    .restart local v5    # "pos":I
    aget-byte v9, v9, v6

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x18

    iget-object v10, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v10, v10, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "pos":I
    .restart local v6    # "pos":I
    aget-byte v10, v10, v5

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x10

    or-int/2addr v9, v10

    .line 66
    iget-object v10, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v10, v10, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "pos":I
    .restart local v5    # "pos":I
    aget-byte v10, v10, v6

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x8

    .line 65
    or-int/2addr v9, v10

    .line 66
    iget-object v10, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v10, v10, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "pos":I
    .restart local v6    # "pos":I
    aget-byte v10, v10, v5

    and-int/lit16 v10, v10, 0xff

    .line 65
    or-int v4, v9, v10

    .line 68
    .local v4, "ordinal":I
    iget-object v9, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v9, v9, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "pos":I
    .restart local v5    # "pos":I
    aget-byte v9, v9, v6

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x18

    iget-object v10, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v10, v10, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "pos":I
    .restart local v6    # "pos":I
    aget-byte v10, v10, v5

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x10

    or-int/2addr v9, v10

    .line 69
    iget-object v10, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v10, v10, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "pos":I
    .restart local v5    # "pos":I
    aget-byte v10, v10, v6

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x8

    .line 68
    or-int/2addr v9, v10

    .line 69
    iget-object v10, p0, Lorg/apache/lucene/facet/associations/SumFloatAssociationFacetsAggregator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v10, v10, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "pos":I
    .restart local v6    # "pos":I
    aget-byte v10, v10, v5

    and-int/lit16 v10, v10, 0xff

    .line 68
    or-int v7, v9, v10

    .line 71
    .local v7, "value":I
    aget v9, v8, v4

    invoke-static {v7}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v10

    add-float/2addr v9, v10

    aput v9, v8, v4

    goto :goto_1
.end method

.method public requiresDocScores()Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public rollupValues(Lorg/apache/lucene/facet/search/FacetRequest;I[I[ILorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 0
    .param p1, "fr"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p2, "ordinal"    # I
    .param p3, "children"    # [I
    .param p4, "siblings"    # [I
    .param p5, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 86
    return-void
.end method
