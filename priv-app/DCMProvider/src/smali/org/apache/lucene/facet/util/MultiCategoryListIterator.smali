.class public Lorg/apache/lucene/facet/util/MultiCategoryListIterator;
.super Ljava/lang/Object;
.source "MultiCategoryListIterator.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/CategoryListIterator;


# instance fields
.field private final iterators:[Lorg/apache/lucene/facet/search/CategoryListIterator;

.field private final validIterators:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/facet/search/CategoryListIterator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Lorg/apache/lucene/facet/search/CategoryListIterator;)V
    .locals 1
    .param p1, "iterators"    # [Lorg/apache/lucene/facet/search/CategoryListIterator;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/facet/util/MultiCategoryListIterator;->iterators:[Lorg/apache/lucene/facet/search/CategoryListIterator;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/facet/util/MultiCategoryListIterator;->validIterators:Ljava/util/List;

    .line 43
    return-void
.end method


# virtual methods
.method public getOrdinals(ILorg/apache/lucene/util/IntsRef;)V
    .locals 6
    .param p1, "docID"    # I
    .param p2, "ints"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    new-instance v1, Lorg/apache/lucene/util/IntsRef;

    iget v2, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    .line 59
    .local v1, "tmp":Lorg/apache/lucene/util/IntsRef;
    iget-object v2, p0, Lorg/apache/lucene/facet/util/MultiCategoryListIterator;->validIterators:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 66
    return-void

    .line 59
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/search/CategoryListIterator;

    .line 60
    .local v0, "cli":Lorg/apache/lucene/facet/search/CategoryListIterator;
    invoke-interface {v0, p1, v1}, Lorg/apache/lucene/facet/search/CategoryListIterator;->getOrdinals(ILorg/apache/lucene/util/IntsRef;)V

    .line 61
    iget-object v3, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v3, v3

    iget v4, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v5, v1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v4, v5

    if-ge v3, v4, :cond_1

    .line 62
    iget v3, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v4, v1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v3, v4

    invoke-virtual {p2, v3}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 64
    :cond_1
    iget v3, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iget v4, v1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/2addr v3, v4

    iput v3, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    goto :goto_0
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Z
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 47
    iget-object v2, p0, Lorg/apache/lucene/facet/util/MultiCategoryListIterator;->validIterators:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 48
    iget-object v3, p0, Lorg/apache/lucene/facet/util/MultiCategoryListIterator;->iterators:[Lorg/apache/lucene/facet/search/CategoryListIterator;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    .line 53
    iget-object v2, p0, Lorg/apache/lucene/facet/util/MultiCategoryListIterator;->validIterators:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    return v1

    .line 48
    :cond_0
    aget-object v0, v3, v2

    .line 49
    .local v0, "cli":Lorg/apache/lucene/facet/search/CategoryListIterator;
    invoke-interface {v0, p1}, Lorg/apache/lucene/facet/search/CategoryListIterator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 50
    iget-object v5, p0, Lorg/apache/lucene/facet/util/MultiCategoryListIterator;->validIterators:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 53
    .end local v0    # "cli":Lorg/apache/lucene/facet/search/CategoryListIterator;
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method
