.class Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;
.super Ljava/lang/Object;
.source "ScoredDocIdsUtils.java"

# interfaces
.implements Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;->iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field next:I

.field final synthetic this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;

.field private final synthetic val$docids:[I

.field private final synthetic val$scores:[F

.field private final synthetic val$size:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;I[F[I)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->this$1:Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;

    iput p2, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->val$size:I

    iput-object p3, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->val$scores:[F

    iput-object p4, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->val$docids:[I

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->next:I

    return-void
.end method


# virtual methods
.method public getDocID()I
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->val$docids:[I

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->next:I

    aget v0, v0, v1

    return v0
.end method

.method public getScore()F
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->val$scores:[F

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->next:I

    aget v0, v0, v1

    return v0
.end method

.method public next()Z
    .locals 2

    .prologue
    .line 181
    iget v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->next:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->next:I

    iget v1, p0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1$2;->val$size:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
