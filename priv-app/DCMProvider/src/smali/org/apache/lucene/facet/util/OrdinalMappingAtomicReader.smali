.class public Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;
.super Lorg/apache/lucene/index/FilterAtomicReader;
.source "OrdinalMappingAtomicReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;
    }
.end annotation


# instance fields
.field private final dvFieldMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/facet/params/CategoryListParams;",
            ">;"
        }
    .end annotation
.end field

.field private final ordinalMap:[I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/AtomicReader;[I)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "ordinalMap"    # [I

    .prologue
    .line 78
    sget-object v0, Lorg/apache/lucene/facet/params/FacetIndexingParams;->DEFAULT:Lorg/apache/lucene/facet/params/FacetIndexingParams;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;-><init>(Lorg/apache/lucene/index/AtomicReader;[ILorg/apache/lucene/facet/params/FacetIndexingParams;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/AtomicReader;[ILorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "ordinalMap"    # [I
    .param p3, "indexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/FilterAtomicReader;-><init>(Lorg/apache/lucene/index/AtomicReader;)V

    .line 70
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;->dvFieldMap:Ljava/util/Map;

    .line 87
    iput-object p2, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;->ordinalMap:[I

    .line 88
    invoke-virtual {p3}, Lorg/apache/lucene/facet/params/FacetIndexingParams;->getAllCategoryListParams()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 91
    return-void

    .line 88
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 89
    .local v0, "params":Lorg/apache/lucene/facet/params/CategoryListParams;
    iget-object v2, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;->dvFieldMap:Ljava/util/Map;

    iget-object v3, v0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method static synthetic access$0(Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;)[I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;->ordinalMap:[I

    return-object v0
.end method


# virtual methods
.method public getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-super {p0, p1}, Lorg/apache/lucene/index/FilterAtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v1

    .line 96
    .local v1, "inner":Lorg/apache/lucene/index/BinaryDocValues;
    if-nez v1, :cond_1

    .line 104
    .end local v1    # "inner":Lorg/apache/lucene/index/BinaryDocValues;
    :cond_0
    :goto_0
    return-object v1

    .line 100
    .restart local v1    # "inner":Lorg/apache/lucene/index/BinaryDocValues;
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;->dvFieldMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 101
    .local v0, "clp":Lorg/apache/lucene/facet/params/CategoryListParams;
    if-eqz v0, :cond_0

    .line 104
    new-instance v2, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;

    invoke-direct {v2, p0, v0, v1}, Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader$OrdinalMappingBinaryDocValues;-><init>(Lorg/apache/lucene/facet/util/OrdinalMappingAtomicReader;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/index/BinaryDocValues;)V

    move-object v1, v2

    goto :goto_0
.end method
