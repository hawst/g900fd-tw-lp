.class public Lorg/apache/lucene/facet/search/CountingFacetsAggregator;
.super Lorg/apache/lucene/facet/search/IntRollupFacetsAggregator;
.source "CountingFacetsAggregator.java"


# instance fields
.field private final ordinals:Lorg/apache/lucene/util/IntsRef;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/facet/search/IntRollupFacetsAggregator;-><init>()V

    .line 37
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/search/CountingFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    .line 35
    return-void
.end method


# virtual methods
.method public aggregate(Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;Lorg/apache/lucene/facet/params/CategoryListParams;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 8
    .param p1, "matchingDocs"    # Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;
    .param p2, "clp"    # Lorg/apache/lucene/facet/params/CategoryListParams;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    const/4 v6, 0x0

    invoke-virtual {p2, v6}, Lorg/apache/lucene/facet/params/CategoryListParams;->createCategoryListIterator(I)Lorg/apache/lucene/facet/search/CategoryListIterator;

    move-result-object v0

    .line 42
    .local v0, "cli":Lorg/apache/lucene/facet/search/CategoryListIterator;
    iget-object v6, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-interface {v0, v6}, Lorg/apache/lucene/facet/search/CategoryListIterator;->setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 57
    :cond_0
    return-void

    .line 46
    :cond_1
    iget-object v6, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v6}, Lorg/apache/lucene/util/FixedBitSet;->length()I

    move-result v4

    .line 47
    .local v4, "length":I
    invoke-virtual {p3}, Lorg/apache/lucene/facet/search/FacetArrays;->getIntArray()[I

    move-result-object v1

    .line 48
    .local v1, "counts":[I
    const/4 v2, 0x0

    .line 49
    .local v2, "doc":I
    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v6, p1, Lorg/apache/lucene/facet/search/FacetsCollector$MatchingDocs;->bits:Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v6, v2}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v2

    const/4 v6, -0x1

    if-eq v2, v6, :cond_0

    .line 50
    iget-object v6, p0, Lorg/apache/lucene/facet/search/CountingFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    invoke-interface {v0, v2, v6}, Lorg/apache/lucene/facet/search/CategoryListIterator;->getOrdinals(ILorg/apache/lucene/util/IntsRef;)V

    .line 51
    iget-object v6, p0, Lorg/apache/lucene/facet/search/CountingFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    iget v6, v6, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget-object v7, p0, Lorg/apache/lucene/facet/search/CountingFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    iget v7, v7, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v5, v6, v7

    .line 52
    .local v5, "upto":I
    iget-object v6, p0, Lorg/apache/lucene/facet/search/CountingFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    iget v3, v6, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v3, "i":I
    :goto_1
    if-lt v3, v5, :cond_2

    .line 55
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 53
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/facet/search/CountingFacetsAggregator;->ordinals:Lorg/apache/lucene/util/IntsRef;

    iget-object v6, v6, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v6, v6, v3

    aget v7, v1, v6

    add-int/lit8 v7, v7, 0x1

    aput v7, v1, v6

    .line 52
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method
