.class public Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;
.super Ljava/lang/Object;
.source "TaxonomyReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChildrenIterator"
.end annotation


# instance fields
.field private child:I

.field private final siblings:[I


# direct methods
.method constructor <init>(I[I)V
    .locals 0
    .param p1, "child"    # I
    .param p2, "siblings"    # [I

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p2, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;->siblings:[I

    .line 76
    iput p1, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;->child:I

    .line 77
    return-void
.end method


# virtual methods
.method public next()I
    .locals 3

    .prologue
    .line 84
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;->child:I

    .line 85
    .local v0, "res":I
    iget v1, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;->child:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 86
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;->siblings:[I

    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;->child:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;->child:I

    .line 88
    :cond_0
    return v0
.end method
