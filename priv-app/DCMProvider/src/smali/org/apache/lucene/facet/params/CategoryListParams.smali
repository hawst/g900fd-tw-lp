.class public Lorg/apache/lucene/facet/params/CategoryListParams;
.super Ljava/lang/Object;
.source "CategoryListParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    }
.end annotation


# static fields
.field public static final DEFAULT_FIELD:Ljava/lang/String; = "$facets"

.field public static final DEFAULT_ORDINAL_POLICY:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;


# instance fields
.field public final field:Ljava/lang/String;

.field private final hashCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;->ALL_BUT_DIMENSION:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    sput-object v0, Lorg/apache/lucene/facet/params/CategoryListParams;->DEFAULT_ORDINAL_POLICY:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 111
    const-string v0, "$facets"

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/params/CategoryListParams;-><init>(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    .line 119
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/facet/params/CategoryListParams;->hashCode:I

    .line 120
    return-void
.end method


# virtual methods
.method public createCategoryListIterator(I)Lorg/apache/lucene/facet/search/CategoryListIterator;
    .locals 4
    .param p1, "partition"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-static {p1}, Lorg/apache/lucene/facet/util/PartitionsUtils;->partitionName(I)Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "categoryListTermStr":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 170
    .local v1, "docValuesField":Ljava/lang/String;
    new-instance v2, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;

    invoke-virtual {p0}, Lorg/apache/lucene/facet/params/CategoryListParams;->createEncoder()Lorg/apache/lucene/facet/encoding/IntEncoder;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/facet/encoding/IntEncoder;->createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lorg/apache/lucene/facet/search/DocValuesCategoryListIterator;-><init>(Ljava/lang/String;Lorg/apache/lucene/facet/encoding/IntDecoder;)V

    return-object v2
.end method

.method public createEncoder()Lorg/apache/lucene/facet/encoding/IntEncoder;
    .locals 3

    .prologue
    .line 143
    new-instance v0, Lorg/apache/lucene/facet/encoding/SortingIntEncoder;

    new-instance v1, Lorg/apache/lucene/facet/encoding/UniqueValuesIntEncoder;

    new-instance v2, Lorg/apache/lucene/facet/encoding/DGapVInt8IntEncoder;

    invoke-direct {v2}, Lorg/apache/lucene/facet/encoding/DGapVInt8IntEncoder;-><init>()V

    invoke-direct {v1, v2}, Lorg/apache/lucene/facet/encoding/UniqueValuesIntEncoder;-><init>(Lorg/apache/lucene/facet/encoding/IntEncoder;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/encoding/SortingIntEncoder;-><init>(Lorg/apache/lucene/facet/encoding/IntEncoder;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 148
    if-ne p1, p0, :cond_1

    .line 149
    const/4 v1, 0x1

    .line 158
    :cond_0
    :goto_0
    return v1

    .line 151
    :cond_1
    instance-of v2, p1, Lorg/apache/lucene/facet/params/CategoryListParams;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 154
    check-cast v0, Lorg/apache/lucene/facet/params/CategoryListParams;

    .line 155
    .local v0, "other":Lorg/apache/lucene/facet/params/CategoryListParams;
    iget v2, p0, Lorg/apache/lucene/facet/params/CategoryListParams;->hashCode:I

    iget v3, v0, Lorg/apache/lucene/facet/params/CategoryListParams;->hashCode:I

    if-ne v2, v3, :cond_0

    .line 158
    iget-object v1, p0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    iget-object v2, v0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getOrdinalPolicy(Ljava/lang/String;)Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;
    .locals 1
    .param p1, "dimension"    # Ljava/lang/String;

    .prologue
    .line 179
    sget-object v0, Lorg/apache/lucene/facet/params/CategoryListParams;->DEFAULT_ORDINAL_POLICY:Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lorg/apache/lucene/facet/params/CategoryListParams;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "field="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/facet/params/CategoryListParams;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " encoder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/facet/params/CategoryListParams;->createEncoder()Lorg/apache/lucene/facet/encoding/IntEncoder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ordinalPolicy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/params/CategoryListParams;->getOrdinalPolicy(Ljava/lang/String;)Lorg/apache/lucene/facet/params/CategoryListParams$OrdinalPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
