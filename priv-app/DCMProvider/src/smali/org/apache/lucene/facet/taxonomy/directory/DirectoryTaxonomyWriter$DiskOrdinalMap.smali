.class public final Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;
.super Ljava/lang/Object;
.source "DirectoryTaxonomyWriter.java"

# interfaces
.implements Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$OrdinalMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DiskOrdinalMap"
.end annotation


# instance fields
.field map:[I

.field out:Ljava/io/DataOutputStream;

.field tmpfile:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 3
    .param p1, "tmpfile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->map:[I

    .line 890
    iput-object p1, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->tmpfile:Ljava/io/File;

    .line 891
    new-instance v0, Ljava/io/DataOutputStream;

    new-instance v1, Ljava/io/BufferedOutputStream;

    .line 892
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 891
    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->out:Ljava/io/DataOutputStream;

    .line 893
    return-void
.end method


# virtual methods
.method public addDone()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 908
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->out:Ljava/io/DataOutputStream;

    if-eqz v0, :cond_0

    .line 909
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 910
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->out:Ljava/io/DataOutputStream;

    .line 912
    :cond_0
    return-void
.end method

.method public addMapping(II)V
    .locals 1
    .param p1, "origOrdinal"    # I
    .param p2, "newOrdinal"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 897
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 898
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 899
    return-void
.end method

.method public getMap()[I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 918
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->map:[I

    if-eqz v4, :cond_0

    .line 919
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->map:[I

    .line 938
    :goto_0
    return-object v4

    .line 921
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->addDone()V

    .line 922
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/BufferedInputStream;

    .line 923
    new-instance v5, Ljava/io/FileInputStream;

    iget-object v6, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->tmpfile:Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 922
    invoke-direct {v1, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 924
    .local v1, "in":Ljava/io/DataInputStream;
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    new-array v4, v4, [I

    iput-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->map:[I

    .line 928
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->map:[I

    array-length v4, v4

    if-lt v0, v4, :cond_2

    .line 933
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 935
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->tmpfile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_1

    .line 936
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->tmpfile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->deleteOnExit()V

    .line 938
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->map:[I

    goto :goto_0

    .line 929
    :cond_2
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 930
    .local v3, "origordinal":I
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    .line 931
    .local v2, "newordinal":I
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->map:[I

    aput v2, v4, v3

    .line 928
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public setSize(I)V
    .locals 1
    .param p1, "taxonomySize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 903
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$DiskOrdinalMap;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 904
    return-void
.end method
