.class Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
.super Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;
.source "TaxonomyIndexArrays.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private children:[I

.field private volatile initializedChildren:Z

.field private final parents:[I

.field private siblings:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;)V
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;-><init>()V

    .line 45
    iput-boolean v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initializedChildren:Z

    .line 54
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    .line 55
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 56
    invoke-direct {p0, p1, v2}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initParents(Lorg/apache/lucene/index/IndexReader;I)V

    .line 64
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    const/4 v1, -0x1

    aput v1, v0, v2

    .line 66
    :cond_0
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;)V
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "copyFrom"    # Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 68
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;-><init>()V

    .line 45
    iput-boolean v3, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initializedChildren:Z

    .line 69
    sget-boolean v1, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 75
    :cond_0
    invoke-virtual {p2}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents()[I

    move-result-object v0

    .line 76
    .local v0, "copyParents":[I
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v1

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    .line 77
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 78
    array-length v1, v0

    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initParents(Lorg/apache/lucene/index/IndexReader;I)V

    .line 80
    iget-boolean v1, p2, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initializedChildren:Z

    if-eqz v1, :cond_1

    .line 81
    invoke-direct {p0, p2}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initChildrenSiblings(Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;)V

    .line 83
    :cond_1
    return-void
.end method

.method private constructor <init>([I)V
    .locals 1
    .param p1, "parents"    # [I

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initializedChildren:Z

    .line 50
    iput-object p1, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    .line 51
    return-void
.end method

.method private computeChildrenSiblings([II)V
    .locals 4
    .param p1, "parents"    # [I
    .param p2, "first"    # I

    .prologue
    const/4 v3, -0x1

    .line 103
    move v0, p2

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_1

    .line 108
    if-nez p2, :cond_0

    .line 109
    const/4 p2, 0x1

    .line 110
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->siblings:[I

    const/4 v2, 0x0

    aput v3, v1, v2

    .line 113
    :cond_0
    move v0, p2

    :goto_1
    array-length v1, p1

    if-lt v0, v1, :cond_2

    .line 119
    return-void

    .line 104
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->children:[I

    aput v3, v1, v0

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->siblings:[I

    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->children:[I

    aget v3, p1, v0

    aget v2, v2, v3

    aput v2, v1, v0

    .line 117
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->children:[I

    aget v2, p1, v0

    aput v0, v1, v2

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private final declared-synchronized initChildrenSiblings(Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;)V
    .locals 5
    .param p1, "copyFrom"    # Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initializedChildren:Z

    if-nez v0, :cond_1

    .line 87
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->children:[I

    .line 88
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->siblings:[I

    .line 89
    if-eqz p1, :cond_0

    .line 91
    invoke-virtual {p1}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->children()[I

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->children:[I

    const/4 v3, 0x0

    invoke-virtual {p1}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->children()[I

    move-result-object v4

    array-length v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    invoke-virtual {p1}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->siblings()[I

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->siblings:[I

    const/4 v3, 0x0

    invoke-virtual {p1}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->siblings()[I

    move-result-object v4

    array-length v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->computeChildrenSiblings([II)V

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initializedChildren:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :cond_1
    monitor-exit p0

    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private initParents(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 8
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "first"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v7, 0x7fffffff

    .line 123
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v3

    if-ne v3, p2, :cond_1

    .line 158
    :cond_0
    return-void

    .line 130
    :cond_1
    const/4 v3, 0x0

    .line 131
    const-string v4, "$payloads$"

    sget-object v5, Lorg/apache/lucene/facet/taxonomy/directory/Consts;->PAYLOAD_PARENT_BYTES_REF:Lorg/apache/lucene/util/BytesRef;

    .line 132
    const/4 v6, 0x2

    .line 130
    invoke-static {p1, v3, v4, v5, v6}, Lorg/apache/lucene/index/MultiFields;->getTermPositionsEnum(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v2

    .line 135
    .local v2, "positions":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    if-eqz v2, :cond_2

    invoke-virtual {v2, p2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v3

    if-ne v3, v7, :cond_3

    .line 136
    :cond_2
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Missing parent data for category "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 139
    :cond_3
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v1

    .line 140
    .local v1, "num":I
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 141
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->docID()I

    move-result v3

    if-ne v3, v0, :cond_5

    .line 142
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v3

    if-nez v3, :cond_4

    .line 143
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Missing parent data for category "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 146
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v4

    aput v4, v3, v0

    .line 148
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v3

    if-ne v3, v7, :cond_6

    .line 149
    add-int/lit8 v3, v0, 0x1

    if-ge v3, v1, :cond_0

    .line 150
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Missing parent data for category "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 155
    :cond_5
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Missing parent data for category "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 140
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method add(II)Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    .locals 3
    .param p1, "ordinal"    # I
    .param p2, "parentOrdinal"    # I

    .prologue
    .line 167
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    array-length v1, v1

    if-lt p1, v1, :cond_0

    .line 168
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    add-int/lit8 v2, p1, 0x1

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v0

    .line 169
    .local v0, "newarray":[I
    aput p2, v0, p1

    .line 170
    new-instance p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;

    .end local p0    # "this":Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;-><init>([I)V

    .line 173
    .end local v0    # "newarray":[I
    :goto_0
    return-object p0

    .line 172
    .restart local p0    # "this":Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    aput p2, v1, p1

    goto :goto_0
.end method

.method public children()[I
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initializedChildren:Z

    if-nez v0, :cond_0

    .line 194
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initChildrenSiblings(Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;)V

    .line 198
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->children:[I

    return-object v0
.end method

.method public parents()[I
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->parents:[I

    return-object v0
.end method

.method public siblings()[I
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initializedChildren:Z

    if-nez v0, :cond_0

    .line 209
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->initChildrenSiblings(Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;)V

    .line 213
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/TaxonomyIndexArrays;->siblings:[I

    return-object v0
.end method
