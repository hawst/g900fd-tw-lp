.class public Lorg/apache/lucene/facet/collections/IntToFloatMap;
.super Ljava/lang/Object;
.source "IntToFloatMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/collections/IntToFloatMap$IndexIterator;,
        Lorg/apache/lucene/facet/collections/IntToFloatMap$KeyIterator;,
        Lorg/apache/lucene/facet/collections/IntToFloatMap$ValueIterator;
    }
.end annotation


# static fields
.field public static final GROUND:F = NaNf

.field private static defaultCapacity:I


# instance fields
.field baseHash:[I

.field private capacity:I

.field private firstEmpty:I

.field private hashFactor:I

.field keys:[I

.field next:[I

.field private prev:I

.field private size:I

.field values:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    const/16 v0, 0x10

    sput v0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->defaultCapacity:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 211
    sget v0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->defaultCapacity:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;-><init>(I)V

    .line 212
    return-void
.end method

.method public constructor <init>(I)V
    .locals 5
    .param p1, "capacity"    # I

    .prologue
    const/4 v4, 0x0

    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    const/16 v2, 0x10

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    .line 224
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    if-lt v2, p1, :cond_0

    .line 231
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    add-int/lit8 v0, v2, 0x1

    .line 233
    .local v0, "arrayLength":I
    new-array v2, v0, [F

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->values:[F

    .line 234
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->keys:[I

    .line 235
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    .line 238
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    shl-int/lit8 v1, v2, 0x1

    .line 240
    .local v1, "baseHashSize":I
    new-array v2, v1, [I

    iput-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->baseHash:[I

    .line 242
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->values:[F

    const/high16 v3, 0x7fc00000    # NaNf

    aput v3, v2, v4

    .line 246
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->hashFactor:I

    .line 248
    iput v4, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    .line 250
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->clear()V

    .line 251
    return-void

    .line 226
    .end local v0    # "arrayLength":I
    .end local v1    # "baseHashSize":I
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    goto :goto_0
.end method

.method private findForRemove(II)I
    .locals 3
    .param p1, "key"    # I
    .param p2, "baseHashIndex"    # I

    .prologue
    const/4 v1, 0x0

    .line 385
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->prev:I

    .line 386
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->baseHash:[I

    aget v0, v2, p2

    .line 389
    .local v0, "index":I
    :goto_0
    if-nez v0, :cond_1

    .line 402
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->prev:I

    move v0, v1

    .line 403
    .end local v0    # "index":I
    :cond_0
    return v0

    .line 391
    .restart local v0    # "index":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->keys:[I

    aget v2, v2, v0

    if-eq v2, p1, :cond_0

    .line 396
    iput v0, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->prev:I

    .line 397
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    aget v0, v2, v0

    goto :goto_0
.end method

.method private getBaseHashAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->baseHash:[I

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private prvt_put(IF)V
    .locals 4
    .param p1, "key"    # I
    .param p2, "v"    # F

    .prologue
    .line 267
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 270
    .local v0, "hashIndex":I
    iget v1, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->firstEmpty:I

    .line 273
    .local v1, "objectIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->firstEmpty:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->firstEmpty:I

    .line 274
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->values:[F

    aput p2, v2, v1

    .line 275
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->keys:[I

    aput p1, v2, v1

    .line 278
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->baseHash:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 279
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->baseHash:[I

    aput v1, v2, v0

    .line 282
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    .line 283
    return-void
.end method


# virtual methods
.method protected calcBaseHashIndex(I)I
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 290
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->hashFactor:I

    and-int/2addr v0, p1

    return v0
.end method

.method public clear()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 298
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->baseHash:[I

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 301
    iput v4, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    .line 306
    const/4 v2, 0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->firstEmpty:I

    .line 310
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    if-lt v0, v2, :cond_0

    .line 315
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    aput v4, v2, v3

    .line 316
    return-void

    .line 311
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput v1, v2, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public containsKey(I)Z
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 326
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->find(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(F)Z
    .locals 3
    .param p1, "value"    # F

    .prologue
    .line 338
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->iterator()Lorg/apache/lucene/facet/collections/FloatIterator;

    move-result-object v1

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/FloatIterator;
    :cond_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/FloatIterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 344
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 339
    :cond_1
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/FloatIterator;->next()F

    move-result v0

    .line 340
    .local v0, "d":F
    cmpl-float v2, v0, p1

    if-nez v2, :cond_0

    .line 341
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 611
    move-object v2, p1

    check-cast v2, Lorg/apache/lucene/facet/collections/IntToFloatMap;

    .line 612
    .local v2, "that":Lorg/apache/lucene/facet/collections/IntToFloatMap;
    invoke-virtual {v2}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size()I

    move-result v6

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size()I

    move-result v7

    if-eq v6, v7, :cond_1

    .line 629
    :cond_0
    :goto_0
    return v5

    .line 616
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v0

    .line 617
    .local v0, "it":Lorg/apache/lucene/facet/collections/IntIterator;
    :cond_2
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 629
    const/4 v5, 0x1

    goto :goto_0

    .line 618
    :cond_3
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    .line 619
    .local v1, "key":I
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 623
    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->get(I)F

    move-result v3

    .line 624
    .local v3, "v1":F
    invoke-virtual {v2, v1}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->get(I)F

    move-result v4

    .line 625
    .local v4, "v2":F
    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-eqz v6, :cond_2

    goto :goto_0
.end method

.method protected find(I)I
    .locals 3
    .param p1, "key"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 357
    .local v0, "baseHashIndex":I
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->baseHash:[I

    aget v1, v2, v0

    .line 360
    .local v1, "localIndex":I
    :goto_0
    if-nez v1, :cond_1

    .line 372
    const/4 v1, 0x0

    .end local v1    # "localIndex":I
    :cond_0
    return v1

    .line 362
    .restart local v1    # "localIndex":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->keys:[I

    aget v2, v2, v1

    if-eq v2, p1, :cond_0

    .line 367
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    aget v1, v2, v1

    goto :goto_0
.end method

.method public get(I)F
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 414
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->values:[F

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->find(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method protected grow()V
    .locals 5

    .prologue
    .line 422
    new-instance v2, Lorg/apache/lucene/facet/collections/IntToFloatMap;

    .line 423
    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    mul-int/lit8 v3, v3, 0x2

    .line 422
    invoke-direct {v2, v3}, Lorg/apache/lucene/facet/collections/IntToFloatMap;-><init>(I)V

    .line 428
    .local v2, "that":Lorg/apache/lucene/facet/collections/IntToFloatMap;
    new-instance v1, Lorg/apache/lucene/facet/collections/IntToFloatMap$IndexIterator;

    invoke-direct {v1, p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToFloatMap;)V

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/IntToFloatMap$IndexIterator;
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/IntToFloatMap$IndexIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 434
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    .line 435
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    .line 436
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToFloatMap;->firstEmpty:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->firstEmpty:I

    .line 437
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToFloatMap;->values:[F

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->values:[F

    .line 438
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToFloatMap;->keys:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->keys:[I

    .line 439
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    .line 440
    iget-object v3, v2, Lorg/apache/lucene/facet/collections/IntToFloatMap;->baseHash:[I

    iput-object v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->baseHash:[I

    .line 441
    iget v3, v2, Lorg/apache/lucene/facet/collections/IntToFloatMap;->hashFactor:I

    iput v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->hashFactor:I

    .line 442
    return-void

    .line 429
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/facet/collections/IntToFloatMap$IndexIterator;->next()I

    move-result v0

    .line 430
    .local v0, "index":I
    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->keys:[I

    aget v3, v3, v0

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->values:[F

    aget v4, v4, v0

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->prvt_put(IF)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 606
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 449
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Lorg/apache/lucene/facet/collections/FloatIterator;
    .locals 1

    .prologue
    .line 456
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToFloatMap$ValueIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap$ValueIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToFloatMap;)V

    return-object v0
.end method

.method public keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;
    .locals 1

    .prologue
    .line 461
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToFloatMap$KeyIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap$KeyIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToFloatMap;)V

    return-object v0
.end method

.method public put(IF)F
    .locals 4
    .param p1, "key"    # I
    .param p2, "v"    # F

    .prologue
    .line 481
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->find(I)I

    move-result v0

    .line 484
    .local v0, "index":I
    if-eqz v0, :cond_0

    .line 486
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->values:[F

    aget v1, v2, v0

    .line 487
    .local v1, "old":F
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->values:[F

    aput p2, v2, v0

    .line 501
    .end local v1    # "old":F
    :goto_0
    return v1

    .line 492
    :cond_0
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->capacity:I

    if-ne v2, v3, :cond_1

    .line 494
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->grow()V

    .line 499
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->prvt_put(IF)V

    .line 501
    const/high16 v1, 0x7fc00000    # NaNf

    goto :goto_0
.end method

.method public remove(I)F
    .locals 5
    .param p1, "key"    # I

    .prologue
    .line 512
    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->calcBaseHashIndex(I)I

    move-result v0

    .line 513
    .local v0, "baseHashIndex":I
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->findForRemove(II)I

    move-result v1

    .line 514
    .local v1, "index":I
    if-eqz v1, :cond_1

    .line 517
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->prev:I

    if-nez v2, :cond_0

    .line 518
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->baseHash:[I

    iget-object v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    aget v3, v3, v1

    aput v3, v2, v0

    .line 521
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->prev:I

    iget-object v4, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    aget v4, v4, v1

    aput v4, v2, v3

    .line 522
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->next:[I

    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->firstEmpty:I

    aput v3, v2, v1

    .line 523
    iput v1, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->firstEmpty:I

    .line 524
    iget v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    .line 525
    iget-object v2, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->values:[F

    aget v2, v2, v1

    .line 528
    :goto_0
    return v2

    :cond_1
    const/high16 v2, 0x7fc00000    # NaNf

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 535
    iget v0, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    return v0
.end method

.method public toArray()[F
    .locals 4

    .prologue
    .line 544
    const/4 v2, -0x1

    .line 545
    .local v2, "j":I
    iget v3, p0, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size:I

    new-array v0, v3, [F

    .line 548
    .local v0, "array":[F
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->iterator()Lorg/apache/lucene/facet/collections/FloatIterator;

    move-result-object v1

    .local v1, "iterator":Lorg/apache/lucene/facet/collections/FloatIterator;
    :goto_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/FloatIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 551
    return-object v0

    .line 549
    :cond_0
    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/FloatIterator;->next()F

    move-result v3

    aput v3, v0, v2

    goto :goto_0
.end method

.method public toArray([F)[F
    .locals 4
    .param p1, "a"    # [F

    .prologue
    .line 568
    const/4 v1, 0x0

    .line 569
    .local v1, "j":I
    array-length v2, p1

    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 570
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->size()I

    move-result v2

    new-array p1, v2, [F

    .line 574
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->iterator()Lorg/apache/lucene/facet/collections/FloatIterator;

    move-result-object v0

    .local v0, "iterator":Lorg/apache/lucene/facet/collections/FloatIterator;
    :goto_0
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/FloatIterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 578
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 579
    const/high16 v2, 0x7fc00000    # NaNf

    aput v2, p1, v1

    .line 582
    :cond_1
    return-object p1

    .line 575
    :cond_2
    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/FloatIterator;->next()F

    move-result v2

    aput v2, p1, v1

    .line 574
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 587
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 588
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/16 v3, 0x7b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 589
    invoke-virtual {p0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->keyIterator()Lorg/apache/lucene/facet/collections/IntIterator;

    move-result-object v1

    .line 590
    .local v1, "keyIterator":Lorg/apache/lucene/facet/collections/IntIterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 600
    const/16 v3, 0x7d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 601
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 591
    :cond_1
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v0

    .line 592
    .local v0, "key":I
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 593
    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 594
    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/collections/IntToFloatMap;->get(I)F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 595
    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 596
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 597
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
