.class public Lorg/apache/lucene/facet/util/ResultSortUtils;
.super Ljava/lang/Object;
.source "ResultSortUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;,
        Lorg/apache/lucene/facet/util/ResultSortUtils$MaxValueHeap;,
        Lorg/apache/lucene/facet/util/ResultSortUtils$MinValueHeap;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createSuitableHeap(Lorg/apache/lucene/facet/search/FacetRequest;)Lorg/apache/lucene/facet/search/Heap;
    .locals 4
    .param p0, "facetRequest"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/facet/search/FacetRequest;",
            ")",
            "Lorg/apache/lucene/facet/search/Heap",
            "<",
            "Lorg/apache/lucene/facet/search/FacetResultNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget v1, p0, Lorg/apache/lucene/facet/search/FacetRequest;->numResults:I

    .line 45
    .local v1, "nresults":I
    invoke-virtual {p0}, Lorg/apache/lucene/facet/search/FacetRequest;->getSortOrder()Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;->ASCENDING:Lorg/apache/lucene/facet/search/FacetRequest$SortOrder;

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    .line 47
    .local v0, "accending":Z
    :goto_0
    const v2, 0x7fffffff

    if-ne v1, v2, :cond_1

    .line 48
    new-instance v2, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;

    invoke-direct {v2, v0}, Lorg/apache/lucene/facet/util/ResultSortUtils$AllValueHeap;-><init>(Z)V

    .line 54
    :goto_1
    return-object v2

    .line 45
    .end local v0    # "accending":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    .restart local v0    # "accending":Z
    :cond_1
    if-eqz v0, :cond_2

    .line 52
    new-instance v2, Lorg/apache/lucene/facet/util/ResultSortUtils$MaxValueHeap;

    invoke-direct {v2, v1}, Lorg/apache/lucene/facet/util/ResultSortUtils$MaxValueHeap;-><init>(I)V

    goto :goto_1

    .line 54
    :cond_2
    new-instance v2, Lorg/apache/lucene/facet/util/ResultSortUtils$MinValueHeap;

    invoke-direct {v2, v1}, Lorg/apache/lucene/facet/util/ResultSortUtils$MinValueHeap;-><init>(I)V

    goto :goto_1
.end method
