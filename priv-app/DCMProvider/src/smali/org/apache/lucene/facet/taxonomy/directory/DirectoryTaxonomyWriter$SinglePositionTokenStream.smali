.class Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "DirectoryTaxonomyWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SinglePositionTokenStream"
.end annotation


# instance fields
.field private posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private returned:Z

.field private termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "word"    # Ljava/lang/String;

    .prologue
    .line 560
    invoke-direct {p0}, Lorg/apache/lucene/analysis/TokenStream;-><init>()V

    .line 561
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 562
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 563
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 564
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;->returned:Z

    .line 565
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 584
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;->returned:Z

    if-eqz v0, :cond_0

    .line 585
    const/4 v0, 0x0

    .line 587
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;->returned:Z

    goto :goto_0
.end method

.method public set(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 579
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v0, p1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 580
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/directory/DirectoryTaxonomyWriter$SinglePositionTokenStream;->returned:Z

    .line 581
    return-void
.end method
