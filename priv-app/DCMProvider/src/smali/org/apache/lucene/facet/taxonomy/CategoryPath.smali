.class public Lorg/apache/lucene/facet/taxonomy/CategoryPath;
.super Ljava/lang/Object;
.source "CategoryPath.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/facet/taxonomy/CategoryPath;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EMPTY:Lorg/apache/lucene/facet/taxonomy/CategoryPath;


# instance fields
.field public final components:[Ljava/lang/String;

.field public final length:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->$assertionsDisabled:Z

    .line 32
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-direct {v0}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>()V

    sput-object v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->EMPTY:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    return-void

    .line 29
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;C)V
    .locals 5
    .param p1, "pathString"    # Ljava/lang/String;
    .param p2, "delimiter"    # C

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {p2}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "comps":[Ljava/lang/String;
    array-length v3, v1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    aget-object v3, v1, v2

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 79
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    .line 80
    iput v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    .line 90
    :goto_0
    return-void

    .line 82
    :cond_0
    array-length v3, v1

    :goto_1
    if-lt v2, v3, :cond_1

    .line 87
    iput-object v1, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    .line 88
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    array-length v2, v2

    iput v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    goto :goto_0

    .line 82
    :cond_1
    aget-object v0, v1, v2

    .line 83
    .local v0, "comp":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 84
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "empty or null components not allowed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 82
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V
    .locals 3
    .param p1, "copyFrom"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "prefixLen"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    sget-boolean v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-lez p2, :cond_0

    iget-object v0, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    array-length v0, v0

    if-le p2, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    .line 57
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "prefixLen cannot be negative nor larger than the given components\' length: prefixLen="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 58
    const-string v2, " components.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 59
    :cond_1
    iget-object v0, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    .line 60
    iput p2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    .line 61
    return-void
.end method

.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 4
    .param p1, "components"    # [Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    sget-boolean v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    array-length v1, p1

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "use CategoryPath.EMPTY to create an empty path"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 66
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_1

    .line 71
    iput-object p1, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    .line 72
    array-length v1, p1

    iput v1, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    .line 73
    return-void

    .line 66
    :cond_1
    aget-object v0, p1, v1

    .line 67
    .local v0, "comp":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 68
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "empty or null components not allowed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 66
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private hasDelimiter(Ljava/lang/String;C)V
    .locals 3
    .param p1, "offender"    # Ljava/lang/String;
    .param p2, "delimiter"    # C

    .prologue
    .line 126
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "delimiter character \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' (U+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") appears in path component \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private noDelimiter([CIIC)V
    .locals 2
    .param p1, "buf"    # [C
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .param p4, "delimiter"    # C

    .prologue
    .line 130
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 135
    return-void

    .line 131
    :cond_0
    add-int v1, p2, v0

    aget-char v1, p1, v1

    if-ne v1, p4, :cond_1

    .line 132
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-direct {p0, v1, p4}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->hasDelimiter(Ljava/lang/String;C)V

    .line 130
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->compareTo(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    .locals 6
    .param p1, "other"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .prologue
    .line 114
    iget v4, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    iget v5, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-ge v4, v5, :cond_0

    iget v3, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    .line 115
    .local v3, "len":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    if-lt v1, v3, :cond_1

    .line 122
    iget v4, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    iget v5, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    sub-int/2addr v4, v5

    :goto_2
    return v4

    .line 114
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v3    # "len":I
    :cond_0
    iget v3, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    goto :goto_0

    .line 116
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    .restart local v3    # "len":I
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v4, v4, v1

    iget-object v5, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 117
    .local v0, "cmp":I
    if-gez v0, :cond_2

    const/4 v4, -0x1

    goto :goto_2

    .line 118
    :cond_2
    if-lez v0, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    .line 115
    :cond_3
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public copyFullPath([CIC)I
    .locals 8
    .param p1, "buf"    # [C
    .param p2, "start"    # I
    .param p3, "delimiter"    # C

    .prologue
    const/4 v5, 0x0

    .line 148
    iget v6, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v6, :cond_0

    .line 164
    :goto_0
    return v5

    .line 152
    :cond_0
    move v1, p2

    .line 153
    .local v1, "idx":I
    iget v6, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    add-int/lit8 v4, v6, -0x1

    .line 154
    .local v4, "upto":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v4, :cond_1

    .line 161
    iget-object v6, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v6, v6, v4

    iget-object v7, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v5, v7, p1, v1}, Ljava/lang/String;->getChars(II[CI)V

    .line 162
    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-direct {p0, p1, v1, v5, p3}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->noDelimiter([CIIC)V

    .line 164
    iget-object v5, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v1

    sub-int/2addr v5, p2

    goto :goto_0

    .line 155
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    .line 156
    .local v3, "len":I
    iget-object v6, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v6, v5, v3, p1, v1}, Ljava/lang/String;->getChars(II[CI)V

    .line 157
    invoke-direct {p0, p1, v1, v3, p3}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->noDelimiter([CIIC)V

    .line 158
    add-int/2addr v1, v3

    .line 159
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "idx":I
    .local v2, "idx":I
    aput-char p3, p1, v1

    .line 154
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 169
    instance-of v3, p1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    if-nez v3, :cond_1

    .line 185
    :cond_0
    :goto_0
    return v2

    :cond_1
    move-object v1, p1

    .line 173
    check-cast v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    .line 174
    .local v1, "other":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    iget v3, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    iget v4, v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-ne v3, v4, :cond_0

    .line 180
    iget v3, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_1
    if-gez v0, :cond_2

    .line 185
    const/4 v2, 0x1

    goto :goto_0

    .line 181
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v3, v3, v0

    iget-object v4, v1, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 180
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public fullPathLength()I
    .locals 3

    .prologue
    .line 98
    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v2, :cond_0

    const/4 v0, 0x0

    .line 105
    :goto_0
    return v0

    .line 100
    :cond_0
    const/4 v0, 0x0

    .line 101
    .local v0, "charsNeeded":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-lt v1, v2, :cond_1

    .line 104
    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    add-int/lit8 v2, v2, -0x1

    add-int/2addr v0, v2

    .line 105
    goto :goto_0

    .line 102
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    .line 101
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 190
    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v2, :cond_1

    .line 191
    const/4 v0, 0x0

    .line 198
    :cond_0
    return v0

    .line 194
    :cond_1
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    .line 195
    .local v0, "hash":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-ge v1, v2, :cond_0

    .line 196
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int v0, v2, v3

    .line 195
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public longHashCode()J
    .locals 8

    .prologue
    .line 203
    iget v3, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v3, :cond_1

    .line 204
    const-wide/16 v0, 0x0

    .line 211
    :cond_0
    return-wide v0

    .line 207
    :cond_1
    iget v3, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    int-to-long v0, v3

    .line 208
    .local v0, "hash":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-ge v2, v3, :cond_0

    .line 209
    const-wide/32 v4, 0x1003f

    mul-long/2addr v4, v0

    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    int-to-long v6, v3

    add-long v0, v4, v6

    .line 208
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public subpath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .locals 1
    .param p1, "length"    # I

    .prologue
    .line 216
    iget v0, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 221
    .end local p0    # "this":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    :cond_0
    :goto_0
    return-object p0

    .line 218
    .restart local p0    # "this":Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    :cond_1
    if-nez p1, :cond_2

    .line 219
    sget-object p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->EMPTY:Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    goto :goto_0

    .line 221
    :cond_2
    new-instance v0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;-><init>(Lorg/apache/lucene/facet/taxonomy/CategoryPath;I)V

    move-object p0, v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->toString(C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(C)Ljava/lang/String;
    .locals 4
    .param p1, "delimiter"    # C

    .prologue
    .line 241
    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-nez v2, :cond_0

    const-string v2, ""

    .line 251
    :goto_0
    return-object v2

    .line 243
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 244
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->length:I

    if-lt v0, v2, :cond_1

    .line 250
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 251
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 245
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 246
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->hasDelimiter(Ljava/lang/String;C)V

    .line 248
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/CategoryPath;->components:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 244
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
