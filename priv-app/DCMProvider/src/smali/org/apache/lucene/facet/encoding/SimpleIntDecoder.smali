.class public final Lorg/apache/lucene/facet/encoding/SimpleIntDecoder;
.super Lorg/apache/lucene/facet/encoding/IntDecoder;
.source "SimpleIntDecoder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/facet/encoding/IntDecoder;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V
    .locals 8
    .param p1, "buf"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "values"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 34
    const/4 v4, 0x0

    iput v4, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v4, p2, Lorg/apache/lucene/util/IntsRef;->offset:I

    .line 35
    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    div-int/lit8 v0, v4, 0x4

    .line 36
    .local v0, "numValues":I
    iget-object v4, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    array-length v4, v4

    if-ge v4, v0, :cond_0

    .line 37
    const/4 v4, 0x4

    invoke-static {v0, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v4

    new-array v4, v4, [I

    iput-object v4, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 40
    :cond_0
    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 41
    .local v1, "offset":I
    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v5, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v3, v4, v5

    .local v3, "upto":I
    move v2, v1

    .line 42
    .end local v1    # "offset":I
    .local v2, "offset":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 49
    return-void

    .line 43
    :cond_1
    iget-object v4, p2, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v5, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p2, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 44
    iget-object v6, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    aget-byte v6, v6, v2

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x18

    .line 45
    iget-object v7, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    aget-byte v7, v7, v1

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x10

    .line 44
    or-int/2addr v6, v7

    .line 46
    iget-object v7, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    aget-byte v7, v7, v2

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    .line 44
    or-int/2addr v6, v7

    .line 47
    iget-object v7, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    aget-byte v7, v7, v1

    and-int/lit16 v7, v7, 0xff

    .line 44
    or-int/2addr v6, v7

    .line 43
    aput v6, v4, v5

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "Simple"

    return-object v0
.end method
