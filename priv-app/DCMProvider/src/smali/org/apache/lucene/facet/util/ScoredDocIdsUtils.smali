.class public Lorg/apache/lucene/facet/util/ScoredDocIdsUtils;
.super Ljava/lang/Object;
.source "ScoredDocIdsUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;,
        Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static clearDeleted(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/FixedBitSet;)V
    .locals 9
    .param p0, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "set"    # Lorg/apache/lucene/util/FixedBitSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v6

    if-nez v6, :cond_1

    .line 103
    :cond_0
    return-void

    .line 84
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/util/FixedBitSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v2

    .line 85
    .local v2, "it":Lorg/apache/lucene/search/DocIdSetIterator;
    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v1

    .line 86
    .local v1, "doc":I
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 87
    .local v0, "context":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v5

    .line 88
    .local v5, "r":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v7

    iget v8, v0, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    add-int v4, v7, v8

    .line 89
    .local v4, "maxDoc":I
    if-ge v1, v4, :cond_2

    .line 92
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->hasDeletions()Z

    move-result v7

    if-nez v7, :cond_4

    .line 93
    :cond_3
    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v1

    if-lt v1, v4, :cond_3

    goto :goto_0

    .line 96
    :cond_4
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v3

    .line 98
    .local v3, "liveDocs":Lorg/apache/lucene/util/Bits;
    :cond_5
    iget v7, v0, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    sub-int v7, v1, v7

    invoke-interface {v3, v7}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v7

    if-nez v7, :cond_6

    .line 99
    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/FixedBitSet;->clear(I)V

    .line 101
    :cond_6
    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v1

    .line 97
    if-lt v1, v4, :cond_5

    goto :goto_0
.end method

.method public static final createAllDocsScoredDocIDs(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .locals 1
    .param p0, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 203
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    new-instance v0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllLiveDocsScoredDocIDs;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    .line 206
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;

    invoke-direct {v0, p0}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$AllDocsScoredDocIDs;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    goto :goto_0
.end method

.method public static final createScoredDocIDsSubset(Lorg/apache/lucene/facet/search/ScoredDocIDs;[I)Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .locals 7
    .param p0, "allDocIds"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .param p1, "sampleSet"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    move-object v1, p1

    .line 116
    .local v1, "docids":[I
    invoke-static {v1}, Ljava/util/Arrays;->sort([I)V

    .line 117
    array-length v6, v1

    new-array v4, v6, [F

    .line 119
    .local v4, "scores":[F
    invoke-interface {p0}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->iterator()Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;

    move-result-object v2

    .line 120
    .local v2, "it":Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;
    const/4 v3, 0x0

    .line 121
    .local v3, "n":I
    :cond_0
    :goto_0
    invoke-interface {v2}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->next()Z

    move-result v6

    if-eqz v6, :cond_1

    array-length v6, v1

    if-lt v3, v6, :cond_2

    .line 128
    :cond_1
    move v5, v3

    .line 130
    .local v5, "size":I
    new-instance v6, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;

    invoke-direct {v6, v5, v1, v4}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$1;-><init>(I[I[F)V

    return-object v6

    .line 122
    .end local v5    # "size":I
    :cond_2
    invoke-interface {v2}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getDocID()I

    move-result v0

    .line 123
    .local v0, "doc":I
    aget v6, v1, v3

    if-ne v0, v6, :cond_0

    .line 124
    invoke-interface {v2}, Lorg/apache/lucene/facet/search/ScoredDocIDsIterator;->getScore()F

    move-result v6

    aput v6, v4, v3

    .line 125
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static final createScoredDocIds(Lorg/apache/lucene/search/DocIdSet;I)Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .locals 1
    .param p0, "docIdSet"    # Lorg/apache/lucene/search/DocIdSet;
    .param p1, "maxDoc"    # I

    .prologue
    .line 213
    new-instance v0, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils$2;-><init>(Lorg/apache/lucene/search/DocIdSet;I)V

    return-object v0
.end method

.method public static final getComplementSet(Lorg/apache/lucene/facet/search/ScoredDocIDs;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .locals 6
    .param p0, "docids"    # Lorg/apache/lucene/facet/search/ScoredDocIDs;
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v4

    .line 56
    .local v4, "maxDoc":I
    invoke-interface {p0}, Lorg/apache/lucene/facet/search/ScoredDocIDs;->getDocIDs()Lorg/apache/lucene/search/DocIdSet;

    move-result-object v2

    .line 58
    .local v2, "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    instance-of v5, v2, Lorg/apache/lucene/util/FixedBitSet;

    if-eqz v5, :cond_1

    .line 60
    check-cast v2, Lorg/apache/lucene/util/FixedBitSet;

    .end local v2    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    invoke-virtual {v2}, Lorg/apache/lucene/util/FixedBitSet;->clone()Lorg/apache/lucene/util/FixedBitSet;

    move-result-object v0

    .line 69
    .local v0, "complement":Lorg/apache/lucene/util/FixedBitSet;
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v0, v5, v4}, Lorg/apache/lucene/util/FixedBitSet;->flip(II)V

    .line 70
    invoke-static {p1, v0}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils;->clearDeleted(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/FixedBitSet;)V

    .line 72
    invoke-static {v0, v4}, Lorg/apache/lucene/facet/util/ScoredDocIdsUtils;->createScoredDocIds(Lorg/apache/lucene/search/DocIdSet;I)Lorg/apache/lucene/facet/search/ScoredDocIDs;

    move-result-object v5

    return-object v5

    .line 62
    .end local v0    # "complement":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v2    # "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    :cond_1
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-direct {v0, v4}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 63
    .restart local v0    # "complement":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v3

    .line 65
    .local v3, "iter":Lorg/apache/lucene/search/DocIdSetIterator;
    :goto_0
    invoke-virtual {v3}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v1

    .local v1, "doc":I
    if-ge v1, v4, :cond_0

    .line 66
    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    goto :goto_0
.end method
