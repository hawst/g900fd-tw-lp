.class public abstract Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
.super Ljava/lang/Object;
.source "TaxonomyReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final INVALID_ORDINAL:I = -0x1

.field public static final ROOT_ORDINAL:I


# instance fields
.field private volatile closed:Z

.field private final refCount:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->$assertionsDisabled:Z

    .line 105
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->closed:Z

    .line 128
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 66
    return-void
.end method

.method public static openIfChanged(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;)Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;",
            ">(TT;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    .local p0, "oldTaxoReader":Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;, "TT;"
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->doOpenIfChanged()Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;

    move-result-object v0

    .line 121
    .local v0, "newTaxoReader":Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;, "TT;"
    sget-boolean v1, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-ne v0, p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 122
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->closed:Z

    if-nez v0, :cond_1

    .line 156
    monitor-enter p0

    .line 157
    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->closed:Z

    if-nez v0, :cond_0

    .line 158
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->decRef()V

    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->closed:Z

    .line 156
    :cond_0
    monitor-exit p0

    .line 163
    :cond_1
    return-void

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final decRef()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->ensureOpen()V

    .line 171
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 172
    .local v0, "rc":I
    if-nez v0, :cond_2

    .line 173
    const/4 v1, 0x0

    .line 175
    .local v1, "success":Z
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->doClose()V

    .line 176
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->closed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    const/4 v1, 0x1

    .line 179
    if-nez v1, :cond_0

    .line 181
    iget-object v2, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 187
    .end local v1    # "success":Z
    :cond_0
    return-void

    .line 178
    .restart local v1    # "success":Z
    :catchall_0
    move-exception v2

    .line 179
    if-nez v1, :cond_1

    .line 181
    iget-object v3, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 183
    :cond_1
    throw v2

    .line 184
    .end local v1    # "success":Z
    :cond_2
    if-gez v0, :cond_0

    .line 185
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "too many decRef calls: refCount is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " after decrement"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected abstract doClose()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract doOpenIfChanged()Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected final ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 148
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getRefCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 149
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this TaxonomyReader is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_0
    return-void
.end method

.method public getChildren(I)Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;
    .locals 4
    .param p1, "ordinal"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->getParallelTaxonomyArrays()Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;

    move-result-object v0

    .line 198
    .local v0, "arrays":Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;
    if-ltz p1, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;->children()[I

    move-result-object v2

    aget v1, v2, p1

    .line 199
    .local v1, "child":I
    :goto_0
    new-instance v2, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;->siblings()[I

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader$ChildrenIterator;-><init>(I[I)V

    return-object v2

    .line 198
    .end local v1    # "child":I
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public abstract getCommitUserData()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getOrdinal(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getParallelTaxonomyArrays()Lorg/apache/lucene/facet/taxonomy/ParallelTaxonomyArrays;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getPath(I)Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final getRefCount()I
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public abstract getSize()I
.end method

.method public final incRef()V
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p0}, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->ensureOpen()V

    .line 246
    iget-object v0, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 247
    return-void
.end method

.method public final tryIncRef()Z
    .locals 3

    .prologue
    .line 254
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .local v0, "count":I
    if-gtz v0, :cond_1

    .line 259
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 255
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 256
    const/4 v1, 0x1

    goto :goto_0
.end method
