.class public abstract Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;
.super Lorg/apache/lucene/facet/search/FacetResultsHandler;
.source "PartitionsFacetResultsHandler.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V
    .locals 0
    .param p1, "taxonomyReader"    # Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;
    .param p2, "facetRequest"    # Lorg/apache/lucene/facet/search/FacetRequest;
    .param p3, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/facet/search/FacetResultsHandler;-><init>(Lorg/apache/lucene/facet/taxonomy/TaxonomyReader;Lorg/apache/lucene/facet/search/FacetRequest;Lorg/apache/lucene/facet/search/FacetArrays;)V

    .line 41
    return-void
.end method


# virtual methods
.method public final compute()Lorg/apache/lucene/facet/search/FacetResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;->fetchPartitionResult(I)Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;->renderFacetResult(Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;)Lorg/apache/lucene/facet/search/FacetResult;

    move-result-object v0

    .line 133
    .local v0, "res":Lorg/apache/lucene/facet/search/FacetResult;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/facet/partitions/PartitionsFacetResultsHandler;->labelResult(Lorg/apache/lucene/facet/search/FacetResult;)V

    .line 134
    return-object v0
.end method

.method public abstract fetchPartitionResult(I)Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected isSelfPartition(ILorg/apache/lucene/facet/search/FacetArrays;I)Z
    .locals 3
    .param p1, "ordinal"    # I
    .param p2, "facetArrays"    # Lorg/apache/lucene/facet/search/FacetArrays;
    .param p3, "offset"    # I

    .prologue
    .line 126
    iget v0, p2, Lorg/apache/lucene/facet/search/FacetArrays;->arrayLength:I

    .line 127
    .local v0, "partitionSize":I
    div-int v1, p1, v0

    div-int v2, p3, v0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public abstract labelResult(Lorg/apache/lucene/facet/search/FacetResult;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public varargs abstract mergeResults([Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;)Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract rearrangeFacetResult(Lorg/apache/lucene/facet/search/FacetResult;)Lorg/apache/lucene/facet/search/FacetResult;
.end method

.method public abstract renderFacetResult(Lorg/apache/lucene/facet/partitions/IntermediateFacetResult;)Lorg/apache/lucene/facet/search/FacetResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
