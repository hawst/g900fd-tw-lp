.class public interface abstract Lorg/apache/lucene/facet/taxonomy/TaxonomyWriter;
.super Ljava/lang/Object;
.source "TaxonomyWriter.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Lorg/apache/lucene/index/TwoPhaseCommit;


# virtual methods
.method public abstract addCategory(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getCommitData()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getParent(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getSize()I
.end method

.method public abstract setCommitData(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method
