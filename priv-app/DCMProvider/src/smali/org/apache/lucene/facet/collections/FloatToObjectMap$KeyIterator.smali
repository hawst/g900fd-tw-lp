.class final Lorg/apache/lucene/facet/collections/FloatToObjectMap$KeyIterator;
.super Ljava/lang/Object;
.source "FloatToObjectMap.java"

# interfaces
.implements Lorg/apache/lucene/facet/collections/FloatIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/FloatToObjectMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "KeyIterator"
.end annotation


# instance fields
.field private iterator:Lorg/apache/lucene/facet/collections/IntIterator;

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/FloatToObjectMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/collections/FloatToObjectMap;)V
    .locals 1

    .prologue
    .line 112
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$KeyIterator;->this$0:Lorg/apache/lucene/facet/collections/FloatToObjectMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/collections/FloatToObjectMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/FloatToObjectMap;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    .line 112
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()F
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$KeyIterator;->this$0:Lorg/apache/lucene/facet/collections/FloatToObjectMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/FloatToObjectMap;->keys:[F

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/FloatToObjectMap$KeyIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->remove()V

    .line 127
    return-void
.end method
