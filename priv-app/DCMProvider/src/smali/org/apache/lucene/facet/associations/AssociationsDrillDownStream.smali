.class public Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;
.super Lorg/apache/lucene/facet/index/DrillDownStream;
.source "AssociationsDrillDownStream.java"


# instance fields
.field private final associations:Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

.field private final output:Lorg/apache/lucene/store/ByteArrayDataOutput;

.field private final payload:Lorg/apache/lucene/util/BytesRef;

.field private final payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V
    .locals 2
    .param p1, "associations"    # Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;
    .param p2, "indexingParams"    # Lorg/apache/lucene/facet/params/FacetIndexingParams;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/facet/index/DrillDownStream;-><init>(Ljava/lang/Iterable;Lorg/apache/lucene/facet/params/FacetIndexingParams;)V

    .line 38
    new-instance v1, Lorg/apache/lucene/store/ByteArrayDataOutput;

    invoke-direct {v1}, Lorg/apache/lucene/store/ByteArrayDataOutput;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->output:Lorg/apache/lucene/store/ByteArrayDataOutput;

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->associations:Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    .line 44
    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iput-object v1, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 45
    iget-object v1, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 46
    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    .end local v0    # "bytes":Lorg/apache/lucene/util/BytesRef;
    const/4 v1, 0x4

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    .line 48
    .restart local v0    # "bytes":Lorg/apache/lucene/util/BytesRef;
    iget-object v1, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-interface {v1, v0}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->setPayload(Lorg/apache/lucene/util/BytesRef;)V

    .line 50
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 51
    iput-object v0, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 52
    return-void
.end method


# virtual methods
.method protected addAdditionalAttributes(Lorg/apache/lucene/facet/taxonomy/CategoryPath;Z)V
    .locals 3
    .param p1, "cp"    # Lorg/apache/lucene/facet/taxonomy/CategoryPath;
    .param p2, "isParent"    # Z

    .prologue
    .line 56
    if-eqz p2, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->associations:Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/facet/associations/CategoryAssociationsContainer;->getAssociation(Lorg/apache/lucene/facet/taxonomy/CategoryPath;)Lorg/apache/lucene/facet/associations/CategoryAssociation;

    move-result-object v0

    .line 61
    .local v0, "association":Lorg/apache/lucene/facet/associations/CategoryAssociation;
    if-eqz v0, :cond_0

    .line 66
    iget-object v1, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    invoke-interface {v0}, Lorg/apache/lucene/facet/associations/CategoryAssociation;->maxBytesNeeded()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 67
    iget-object v1, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0}, Lorg/apache/lucene/facet/associations/CategoryAssociation;->maxBytesNeeded()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 69
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->output:Lorg/apache/lucene/store/ByteArrayDataOutput;

    iget-object v2, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/ByteArrayDataOutput;->reset([B)V

    .line 70
    iget-object v1, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->output:Lorg/apache/lucene/store/ByteArrayDataOutput;

    invoke-interface {v0, v1}, Lorg/apache/lucene/facet/associations/CategoryAssociation;->serialize(Lorg/apache/lucene/store/ByteArrayDataOutput;)V

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p0, Lorg/apache/lucene/facet/associations/AssociationsDrillDownStream;->output:Lorg/apache/lucene/store/ByteArrayDataOutput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/ByteArrayDataOutput;->getPosition()I

    move-result v2

    iput v2, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_0
.end method
