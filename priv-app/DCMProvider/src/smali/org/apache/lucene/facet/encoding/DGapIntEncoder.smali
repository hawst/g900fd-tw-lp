.class public final Lorg/apache/lucene/facet/encoding/DGapIntEncoder;
.super Lorg/apache/lucene/facet/encoding/IntEncoderFilter;
.source "DGapIntEncoder.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/facet/encoding/IntEncoder;)V
    .locals 0
    .param p1, "encoder"    # Lorg/apache/lucene/facet/encoding/IntEncoder;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lorg/apache/lucene/facet/encoding/IntEncoderFilter;-><init>(Lorg/apache/lucene/facet/encoding/IntEncoder;)V

    .line 43
    return-void
.end method


# virtual methods
.method public createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lorg/apache/lucene/facet/encoding/DGapIntDecoder;

    iget-object v1, p0, Lorg/apache/lucene/facet/encoding/DGapIntEncoder;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    invoke-virtual {v1}, Lorg/apache/lucene/facet/encoding/IntEncoder;->createMatchingDecoder()Lorg/apache/lucene/facet/encoding/IntDecoder;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/facet/encoding/DGapIntDecoder;-><init>(Lorg/apache/lucene/facet/encoding/IntDecoder;)V

    return-object v0
.end method

.method public encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 6
    .param p1, "values"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "buf"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 47
    const/4 v1, 0x0

    .line 48
    .local v1, "prev":I
    iget v4, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int v3, v4, v5

    .line 49
    .local v3, "upto":I
    iget v0, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    .local v0, "i":I
    :goto_0
    if-lt v0, v3, :cond_0

    .line 54
    iget-object v4, p0, Lorg/apache/lucene/facet/encoding/DGapIntEncoder;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    invoke-virtual {v4, p1, p2}, Lorg/apache/lucene/facet/encoding/IntEncoder;->encode(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 55
    return-void

    .line 50
    :cond_0
    iget-object v4, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v2, v4, v0

    .line 51
    .local v2, "tmp":I
    iget-object v4, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v5, v4, v0

    sub-int/2addr v5, v1

    aput v5, v4, v0

    .line 52
    move v1, v2

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DGap("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/facet/encoding/DGapIntEncoder;->encoder:Lorg/apache/lucene/facet/encoding/IntEncoder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
