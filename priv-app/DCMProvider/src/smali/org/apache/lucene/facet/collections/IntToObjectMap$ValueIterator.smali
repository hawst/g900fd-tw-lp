.class final Lorg/apache/lucene/facet/collections/IntToObjectMap$ValueIterator;
.super Ljava/lang/Object;
.source "IntToObjectMap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/facet/collections/IntToObjectMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ValueIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private iterator:Lorg/apache/lucene/facet/collections/IntIterator;

.field final synthetic this$0:Lorg/apache/lucene/facet/collections/IntToObjectMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/facet/collections/IntToObjectMap;)V
    .locals 1

    .prologue
    .line 136
    iput-object p1, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToObjectMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    new-instance v0, Lorg/apache/lucene/facet/collections/IntToObjectMap$IndexIterator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/facet/collections/IntToObjectMap$IndexIterator;-><init>(Lorg/apache/lucene/facet/collections/IntToObjectMap;)V

    iput-object v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    .line 136
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$ValueIterator;->this$0:Lorg/apache/lucene/facet/collections/IntToObjectMap;

    iget-object v0, v0, Lorg/apache/lucene/facet/collections/IntToObjectMap;->values:[Ljava/lang/Object;

    iget-object v1, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v1}, Lorg/apache/lucene/facet/collections/IntIterator;->next()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/facet/collections/IntToObjectMap$ValueIterator;->iterator:Lorg/apache/lucene/facet/collections/IntIterator;

    invoke-interface {v0}, Lorg/apache/lucene/facet/collections/IntIterator;->remove()V

    .line 152
    return-void
.end method
