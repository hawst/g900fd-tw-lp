.class public abstract Lorg/apache/lucene/queries/function/valuesource/ConstNumberSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "ConstNumberSource.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getBool()Z
.end method

.method public abstract getDouble()D
.end method

.method public abstract getFloat()F
.end method

.method public abstract getInt()I
.end method

.method public abstract getLong()J
.end method

.method public abstract getNumber()Ljava/lang/Number;
.end method
