.class public Lorg/apache/lucene/queries/BoostingQuery;
.super Lorg/apache/lucene/search/Query;
.source "BoostingQuery.java"


# instance fields
.field private final boost:F

.field private final context:Lorg/apache/lucene/search/Query;

.field private final match:Lorg/apache/lucene/search/Query;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;F)V
    .locals 2
    .param p1, "match"    # Lorg/apache/lucene/search/Query;
    .param p2, "context"    # Lorg/apache/lucene/search/Query;
    .param p3, "boost"    # F

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 46
    iput-object p1, p0, Lorg/apache/lucene/queries/BoostingQuery;->match:Lorg/apache/lucene/search/Query;

    .line 47
    invoke-virtual {p2}, Lorg/apache/lucene/search/Query;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/BoostingQuery;->context:Lorg/apache/lucene/search/Query;

    .line 48
    iput p3, p0, Lorg/apache/lucene/queries/BoostingQuery;->boost:F

    .line 49
    iget-object v0, p0, Lorg/apache/lucene/queries/BoostingQuery;->context:Lorg/apache/lucene/search/Query;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 50
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/queries/BoostingQuery;)F
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lorg/apache/lucene/queries/BoostingQuery;->boost:F

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 96
    if-ne p0, p1, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v1

    .line 99
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 100
    goto :goto_0

    .line 102
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 103
    goto :goto_0

    .line 106
    :cond_3
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 107
    goto :goto_0

    :cond_4
    move-object v0, p1

    .line 110
    check-cast v0, Lorg/apache/lucene/queries/BoostingQuery;

    .line 111
    .local v0, "other":Lorg/apache/lucene/queries/BoostingQuery;
    iget v3, p0, Lorg/apache/lucene/queries/BoostingQuery;->boost:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lorg/apache/lucene/queries/BoostingQuery;->boost:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 112
    goto :goto_0

    .line 115
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/queries/BoostingQuery;->context:Lorg/apache/lucene/search/Query;

    if-nez v3, :cond_6

    .line 116
    iget-object v3, v0, Lorg/apache/lucene/queries/BoostingQuery;->context:Lorg/apache/lucene/search/Query;

    if-eqz v3, :cond_7

    move v1, v2

    .line 117
    goto :goto_0

    .line 119
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/queries/BoostingQuery;->context:Lorg/apache/lucene/search/Query;

    iget-object v4, v0, Lorg/apache/lucene/queries/BoostingQuery;->context:Lorg/apache/lucene/search/Query;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 120
    goto :goto_0

    .line 123
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/queries/BoostingQuery;->match:Lorg/apache/lucene/search/Query;

    if-nez v3, :cond_8

    .line 124
    iget-object v3, v0, Lorg/apache/lucene/queries/BoostingQuery;->match:Lorg/apache/lucene/search/Query;

    if-eqz v3, :cond_0

    move v1, v2

    .line 125
    goto :goto_0

    .line 127
    :cond_8
    iget-object v3, p0, Lorg/apache/lucene/queries/BoostingQuery;->match:Lorg/apache/lucene/search/Query;

    iget-object v4, v0, Lorg/apache/lucene/queries/BoostingQuery;->match:Lorg/apache/lucene/search/Query;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 128
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 86
    const/16 v0, 0x1f

    .line 87
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v1

    .line 88
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/queries/BoostingQuery;->boost:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 89
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/queries/BoostingQuery;->context:Lorg/apache/lucene/search/Query;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 90
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/queries/BoostingQuery;->match:Lorg/apache/lucene/search/Query;

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 91
    return v1

    .line 89
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queries/BoostingQuery;->context:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v2

    goto :goto_0

    .line 90
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/queries/BoostingQuery;->match:Lorg/apache/lucene/search/Query;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lorg/apache/lucene/queries/BoostingQuery$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/queries/BoostingQuery$1;-><init>(Lorg/apache/lucene/queries/BoostingQuery;)V

    .line 78
    .local v0, "result":Lorg/apache/lucene/search/BooleanQuery;
    iget-object v1, p0, Lorg/apache/lucene/queries/BoostingQuery;->match:Lorg/apache/lucene/search/Query;

    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 79
    iget-object v1, p0, Lorg/apache/lucene/queries/BoostingQuery;->context:Lorg/apache/lucene/search/Query;

    sget-object v2, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 81
    return-object v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/BoostingQuery;->match:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/BoostingQuery;->context:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
