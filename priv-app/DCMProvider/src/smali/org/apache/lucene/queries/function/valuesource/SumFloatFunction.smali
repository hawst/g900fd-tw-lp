.class public Lorg/apache/lucene/queries/function/valuesource/SumFloatFunction;
.super Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;
.source "SumFloatFunction.java"


# direct methods
.method public constructor <init>([Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "sources"    # [Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;-><init>([Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected func(I[Lorg/apache/lucene/queries/function/FunctionValues;)F
    .locals 5
    .param p1, "doc"    # I
    .param p2, "valsArr"    # [Lorg/apache/lucene/queries/function/FunctionValues;

    .prologue
    .line 38
    const/4 v0, 0x0

    .line 39
    .local v0, "val":F
    array-length v3, p2

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 42
    return v0

    .line 39
    :cond_0
    aget-object v1, p2, v2

    .line 40
    .local v1, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v4

    add-float/2addr v0, v4

    .line 39
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const-string v0, "sum"

    return-object v0
.end method
