.class public Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "ScaleFloatFunction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;
    }
.end annotation


# instance fields
.field protected final max:F

.field protected final min:F

.field protected final source:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;FF)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/queries/function/ValueSource;
    .param p2, "min"    # F
    .param p3, "max"    # F

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 48
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    .line 49
    iput p2, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->min:F

    .line 50
    iput p3, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->max:F

    .line 51
    return-void
.end method

.method private createScaleInfo(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;
    .locals 12
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/high16 v11, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 64
    invoke-static {p2}, Lorg/apache/lucene/index/ReaderUtil;->getTopLevelContext(Lorg/apache/lucene/index/IndexReaderContext;)Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/lucene/index/IndexReaderContext;->leaves()Ljava/util/List;

    move-result-object v2

    .line 66
    .local v2, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    const/high16 v5, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 67
    .local v5, "minVal":F
    const/high16 v4, -0x800000    # Float.NEGATIVE_INFINITY

    .line 69
    .local v4, "maxVal":F
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 89
    const/high16 v9, 0x7f800000    # Float.POSITIVE_INFINITY

    cmpl-float v9, v5, v9

    if-nez v9, :cond_1

    .line 91
    const/4 v4, 0x0

    move v5, v4

    .line 94
    :cond_1
    new-instance v6, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;

    const/4 v9, 0x0

    invoke-direct {v6, v9}, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;-><init>(Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;)V

    .line 95
    .local v6, "scaleInfo":Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;
    iput v5, v6, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;->minVal:F

    .line 96
    iput v4, v6, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;->maxVal:F

    .line 97
    iget-object v9, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-interface {p1, v9, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    return-object v6

    .line 69
    .end local v6    # "scaleInfo":Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 70
    .local v1, "leaf":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    .line 71
    .local v3, "maxDoc":I
    iget-object v10, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v10, p1, v1}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v8

    .line 72
    .local v8, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 74
    invoke-virtual {v8, v0}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v7

    .line 75
    .local v7, "val":F
    invoke-static {v7}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v10

    and-int/2addr v10, v11

    if-ne v10, v11, :cond_4

    .line 72
    :cond_3
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_4
    cmpg-float v10, v7, v5

    if-gez v10, :cond_5

    .line 81
    move v5, v7

    .line 83
    :cond_5
    cmpl-float v10, v7, v4

    if-lez v10, :cond_3

    .line 84
    move v4, v7

    goto :goto_1
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 133
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "scale("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->min:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->max:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 147
    const-class v2, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 148
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;

    .line 149
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->min:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->min:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 150
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->max:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->max:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 151
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 8
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 104
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;

    .line 105
    .local v7, "scaleInfo":Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;
    if-nez v7, :cond_0

    .line 106
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->createScaleInfo(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;

    move-result-object v7

    .line 109
    :cond_0
    iget v0, v7, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;->maxVal:F

    iget v1, v7, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;->minVal:F

    sub-float/2addr v0, v1

    cmpl-float v0, v0, v5

    if-nez v0, :cond_1

    .line 110
    .local v5, "scale":F
    :goto_0
    iget v4, v7, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;->minVal:F

    .line 111
    .local v4, "minSource":F
    iget v6, v7, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;->maxVal:F

    .line 113
    .local v6, "maxSource":F
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v3

    .line 115
    .local v3, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;FFF)V

    return-object v0

    .line 109
    .end local v3    # "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    .end local v4    # "minSource":F
    .end local v5    # "scale":F
    .end local v6    # "maxSource":F
    :cond_1
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->max:F

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->min:F

    sub-float/2addr v0, v1

    iget v1, v7, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;->maxVal:F

    iget v2, v7, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$ScaleInfo;->minVal:F

    sub-float/2addr v1, v2

    div-float v5, v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 137
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->min:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 138
    .local v0, "h":I
    mul-int/lit8 v0, v0, 0x1d

    .line 139
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->max:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 140
    mul-int/lit8 v0, v0, 0x1d

    .line 141
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    return v0
.end method
