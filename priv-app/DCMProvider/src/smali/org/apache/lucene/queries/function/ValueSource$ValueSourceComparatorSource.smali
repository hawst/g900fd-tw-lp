.class Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparatorSource;
.super Lorg/apache/lucene/search/FieldComparatorSource;
.source "ValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/function/ValueSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ValueSourceComparatorSource"
.end annotation


# instance fields
.field private final context:Ljava/util/Map;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;Ljava/util/Map;)V
    .locals 0
    .param p2, "context"    # Ljava/util/Map;

    .prologue
    .line 113
    iput-object p1, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparatorSource;->this$0:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparatorSource;-><init>()V

    .line 114
    iput-object p2, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparatorSource;->context:Ljava/util/Map;

    .line 115
    return-void
.end method


# virtual methods
.method public newComparator(Ljava/lang/String;IIZ)Lorg/apache/lucene/search/FieldComparator;
    .locals 3
    .param p1, "fieldname"    # Ljava/lang/String;
    .param p2, "numHits"    # I
    .param p3, "sortPos"    # I
    .param p4, "reversed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIZ)",
            "Lorg/apache/lucene/search/FieldComparator",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    new-instance v0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparatorSource;->this$0:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparatorSource;->context:Ljava/util/Map;

    invoke-direct {v0, v1, v2, p2}, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;-><init>(Lorg/apache/lucene/queries/function/ValueSource;Ljava/util/Map;I)V

    return-object v0
.end method
