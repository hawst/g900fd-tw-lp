.class public Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "OrdFieldSource.java"


# static fields
.field private static final hcode:I


# instance fields
.field protected final field:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    const-class v0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    sput v0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;->hcode:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 59
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;->field:Ljava/lang/String;

    .line 60
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ord("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 121
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;->field:Ljava/lang/String;

    check-cast p1, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 6
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget v0, p2, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    .line 72
    .local v0, "off":I
    invoke-static {p2}, Lorg/apache/lucene/index/ReaderUtil;->getTopLevelContext(Lorg/apache/lucene/index/IndexReaderContext;)Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexReaderContext;->reader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v3

    .line 73
    .local v3, "topReader":Lorg/apache/lucene/index/IndexReader;
    instance-of v4, v3, Lorg/apache/lucene/index/CompositeReader;

    if-eqz v4, :cond_0

    .line 74
    new-instance v1, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;

    check-cast v3, Lorg/apache/lucene/index/CompositeReader;

    .end local v3    # "topReader":Lorg/apache/lucene/index/IndexReader;
    invoke-direct {v1, v3}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;-><init>(Lorg/apache/lucene/index/CompositeReader;)V

    .line 76
    .local v1, "r":Lorg/apache/lucene/index/AtomicReader;
    :goto_0
    sget-object v4, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v5, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;->field:Ljava/lang/String;

    invoke-interface {v4, v1, v5}, Lorg/apache/lucene/search/FieldCache;->getTermsIndex(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v2

    .line 77
    .local v2, "sindex":Lorg/apache/lucene/index/SortedDocValues;
    new-instance v4, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;

    invoke-direct {v4, p0, p0, v2, v0}, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/index/SortedDocValues;I)V

    return-object v4

    .line 75
    .end local v1    # "r":Lorg/apache/lucene/index/AtomicReader;
    .end local v2    # "sindex":Lorg/apache/lucene/index/SortedDocValues;
    .restart local v3    # "topReader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    check-cast v3, Lorg/apache/lucene/index/AtomicReader;

    move-object v1, v3

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 127
    sget v0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;->hcode:I

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
