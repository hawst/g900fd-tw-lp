.class public abstract Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;
.super Lorg/apache/lucene/queries/function/valuesource/BoolFunction;
.source "SimpleBoolFunction.java"


# instance fields
.field protected final source:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/valuesource/BoolFunction;-><init>()V

    .line 40
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    .line 41
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 82
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 74
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    .line 76
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 75
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;

    .line 76
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method protected abstract func(ILorg/apache/lucene/queries/function/FunctionValues;)Z
.end method

.method public bridge synthetic getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;
    .locals 2
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    .line 50
    .local v0, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    new-instance v1, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction$1;

    invoke-direct {v1, p0, p0, v0}, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected abstract name()Ljava/lang/String;
.end method
