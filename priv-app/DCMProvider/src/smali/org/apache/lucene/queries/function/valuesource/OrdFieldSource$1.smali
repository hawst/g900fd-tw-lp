.class Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;
.super Lorg/apache/lucene/queries/function/docvalues/IntDocValues;
.source "OrdFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;

.field private final synthetic val$off:I

.field private final synthetic val$sindex:Lorg/apache/lucene/index/SortedDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/index/SortedDocValues;I)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->val$sindex:Lorg/apache/lucene/index/SortedDocValues;

    iput p4, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->val$off:I

    .line 77
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/IntDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public exists(I)Z
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->val$sindex:Lorg/apache/lucene/index/SortedDocValues;

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->val$off:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1$1;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->val$sindex:Lorg/apache/lucene/index/SortedDocValues;

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;Lorg/apache/lucene/index/SortedDocValues;)V

    return-object v0
.end method

.method public intVal(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->val$sindex:Lorg/apache/lucene/index/SortedDocValues;

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->val$off:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    return v0
.end method

.method public numOrd()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->val$sindex:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v0

    return v0
.end method

.method public ordVal(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->val$sindex:Lorg/apache/lucene/index/SortedDocValues;

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->val$off:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    return v0
.end method

.method protected toTerm(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "readableValue"    # Ljava/lang/String;

    .prologue
    .line 79
    return-object p1
.end method
