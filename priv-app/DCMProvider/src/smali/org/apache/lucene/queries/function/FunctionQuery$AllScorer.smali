.class public Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "FunctionQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/function/FunctionQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "AllScorer"
.end annotation


# instance fields
.field final acceptDocs:Lorg/apache/lucene/util/Bits;

.field doc:I

.field final maxDoc:I

.field final qWeight:F

.field final reader:Lorg/apache/lucene/index/IndexReader;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/FunctionQuery;

.field final vals:Lorg/apache/lucene/queries/function/FunctionValues;

.field final weight:Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/FunctionQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;F)V
    .locals 2
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .param p4, "w"    # Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;
    .param p5, "qWeight"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iput-object p1, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->this$0:Lorg/apache/lucene/queries/function/FunctionQuery;

    .line 114
    invoke-direct {p0, p4}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 109
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->doc:I

    .line 115
    iput-object p4, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->weight:Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;

    .line 116
    iput p5, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->qWeight:F

    .line 117
    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 118
    iget-object v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->maxDoc:I

    .line 119
    iput-object p3, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->acceptDocs:Lorg/apache/lucene/util/Bits;

    .line 120
    iget-object v0, p1, Lorg/apache/lucene/queries/function/FunctionQuery;->func:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->weight:Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;

    iget-object v1, v1, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->context:Ljava/util/Map;

    invoke-virtual {v0, v1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->vals:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 121
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->doc:I

    .line 148
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->nextDoc()I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 163
    iget v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->maxDoc:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->doc:I

    return v0
.end method

.method public explain(I)Lorg/apache/lucene/search/Explanation;
    .locals 5
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    iget v2, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->qWeight:F

    iget-object v3, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v3

    mul-float v1, v2, v3

    .line 174
    .local v1, "sc":F
    new-instance v0, Lorg/apache/lucene/search/ComplexExplanation;

    .line 175
    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FunctionQuery("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->this$0:Lorg/apache/lucene/queries/function/FunctionQuery;

    iget-object v4, v4, Lorg/apache/lucene/queries/function/FunctionQuery;->func:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "), product of:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 174
    invoke-direct {v0, v2, v1, v3}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    .line 177
    .local v0, "result":Lorg/apache/lucene/search/Explanation;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->explain(I)Lorg/apache/lucene/search/Explanation;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 178
    new-instance v2, Lorg/apache/lucene/search/Explanation;

    iget-object v3, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->this$0:Lorg/apache/lucene/queries/function/FunctionQuery;

    invoke-virtual {v3}, Lorg/apache/lucene/queries/function/FunctionQuery;->getBoost()F

    move-result v3

    const-string v4, "boost"

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 179
    new-instance v2, Lorg/apache/lucene/search/Explanation;

    iget-object v3, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->weight:Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;

    iget v3, v3, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->queryNorm:F

    const-string v4, "queryNorm"

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 180
    return-object v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    :cond_0
    iget v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->doc:I

    .line 136
    iget v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->doc:I

    iget v1, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->maxDoc:I

    if-lt v0, v1, :cond_1

    .line 137
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->doc:I

    .line 140
    :goto_0
    return v0

    .line 139
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->acceptDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->acceptDocs:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->doc:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    :cond_2
    iget v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->doc:I

    goto :goto_0
.end method

.method public score()F
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    iget v1, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->qWeight:F

    iget-object v2, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->vals:Lorg/apache/lucene/queries/function/FunctionValues;

    iget v3, p0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->doc:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v2

    mul-float v0, v1, v2

    .line 158
    .local v0, "score":F
    const/high16 v1, -0x800000    # Float.NEGATIVE_INFINITY

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .end local v0    # "score":F
    :goto_0
    return v0

    .restart local v0    # "score":F
    :cond_0
    const v0, -0x800001

    goto :goto_0
.end method
