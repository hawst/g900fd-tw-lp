.class public final Lorg/apache/lucene/queries/TermsFilter;
.super Lorg/apache/lucene/search/Filter;
.source "TermsFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queries/TermsFilter$FieldAndTermEnum;,
        Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
    }
.end annotation


# static fields
.field private static final PRIME:I = 0x1f


# instance fields
.field private final hashCode:I

.field private final offsets:[I

.field private final termsAndFields:[Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

.field private final termsBytes:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p2, "terms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/BytesRef;>;"
    new-instance v0, Lorg/apache/lucene/queries/TermsFilter$2;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/queries/TermsFilter$2;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 95
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queries/TermsFilter;-><init>(Lorg/apache/lucene/queries/TermsFilter$FieldAndTermEnum;I)V

    .line 96
    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/String;[Lorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "terms"    # [Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 104
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queries/TermsFilter;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 105
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "terms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    new-instance v0, Lorg/apache/lucene/queries/TermsFilter$1;

    invoke-direct {v0, p1}, Lorg/apache/lucene/queries/TermsFilter$1;-><init>(Ljava/util/List;)V

    .line 77
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/queries/TermsFilter;-><init>(Lorg/apache/lucene/queries/TermsFilter$FieldAndTermEnum;I)V

    .line 78
    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/queries/TermsFilter$FieldAndTermEnum;I)V
    .locals 14
    .param p1, "iter"    # Lorg/apache/lucene/queries/TermsFilter$FieldAndTermEnum;
    .param p2, "length"    # I

    .prologue
    .line 116
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 127
    const/16 v2, 0x9

    .line 128
    .local v2, "hash":I
    const/4 v11, 0x0

    new-array v8, v11, [B

    .line 129
    .local v8, "serializedTerms":[B
    add-int/lit8 v11, p2, 0x1

    new-array v11, v11, [I

    iput-object v11, p0, Lorg/apache/lucene/queries/TermsFilter;->offsets:[I

    .line 130
    const/4 v4, 0x0

    .line 131
    .local v4, "lastEndOffset":I
    const/4 v3, 0x0

    .line 132
    .local v3, "index":I
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v10, "termsAndFields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/queries/TermsFilter$TermsAndField;>;"
    const/4 v5, 0x0

    .line 134
    .local v5, "lastTermsAndField":Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
    const/4 v7, 0x0

    .line 135
    .local v7, "previousTerm":Lorg/apache/lucene/util/BytesRef;
    const/4 v6, 0x0

    .line 138
    .local v6, "previousField":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/queries/TermsFilter$FieldAndTermEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .local v1, "currentTerm":Lorg/apache/lucene/util/BytesRef;
    if-nez v1, :cond_1

    .line 167
    iget-object v11, p0, Lorg/apache/lucene/queries/TermsFilter;->offsets:[I

    aput v4, v11, v3

    .line 168
    if-nez v5, :cond_7

    const/4 v9, 0x0

    .line 169
    .local v9, "start":I
    :goto_1
    new-instance v5, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    .end local v5    # "lastTermsAndField":Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
    invoke-direct {v5, v9, v3, v6}, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;-><init>(IILjava/lang/String;)V

    .line 170
    .restart local v5    # "lastTermsAndField":Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    invoke-static {v8, v4}, Lorg/apache/lucene/util/ArrayUtil;->shrink([BI)[B

    move-result-object v11

    iput-object v11, p0, Lorg/apache/lucene/queries/TermsFilter;->termsBytes:[B

    .line 172
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v11, v11, [Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    iput-object v11, p0, Lorg/apache/lucene/queries/TermsFilter;->termsAndFields:[Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    .line 173
    iput v2, p0, Lorg/apache/lucene/queries/TermsFilter;->hashCode:I

    .line 175
    return-void

    .line 139
    .end local v9    # "start":I
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/queries/TermsFilter$FieldAndTermEnum;->field()Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "currentField":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 141
    new-instance v11, Ljava/lang/IllegalArgumentException;

    const-string v12, "Field must not be null"

    invoke-direct {v11, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 143
    :cond_2
    if-eqz v6, :cond_3

    .line 145
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 146
    invoke-virtual {v7, v1}, Lorg/apache/lucene/util/BytesRef;->bytesEquals(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 155
    :cond_3
    :goto_2
    mul-int/lit8 v11, v2, 0x1f

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v12

    add-int v2, v11, v12

    .line 156
    mul-int/lit8 v11, v2, 0x1f

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v12

    add-int v2, v11, v12

    .line 157
    array-length v11, v8

    iget v12, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v12, v4

    if-ge v11, v12, :cond_4

    .line 158
    iget v11, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v11, v4

    invoke-static {v8, v11}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v8

    .line 160
    :cond_4
    iget-object v11, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v12, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v13, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v11, v12, v8, v4, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 161
    iget-object v11, p0, Lorg/apache/lucene/queries/TermsFilter;->offsets:[I

    aput v4, v11, v3

    .line 162
    iget v11, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v4, v11

    .line 163
    add-int/lit8 v3, v3, 0x1

    .line 164
    move-object v7, v1

    .line 165
    move-object v6, v0

    goto :goto_0

    .line 150
    :cond_5
    if-nez v5, :cond_6

    const/4 v9, 0x0

    .line 151
    .restart local v9    # "start":I
    :goto_3
    new-instance v5, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    .end local v5    # "lastTermsAndField":Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
    invoke-direct {v5, v9, v3, v6}, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;-><init>(IILjava/lang/String;)V

    .line 152
    .restart local v5    # "lastTermsAndField":Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 150
    .end local v9    # "start":I
    :cond_6
    iget v9, v5, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->end:I

    goto :goto_3

    .line 168
    .end local v0    # "currentField":Ljava/lang/String;
    :cond_7
    iget v9, v5, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->end:I

    goto :goto_1
.end method

.method public varargs constructor <init>([Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    .line 112
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/queries/TermsFilter;-><init>(Ljava/util/List;)V

    .line 113
    return-void
.end method

.method static synthetic access$0(Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 330
    invoke-static {p0}, Lorg/apache/lucene/queries/TermsFilter;->sort(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static sort(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 331
    .local p0, "toSort":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "no terms provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 334
    :cond_0
    invoke-static {p0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 335
    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 217
    if-ne p0, p1, :cond_1

    .line 244
    :cond_0
    :goto_0
    return v6

    .line 220
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    if-eq v8, v9, :cond_3

    :cond_2
    move v6, v7

    .line 221
    goto :goto_0

    :cond_3
    move-object v5, p1

    .line 224
    check-cast v5, Lorg/apache/lucene/queries/TermsFilter;

    .line 225
    .local v5, "test":Lorg/apache/lucene/queries/TermsFilter;
    iget v8, v5, Lorg/apache/lucene/queries/TermsFilter;->hashCode:I

    iget v9, p0, Lorg/apache/lucene/queries/TermsFilter;->hashCode:I

    if-ne v8, v9, :cond_7

    iget-object v8, p0, Lorg/apache/lucene/queries/TermsFilter;->termsAndFields:[Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    array-length v8, v8

    iget-object v9, v5, Lorg/apache/lucene/queries/TermsFilter;->termsAndFields:[Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    array-length v9, v9

    if-ne v8, v9, :cond_7

    .line 227
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v8, p0, Lorg/apache/lucene/queries/TermsFilter;->termsAndFields:[Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    array-length v8, v8

    if-lt v2, v8, :cond_4

    .line 234
    iget-object v8, p0, Lorg/apache/lucene/queries/TermsFilter;->offsets:[I

    iget-object v9, p0, Lorg/apache/lucene/queries/TermsFilter;->termsAndFields:[Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    array-length v9, v9

    aget v1, v8, v9

    .line 235
    .local v1, "end":I
    iget-object v3, p0, Lorg/apache/lucene/queries/TermsFilter;->termsBytes:[B

    .line 236
    .local v3, "left":[B
    iget-object v4, v5, Lorg/apache/lucene/queries/TermsFilter;->termsBytes:[B

    .line 237
    .local v4, "right":[B
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v1, :cond_0

    .line 238
    aget-byte v8, v3, v2

    aget-byte v9, v4, v2

    if-eq v8, v9, :cond_6

    move v6, v7

    .line 239
    goto :goto_0

    .line 228
    .end local v1    # "end":I
    .end local v3    # "left":[B
    .end local v4    # "right":[B
    :cond_4
    iget-object v8, p0, Lorg/apache/lucene/queries/TermsFilter;->termsAndFields:[Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    aget-object v0, v8, v2

    .line 229
    .local v0, "current":Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
    iget-object v8, v5, Lorg/apache/lucene/queries/TermsFilter;->termsAndFields:[Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    aget-object v8, v8, v2

    invoke-virtual {v0, v8}, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    move v6, v7

    .line 230
    goto :goto_0

    .line 227
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 237
    .end local v0    # "current":Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
    .restart local v1    # "end":I
    .restart local v3    # "left":[B
    .restart local v4    # "right":[B
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .end local v1    # "end":I
    .end local v2    # "i":I
    .end local v3    # "left":[B
    .end local v4    # "right":[B
    :cond_7
    move v6, v7

    .line 244
    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 16
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v4

    .line 181
    .local v4, "reader":Lorg/apache/lucene/index/AtomicReader;
    const/4 v5, 0x0

    .line 182
    .local v5, "result":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v2

    .line 183
    .local v2, "fields":Lorg/apache/lucene/index/Fields;
    new-instance v7, Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/queries/TermsFilter;->termsBytes:[B

    invoke-direct {v7, v11}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    .line 184
    .local v7, "spare":Lorg/apache/lucene/util/BytesRef;
    if-nez v2, :cond_0

    move-object v6, v5

    .line 212
    .end local v5    # "result":Lorg/apache/lucene/util/FixedBitSet;
    .local v6, "result":Lorg/apache/lucene/util/FixedBitSet;
    :goto_0
    return-object v6

    .line 187
    .end local v6    # "result":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "result":Lorg/apache/lucene/util/FixedBitSet;
    :cond_0
    const/4 v8, 0x0

    .line 188
    .local v8, "terms":Lorg/apache/lucene/index/Terms;
    const/4 v10, 0x0

    .line 189
    .local v10, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v1, 0x0

    .line 190
    .local v1, "docs":Lorg/apache/lucene/index/DocsEnum;
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/queries/TermsFilter;->termsAndFields:[Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    array-length v13, v12

    const/4 v11, 0x0

    :goto_1
    if-lt v11, v13, :cond_1

    move-object v6, v5

    .line 212
    .end local v5    # "result":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v6    # "result":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_0

    .line 190
    .end local v6    # "result":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "result":Lorg/apache/lucene/util/FixedBitSet;
    :cond_1
    aget-object v9, v12, v11

    .line 191
    .local v9, "termsAndField":Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
    iget-object v14, v9, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->field:Ljava/lang/String;

    invoke-virtual {v2, v14}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 192
    invoke-virtual {v8, v10}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v10

    .line 193
    iget v3, v9, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->start:I

    .local v3, "i":I
    :goto_2
    iget v14, v9, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->end:I

    if-lt v3, v14, :cond_3

    .line 190
    .end local v3    # "i":I
    :cond_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 194
    .restart local v3    # "i":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/queries/TermsFilter;->offsets:[I

    aget v14, v14, v3

    iput v14, v7, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 195
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/queries/TermsFilter;->offsets:[I

    add-int/lit8 v15, v3, 0x1

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/queries/TermsFilter;->offsets:[I

    aget v15, v15, v3

    sub-int/2addr v14, v15

    iput v14, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 196
    const/4 v14, 0x0

    invoke-virtual {v10, v7, v14}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 197
    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v10, v0, v1, v14}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v1

    .line 198
    if-nez v5, :cond_4

    .line 199
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v14

    const v15, 0x7fffffff

    if-eq v14, v15, :cond_4

    .line 200
    new-instance v5, Lorg/apache/lucene/util/FixedBitSet;

    .end local v5    # "result":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v14

    invoke-direct {v5, v14}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 202
    .restart local v5    # "result":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v14

    invoke-virtual {v5, v14}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 205
    :cond_4
    :goto_3
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v14

    const v15, 0x7fffffff

    if-ne v14, v15, :cond_6

    .line 193
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 206
    :cond_6
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v14

    invoke-virtual {v5, v14}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    goto :goto_3
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 249
    iget v0, p0, Lorg/apache/lucene/queries/TermsFilter;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 255
    .local v0, "builder":Ljava/lang/StringBuilder;
    new-instance v5, Lorg/apache/lucene/util/BytesRef;

    iget-object v6, p0, Lorg/apache/lucene/queries/TermsFilter;->termsBytes:[B

    invoke-direct {v5, v6}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    .line 256
    .local v5, "spare":Lorg/apache/lucene/util/BytesRef;
    const/4 v2, 0x1

    .line 257
    .local v2, "first":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/queries/TermsFilter;->termsAndFields:[Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    array-length v6, v6

    if-lt v3, v6, :cond_0

    .line 271
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 258
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/queries/TermsFilter;->termsAndFields:[Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    aget-object v1, v6, v3

    .line 259
    .local v1, "current":Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
    iget v4, v1, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->start:I

    .local v4, "j":I
    :goto_1
    iget v6, v1, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->end:I

    if-lt v4, v6, :cond_1

    .line 257
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 260
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/queries/TermsFilter;->offsets:[I

    aget v6, v6, v4

    iput v6, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 261
    iget-object v6, p0, Lorg/apache/lucene/queries/TermsFilter;->offsets:[I

    add-int/lit8 v7, v4, 0x1

    aget v6, v6, v7

    iget-object v7, p0, Lorg/apache/lucene/queries/TermsFilter;->offsets:[I

    aget v7, v7, v4

    sub-int/2addr v6, v7

    iput v6, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 262
    if-nez v2, :cond_2

    .line 263
    const/16 v6, 0x20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 265
    :cond_2
    const/4 v2, 0x0

    .line 266
    iget-object v6, v1, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->field:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x3a

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 267
    invoke-virtual {v5}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
