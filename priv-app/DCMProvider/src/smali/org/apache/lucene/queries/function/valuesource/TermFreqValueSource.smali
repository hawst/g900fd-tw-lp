.class public Lorg/apache/lucene/queries/function/valuesource/TermFreqValueSource;
.super Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;
.source "TermFreqValueSource.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "indexedField"    # Ljava/lang/String;
    .param p4, "indexedBytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 39
    return-void
.end method


# virtual methods
.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 3
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 49
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/TermFreqValueSource;->indexedField:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    .line 51
    .local v1, "terms":Lorg/apache/lucene/index/Terms;
    new-instance v2, Lorg/apache/lucene/queries/function/valuesource/TermFreqValueSource$1;

    invoke-direct {v2, p0, p0, v1}, Lorg/apache/lucene/queries/function/valuesource/TermFreqValueSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/TermFreqValueSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/index/Terms;)V

    return-object v2
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "termfreq"

    return-object v0
.end method
