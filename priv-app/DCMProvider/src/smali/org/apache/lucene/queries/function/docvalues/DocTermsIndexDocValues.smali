.class public abstract Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;
.super Lorg/apache/lucene/queries/function/FunctionValues;
.source "DocTermsIndexDocValues.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$DocTermsIndexException;
    }
.end annotation


# instance fields
.field protected final spare:Lorg/apache/lucene/util/BytesRef;

.field protected final spareChars:Lorg/apache/lucene/util/CharsRef;

.field protected final termsIndex:Lorg/apache/lucene/index/SortedDocValues;

.field protected final val:Lorg/apache/lucene/util/mutable/MutableValueStr;

.field protected final vs:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/index/AtomicReaderContext;Ljava/lang/String;)V
    .locals 3
    .param p1, "vs"    # Lorg/apache/lucene/queries/function/ValueSource;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues;-><init>()V

    .line 42
    new-instance v1, Lorg/apache/lucene/util/mutable/MutableValueStr;

    invoke-direct {v1}, Lorg/apache/lucene/util/mutable/MutableValueStr;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->val:Lorg/apache/lucene/util/mutable/MutableValueStr;

    .line 43
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->spare:Lorg/apache/lucene/util/BytesRef;

    .line 44
    new-instance v1, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->spareChars:Lorg/apache/lucene/util/CharsRef;

    .line 48
    :try_start_0
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    invoke-interface {v1, v2, p3}, Lorg/apache/lucene/search/FieldCache;->getTermsIndex(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    iput-object p1, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->vs:Lorg/apache/lucene/queries/function/ValueSource;

    .line 53
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Ljava/lang/RuntimeException;
    new-instance v1, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$DocTermsIndexException;

    invoke-direct {v1, p3, v0}, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$DocTermsIndexException;-><init>(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    throw v1
.end method


# virtual methods
.method public boolVal(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->exists(I)Z

    move-result v0

    return v0
.end method

.method public bytesVal(ILorg/apache/lucene/util/BytesRef;)Z
    .locals 1
    .param p1, "doc"    # I
    .param p2, "target"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/SortedDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 75
    iget v0, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public exists(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->ordVal(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRangeScorer(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/queries/function/ValueSourceScorer;
    .locals 8
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "lowerVal"    # Ljava/lang/String;
    .param p3, "upperVal"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z

    .prologue
    const/4 v0, 0x0

    .line 99
    if-nez p2, :cond_2

    move-object p2, v0

    .line 100
    :goto_0
    if-nez p3, :cond_3

    move-object p3, v0

    .line 102
    :goto_1
    const/high16 v6, -0x80000000

    .line 103
    .local v6, "lower":I
    if-eqz p2, :cond_0

    .line 104
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1, p2}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SortedDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)I

    move-result v6

    .line 105
    if-gez v6, :cond_4

    .line 106
    neg-int v0, v6

    add-int/lit8 v6, v0, -0x1

    .line 112
    :cond_0
    :goto_2
    const v7, 0x7fffffff

    .line 113
    .local v7, "upper":I
    if-eqz p3, :cond_1

    .line 114
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1, p3}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SortedDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)I

    move-result v7

    .line 115
    if-gez v7, :cond_5

    .line 116
    neg-int v0, v7

    add-int/lit8 v7, v0, -0x2

    .line 122
    :cond_1
    :goto_3
    move v4, v6

    .line 123
    .local v4, "ll":I
    move v5, v7

    .line 125
    .local v5, "uu":I
    new-instance v0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$1;-><init>(Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;II)V

    return-object v0

    .line 99
    .end local v4    # "ll":I
    .end local v5    # "uu":I
    .end local v6    # "lower":I
    .end local v7    # "upper":I
    :cond_2
    invoke-virtual {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->toTerm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 100
    :cond_3
    invoke-virtual {p0, p3}, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->toTerm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 107
    .restart local v6    # "lower":I
    :cond_4
    if-nez p4, :cond_0

    .line 108
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 117
    .restart local v7    # "upper":I
    :cond_5
    if-nez p5, :cond_1

    .line 118
    add-int/lit8 v7, v7, -0x1

    goto :goto_3
.end method

.method public getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$2;-><init>(Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;)V

    return-object v0
.end method

.method public numOrd()I
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v0

    return v0
.end method

.method public abstract objectVal(I)Ljava/lang/Object;
.end method

.method public ordVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    return v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->spare:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/index/SortedDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 81
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->spare:Lorg/apache/lucene/util/BytesRef;

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 85
    :goto_0
    return-object v0

    .line 84
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->spare:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->spareChars:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v0, v1}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/CharsRef;)V

    .line 85
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->spareChars:Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v0}, Lorg/apache/lucene/util/CharsRef;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->vs:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->strVal(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract toTerm(Ljava/lang/String;)Ljava/lang/String;
.end method
