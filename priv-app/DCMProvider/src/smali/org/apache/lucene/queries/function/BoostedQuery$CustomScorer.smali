.class Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "BoostedQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/function/BoostedQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomScorer"
.end annotation


# instance fields
.field private final qWeight:F

.field private final readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

.field private final scorer:Lorg/apache/lucene/search/Scorer;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

.field private final vals:Lorg/apache/lucene/queries/function/FunctionValues;

.field private final weight:Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/queries/function/BoostedQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;FLorg/apache/lucene/search/Scorer;Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 1
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "w"    # Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;
    .param p4, "qWeight"    # F
    .param p5, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .param p6, "vs"    # Lorg/apache/lucene/queries/function/ValueSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    iput-object p1, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    .line 136
    invoke-direct {p0, p3}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 137
    iput-object p3, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->weight:Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;

    .line 138
    iput p4, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->qWeight:F

    .line 139
    iput-object p5, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 140
    iput-object p2, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 141
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->weight:Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;

    iget-object v0, v0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->fcontext:Ljava/util/Map;

    invoke-virtual {p6, v0, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->vals:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 142
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/queries/function/BoostedQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;FLorg/apache/lucene/search/Scorer;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    invoke-direct/range {p0 .. p6}, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;-><init>(Lorg/apache/lucene/queries/function/BoostedQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;FLorg/apache/lucene/search/Scorer;Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    return v0
.end method

.method public explain(I)Lorg/apache/lucene/search/Explanation;
    .locals 6
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    iget-object v3, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->weight:Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;

    iget-object v3, v3, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->qWeight:Lorg/apache/lucene/search/Weight;

    iget-object v4, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v3, v4, p1}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v2

    .line 181
    .local v2, "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v2}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v3

    if-nez v3, :cond_0

    .line 189
    .end local v2    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :goto_0
    return-object v2

    .line 184
    .restart local v2    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :cond_0
    invoke-virtual {v2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v4

    mul-float v1, v3, v4

    .line 185
    .local v1, "sc":F
    new-instance v0, Lorg/apache/lucene/search/ComplexExplanation;

    .line 186
    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    invoke-virtual {v5}, Lorg/apache/lucene/queries/function/BoostedQuery;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ", product of:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 185
    invoke-direct {v0, v3, v1, v4}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    .line 187
    .local v0, "res":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 188
    iget-object v3, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->explain(I)Lorg/apache/lucene/search/Explanation;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    move-object v2, v0

    .line 189
    goto :goto_0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->freq()I

    move-result v0

    return v0
.end method

.method public getChildren()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    new-instance v0, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    const-string v2, "CUSTOM"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    return v0
.end method

.method public score()F
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    iget v1, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->qWeight:F

    iget-object v2, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->vals:Lorg/apache/lucene/queries/function/FunctionValues;

    iget-object v3, p0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v2

    mul-float v0, v1, v2

    .line 166
    .local v0, "score":F
    const/high16 v1, -0x800000    # Float.NEGATIVE_INFINITY

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .end local v0    # "score":F
    :goto_0
    return v0

    .restart local v0    # "score":F
    :cond_0
    const v0, -0x800001

    goto :goto_0
.end method
