.class public Lorg/apache/lucene/queries/function/FunctionQuery;
.super Lorg/apache/lucene/search/Query;
.source "FunctionQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;,
        Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;
    }
.end annotation


# instance fields
.field final func:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "func"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/queries/function/FunctionQuery;->func:Lorg/apache/lucene/queries/function/ValueSource;

    .line 48
    return-void
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    new-instance v0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;-><init>(Lorg/apache/lucene/queries/function/FunctionQuery;Lorg/apache/lucene/search/IndexSearcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 204
    const-class v2, Lorg/apache/lucene/queries/function/FunctionQuery;

    invoke-virtual {v2, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 206
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 205
    check-cast v0, Lorg/apache/lucene/queries/function/FunctionQuery;

    .line 206
    .local v0, "other":Lorg/apache/lucene/queries/function/FunctionQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/FunctionQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/FunctionQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 207
    iget-object v2, p0, Lorg/apache/lucene/queries/function/FunctionQuery;->func:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/FunctionQuery;->func:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 206
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    return-void
.end method

.method public getValueSource()Lorg/apache/lucene/queries/function/ValueSource;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery;->func:Lorg/apache/lucene/queries/function/ValueSource;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery;->func:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/FunctionQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    return-object p0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 195
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/FunctionQuery;->getBoost()F

    move-result v0

    .line 196
    .local v0, "boost":F
    new-instance v2, Ljava/lang/StringBuilder;

    float-to-double v4, v0

    cmpl-double v1, v4, v6

    if-eqz v1, :cond_0

    const-string v1, "("

    :goto_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/FunctionQuery;->func:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 197
    float-to-double v4, v0

    cmpl-double v1, v4, v6

    if-nez v1, :cond_1

    const-string v1, ""

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 196
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0

    .line 197
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, ")^"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
