.class public abstract Lorg/apache/lucene/queries/function/FunctionValues;
.super Ljava/lang/Object;
.source "FunctionValues.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public boolVal(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->intVal(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public byteVal(I)B
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 41
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public byteVal(I[B)V
    .locals 1
    .param p1, "doc"    # I
    .param p2, "vals"    # [B

    .prologue
    .line 124
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bytesVal(ILorg/apache/lucene/util/BytesRef;)Z
    .locals 2
    .param p1, "doc"    # I
    .param p2, "target"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->strVal(I)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "s":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 59
    iput v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 63
    :goto_0
    return v1

    .line 62
    :cond_0
    invoke-virtual {p2, v0}, Lorg/apache/lucene/util/BytesRef;->copyChars(Ljava/lang/CharSequence;)V

    .line 63
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public doubleVal(I)D
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 47
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public doubleVal(I[D)V
    .locals 1
    .param p1, "doc"    # I
    .param p2, "vals"    # [D

    .prologue
    .line 130
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public exists(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method public explain(I)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "doc"    # I

    .prologue
    .line 136
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v1

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    return-object v0
.end method

.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 44
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public floatVal(I[F)V
    .locals 1
    .param p1, "doc"    # I
    .param p2, "vals"    # [F

    .prologue
    .line 127
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getRangeScorer(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/queries/function/ValueSourceScorer;
    .locals 8
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "lowerVal"    # Ljava/lang/String;
    .param p3, "upperVal"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z

    .prologue
    .line 151
    if-nez p2, :cond_0

    .line 152
    const/high16 v6, -0x800000    # Float.NEGATIVE_INFINITY

    .line 156
    .local v6, "lower":F
    :goto_0
    if-nez p3, :cond_1

    .line 157
    const/high16 v7, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 162
    .local v7, "upper":F
    :goto_1
    move v4, v6

    .line 163
    .local v4, "l":F
    move v5, v7

    .line 165
    .local v5, "u":F
    if-eqz p4, :cond_2

    if-eqz p5, :cond_2

    .line 166
    new-instance v0, Lorg/apache/lucene/queries/function/FunctionValues$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/queries/function/FunctionValues$2;-><init>(Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;FF)V

    .line 193
    :goto_2
    return-object v0

    .line 154
    .end local v4    # "l":F
    .end local v5    # "u":F
    .end local v6    # "lower":F
    .end local v7    # "upper":F
    :cond_0
    invoke-static {p2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    .restart local v6    # "lower":F
    goto :goto_0

    .line 159
    :cond_1
    invoke-static {p3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    .restart local v7    # "upper":F
    goto :goto_1

    .line 174
    .restart local v4    # "l":F
    .restart local v5    # "u":F
    :cond_2
    if-eqz p4, :cond_3

    if-nez p5, :cond_3

    .line 175
    new-instance v0, Lorg/apache/lucene/queries/function/FunctionValues$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/queries/function/FunctionValues$3;-><init>(Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;FF)V

    goto :goto_2

    .line 183
    :cond_3
    if-nez p4, :cond_4

    if-eqz p5, :cond_4

    .line 184
    new-instance v0, Lorg/apache/lucene/queries/function/FunctionValues$4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/queries/function/FunctionValues$4;-><init>(Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;FF)V

    goto :goto_2

    .line 193
    :cond_4
    new-instance v0, Lorg/apache/lucene/queries/function/FunctionValues$5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/queries/function/FunctionValues$5;-><init>(Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;FF)V

    goto :goto_2
.end method

.method public getScorer(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/queries/function/ValueSourceScorer;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 140
    new-instance v0, Lorg/apache/lucene/queries/function/ValueSourceScorer;

    invoke-direct {v0, p1, p0}, Lorg/apache/lucene/queries/function/ValueSourceScorer;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v0
.end method

.method public getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lorg/apache/lucene/queries/function/FunctionValues$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/queries/function/FunctionValues$1;-><init>(Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v0
.end method

.method public intVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 45
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public intVal(I[I)V
    .locals 1
    .param p1, "doc"    # I
    .param p2, "vals"    # [I

    .prologue
    .line 128
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public longVal(I)J
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 46
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public longVal(I[J)V
    .locals 1
    .param p1, "doc"    # I
    .param p2, "vals"    # [J

    .prologue
    .line 129
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public numOrd()I
    .locals 1

    .prologue
    .line 87
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public ordVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 82
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public shortVal(I)S
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 42
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public shortVal(I[S)V
    .locals 1
    .param p1, "doc"    # I
    .param p2, "vals"    # [S

    .prologue
    .line 125
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 49
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public strVal(I[Ljava/lang/String;)V
    .locals 1
    .param p1, "doc"    # I
    .param p2, "vals"    # [Ljava/lang/String;

    .prologue
    .line 133
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract toString(I)Ljava/lang/String;
.end method
