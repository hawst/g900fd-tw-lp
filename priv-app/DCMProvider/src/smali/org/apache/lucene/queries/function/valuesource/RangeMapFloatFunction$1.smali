.class Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;
.super Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;
.source "RangeMapFloatFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

.field private final synthetic val$vals:Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 60
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public floatVal(I)F
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 63
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v0

    .line 64
    .local v0, "val":F
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->min:F

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->max:F

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

    iget v0, v1, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->target:F

    .end local v0    # "val":F
    :cond_0
    :goto_0
    return v0

    .restart local v0    # "val":F
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

    iget-object v1, v1, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->defaultVal:Ljava/lang/Float;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

    iget-object v1, v1, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->defaultVal:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "map("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",min="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->min:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",max="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->max:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",target="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->target:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
