.class Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;
.super Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.source "DoubleFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mval:Lorg/apache/lucene/util/mutable/MutableValueDouble;

.field final synthetic this$1:Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;

.field private final synthetic val$arr:Lorg/apache/lucene/search/FieldCache$Doubles;

.field private final synthetic val$valid:Lorg/apache/lucene/util/Bits;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;Lorg/apache/lucene/search/FieldCache$Doubles;Lorg/apache/lucene/util/Bits;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;->this$1:Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;

    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;->val$arr:Lorg/apache/lucene/search/FieldCache$Doubles;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;->val$valid:Lorg/apache/lucene/util/Bits;

    .line 134
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;-><init>()V

    .line 135
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueDouble;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;->mval:Lorg/apache/lucene/util/mutable/MutableValueDouble;

    return-void
.end method


# virtual methods
.method public fillValue(I)V
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;->mval:Lorg/apache/lucene/util/mutable/MutableValueDouble;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;->val$arr:Lorg/apache/lucene/search/FieldCache$Doubles;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldCache$Doubles;->get(I)D

    move-result-wide v2

    iput-wide v2, v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->value:D

    .line 145
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;->mval:Lorg/apache/lucene/util/mutable/MutableValueDouble;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueDouble;->exists:Z

    .line 146
    return-void
.end method

.method public getValue()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;->mval:Lorg/apache/lucene/util/mutable/MutableValueDouble;

    return-object v0
.end method
