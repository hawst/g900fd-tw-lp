.class public Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;
.super Lorg/apache/lucene/queries/function/valuesource/MultiValueSource;
.source "VectorValueSource.java"


# instance fields
.field protected final sources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/function/ValueSource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/function/ValueSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "sources":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queries/function/ValueSource;>;"
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/valuesource/MultiValueSource;-><init>()V

    .line 39
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    .line 40
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 3
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 186
    return-void

    .line 184
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queries/function/ValueSource;

    .line 185
    .local v0, "source":Lorg/apache/lucene/queries/function/ValueSource;
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    goto :goto_0
.end method

.method public description()Ljava/lang/String;
    .locals 5

    .prologue
    .line 191
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 193
    const/4 v0, 0x1

    .line 194
    .local v0, "firstTime":Z
    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 202
    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 194
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queries/function/ValueSource;

    .line 195
    .local v2, "source":Lorg/apache/lucene/queries/function/ValueSource;
    if-eqz v0, :cond_1

    .line 196
    const/4 v0, 0x0

    .line 200
    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 198
    :cond_1
    const/16 v4, 0x2c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public dimension()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 208
    if-ne p0, p1, :cond_0

    const/4 v1, 0x1

    .line 213
    :goto_0
    return v1

    .line 209
    :cond_0
    instance-of v1, p1, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 211
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;

    .line 213
    .local v0, "that":Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getSources()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/function/ValueSource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    return-object v0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 7
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v5, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    .line 60
    .local v1, "size":I
    const/4 v5, 0x2

    if-ne v1, v5, :cond_0

    .line 61
    iget-object v5, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v5, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v3

    .line 62
    .local v3, "x":Lorg/apache/lucene/queries/function/FunctionValues;
    iget-object v5, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v5, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v4

    .line 63
    .local v4, "y":Lorg/apache/lucene/queries/function/FunctionValues;
    new-instance v5, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;

    invoke-direct {v5, p0, v3, v4}, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/queries/function/FunctionValues;)V

    .line 113
    .end local v3    # "x":Lorg/apache/lucene/queries/function/FunctionValues;
    .end local v4    # "y":Lorg/apache/lucene/queries/function/FunctionValues;
    :goto_0
    return-object v5

    .line 108
    :cond_0
    new-array v2, v1, [Lorg/apache/lucene/queries/function/FunctionValues;

    .line 109
    .local v2, "valsArr":[Lorg/apache/lucene/queries/function/FunctionValues;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 113
    new-instance v5, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;

    invoke-direct {v5, p0, v2}, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;-><init>(Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;[Lorg/apache/lucene/queries/function/FunctionValues;)V

    goto :goto_0

    .line 110
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v5, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v5

    aput-object v5, v2, v0

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->sources:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "vector"

    return-object v0
.end method
