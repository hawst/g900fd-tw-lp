.class Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;
.super Lorg/apache/lucene/queries/function/FunctionValues;
.source "VectorValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;

.field private final synthetic val$x:Lorg/apache/lucene/queries/function/FunctionValues;

.field private final synthetic val$y:Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;

    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$x:Lorg/apache/lucene/queries/function/FunctionValues;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$y:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 63
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues;-><init>()V

    return-void
.end method


# virtual methods
.method public byteVal(I[B)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "vals"    # [B

    .prologue
    .line 66
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$x:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->byteVal(I)B

    move-result v1

    aput-byte v1, p2, v0

    .line 67
    const/4 v0, 0x1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$y:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->byteVal(I)B

    move-result v1

    aput-byte v1, p2, v0

    .line 68
    return-void
.end method

.method public doubleVal(I[D)V
    .locals 4
    .param p1, "doc"    # I
    .param p2, "vals"    # [D

    .prologue
    .line 92
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$x:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->doubleVal(I)D

    move-result-wide v2

    aput-wide v2, p2, v0

    .line 93
    const/4 v0, 0x1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$y:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->doubleVal(I)D

    move-result-wide v2

    aput-wide v2, p2, v0

    .line 94
    return-void
.end method

.method public floatVal(I[F)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "vals"    # [F

    .prologue
    .line 87
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$x:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v1

    aput v1, p2, v0

    .line 88
    const/4 v0, 0x1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$y:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v1

    aput v1, p2, v0

    .line 89
    return-void
.end method

.method public intVal(I[I)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "vals"    # [I

    .prologue
    .line 77
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$x:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->intVal(I)I

    move-result v1

    aput v1, p2, v0

    .line 78
    const/4 v0, 0x1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$y:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->intVal(I)I

    move-result v1

    aput v1, p2, v0

    .line 79
    return-void
.end method

.method public longVal(I[J)V
    .locals 4
    .param p1, "doc"    # I
    .param p2, "vals"    # [J

    .prologue
    .line 82
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$x:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->longVal(I)J

    move-result-wide v2

    aput-wide v2, p2, v0

    .line 83
    const/4 v0, 0x1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$y:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->longVal(I)J

    move-result-wide v2

    aput-wide v2, p2, v0

    .line 84
    return-void
.end method

.method public shortVal(I[S)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "vals"    # [S

    .prologue
    .line 72
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$x:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->shortVal(I)S

    move-result v1

    aput-short v1, p2, v0

    .line 73
    const/4 v0, 0x1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$y:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->shortVal(I)S

    move-result v1

    aput-short v1, p2, v0

    .line 74
    return-void
.end method

.method public strVal(I[Ljava/lang/String;)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "vals"    # [Ljava/lang/String;

    .prologue
    .line 97
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$x:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->strVal(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    .line 98
    const/4 v0, 0x1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$y:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->strVal(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    .line 99
    return-void
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$x:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$1;->val$y:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
