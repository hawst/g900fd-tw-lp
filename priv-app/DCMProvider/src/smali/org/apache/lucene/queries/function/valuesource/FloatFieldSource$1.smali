.class Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;
.super Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;
.source "FloatFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource;

.field private final synthetic val$arr:Lorg/apache/lucene/search/FieldCache$Floats;

.field private final synthetic val$valid:Lorg/apache/lucene/util/Bits;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/search/FieldCache$Floats;Lorg/apache/lucene/util/Bits;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Floats;

    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    .line 62
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public exists(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    return v0
.end method

.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Floats;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Floats;->get(I)F

    move-result v0

    return v0
.end method

.method public getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    .locals 3

    .prologue
    .line 80
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Floats;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;Lorg/apache/lucene/search/FieldCache$Floats;Lorg/apache/lucene/util/Bits;)V

    return-object v0
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Floats;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Floats;->get(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
