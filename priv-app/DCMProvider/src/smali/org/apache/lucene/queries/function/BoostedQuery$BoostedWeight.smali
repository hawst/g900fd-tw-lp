.class Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;
.super Lorg/apache/lucene/search/Weight;
.source "BoostedQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/function/BoostedQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BoostedWeight"
.end annotation


# instance fields
.field fcontext:Ljava/util/Map;

.field qWeight:Lorg/apache/lucene/search/Weight;

.field final searcher:Lorg/apache/lucene/search/IndexSearcher;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/BoostedQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/BoostedQuery;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 2
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    iput-object p1, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 75
    iput-object p2, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 76
    # getter for: Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;
    invoke-static {p1}, Lorg/apache/lucene/queries/function/BoostedQuery;->access$0(Lorg/apache/lucene/queries/function/BoostedQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->qWeight:Lorg/apache/lucene/search/Weight;

    .line 77
    invoke-static {p2}, Lorg/apache/lucene/queries/function/ValueSource;->newContext(Lorg/apache/lucene/search/IndexSearcher;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->fcontext:Ljava/util/Map;

    .line 78
    # getter for: Lorg/apache/lucene/queries/function/BoostedQuery;->boostVal:Lorg/apache/lucene/queries/function/ValueSource;
    invoke-static {p1}, Lorg/apache/lucene/queries/function/BoostedQuery;->access$1(Lorg/apache/lucene/queries/function/BoostedQuery;)Lorg/apache/lucene/queries/function/ValueSource;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->fcontext:Ljava/util/Map;

    invoke-virtual {v0, v1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 79
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 7
    .param p1, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iget-object v4, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->qWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v4, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v2

    .line 113
    .local v2, "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v2}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v4

    if-nez v4, :cond_0

    .line 122
    .end local v2    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :goto_0
    return-object v2

    .line 116
    .restart local v2    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    # getter for: Lorg/apache/lucene/queries/function/BoostedQuery;->boostVal:Lorg/apache/lucene/queries/function/ValueSource;
    invoke-static {v4}, Lorg/apache/lucene/queries/function/BoostedQuery;->access$1(Lorg/apache/lucene/queries/function/BoostedQuery;)Lorg/apache/lucene/queries/function/ValueSource;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->fcontext:Ljava/util/Map;

    invoke-virtual {v4, v5, p1}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v3

    .line 117
    .local v3, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    invoke-virtual {v2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v4

    invoke-virtual {v3, p2}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v5

    mul-float v1, v4, v5

    .line 118
    .local v1, "sc":F
    new-instance v0, Lorg/apache/lucene/search/ComplexExplanation;

    .line 119
    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    invoke-virtual {v6}, Lorg/apache/lucene/queries/function/BoostedQuery;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ", product of:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 118
    invoke-direct {v0, v4, v1, v5}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    .line 120
    .local v0, "res":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 121
    invoke-virtual {v3, p2}, Lorg/apache/lucene/queries/function/FunctionValues;->explain(I)Lorg/apache/lucene/search/Explanation;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    move-object v2, v0

    .line 122
    goto :goto_0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v1, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->qWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Weight;->getValueForNormalization()F

    move-result v0

    .line 89
    .local v0, "sum":F
    iget-object v1, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/BoostedQuery;->getBoost()F

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    invoke-virtual {v2}, Lorg/apache/lucene/queries/function/BoostedQuery;->getBoost()F

    move-result v2

    mul-float/2addr v1, v2

    mul-float/2addr v0, v1

    .line 90
    return v0
.end method

.method public normalize(FF)V
    .locals 1
    .param p1, "norm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/BoostedQuery;->getBoost()F

    move-result v0

    mul-float/2addr p2, v0

    .line 96
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->qWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Weight;->normalize(FF)V

    .line 97
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 8
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 103
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->qWeight:Lorg/apache/lucene/search/Weight;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2, p4}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v5

    .line 104
    .local v5, "subQueryScorer":Lorg/apache/lucene/search/Scorer;
    if-nez v5, :cond_0

    .line 107
    :goto_0
    return-object v7

    :cond_0
    new-instance v0, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    invoke-virtual {v2}, Lorg/apache/lucene/queries/function/BoostedQuery;->getBoost()F

    move-result v4

    iget-object v2, p0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;->this$0:Lorg/apache/lucene/queries/function/BoostedQuery;

    # getter for: Lorg/apache/lucene/queries/function/BoostedQuery;->boostVal:Lorg/apache/lucene/queries/function/ValueSource;
    invoke-static {v2}, Lorg/apache/lucene/queries/function/BoostedQuery;->access$1(Lorg/apache/lucene/queries/function/BoostedQuery;)Lorg/apache/lucene/queries/function/ValueSource;

    move-result-object v6

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;-><init>(Lorg/apache/lucene/queries/function/BoostedQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;FLorg/apache/lucene/search/Scorer;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;)V

    move-object v7, v0

    goto :goto_0
.end method
