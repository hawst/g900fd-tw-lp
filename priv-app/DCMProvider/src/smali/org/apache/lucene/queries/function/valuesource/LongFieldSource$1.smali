.class Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;
.super Lorg/apache/lucene/queries/function/docvalues/LongDocValues;
.source "LongFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;

.field private final synthetic val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

.field private final synthetic val$valid:Lorg/apache/lucene/util/Bits;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/search/FieldCache$Longs;Lorg/apache/lucene/util/Bits;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    .line 76
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;)Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;

    return-object v0
.end method


# virtual methods
.method public exists(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    return v0
.end method

.method public getRangeScorer(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/queries/function/ValueSourceScorer;
    .locals 16
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "lowerVal"    # Ljava/lang/String;
    .param p3, "upperVal"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z

    .prologue
    .line 103
    if-nez p2, :cond_2

    .line 104
    const-wide/high16 v12, -0x8000000000000000L

    .line 110
    .local v12, "lower":J
    :cond_0
    :goto_0
    if-nez p3, :cond_3

    .line 111
    const-wide v14, 0x7fffffffffffffffL

    .line 117
    .local v14, "upper":J
    :cond_1
    :goto_1
    move-wide v8, v12

    .line 118
    .local v8, "ll":J
    move-wide v10, v14

    .line 120
    .local v10, "uu":J
    new-instance v3, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$1;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p0

    invoke-direct/range {v3 .. v11}, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/search/FieldCache$Longs;JJ)V

    return-object v3

    .line 106
    .end local v8    # "ll":J
    .end local v10    # "uu":J
    .end local v12    # "lower":J
    .end local v14    # "upper":J
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->externalToLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 107
    .restart local v12    # "lower":J
    if-nez p4, :cond_0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, v12, v2

    if-gez v2, :cond_0

    const-wide/16 v2, 0x1

    add-long/2addr v12, v2

    goto :goto_0

    .line 113
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->externalToLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 114
    .restart local v14    # "upper":J
    if-nez p5, :cond_1

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v14, v2

    if-lez v2, :cond_1

    const-wide/16 v2, 0x1

    sub-long/2addr v14, v2

    goto :goto_1
.end method

.method public getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    .locals 3

    .prologue
    .line 133
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;-><init>(Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;Lorg/apache/lucene/search/FieldCache$Longs;Lorg/apache/lucene/util/Bits;)V

    return-object v0
.end method

.method public longVal(I)J
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Longs;->get(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldCache$Longs;->get(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->longToObject(J)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldCache$Longs;->get(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->longToString(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
