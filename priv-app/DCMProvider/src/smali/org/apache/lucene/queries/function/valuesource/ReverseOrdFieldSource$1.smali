.class Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource$1;
.super Lorg/apache/lucene/queries/function/docvalues/IntDocValues;
.source "ReverseOrdFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;

.field private final synthetic val$end:I

.field private final synthetic val$off:I

.field private final synthetic val$sindex:Lorg/apache/lucene/index/SortedDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;Lorg/apache/lucene/queries/function/ValueSource;ILorg/apache/lucene/index/SortedDocValues;I)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;

    iput p3, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource$1;->val$end:I

    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource$1;->val$sindex:Lorg/apache/lucene/index/SortedDocValues;

    iput p5, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource$1;->val$off:I

    .line 80
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/IntDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public intVal(I)I
    .locals 3
    .param p1, "doc"    # I

    .prologue
    .line 83
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource$1;->val$end:I

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource$1;->val$sindex:Lorg/apache/lucene/index/SortedDocValues;

    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource$1;->val$off:I

    add-int/2addr v2, p1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method
