.class Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;
.super Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.source "IntFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

.field final synthetic this$1:Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;

.field private final synthetic val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

.field private final synthetic val$valid:Lorg/apache/lucene/util/Bits;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;Lorg/apache/lucene/search/FieldCache$Ints;Lorg/apache/lucene/util/Bits;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;->this$1:Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;

    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;->val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;->val$valid:Lorg/apache/lucene/util/Bits;

    .line 142
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;-><init>()V

    .line 143
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueInt;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueInt;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    return-void
.end method


# virtual methods
.method public fillValue(I)V
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;->val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldCache$Ints;->get(I)I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    .line 153
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    .line 154
    return-void
.end method

.method public getValue()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    return-object v0
.end method
