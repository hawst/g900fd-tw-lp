.class Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;
.super Lorg/apache/lucene/queries/function/FunctionValues;
.source "IfFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/IfFunction;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/IfFunction;

.field private final synthetic val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

.field private final synthetic val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

.field private final synthetic val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/IfFunction;Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/IfFunction;

    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 55
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues;-><init>()V

    return-void
.end method


# virtual methods
.method public boolVal(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    goto :goto_0
.end method

.method public byteVal(I)B
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->byteVal(I)B

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->byteVal(I)B

    move-result v0

    goto :goto_0
.end method

.method public bytesVal(ILorg/apache/lucene/util/BytesRef;)Z
    .locals 1
    .param p1, "doc"    # I
    .param p2, "target"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/FunctionValues;->bytesVal(ILorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/FunctionValues;->bytesVal(ILorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    goto :goto_0
.end method

.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->doubleVal(I)D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->doubleVal(I)D

    move-result-wide v0

    goto :goto_0
.end method

.method public exists(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 108
    const/4 v0, 0x1

    return v0
.end method

.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v0

    goto :goto_0
.end method

.method public getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    .locals 1

    .prologue
    .line 115
    invoke-super {p0}, Lorg/apache/lucene/queries/function/FunctionValues;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;

    move-result-object v0

    return-object v0
.end method

.method public intVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->intVal(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->intVal(I)I

    move-result v0

    goto :goto_0
.end method

.method public longVal(I)J
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->longVal(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->longVal(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->objectVal(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->objectVal(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public shortVal(I)S
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->shortVal(I)S

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->shortVal(I)S

    move-result v0

    goto :goto_0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->strVal(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->strVal(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 3
    .param p1, "doc"    # I

    .prologue
    const/16 v2, 0x2c

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "if("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$ifVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$trueVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;->val$falseVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
