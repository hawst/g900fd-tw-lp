.class Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;
.super Lorg/apache/lucene/queries/function/docvalues/IntDocValues;
.source "JoinDocFreqValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final ref:Lorg/apache/lucene/util/BytesRef;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;

.field private final synthetic val$terms:Lorg/apache/lucene/index/BinaryDocValues;

.field private final synthetic val$termsEnum:Lorg/apache/lucene/index/TermsEnum;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/index/BinaryDocValues;Lorg/apache/lucene/index/TermsEnum;)V
    .locals 1
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;->val$terms:Lorg/apache/lucene/index/BinaryDocValues;

    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;->val$termsEnum:Lorg/apache/lucene/index/TermsEnum;

    .line 64
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/IntDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 65
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;->ref:Lorg/apache/lucene/util/BytesRef;

    return-void
.end method


# virtual methods
.method public intVal(I)I
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 71
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;->val$terms:Lorg/apache/lucene/index/BinaryDocValues;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;->ref:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, p1, v2}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 72
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;->val$termsEnum:Lorg/apache/lucene/index/TermsEnum;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;->ref:Lorg/apache/lucene/util/BytesRef;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;->val$termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 75
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "caught exception in function "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;

    invoke-virtual {v3}, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;->description()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : doc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
