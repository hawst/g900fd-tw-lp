.class Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1$1;
.super Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.source "OrdFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

.field final synthetic this$1:Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;

.field private final synthetic val$sindex:Lorg/apache/lucene/index/SortedDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;Lorg/apache/lucene/index/SortedDocValues;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1$1;->this$1:Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1;

    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1$1;->val$sindex:Lorg/apache/lucene/index/SortedDocValues;

    .line 101
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;-><init>()V

    .line 102
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueInt;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueInt;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    return-void
.end method


# virtual methods
.method public fillValue(I)V
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1$1;->val$sindex:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    .line 112
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    iget v0, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    .line 113
    return-void

    .line 112
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getValue()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/OrdFieldSource$1$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    return-object v0
.end method
