.class public abstract Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "MultiFloatFunction.java"


# instance fields
.field protected final sources:[Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>([Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "sources"    # [Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->sources:[Lorg/apache/lucene/queries/function/ValueSource;

    .line 39
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 4
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->sources:[Lorg/apache/lucene/queries/function/ValueSource;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 96
    return-void

    .line 94
    :cond_0
    aget-object v0, v2, v1

    .line 95
    .local v0, "source":Lorg/apache/lucene/queries/function/ValueSource;
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public description()Ljava/lang/String;
    .locals 7

    .prologue
    .line 46
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 48
    const/4 v0, 0x1

    .line 49
    .local v0, "firstTime":Z
    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->sources:[Lorg/apache/lucene/queries/function/ValueSource;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 57
    const/16 v3, 0x29

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 58
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 49
    :cond_0
    aget-object v2, v4, v3

    .line 50
    .local v2, "source":Lorg/apache/lucene/queries/function/ValueSource;
    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x0

    .line 55
    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 49
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 53
    :cond_1
    const/16 v6, 0x2c

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 106
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;

    .line 107
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->sources:[Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->sources:[Lorg/apache/lucene/queries/function/ValueSource;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected abstract func(I[Lorg/apache/lucene/queries/function/FunctionValues;)F
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 3
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->sources:[Lorg/apache/lucene/queries/function/ValueSource;

    array-length v2, v2

    new-array v1, v2, [Lorg/apache/lucene/queries/function/FunctionValues;

    .line 64
    .local v1, "valsArr":[Lorg/apache/lucene/queries/function/FunctionValues;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->sources:[Lorg/apache/lucene/queries/function/ValueSource;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 68
    new-instance v2, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction$1;

    invoke-direct {v2, p0, p0, v1}, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;[Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v2

    .line 65
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->sources:[Lorg/apache/lucene/queries/function/ValueSource;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v2

    aput-object v2, v1, v0

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->sources:[Lorg/apache/lucene/queries/function/ValueSource;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected abstract name()Ljava/lang/String;
.end method
