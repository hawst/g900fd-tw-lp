.class public Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "ReverseOrdFieldSource.java"


# static fields
.field private static final hcode:I


# instance fields
.field public final field:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const-class v0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    sput v0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;->hcode:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 60
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;->field:Ljava/lang/String;

    .line 61
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "rord("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 90
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;

    if-eq v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 92
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 91
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;

    .line 92
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;->field:Ljava/lang/String;

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 8
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-static {p2}, Lorg/apache/lucene/index/ReaderUtil;->getTopLevelContext(Lorg/apache/lucene/index/IndexReaderContext;)Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReaderContext;->reader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v7

    .line 72
    .local v7, "topReader":Lorg/apache/lucene/index/IndexReader;
    instance-of v0, v7, Lorg/apache/lucene/index/CompositeReader;

    if-eqz v0, :cond_0

    .line 73
    new-instance v6, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;

    check-cast v7, Lorg/apache/lucene/index/CompositeReader;

    .end local v7    # "topReader":Lorg/apache/lucene/index/IndexReader;
    invoke-direct {v6, v7}, Lorg/apache/lucene/index/SlowCompositeReaderWrapper;-><init>(Lorg/apache/lucene/index/CompositeReader;)V

    .line 75
    .local v6, "r":Lorg/apache/lucene/index/AtomicReader;
    :goto_0
    iget v5, p2, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    .line 77
    .local v5, "off":I
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;->field:Ljava/lang/String;

    invoke-interface {v0, v6, v1}, Lorg/apache/lucene/search/FieldCache;->getTermsIndex(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v4

    .line 78
    .local v4, "sindex":Lorg/apache/lucene/index/SortedDocValues;
    invoke-virtual {v4}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v3

    .line 80
    .local v3, "end":I
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource$1;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;Lorg/apache/lucene/queries/function/ValueSource;ILorg/apache/lucene/index/SortedDocValues;I)V

    return-object v0

    .line 74
    .end local v3    # "end":I
    .end local v4    # "sindex":Lorg/apache/lucene/index/SortedDocValues;
    .end local v5    # "off":I
    .end local v6    # "r":Lorg/apache/lucene/index/AtomicReader;
    .restart local v7    # "topReader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    check-cast v7, Lorg/apache/lucene/index/AtomicReader;

    move-object v6, v7

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 98
    sget v0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;->hcode:I

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReverseOrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
