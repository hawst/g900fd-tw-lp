.class public Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;
.super Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;
.source "LongFieldSource.java"


# instance fields
.field protected final parser:Lorg/apache/lucene/search/FieldCache$LongParser;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/apache/lucene/search/FieldCache$LongParser;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;-><init>(Ljava/lang/String;)V

    .line 51
    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    .line 52
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "long("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 158
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 160
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 159
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;

    .line 160
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;
    invoke-super {p0, v0}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 161
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    if-nez v2, :cond_2

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    if-nez v2, :cond_0

    .line 160
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 162
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    goto :goto_1
.end method

.method public externalToLong(Ljava/lang/String;)J
    .locals 2
    .param p1, "extVal"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 7
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->cache:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->field:Ljava/lang/String;

    iget-object v5, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    const/4 v6, 0x1

    invoke-interface {v2, v3, v4, v5, v6}, Lorg/apache/lucene/search/FieldCache;->getLongs(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)Lorg/apache/lucene/search/FieldCache$Longs;

    move-result-object v0

    .line 74
    .local v0, "arr":Lorg/apache/lucene/search/FieldCache$Longs;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->cache:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->field:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lorg/apache/lucene/search/FieldCache;->getDocsWithField(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/util/Bits;

    move-result-object v1

    .line 76
    .local v1, "valid":Lorg/apache/lucene/util/Bits;
    new-instance v2, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;

    invoke-direct {v2, p0, p0, v0, v1}, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/search/FieldCache$Longs;Lorg/apache/lucene/util/Bits;)V

    return-object v2
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 167
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 168
    .local v0, "h":I
    :goto_0
    invoke-super {p0}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    return v0

    .line 167
    .end local v0    # "h":I
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$LongParser;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public longToObject(J)Ljava/lang/Object;
    .locals 1
    .param p1, "val"    # J

    .prologue
    .line 64
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public longToString(J)Ljava/lang/String;
    .locals 1
    .param p1, "val"    # J

    .prologue
    .line 68
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->longToObject(J)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected newMutableValueLong()Lorg/apache/lucene/util/mutable/MutableValueLong;
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueLong;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueLong;-><init>()V

    return-object v0
.end method
