.class Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;
.super Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;
.source "ScaleFloatFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;

.field private final synthetic val$maxSource:F

.field private final synthetic val$minSource:F

.field private final synthetic val$scale:F

.field private final synthetic val$vals:Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;FFF)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    iput p4, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->val$minSource:F

    iput p5, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->val$scale:F

    iput p6, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->val$maxSource:F

    .line 115
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public floatVal(I)F
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->val$minSource:F

    sub-float/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->val$scale:F

    mul-float/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->min:F

    add-float/2addr v0, v1

    return v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "scale("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",toMin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->min:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",toMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction;->max:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 123
    const-string v1, ",fromMin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->val$minSource:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 124
    const-string v1, ",fromMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ScaleFloatFunction$1;->val$maxSource:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 125
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
