.class public Lorg/apache/lucene/queries/CustomScoreQuery;
.super Lorg/apache/lucene/search/Query;
.source "CustomScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;,
        Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;
    }
.end annotation


# instance fields
.field private scoringQueries:[Lorg/apache/lucene/search/Query;

.field private strict:Z

.field private subQuery:Lorg/apache/lucene/search/Query;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Query;)V
    .locals 1
    .param p1, "subQuery"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 61
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/search/Query;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queries/CustomScoreQuery;-><init>(Lorg/apache/lucene/search/Query;[Lorg/apache/lucene/search/Query;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;)V
    .locals 2
    .param p1, "subQuery"    # Lorg/apache/lucene/search/Query;
    .param p2, "scoringQuery"    # Lorg/apache/lucene/search/Query;

    .prologue
    const/4 v1, 0x0

    .line 71
    if-eqz p2, :cond_0

    .line 72
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/search/Query;

    aput-object p2, v0, v1

    :goto_0
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queries/CustomScoreQuery;-><init>(Lorg/apache/lucene/search/Query;[Lorg/apache/lucene/search/Query;)V

    .line 73
    return-void

    .line 72
    :cond_0
    new-array v0, v1, [Lorg/apache/lucene/search/Query;

    goto :goto_0
.end method

.method public varargs constructor <init>(Lorg/apache/lucene/search/Query;[Lorg/apache/lucene/search/Query;)V
    .locals 2
    .param p1, "subQuery"    # Lorg/apache/lucene/search/Query;
    .param p2, "scoringQueries"    # [Lorg/apache/lucene/search/Query;

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 54
    iput-boolean v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->strict:Z

    .line 82
    iput-object p1, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    .line 83
    if-eqz p2, :cond_0

    .end local p2    # "scoringQueries":[Lorg/apache/lucene/search/Query;
    :goto_0
    iput-object p2, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    .line 85
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "<subquery> must not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    .restart local p2    # "scoringQueries":[Lorg/apache/lucene/search/Query;
    :cond_0
    new-array p2, v0, [Lorg/apache/lucene/search/Query;

    goto :goto_0

    .line 86
    .end local p2    # "scoringQueries":[Lorg/apache/lucene/search/Query;
    :cond_1
    return-void
.end method

.method static synthetic access$1(Lorg/apache/lucene/queries/CustomScoreQuery;)Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/queries/CustomScoreQuery;)[Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/queries/CustomScoreQuery;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->strict:Z

    return v0
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/queries/CustomScoreQuery;
    .locals 4

    .prologue
    .line 122
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queries/CustomScoreQuery;

    .line 123
    .local v0, "clone":Lorg/apache/lucene/queries/CustomScoreQuery;
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Query;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v2

    iput-object v2, v0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    .line 124
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    array-length v2, v2

    new-array v2, v2, [Lorg/apache/lucene/search/Query;

    iput-object v2, v0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    .line 125
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 128
    return-object v0

    .line 126
    :cond_0
    iget-object v2, v0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    iget-object v3, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/apache/lucene/search/Query;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v3

    aput-object v3, v2, v1

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CustomScoreQuery;->clone()Lorg/apache/lucene/queries/CustomScoreQuery;

    move-result-object v0

    return-object v0
.end method

.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 360
    new-instance v0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;-><init>(Lorg/apache/lucene/queries/CustomScoreQuery;Lorg/apache/lucene/search/IndexSearcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 147
    if-ne p0, p1, :cond_1

    .line 148
    const/4 v1, 0x1

    .line 161
    :cond_0
    :goto_0
    return v1

    .line 149
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 154
    check-cast v0, Lorg/apache/lucene/queries/CustomScoreQuery;

    .line 155
    .local v0, "other":Lorg/apache/lucene/queries/CustomScoreQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CustomScoreQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/queries/CustomScoreQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 156
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    iget-object v3, v0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 157
    iget-boolean v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->strict:Z

    iget-boolean v3, v0, Lorg/apache/lucene/queries/CustomScoreQuery;->strict:Z

    if-ne v2, v3, :cond_0

    .line 158
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    array-length v2, v2

    iget-object v3, v0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 161
    iget-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    iget-object v2, v0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Query;->extractTerms(Ljava/util/Set;)V

    .line 114
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 117
    return-void

    .line 114
    :cond_0
    aget-object v0, v2, v1

    .line 115
    .local v0, "scoringQuery":Lorg/apache/lucene/search/Query;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Query;->extractTerms(Ljava/util/Set;)V

    .line 114
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected getCustomScoreProvider(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/CustomScoreProvider;
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    new-instance v0, Lorg/apache/lucene/queries/CustomScoreProvider;

    invoke-direct {v0, p1}, Lorg/apache/lucene/queries/CustomScoreProvider;-><init>(Lorg/apache/lucene/index/AtomicReaderContext;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 167
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CustomScoreQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    .line 167
    xor-int/2addr v1, v0

    .line 168
    iget-boolean v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->strict:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4d2

    .line 167
    :goto_0
    xor-int/2addr v0, v1

    return v0

    .line 168
    :cond_0
    const/16 v0, 0x10e1

    goto :goto_0
.end method

.method public isStrict()Z
    .locals 1

    .prologue
    .line 373
    iget-boolean v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->strict:Z

    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 389
    const-string v0, "custom"

    return-object v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    const/4 v0, 0x0

    .line 93
    .local v0, "clone":Lorg/apache/lucene/queries/CustomScoreQuery;
    iget-object v4, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 94
    .local v2, "sq":Lorg/apache/lucene/search/Query;
    iget-object v4, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    if-eq v2, v4, :cond_0

    .line 95
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CustomScoreQuery;->clone()Lorg/apache/lucene/queries/CustomScoreQuery;

    move-result-object v0

    .line 96
    iput-object v2, v0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    .line 99
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    array-length v4, v4

    if-lt v1, v4, :cond_1

    .line 107
    if-nez v0, :cond_4

    .end local p0    # "this":Lorg/apache/lucene/queries/CustomScoreQuery;
    :goto_1
    return-object p0

    .line 100
    .restart local p0    # "this":Lorg/apache/lucene/queries/CustomScoreQuery;
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    aget-object v4, v4, v1

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 101
    .local v3, "v":Lorg/apache/lucene/search/Query;
    iget-object v4, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    aget-object v4, v4, v1

    if-eq v3, v4, :cond_3

    .line 102
    if-nez v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/lucene/queries/CustomScoreQuery;->clone()Lorg/apache/lucene/queries/CustomScoreQuery;

    move-result-object v0

    .line 103
    :cond_2
    iget-object v4, v0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    aput-object v3, v4, v1

    .line 99
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v3    # "v":Lorg/apache/lucene/search/Query;
    :cond_4
    move-object p0, v0

    .line 107
    goto :goto_1
.end method

.method public setStrict(Z)V
    .locals 0
    .param p1, "strict"    # Z

    .prologue
    .line 382
    iput-boolean p1, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->strict:Z

    .line 383
    return-void
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 134
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/CustomScoreQuery;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 135
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    iget-object v3, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 139
    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    iget-boolean v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery;->strict:Z

    if-eqz v2, :cond_1

    const-string v2, " STRICT"

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/queries/CustomScoreQuery;->getBoost()F

    move-result v3

    invoke-static {v3}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 136
    :cond_0
    aget-object v1, v3, v2

    .line 137
    .local v1, "scoringQuery":Lorg/apache/lucene/search/Query;
    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 140
    .end local v1    # "scoringQuery":Lorg/apache/lucene/search/Query;
    :cond_1
    const-string v2, ""

    goto :goto_1
.end method
