.class public Lorg/apache/lucene/queries/function/valuesource/TFValueSource;
.super Lorg/apache/lucene/queries/function/valuesource/TermFreqValueSource;
.source "TFValueSource.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "indexedField"    # Ljava/lang/String;
    .param p4, "indexedBytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/queries/function/valuesource/TermFreqValueSource;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 41
    return-void
.end method


# virtual methods
.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 6
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 51
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource;->indexedField:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v3

    .line 52
    .local v3, "terms":Lorg/apache/lucene/index/Terms;
    const-string v4, "searcher"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/IndexSearcher;

    .line 53
    .local v1, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    invoke-virtual {v1}, Lorg/apache/lucene/search/IndexSearcher;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource;->indexedField:Ljava/lang/String;

    invoke-static {v4, v5}, Lorg/apache/lucene/queries/function/valuesource/IDFValueSource;->asTFIDF(Lorg/apache/lucene/search/similarities/Similarity;Ljava/lang/String;)Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    move-result-object v2

    .line 54
    .local v2, "similarity":Lorg/apache/lucene/search/similarities/TFIDFSimilarity;
    if-nez v2, :cond_0

    .line 55
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    const-string v5, "requires a TFIDFSimilarity (such as DefaultSimilarity)"

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 58
    :cond_0
    new-instance v4, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;

    invoke-direct {v4, p0, p0, v3, v2}, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/TFValueSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/search/similarities/TFIDFSimilarity;)V

    return-object v4
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    const-string v0, "tf"

    return-object v0
.end method
