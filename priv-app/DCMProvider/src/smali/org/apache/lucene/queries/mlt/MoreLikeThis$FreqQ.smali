.class Lorg/apache/lucene/queries/mlt/MoreLikeThis$FreqQ;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "MoreLikeThis.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/mlt/MoreLikeThis;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FreqQ"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<[",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "s"    # I

    .prologue
    .line 898
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/PriorityQueue;-><init>(I)V

    .line 899
    return-void
.end method


# virtual methods
.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Object;

    check-cast p2, [Ljava/lang/Object;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queries/mlt/MoreLikeThis$FreqQ;->lessThan([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected lessThan([Ljava/lang/Object;[Ljava/lang/Object;)Z
    .locals 4
    .param p1, "aa"    # [Ljava/lang/Object;
    .param p2, "bb"    # [Ljava/lang/Object;

    .prologue
    const/4 v2, 0x2

    .line 903
    aget-object v0, p1, v2

    check-cast v0, Ljava/lang/Float;

    .line 904
    .local v0, "fa":Ljava/lang/Float;
    aget-object v1, p2, v2

    check-cast v1, Ljava/lang/Float;

    .line 905
    .local v1, "fb":Ljava/lang/Float;
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
