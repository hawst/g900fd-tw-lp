.class public Lorg/apache/lucene/queries/BooleanFilter;
.super Lorg/apache/lucene/search/Filter;
.source "BooleanFilter.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/Filter;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/lucene/queries/FilterClause;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final clauses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/FilterClause;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lorg/apache/lucene/queries/BooleanFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/queries/BooleanFilter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/BooleanFilter;->clauses:Ljava/util/List;

    .line 44
    return-void
.end method

.method private static getDISI(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 3
    .param p0, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v0

    .line 108
    .local v0, "set":Lorg/apache/lucene/search/DocIdSet;
    if-eqz v0, :cond_0

    sget-object v2, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    if-ne v0, v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/lucene/queries/FilterClause;)V
    .locals 1
    .param p1, "filterClause"    # Lorg/apache/lucene/queries/FilterClause;

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/queries/BooleanFilter;->clauses:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    return-void
.end method

.method public final add(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    .locals 1
    .param p1, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p2, "occur"    # Lorg/apache/lucene/search/BooleanClause$Occur;

    .prologue
    .line 120
    new-instance v0, Lorg/apache/lucene/queries/FilterClause;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/queries/FilterClause;-><init>(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queries/BooleanFilter;->add(Lorg/apache/lucene/queries/FilterClause;)V

    .line 121
    return-void
.end method

.method public clauses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/FilterClause;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lorg/apache/lucene/queries/BooleanFilter;->clauses:Ljava/util/List;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 141
    if-ne p0, p1, :cond_0

    .line 142
    const/4 v1, 0x1

    .line 150
    :goto_0
    return v1

    .line 145
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 146
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 149
    check-cast v0, Lorg/apache/lucene/queries/BooleanFilter;

    .line 150
    .local v0, "other":Lorg/apache/lucene/queries/BooleanFilter;
    iget-object v1, p0, Lorg/apache/lucene/queries/BooleanFilter;->clauses:Ljava/util/List;

    iget-object v2, v0, Lorg/apache/lucene/queries/BooleanFilter;->clauses:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 8
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    const/4 v4, 0x0

    .line 55
    .local v4, "res":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    .line 57
    .local v3, "reader":Lorg/apache/lucene/index/AtomicReader;
    const/4 v2, 0x0

    .line 58
    .local v2, "hasShouldClauses":Z
    iget-object v5, p0, Lorg/apache/lucene/queries/BooleanFilter;->clauses:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 69
    if-eqz v2, :cond_3

    if-nez v4, :cond_3

    .line 70
    sget-object v5, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 101
    :goto_1
    return-object v5

    .line 58
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queries/FilterClause;

    .line 59
    .local v1, "fc":Lorg/apache/lucene/queries/FilterClause;
    invoke-virtual {v1}, Lorg/apache/lucene/queries/FilterClause;->getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v6

    sget-object v7, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v6, v7, :cond_0

    .line 60
    const/4 v2, 0x1

    .line 61
    invoke-virtual {v1}, Lorg/apache/lucene/queries/FilterClause;->getFilter()Lorg/apache/lucene/search/Filter;

    move-result-object v6

    invoke-static {v6, p1}, Lorg/apache/lucene/queries/BooleanFilter;->getDISI(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    .line 62
    .local v0, "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    if-eqz v0, :cond_0

    .line 63
    if-nez v4, :cond_2

    .line 64
    new-instance v4, Lorg/apache/lucene/util/FixedBitSet;

    .end local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v6

    invoke-direct {v4, v6}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 66
    .restart local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    :cond_2
    invoke-virtual {v4, v0}, Lorg/apache/lucene/util/FixedBitSet;->or(Lorg/apache/lucene/search/DocIdSetIterator;)V

    goto :goto_0

    .line 72
    .end local v0    # "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    .end local v1    # "fc":Lorg/apache/lucene/queries/FilterClause;
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/queries/BooleanFilter;->clauses:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_6

    .line 86
    iget-object v5, p0, Lorg/apache/lucene/queries/BooleanFilter;->clauses:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_9

    .line 101
    if-eqz v4, :cond_c

    invoke-static {v4, p2}, Lorg/apache/lucene/search/BitsFilteredDocIdSet;->wrap(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v5

    goto :goto_1

    .line 72
    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queries/FilterClause;

    .line 73
    .restart local v1    # "fc":Lorg/apache/lucene/queries/FilterClause;
    invoke-virtual {v1}, Lorg/apache/lucene/queries/FilterClause;->getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v6

    sget-object v7, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v6, v7, :cond_4

    .line 74
    if-nez v4, :cond_8

    .line 75
    sget-boolean v6, Lorg/apache/lucene/queries/BooleanFilter;->$assertionsDisabled:Z

    if-nez v6, :cond_7

    if-eqz v2, :cond_7

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 76
    :cond_7
    new-instance v4, Lorg/apache/lucene/util/FixedBitSet;

    .end local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v6

    invoke-direct {v4, v6}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 77
    .restart local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    const/4 v6, 0x0

    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v7

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/FixedBitSet;->set(II)V

    .line 79
    :cond_8
    invoke-virtual {v1}, Lorg/apache/lucene/queries/FilterClause;->getFilter()Lorg/apache/lucene/search/Filter;

    move-result-object v6

    invoke-static {v6, p1}, Lorg/apache/lucene/queries/BooleanFilter;->getDISI(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    .line 80
    .restart local v0    # "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    if-eqz v0, :cond_4

    .line 81
    invoke-virtual {v4, v0}, Lorg/apache/lucene/util/FixedBitSet;->andNot(Lorg/apache/lucene/search/DocIdSetIterator;)V

    goto :goto_2

    .line 86
    .end local v0    # "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    .end local v1    # "fc":Lorg/apache/lucene/queries/FilterClause;
    :cond_9
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queries/FilterClause;

    .line 87
    .restart local v1    # "fc":Lorg/apache/lucene/queries/FilterClause;
    invoke-virtual {v1}, Lorg/apache/lucene/queries/FilterClause;->getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;

    move-result-object v6

    sget-object v7, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v6, v7, :cond_5

    .line 88
    invoke-virtual {v1}, Lorg/apache/lucene/queries/FilterClause;->getFilter()Lorg/apache/lucene/search/Filter;

    move-result-object v6

    invoke-static {v6, p1}, Lorg/apache/lucene/queries/BooleanFilter;->getDISI(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    .line 89
    .restart local v0    # "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    if-nez v0, :cond_a

    .line 90
    sget-object v5, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto/16 :goto_1

    .line 92
    :cond_a
    if-nez v4, :cond_b

    .line 93
    new-instance v4, Lorg/apache/lucene/util/FixedBitSet;

    .end local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v6

    invoke-direct {v4, v6}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 94
    .restart local v4    # "res":Lorg/apache/lucene/util/FixedBitSet;
    invoke-virtual {v4, v0}, Lorg/apache/lucene/util/FixedBitSet;->or(Lorg/apache/lucene/search/DocIdSetIterator;)V

    goto :goto_3

    .line 96
    :cond_b
    invoke-virtual {v4, v0}, Lorg/apache/lucene/util/FixedBitSet;->and(Lorg/apache/lucene/search/DocIdSetIterator;)V

    goto :goto_3

    .line 101
    .end local v0    # "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    .end local v1    # "fc":Lorg/apache/lucene/queries/FilterClause;
    :cond_c
    sget-object v5, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto/16 :goto_1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 155
    const v0, 0x272b5eb6

    iget-object v1, p0, Lorg/apache/lucene/queries/BooleanFilter;->clauses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/queries/FilterClause;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0}, Lorg/apache/lucene/queries/BooleanFilter;->clauses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "BooleanFilter("

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 162
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    .line 163
    .local v2, "minLen":I
    iget-object v3, p0, Lorg/apache/lucene/queries/BooleanFilter;->clauses:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 169
    const/16 v3, 0x29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 163
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/queries/FilterClause;

    .line 164
    .local v1, "c":Lorg/apache/lucene/queries/FilterClause;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-le v4, v2, :cond_1

    .line 165
    const/16 v4, 0x20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 167
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
