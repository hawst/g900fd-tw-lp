.class Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;
.super Lorg/apache/lucene/search/FieldComparator;
.source "ValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/function/ValueSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ValueSourceComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator",
        "<",
        "Ljava/lang/Double;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:D

.field private docVals:Lorg/apache/lucene/queries/function/FunctionValues;

.field private final fcontext:Ljava/util/Map;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/ValueSource;

.field private final values:[D


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;Ljava/util/Map;I)V
    .locals 1
    .param p2, "fcontext"    # Ljava/util/Map;
    .param p3, "numHits"    # I

    .prologue
    .line 135
    iput-object p1, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->this$0:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    .line 136
    iput-object p2, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->fcontext:Ljava/util/Map;

    .line 137
    new-array v0, p3, [D

    iput-object v0, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->values:[D

    .line 138
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 4
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->values:[D

    aget-wide v0, v0, p1

    iget-object v2, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->values:[D

    aget-wide v2, v2, p2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    return v0
.end method

.method public compareBottom(I)I
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 147
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->bottom:D

    iget-object v2, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->docVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->doubleVal(I)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    return v0
.end method

.method public compareDocToValue(ILjava/lang/Double;)I
    .locals 5
    .param p1, "doc"    # I
    .param p2, "valueObj"    # Ljava/lang/Double;

    .prologue
    .line 173
    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 174
    .local v2, "value":D
    iget-object v4, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->docVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->doubleVal(I)D

    move-result-wide v0

    .line 175
    .local v0, "docValue":D
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v4

    return v4
.end method

.method public bridge synthetic compareDocToValue(ILjava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->compareDocToValue(ILjava/lang/Double;)I

    move-result v0

    return v0
.end method

.method public copy(II)V
    .locals 4
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->values:[D

    iget-object v1, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->docVals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/queries/function/FunctionValues;->doubleVal(I)D

    move-result-wide v2

    aput-wide v2, v0, p1

    .line 153
    return-void
.end method

.method public setBottom(I)V
    .locals 2
    .param p1, "bottom"    # I

    .prologue
    .line 163
    iget-object v0, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->values:[D

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->bottom:D

    .line 164
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/FieldComparator;
    .locals 2
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->this$0:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->fcontext:Ljava/util/Map;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->docVals:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 158
    return-object p0
.end method

.method public value(I)Ljava/lang/Double;
    .locals 2
    .param p1, "slot"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->values:[D

    aget-wide v0, v0, p1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;->value(I)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method
