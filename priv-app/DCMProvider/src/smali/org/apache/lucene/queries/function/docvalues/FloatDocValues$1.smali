.class Lorg/apache/lucene/queries/function/docvalues/FloatDocValues$1;
.super Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.source "FloatDocValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;

    .line 81
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;-><init>()V

    .line 82
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueFloat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    return-void
.end method


# virtual methods
.method public fillValue(I)V
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;->floatVal(I)F

    move-result v1

    iput v1, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    .line 92
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;->exists(I)Z

    move-result v1

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    .line 93
    return-void
.end method

.method public getValue()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    return-object v0
.end method
