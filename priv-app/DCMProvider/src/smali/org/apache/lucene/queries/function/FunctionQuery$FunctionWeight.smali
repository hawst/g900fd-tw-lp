.class public Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;
.super Lorg/apache/lucene/search/Weight;
.source "FunctionQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/function/FunctionQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "FunctionWeight"
.end annotation


# instance fields
.field protected final context:Ljava/util/Map;

.field protected queryNorm:F

.field protected queryWeight:F

.field protected final searcher:Lorg/apache/lucene/search/IndexSearcher;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/FunctionQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/FunctionQuery;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 2
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    iput-object p1, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->this$0:Lorg/apache/lucene/queries/function/FunctionQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 70
    iput-object p2, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 71
    invoke-static {p2}, Lorg/apache/lucene/queries/function/ValueSource;->newContext(Lorg/apache/lucene/search/IndexSearcher;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->context:Ljava/util/Map;

    .line 72
    iget-object v0, p1, Lorg/apache/lucene/queries/function/FunctionQuery;->func:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->context:Ljava/util/Map;

    invoke-virtual {v0, v1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 73
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 2
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 100
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    invoke-virtual {p0, p1, v1, v1, v0}, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;->explain(I)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->this$0:Lorg/apache/lucene/queries/function/FunctionQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->this$0:Lorg/apache/lucene/queries/function/FunctionQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/FunctionQuery;->getBoost()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->queryWeight:F

    .line 83
    iget v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->queryWeight:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public normalize(FF)V
    .locals 2
    .param p1, "norm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    .line 88
    mul-float v0, p1, p2

    iput v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->queryNorm:F

    .line 89
    iget v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->queryNorm:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->queryWeight:F

    .line 90
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    new-instance v0, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->this$0:Lorg/apache/lucene/queries/function/FunctionQuery;

    iget v5, p0, Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;->queryWeight:F

    move-object v2, p1

    move-object v3, p4

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/queries/function/FunctionQuery$AllScorer;-><init>(Lorg/apache/lucene/queries/function/FunctionQuery;Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/queries/function/FunctionQuery$FunctionWeight;F)V

    return-object v0
.end method
