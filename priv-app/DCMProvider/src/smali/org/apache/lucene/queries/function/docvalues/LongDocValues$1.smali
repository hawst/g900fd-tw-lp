.class Lorg/apache/lucene/queries/function/docvalues/LongDocValues$1;
.super Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.source "LongDocValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mval:Lorg/apache/lucene/util/mutable/MutableValueLong;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/docvalues/LongDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/docvalues/LongDocValues;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/docvalues/LongDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/LongDocValues;

    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;-><init>()V

    .line 87
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueLong;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueLong;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/LongDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueLong;

    return-void
.end method


# virtual methods
.method public fillValue(I)V
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/LongDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueLong;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/LongDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/LongDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->longVal(I)J

    move-result-wide v2

    iput-wide v2, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    .line 97
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/LongDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueLong;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/LongDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/LongDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->exists(I)Z

    move-result v1

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    .line 98
    return-void
.end method

.method public getValue()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/LongDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueLong;

    return-object v0
.end method
