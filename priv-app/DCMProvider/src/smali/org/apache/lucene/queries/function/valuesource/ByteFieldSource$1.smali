.class Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;
.super Lorg/apache/lucene/queries/function/FunctionValues;
.source "ByteFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource;

.field private final synthetic val$arr:Lorg/apache/lucene/search/FieldCache$Bytes;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource;Lorg/apache/lucene/search/FieldCache$Bytes;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource;

    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Bytes;

    .line 56
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues;-><init>()V

    return-void
.end method


# virtual methods
.method public byteVal(I)B
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Bytes;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Bytes;->get(I)B

    move-result v0

    return v0
.end method

.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Bytes;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Bytes;->get(I)B

    move-result v0

    int-to-double v0, v0

    return-wide v0
.end method

.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Bytes;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Bytes;->get(I)B

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public intVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Bytes;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Bytes;->get(I)B

    move-result v0

    return v0
.end method

.method public longVal(I)J
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Bytes;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Bytes;->get(I)B

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Bytes;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Bytes;->get(I)B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public shortVal(I)S
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Bytes;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Bytes;->get(I)B

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Bytes;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Bytes;->get(I)B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/ByteFieldSource$1;->byteVal(I)B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
