.class public abstract Lorg/apache/lucene/queries/function/docvalues/LongDocValues;
.super Lorg/apache/lucene/queries/function/FunctionValues;
.source "LongDocValues.java"


# instance fields
.field protected final vs:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "vs"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->vs:Lorg/apache/lucene/queries/function/ValueSource;

    .line 34
    return-void
.end method


# virtual methods
.method public boolVal(I)Z
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->longVal(I)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public byteVal(I)B
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->longVal(I)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-byte v0, v0

    return v0
.end method

.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->longVal(I)J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method public floatVal(I)F
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->longVal(I)J

    move-result-wide v0

    long-to-float v0, v0

    return v0
.end method

.method public getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lorg/apache/lucene/queries/function/docvalues/LongDocValues$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues$1;-><init>(Lorg/apache/lucene/queries/function/docvalues/LongDocValues;)V

    return-object v0
.end method

.method public intVal(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->longVal(I)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public abstract longVal(I)J
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->exists(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->longVal(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shortVal(I)S
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->longVal(I)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-short v0, v0

    return v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->longVal(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->vs:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/LongDocValues;->strVal(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
