.class Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;
.super Lorg/apache/lucene/queries/function/FunctionValues;
.source "VectorValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;

.field private final synthetic val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;[Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->this$0:Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;

    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    .line 113
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues;-><init>()V

    return-void
.end method


# virtual methods
.method public byteVal(I[B)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "vals"    # [B

    .prologue
    .line 116
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 119
    return-void

    .line 117
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->byteVal(I)B

    move-result v1

    aput-byte v1, p2, v0

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public doubleVal(I[D)V
    .locals 4
    .param p1, "doc"    # I
    .param p2, "vals"    # [D

    .prologue
    .line 151
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 154
    return-void

    .line 152
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->doubleVal(I)D

    move-result-wide v2

    aput-wide v2, p2, v0

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public floatVal(I[F)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "vals"    # [F

    .prologue
    .line 130
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 133
    return-void

    .line 131
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v1

    aput v1, p2, v0

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public intVal(I[I)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "vals"    # [I

    .prologue
    .line 137
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 140
    return-void

    .line 138
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->intVal(I)I

    move-result v1

    aput v1, p2, v0

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public longVal(I[J)V
    .locals 4
    .param p1, "doc"    # I
    .param p2, "vals"    # [J

    .prologue
    .line 144
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 147
    return-void

    .line 145
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->longVal(I)J

    move-result-wide v2

    aput-wide v2, p2, v0

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public shortVal(I[S)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "vals"    # [S

    .prologue
    .line 123
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 126
    return-void

    .line 124
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->shortVal(I)S

    move-result v1

    aput-short v1, p2, v0

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public strVal(I[Ljava/lang/String;)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "vals"    # [Ljava/lang/String;

    .prologue
    .line 158
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 161
    return-void

    .line 159
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->strVal(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 7
    .param p1, "doc"    # I

    .prologue
    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->this$0:Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;

    invoke-virtual {v3}, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 167
    const/4 v0, 0x1

    .line 168
    .local v0, "firstTime":Z
    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/VectorValueSource$2;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 176
    const/16 v3, 0x29

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 177
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 168
    :cond_0
    aget-object v2, v4, v3

    .line 169
    .local v2, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    if-eqz v0, :cond_1

    .line 170
    const/4 v0, 0x0

    .line 174
    :goto_1
    invoke-virtual {v2, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 172
    :cond_1
    const/16 v6, 0x2c

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
