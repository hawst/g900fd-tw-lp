.class Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;
.super Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;
.source "QueryValueSource.java"


# instance fields
.field final acceptDocs:Lorg/apache/lucene/util/Bits;

.field final defVal:F

.field final fcontext:Ljava/util/Map;

.field lastDocRequested:I

.field noMatches:Z

.field final q:Lorg/apache/lucene/search/Query;

.field final readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

.field scorer:Lorg/apache/lucene/search/Scorer;

.field scorerDoc:I

.field final weight:Lorg/apache/lucene/search/Weight;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;Lorg/apache/lucene/index/AtomicReaderContext;Ljava/util/Map;)V
    .locals 3
    .param p1, "vs"    # Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p3, "fcontext"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 88
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->noMatches:Z

    .line 92
    const v2, 0x7fffffff

    iput v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->lastDocRequested:I

    .line 98
    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 99
    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->acceptDocs:Lorg/apache/lucene/util/Bits;

    .line 100
    iget v2, p1, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->defVal:F

    iput v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->defVal:F

    .line 101
    iget-object v2, p1, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->q:Lorg/apache/lucene/search/Query;

    iput-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->q:Lorg/apache/lucene/search/Query;

    .line 102
    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->fcontext:Ljava/util/Map;

    .line 104
    if-nez p3, :cond_2

    const/4 v0, 0x0

    .line 105
    .local v0, "w":Lorg/apache/lucene/search/Weight;
    :goto_0
    if-nez v0, :cond_1

    .line 107
    if-nez p3, :cond_3

    .line 108
    new-instance v1, Lorg/apache/lucene/search/IndexSearcher;

    invoke-static {p2}, Lorg/apache/lucene/index/ReaderUtil;->getTopLevelContext(Lorg/apache/lucene/index/IndexReaderContext;)Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReaderContext;)V

    .line 115
    .local v1, "weightSearcher":Lorg/apache/lucene/search/IndexSearcher;
    :cond_0
    :goto_1
    invoke-virtual {p1, p3, v1}, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 116
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "w":Lorg/apache/lucene/search/Weight;
    check-cast v0, Lorg/apache/lucene/search/Weight;

    .line 118
    .end local v1    # "weightSearcher":Lorg/apache/lucene/search/IndexSearcher;
    .restart local v0    # "w":Lorg/apache/lucene/search/Weight;
    :cond_1
    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->weight:Lorg/apache/lucene/search/Weight;

    .line 119
    return-void

    .line 104
    .end local v0    # "w":Lorg/apache/lucene/search/Weight;
    :cond_2
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Weight;

    move-object v0, v2

    goto :goto_0

    .line 110
    .restart local v0    # "w":Lorg/apache/lucene/search/Weight;
    :cond_3
    const-string v2, "searcher"

    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/IndexSearcher;

    .line 111
    .restart local v1    # "weightSearcher":Lorg/apache/lucene/search/IndexSearcher;
    if-nez v1, :cond_0

    .line 112
    new-instance v1, Lorg/apache/lucene/search/IndexSearcher;

    .end local v1    # "weightSearcher":Lorg/apache/lucene/search/IndexSearcher;
    invoke-static {p2}, Lorg/apache/lucene/index/ReaderUtil;->getTopLevelContext(Lorg/apache/lucene/index/IndexReaderContext;)Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReaderContext;)V

    .restart local v1    # "weightSearcher":Lorg/apache/lucene/search/IndexSearcher;
    goto :goto_1
.end method


# virtual methods
.method public exists(I)Z
    .locals 8
    .param p1, "doc"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 155
    :try_start_0
    iget v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->lastDocRequested:I

    if-ge p1, v3, :cond_2

    .line 156
    iget-boolean v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->noMatches:Z

    if-eqz v3, :cond_1

    .line 177
    :cond_0
    :goto_0
    return v1

    .line 157
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v7, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->acceptDocs:Lorg/apache/lucene/util/Bits;

    invoke-virtual {v3, v4, v5, v6, v7}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 158
    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I

    .line 159
    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    if-nez v3, :cond_2

    .line 160
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->noMatches:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 178
    :catch_0
    move-exception v0

    .line 179
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "caught exception in QueryDocVals("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") doc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 164
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_1
    iput p1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->lastDocRequested:I

    .line 166
    iget v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I

    if-ge v3, p1, :cond_3

    .line 167
    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I

    .line 170
    :cond_3
    iget v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    if-gt v3, p1, :cond_0

    move v1, v2

    .line 177
    goto :goto_0
.end method

.method public floatVal(I)F
    .locals 6
    .param p1, "doc"    # I

    .prologue
    .line 124
    :try_start_0
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->lastDocRequested:I

    if-ge p1, v1, :cond_2

    .line 125
    iget-boolean v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->noMatches:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->defVal:F

    .line 146
    :goto_0
    return v1

    .line 126
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->acceptDocs:Lorg/apache/lucene/util/Bits;

    invoke-virtual {v1, v2, v3, v4, v5}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 127
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    if-nez v1, :cond_1

    .line 128
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->noMatches:Z

    .line 129
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->defVal:F

    goto :goto_0

    .line 131
    :cond_1
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I

    .line 133
    :cond_2
    iput p1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->lastDocRequested:I

    .line 135
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I

    if-ge v1, p1, :cond_3

    .line 136
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I

    .line 139
    :cond_3
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I

    if-le v1, p1, :cond_4

    .line 142
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->defVal:F

    goto :goto_0

    .line 146
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "caught exception in QueryDocVals("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") doc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    .locals 1

    .prologue
    .line 199
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;)V

    return-object v0
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 186
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->exists(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 187
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "caught exception in QueryDocVals("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") doc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "query("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",def="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->defVal:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->floatVal(I)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
