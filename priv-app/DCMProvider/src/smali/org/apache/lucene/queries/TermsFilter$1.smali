.class Lorg/apache/lucene/queries/TermsFilter$1;
.super Lorg/apache/lucene/queries/TermsFilter$FieldAndTermEnum;
.source "TermsFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/TermsFilter;-><init>(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final iter:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lorg/apache/lucene/queries/TermsFilter$FieldAndTermEnum;-><init>()V

    .line 68
    # invokes: Lorg/apache/lucene/queries/TermsFilter;->sort(Ljava/util/List;)Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/queries/TermsFilter;->access$0(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/TermsFilter$1;->iter:Ljava/util/Iterator;

    .line 1
    return-void
.end method


# virtual methods
.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 2

    .prologue
    .line 71
    iget-object v1, p0, Lorg/apache/lucene/queries/TermsFilter$1;->iter:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lorg/apache/lucene/queries/TermsFilter$1;->iter:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/Term;

    .line 73
    .local v0, "next":Lorg/apache/lucene/index/Term;
    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/queries/TermsFilter$1;->field:Ljava/lang/String;

    .line 74
    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 76
    .end local v0    # "next":Lorg/apache/lucene/index/Term;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
