.class public Lorg/apache/lucene/queries/function/valuesource/NumDocsValueSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "NumDocsValueSource.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/NumDocsValueSource;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 51
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 2
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;

    invoke-static {p2}, Lorg/apache/lucene/index/ReaderUtil;->getTopLevelContext(Lorg/apache/lucene/index/IndexReaderContext;)Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReaderContext;->reader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v1

    invoke-direct {v0, v1, p0}, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;-><init>(ILorg/apache/lucene/queries/function/ValueSource;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string v0, "numdocs"

    return-object v0
.end method
