.class public final Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$DocTermsIndexException;
.super Ljava/lang/RuntimeException;
.source "DocTermsIndexDocValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DocTermsIndexException"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/RuntimeException;)V
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/RuntimeException;

    .prologue
    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Can\'t initialize DocTermsIndex to generate (function) FunctionValues for field: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 164
    return-void
.end method
