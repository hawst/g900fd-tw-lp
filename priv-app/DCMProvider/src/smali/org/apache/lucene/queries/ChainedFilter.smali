.class public Lorg/apache/lucene/queries/ChainedFilter;
.super Lorg/apache/lucene/search/Filter;
.source "ChainedFilter.java"


# static fields
.field public static final AND:I = 0x1

.field public static final ANDNOT:I = 0x2

.field public static final DEFAULT:I = 0x0

.field public static final OR:I = 0x0

.field public static final XOR:I = 0x3


# instance fields
.field private chain:[Lorg/apache/lucene/search/Filter;

.field private logic:I

.field private logicArray:[I


# direct methods
.method public constructor <init>([Lorg/apache/lucene/search/Filter;)V
    .locals 1
    .param p1, "chain"    # [Lorg/apache/lucene/search/Filter;

    .prologue
    .line 72
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queries/ChainedFilter;->logic:I

    .line 73
    iput-object p1, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    .line 74
    return-void
.end method

.method public constructor <init>([Lorg/apache/lucene/search/Filter;I)V
    .locals 1
    .param p1, "chain"    # [Lorg/apache/lucene/search/Filter;
    .param p2, "logic"    # I

    .prologue
    .line 93
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queries/ChainedFilter;->logic:I

    .line 94
    iput-object p1, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    .line 95
    iput p2, p0, Lorg/apache/lucene/queries/ChainedFilter;->logic:I

    .line 96
    return-void
.end method

.method public constructor <init>([Lorg/apache/lucene/search/Filter;[I)V
    .locals 1
    .param p1, "chain"    # [Lorg/apache/lucene/search/Filter;
    .param p2, "logicArray"    # [I

    .prologue
    .line 82
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queries/ChainedFilter;->logic:I

    .line 83
    iput-object p1, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    .line 84
    iput-object p2, p0, Lorg/apache/lucene/queries/ChainedFilter;->logicArray:[I

    .line 85
    return-void
.end method

.method private doChain(Lorg/apache/lucene/util/OpenBitSetDISI;ILorg/apache/lucene/search/DocIdSet;)V
    .locals 3
    .param p1, "result"    # Lorg/apache/lucene/util/OpenBitSetDISI;
    .param p2, "logic"    # I
    .param p3, "dis"    # Lorg/apache/lucene/search/DocIdSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 204
    instance-of v1, p3, Lorg/apache/lucene/util/OpenBitSet;

    if-eqz v1, :cond_0

    .line 206
    packed-switch p2, :pswitch_data_0

    .line 220
    invoke-direct {p0, p1, v2, p3}, Lorg/apache/lucene/queries/ChainedFilter;->doChain(Lorg/apache/lucene/util/OpenBitSetDISI;ILorg/apache/lucene/search/DocIdSet;)V

    .line 252
    .end local p3    # "dis":Lorg/apache/lucene/search/DocIdSet;
    :goto_0
    return-void

    .line 208
    .restart local p3    # "dis":Lorg/apache/lucene/search/DocIdSet;
    :pswitch_0
    check-cast p3, Lorg/apache/lucene/util/OpenBitSet;

    .end local p3    # "dis":Lorg/apache/lucene/search/DocIdSet;
    invoke-virtual {p1, p3}, Lorg/apache/lucene/util/OpenBitSetDISI;->or(Lorg/apache/lucene/util/OpenBitSet;)V

    goto :goto_0

    .line 211
    .restart local p3    # "dis":Lorg/apache/lucene/search/DocIdSet;
    :pswitch_1
    check-cast p3, Lorg/apache/lucene/util/OpenBitSet;

    .end local p3    # "dis":Lorg/apache/lucene/search/DocIdSet;
    invoke-virtual {p1, p3}, Lorg/apache/lucene/util/OpenBitSetDISI;->and(Lorg/apache/lucene/util/OpenBitSet;)V

    goto :goto_0

    .line 214
    .restart local p3    # "dis":Lorg/apache/lucene/search/DocIdSet;
    :pswitch_2
    check-cast p3, Lorg/apache/lucene/util/OpenBitSet;

    .end local p3    # "dis":Lorg/apache/lucene/search/DocIdSet;
    invoke-virtual {p1, p3}, Lorg/apache/lucene/util/OpenBitSetDISI;->andNot(Lorg/apache/lucene/util/OpenBitSet;)V

    goto :goto_0

    .line 217
    .restart local p3    # "dis":Lorg/apache/lucene/search/DocIdSet;
    :pswitch_3
    check-cast p3, Lorg/apache/lucene/util/OpenBitSet;

    .end local p3    # "dis":Lorg/apache/lucene/search/DocIdSet;
    invoke-virtual {p1, p3}, Lorg/apache/lucene/util/OpenBitSetDISI;->xor(Lorg/apache/lucene/util/OpenBitSet;)V

    goto :goto_0

    .line 225
    .restart local p3    # "dis":Lorg/apache/lucene/search/DocIdSet;
    :cond_0
    if-nez p3, :cond_2

    .line 226
    sget-object v1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v1}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    .line 234
    .local v0, "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    :cond_1
    :goto_1
    packed-switch p2, :pswitch_data_1

    .line 248
    invoke-direct {p0, p1, v2, p3}, Lorg/apache/lucene/queries/ChainedFilter;->doChain(Lorg/apache/lucene/util/OpenBitSetDISI;ILorg/apache/lucene/search/DocIdSet;)V

    goto :goto_0

    .line 228
    .end local v0    # "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    :cond_2
    invoke-virtual {p3}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    .line 229
    .restart local v0    # "disi":Lorg/apache/lucene/search/DocIdSetIterator;
    if-nez v0, :cond_1

    .line 230
    sget-object v1, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v1}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    goto :goto_1

    .line 236
    :pswitch_4
    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/OpenBitSetDISI;->inPlaceOr(Lorg/apache/lucene/search/DocIdSetIterator;)V

    goto :goto_0

    .line 239
    :pswitch_5
    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/OpenBitSetDISI;->inPlaceAnd(Lorg/apache/lucene/search/DocIdSetIterator;)V

    goto :goto_0

    .line 242
    :pswitch_6
    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/OpenBitSetDISI;->inPlaceNot(Lorg/apache/lucene/search/DocIdSetIterator;)V

    goto :goto_0

    .line 245
    :pswitch_7
    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/OpenBitSetDISI;->inPlaceXor(Lorg/apache/lucene/search/DocIdSetIterator;)V

    goto :goto_0

    .line 206
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 234
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private getDISI(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 3
    .param p1, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p2, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    const/4 v2, 0x0

    invoke-virtual {p1, p2, v2}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v0

    .line 118
    .local v0, "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    if-nez v0, :cond_1

    .line 119
    sget-object v2, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 125
    :cond_0
    :goto_0
    return-object v1

    .line 121
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 122
    .local v1, "iter":Lorg/apache/lucene/search/DocIdSetIterator;
    if-nez v1, :cond_0

    .line 123
    sget-object v2, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v2}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    goto :goto_0
.end method

.method private getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;I[I)Lorg/apache/lucene/search/DocIdSet;
    .locals 4
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "logic"    # I
    .param p3, "index"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 160
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/queries/ChainedFilter;->initialResult(Lorg/apache/lucene/index/AtomicReaderContext;I[I)Lorg/apache/lucene/util/OpenBitSetDISI;

    move-result-object v0

    .line 161
    .local v0, "result":Lorg/apache/lucene/util/OpenBitSetDISI;
    :goto_0
    aget v1, p3, v3

    iget-object v2, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 165
    return-object v0

    .line 163
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    aget v2, p3, v3

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v1

    invoke-direct {p0, v0, p2, v1}, Lorg/apache/lucene/queries/ChainedFilter;->doChain(Lorg/apache/lucene/util/OpenBitSetDISI;ILorg/apache/lucene/search/DocIdSet;)V

    .line 161
    aget v1, p3, v3

    add-int/lit8 v1, v1, 0x1

    aput v1, p3, v3

    goto :goto_0
.end method

.method private getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;[I[I)Lorg/apache/lucene/search/DocIdSet;
    .locals 5
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "logic"    # [I
    .param p3, "index"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 177
    array-length v1, p2

    iget-object v2, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    array-length v2, v2

    if-eq v1, v2, :cond_0

    .line 178
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid number of elements in logic array"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 181
    :cond_0
    aget v1, p2, v4

    invoke-direct {p0, p1, v1, p3}, Lorg/apache/lucene/queries/ChainedFilter;->initialResult(Lorg/apache/lucene/index/AtomicReaderContext;I[I)Lorg/apache/lucene/util/OpenBitSetDISI;

    move-result-object v0

    .line 182
    .local v0, "result":Lorg/apache/lucene/util/OpenBitSetDISI;
    :goto_0
    aget v1, p3, v4

    iget-object v2, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 186
    return-object v0

    .line 184
    :cond_1
    aget v1, p3, v4

    aget v1, p2, v1

    iget-object v2, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    aget v3, p3, v4

    aget-object v2, v2, v3

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/queries/ChainedFilter;->doChain(Lorg/apache/lucene/util/OpenBitSetDISI;ILorg/apache/lucene/search/DocIdSet;)V

    .line 182
    aget v1, p3, v4

    add-int/lit8 v1, v1, 0x1

    aput v1, p3, v4

    goto :goto_0
.end method

.method private initialResult(Lorg/apache/lucene/index/AtomicReaderContext;I[I)Lorg/apache/lucene/util/OpenBitSetDISI;
    .locals 7
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "logic"    # I
    .param p3, "index"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 132
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    .line 138
    .local v0, "reader":Lorg/apache/lucene/index/AtomicReader;
    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    .line 139
    new-instance v1, Lorg/apache/lucene/util/OpenBitSetDISI;

    iget-object v2, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    aget v3, p3, v6

    aget-object v2, v2, v3

    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queries/ChainedFilter;->getDISI(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/util/OpenBitSetDISI;-><init>(Lorg/apache/lucene/search/DocIdSetIterator;I)V

    .line 140
    .local v1, "result":Lorg/apache/lucene/util/OpenBitSetDISI;
    aget v2, p3, v6

    add-int/lit8 v2, v2, 0x1

    aput v2, p3, v6

    .line 148
    :goto_0
    return-object v1

    .line 141
    .end local v1    # "result":Lorg/apache/lucene/util/OpenBitSetDISI;
    :cond_0
    const/4 v2, 0x2

    if-ne p2, v2, :cond_1

    .line 142
    new-instance v1, Lorg/apache/lucene/util/OpenBitSetDISI;

    iget-object v2, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    aget v3, p3, v6

    aget-object v2, v2, v3

    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/queries/ChainedFilter;->getDISI(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/util/OpenBitSetDISI;-><init>(Lorg/apache/lucene/search/DocIdSetIterator;I)V

    .line 143
    .restart local v1    # "result":Lorg/apache/lucene/util/OpenBitSetDISI;
    const-wide/16 v2, 0x0

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lorg/apache/lucene/util/OpenBitSetDISI;->flip(JJ)V

    .line 144
    aget v2, p3, v6

    add-int/lit8 v2, v2, 0x1

    aput v2, p3, v6

    goto :goto_0

    .line 146
    .end local v1    # "result":Lorg/apache/lucene/util/OpenBitSetDISI;
    :cond_1
    new-instance v1, Lorg/apache/lucene/util/OpenBitSetDISI;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/OpenBitSetDISI;-><init>(I)V

    .restart local v1    # "result":Lorg/apache/lucene/util/OpenBitSetDISI;
    goto :goto_0
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;
    .locals 4
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 103
    const/4 v1, 0x1

    new-array v0, v1, [I

    .line 104
    .local v0, "index":[I
    aput v3, v0, v3

    .line 105
    iget v1, p0, Lorg/apache/lucene/queries/ChainedFilter;->logic:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 106
    iget v1, p0, Lorg/apache/lucene/queries/ChainedFilter;->logic:I

    invoke-direct {p0, p1, v1, v0}, Lorg/apache/lucene/queries/ChainedFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;I[I)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v1

    invoke-static {v1, p2}, Lorg/apache/lucene/search/BitsFilteredDocIdSet;->wrap(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v1

    .line 111
    :goto_0
    return-object v1

    .line 107
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/ChainedFilter;->logicArray:[I

    if-eqz v1, :cond_1

    .line 108
    iget-object v1, p0, Lorg/apache/lucene/queries/ChainedFilter;->logicArray:[I

    invoke-direct {p0, p1, v1, v0}, Lorg/apache/lucene/queries/ChainedFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;[I[I)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v1

    invoke-static {v1, p2}, Lorg/apache/lucene/search/BitsFilteredDocIdSet;->wrap(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v1

    goto :goto_0

    .line 111
    :cond_1
    invoke-direct {p0, p1, v3, v0}, Lorg/apache/lucene/queries/ChainedFilter;->getDocIdSet(Lorg/apache/lucene/index/AtomicReaderContext;I[I)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v1

    invoke-static {v1, p2}, Lorg/apache/lucene/search/BitsFilteredDocIdSet;->wrap(Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 191
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v2, "ChainedFilter: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    iget-object v3, p0, Lorg/apache/lucene/queries/ChainedFilter;->chain:[Lorg/apache/lucene/search/Filter;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 197
    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 193
    :cond_0
    aget-object v0, v3, v2

    .line 194
    .local v0, "aChain":Lorg/apache/lucene/search/Filter;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    const/16 v5, 0x20

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 193
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
