.class Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;
.super Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.source "LongFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mval:Lorg/apache/lucene/util/mutable/MutableValueLong;

.field final synthetic this$1:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;

.field private final synthetic val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

.field private final synthetic val$valid:Lorg/apache/lucene/util/Bits;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;Lorg/apache/lucene/search/FieldCache$Longs;Lorg/apache/lucene/util/Bits;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;->this$1:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;

    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;->val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;->val$valid:Lorg/apache/lucene/util/Bits;

    .line 133
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;-><init>()V

    .line 134
    # getter for: Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;
    invoke-static {p1}, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->access$0(Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;)Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource;->newMutableValueLong()Lorg/apache/lucene/util/mutable/MutableValueLong;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueLong;

    return-void
.end method


# virtual methods
.method public fillValue(I)V
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueLong;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;->val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldCache$Longs;->get(I)J

    move-result-wide v2

    iput-wide v2, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->value:J

    .line 144
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueLong;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueLong;->exists:Z

    .line 145
    return-void
.end method

.method public getValue()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueLong;

    return-object v0
.end method
