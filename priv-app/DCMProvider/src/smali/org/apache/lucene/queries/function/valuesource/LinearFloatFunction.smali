.class public Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "LinearFloatFunction.java"


# instance fields
.field protected final intercept:F

.field protected final slope:F

.field protected final source:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;FF)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/queries/function/ValueSource;
    .param p2, "slope"    # F
    .param p3, "intercept"    # F

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    .line 44
    iput p2, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->slope:F

    .line 45
    iput p3, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->intercept:F

    .line 46
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 71
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->slope:F

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "*float("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->intercept:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 84
    const-class v2, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 85
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;

    .line 86
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->slope:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->slope:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 87
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->intercept:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->intercept:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 88
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 2
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    .line 56
    .local v0, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    new-instance v1, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction$1;

    invoke-direct {v1, p0, p0, v0}, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 75
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->slope:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 76
    .local v0, "h":I
    ushr-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0x1e

    or-int v0, v1, v2

    .line 77
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->intercept:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    shl-int/lit8 v1, v0, 0xe

    ushr-int/lit8 v2, v0, 0x13

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 79
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method
