.class public Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "QueryValueSource.java"


# instance fields
.field final defVal:F

.field final q:Lorg/apache/lucene/search/Query;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Query;F)V
    .locals 0
    .param p1, "q"    # Lorg/apache/lucene/search/Query;
    .param p2, "defVal"    # F

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->q:Lorg/apache/lucene/search/Query;

    .line 42
    iput p2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->defVal:F

    .line 43
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 2
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {p2, v1}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    .line 73
    .local v0, "w":Lorg/apache/lucene/search/Weight;
    invoke-interface {p1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "query("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",def="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->defVal:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 65
    const-class v2, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 66
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;

    .line 67
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->q:Lorg/apache/lucene/search/Query;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->defVal:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->defVal:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getDefaultValue()F
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->defVal:F

    return v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->q:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 1
    .param p1, "fcontext"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    invoke-direct {v0, p0, p2, p1}, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;-><init>(Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;Lorg/apache/lucene/index/AtomicReaderContext;Ljava/util/Map;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/QueryValueSource;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1d

    return v0
.end method
