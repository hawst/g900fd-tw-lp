.class public abstract Lorg/apache/lucene/queries/function/valuesource/MultiFunction;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "MultiFunction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queries/function/valuesource/MultiFunction$Values;
    }
.end annotation


# instance fields
.field protected final sources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/function/ValueSource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/function/ValueSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "sources":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queries/function/ValueSource;>;"
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFunction;->sources:Ljava/util/List;

    .line 39
    return-void
.end method

.method public static description(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/function/ValueSource;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "sources":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queries/function/ValueSource;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 51
    const/4 v0, 0x1

    .line 52
    .local v0, "firstTime":Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 60
    const/16 v3, 0x29

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 52
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queries/function/ValueSource;

    .line 53
    .local v2, "source":Lorg/apache/lucene/queries/function/ValueSource;
    if-eqz v0, :cond_1

    .line 54
    const/4 v0, 0x0

    .line 58
    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 56
    :cond_1
    const/16 v4, 0x2c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static toString(Ljava/lang/String;[Lorg/apache/lucene/queries/function/FunctionValues;I)Ljava/lang/String;
    .locals 6
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "valsArr"    # [Lorg/apache/lucene/queries/function/FunctionValues;
    .param p2, "doc"    # I

    .prologue
    .line 94
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 96
    const/4 v0, 0x1

    .line 97
    .local v0, "firstTime":Z
    array-length v4, p1

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v4, :cond_0

    .line 105
    const/16 v3, 0x29

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 106
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 97
    :cond_0
    aget-object v2, p1, v3

    .line 98
    .local v2, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    if-eqz v0, :cond_1

    .line 99
    const/4 v0, 0x0

    .line 103
    :goto_1
    invoke-virtual {v2, p2}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 101
    :cond_1
    const/16 v5, 0x2c

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static valsArr(Ljava/util/List;Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)[Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 6
    .param p1, "fcontext"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/function/ValueSource;",
            ">;",
            "Ljava/util/Map;",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ")[",
            "Lorg/apache/lucene/queries/function/FunctionValues;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    .local p0, "sources":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queries/function/ValueSource;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    new-array v3, v4, [Lorg/apache/lucene/queries/function/FunctionValues;

    .line 66
    .local v3, "valsArr":[Lorg/apache/lucene/queries/function/FunctionValues;
    const/4 v0, 0x0

    .line 67
    .local v0, "i":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 70
    return-object v3

    .line 67
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queries/function/ValueSource;

    .line 68
    .local v2, "source":Lorg/apache/lucene/queries/function/ValueSource;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v5

    aput-object v5, v3, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 3
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFunction;->sources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 113
    return-void

    .line 111
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queries/function/ValueSource;

    .line 112
    .local v0, "source":Lorg/apache/lucene/queries/function/ValueSource;
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    goto :goto_0
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/MultiFunction;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFunction;->sources:Ljava/util/List;

    invoke-static {v0, v1}, Lorg/apache/lucene/queries/function/valuesource/MultiFunction;->description(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 122
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    .line 124
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 123
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/MultiFunction;

    .line 124
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/MultiFunction;
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFunction;->sources:Ljava/util/List;

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/MultiFunction;->sources:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFunction;->sources:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/MultiFunction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected abstract name()Ljava/lang/String;
.end method
