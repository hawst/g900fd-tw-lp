.class Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;
.super Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.source "FloatFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

.field final synthetic this$1:Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;

.field private final synthetic val$arr:Lorg/apache/lucene/search/FieldCache$Floats;

.field private final synthetic val$valid:Lorg/apache/lucene/util/Bits;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;Lorg/apache/lucene/search/FieldCache$Floats;Lorg/apache/lucene/util/Bits;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;->this$1:Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1;

    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Floats;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;->val$valid:Lorg/apache/lucene/util/Bits;

    .line 80
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;-><init>()V

    .line 81
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueFloat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    return-void
.end method


# virtual methods
.method public fillValue(I)V
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Floats;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/FieldCache$Floats;->get(I)F

    move-result v1

    iput v1, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    .line 91
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    .line 92
    return-void
.end method

.method public getValue()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FloatFieldSource$1$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    return-object v0
.end method
