.class public Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;
.super Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;
.source "BytesRefFieldSource.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;-><init>(Ljava/lang/String;)V

    .line 40
    return-void
.end method


# virtual methods
.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 5
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v1

    .line 47
    .local v1, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v2, v3, :cond_0

    .line 48
    sget-object v2, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;->field:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lorg/apache/lucene/search/FieldCache;->getTerms(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    .line 49
    .local v0, "binaryValues":Lorg/apache/lucene/index/BinaryDocValues;
    new-instance v2, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$1;

    invoke-direct {v2, p0, v0}, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;Lorg/apache/lucene/index/BinaryDocValues;)V

    .line 80
    .end local v0    # "binaryValues":Lorg/apache/lucene/index/BinaryDocValues;
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$2;

    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;->field:Ljava/lang/String;

    invoke-direct {v2, p0, p0, p2, v3}, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$2;-><init>(Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/index/AtomicReaderContext;Ljava/lang/String;)V

    goto :goto_0
.end method
