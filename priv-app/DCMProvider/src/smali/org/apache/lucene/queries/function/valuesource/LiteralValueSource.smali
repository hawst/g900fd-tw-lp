.class public Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "LiteralValueSource.java"


# static fields
.field public static final hash:I


# instance fields
.field protected final bytesRef:Lorg/apache/lucene/util/BytesRef;

.field protected final string:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    const-class v0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    sput v0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;->hash:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;->string:Ljava/lang/String;

    .line 39
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, p1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;->bytesRef:Lorg/apache/lucene/util/BytesRef;

    .line 40
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "literal("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;->string:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 76
    if-ne p0, p1, :cond_0

    const/4 v1, 0x1

    .line 81
    :goto_0
    return v1

    .line 77
    :cond_0
    instance-of v1, p1, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 79
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;

    .line 81
    .local v0, "that":Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;->string:Ljava/lang/String;

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;->string:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;->string:Ljava/lang/String;

    return-object v0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource$1;

    invoke-direct {v0, p0, p0}, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;Lorg/apache/lucene/queries/function/ValueSource;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 88
    sget v0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;->hash:I

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LiteralValueSource;->string:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
