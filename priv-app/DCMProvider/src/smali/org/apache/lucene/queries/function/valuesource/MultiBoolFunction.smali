.class public abstract Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;
.super Lorg/apache/lucene/queries/function/valuesource/BoolFunction;
.source "MultiBoolFunction.java"


# instance fields
.field protected final sources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/function/ValueSource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/function/ValueSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "sources":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queries/function/ValueSource;>;"
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/valuesource/BoolFunction;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->sources:Ljava/util/List;

    .line 39
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 3
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->sources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 110
    return-void

    .line 107
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queries/function/ValueSource;

    .line 108
    .local v0, "source":Lorg/apache/lucene/queries/function/ValueSource;
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    goto :goto_0
.end method

.method public description()Ljava/lang/String;
    .locals 5

    .prologue
    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 80
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/16 v3, 0x28

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81
    const/4 v0, 0x1

    .line 82
    .local v0, "first":Z
    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->sources:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 90
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 82
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queries/function/ValueSource;

    .line 83
    .local v2, "source":Lorg/apache/lucene/queries/function/ValueSource;
    if-eqz v0, :cond_1

    .line 84
    const/4 v0, 0x0

    .line 88
    :goto_1
    invoke-virtual {v2}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 86
    :cond_1
    const/16 v4, 0x2c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 100
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    .line 102
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 101
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;

    .line 102
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->sources:Ljava/util/List;

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->sources:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method protected abstract func(I[Lorg/apache/lucene/queries/function/FunctionValues;)Z
.end method

.method public bridge synthetic getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;

    move-result-object v0

    return-object v0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;
    .locals 6
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->sources:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v3, v4, [Lorg/apache/lucene/queries/function/FunctionValues;

    .line 48
    .local v3, "vals":[Lorg/apache/lucene/queries/function/FunctionValues;
    const/4 v0, 0x0

    .line 49
    .local v0, "i":I
    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->sources:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 53
    new-instance v4, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction$1;

    invoke-direct {v4, p0, p0, v3}, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;Lorg/apache/lucene/queries/function/ValueSource;[Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v4

    .line 49
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/queries/function/ValueSource;

    .line 50
    .local v2, "source":Lorg/apache/lucene/queries/function/ValueSource;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v5

    aput-object v5, v3, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->sources:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected abstract name()Ljava/lang/String;
.end method
