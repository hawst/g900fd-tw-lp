.class public Lorg/apache/lucene/queries/function/valuesource/DivFloatFunction;
.super Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;
.source "DivFloatFunction.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "a"    # Lorg/apache/lucene/queries/function/ValueSource;
    .param p2, "b"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;-><init>(Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected func(ILorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/queries/function/FunctionValues;)F
    .locals 2
    .param p1, "doc"    # I
    .param p2, "aVals"    # Lorg/apache/lucene/queries/function/FunctionValues;
    .param p3, "bVals"    # Lorg/apache/lucene/queries/function/FunctionValues;

    .prologue
    .line 41
    invoke-virtual {p2, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v0

    invoke-virtual {p3, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v1

    div-float/2addr v0, v1

    return v0
.end method

.method protected name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    const-string v0, "div"

    return-object v0
.end method
