.class public Lorg/apache/lucene/queries/function/ValueSourceScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ValueSourceScorer.java"


# instance fields
.field protected checkDeletes:Z

.field private doc:I

.field private final liveDocs:Lorg/apache/lucene/util/Bits;

.field protected final maxDoc:I

.field protected final reader:Lorg/apache/lucene/index/IndexReader;

.field protected final values:Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "values"    # Lorg/apache/lucene/queries/function/FunctionValues;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->doc:I

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 42
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->maxDoc:I

    .line 43
    iput-object p2, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->values:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 44
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queries/function/ValueSourceScorer;->setCheckDeletes(Z)V

    .line 45
    invoke-static {p1}, Lorg/apache/lucene/index/MultiFields;->getLiveDocs(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/util/Bits;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 46
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->doc:I

    .line 82
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/ValueSourceScorer;->nextDoc()I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 97
    iget v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->maxDoc:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    const/4 v0, 0x1

    return v0
.end method

.method public getReader()Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->reader:Lorg/apache/lucene/index/IndexReader;

    return-object v0
.end method

.method public matches(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 57
    iget-boolean v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->checkDeletes:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/ValueSourceScorer;->matchesValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public matchesValue(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 61
    const/4 v0, 0x1

    return v0
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    :cond_0
    iget v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->doc:I

    .line 73
    iget v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->doc:I

    iget v1, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->maxDoc:I

    if-lt v0, v1, :cond_1

    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->doc:I

    .line 74
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->doc:I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/queries/function/ValueSourceScorer;->matches(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->doc:I

    goto :goto_0
.end method

.method public score()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->values:Lorg/apache/lucene/queries/function/FunctionValues;

    iget v1, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->doc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v0

    return v0
.end method

.method public setCheckDeletes(Z)V
    .locals 1
    .param p1, "checkDeletes"    # Z

    .prologue
    .line 53
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/queries/function/ValueSourceScorer;->checkDeletes:Z

    .line 54
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
