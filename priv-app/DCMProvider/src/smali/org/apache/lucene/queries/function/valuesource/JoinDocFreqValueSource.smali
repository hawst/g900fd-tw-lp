.class public Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;
.super Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;
.source "JoinDocFreqValueSource.java"


# static fields
.field public static final NAME:Ljava/lang/String; = "joindf"


# instance fields
.field protected final qfield:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "qfield"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;-><init>(Ljava/lang/String;)V

    .line 48
    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;->qfield:Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "joindf("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;->qfield:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;

    if-eq v2, v3, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 88
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;

    .line 89
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;->qfield:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;->qfield:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    invoke-super {p0, v0}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 8
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;->cache:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;->field:Ljava/lang/String;

    const/high16 v7, 0x3f000000    # 0.5f

    invoke-interface {v4, v5, v6, v7}, Lorg/apache/lucene/search/FieldCache;->getTerms(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;F)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v1

    .line 60
    .local v1, "terms":Lorg/apache/lucene/index/BinaryDocValues;
    invoke-static {p2}, Lorg/apache/lucene/index/ReaderUtil;->getTopLevelContext(Lorg/apache/lucene/index/IndexReaderContext;)Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexReaderContext;->reader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v3

    .line 61
    .local v3, "top":Lorg/apache/lucene/index/IndexReader;
    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;->qfield:Ljava/lang/String;

    invoke-static {v3, v4}, Lorg/apache/lucene/index/MultiFields;->getTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v0

    .line 62
    .local v0, "t":Lorg/apache/lucene/index/Terms;
    if-nez v0, :cond_0

    sget-object v2, Lorg/apache/lucene/index/TermsEnum;->EMPTY:Lorg/apache/lucene/index/TermsEnum;

    .line 64
    .local v2, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :goto_0
    new-instance v4, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;

    invoke-direct {v4, p0, p0, v1, v2}, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/index/BinaryDocValues;Lorg/apache/lucene/index/TermsEnum;)V

    return-object v4

    .line 62
    .end local v2    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/JoinDocFreqValueSource;->qfield:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-super {p0}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
