.class Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction$1;
.super Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;
.source "MultiBoolFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;

.field private final synthetic val$vals:[Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;Lorg/apache/lucene/queries/function/ValueSource;[Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction$1;->val$vals:[Lorg/apache/lucene/queries/function/FunctionValues;

    .line 53
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public boolVal(I)Z
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction$1;->val$vals:[Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->func(I[Lorg/apache/lucene/queries/function/FunctionValues;)Z

    move-result v0

    return v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 7
    .param p1, "doc"    # I

    .prologue
    .line 61
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;

    invoke-virtual {v3}, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 62
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/16 v3, 0x28

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 63
    const/4 v1, 0x1

    .line 64
    .local v1, "first":Z
    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/MultiBoolFunction$1;->val$vals:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 72
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 64
    :cond_0
    aget-object v0, v4, v3

    .line 65
    .local v0, "dv":Lorg/apache/lucene/queries/function/FunctionValues;
    if-eqz v1, :cond_1

    .line 66
    const/4 v1, 0x0

    .line 70
    :goto_1
    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 68
    :cond_1
    const/16 v6, 0x2c

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
