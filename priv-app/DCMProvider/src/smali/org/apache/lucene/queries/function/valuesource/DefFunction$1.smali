.class Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;
.super Lorg/apache/lucene/queries/function/valuesource/MultiFunction$Values;
.source "DefFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/DefFunction;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/DefFunction;

.field final upto:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/DefFunction;Lorg/apache/lucene/queries/function/valuesource/MultiFunction;[Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 1
    .param p3, "$anonymous0"    # [Lorg/apache/lucene/queries/function/FunctionValues;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/DefFunction;

    .line 51
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/queries/function/valuesource/MultiFunction$Values;-><init>(Lorg/apache/lucene/queries/function/valuesource/MultiFunction;[Lorg/apache/lucene/queries/function/FunctionValues;)V

    .line 52
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->upto:I

    return-void
.end method

.method private get(I)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 55
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->upto:I

    if-lt v0, v2, :cond_1

    .line 61
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    iget v3, p0, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->upto:I

    aget-object v1, v2, v3

    :cond_0
    return-object v1

    .line 56
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    aget-object v1, v2, v0

    .line 57
    .local v1, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->exists(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public boolVal(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->get(I)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->boolVal(I)Z

    move-result v0

    return v0
.end method

.method public byteVal(I)B
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->get(I)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->byteVal(I)B

    move-result v0

    return v0
.end method

.method public bytesVal(ILorg/apache/lucene/util/BytesRef;)Z
    .locals 1
    .param p1, "doc"    # I
    .param p2, "target"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->get(I)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/FunctionValues;->bytesVal(ILorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    return v0
.end method

.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->get(I)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->doubleVal(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public exists(I)Z
    .locals 6
    .param p1, "doc"    # I

    .prologue
    const/4 v1, 0x0

    .line 117
    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    .line 122
    :goto_1
    return v1

    .line 117
    :cond_0
    aget-object v0, v3, v2

    .line 118
    .local v0, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->exists(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 119
    const/4 v1, 0x1

    goto :goto_1

    .line 117
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->get(I)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v0

    return v0
.end method

.method public getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Lorg/apache/lucene/queries/function/valuesource/MultiFunction$Values;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;

    move-result-object v0

    return-object v0
.end method

.method public intVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->get(I)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->intVal(I)I

    move-result v0

    return v0
.end method

.method public longVal(I)J
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->get(I)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->longVal(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->get(I)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->objectVal(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public shortVal(I)S
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->get(I)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->shortVal(I)S

    move-result v0

    return v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;->get(I)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->strVal(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
