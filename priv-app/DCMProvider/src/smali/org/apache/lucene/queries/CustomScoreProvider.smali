.class public Lorg/apache/lucene/queries/CustomScoreProvider;
.super Ljava/lang/Object;
.source "CustomScoreProvider.java"


# instance fields
.field protected final context:Lorg/apache/lucene/index/AtomicReaderContext;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/AtomicReaderContext;)V
    .locals 0
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lorg/apache/lucene/queries/CustomScoreProvider;->context:Lorg/apache/lucene/index/AtomicReaderContext;

    .line 49
    return-void
.end method


# virtual methods
.method public customExplain(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 4
    .param p1, "doc"    # I
    .param p2, "subQueryExpl"    # Lorg/apache/lucene/search/Explanation;
    .param p3, "valSrcExpl"    # Lorg/apache/lucene/search/Explanation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 154
    const/high16 v1, 0x3f800000    # 1.0f

    .line 155
    .local v1, "valSrcScore":F
    if-eqz p3, :cond_0

    .line 156
    invoke-virtual {p3}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v2

    mul-float/2addr v1, v2

    .line 158
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v2

    mul-float/2addr v2, v1

    const-string v3, "custom score: product of:"

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 159
    .local v0, "exp":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 160
    invoke-virtual {v0, p3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 161
    return-object v0
.end method

.method public customExplain(ILorg/apache/lucene/search/Explanation;[Lorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 7
    .param p1, "doc"    # I
    .param p2, "subQueryExpl"    # Lorg/apache/lucene/search/Explanation;
    .param p3, "valSrcExpls"    # [Lorg/apache/lucene/search/Explanation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 124
    array-length v4, p3

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 125
    aget-object v3, p3, v3

    invoke-virtual {p0, p1, p2, v3}, Lorg/apache/lucene/queries/CustomScoreProvider;->customExplain(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object p2

    .line 139
    .end local p2    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :cond_0
    :goto_0
    return-object p2

    .line 127
    .restart local p2    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :cond_1
    array-length v4, p3

    if-eqz v4, :cond_0

    .line 130
    const/high16 v2, 0x3f800000    # 1.0f

    .line 131
    .local v2, "valSrcScore":F
    array-length v5, p3

    move v4, v3

    :goto_1
    if-lt v4, v5, :cond_2

    .line 134
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v4

    mul-float/2addr v4, v2

    const-string v5, "custom score: product of:"

    invoke-direct {v0, v4, v5}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 135
    .local v0, "exp":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 136
    array-length v4, p3

    :goto_2
    if-lt v3, v4, :cond_3

    move-object p2, v0

    .line 139
    goto :goto_0

    .line 131
    .end local v0    # "exp":Lorg/apache/lucene/search/Explanation;
    :cond_2
    aget-object v1, p3, v4

    .line 132
    .local v1, "valSrcExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v1}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v6

    mul-float/2addr v2, v6

    .line 131
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 136
    .end local v1    # "valSrcExpl":Lorg/apache/lucene/search/Explanation;
    .restart local v0    # "exp":Lorg/apache/lucene/search/Explanation;
    :cond_3
    aget-object v1, p3, v3

    .line 137
    .restart local v1    # "valSrcExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 136
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public customScore(IFF)F
    .locals 1
    .param p1, "doc"    # I
    .param p2, "subQueryScore"    # F
    .param p3, "valSrcScore"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    mul-float v0, p2, p3

    return v0
.end method

.method public customScore(IF[F)F
    .locals 5
    .param p1, "doc"    # I
    .param p2, "subQueryScore"    # F
    .param p3, "valSrcScores"    # [F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 75
    array-length v3, p3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 76
    aget v2, p3, v2

    invoke-virtual {p0, p1, p2, v2}, Lorg/apache/lucene/queries/CustomScoreProvider;->customScore(IFF)F

    move-result v0

    .line 85
    :cond_0
    :goto_0
    return v0

    .line 78
    :cond_1
    array-length v3, p3

    if-nez v3, :cond_2

    .line 79
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1, p2, v2}, Lorg/apache/lucene/queries/CustomScoreProvider;->customScore(IFF)F

    move-result v0

    goto :goto_0

    .line 81
    :cond_2
    move v0, p2

    .line 82
    .local v0, "score":F
    array-length v3, p3

    :goto_1
    if-ge v2, v3, :cond_0

    aget v1, p3, v2

    .line 83
    .local v1, "valSrcScore":F
    mul-float/2addr v0, v1

    .line 82
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
