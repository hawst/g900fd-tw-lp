.class Lorg/apache/lucene/queries/function/valuesource/NormValueSource$1;
.super Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;
.source "NormValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/NormValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/NormValueSource;

.field private final synthetic val$norms:Lorg/apache/lucene/index/NumericDocValues;

.field private final synthetic val$similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/NormValueSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/search/similarities/TFIDFSimilarity;Lorg/apache/lucene/index/NumericDocValues;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/NormValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/NormValueSource;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/NormValueSource$1;->val$similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/NormValueSource$1;->val$norms:Lorg/apache/lucene/index/NumericDocValues;

    .line 71
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public floatVal(I)F
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/NormValueSource$1;->val$similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/NormValueSource$1;->val$norms:Lorg/apache/lucene/index/NumericDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v2

    long-to-int v1, v2

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->decodeNormValue(B)F

    move-result v0

    return v0
.end method
