.class public Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "TotalTermFreqValueSource.java"


# instance fields
.field protected final field:Ljava/lang/String;

.field protected final indexedBytes:Lorg/apache/lucene/util/BytesRef;

.field protected final indexedField:Ljava/lang/String;

.field protected final val:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "indexedField"    # Ljava/lang/String;
    .param p4, "indexedBytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->field:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->val:Ljava/lang/String;

    .line 47
    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->indexedField:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    .line 49
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 12
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    const-wide/16 v2, 0x0

    .line 68
    .local v2, "totalTermFreq":J
    invoke-virtual {p2}, Lorg/apache/lucene/search/IndexSearcher;->getTopReaderContext()Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReaderContext;->leaves()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 77
    :goto_1
    move-wide v4, v2

    .line 78
    .local v4, "ttf":J
    new-instance v1, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource$1;

    invoke-direct {v1, p0, p0, v4, v5}, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;Lorg/apache/lucene/queries/function/ValueSource;J)V

    invoke-interface {p1, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    return-void

    .line 68
    .end local v4    # "ttf":J
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 69
    .local v0, "readerContext":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v8

    new-instance v9, Lorg/apache/lucene/index/Term;

    iget-object v10, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->indexedField:Ljava/lang/String;

    iget-object v11, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v9, v10, v11}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    invoke-virtual {v8, v9}, Lorg/apache/lucene/index/AtomicReader;->totalTermFreq(Lorg/apache/lucene/index/Term;)J

    move-result-wide v6

    .line 70
    .local v6, "val":J
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-nez v8, :cond_1

    .line 71
    const-wide/16 v2, -0x1

    .line 72
    goto :goto_1

    .line 74
    :cond_1
    add-long/2addr v2, v6

    goto :goto_0
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->val:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 94
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;

    .line 95
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->indexedField:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->indexedField:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queries/function/FunctionValues;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 88
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->indexedField:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1d

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TotalTermFreqValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "totaltermfreq"

    return-object v0
.end method
