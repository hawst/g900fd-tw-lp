.class Lorg/apache/lucene/queries/function/FunctionValues$4;
.super Lorg/apache/lucene/queries/function/ValueSourceScorer;
.source "FunctionValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/FunctionValues;->getRangeScorer(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/queries/function/ValueSourceScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/FunctionValues;

.field private final synthetic val$l:F

.field private final synthetic val$u:F


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;FF)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "$anonymous1"    # Lorg/apache/lucene/queries/function/FunctionValues;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/FunctionValues$4;->this$0:Lorg/apache/lucene/queries/function/FunctionValues;

    iput p4, p0, Lorg/apache/lucene/queries/function/FunctionValues$4;->val$l:F

    iput p5, p0, Lorg/apache/lucene/queries/function/FunctionValues$4;->val$u:F

    .line 184
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/queries/function/ValueSourceScorer;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-void
.end method


# virtual methods
.method public matchesValue(I)Z
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 187
    iget-object v1, p0, Lorg/apache/lucene/queries/function/FunctionValues$4;->this$0:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v0

    .line 188
    .local v0, "docVal":F
    iget v1, p0, Lorg/apache/lucene/queries/function/FunctionValues$4;->val$l:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/queries/function/FunctionValues$4;->val$u:F

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
