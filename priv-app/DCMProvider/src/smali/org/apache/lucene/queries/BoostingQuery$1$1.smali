.class Lorg/apache/lucene/queries/BoostingQuery$1$1;
.super Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;
.source "BoostingQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/BoostingQuery$1;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/queries/BoostingQuery$1;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/BoostingQuery$1;Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/search/IndexSearcher;Z)V
    .locals 0
    .param p3, "$anonymous0"    # Lorg/apache/lucene/search/IndexSearcher;
    .param p4, "$anonymous1"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/BoostingQuery$1$1;->this$1:Lorg/apache/lucene/queries/BoostingQuery$1;

    .line 57
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/search/BooleanQuery$BooleanWeight;-><init>(Lorg/apache/lucene/search/BooleanQuery;Lorg/apache/lucene/search/IndexSearcher;Z)V

    return-void
.end method


# virtual methods
.method public coord(II)F
    .locals 1
    .param p1, "overlap"    # I
    .param p2, "max"    # I

    .prologue
    .line 61
    packed-switch p1, :pswitch_data_0

    .line 70
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 64
    :pswitch_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 67
    :pswitch_1
    iget-object v0, p0, Lorg/apache/lucene/queries/BoostingQuery$1$1;->this$1:Lorg/apache/lucene/queries/BoostingQuery$1;

    # getter for: Lorg/apache/lucene/queries/BoostingQuery$1;->this$0:Lorg/apache/lucene/queries/BoostingQuery;
    invoke-static {v0}, Lorg/apache/lucene/queries/BoostingQuery$1;->access$0(Lorg/apache/lucene/queries/BoostingQuery$1;)Lorg/apache/lucene/queries/BoostingQuery;

    move-result-object v0

    # getter for: Lorg/apache/lucene/queries/BoostingQuery;->boost:F
    invoke-static {v0}, Lorg/apache/lucene/queries/BoostingQuery;->access$0(Lorg/apache/lucene/queries/BoostingQuery;)F

    move-result v0

    goto :goto_0

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
