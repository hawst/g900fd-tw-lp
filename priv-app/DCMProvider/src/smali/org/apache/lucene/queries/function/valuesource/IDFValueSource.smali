.class public Lorg/apache/lucene/queries/function/valuesource/IDFValueSource;
.super Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;
.source "IDFValueSource.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "indexedField"    # Ljava/lang/String;
    .param p4, "indexedBytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 41
    return-void
.end method

.method static asTFIDF(Lorg/apache/lucene/search/similarities/Similarity;Ljava/lang/String;)Lorg/apache/lucene/search/similarities/TFIDFSimilarity;
    .locals 1
    .param p0, "sim"    # Lorg/apache/lucene/search/similarities/Similarity;
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 62
    :goto_0
    instance-of v0, p0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper;

    if-nez v0, :cond_0

    .line 65
    instance-of v0, p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    if-eqz v0, :cond_1

    .line 66
    check-cast p0, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    .line 68
    .end local p0    # "sim":Lorg/apache/lucene/search/similarities/Similarity;
    :goto_1
    return-object p0

    .line 63
    .restart local p0    # "sim":Lorg/apache/lucene/search/similarities/Similarity;
    :cond_0
    check-cast p0, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper;

    .end local p0    # "sim":Lorg/apache/lucene/search/similarities/Similarity;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/similarities/PerFieldSimilarityWrapper;->get(Ljava/lang/String;)Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object p0

    .restart local p0    # "sim":Lorg/apache/lucene/search/similarities/Similarity;
    goto :goto_0

    .line 68
    :cond_1
    const/4 p0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 8
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    const-string v4, "searcher"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/IndexSearcher;

    .line 51
    .local v2, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    invoke-virtual {v2}, Lorg/apache/lucene/search/IndexSearcher;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/queries/function/valuesource/IDFValueSource;->field:Ljava/lang/String;

    invoke-static {v4, v5}, Lorg/apache/lucene/queries/function/valuesource/IDFValueSource;->asTFIDF(Lorg/apache/lucene/search/similarities/Similarity;Ljava/lang/String;)Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    move-result-object v3

    .line 52
    .local v3, "sim":Lorg/apache/lucene/search/similarities/TFIDFSimilarity;
    if-nez v3, :cond_0

    .line 53
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    const-string v5, "requires a TFIDFSimilarity (such as DefaultSimilarity)"

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 55
    :cond_0
    invoke-virtual {v2}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v4

    new-instance v5, Lorg/apache/lucene/index/Term;

    iget-object v6, p0, Lorg/apache/lucene/queries/function/valuesource/IDFValueSource;->indexedField:Ljava/lang/String;

    iget-object v7, p0, Lorg/apache/lucene/queries/function/valuesource/IDFValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v5, v6, v7}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    .line 56
    .local v0, "docfreq":I
    int-to-long v4, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v3, v4, v5, v6, v7}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->idf(JJ)F

    move-result v1

    .line 57
    .local v1, "idf":F
    new-instance v4, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;

    float-to-double v6, v1

    invoke-direct {v4, v6, v7, p0}, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;-><init>(DLorg/apache/lucene/queries/function/ValueSource;)V

    return-object v4
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    const-string v0, "idf"

    return-object v0
.end method
