.class public Lorg/apache/lucene/queries/function/valuesource/MaxDocValueSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "MaxDocValueSource.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    const-string v0, "searcher"

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/MaxDocValueSource;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 3
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    const-string v1, "searcher"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/IndexSearcher;

    .line 51
    .local v0, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    new-instance v1, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;

    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v2

    invoke-direct {v1, v2, p0}, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;-><init>(ILorg/apache/lucene/queries/function/ValueSource;)V

    return-object v1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string v0, "maxdoc"

    return-object v0
.end method
