.class Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;
.super Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;
.source "TFValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/TFValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field atDoc:I

.field docs:Lorg/apache/lucene/index/DocsEnum;

.field lastDocRequested:I

.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/TFValueSource;

.field private final synthetic val$similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

.field private final synthetic val$terms:Lorg/apache/lucene/index/Terms;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/TFValueSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/search/similarities/TFIDFSimilarity;)V
    .locals 1
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/TFValueSource;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->val$terms:Lorg/apache/lucene/index/Terms;

    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->val$similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    .line 58
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->lastDocRequested:I

    .line 63
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->reset()V

    return-void
.end method


# virtual methods
.method public floatVal(I)F
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 113
    :try_start_0
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->lastDocRequested:I

    if-ge p1, v1, :cond_0

    .line 115
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->reset()V

    .line 117
    :cond_0
    iput p1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->lastDocRequested:I

    .line 119
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->atDoc:I

    if-ge v1, p1, :cond_1

    .line 120
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->docs:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/DocsEnum;->advance(I)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->atDoc:I

    .line 123
    :cond_1
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->atDoc:I

    if-le v1, p1, :cond_2

    .line 126
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->val$similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->tf(I)F

    move-result v1

    .line 130
    :goto_0
    return v1

    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->val$similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->docs:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->tf(I)F
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 131
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "caught exception in function "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/TFValueSource;

    invoke-virtual {v3}, Lorg/apache/lucene/queries/function/valuesource/TFValueSource;->description()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : doc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public reset()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 68
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->val$terms:Lorg/apache/lucene/index/Terms;

    if-eqz v1, :cond_2

    .line 69
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->val$terms:Lorg/apache/lucene/index/Terms;

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    .line 70
    .local v0, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/TFValueSource;

    iget-object v1, v1, Lorg/apache/lucene/queries/function/valuesource/TFValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 71
    invoke-virtual {v0, v3, v3}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->docs:Lorg/apache/lucene/index/DocsEnum;

    .line 79
    .end local v0    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->docs:Lorg/apache/lucene/index/DocsEnum;

    if-nez v1, :cond_0

    .line 80
    new-instance v1, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1$1;

    invoke-direct {v1, p0}, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;)V

    iput-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->docs:Lorg/apache/lucene/index/DocsEnum;

    .line 107
    :cond_0
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->atDoc:I

    .line 108
    return-void

    .line 73
    .restart local v0    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_1
    iput-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->docs:Lorg/apache/lucene/index/DocsEnum;

    goto :goto_0

    .line 76
    .end local v0    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_2
    iput-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/TFValueSource$1;->docs:Lorg/apache/lucene/index/DocsEnum;

    goto :goto_0
.end method
