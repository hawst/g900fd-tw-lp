.class Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;
.super Lorg/apache/lucene/queries/function/docvalues/IntDocValues;
.source "DocFreqValueSource.java"


# instance fields
.field final dval:D

.field final fval:F

.field final ival:I

.field final lval:J

.field final parent:Lorg/apache/lucene/queries/function/ValueSource;

.field final sval:Ljava/lang/String;


# direct methods
.method constructor <init>(ILorg/apache/lucene/queries/function/ValueSource;)V
    .locals 2
    .param p1, "val"    # I
    .param p2, "parent"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 42
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/IntDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 43
    iput p1, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->ival:I

    .line 44
    int-to-float v0, p1

    iput v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->fval:F

    .line 45
    int-to-double v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->dval:D

    .line 46
    int-to-long v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->lval:J

    .line 47
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->sval:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->parent:Lorg/apache/lucene/queries/function/ValueSource;

    .line 49
    return-void
.end method


# virtual methods
.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 65
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->dval:D

    return-wide v0
.end method

.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 53
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->fval:F

    return v0
.end method

.method public intVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 57
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->ival:I

    return v0
.end method

.method public longVal(I)J
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 61
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->lval:J

    return-wide v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->sval:Ljava/lang/String;

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->parent:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;->sval:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
