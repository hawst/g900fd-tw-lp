.class Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction$1;
.super Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;
.source "SimpleBoolFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;

.field private final synthetic val$vals:Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 50
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public boolVal(I)Z
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->func(ILorg/apache/lucene/queries/function/FunctionValues;)Z

    move-result v0

    return v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleBoolFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
