.class public Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;
.super Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;
.source "ShortFieldSource.java"


# instance fields
.field final parser:Lorg/apache/lucene/search/FieldCache$ShortParser;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/apache/lucene/search/FieldCache$ShortParser;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;-><init>(Ljava/lang/String;)V

    .line 42
    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    .line 43
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "short("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;

    if-eq v2, v3, :cond_1

    .line 103
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 102
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;

    .line 103
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;
    invoke-super {p0, v0}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    if-nez v2, :cond_2

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    if-nez v2, :cond_0

    .line 103
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 105
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    goto :goto_1
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 6
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->cache:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->field:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v4, v5}, Lorg/apache/lucene/search/FieldCache;->getShorts(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Z)Lorg/apache/lucene/search/FieldCache$Shorts;

    move-result-object v0

    .line 54
    .local v0, "arr":Lorg/apache/lucene/search/FieldCache$Shorts;
    new-instance v1, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;Lorg/apache/lucene/search/FieldCache$Shorts;)V

    return-object v1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 110
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    if-nez v1, :cond_0

    const-class v1, Ljava/lang/Short;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 111
    .local v0, "h":I
    :goto_0
    invoke-super {p0}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    return v0

    .line 110
    .end local v0    # "h":I
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ShortParser;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
