.class public Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;
.super Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;
.source "DoubleFieldSource.java"


# instance fields
.field protected final parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/apache/lucene/search/FieldCache$DoubleParser;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;-><init>(Ljava/lang/String;)V

    .line 51
    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    .line 52
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "double("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 157
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;

    if-eq v2, v3, :cond_1

    .line 159
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 158
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;

    .line 159
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;
    invoke-super {p0, v0}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 160
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    if-nez v2, :cond_2

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    if-nez v2, :cond_0

    .line 159
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 161
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    goto :goto_1
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 7
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->cache:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->field:Ljava/lang/String;

    iget-object v5, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    const/4 v6, 0x1

    invoke-interface {v2, v3, v4, v5, v6}, Lorg/apache/lucene/search/FieldCache;->getDoubles(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)Lorg/apache/lucene/search/FieldCache$Doubles;

    move-result-object v0

    .line 62
    .local v0, "arr":Lorg/apache/lucene/search/FieldCache$Doubles;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->cache:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->field:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lorg/apache/lucene/search/FieldCache;->getDocsWithField(Lorg/apache/lucene/index/AtomicReader;Ljava/lang/String;)Lorg/apache/lucene/util/Bits;

    move-result-object v1

    .line 63
    .local v1, "valid":Lorg/apache/lucene/util/Bits;
    new-instance v2, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;

    invoke-direct {v2, p0, p0, v0, v1}, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/search/FieldCache$Doubles;Lorg/apache/lucene/util/Bits;)V

    return-object v2
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 166
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    if-nez v1, :cond_0

    const-class v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 167
    .local v0, "h":I
    :goto_0
    invoke-super {p0}, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    return v0

    .line 166
    .end local v0    # "h":I
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
