.class Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction$1;
.super Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;
.source "MultiFloatFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;

.field private final synthetic val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;[Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction$1;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    .line 68
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public floatVal(I)F
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction$1;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->func(I[Lorg/apache/lucene/queries/function/FunctionValues;)F

    move-result v0

    return v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 7
    .param p1, "doc"    # I

    .prologue
    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;

    invoke-virtual {v3}, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 77
    const/4 v0, 0x1

    .line 78
    .local v0, "firstTime":Z
    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction$1;->val$valsArr:[Lorg/apache/lucene/queries/function/FunctionValues;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 86
    const/16 v3, 0x29

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 87
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 78
    :cond_0
    aget-object v2, v4, v3

    .line 79
    .local v2, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    if-eqz v0, :cond_1

    .line 80
    const/4 v0, 0x0

    .line 84
    :goto_1
    invoke-virtual {v2, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 82
    :cond_1
    const/16 v6, 0x2c

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
