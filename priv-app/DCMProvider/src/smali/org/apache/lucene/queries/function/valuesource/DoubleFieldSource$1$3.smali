.class Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$3;
.super Lorg/apache/lucene/queries/function/ValueSourceScorer;
.source "DoubleFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;->getRangeScorer(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/queries/function/ValueSourceScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;

.field private final synthetic val$l:D

.field private final synthetic val$u:D


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;DD)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "$anonymous1"    # Lorg/apache/lucene/queries/function/FunctionValues;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$3;->this$1:Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;

    iput-wide p4, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$3;->val$l:D

    iput-wide p6, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$3;->val$u:D

    .line 113
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/queries/function/ValueSourceScorer;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-void
.end method


# virtual methods
.method public matchesValue(I)Z
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 116
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$3;->this$1:Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;->doubleVal(I)D

    move-result-wide v0

    .line 117
    .local v0, "docVal":D
    iget-wide v2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$3;->val$l:D

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    iget-wide v2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$3;->val$u:D

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
