.class Lorg/apache/lucene/queries/function/docvalues/BoolDocValues$1;
.super Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.source "BoolDocValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mval:Lorg/apache/lucene/util/mutable/MutableValueBool;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/docvalues/BoolDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;

    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;-><init>()V

    .line 87
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueBool;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueBool;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/BoolDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueBool;

    return-void
.end method


# virtual methods
.method public fillValue(I)V
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/BoolDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueBool;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/BoolDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;->boolVal(I)Z

    move-result v1

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueBool;->value:Z

    .line 97
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/BoolDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueBool;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/BoolDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/docvalues/BoolDocValues;->exists(I)Z

    move-result v1

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueBool;->exists:Z

    .line 98
    return-void
.end method

.method public getValue()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/BoolDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueBool;

    return-object v0
.end method
