.class public abstract Lorg/apache/lucene/queries/function/ValueSource;
.super Ljava/lang/Object;
.source "ValueSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparator;,
        Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparatorSource;,
        Lorg/apache/lucene/queries/function/ValueSource$ValueSourceSortField;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newContext(Lorg/apache/lucene/search/IndexSearcher;)Ljava/util/Map;
    .locals 2
    .param p0, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;

    .prologue
    .line 74
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    .line 75
    .local v0, "context":Ljava/util/Map;
    const-string v1, "searcher"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    return-object v0
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 0
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    return-void
.end method

.method public abstract description()Ljava/lang/String;
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.end method

.method public getSortField(Z)Lorg/apache/lucene/search/SortField;
    .locals 1
    .param p1, "reverse"    # Z

    .prologue
    .line 94
    new-instance v0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceSortField;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceSortField;-><init>(Lorg/apache/lucene/queries/function/ValueSource;Z)V

    return-object v0
.end method

.method public abstract getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract hashCode()I
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
