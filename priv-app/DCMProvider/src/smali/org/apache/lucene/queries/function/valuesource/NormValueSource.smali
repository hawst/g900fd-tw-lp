.class public Lorg/apache/lucene/queries/function/valuesource/NormValueSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "NormValueSource.java"


# instance fields
.field protected final field:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/NormValueSource;->field:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    const-string v0, "searcher"

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/NormValueSource;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/NormValueSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 81
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    .line 82
    .end local p1    # "o":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/NormValueSource;->field:Ljava/lang/String;

    check-cast p1, Lorg/apache/lucene/queries/function/valuesource/NormValueSource;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/lucene/queries/function/valuesource/NormValueSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 6
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    const-string v3, "searcher"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/IndexSearcher;

    .line 61
    .local v1, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    invoke-virtual {v1}, Lorg/apache/lucene/search/IndexSearcher;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/NormValueSource;->field:Ljava/lang/String;

    invoke-static {v3, v4}, Lorg/apache/lucene/queries/function/valuesource/IDFValueSource;->asTFIDF(Lorg/apache/lucene/search/similarities/Similarity;Ljava/lang/String;)Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    move-result-object v2

    .line 62
    .local v2, "similarity":Lorg/apache/lucene/search/similarities/TFIDFSimilarity;
    if-nez v2, :cond_0

    .line 63
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "requires a TFIDFSimilarity (such as DefaultSimilarity)"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 65
    :cond_0
    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/NormValueSource;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    .line 67
    .local v0, "norms":Lorg/apache/lucene/index/NumericDocValues;
    if-nez v0, :cond_1

    .line 68
    new-instance v3, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;

    const-wide/16 v4, 0x0

    invoke-direct {v3, v4, v5, p0}, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;-><init>(DLorg/apache/lucene/queries/function/ValueSource;)V

    .line 71
    :goto_0
    return-object v3

    :cond_1
    new-instance v3, Lorg/apache/lucene/queries/function/valuesource/NormValueSource$1;

    invoke-direct {v3, p0, p0, v2, v0}, Lorg/apache/lucene/queries/function/valuesource/NormValueSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/NormValueSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/search/similarities/TFIDFSimilarity;Lorg/apache/lucene/index/NumericDocValues;)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/NormValueSource;->field:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    const-string v0, "norm"

    return-object v0
.end method
