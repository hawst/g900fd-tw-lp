.class Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$1;
.super Lorg/apache/lucene/queries/function/FunctionValues;
.source "BytesRefFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;

.field private final synthetic val$binaryValues:Lorg/apache/lucene/index/BinaryDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;Lorg/apache/lucene/index/BinaryDocValues;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;

    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$1;->val$binaryValues:Lorg/apache/lucene/index/BinaryDocValues;

    .line 49
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues;-><init>()V

    return-void
.end method


# virtual methods
.method public bytesVal(ILorg/apache/lucene/util/BytesRef;)Z
    .locals 1
    .param p1, "doc"    # I
    .param p2, "target"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$1;->val$binaryValues:Lorg/apache/lucene/index/BinaryDocValues;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 59
    iget v0, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public exists(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 53
    const/4 v0, 0x1

    return v0
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$1;->strVal(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 63
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 64
    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$1;->bytesVal(ILorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v1

    .line 64
    :goto_0
    return-object v1

    .line 66
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$1;->strVal(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
