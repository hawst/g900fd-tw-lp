.class Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;
.super Lorg/apache/lucene/queries/function/docvalues/DoubleDocValues;
.source "DocFreqValueSource.java"


# instance fields
.field final dval:D

.field final fval:F

.field final ival:I

.field final lval:J

.field final parent:Lorg/apache/lucene/queries/function/ValueSource;

.field final sval:Ljava/lang/String;


# direct methods
.method constructor <init>(DLorg/apache/lucene/queries/function/ValueSource;)V
    .locals 3
    .param p1, "val"    # D
    .param p3, "parent"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 86
    invoke-direct {p0, p3}, Lorg/apache/lucene/queries/function/docvalues/DoubleDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 87
    double-to-int v0, p1

    iput v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->ival:I

    .line 88
    double-to-float v0, p1

    iput v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->fval:F

    .line 89
    iput-wide p1, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->dval:D

    .line 90
    double-to-long v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->lval:J

    .line 91
    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->sval:Ljava/lang/String;

    .line 92
    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->parent:Lorg/apache/lucene/queries/function/ValueSource;

    .line 93
    return-void
.end method


# virtual methods
.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 109
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->dval:D

    return-wide v0
.end method

.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 97
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->fval:F

    return v0
.end method

.method public intVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->ival:I

    return v0
.end method

.method public longVal(I)J
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 105
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->lval:J

    return-wide v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->sval:Ljava/lang/String;

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->parent:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ConstDoubleDocValues;->sval:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
