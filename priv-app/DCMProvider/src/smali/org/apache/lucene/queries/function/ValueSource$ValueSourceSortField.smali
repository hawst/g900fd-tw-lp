.class Lorg/apache/lucene/queries/function/ValueSource$ValueSourceSortField;
.super Lorg/apache/lucene/search/SortField;
.source "ValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/function/ValueSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ValueSourceSortField"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;Z)V
    .locals 2
    .param p2, "reverse"    # Z

    .prologue
    .line 98
    iput-object p1, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceSortField;->this$0:Lorg/apache/lucene/queries/function/ValueSource;

    .line 99
    invoke-virtual {p1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/search/SortField$Type;->REWRITEABLE:Lorg/apache/lucene/search/SortField$Type;

    invoke-direct {p0, v0, v1, p2}, Lorg/apache/lucene/search/SortField;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/SortField$Type;Z)V

    .line 100
    return-void
.end method


# virtual methods
.method public rewrite(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/SortField;
    .locals 5
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-static {p1}, Lorg/apache/lucene/queries/function/ValueSource;->newContext(Lorg/apache/lucene/search/IndexSearcher;)Ljava/util/Map;

    move-result-object v0

    .line 105
    .local v0, "context":Ljava/util/Map;
    iget-object v1, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceSortField;->this$0:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1, v0, p1}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 106
    new-instance v1, Lorg/apache/lucene/search/SortField;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceSortField;->getField()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparatorSource;

    iget-object v4, p0, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceSortField;->this$0:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-direct {v3, v4, v0}, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceComparatorSource;-><init>(Lorg/apache/lucene/queries/function/ValueSource;Ljava/util/Map;)V

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/ValueSource$ValueSourceSortField;->getReverse()Z

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/search/SortField;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldComparatorSource;Z)V

    return-object v1
.end method
