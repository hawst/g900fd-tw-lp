.class Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "CustomScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/CustomScoreQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomScorer"
.end annotation


# instance fields
.field private final provider:Lorg/apache/lucene/queries/CustomScoreProvider;

.field private final qWeight:F

.field private final subQueryScorer:Lorg/apache/lucene/search/Scorer;

.field final synthetic this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

.field private final vScores:[F

.field private final valSrcScorers:[Lorg/apache/lucene/search/Scorer;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/queries/CustomScoreQuery;Lorg/apache/lucene/queries/CustomScoreProvider;Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;FLorg/apache/lucene/search/Scorer;[Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p2, "provider"    # Lorg/apache/lucene/queries/CustomScoreProvider;
    .param p3, "w"    # Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;
    .param p4, "qWeight"    # F
    .param p5, "subQueryScorer"    # Lorg/apache/lucene/search/Scorer;
    .param p6, "valSrcScorers"    # [Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 297
    iput-object p1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    .line 298
    invoke-direct {p0, p3}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 299
    iput p4, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->qWeight:F

    .line 300
    iput-object p5, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    .line 301
    iput-object p6, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    .line 302
    array-length v0, p6

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->vScores:[F

    .line 303
    iput-object p2, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->provider:Lorg/apache/lucene/queries/CustomScoreProvider;

    .line 304
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/queries/CustomScoreQuery;Lorg/apache/lucene/queries/CustomScoreProvider;Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;FLorg/apache/lucene/search/Scorer;[Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;)V
    .locals 0

    .prologue
    .line 296
    invoke-direct/range {p0 .. p6}, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;-><init>(Lorg/apache/lucene/queries/CustomScoreQuery;Lorg/apache/lucene/queries/CustomScoreProvider;Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;FLorg/apache/lucene/search/Scorer;[Lorg/apache/lucene/search/Scorer;)V

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 5
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 343
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    .line 344
    .local v0, "doc":I
    const v2, 0x7fffffff

    if-eq v0, v2, :cond_0

    .line 345
    iget-object v3, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_1

    .line 349
    :cond_0
    return v0

    .line 345
    :cond_1
    aget-object v1, v3, v2

    .line 346
    .local v1, "valSrcScorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    .line 345
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 354
    iget-object v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 333
    iget-object v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->freq()I

    move-result v0

    return v0
.end method

.method public getChildren()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer$ChildScorer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 338
    new-instance v0, Lorg/apache/lucene/search/Scorer$ChildScorer;

    iget-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    const-string v2, "CUSTOM"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Scorer$ChildScorer;-><init>(Lorg/apache/lucene/search/Scorer;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public nextDoc()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 308
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    .line 309
    .local v0, "doc":I
    const v2, 0x7fffffff

    if-eq v0, v2, :cond_0

    .line 310
    iget-object v3, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_1

    .line 314
    :cond_0
    return v0

    .line 310
    :cond_1
    aget-object v1, v3, v2

    .line 311
    .local v1, "valSrcScorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    .line 310
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public score()F
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 325
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 328
    iget v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->qWeight:F

    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->provider:Lorg/apache/lucene/queries/CustomScoreProvider;

    iget-object v3, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->vScores:[F

    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/lucene/queries/CustomScoreProvider;->customScore(IF[F)F

    move-result v2

    mul-float/2addr v1, v2

    return v1

    .line 326
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->vScores:[F

    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    aput v2, v1, v0

    .line 325
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
