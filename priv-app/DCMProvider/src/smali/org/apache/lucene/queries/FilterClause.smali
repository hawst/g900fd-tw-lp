.class public final Lorg/apache/lucene/queries/FilterClause;
.super Ljava/lang/Object;
.source "FilterClause.java"


# instance fields
.field private final filter:Lorg/apache/lucene/search/Filter;

.field private final occur:Lorg/apache/lucene/search/BooleanClause$Occur;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    .locals 0
    .param p1, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p2, "occur"    # Lorg/apache/lucene/search/BooleanClause$Occur;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p2, p0, Lorg/apache/lucene/queries/FilterClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/queries/FilterClause;->filter:Lorg/apache/lucene/search/Filter;

    .line 43
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    if-ne p1, p0, :cond_1

    .line 68
    :cond_0
    :goto_0
    return v1

    .line 65
    :cond_1
    if-eqz p1, :cond_2

    instance-of v3, p1, Lorg/apache/lucene/queries/FilterClause;

    if-nez v3, :cond_3

    :cond_2
    move v1, v2

    .line 66
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 67
    check-cast v0, Lorg/apache/lucene/queries/FilterClause;

    .line 68
    .local v0, "other":Lorg/apache/lucene/queries/FilterClause;
    iget-object v3, p0, Lorg/apache/lucene/queries/FilterClause;->filter:Lorg/apache/lucene/search/Filter;

    iget-object v4, v0, Lorg/apache/lucene/queries/FilterClause;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 69
    iget-object v3, p0, Lorg/apache/lucene/queries/FilterClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    iget-object v4, v0, Lorg/apache/lucene/queries/FilterClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    .line 68
    goto :goto_0
.end method

.method public getFilter()Lorg/apache/lucene/search/Filter;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/lucene/queries/FilterClause;->filter:Lorg/apache/lucene/search/Filter;

    return-object v0
.end method

.method public getOccur()Lorg/apache/lucene/search/BooleanClause$Occur;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/queries/FilterClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/queries/FilterClause;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/queries/FilterClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1}, Lorg/apache/lucene/search/BooleanClause$Occur;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/FilterClause;->occur:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1}, Lorg/apache/lucene/search/BooleanClause$Occur;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/FilterClause;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
