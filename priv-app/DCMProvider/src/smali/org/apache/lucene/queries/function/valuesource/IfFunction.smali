.class public Lorg/apache/lucene/queries/function/valuesource/IfFunction;
.super Lorg/apache/lucene/queries/function/valuesource/BoolFunction;
.source "IfFunction.java"


# instance fields
.field private final falseSource:Lorg/apache/lucene/queries/function/ValueSource;

.field private final ifSource:Lorg/apache/lucene/queries/function/ValueSource;

.field private final trueSource:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "ifSource"    # Lorg/apache/lucene/queries/function/ValueSource;
    .param p2, "trueSource"    # Lorg/apache/lucene/queries/function/ValueSource;
    .param p3, "falseSource"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/valuesource/BoolFunction;-><init>()V

    .line 44
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->ifSource:Lorg/apache/lucene/queries/function/ValueSource;

    .line 45
    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->trueSource:Lorg/apache/lucene/queries/function/ValueSource;

    .line 46
    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->falseSource:Lorg/apache/lucene/queries/function/ValueSource;

    .line 47
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->ifSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 151
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->trueSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 152
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->falseSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 153
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x2c

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "if("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->ifSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->trueSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->falseSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 141
    instance-of v2, p1, Lorg/apache/lucene/queries/function/valuesource/IfFunction;

    if-nez v2, :cond_1

    .line 143
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 142
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;

    .line 143
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/IfFunction;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->ifSource:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->ifSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 144
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->trueSource:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->trueSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->falseSource:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->falseSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 143
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 4
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->ifSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v3, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v1

    .line 52
    .local v1, "ifVals":Lorg/apache/lucene/queries/function/FunctionValues;
    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->trueSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v3, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v2

    .line 53
    .local v2, "trueVals":Lorg/apache/lucene/queries/function/FunctionValues;
    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->falseSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v3, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    .line 55
    .local v0, "falseVals":Lorg/apache/lucene/queries/function/FunctionValues;
    new-instance v3, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;

    invoke-direct {v3, p0, v1, v2, v0}, Lorg/apache/lucene/queries/function/valuesource/IfFunction$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/IfFunction;Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v3
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 133
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->ifSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v0

    .line 134
    .local v0, "h":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->trueSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 135
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/IfFunction;->falseSource:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 136
    return v0
.end method
