.class public Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "SumTotalTermFreqValueSource.java"


# instance fields
.field protected final indexedField:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "indexedField"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource;->indexedField:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 12
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    const-wide/16 v2, 0x0

    .line 63
    .local v2, "sumTotalTermFreq":J
    invoke-virtual {p2}, Lorg/apache/lucene/search/IndexSearcher;->getTopReaderContext()Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/index/IndexReaderContext;->leaves()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 76
    :goto_1
    move-wide v6, v2

    .line 77
    .local v6, "ttf":J
    new-instance v5, Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource$1;

    invoke-direct {v5, p0, p0, v6, v7}, Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource;Lorg/apache/lucene/queries/function/ValueSource;J)V

    invoke-interface {p1, p0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    return-void

    .line 63
    .end local v6    # "ttf":J
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 64
    .local v1, "readerContext":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 65
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    if-eqz v0, :cond_0

    .line 66
    iget-object v10, p0, Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource;->indexedField:Ljava/lang/String;

    invoke-virtual {v0, v10}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v4

    .line 67
    .local v4, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v4, :cond_0

    .line 68
    invoke-virtual {v4}, Lorg/apache/lucene/index/Terms;->getSumTotalTermFreq()J

    move-result-wide v8

    .line 69
    .local v8, "v":J
    const-wide/16 v10, -0x1

    cmp-long v10, v8, v10

    if-nez v10, :cond_2

    .line 70
    const-wide/16 v2, -0x1

    .line 71
    goto :goto_1

    .line 73
    :cond_2
    add-long/2addr v2, v8

    goto :goto_0
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource;->indexedField:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 92
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    .line 94
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 93
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource;

    .line 94
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource;
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource;->indexedField:Ljava/lang/String;

    iget-object v2, v0, Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource;->indexedField:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queries/function/FunctionValues;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SumTotalTermFreqValueSource;->indexedField:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, "sumtotaltermfreq"

    return-object v0
.end method
