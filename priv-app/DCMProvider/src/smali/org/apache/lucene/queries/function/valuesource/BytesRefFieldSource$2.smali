.class Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$2;
.super Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;
.source "BytesRefFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/index/AtomicReaderContext;Ljava/lang/String;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;
    .param p3, "$anonymous1"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p4, "$anonymous2"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$2;->this$0:Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;

    .line 80
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/index/AtomicReaderContext;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public objectVal(I)Ljava/lang/Object;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$2;->strVal(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$2;->this$0:Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/BytesRefFieldSource$2;->strVal(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected toTerm(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "readableValue"    # Ljava/lang/String;

    .prologue
    .line 84
    return-object p1
.end method
