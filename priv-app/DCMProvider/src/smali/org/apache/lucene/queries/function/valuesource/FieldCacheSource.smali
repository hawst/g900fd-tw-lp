.class public abstract Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "FieldCacheSource.java"


# instance fields
.field protected final cache:Lorg/apache/lucene/search/FieldCache;

.field protected final field:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 31
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->cache:Lorg/apache/lucene/search/FieldCache;

    .line 34
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->field:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->field:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 52
    instance-of v2, p1, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;

    if-nez v2, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 53
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;

    .line 54
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->field:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->field:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->cache:Lorg/apache/lucene/search/FieldCache;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->cache:Lorg/apache/lucene/search/FieldCache;

    if-ne v2, v3, :cond_0

    .line 54
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldCache()Lorg/apache/lucene/search/FieldCache;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->cache:Lorg/apache/lucene/search/FieldCache;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->cache:Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/FieldCacheSource;->field:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
