.class Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$1;
.super Lorg/apache/lucene/queries/function/ValueSourceScorer;
.source "LongFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;->getRangeScorer(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/queries/function/ValueSourceScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;

.field private final synthetic val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

.field private final synthetic val$ll:J

.field private final synthetic val$uu:J


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/search/FieldCache$Longs;JJ)V
    .locals 1
    .param p2, "$anonymous0"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "$anonymous1"    # Lorg/apache/lucene/queries/function/FunctionValues;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$1;->this$1:Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1;

    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

    iput-wide p5, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$1;->val$ll:J

    iput-wide p7, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$1;->val$uu:J

    .line 120
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/queries/function/ValueSourceScorer;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-void
.end method


# virtual methods
.method public matchesValue(I)Z
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 123
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Longs;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/FieldCache$Longs;->get(I)J

    move-result-wide v0

    .line 126
    .local v0, "val":J
    iget-wide v2, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$1;->val$ll:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    iget-wide v2, p0, Lorg/apache/lucene/queries/function/valuesource/LongFieldSource$1$1;->val$uu:J

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
