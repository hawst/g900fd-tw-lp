.class public Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "ReciprocalFloatFunction.java"


# instance fields
.field protected final a:F

.field protected final b:F

.field protected final m:F

.field protected final source:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;FFF)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/queries/function/ValueSource;
    .param p2, "m"    # F
    .param p3, "a"    # F
    .param p4, "b"    # F

    .prologue
    .line 56
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 57
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    .line 58
    iput p2, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->m:F

    .line 59
    iput p3, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->a:F

    .line 60
    iput p4, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->b:F

    .line 61
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 83
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->a:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 88
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->m:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*float("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 89
    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 101
    const-class v2, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 103
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 102
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;

    .line 103
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->m:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->m:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 104
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->a:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->a:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 105
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->b:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 106
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 103
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 2
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    .line 66
    .local v0, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    new-instance v1, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction$1;

    invoke-direct {v1, p0, p0, v0}, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 94
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->a:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->m:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    .line 95
    .local v0, "h":I
    shl-int/lit8 v1, v0, 0xd

    ushr-int/lit8 v2, v0, 0x14

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 96
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v1, v0

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/ReciprocalFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    return v1
.end method
