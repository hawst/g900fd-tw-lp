.class public Lorg/apache/lucene/queries/function/valuesource/MaxFloatFunction;
.super Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;
.source "MaxFloatFunction.java"


# direct methods
.method public constructor <init>([Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "sources"    # [Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/MultiFloatFunction;-><init>([Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected func(I[Lorg/apache/lucene/queries/function/FunctionValues;)F
    .locals 6
    .param p1, "doc"    # I
    .param p2, "valsArr"    # [Lorg/apache/lucene/queries/function/FunctionValues;

    .prologue
    .line 38
    const/4 v0, 0x1

    .line 39
    .local v0, "first":Z
    const/4 v1, 0x0

    .line 40
    .local v1, "val":F
    array-length v4, p2

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v4, :cond_0

    .line 48
    return v1

    .line 40
    :cond_0
    aget-object v2, p2, v3

    .line 41
    .local v2, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    if-eqz v0, :cond_1

    .line 42
    const/4 v0, 0x0

    .line 43
    invoke-virtual {v2, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v1

    .line 40
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 45
    :cond_1
    invoke-virtual {v2, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v5

    invoke-static {v5, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto :goto_1
.end method

.method protected name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const-string v0, "max"

    return-object v0
.end method
