.class final Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
.super Ljava/lang/Object;
.source "TermsFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/TermsFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TermsAndField"
.end annotation


# instance fields
.field final end:I

.field final field:Ljava/lang/String;

.field final start:I


# direct methods
.method constructor <init>(IILjava/lang/String;)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "field"    # Ljava/lang/String;

    .prologue
    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282
    iput p1, p0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->start:I

    .line 283
    iput p2, p0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->end:I

    .line 284
    iput-object p3, p0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->field:Ljava/lang/String;

    .line 285
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 299
    if-ne p0, p1, :cond_1

    .line 308
    :cond_0
    :goto_0
    return v1

    .line 300
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    .line 301
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 302
    check-cast v0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;

    .line 303
    .local v0, "other":Lorg/apache/lucene/queries/TermsFilter$TermsAndField;
    iget-object v3, p0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->field:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 304
    iget-object v3, v0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->field:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 305
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->field:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 306
    :cond_5
    iget v3, p0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->end:I

    iget v4, v0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->end:I

    if-eq v3, v4, :cond_6

    move v1, v2

    goto :goto_0

    .line 307
    :cond_6
    iget v3, p0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->start:I

    iget v4, v0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->start:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 289
    const/16 v0, 0x1f

    .line 290
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 291
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->field:Ljava/lang/String;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 292
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->end:I

    add-int v1, v2, v3

    .line 293
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->start:I

    add-int v1, v2, v3

    .line 294
    return v1

    .line 291
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queries/TermsFilter$TermsAndField;->field:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0
.end method
