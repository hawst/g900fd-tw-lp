.class Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;
.super Lorg/apache/lucene/queries/function/FunctionValues;
.source "ShortFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;

.field private final synthetic val$arr:Lorg/apache/lucene/search/FieldCache$Shorts;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;Lorg/apache/lucene/search/FieldCache$Shorts;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;

    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Shorts;

    .line 54
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues;-><init>()V

    return-void
.end method


# virtual methods
.method public byteVal(I)B
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Shorts;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Shorts;->get(I)S

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Shorts;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Shorts;->get(I)S

    move-result v0

    int-to-double v0, v0

    return-wide v0
.end method

.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Shorts;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Shorts;->get(I)S

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public intVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Shorts;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Shorts;->get(I)S

    move-result v0

    return v0
.end method

.method public longVal(I)J
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Shorts;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Shorts;->get(I)S

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public shortVal(I)S
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Shorts;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Shorts;->get(I)S

    move-result v0

    return v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Shorts;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Shorts;->get(I)S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/ShortFieldSource$1;->shortVal(I)S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
