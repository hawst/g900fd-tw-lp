.class public abstract Lorg/apache/lucene/queries/function/valuesource/SingleFunction;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "SingleFunction.java"


# instance fields
.field protected final source:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 32
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    .line 33
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 58
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 50
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;

    .line 51
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/SingleFunction;
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 51
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected abstract name()Ljava/lang/String;
.end method
