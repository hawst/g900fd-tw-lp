.class public abstract Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction;
.super Lorg/apache/lucene/queries/function/valuesource/SingleFunction;
.source "SimpleFloatFunction.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/SingleFunction;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected abstract func(ILorg/apache/lucene/queries/function/FunctionValues;)F
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 2
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    .line 40
    .local v0, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    new-instance v1, Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction$1;

    invoke-direct {v1, p0, p0, v0}, Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v1
.end method
