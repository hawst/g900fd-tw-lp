.class public Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;
.super Lorg/apache/lucene/queries/function/valuesource/ConstNumberSource;
.source "ConstValueSource.java"


# instance fields
.field final constant:F

.field private final dv:D


# direct methods
.method public constructor <init>(F)V
    .locals 2
    .param p1, "constant"    # F

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/valuesource/ConstNumberSource;-><init>()V

    .line 35
    iput p1, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    .line 36
    float-to-double v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->dv:D

    .line 37
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;)D
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->dv:D

    return-wide v0
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "const("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 85
    instance-of v2, p1, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;

    if-nez v2, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 86
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;

    .line 87
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getBool()Z
    .locals 2

    .prologue
    .line 117
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDouble()D
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->dv:D

    return-wide v0
.end method

.method public getFloat()F
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    return v0
.end method

.method public getInt()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    float-to-int v0, v0

    return v0
.end method

.method public getLong()J
    .locals 2

    .prologue
    .line 97
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    float-to-long v0, v0

    return-wide v0
.end method

.method public getNumber()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource$1;

    invoke-direct {v0, p0, p0}, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;Lorg/apache/lucene/queries/function/ValueSource;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    return v0
.end method
