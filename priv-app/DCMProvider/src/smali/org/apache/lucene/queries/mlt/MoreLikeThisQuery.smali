.class public Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;
.super Lorg/apache/lucene/search/Query;
.source "MoreLikeThisQuery.java"


# instance fields
.field private analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private final fieldName:Ljava/lang/String;

.field private likeText:Ljava/lang/String;

.field private maxQueryTerms:I

.field private minDocFreq:I

.field private minTermFrequency:I

.field private moreLikeFields:[Ljava/lang/String;

.field private percentTermsToMatch:F

.field private stopWords:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;[Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;Ljava/lang/String;)V
    .locals 1
    .param p1, "likeText"    # Ljava/lang/String;
    .param p2, "moreLikeFields"    # [Ljava/lang/String;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p4, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 46
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->percentTermsToMatch:F

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minTermFrequency:I

    .line 48
    const/4 v0, 0x5

    iput v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->maxQueryTerms:I

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->stopWords:Ljava/util/Set;

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minDocFreq:I

    .line 56
    iput-object p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->likeText:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->moreLikeFields:[Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 59
    iput-object p4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->fieldName:Ljava/lang/String;

    .line 60
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 171
    if-ne p0, p1, :cond_1

    .line 193
    :cond_0
    :goto_0
    return v1

    .line 172
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    .line 173
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 174
    check-cast v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;

    .line 175
    .local v0, "other":Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    if-nez v3, :cond_4

    .line 176
    iget-object v3, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    if-eqz v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 177
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    iget-object v4, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    .line 178
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->fieldName:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 179
    iget-object v3, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->fieldName:Ljava/lang/String;

    if-eqz v3, :cond_7

    move v1, v2

    goto :goto_0

    .line 180
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->fieldName:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->fieldName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    goto :goto_0

    .line 181
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->likeText:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 182
    iget-object v3, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->likeText:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v1, v2

    goto :goto_0

    .line 183
    :cond_8
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->likeText:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->likeText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    goto :goto_0

    .line 184
    :cond_9
    iget v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->maxQueryTerms:I

    iget v4, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->maxQueryTerms:I

    if-eq v3, v4, :cond_a

    move v1, v2

    goto :goto_0

    .line 185
    :cond_a
    iget v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minDocFreq:I

    iget v4, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minDocFreq:I

    if-eq v3, v4, :cond_b

    move v1, v2

    goto :goto_0

    .line 186
    :cond_b
    iget v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minTermFrequency:I

    iget v4, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minTermFrequency:I

    if-eq v3, v4, :cond_c

    move v1, v2

    goto :goto_0

    .line 187
    :cond_c
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->moreLikeFields:[Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->moreLikeFields:[Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    goto :goto_0

    .line 188
    :cond_d
    iget v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->percentTermsToMatch:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 189
    iget v4, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->percentTermsToMatch:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 188
    if-eq v3, v4, :cond_e

    move v1, v2

    .line 189
    goto/16 :goto_0

    .line 190
    :cond_e
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->stopWords:Ljava/util/Set;

    if-nez v3, :cond_f

    .line 191
    iget-object v3, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->stopWords:Ljava/util/Set;

    if-eqz v3, :cond_0

    move v1, v2

    goto/16 :goto_0

    .line 192
    :cond_f
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->stopWords:Ljava/util/Set;

    iget-object v4, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->stopWords:Ljava/util/Set;

    invoke-interface {v3, v4}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto/16 :goto_0
.end method

.method public getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method public getLikeText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->likeText:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxQueryTerms()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->maxQueryTerms:I

    return v0
.end method

.method public getMinDocFreq()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minDocFreq:I

    return v0
.end method

.method public getMinTermFrequency()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minTermFrequency:I

    return v0
.end method

.method public getMoreLikeFields()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->moreLikeFields:[Ljava/lang/String;

    return-object v0
.end method

.method public getPercentTermsToMatch()F
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->percentTermsToMatch:F

    return v0
.end method

.method public getStopWords()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->stopWords:Ljava/util/Set;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 155
    const/16 v0, 0x1f

    .line 156
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v1

    .line 157
    .local v1, "result":I
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 158
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->fieldName:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 159
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->likeText:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 160
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->maxQueryTerms:I

    add-int v1, v2, v4

    .line 161
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minDocFreq:I

    add-int v1, v2, v4

    .line 162
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minTermFrequency:I

    add-int v1, v2, v4

    .line 163
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->moreLikeFields:[Ljava/lang/String;

    invoke-static {v4}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v4

    add-int v1, v2, v4

    .line 164
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->percentTermsToMatch:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 165
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->stopWords:Ljava/util/Set;

    if-nez v4, :cond_3

    :goto_3
    add-int v1, v2, v3

    .line 166
    return v1

    .line 157
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    .line 158
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->fieldName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 159
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->likeText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 165
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->stopWords:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->hashCode()I

    move-result v3

    goto :goto_3
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    new-instance v2, Lorg/apache/lucene/queries/mlt/MoreLikeThis;

    invoke-direct {v2, p1}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    .line 66
    .local v2, "mlt":Lorg/apache/lucene/queries/mlt/MoreLikeThis;
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->moreLikeFields:[Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->setFieldNames([Ljava/lang/String;)V

    .line 67
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->setAnalyzer(Lorg/apache/lucene/analysis/Analyzer;)V

    .line 68
    iget v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minTermFrequency:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->setMinTermFreq(I)V

    .line 69
    iget v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minDocFreq:I

    if-ltz v3, :cond_0

    .line 70
    iget v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minDocFreq:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->setMinDocFreq(I)V

    .line 72
    :cond_0
    iget v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->maxQueryTerms:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->setMaxQueryTerms(I)V

    .line 73
    iget-object v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->stopWords:Ljava/util/Set;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->setStopWords(Ljava/util/Set;)V

    .line 74
    new-instance v3, Ljava/io/StringReader;

    iget-object v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->likeText:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->fieldName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->like(Ljava/io/Reader;Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanQuery;

    .line 75
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v1

    .line 77
    .local v1, "clauses":[Lorg/apache/lucene/search/BooleanClause;
    array-length v3, v1

    int-to-float v3, v3

    iget v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->percentTermsToMatch:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/BooleanQuery;->setMinimumNumberShouldMatch(I)V

    .line 78
    return-object v0
.end method

.method public setAnalyzer(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 0
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 102
    iput-object p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 103
    return-void
.end method

.method public setLikeText(Ljava/lang/String;)V
    .locals 0
    .param p1, "likeText"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->likeText:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public setMaxQueryTerms(I)V
    .locals 0
    .param p1, "maxQueryTerms"    # I

    .prologue
    .line 118
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->maxQueryTerms:I

    .line 119
    return-void
.end method

.method public setMinDocFreq(I)V
    .locals 0
    .param p1, "minDocFreq"    # I

    .prologue
    .line 150
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minDocFreq:I

    .line 151
    return-void
.end method

.method public setMinTermFrequency(I)V
    .locals 0
    .param p1, "minTermFrequency"    # I

    .prologue
    .line 126
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->minTermFrequency:I

    .line 127
    return-void
.end method

.method public setMoreLikeFields([Ljava/lang/String;)V
    .locals 0
    .param p1, "moreLikeFields"    # [Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->moreLikeFields:[Ljava/lang/String;

    .line 135
    return-void
.end method

.method public setPercentTermsToMatch(F)V
    .locals 0
    .param p1, "percentTermsToMatch"    # F

    .prologue
    .line 94
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->percentTermsToMatch:F

    .line 95
    return-void
.end method

.method public setStopWords(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    iput-object p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->stopWords:Ljava/util/Set;

    .line 143
    return-void
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "like:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThisQuery;->likeText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
