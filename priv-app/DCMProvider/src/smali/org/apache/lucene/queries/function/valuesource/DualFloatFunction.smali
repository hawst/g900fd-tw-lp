.class public abstract Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "DualFloatFunction.java"


# instance fields
.field protected final a:Lorg/apache/lucene/queries/function/ValueSource;

.field protected final b:Lorg/apache/lucene/queries/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "a"    # Lorg/apache/lucene/queries/function/ValueSource;
    .param p2, "b"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->a:Lorg/apache/lucene/queries/function/ValueSource;

    .line 43
    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->b:Lorg/apache/lucene/queries/function/ValueSource;

    .line 44
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->a:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 74
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->b:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 75
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->a:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->b:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 89
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 90
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;

    .line 91
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->a:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->a:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 92
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->b:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->b:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected abstract func(ILorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/queries/function/FunctionValues;)F
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 3
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->a:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    .line 57
    .local v0, "aVals":Lorg/apache/lucene/queries/function/FunctionValues;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->b:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v1

    .line 58
    .local v1, "bVals":Lorg/apache/lucene/queries/function/FunctionValues;
    new-instance v2, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction$1;

    invoke-direct {v2, p0, p0, v0, v1}, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v2
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 79
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->a:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v0

    .line 80
    .local v0, "h":I
    shl-int/lit8 v1, v0, 0xd

    ushr-int/lit8 v2, v0, 0x14

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 81
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->b:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    shl-int/lit8 v1, v0, 0x17

    ushr-int/lit8 v2, v0, 0xa

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 83
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/DualFloatFunction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    return v0
.end method

.method protected abstract name()Ljava/lang/String;
.end method
