.class Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource$1;
.super Lorg/apache/lucene/queries/function/docvalues/DoubleDocValues;
.source "DoubleConstValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;

    .line 48
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/DoubleDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;

    iget-wide v0, v0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->constant:D

    return-wide v0
.end method

.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;

    # getter for: Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->fv:F
    invoke-static {v0}, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->access$0(Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;)F

    move-result v0

    return v0
.end method

.method public intVal(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;

    # getter for: Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->lv:J
    invoke-static {v0}, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->access$1(Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public longVal(I)J
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;

    # getter for: Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->lv:J
    invoke-static {v0}, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->access$1(Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;)J

    move-result-wide v0

    return-wide v0
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;

    iget-wide v0, v0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->constant:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;

    iget-wide v0, v0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->constant:D

    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->description()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
