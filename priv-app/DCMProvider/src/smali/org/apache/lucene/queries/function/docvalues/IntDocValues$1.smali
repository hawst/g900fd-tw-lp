.class Lorg/apache/lucene/queries/function/docvalues/IntDocValues$1;
.super Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.source "IntDocValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/docvalues/IntDocValues;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/docvalues/IntDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/docvalues/IntDocValues;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/docvalues/IntDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/IntDocValues;

    .line 81
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;-><init>()V

    .line 82
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueInt;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueInt;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/IntDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    return-void
.end method


# virtual methods
.method public fillValue(I)V
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/IntDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/IntDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/IntDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/docvalues/IntDocValues;->intVal(I)I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->value:I

    .line 92
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/IntDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/IntDocValues$1;->this$0:Lorg/apache/lucene/queries/function/docvalues/IntDocValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/docvalues/IntDocValues;->exists(I)Z

    move-result v1

    iput-boolean v1, v0, Lorg/apache/lucene/util/mutable/MutableValueInt;->exists:Z

    .line 93
    return-void
.end method

.method public getValue()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/IntDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueInt;

    return-object v0
.end method
