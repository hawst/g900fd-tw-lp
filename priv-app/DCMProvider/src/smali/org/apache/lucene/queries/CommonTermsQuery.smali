.class public Lorg/apache/lucene/queries/CommonTermsQuery;
.super Lorg/apache/lucene/search/Query;
.source "CommonTermsQuery.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final disableCoord:Z

.field protected highFreqBoost:F

.field protected final highFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

.field protected lowFreqBoost:F

.field protected final lowFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

.field protected final maxTermFrequency:F

.field protected minNrShouldMatch:F

.field protected final terms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lorg/apache/lucene/queries/CommonTermsQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/queries/CommonTermsQuery;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/BooleanClause$Occur;F)V
    .locals 1
    .param p1, "highFreqOccur"    # Lorg/apache/lucene/search/BooleanClause$Occur;
    .param p2, "lowFreqOccur"    # Lorg/apache/lucene/search/BooleanClause$Occur;
    .param p3, "maxTermFrequency"    # F

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/queries/CommonTermsQuery;-><init>(Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/BooleanClause$Occur;FZ)V

    .line 97
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/BooleanClause$Occur;FZ)V
    .locals 2
    .param p1, "highFreqOccur"    # Lorg/apache/lucene/search/BooleanClause$Occur;
    .param p2, "lowFreqOccur"    # Lorg/apache/lucene/search/BooleanClause$Occur;
    .param p3, "maxTermFrequency"    # F
    .param p4, "disableCoord"    # Z

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 117
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    .line 75
    iput v1, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqBoost:F

    .line 76
    iput v1, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqBoost:F

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->minNrShouldMatch:F

    .line 119
    sget-object v0, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne p1, v0, :cond_0

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 121
    const-string v1, "highFreqOccur should be MUST or SHOULD but was MUST_NOT"

    .line 120
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_0
    sget-object v0, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne p2, v0, :cond_1

    .line 124
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 125
    const-string v1, "lowFreqOccur should be MUST or SHOULD but was MUST_NOT"

    .line 124
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_1
    iput-boolean p4, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->disableCoord:Z

    .line 128
    iput-object p1, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 129
    iput-object p2, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 130
    iput p3, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->maxTermFrequency:F

    .line 131
    return-void
.end method


# virtual methods
.method public add(Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 140
    if-nez p1, :cond_0

    .line 141
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Term must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    return-void
.end method

.method protected buildQuery(I[Lorg/apache/lucene/index/TermContext;[Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;
    .locals 12
    .param p1, "maxDoc"    # I
    .param p2, "contextArray"    # [Lorg/apache/lucene/index/TermContext;
    .param p3, "queryTerms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    .line 172
    new-instance v4, Lorg/apache/lucene/search/BooleanQuery;

    iget-boolean v9, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->disableCoord:Z

    invoke-direct {v4, v9}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 173
    .local v4, "lowFreq":Lorg/apache/lucene/search/BooleanQuery;
    new-instance v1, Lorg/apache/lucene/search/BooleanQuery;

    iget-boolean v9, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->disableCoord:Z

    invoke-direct {v1, v9}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 174
    .local v1, "highFreq":Lorg/apache/lucene/search/BooleanQuery;
    iget v9, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqBoost:F

    invoke-virtual {v1, v9}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    .line 175
    iget v9, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqBoost:F

    invoke-virtual {v4, v9}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    .line 176
    new-instance v7, Lorg/apache/lucene/search/BooleanQuery;

    const/4 v9, 0x1

    invoke-direct {v7, v9}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 177
    .local v7, "query":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v9, p3

    if-lt v3, v9, :cond_1

    .line 193
    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->clauses()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v6

    .line 194
    .local v6, "numLowFreqClauses":I
    iget-object v9, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    sget-object v10, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v9, v10, :cond_0

    if-lez v6, :cond_0

    .line 195
    invoke-virtual {p0, v6}, Lorg/apache/lucene/queries/CommonTermsQuery;->calcLowFreqMinimumNumberShouldMatch(I)I

    move-result v5

    .line 196
    .local v5, "minMustMatch":I
    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/BooleanQuery;->setMinimumNumberShouldMatch(I)V

    .line 198
    .end local v5    # "minMustMatch":I
    :cond_0
    invoke-virtual {v4}, Lorg/apache/lucene/search/BooleanQuery;->clauses()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 203
    iget-object v9, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    sget-object v10, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-ne v9, v10, :cond_6

    .line 204
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CommonTermsQuery;->getBoost()F

    move-result v9

    invoke-virtual {v1, v9}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    .line 223
    .end local v1    # "highFreq":Lorg/apache/lucene/search/BooleanQuery;
    :goto_1
    return-object v1

    .line 178
    .end local v6    # "numLowFreqClauses":I
    .restart local v1    # "highFreq":Lorg/apache/lucene/search/BooleanQuery;
    :cond_1
    aget-object v8, p2, v3

    .line 179
    .local v8, "termContext":Lorg/apache/lucene/index/TermContext;
    if-nez v8, :cond_2

    .line 180
    new-instance v9, Lorg/apache/lucene/search/TermQuery;

    aget-object v10, p3, v3

    invoke-direct {v9, v10}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    iget-object v10, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v4, v9, v10}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 177
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 182
    :cond_2
    iget v9, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->maxTermFrequency:F

    const/high16 v10, 0x3f800000    # 1.0f

    cmpl-float v9, v9, v10

    if-ltz v9, :cond_3

    invoke-virtual {v8}, Lorg/apache/lucene/index/TermContext;->docFreq()I

    move-result v9

    int-to-float v9, v9

    iget v10, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->maxTermFrequency:F

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_4

    .line 183
    :cond_3
    invoke-virtual {v8}, Lorg/apache/lucene/index/TermContext;->docFreq()I

    move-result v9

    iget v10, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->maxTermFrequency:F

    .line 184
    int-to-float v11, p1

    mul-float/2addr v10, v11

    float-to-double v10, v10

    .line 183
    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    if-le v9, v10, :cond_5

    .line 186
    :cond_4
    new-instance v9, Lorg/apache/lucene/search/TermQuery;

    aget-object v10, p3, v3

    invoke-direct {v9, v10, v8}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;)V

    iget-object v10, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v1, v9, v10}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_2

    .line 188
    :cond_5
    new-instance v9, Lorg/apache/lucene/search/TermQuery;

    aget-object v10, p3, v3

    invoke-direct {v9, v10, v8}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermContext;)V

    iget-object v10, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v4, v9, v10}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_2

    .line 207
    .end local v8    # "termContext":Lorg/apache/lucene/index/TermContext;
    .restart local v6    # "numLowFreqClauses":I
    :cond_6
    new-instance v2, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v2}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 208
    .local v2, "highFreqConjunction":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {v1}, Lorg/apache/lucene/search/BooleanQuery;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_7

    .line 211
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CommonTermsQuery;->getBoost()F

    move-result v9

    invoke-virtual {v2, v9}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    move-object v1, v2

    .line 212
    goto :goto_1

    .line 208
    :cond_7
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause;

    .line 209
    .local v0, "booleanClause":Lorg/apache/lucene/search/BooleanClause;
    invoke-virtual {v0}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v10

    sget-object v11, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v10, v11}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_3

    .line 215
    .end local v0    # "booleanClause":Lorg/apache/lucene/search/BooleanClause;
    .end local v2    # "highFreqConjunction":Lorg/apache/lucene/search/BooleanQuery;
    :cond_8
    invoke-virtual {v1}, Lorg/apache/lucene/search/BooleanQuery;->clauses()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_9

    .line 217
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CommonTermsQuery;->getBoost()F

    move-result v9

    invoke-virtual {v4, v9}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    move-object v1, v4

    .line 218
    goto/16 :goto_1

    .line 220
    :cond_9
    sget-object v9, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v7, v1, v9}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 221
    sget-object v9, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v7, v4, v9}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 222
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CommonTermsQuery;->getBoost()F

    move-result v9

    invoke-virtual {v7, v9}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    move-object v1, v7

    .line 223
    goto/16 :goto_1
.end method

.method protected calcLowFreqMinimumNumberShouldMatch(I)I
    .locals 2
    .param p1, "numOptional"    # I

    .prologue
    .line 164
    iget v0, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->minNrShouldMatch:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->minNrShouldMatch:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 165
    :cond_0
    iget v0, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->minNrShouldMatch:F

    float-to-int v0, v0

    .line 167
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->minNrShouldMatch:F

    int-to-float v1, p1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0
.end method

.method public collectTermContext(Lorg/apache/lucene/index/IndexReader;Ljava/util/List;[Lorg/apache/lucene/index/TermContext;[Lorg/apache/lucene/index/Term;)V
    .locals 17
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "contextArray"    # [Lorg/apache/lucene/index/TermContext;
    .param p4, "queryTerms"    # [Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexReader;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;[",
            "Lorg/apache/lucene/index/TermContext;",
            "[",
            "Lorg/apache/lucene/index/Term;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 230
    .local p2, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    const/4 v15, 0x0

    .line 231
    .local v15, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 263
    return-void

    .line 231
    :cond_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 232
    .local v10, "context":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v10}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v11

    .line 233
    .local v11, "fields":Lorg/apache/lucene/index/Fields;
    if-eqz v11, :cond_0

    .line 237
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    move-object/from16 v0, p4

    array-length v3, v0

    if-ge v12, v3, :cond_0

    .line 238
    aget-object v13, p4, v12

    .line 239
    .local v13, "term":Lorg/apache/lucene/index/Term;
    aget-object v2, p3, v12

    .line 240
    .local v2, "termContext":Lorg/apache/lucene/index/TermContext;
    invoke-virtual {v13}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v14

    .line 241
    .local v14, "terms":Lorg/apache/lucene/index/Terms;
    if-nez v14, :cond_3

    .line 237
    :cond_2
    :goto_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 245
    :cond_3
    invoke-virtual {v14, v15}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v15

    .line 246
    sget-boolean v3, Lorg/apache/lucene/queries/CommonTermsQuery;->$assertionsDisabled:Z

    if-nez v3, :cond_4

    if-nez v15, :cond_4

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 248
    :cond_4
    sget-object v3, Lorg/apache/lucene/index/TermsEnum;->EMPTY:Lorg/apache/lucene/index/TermsEnum;

    if-eq v15, v3, :cond_2

    .line 249
    invoke-virtual {v13}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v15, v3, v4}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 250
    if-nez v2, :cond_5

    .line 251
    new-instance v3, Lorg/apache/lucene/index/TermContext;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexReader;->getContext()Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v4

    .line 252
    invoke-virtual {v15}, Lorg/apache/lucene/index/TermsEnum;->termState()Lorg/apache/lucene/index/TermState;

    move-result-object v5

    iget v6, v10, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    invoke-virtual {v15}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v7

    .line 253
    invoke-virtual {v15}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v8

    invoke-direct/range {v3 .. v9}, Lorg/apache/lucene/index/TermContext;-><init>(Lorg/apache/lucene/index/IndexReaderContext;Lorg/apache/lucene/index/TermState;IIJ)V

    .line 251
    aput-object v3, p3, v12

    goto :goto_1

    .line 255
    :cond_5
    invoke-virtual {v15}, Lorg/apache/lucene/index/TermsEnum;->termState()Lorg/apache/lucene/index/TermState;

    move-result-object v3

    iget v4, v10, Lorg/apache/lucene/index/AtomicReaderContext;->ord:I

    .line 256
    invoke-virtual {v15}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v5

    invoke-virtual {v15}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v6

    .line 255
    invoke-virtual/range {v2 .. v7}, Lorg/apache/lucene/index/TermContext;->register(Lorg/apache/lucene/index/TermState;IIJ)V

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 353
    if-ne p0, p1, :cond_1

    .line 370
    :cond_0
    :goto_0
    return v1

    .line 354
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    .line 355
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 356
    check-cast v0, Lorg/apache/lucene/queries/CommonTermsQuery;

    .line 357
    .local v0, "other":Lorg/apache/lucene/queries/CommonTermsQuery;
    iget-boolean v3, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->disableCoord:Z

    iget-boolean v4, v0, Lorg/apache/lucene/queries/CommonTermsQuery;->disableCoord:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 358
    :cond_4
    iget v3, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqBoost:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 359
    iget v4, v0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqBoost:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 358
    if-eq v3, v4, :cond_5

    move v1, v2

    .line 359
    goto :goto_0

    .line 360
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    iget-object v4, v0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-eq v3, v4, :cond_6

    move v1, v2

    goto :goto_0

    .line 361
    :cond_6
    iget v3, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqBoost:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 362
    iget v4, v0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqBoost:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 361
    if-eq v3, v4, :cond_7

    move v1, v2

    .line 362
    goto :goto_0

    .line 363
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    iget-object v4, v0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-eq v3, v4, :cond_8

    move v1, v2

    goto :goto_0

    .line 364
    :cond_8
    iget v3, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->maxTermFrequency:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 365
    iget v4, v0, Lorg/apache/lucene/queries/CommonTermsQuery;->maxTermFrequency:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 364
    if-eq v3, v4, :cond_9

    move v1, v2

    .line 365
    goto :goto_0

    .line 366
    :cond_9
    iget v3, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->minNrShouldMatch:F

    iget v4, v0, Lorg/apache/lucene/queries/CommonTermsQuery;->minNrShouldMatch:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_a

    move v1, v2

    goto :goto_0

    .line 367
    :cond_a
    iget-object v3, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    if-nez v3, :cond_b

    .line 368
    iget-object v3, v0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 369
    :cond_b
    iget-object v3, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    iget-object v4, v0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 304
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v0, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 305
    return-void
.end method

.method public getMinimumNumberShouldMatch()F
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->minNrShouldMatch:F

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 336
    const/16 v0, 0x1f

    .line 337
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v1

    .line 338
    .local v1, "result":I
    mul-int/lit8 v4, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->disableCoord:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x4cf

    :goto_0
    add-int v1, v4, v2

    .line 339
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqBoost:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 340
    mul-int/lit8 v4, v1, 0x1f

    .line 341
    iget-object v2, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-nez v2, :cond_1

    move v2, v3

    .line 340
    :goto_1
    add-int v1, v4, v2

    .line 342
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqBoost:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 343
    mul-int/lit8 v4, v1, 0x1f

    .line 344
    iget-object v2, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    if-nez v2, :cond_2

    move v2, v3

    .line 343
    :goto_2
    add-int v1, v4, v2

    .line 345
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->maxTermFrequency:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 346
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->minNrShouldMatch:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 347
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    if-nez v4, :cond_3

    :goto_3
    add-int v1, v2, v3

    .line 348
    return v1

    .line 338
    :cond_0
    const/16 v2, 0x4d5

    goto :goto_0

    .line 341
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->highFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2}, Lorg/apache/lucene/search/BooleanClause$Occur;->hashCode()I

    move-result v2

    goto :goto_1

    .line 344
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->lowFreqOccur:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2}, Lorg/apache/lucene/search/BooleanClause$Occur;->hashCode()I

    move-result v2

    goto :goto_2

    .line 347
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    goto :goto_3
.end method

.method public isCoordDisabled()Z
    .locals 1

    .prologue
    .line 271
    iget-boolean v0, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->disableCoord:Z

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 8
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 148
    iget-object v5, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 149
    new-instance v4, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v4}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 160
    :goto_0
    return-object v4

    .line 150
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 151
    new-instance v4, Lorg/apache/lucene/search/TermQuery;

    iget-object v5, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/Term;

    invoke-direct {v4, v5}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 152
    .local v4, "tq":Lorg/apache/lucene/search/TermQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CommonTermsQuery;->getBoost()F

    move-result v5

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/TermQuery;->setBoost(F)V

    goto :goto_0

    .line 155
    .end local v4    # "tq":Lorg/apache/lucene/search/TermQuery;
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v1

    .line 156
    .local v1, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v2

    .line 157
    .local v2, "maxDoc":I
    iget-object v5, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v0, v5, [Lorg/apache/lucene/index/TermContext;

    .line 158
    .local v0, "contextArray":[Lorg/apache/lucene/index/TermContext;
    iget-object v5, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    new-array v6, v7, [Lorg/apache/lucene/index/Term;

    invoke-interface {v5, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/index/Term;

    .line 159
    .local v3, "queryTerms":[Lorg/apache/lucene/index/Term;
    invoke-virtual {p0, p1, v1, v0, v3}, Lorg/apache/lucene/queries/CommonTermsQuery;->collectTermContext(Lorg/apache/lucene/index/IndexReader;Ljava/util/List;[Lorg/apache/lucene/index/TermContext;[Lorg/apache/lucene/index/Term;)V

    .line 160
    invoke-virtual {p0, v2, v0, v3}, Lorg/apache/lucene/queries/CommonTermsQuery;->buildQuery(I[Lorg/apache/lucene/index/TermContext;[Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/search/Query;

    move-result-object v4

    goto :goto_0
.end method

.method public setMinimumNumberShouldMatch(F)V
    .locals 0
    .param p1, "min"    # F

    .prologue
    .line 291
    iput p1, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->minNrShouldMatch:F

    .line 292
    return-void
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 309
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 310
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CommonTermsQuery;->getBoost()F

    move-result v4

    float-to-double v4, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_4

    .line 311
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CommonTermsQuery;->getMinimumNumberShouldMatch()F

    move-result v4

    .line 310
    cmpl-float v4, v4, v8

    if-gtz v4, :cond_4

    const/4 v2, 0x0

    .line 312
    .local v2, "needParens":Z
    :goto_0
    if-eqz v2, :cond_0

    .line 313
    const-string v4, "("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_5

    .line 321
    if-eqz v2, :cond_1

    .line 322
    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CommonTermsQuery;->getMinimumNumberShouldMatch()F

    move-result v4

    cmpl-float v4, v4, v8

    if-lez v4, :cond_2

    .line 325
    const/16 v4, 0x7e

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 326
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CommonTermsQuery;->getMinimumNumberShouldMatch()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 328
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CommonTermsQuery;->getBoost()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_3

    .line 329
    invoke-virtual {p0}, Lorg/apache/lucene/queries/CommonTermsQuery;->getBoost()F

    move-result v4

    invoke-static {v4}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 310
    .end local v1    # "i":I
    .end local v2    # "needParens":Z
    :cond_4
    const/4 v2, 0x1

    goto :goto_0

    .line 316
    .restart local v1    # "i":I
    .restart local v2    # "needParens":Z
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/Term;

    .line 317
    .local v3, "t":Lorg/apache/lucene/index/Term;
    new-instance v4, Lorg/apache/lucene/search/TermQuery;

    invoke-direct {v4, v3}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    invoke-virtual {v4}, Lorg/apache/lucene/search/TermQuery;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    iget-object v4, p0, Lorg/apache/lucene/queries/CommonTermsQuery;->terms:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v1, v4, :cond_6

    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
