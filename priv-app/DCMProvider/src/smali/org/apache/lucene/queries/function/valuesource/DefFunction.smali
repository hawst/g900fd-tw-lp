.class public Lorg/apache/lucene/queries/function/valuesource/DefFunction;
.super Lorg/apache/lucene/queries/function/valuesource/MultiFunction;
.source "DefFunction.java"


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/queries/function/ValueSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "sources":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/queries/function/ValueSource;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/MultiFunction;-><init>(Ljava/util/List;)V

    .line 39
    return-void
.end method


# virtual methods
.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 2
    .param p1, "fcontext"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DefFunction;->sources:Ljava/util/List;

    invoke-static {v1, p1, p2}, Lorg/apache/lucene/queries/function/valuesource/DefFunction;->valsArr(Ljava/util/List;Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)[Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lorg/apache/lucene/queries/function/valuesource/DefFunction$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/DefFunction;Lorg/apache/lucene/queries/function/valuesource/MultiFunction;[Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v0
.end method

.method protected name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "def"

    return-object v0
.end method
