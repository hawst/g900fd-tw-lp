.class public final Lorg/apache/lucene/queries/mlt/MoreLikeThis;
.super Ljava/lang/Object;
.source "MoreLikeThis.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queries/mlt/MoreLikeThis$FreqQ;,
        Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;
    }
.end annotation


# static fields
.field public static final DEFAULT_BOOST:Z = false

.field public static final DEFAULT_FIELD_NAMES:[Ljava/lang/String;

.field public static final DEFAULT_MAX_DOC_FREQ:I = 0x7fffffff

.field public static final DEFAULT_MAX_NUM_TOKENS_PARSED:I = 0x1388

.field public static final DEFAULT_MAX_QUERY_TERMS:I = 0x19

.field public static final DEFAULT_MAX_WORD_LENGTH:I = 0x0

.field public static final DEFAULT_MIN_DOC_FREQ:I = 0x5

.field public static final DEFAULT_MIN_TERM_FREQ:I = 0x2

.field public static final DEFAULT_MIN_WORD_LENGTH:I

.field public static final DEFAULT_STOP_WORDS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private boost:Z

.field private boostFactor:F

.field private fieldNames:[Ljava/lang/String;

.field private final ir:Lorg/apache/lucene/index/IndexReader;

.field private maxDocFreq:I

.field private maxNumTokensParsed:I

.field private maxQueryTerms:I

.field private maxWordLen:I

.field private minDocFreq:I

.field private minTermFreq:I

.field private minWordLen:I

.field private similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

.field private stopWords:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 183
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "contents"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->DEFAULT_FIELD_NAMES:[Ljava/lang/String;

    .line 208
    const/4 v0, 0x0

    sput-object v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->DEFAULT_STOP_WORDS:Ljava/util/Set;

    .line 222
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "ir"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 312
    new-instance v0, Lorg/apache/lucene/search/similarities/DefaultSimilarity;

    invoke-direct {v0}, Lorg/apache/lucene/search/similarities/DefaultSimilarity;-><init>()V

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/similarities/TFIDFSimilarity;)V

    .line 313
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/similarities/TFIDFSimilarity;)V
    .locals 2
    .param p1, "ir"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "sim"    # Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    .prologue
    const/4 v1, 0x0

    .line 315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    sget-object v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->DEFAULT_STOP_WORDS:Ljava/util/Set;

    iput-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->stopWords:Ljava/util/Set;

    .line 227
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 232
    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minTermFreq:I

    .line 237
    const/4 v0, 0x5

    iput v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minDocFreq:I

    .line 242
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxDocFreq:I

    .line 247
    iput-boolean v1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->boost:Z

    .line 252
    sget-object v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->DEFAULT_FIELD_NAMES:[Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->fieldNames:[Ljava/lang/String;

    .line 257
    const/16 v0, 0x1388

    iput v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxNumTokensParsed:I

    .line 262
    iput v1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minWordLen:I

    .line 267
    iput v1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxWordLen:I

    .line 272
    const/16 v0, 0x19

    iput v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxQueryTerms:I

    .line 287
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->boostFactor:F

    .line 316
    iput-object p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->ir:Lorg/apache/lucene/index/IndexReader;

    .line 317
    iput-object p2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    .line 318
    return-void
.end method

.method private addTermFrequencies(Ljava/io/Reader;Ljava/util/Map;Ljava/lang/String;)V
    .locals 7
    .param p1, "r"    # Ljava/io/Reader;
    .param p3, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/Reader;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 774
    .local p2, "termFreqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;>;"
    iget-object v5, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    if-nez v5, :cond_0

    .line 775
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    const-string v6, "To use MoreLikeThis without term vectors, you must provide an Analyzer"

    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 778
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v5, p3, p1}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v3

    .line 779
    .local v3, "ts":Lorg/apache/lucene/analysis/TokenStream;
    const/4 v2, 0x0

    .line 781
    .local v2, "tokenCount":I
    const-class v5, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v3, v5}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 782
    .local v1, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 783
    :cond_1
    :goto_0
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v5

    if-nez v5, :cond_3

    .line 801
    :cond_2
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 802
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    .line 803
    return-void

    .line 784
    :cond_3
    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v4

    .line 785
    .local v4, "word":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    .line 786
    iget v5, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxNumTokensParsed:I

    if-gt v2, v5, :cond_2

    .line 789
    invoke-direct {p0, v4}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->isNoiseWord(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 794
    invoke-interface {p2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;

    .line 795
    .local v0, "cnt":Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;
    if-nez v0, :cond_4

    .line 796
    new-instance v5, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;

    invoke-direct {v5}, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;-><init>()V

    invoke-interface {p2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 798
    :cond_4
    iget v5, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;->x:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;->x:I

    goto :goto_0
.end method

.method private addTermFrequencies(Ljava/util/Map;Lorg/apache/lucene/index/Terms;)V
    .locals 8
    .param p2, "vector"    # Lorg/apache/lucene/index/Terms;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;",
            ">;",
            "Lorg/apache/lucene/index/Terms;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 742
    .local p1, "termFreqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;>;"
    const/4 v6, 0x0

    invoke-virtual {p2, v6}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v4

    .line 743
    .local v4, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    new-instance v2, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v2}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    .line 745
    .local v2, "spare":Lorg/apache/lucene/util/CharsRef;
    :cond_0
    :goto_0
    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v5

    .local v5, "text":Lorg/apache/lucene/util/BytesRef;
    if-nez v5, :cond_1

    .line 763
    return-void

    .line 746
    :cond_1
    invoke-static {v5, v2}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/CharsRef;)V

    .line 747
    invoke-virtual {v2}, Lorg/apache/lucene/util/CharsRef;->toString()Ljava/lang/String;

    move-result-object v3

    .line 748
    .local v3, "term":Ljava/lang/String;
    invoke-direct {p0, v3}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->isNoiseWord(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 751
    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v6

    long-to-int v1, v6

    .line 754
    .local v1, "freq":I
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;

    .line 755
    .local v0, "cnt":Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;
    if-nez v0, :cond_2

    .line 756
    new-instance v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;

    .end local v0    # "cnt":Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;
    invoke-direct {v0}, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;-><init>()V

    .line 757
    .restart local v0    # "cnt":Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;
    invoke-interface {p1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 758
    iput v1, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;->x:I

    goto :goto_0

    .line 760
    :cond_2
    iget v6, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;->x:I

    add-int/2addr v6, v1

    iput v6, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;->x:I

    goto :goto_0
.end method

.method private createQuery(Lorg/apache/lucene/util/PriorityQueue;)Lorg/apache/lucene/search/Query;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/PriorityQueue",
            "<[",
            "Ljava/lang/Object;",
            ">;)",
            "Lorg/apache/lucene/search/Query;"
        }
    .end annotation

    .prologue
    .local p1, "q":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<[Ljava/lang/Object;>;"
    const/4 v11, 0x2

    .line 594
    new-instance v6, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v6}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 596
    .local v6, "query":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v5, 0x0

    .line 597
    .local v5, "qterms":I
    const/4 v1, 0x0

    .line 599
    .local v1, "bestScore":F
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    move-result-object v2

    .local v2, "cur":Ljava/lang/Object;
    if-nez v2, :cond_1

    .line 625
    :goto_0
    return-object v6

    :cond_1
    move-object v0, v2

    .line 600
    check-cast v0, [Ljava/lang/Object;

    .line 601
    .local v0, "ar":[Ljava/lang/Object;
    new-instance v7, Lorg/apache/lucene/search/TermQuery;

    new-instance v10, Lorg/apache/lucene/index/Term;

    const/4 v8, 0x1

    aget-object v8, v0, v8

    check-cast v8, Ljava/lang/String;

    const/4 v9, 0x0

    aget-object v9, v0, v9

    check-cast v9, Ljava/lang/String;

    invoke-direct {v10, v8, v9}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v7, v10}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 603
    .local v7, "tq":Lorg/apache/lucene/search/TermQuery;
    iget-boolean v8, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->boost:Z

    if-eqz v8, :cond_3

    .line 604
    if-nez v5, :cond_2

    .line 605
    aget-object v8, v0, v11

    check-cast v8, Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 607
    :cond_2
    aget-object v8, v0, v11

    check-cast v8, Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 609
    .local v4, "myScore":F
    iget v8, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->boostFactor:F

    mul-float/2addr v8, v4

    div-float/2addr v8, v1

    invoke-virtual {v7, v8}, Lorg/apache/lucene/search/TermQuery;->setBoost(F)V

    .line 613
    .end local v4    # "myScore":F
    :cond_3
    :try_start_0
    sget-object v8, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    :try_end_0
    .catch Lorg/apache/lucene/search/BooleanQuery$TooManyClauses; {:try_start_0 .. :try_end_0} :catch_0

    .line 619
    add-int/lit8 v5, v5, 0x1

    .line 620
    iget v8, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxQueryTerms:I

    if-lez v8, :cond_0

    iget v8, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxQueryTerms:I

    if-lt v5, v8, :cond_0

    goto :goto_0

    .line 615
    :catch_0
    move-exception v3

    .line 616
    .local v3, "ignore":Lorg/apache/lucene/search/BooleanQuery$TooManyClauses;
    goto :goto_0
.end method

.method private createQueue(Ljava/util/Map;)Lorg/apache/lucene/util/PriorityQueue;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;",
            ">;)",
            "Lorg/apache/lucene/util/PriorityQueue",
            "<[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 635
    .local p1, "words":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;>;"
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->ir:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v12}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v6

    .line 636
    .local v6, "numDocs":I
    new-instance v7, Lorg/apache/lucene/queries/mlt/MoreLikeThis$FreqQ;

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->size()I

    move-result v12

    invoke-direct {v7, v12}, Lorg/apache/lucene/queries/mlt/MoreLikeThis$FreqQ;-><init>(I)V

    .line 638
    .local v7, "res":Lorg/apache/lucene/queries/mlt/MoreLikeThis$FreqQ;
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_1

    .line 677
    return-object v7

    .line 638
    :cond_1
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 639
    .local v11, "word":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;

    iget v9, v12, Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;->x:I

    .line 640
    .local v9, "tf":I
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minTermFreq:I

    if-lez v12, :cond_2

    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minTermFreq:I

    if-lt v9, v12, :cond_0

    .line 645
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->fieldNames:[Ljava/lang/String;

    const/4 v14, 0x0

    aget-object v10, v12, v14

    .line 646
    .local v10, "topField":Ljava/lang/String;
    const/4 v2, 0x0

    .line 647
    .local v2, "docFreq":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->fieldNames:[Ljava/lang/String;

    array-length v15, v14

    const/4 v12, 0x0

    :goto_1
    if-lt v12, v15, :cond_4

    .line 653
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minDocFreq:I

    if-lez v12, :cond_3

    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minDocFreq:I

    if-lt v2, v12, :cond_0

    .line 657
    :cond_3
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxDocFreq:I

    if-gt v2, v12, :cond_0

    .line 661
    if-eqz v2, :cond_0

    .line 665
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    int-to-long v14, v2

    int-to-long v0, v6

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v12, v14, v15, v0, v1}, Lorg/apache/lucene/search/similarities/TFIDFSimilarity;->idf(JJ)F

    move-result v5

    .line 666
    .local v5, "idf":F
    int-to-float v12, v9

    mul-float v8, v12, v5

    .line 669
    .local v8, "score":F
    const/4 v12, 0x6

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v11, v12, v14

    const/4 v14, 0x1

    .line 670
    aput-object v10, v12, v14

    const/4 v14, 0x2

    .line 671
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    aput-object v15, v12, v14

    const/4 v14, 0x3

    .line 672
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    aput-object v15, v12, v14

    const/4 v14, 0x4

    .line 673
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v12, v14

    const/4 v14, 0x5

    .line 674
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v12, v14

    .line 669
    invoke-virtual {v7, v12}, Lorg/apache/lucene/queries/mlt/MoreLikeThis$FreqQ;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 647
    .end local v5    # "idf":F
    .end local v8    # "score":F
    :cond_4
    aget-object v3, v14, v12

    .line 648
    .local v3, "fieldName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->ir:Lorg/apache/lucene/index/IndexReader;

    move-object/from16 v16, v0

    new-instance v17, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v17

    invoke-direct {v0, v3, v11}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v17}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v4

    .line 649
    .local v4, "freq":I
    if-le v4, v2, :cond_5

    move-object v10, v3

    .line 650
    :cond_5
    if-le v4, v2, :cond_6

    move v2, v4

    .line 647
    :cond_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_1
.end method

.method private isNoiseWord(Ljava/lang/String;)Z
    .locals 3
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 813
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 814
    .local v0, "len":I
    iget v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minWordLen:I

    if-lez v2, :cond_1

    iget v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minWordLen:I

    if-ge v0, v2, :cond_1

    .line 820
    :cond_0
    :goto_0
    return v1

    .line 817
    :cond_1
    iget v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxWordLen:I

    if-lez v2, :cond_2

    iget v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxWordLen:I

    if-gt v0, v2, :cond_0

    .line 820
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->stopWords:Ljava/util/Set;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->stopWords:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeParams()Ljava/lang/String;
    .locals 7

    .prologue
    .line 684
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 685
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "maxQueryTerms  : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxQueryTerms:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 686
    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "minWordLen     : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minWordLen:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 687
    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "maxWordLen     : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxWordLen:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 688
    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "fieldNames     : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 689
    const-string v0, ""

    .line 690
    .local v0, "delim":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->fieldNames:[Ljava/lang/String;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 694
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "boost          : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->boost:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 696
    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "minTermFreq    : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minTermFreq:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "minDocFreq     : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minDocFreq:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 690
    :cond_0
    aget-object v1, v4, v3

    .line 691
    .local v1, "fieldName":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 692
    const-string v0, ", "

    .line 690
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method public getBoostFactor()F
    .locals 1

    .prologue
    .line 296
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->boostFactor:F

    return v0
.end method

.method public getFieldNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->fieldNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getMaxDocFreq()I
    .locals 1

    .prologue
    .line 399
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxDocFreq:I

    return v0
.end method

.method public getMaxNumTokensParsed()I
    .locals 1

    .prologue
    .line 554
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxNumTokensParsed:I

    return v0
.end method

.method public getMaxQueryTerms()I
    .locals 1

    .prologue
    .line 536
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxQueryTerms:I

    return v0
.end method

.method public getMaxWordLen()I
    .locals 1

    .prologue
    .line 494
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxWordLen:I

    return v0
.end method

.method public getMinDocFreq()I
    .locals 1

    .prologue
    .line 376
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minDocFreq:I

    return v0
.end method

.method public getMinTermFreq()I
    .locals 1

    .prologue
    .line 356
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minTermFreq:I

    return v0
.end method

.method public getMinWordLen()I
    .locals 1

    .prologue
    .line 475
    iget v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minWordLen:I

    return v0
.end method

.method public getSimilarity()Lorg/apache/lucene/search/similarities/TFIDFSimilarity;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    return-object v0
.end method

.method public getStopWords()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 525
    iget-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->stopWords:Ljava/util/Set;

    return-object v0
.end method

.method public isBoost()Z
    .locals 1

    .prologue
    .line 433
    iget-boolean v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->boost:Z

    return v0
.end method

.method public like(I)Lorg/apache/lucene/search/Query;
    .locals 2
    .param p1, "docNum"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 572
    iget-object v1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->fieldNames:[Ljava/lang/String;

    if-nez v1, :cond_0

    .line 574
    iget-object v1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->ir:Lorg/apache/lucene/index/IndexReader;

    invoke-static {v1}, Lorg/apache/lucene/index/MultiFields;->getIndexedFields(Lorg/apache/lucene/index/IndexReader;)Ljava/util/Collection;

    move-result-object v0

    .line 575
    .local v0, "fields":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->fieldNames:[Ljava/lang/String;

    .line 578
    .end local v0    # "fields":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->retrieveTerms(I)Lorg/apache/lucene/util/PriorityQueue;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->createQuery(Lorg/apache/lucene/util/PriorityQueue;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    return-object v1
.end method

.method public like(Ljava/io/Reader;Ljava/lang/String;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "r"    # Ljava/io/Reader;
    .param p2, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 587
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->retrieveTerms(Ljava/io/Reader;Ljava/lang/String;)Lorg/apache/lucene/util/PriorityQueue;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->createQuery(Lorg/apache/lucene/util/PriorityQueue;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public retrieveInterestingTerms(I)[Ljava/lang/String;
    .locals 8
    .param p1, "docNum"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 856
    new-instance v0, Ljava/util/ArrayList;

    iget v7, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxQueryTerms:I

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 857
    .local v0, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->retrieveTerms(I)Lorg/apache/lucene/util/PriorityQueue;

    move-result-object v5

    .line 859
    .local v5, "pq":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<[Ljava/lang/Object;>;"
    iget v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxQueryTerms:I

    .line 861
    .local v3, "lim":I
    :goto_0
    invoke-virtual {v5}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    move-result-object v2

    .local v2, "cur":Ljava/lang/Object;
    if-eqz v2, :cond_0

    add-int/lit8 v4, v3, -0x1

    .end local v3    # "lim":I
    .local v4, "lim":I
    if-gtz v3, :cond_1

    move v3, v4

    .line 865
    .end local v4    # "lim":I
    .restart local v3    # "lim":I
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v6, v7, [Ljava/lang/String;

    .line 866
    .local v6, "res":[Ljava/lang/String;
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    return-object v7

    .end local v3    # "lim":I
    .end local v6    # "res":[Ljava/lang/String;
    .restart local v4    # "lim":I
    :cond_1
    move-object v1, v2

    .line 862
    check-cast v1, [Ljava/lang/Object;

    .line 863
    .local v1, "ar":[Ljava/lang/Object;
    const/4 v7, 0x0

    aget-object v7, v1, v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v3, v4

    .end local v4    # "lim":I
    .restart local v3    # "lim":I
    goto :goto_0
.end method

.method public retrieveInterestingTerms(Ljava/io/Reader;Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p1, "r"    # Ljava/io/Reader;
    .param p2, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 880
    new-instance v0, Ljava/util/ArrayList;

    iget v7, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxQueryTerms:I

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 881
    .local v0, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->retrieveTerms(Ljava/io/Reader;Ljava/lang/String;)Lorg/apache/lucene/util/PriorityQueue;

    move-result-object v5

    .line 883
    .local v5, "pq":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<[Ljava/lang/Object;>;"
    iget v3, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxQueryTerms:I

    .line 885
    .local v3, "lim":I
    :goto_0
    invoke-virtual {v5}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    move-result-object v2

    .local v2, "cur":Ljava/lang/Object;
    if-eqz v2, :cond_0

    add-int/lit8 v4, v3, -0x1

    .end local v3    # "lim":I
    .local v4, "lim":I
    if-gtz v3, :cond_1

    move v3, v4

    .line 889
    .end local v4    # "lim":I
    .restart local v3    # "lim":I
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v6, v7, [Ljava/lang/String;

    .line 890
    .local v6, "res":[Ljava/lang/String;
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    return-object v7

    .end local v3    # "lim":I
    .end local v6    # "res":[Ljava/lang/String;
    .restart local v4    # "lim":I
    :cond_1
    move-object v1, v2

    .line 886
    check-cast v1, [Ljava/lang/Object;

    .line 887
    .local v1, "ar":[Ljava/lang/Object;
    const/4 v7, 0x0

    aget-object v7, v1, v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v3, v4

    .end local v4    # "lim":I
    .restart local v3    # "lim":I
    goto :goto_0
.end method

.method public retrieveTerms(I)Lorg/apache/lucene/util/PriorityQueue;
    .locals 14
    .param p1, "docNum"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lorg/apache/lucene/util/PriorityQueue",
            "<[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 707
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 708
    .local v5, "termFreqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;>;"
    iget-object v10, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->fieldNames:[Ljava/lang/String;

    array-length v11, v10

    const/4 v8, 0x0

    move v9, v8

    :goto_0
    if-lt v9, v11, :cond_0

    .line 732
    invoke-direct {p0, v5}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->createQueue(Ljava/util/Map;)Lorg/apache/lucene/util/PriorityQueue;

    move-result-object v8

    return-object v8

    .line 708
    :cond_0
    aget-object v2, v10, v9

    .line 709
    .local v2, "fieldName":Ljava/lang/String;
    iget-object v8, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->ir:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v8, p1}, Lorg/apache/lucene/index/IndexReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v7

    .line 711
    .local v7, "vectors":Lorg/apache/lucene/index/Fields;
    if-eqz v7, :cond_1

    .line 712
    invoke-virtual {v7, v2}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v6

    .line 718
    .local v6, "vector":Lorg/apache/lucene/index/Terms;
    :goto_1
    if-nez v6, :cond_4

    .line 719
    iget-object v8, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->ir:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v8, p1}, Lorg/apache/lucene/index/IndexReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v0

    .line 720
    .local v0, "d":Lorg/apache/lucene/document/Document;
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/Document;->getFields(Ljava/lang/String;)[Lorg/apache/lucene/index/IndexableField;

    move-result-object v3

    .line 721
    .local v3, "fields":[Lorg/apache/lucene/index/IndexableField;
    array-length v12, v3

    const/4 v8, 0x0

    :goto_2
    if-lt v8, v12, :cond_2

    .line 708
    .end local v0    # "d":Lorg/apache/lucene/document/Document;
    .end local v3    # "fields":[Lorg/apache/lucene/index/IndexableField;
    :goto_3
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_0

    .line 714
    .end local v6    # "vector":Lorg/apache/lucene/index/Terms;
    :cond_1
    const/4 v6, 0x0

    .restart local v6    # "vector":Lorg/apache/lucene/index/Terms;
    goto :goto_1

    .line 721
    .restart local v0    # "d":Lorg/apache/lucene/document/Document;
    .restart local v3    # "fields":[Lorg/apache/lucene/index/IndexableField;
    :cond_2
    aget-object v1, v3, v8

    .line 722
    .local v1, "field":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v1}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v4

    .line 723
    .local v4, "stringValue":Ljava/lang/String;
    if-eqz v4, :cond_3

    .line 724
    new-instance v13, Ljava/io/StringReader;

    invoke-direct {v13, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v13, v5, v2}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->addTermFrequencies(Ljava/io/Reader;Ljava/util/Map;Ljava/lang/String;)V

    .line 721
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 728
    .end local v0    # "d":Lorg/apache/lucene/document/Document;
    .end local v1    # "field":Lorg/apache/lucene/index/IndexableField;
    .end local v3    # "fields":[Lorg/apache/lucene/index/IndexableField;
    .end local v4    # "stringValue":Ljava/lang/String;
    :cond_4
    invoke-direct {p0, v5, v6}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->addTermFrequencies(Ljava/util/Map;Lorg/apache/lucene/index/Terms;)V

    goto :goto_3
.end method

.method public retrieveTerms(Ljava/io/Reader;Ljava/lang/String;)Lorg/apache/lucene/util/PriorityQueue;
    .locals 2
    .param p1, "r"    # Ljava/io/Reader;
    .param p2, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/Reader;",
            "Ljava/lang/String;",
            ")",
            "Lorg/apache/lucene/util/PriorityQueue",
            "<[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 847
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 848
    .local v0, "words":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/queries/mlt/MoreLikeThis$Int;>;"
    invoke-direct {p0, p1, v0, p2}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->addTermFrequencies(Ljava/io/Reader;Ljava/util/Map;Ljava/lang/String;)V

    .line 849
    invoke-direct {p0, v0}, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->createQueue(Ljava/util/Map;)Lorg/apache/lucene/util/PriorityQueue;

    move-result-object v1

    return-object v1
.end method

.method public setAnalyzer(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 0
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 346
    iput-object p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 347
    return-void
.end method

.method public setBoost(Z)V
    .locals 0
    .param p1, "boost"    # Z

    .prologue
    .line 443
    iput-boolean p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->boost:Z

    .line 444
    return-void
.end method

.method public setBoostFactor(F)V
    .locals 0
    .param p1, "boostFactor"    # F

    .prologue
    .line 305
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->boostFactor:F

    .line 306
    return-void
.end method

.method public setFieldNames([Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldNames"    # [Ljava/lang/String;

    .prologue
    .line 465
    iput-object p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->fieldNames:[Ljava/lang/String;

    .line 466
    return-void
.end method

.method public setMaxDocFreq(I)V
    .locals 0
    .param p1, "maxFreq"    # I

    .prologue
    .line 410
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxDocFreq:I

    .line 411
    return-void
.end method

.method public setMaxDocFreqPct(I)V
    .locals 1
    .param p1, "maxPercentage"    # I

    .prologue
    .line 421
    iget-object v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->ir:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v0

    mul-int/2addr v0, p1

    div-int/lit8 v0, v0, 0x64

    iput v0, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxDocFreq:I

    .line 422
    return-void
.end method

.method public setMaxNumTokensParsed(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 561
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxNumTokensParsed:I

    .line 562
    return-void
.end method

.method public setMaxQueryTerms(I)V
    .locals 0
    .param p1, "maxQueryTerms"    # I

    .prologue
    .line 546
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxQueryTerms:I

    .line 547
    return-void
.end method

.method public setMaxWordLen(I)V
    .locals 0
    .param p1, "maxWordLen"    # I

    .prologue
    .line 503
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->maxWordLen:I

    .line 504
    return-void
.end method

.method public setMinDocFreq(I)V
    .locals 0
    .param p1, "minDocFreq"    # I

    .prologue
    .line 387
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minDocFreq:I

    .line 388
    return-void
.end method

.method public setMinTermFreq(I)V
    .locals 0
    .param p1, "minTermFreq"    # I

    .prologue
    .line 365
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minTermFreq:I

    .line 366
    return-void
.end method

.method public setMinWordLen(I)V
    .locals 0
    .param p1, "minWordLen"    # I

    .prologue
    .line 484
    iput p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->minWordLen:I

    .line 485
    return-void
.end method

.method public setSimilarity(Lorg/apache/lucene/search/similarities/TFIDFSimilarity;)V
    .locals 0
    .param p1, "similarity"    # Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    .prologue
    .line 326
    iput-object p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->similarity:Lorg/apache/lucene/search/similarities/TFIDFSimilarity;

    .line 327
    return-void
.end method

.method public setStopWords(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 516
    .local p1, "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    iput-object p1, p0, Lorg/apache/lucene/queries/mlt/MoreLikeThis;->stopWords:Ljava/util/Set;

    .line 517
    return-void
.end method
