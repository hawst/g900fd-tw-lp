.class Lorg/apache/lucene/queries/function/valuesource/ConstValueSource$1;
.super Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;
.source "ConstValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;

    .line 46
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public boolVal(I)Z
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;

    iget v0, v0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;

    # getter for: Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->dv:D
    invoke-static {v0}, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->access$0(Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;)D

    move-result-wide v0

    return-wide v0
.end method

.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;

    iget v0, v0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    return v0
.end method

.method public intVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;

    iget v0, v0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    float-to-int v0, v0

    return v0
.end method

.method public longVal(I)J
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;

    iget v0, v0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    float-to-long v0, v0

    return-wide v0
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;

    iget v0, v0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->constant:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;

    invoke-virtual {v0}, Lorg/apache/lucene/queries/function/valuesource/ConstValueSource;->description()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
