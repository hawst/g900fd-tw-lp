.class public Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;
.super Lorg/apache/lucene/queries/function/valuesource/ConstNumberSource;
.source "DoubleConstValueSource.java"


# instance fields
.field final constant:D

.field private final fv:F

.field private final lv:J


# direct methods
.method public constructor <init>(D)V
    .locals 3
    .param p1, "constant"    # D

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/valuesource/ConstNumberSource;-><init>()V

    .line 36
    iput-wide p1, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->constant:D

    .line 37
    double-to-float v0, p1

    iput v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->fv:F

    .line 38
    double-to-long v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->lv:J

    .line 39
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;)F
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->fv:F

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;)J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->lv:J

    return-wide v0
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 4

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "const("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->constant:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 94
    instance-of v2, p1, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;

    if-nez v2, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 95
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;

    .line 96
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;
    iget-wide v2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->constant:D

    iget-wide v4, v0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->constant:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getBool()Z
    .locals 4

    .prologue
    .line 126
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->constant:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDouble()D
    .locals 2

    .prologue
    .line 116
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->constant:D

    return-wide v0
.end method

.method public getFloat()F
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->fv:F

    return v0
.end method

.method public getInt()I
    .locals 2

    .prologue
    .line 101
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->lv:J

    long-to-int v0, v0

    return v0
.end method

.method public getLong()J
    .locals 2

    .prologue
    .line 106
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->lv:J

    return-wide v0
.end method

.method public getNumber()Ljava/lang/Number;
    .locals 2

    .prologue
    .line 121
    iget-wide v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->constant:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource$1;

    invoke-direct {v0, p0, p0}, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;Lorg/apache/lucene/queries/function/ValueSource;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 88
    iget-wide v2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleConstValueSource;->constant:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    .line 89
    .local v0, "bits":J
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method
