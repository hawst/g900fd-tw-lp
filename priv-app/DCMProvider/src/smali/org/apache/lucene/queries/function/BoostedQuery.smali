.class public Lorg/apache/lucene/queries/function/BoostedQuery;
.super Lorg/apache/lucene/search/Query;
.source "BoostedQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;,
        Lorg/apache/lucene/queries/function/BoostedQuery$CustomScorer;
    }
.end annotation


# instance fields
.field private final boostVal:Lorg/apache/lucene/queries/function/ValueSource;

.field private q:Lorg/apache/lucene/search/Query;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/queries/function/ValueSource;)V
    .locals 0
    .param p1, "subQuery"    # Lorg/apache/lucene/search/Query;
    .param p2, "boostVal"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;

    .line 44
    iput-object p2, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->boostVal:Lorg/apache/lucene/queries/function/ValueSource;

    .line 45
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/queries/function/BoostedQuery;)Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/queries/function/BoostedQuery;)Lorg/apache/lucene/queries/function/ValueSource;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->boostVal:Lorg/apache/lucene/queries/function/ValueSource;

    return-object v0
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/queries/function/BoostedQuery$BoostedWeight;-><init>(Lorg/apache/lucene/queries/function/BoostedQuery;Lorg/apache/lucene/search/IndexSearcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 209
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 211
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 210
    check-cast v0, Lorg/apache/lucene/queries/function/BoostedQuery;

    .line 211
    .local v0, "other":Lorg/apache/lucene/queries/function/BoostedQuery;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 212
    iget-object v2, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->boostVal:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/BoostedQuery;->boostVal:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 211
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Query;->extractTerms(Ljava/util/Set;)V

    .line 62
    return-void
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method public getValueSource()Lorg/apache/lucene/queries/function/ValueSource;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->boostVal:Lorg/apache/lucene/queries/function/ValueSource;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 217
    iget-object v1, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v0

    .line 218
    .local v0, "h":I
    shl-int/lit8 v1, v0, 0x11

    ushr-int/lit8 v2, v0, 0x10

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 219
    iget-object v1, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->boostVal:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    shl-int/lit8 v1, v0, 0x8

    ushr-int/lit8 v2, v0, 0x19

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 221
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/BoostedQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v2, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 53
    .local v1, "newQ":Lorg/apache/lucene/search/Query;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;

    if-ne v1, v2, :cond_0

    .line 56
    .end local p0    # "this":Lorg/apache/lucene/queries/function/BoostedQuery;
    :goto_0
    return-object p0

    .line 54
    .restart local p0    # "this":Lorg/apache/lucene/queries/function/BoostedQuery;
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/BoostedQuery;->clone()Lorg/apache/lucene/search/Query;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/queries/function/BoostedQuery;

    .line 55
    .local v0, "bq":Lorg/apache/lucene/queries/function/BoostedQuery;
    iput-object v1, v0, Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;

    move-object p0, v0

    .line 56
    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 202
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "boost("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/queries/function/BoostedQuery;->boostVal:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 203
    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/BoostedQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
