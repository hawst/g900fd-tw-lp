.class public Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "DocFreqValueSource.java"


# instance fields
.field protected final field:Ljava/lang/String;

.field protected final indexedBytes:Lorg/apache/lucene/util/BytesRef;

.field protected final indexedField:Ljava/lang/String;

.field protected final val:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "indexedField"    # Ljava/lang/String;
    .param p4, "indexedBytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 132
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 133
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->field:Ljava/lang/String;

    .line 134
    iput-object p2, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->val:Ljava/lang/String;

    .line 135
    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->indexedField:Ljava/lang/String;

    .line 136
    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    .line 137
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    const-string v0, "searcher"

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->val:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 167
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 169
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 168
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;

    .line 169
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->indexedField:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->indexedField:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 6
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    const-string v2, "searcher"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/IndexSearcher;

    .line 151
    .local v1, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    invoke-virtual {v1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v2

    new-instance v3, Lorg/apache/lucene/index/Term;

    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->indexedField:Ljava/lang/String;

    iget-object v5, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    .line 152
    .local v0, "docfreq":I
    new-instance v2, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;

    invoke-direct {v2, v0, p0}, Lorg/apache/lucene/queries/function/valuesource/ConstIntDocValues;-><init>(ILorg/apache/lucene/queries/function/ValueSource;)V

    return-object v2
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 162
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->indexedField:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1d

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DocFreqValueSource;->indexedBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    const-string v0, "docfreq"

    return-object v0
.end method
