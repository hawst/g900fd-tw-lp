.class Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;
.super Lorg/apache/lucene/search/Weight;
.source "CustomScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/queries/CustomScoreQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomWeight"
.end annotation


# instance fields
.field qStrict:Z

.field subQueryWeight:Lorg/apache/lucene/search/Weight;

.field final synthetic this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

.field valSrcWeights:[Lorg/apache/lucene/search/Weight;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/CustomScoreQuery;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 3
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 188
    iput-object p1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 189
    # getter for: Lorg/apache/lucene/queries/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;
    invoke-static {p1}, Lorg/apache/lucene/queries/CustomScoreQuery;->access$1(Lorg/apache/lucene/queries/CustomScoreQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->subQueryWeight:Lorg/apache/lucene/search/Weight;

    .line 190
    # getter for: Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;
    invoke-static {p1}, Lorg/apache/lucene/queries/CustomScoreQuery;->access$2(Lorg/apache/lucene/queries/CustomScoreQuery;)[Lorg/apache/lucene/search/Query;

    move-result-object v1

    array-length v1, v1

    new-array v1, v1, [Lorg/apache/lucene/search/Weight;

    iput-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    .line 191
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    # getter for: Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;
    invoke-static {p1}, Lorg/apache/lucene/queries/CustomScoreQuery;->access$2(Lorg/apache/lucene/queries/CustomScoreQuery;)[Lorg/apache/lucene/search/Query;

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 194
    # getter for: Lorg/apache/lucene/queries/CustomScoreQuery;->strict:Z
    invoke-static {p1}, Lorg/apache/lucene/queries/CustomScoreQuery;->access$3(Lorg/apache/lucene/queries/CustomScoreQuery;)Z

    move-result v1

    iput-boolean v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->qStrict:Z

    .line 195
    return-void

    .line 192
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    # getter for: Lorg/apache/lucene/queries/CustomScoreQuery;->scoringQueries:[Lorg/apache/lucene/search/Query;
    invoke-static {p1}, Lorg/apache/lucene/queries/CustomScoreQuery;->access$2(Lorg/apache/lucene/queries/CustomScoreQuery;)[Lorg/apache/lucene/search/Query;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v2, p2}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v2

    aput-object v2, v1, v0

    .line 191
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private doExplain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 9
    .param p1, "info"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 257
    iget-object v6, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->subQueryWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v6, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v4

    .line 258
    .local v4, "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v4}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v6

    if-nez v6, :cond_0

    .line 272
    .end local v4    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :goto_0
    return-object v4

    .line 262
    .restart local v4    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    array-length v6, v6

    new-array v5, v6, [Lorg/apache/lucene/search/Explanation;

    .line 263
    .local v5, "valSrcExpls":[Lorg/apache/lucene/search/Explanation;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    array-length v6, v6

    if-lt v1, v6, :cond_1

    .line 266
    iget-object v6, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    invoke-virtual {v6, p1}, Lorg/apache/lucene/queries/CustomScoreQuery;->getCustomScoreProvider(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/CustomScoreProvider;

    move-result-object v6

    invoke-virtual {v6, p2, v4, v5}, Lorg/apache/lucene/queries/CustomScoreProvider;->customExplain(ILorg/apache/lucene/search/Explanation;[Lorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    .line 267
    .local v0, "customExp":Lorg/apache/lucene/search/Explanation;
    iget-object v6, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    invoke-virtual {v6}, Lorg/apache/lucene/queries/CustomScoreQuery;->getBoost()F

    move-result v6

    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v7

    mul-float v3, v6, v7

    .line 268
    .local v3, "sc":F
    new-instance v2, Lorg/apache/lucene/search/ComplexExplanation;

    .line 269
    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    invoke-virtual {v8}, Lorg/apache/lucene/queries/CustomScoreQuery;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ", product of:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 268
    invoke-direct {v2, v6, v3, v7}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    .line 270
    .local v2, "res":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 271
    new-instance v6, Lorg/apache/lucene/search/Explanation;

    iget-object v7, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    invoke-virtual {v7}, Lorg/apache/lucene/queries/CustomScoreQuery;->getBoost()F

    move-result v7

    const-string v8, "queryBoost"

    invoke-direct {v6, v7, v8}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v2, v6}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    move-object v4, v2

    .line 272
    goto :goto_0

    .line 264
    .end local v0    # "customExp":Lorg/apache/lucene/search/Explanation;
    .end local v2    # "res":Lorg/apache/lucene/search/Explanation;
    .end local v3    # "sc":F
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    aget-object v6, v6, v1

    invoke-virtual {v6, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v6

    aput-object v6, v5, v1

    .line 263
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 252
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->doExplain(Lorg/apache/lucene/index/AtomicReaderContext;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    .line 253
    .local v0, "explain":Lorg/apache/lucene/search/Explanation;
    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/lucene/search/Explanation;

    .end local v0    # "explain":Lorg/apache/lucene/search/Explanation;
    const/4 v1, 0x0

    const-string v2, "no matching docs"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    return-object v0
.end method

.method public getValueForNormalization()F
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 205
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->subQueryWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Weight;->getValueForNormalization()F

    move-result v0

    .line 206
    .local v0, "sum":F
    iget-object v3, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 213
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    invoke-virtual {v2}, Lorg/apache/lucene/queries/CustomScoreQuery;->getBoost()F

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    invoke-virtual {v3}, Lorg/apache/lucene/queries/CustomScoreQuery;->getBoost()F

    move-result v3

    mul-float/2addr v2, v3

    mul-float/2addr v0, v2

    .line 214
    return v0

    .line 206
    :cond_0
    aget-object v1, v3, v2

    .line 207
    .local v1, "valSrcWeight":Lorg/apache/lucene/search/Weight;
    iget-boolean v5, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->qStrict:Z

    if-eqz v5, :cond_1

    .line 208
    invoke-virtual {v1}, Lorg/apache/lucene/search/Weight;->getValueForNormalization()F

    .line 206
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 210
    :cond_1
    invoke-virtual {v1}, Lorg/apache/lucene/search/Weight;->getValueForNormalization()F

    move-result v5

    add-float/2addr v0, v5

    goto :goto_1
.end method

.method public normalize(FF)V
    .locals 6
    .param p1, "norm"    # F
    .param p2, "topLevelBoost"    # F

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 220
    iget-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/CustomScoreQuery;->getBoost()F

    move-result v1

    mul-float/2addr p2, v1

    .line 221
    iget-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->subQueryWeight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/search/Weight;->normalize(FF)V

    .line 222
    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 229
    return-void

    .line 222
    :cond_0
    aget-object v0, v2, v1

    .line 223
    .local v0, "valSrcWeight":Lorg/apache/lucene/search/Weight;
    iget-boolean v4, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->qStrict:Z

    if-eqz v4, :cond_1

    .line 224
    invoke-virtual {v0, v5, v5}, Lorg/apache/lucene/search/Weight;->normalize(FF)V

    .line 222
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 226
    :cond_1
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Weight;->normalize(FF)V

    goto :goto_1
.end method

.method public scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;
    .locals 9
    .param p1, "context"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .param p4, "acceptDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 239
    iget-object v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->subQueryWeight:Lorg/apache/lucene/search/Weight;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v2, v1, p4}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v5

    .line 240
    .local v5, "subQueryScorer":Lorg/apache/lucene/search/Scorer;
    if-nez v5, :cond_0

    .line 247
    :goto_0
    return-object v7

    .line 243
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    array-length v0, v0

    new-array v6, v0, [Lorg/apache/lucene/search/Scorer;

    .line 244
    .local v6, "valSrcScorers":[Lorg/apache/lucene/search/Scorer;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    array-length v0, v6

    if-lt v8, v0, :cond_1

    .line 247
    new-instance v0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;

    iget-object v1, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    iget-object v2, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/queries/CustomScoreQuery;->getCustomScoreProvider(Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/CustomScoreProvider;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->this$0:Lorg/apache/lucene/queries/CustomScoreQuery;

    invoke-virtual {v3}, Lorg/apache/lucene/queries/CustomScoreQuery;->getBoost()F

    move-result v4

    move-object v3, p0

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;-><init>(Lorg/apache/lucene/queries/CustomScoreQuery;Lorg/apache/lucene/queries/CustomScoreProvider;Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;FLorg/apache/lucene/search/Scorer;[Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/queries/CustomScoreQuery$CustomScorer;)V

    move-object v7, v0

    goto :goto_0

    .line 245
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/queries/CustomScoreQuery$CustomWeight;->valSrcWeights:[Lorg/apache/lucene/search/Weight;

    aget-object v0, v0, v8

    invoke-virtual {v0, p1, v2, p3, p4}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    aput-object v0, v6, v8

    .line 244
    add-int/lit8 v8, v8, 0x1

    goto :goto_1
.end method

.method public scoresDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    return v0
.end method
