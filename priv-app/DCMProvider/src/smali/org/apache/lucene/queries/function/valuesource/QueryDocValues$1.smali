.class Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;
.super Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.source "QueryValueSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    .line 199
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;-><init>()V

    .line 200
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueFloat;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueFloat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    return-void
.end method


# virtual methods
.method public fillValue(I)V
    .locals 7
    .param p1, "doc"    # I

    .prologue
    .line 210
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget-boolean v1, v1, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->noMatches:Z

    if-eqz v1, :cond_0

    .line 211
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget v2, v2, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->defVal:F

    iput v2, v1, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    .line 212
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    .line 243
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget-object v2, v2, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget-object v3, v3, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->readerContext:Lorg/apache/lucene/index/AtomicReaderContext;

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget-object v6, v6, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->acceptDocs:Lorg/apache/lucene/util/Bits;

    invoke-virtual {v2, v3, v4, v5, v6}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/AtomicReaderContext;ZZLorg/apache/lucene/util/Bits;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 216
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    const/4 v2, -0x1

    iput v2, v1, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I

    .line 217
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget-object v1, v1, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    if-nez v1, :cond_1

    .line 218
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->noMatches:Z

    .line 219
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget v2, v2, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->defVal:F

    iput v2, v1, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    .line 220
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 240
    :catch_0
    move-exception v0

    .line 241
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "caught exception in QueryDocVals("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget-object v3, v3, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->q:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") doc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 223
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iput p1, v1, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->lastDocRequested:I

    .line 225
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I

    if-ge v1, p1, :cond_2

    .line 226
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget-object v2, v2, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v2

    iput v2, v1, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I

    .line 229
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorerDoc:I

    if-le v1, p1, :cond_3

    .line 232
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget v2, v2, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->defVal:F

    iput v2, v1, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    .line 233
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z

    goto/16 :goto_0

    .line 238
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;

    iget-object v2, v2, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    iput v2, v1, Lorg/apache/lucene/util/mutable/MutableValueFloat;->value:F

    .line 239
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lorg/apache/lucene/util/mutable/MutableValueFloat;->exists:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public getValue()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/QueryDocValues$1;->mval:Lorg/apache/lucene/util/mutable/MutableValueFloat;

    return-object v0
.end method
