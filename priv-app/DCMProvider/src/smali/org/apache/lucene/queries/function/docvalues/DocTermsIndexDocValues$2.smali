.class Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$2;
.super Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.source "DocTermsIndexDocValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mval:Lorg/apache/lucene/util/mutable/MutableValueStr;

.field final synthetic this$0:Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$2;->this$0:Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;

    .line 141
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;-><init>()V

    .line 142
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueStr;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueStr;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueStr;

    return-void
.end method


# virtual methods
.method public fillValue(I)V
    .locals 3
    .param p1, "doc"    # I

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$2;->this$0:Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;

    iget-object v0, v0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues;->termsIndex:Lorg/apache/lucene/index/SortedDocValues;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueStr;

    iget-object v1, v1, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/index/SortedDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 152
    iget-object v1, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueStr;

    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueStr;

    iget-object v0, v0, Lorg/apache/lucene/util/mutable/MutableValueStr;->value:Lorg/apache/lucene/util/BytesRef;

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    sget-object v2, Lorg/apache/lucene/index/SortedDocValues;->MISSING:[B

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lorg/apache/lucene/util/mutable/MutableValueStr;->exists:Z

    .line 153
    return-void

    .line 152
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getValue()Lorg/apache/lucene/util/mutable/MutableValue;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/lucene/queries/function/docvalues/DocTermsIndexDocValues$2;->mval:Lorg/apache/lucene/util/mutable/MutableValueStr;

    return-object v0
.end method
