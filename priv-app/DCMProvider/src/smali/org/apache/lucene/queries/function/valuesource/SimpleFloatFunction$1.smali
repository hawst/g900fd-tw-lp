.class Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction$1;
.super Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;
.source "SimpleFloatFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction;

.field private final synthetic val$vals:Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 40
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public floatVal(I)F
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction;->func(ILorg/apache/lucene/queries/function/FunctionValues;)F

    move-result v0

    return v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/SimpleFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
