.class Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction$1;
.super Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;
.source "LinearFloatFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;

.field private final synthetic val$vals:Lorg/apache/lucene/queries/function/FunctionValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    .line 56
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/FloatDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public floatVal(I)F
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->floatVal(I)F

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->slope:F

    mul-float/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->intercept:F

    add-float/2addr v0, v1

    return v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->slope:F

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "*float("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction$1;->val$vals:Lorg/apache/lucene/queries/function/FunctionValues;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/queries/function/FunctionValues;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;

    iget v1, v1, Lorg/apache/lucene/queries/function/valuesource/LinearFloatFunction;->intercept:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
