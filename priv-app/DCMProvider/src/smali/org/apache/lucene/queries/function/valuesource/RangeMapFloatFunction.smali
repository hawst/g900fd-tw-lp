.class public Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;
.super Lorg/apache/lucene/queries/function/ValueSource;
.source "RangeMapFloatFunction.java"


# instance fields
.field protected final defaultVal:Ljava/lang/Float;

.field protected final max:F

.field protected final min:F

.field protected final source:Lorg/apache/lucene/queries/function/ValueSource;

.field protected final target:F


# direct methods
.method public constructor <init>(Lorg/apache/lucene/queries/function/ValueSource;FFFLjava/lang/Float;)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/queries/function/ValueSource;
    .param p2, "min"    # F
    .param p3, "max"    # F
    .param p4, "target"    # F
    .param p5, "def"    # Ljava/lang/Float;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/queries/function/ValueSource;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    .line 46
    iput p2, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->min:F

    .line 47
    iput p3, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->max:F

    .line 48
    iput p4, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->target:F

    .line 49
    iput-object p5, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->defaultVal:Ljava/lang/Float;

    .line 50
    return-void
.end method


# virtual methods
.method public createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->createWeight(Ljava/util/Map;Lorg/apache/lucene/search/IndexSearcher;)V

    .line 76
    return-void
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "map("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->min:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->max:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->target:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 94
    const-class v2, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 95
    check-cast v0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;

    .line 96
    .local v0, "other":Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->min:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->min:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 97
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->max:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->max:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 98
    iget v2, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->target:F

    iget v3, v0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->target:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 99
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/queries/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 100
    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->defaultVal:Ljava/lang/Float;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->defaultVal:Ljava/lang/Float;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->defaultVal:Ljava/lang/Float;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->defaultVal:Ljava/lang/Float;

    iget-object v3, v0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->defaultVal:Ljava/lang/Float;

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 96
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
    .locals 2
    .param p1, "context"    # Ljava/util/Map;
    .param p2, "readerContext"    # Lorg/apache/lucene/index/AtomicReaderContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/queries/function/ValueSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;

    move-result-object v0

    .line 60
    .local v0, "vals":Lorg/apache/lucene/queries/function/FunctionValues;
    new-instance v1, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;

    invoke-direct {v1, p0, p0, v0}, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/queries/function/FunctionValues;)V

    return-object v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 80
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->source:Lorg/apache/lucene/queries/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/ValueSource;->hashCode()I

    move-result v0

    .line 81
    .local v0, "h":I
    shl-int/lit8 v1, v0, 0xa

    ushr-int/lit8 v2, v0, 0x17

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 82
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->min:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    shl-int/lit8 v1, v0, 0xe

    ushr-int/lit8 v2, v0, 0x13

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 84
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->max:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    shl-int/lit8 v1, v0, 0xd

    ushr-int/lit8 v2, v0, 0x14

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 86
    iget v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->target:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->defaultVal:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/RangeMapFloatFunction;->defaultVal:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_0
    return v0
.end method
