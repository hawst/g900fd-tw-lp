.class Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;
.super Lorg/apache/lucene/queries/function/docvalues/DoubleDocValues;
.source "DoubleFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;

.field private final synthetic val$arr:Lorg/apache/lucene/search/FieldCache$Doubles;

.field private final synthetic val$valid:Lorg/apache/lucene/util/Bits;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/search/FieldCache$Doubles;Lorg/apache/lucene/util/Bits;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Doubles;

    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    .line 63
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/DoubleDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    return-void
.end method


# virtual methods
.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Doubles;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Doubles;->get(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public exists(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    return v0
.end method

.method public getRangeScorer(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/queries/function/ValueSourceScorer;
    .locals 12
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "lowerVal"    # Ljava/lang/String;
    .param p3, "upperVal"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z

    .prologue
    .line 78
    if-nez p2, :cond_0

    .line 79
    const-wide/high16 v8, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    .line 84
    .local v8, "lower":D
    :goto_0
    if-nez p3, :cond_1

    .line 85
    const-wide/high16 v10, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    .line 90
    .local v10, "upper":D
    :goto_1
    move-wide v4, v8

    .line 91
    .local v4, "l":D
    move-wide v6, v10

    .line 94
    .local v6, "u":D
    if-eqz p4, :cond_2

    if-eqz p5, :cond_2

    .line 95
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;DD)V

    .line 122
    :goto_2
    return-object v0

    .line 81
    .end local v4    # "l":D
    .end local v6    # "u":D
    .end local v8    # "lower":D
    .end local v10    # "upper":D
    :cond_0
    invoke-static {p2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .restart local v8    # "lower":D
    goto :goto_0

    .line 87
    :cond_1
    invoke-static {p3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    .restart local v10    # "upper":D
    goto :goto_1

    .line 103
    .restart local v4    # "l":D
    .restart local v6    # "u":D
    :cond_2
    if-eqz p4, :cond_3

    if-nez p5, :cond_3

    .line 104
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$2;-><init>(Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;DD)V

    goto :goto_2

    .line 112
    :cond_3
    if-nez p4, :cond_4

    if-eqz p5, :cond_4

    .line 113
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$3;-><init>(Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;DD)V

    goto :goto_2

    .line 122
    :cond_4
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$4;-><init>(Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;DD)V

    goto :goto_2
.end method

.method public getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    .locals 3

    .prologue
    .line 134
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Doubles;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1$5;-><init>(Lorg/apache/lucene/queries/function/valuesource/DoubleFieldSource$1;Lorg/apache/lucene/search/FieldCache$Doubles;Lorg/apache/lucene/util/Bits;)V

    return-object v0
.end method
