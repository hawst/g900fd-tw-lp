.class Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;
.super Lorg/apache/lucene/queries/function/docvalues/IntDocValues;
.source "IntFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/queries/function/valuesource/IntFieldSource;->getValues(Ljava/util/Map;Lorg/apache/lucene/index/AtomicReaderContext;)Lorg/apache/lucene/queries/function/FunctionValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/queries/function/valuesource/IntFieldSource;

.field final val:Lorg/apache/lucene/util/mutable/MutableValueInt;

.field private final synthetic val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

.field private final synthetic val$valid:Lorg/apache/lucene/util/Bits;


# direct methods
.method constructor <init>(Lorg/apache/lucene/queries/function/valuesource/IntFieldSource;Lorg/apache/lucene/queries/function/ValueSource;Lorg/apache/lucene/search/FieldCache$Ints;Lorg/apache/lucene/util/Bits;)V
    .locals 1
    .param p2, "$anonymous0"    # Lorg/apache/lucene/queries/function/ValueSource;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/IntFieldSource;

    iput-object p3, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

    iput-object p4, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    .line 63
    invoke-direct {p0, p2}, Lorg/apache/lucene/queries/function/docvalues/IntDocValues;-><init>(Lorg/apache/lucene/queries/function/ValueSource;)V

    .line 64
    new-instance v0, Lorg/apache/lucene/util/mutable/MutableValueInt;

    invoke-direct {v0}, Lorg/apache/lucene/util/mutable/MutableValueInt;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val:Lorg/apache/lucene/util/mutable/MutableValueInt;

    return-void
.end method


# virtual methods
.method public doubleVal(I)D
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Ints;->get(I)I

    move-result v0

    int-to-double v0, v0

    return-wide v0
.end method

.method public exists(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    return v0
.end method

.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Ints;->get(I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public getRangeScorer(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/queries/function/ValueSourceScorer;
    .locals 9
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "lowerVal"    # Ljava/lang/String;
    .param p3, "upperVal"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z

    .prologue
    .line 112
    if-nez p2, :cond_2

    .line 113
    const/high16 v7, -0x80000000

    .line 119
    .local v7, "lower":I
    :cond_0
    :goto_0
    if-nez p3, :cond_3

    .line 120
    const v8, 0x7fffffff

    .line 126
    .local v8, "upper":I
    :cond_1
    :goto_1
    move v5, v7

    .line 127
    .local v5, "ll":I
    move v6, v8

    .line 129
    .local v6, "uu":I
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$1;

    iget-object v4, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$1;-><init>(Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/queries/function/FunctionValues;Lorg/apache/lucene/search/FieldCache$Ints;II)V

    return-object v0

    .line 115
    .end local v5    # "ll":I
    .end local v6    # "uu":I
    .end local v7    # "lower":I
    .end local v8    # "upper":I
    :cond_2
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 116
    .restart local v7    # "lower":I
    if-nez p4, :cond_0

    const v0, 0x7fffffff

    if-ge v7, v0, :cond_0

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 122
    :cond_3
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 123
    .restart local v8    # "upper":I
    if-nez p5, :cond_1

    const/high16 v0, -0x80000000

    if-le v8, v0, :cond_1

    add-int/lit8 v8, v8, -0x1

    goto :goto_1
.end method

.method public getValueFiller()Lorg/apache/lucene/queries/function/FunctionValues$ValueFiller;
    .locals 3

    .prologue
    .line 142
    new-instance v0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

    iget-object v2, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1$2;-><init>(Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;Lorg/apache/lucene/search/FieldCache$Ints;Lorg/apache/lucene/util/Bits;)V

    return-object v0
.end method

.method public intVal(I)I
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Ints;->get(I)I

    move-result v0

    return v0
.end method

.method public longVal(I)J
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Ints;->get(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public objectVal(I)Ljava/lang/Object;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$valid:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Ints;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public strVal(I)Ljava/lang/String;
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->val$arr:Lorg/apache/lucene/search/FieldCache$Ints;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCache$Ints;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->this$0:Lorg/apache/lucene/queries/function/valuesource/IntFieldSource;

    invoke-virtual {v1}, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/queries/function/valuesource/IntFieldSource$1;->intVal(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
