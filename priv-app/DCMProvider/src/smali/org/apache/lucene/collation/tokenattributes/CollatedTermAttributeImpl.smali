.class public Lorg/apache/lucene/collation/tokenattributes/CollatedTermAttributeImpl;
.super Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
.source "CollatedTermAttributeImpl.java"


# instance fields
.field private final collator:Ljava/text/Collator;


# direct methods
.method public constructor <init>(Ljava/text/Collator;)V
    .locals 1
    .param p1, "collator"    # Ljava/text/Collator;

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;-><init>()V

    .line 39
    invoke-virtual {p1}, Ljava/text/Collator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/Collator;

    iput-object v0, p0, Lorg/apache/lucene/collation/tokenattributes/CollatedTermAttributeImpl;->collator:Ljava/text/Collator;

    .line 40
    return-void
.end method


# virtual methods
.method public fillBytesRef()I
    .locals 3

    .prologue
    .line 44
    invoke-virtual {p0}, Lorg/apache/lucene/collation/tokenattributes/CollatedTermAttributeImpl;->getBytesRef()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 45
    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    iget-object v1, p0, Lorg/apache/lucene/collation/tokenattributes/CollatedTermAttributeImpl;->collator:Ljava/text/Collator;

    invoke-virtual {p0}, Lorg/apache/lucene/collation/tokenattributes/CollatedTermAttributeImpl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/Collator;->getCollationKey(Ljava/lang/String;)Ljava/text/CollationKey;

    move-result-object v1

    invoke-virtual {v1}, Ljava/text/CollationKey;->toByteArray()[B

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 46
    const/4 v1, 0x0

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 47
    iget-object v1, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 48
    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v1

    return v1
.end method
