.class public final Lorg/apache/lucene/collation/CollationKeyAnalyzer;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "CollationKeyAnalyzer.java"


# instance fields
.field private final collator:Ljava/text/Collator;

.field private final factory:Lorg/apache/lucene/collation/CollationAttributeFactory;

.field private final matchVersion:Lorg/apache/lucene/util/Version;


# direct methods
.method public constructor <init>(Ljava/text/Collator;)V
    .locals 1
    .param p1, "collator"    # Ljava/text/Collator;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 108
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/collation/CollationKeyAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/text/Collator;)V

    .line 109
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/text/Collator;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "collator"    # Ljava/text/Collator;

    .prologue
    .line 96
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    .line 97
    iput-object p1, p0, Lorg/apache/lucene/collation/CollationKeyAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 98
    iput-object p2, p0, Lorg/apache/lucene/collation/CollationKeyAnalyzer;->collator:Ljava/text/Collator;

    .line 99
    new-instance v0, Lorg/apache/lucene/collation/CollationAttributeFactory;

    invoke-direct {v0, p2}, Lorg/apache/lucene/collation/CollationAttributeFactory;-><init>(Ljava/text/Collator;)V

    iput-object v0, p0, Lorg/apache/lucene/collation/CollationKeyAnalyzer;->factory:Lorg/apache/lucene/collation/CollationAttributeFactory;

    .line 100
    return-void
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 114
    iget-object v1, p0, Lorg/apache/lucene/collation/CollationKeyAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_40:Lorg/apache/lucene/util/Version;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    new-instance v0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;

    iget-object v1, p0, Lorg/apache/lucene/collation/CollationKeyAnalyzer;->factory:Lorg/apache/lucene/collation/CollationAttributeFactory;

    const/16 v2, 0x100

    invoke-direct {v0, v1, p2, v2}, Lorg/apache/lucene/analysis/core/KeywordTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;I)V

    .line 116
    .local v0, "tokenizer":Lorg/apache/lucene/analysis/core/KeywordTokenizer;
    new-instance v1, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v1, v0, v0}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 119
    :goto_0
    return-object v1

    .line 118
    .end local v0    # "tokenizer":Lorg/apache/lucene/analysis/core/KeywordTokenizer;
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;

    invoke-direct {v0, p2}, Lorg/apache/lucene/analysis/core/KeywordTokenizer;-><init>(Ljava/io/Reader;)V

    .line 119
    .restart local v0    # "tokenizer":Lorg/apache/lucene/analysis/core/KeywordTokenizer;
    new-instance v1, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    new-instance v2, Lorg/apache/lucene/collation/CollationKeyFilter;

    iget-object v3, p0, Lorg/apache/lucene/collation/CollationKeyAnalyzer;->collator:Ljava/text/Collator;

    invoke-direct {v2, v0, v3}, Lorg/apache/lucene/collation/CollationKeyFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/text/Collator;)V

    invoke-direct {v1, v0, v2}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    goto :goto_0
.end method
