.class public Lorg/apache/lucene/collation/CollationAttributeFactory;
.super Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
.source "CollationAttributeFactory.java"


# instance fields
.field private final collator:Ljava/text/Collator;

.field private final delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;


# direct methods
.method public constructor <init>(Ljava/text/Collator;)V
    .locals 1
    .param p1, "collator"    # Ljava/text/Collator;

    .prologue
    .line 82
    sget-object v0, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/collation/CollationAttributeFactory;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/text/Collator;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/text/Collator;)V
    .locals 0
    .param p1, "delegate"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "collator"    # Ljava/text/Collator;

    .prologue
    .line 91
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;-><init>()V

    .line 92
    iput-object p1, p0, Lorg/apache/lucene/collation/CollationAttributeFactory;->delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .line 93
    iput-object p2, p0, Lorg/apache/lucene/collation/CollationAttributeFactory;->collator:Ljava/text/Collator;

    .line 94
    return-void
.end method


# virtual methods
.method public createAttributeInstance(Ljava/lang/Class;)Lorg/apache/lucene/util/AttributeImpl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;)",
            "Lorg/apache/lucene/util/AttributeImpl;"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "attClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;"
    const-class v0, Lorg/apache/lucene/collation/tokenattributes/CollatedTermAttributeImpl;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    new-instance v0, Lorg/apache/lucene/collation/tokenattributes/CollatedTermAttributeImpl;

    iget-object v1, p0, Lorg/apache/lucene/collation/CollationAttributeFactory;->collator:Ljava/text/Collator;

    invoke-direct {v0, v1}, Lorg/apache/lucene/collation/tokenattributes/CollatedTermAttributeImpl;-><init>(Ljava/text/Collator;)V

    .line 99
    :goto_0
    return-object v0

    .line 101
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationAttributeFactory;->delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->createAttributeInstance(Ljava/lang/Class;)Lorg/apache/lucene/util/AttributeImpl;

    move-result-object v0

    goto :goto_0
.end method
