.class public Lorg/apache/lucene/collation/CollationKeyFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "CollationKeyFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/MultiTermAwareComponent;
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private collator:Ljava/text/Collator;

.field private final country:Ljava/lang/String;

.field private final custom:Ljava/lang/String;

.field private final decomposition:Ljava/lang/String;

.field private final language:Ljava/lang/String;

.field private final strength:Ljava/lang/String;

.field private final variant:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 85
    const-string v0, "custom"

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->custom:Ljava/lang/String;

    .line 86
    const-string v0, "language"

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->language:Ljava/lang/String;

    .line 87
    const-string v0, "country"

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->country:Ljava/lang/String;

    .line 88
    const-string v0, "variant"

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->variant:Ljava/lang/String;

    .line 89
    const-string v0, "strength"

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->strength:Ljava/lang/String;

    .line 90
    const-string v0, "decomposition"

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->decomposition:Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->custom:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->language:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Either custom or language is required."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->custom:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 96
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->language:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->country:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->variant:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 97
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot specify both language and custom. To tailor rules for a built-in language, see the javadocs for RuleBasedCollator. Then save the entire customized ruleset to a file, and use with the custom parameter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_2
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 102
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_3
    return-void
.end method

.method private createFromLocale(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/text/Collator;
    .locals 3
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;
    .param p3, "variant"    # Ljava/lang/String;

    .prologue
    .line 153
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    if-eqz p3, :cond_0

    .line 154
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "To specify variant, country is required"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 155
    :cond_0
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    .line 156
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p1, p2, p3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    .local v0, "locale":Ljava/util/Locale;
    :goto_0
    invoke-static {v0}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v1

    return-object v1

    .line 157
    .end local v0    # "locale":Ljava/util/Locale;
    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 158
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p1, p2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v0    # "locale":Ljava/util/Locale;
    goto :goto_0

    .line 160
    .end local v0    # "locale":Ljava/util/Locale;
    :cond_2
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .restart local v0    # "locale":Ljava/util/Locale;
    goto :goto_0
.end method

.method private createFromRules(Ljava/lang/String;Lorg/apache/lucene/analysis/util/ResourceLoader;)Ljava/text/Collator;
    .locals 7
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 170
    const/4 v1, 0x0

    .line 172
    .local v1, "input":Ljava/io/InputStream;
    :try_start_0
    invoke-interface {p2, p1}, Lorg/apache/lucene/analysis/util/ResourceLoader;->openResource(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 173
    invoke-direct {p0, v1}, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->toUTF8String(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 174
    .local v2, "rules":Ljava/lang/String;
    new-instance v3, Ljava/text/RuleBasedCollator;

    invoke-direct {v3, v2}, Ljava/text/RuleBasedCollator;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    new-array v4, v6, [Ljava/io/Closeable;

    .line 179
    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 174
    return-object v3

    .line 175
    .end local v2    # "rules":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/text/ParseException;
    :try_start_1
    new-instance v3, Ljava/io/IOException;

    const-string v4, "ParseException thrown while parsing rules"

    invoke-direct {v3, v4, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178
    .end local v0    # "e":Ljava/text/ParseException;
    :catchall_0
    move-exception v3

    new-array v4, v6, [Ljava/io/Closeable;

    .line 179
    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 180
    throw v3
.end method

.method private toUTF8String(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 5
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 189
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/16 v4, 0x400

    new-array v0, v4, [C

    .line 191
    .local v0, "buffer":[C
    sget-object v4, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-static {p1, v4}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Ljava/io/Reader;

    move-result-object v2

    .line 192
    .local v2, "r":Ljava/io/Reader;
    const/4 v1, 0x0

    .line 193
    .local v1, "len":I
    :goto_0
    invoke-virtual {v2, v0}, Ljava/io/Reader;->read([C)I

    move-result v1

    if-gtz v1, :cond_0

    .line 196
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 194
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 143
    new-instance v0, Lorg/apache/lucene/collation/CollationKeyFilter;

    iget-object v1, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->collator:Ljava/text/Collator;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/collation/CollationKeyFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/text/Collator;)V

    return-object v0
.end method

.method public getMultiTermComponent()Lorg/apache/lucene/analysis/util/AbstractAnalysisFactory;
    .locals 0

    .prologue
    .line 185
    return-object p0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 6
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 107
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->language:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 109
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->language:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->country:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->variant:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->createFromLocale(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->collator:Ljava/text/Collator;

    .line 116
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->strength:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->strength:Ljava/lang/String;

    const-string v1, "primary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 118
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->collator:Ljava/text/Collator;

    invoke-virtual {v0, v3}, Ljava/text/Collator;->setStrength(I)V

    .line 130
    :cond_0
    :goto_1
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->decomposition:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->decomposition:Ljava/lang/String;

    const-string v1, "no"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->collator:Ljava/text/Collator;

    invoke-virtual {v0, v3}, Ljava/text/Collator;->setDecomposition(I)V

    .line 140
    :cond_1
    :goto_2
    return-void

    .line 112
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->custom:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->createFromRules(Ljava/lang/String;Lorg/apache/lucene/analysis/util/ResourceLoader;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->collator:Ljava/text/Collator;

    goto :goto_0

    .line 119
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->strength:Ljava/lang/String;

    const-string v1, "secondary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 120
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->collator:Ljava/text/Collator;

    invoke-virtual {v0, v4}, Ljava/text/Collator;->setStrength(I)V

    goto :goto_1

    .line 121
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->strength:Ljava/lang/String;

    const-string v1, "tertiary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 122
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->collator:Ljava/text/Collator;

    invoke-virtual {v0, v5}, Ljava/text/Collator;->setStrength(I)V

    goto :goto_1

    .line 123
    :cond_5
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->strength:Ljava/lang/String;

    const-string v1, "identical"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 124
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->collator:Ljava/text/Collator;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/text/Collator;->setStrength(I)V

    goto :goto_1

    .line 126
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid strength: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->strength:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_7
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->decomposition:Ljava/lang/String;

    const-string v1, "canonical"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 134
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->collator:Ljava/text/Collator;

    invoke-virtual {v0, v4}, Ljava/text/Collator;->setDecomposition(I)V

    goto :goto_2

    .line 135
    :cond_8
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->decomposition:Ljava/lang/String;

    const-string v1, "full"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 136
    iget-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->collator:Ljava/text/Collator;

    invoke-virtual {v0, v5}, Ljava/text/Collator;->setDecomposition(I)V

    goto :goto_2

    .line 138
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid decomposition: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/collation/CollationKeyFilterFactory;->decomposition:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
