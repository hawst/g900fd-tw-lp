.class public final Lorg/apache/lucene/collation/CollationKeyFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "CollationKeyFilter.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final collator:Ljava/text/Collator;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/text/Collator;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "collator"    # Ljava/text/Collator;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 80
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/collation/CollationKeyFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 90
    invoke-virtual {p2}, Ljava/text/Collator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/Collator;

    iput-object v0, p0, Lorg/apache/lucene/collation/CollationKeyFilter;->collator:Ljava/text/Collator;

    .line 91
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v2, p0, Lorg/apache/lucene/collation/CollationKeyFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 96
    iget-object v2, p0, Lorg/apache/lucene/collation/CollationKeyFilter;->collator:Ljava/text/Collator;

    iget-object v3, p0, Lorg/apache/lucene/collation/CollationKeyFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/Collator;->getCollationKey(Ljava/lang/String;)Ljava/text/CollationKey;

    move-result-object v2

    invoke-virtual {v2}, Ljava/text/CollationKey;->toByteArray()[B

    move-result-object v0

    .line 98
    .local v0, "collationKey":[B
    array-length v2, v0

    .line 97
    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->getEncodedLength([BII)I

    move-result v5

    .line 99
    .local v5, "encodedLength":I
    iget-object v2, p0, Lorg/apache/lucene/collation/CollationKeyFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2, v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    .line 100
    iget-object v2, p0, Lorg/apache/lucene/collation/CollationKeyFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2, v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 101
    array-length v2, v0

    .line 102
    iget-object v3, p0, Lorg/apache/lucene/collation/CollationKeyFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v3

    move v4, v1

    .line 101
    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->encode([BII[CII)V

    .line 103
    const/4 v1, 0x1

    .line 105
    .end local v0    # "collationKey":[B
    .end local v5    # "encodedLength":I
    :cond_0
    return v1
.end method
