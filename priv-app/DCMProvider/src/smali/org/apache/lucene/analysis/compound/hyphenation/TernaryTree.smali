.class public Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;
.super Ljava/lang/Object;
.source "TernaryTree.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;
    }
.end annotation


# static fields
.field protected static final BLOCK_SIZE:I = 0x800


# instance fields
.field protected eq:[C

.field protected freenode:C

.field protected hi:[C

.field protected kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

.field protected length:I

.field protected lo:[C

.field protected root:C

.field protected sc:[C


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->init()V

    .line 123
    return-void
.end method

.method private compact(Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;C)V
    .locals 4
    .param p1, "kx"    # Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;
    .param p2, "map"    # Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;
    .param p3, "p"    # C

    .prologue
    .line 436
    if-nez p3, :cond_0

    .line 454
    :goto_0
    return-void

    .line 439
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aget-char v1, v1, p3

    const v2, 0xffff

    if-ne v1, v2, :cond_2

    .line 440
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->getArray()[C

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v2, v2, p3

    invoke-virtual {p2, v1, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->find([CI)I

    move-result v0

    .line 441
    .local v0, "k":I
    if-gez v0, :cond_1

    .line 442
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->getArray()[C

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v2, v2, p3

    invoke-static {v1, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->strlen([CI)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->alloc(I)I

    move-result v0

    .line 443
    invoke-virtual {p1}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->getArray()[C

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->getArray()[C

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v3, v3, p3

    invoke-static {v1, v0, v2, v3}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->strcpy([CI[CI)V

    .line 444
    invoke-virtual {p1}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->getArray()[C

    move-result-object v1

    int-to-char v2, v0

    invoke-virtual {p2, v1, v0, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insert([CIC)V

    .line 446
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    int-to-char v2, v0

    aput-char v2, v1, p3

    goto :goto_0

    .line 448
    .end local v0    # "k":I
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v1, v1, p3

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->compact(Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;C)V

    .line 449
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aget-char v1, v1, p3

    if-eqz v1, :cond_3

    .line 450
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    aget-char v1, v1, p3

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->compact(Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;C)V

    .line 452
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    aget-char v1, v1, p3

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->compact(Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;C)V

    goto :goto_0
.end method

.method private insert(C[CIC)C
    .locals 9
    .param p1, "p"    # C
    .param p2, "key"    # [C
    .param p3, "start"    # I
    .param p4, "val"    # C

    .prologue
    const v8, 0xffff

    const/4 v7, 0x0

    .line 166
    invoke-static {p2, p3}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->strlen([CI)I

    move-result v0

    .line 167
    .local v0, "len":I
    if-nez p1, :cond_1

    .line 171
    iget-char p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->freenode:C

    .end local p1    # "p":C
    add-int/lit8 v4, p1, 0x1

    int-to-char v4, v4

    iput-char v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->freenode:C

    .line 172
    .restart local p1    # "p":C
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    aput-char p4, v4, p1

    .line 173
    iget v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->length:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->length:I

    .line 174
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    aput-char v7, v4, p1

    .line 175
    if-lez v0, :cond_0

    .line 176
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aput-char v8, v4, p1

    .line 177
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v5, v6}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->alloc(I)I

    move-result v5

    int-to-char v5, v5

    aput-char v5, v4, p1

    .line 178
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-virtual {v4}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->getArray()[C

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v5, v5, p1

    invoke-static {v4, v5, p2, p3}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->strcpy([CI[CI)V

    :goto_0
    move v1, p1

    .line 231
    .end local p1    # "p":C
    .local v1, "p":C
    :goto_1
    return v1

    .line 180
    .end local v1    # "p":C
    .restart local p1    # "p":C
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aput-char v7, v4, p1

    .line 181
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aput-char v7, v4, p1

    goto :goto_0

    .line 186
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aget-char v4, v4, p1

    if-ne v4, v8, :cond_2

    .line 190
    iget-char v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->freenode:C

    add-int/lit8 v4, v2, 0x1

    int-to-char v4, v4

    iput-char v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->freenode:C

    .line 191
    .local v2, "pp":C
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v5, v5, p1

    aput-char v5, v4, v2

    .line 192
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    aget-char v5, v5, p1

    aput-char v5, v4, v2

    .line 193
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aput-char v7, v4, p1

    .line 194
    if-lez v0, :cond_4

    .line 195
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    iget-object v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v6, v6, v2

    invoke-virtual {v5, v6}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->get(I)C

    move-result v5

    aput-char v5, v4, p1

    .line 196
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    aput-char v2, v4, p1

    .line 197
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v5, v4, v2

    add-int/lit8 v5, v5, 0x1

    int-to-char v5, v5

    aput-char v5, v4, v2

    .line 198
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v5, v5, v2

    invoke-virtual {v4, v5}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->get(I)C

    move-result v4

    if-nez v4, :cond_3

    .line 200
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aput-char v7, v4, v2

    .line 201
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aput-char v7, v4, v2

    .line 202
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    aput-char v7, v4, v2

    .line 218
    .end local v2    # "pp":C
    :cond_2
    :goto_2
    aget-char v3, p2, p3

    .line 219
    .local v3, "s":C
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aget-char v4, v4, p1

    if-ge v3, v4, :cond_5

    .line 220
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v5, v5, p1

    invoke-direct {p0, v5, p2, p3, p4}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insert(C[CIC)C

    move-result v5

    aput-char v5, v4, p1

    :goto_3
    move v1, p1

    .line 231
    .end local p1    # "p":C
    .restart local v1    # "p":C
    goto :goto_1

    .line 205
    .end local v1    # "p":C
    .end local v3    # "s":C
    .restart local v2    # "pp":C
    .restart local p1    # "p":C
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aput-char v8, v4, v2

    goto :goto_2

    .line 210
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aput-char v8, v4, v2

    .line 211
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    aput-char v2, v4, p1

    .line 212
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aput-char v7, v4, p1

    .line 213
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    aput-char p4, v4, p1

    .line 214
    iget v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->length:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->length:I

    move v1, p1

    .line 215
    .end local p1    # "p":C
    .restart local v1    # "p":C
    goto/16 :goto_1

    .line 221
    .end local v1    # "p":C
    .end local v2    # "pp":C
    .restart local v3    # "s":C
    .restart local p1    # "p":C
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aget-char v4, v4, p1

    if-ne v3, v4, :cond_7

    .line 222
    if-eqz v3, :cond_6

    .line 223
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    aget-char v5, v5, p1

    add-int/lit8 v6, p3, 0x1

    invoke-direct {p0, v5, p2, v6, p4}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insert(C[CIC)C

    move-result v5

    aput-char v5, v4, p1

    goto :goto_3

    .line 226
    :cond_6
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    aput-char p4, v4, p1

    goto :goto_3

    .line 229
    :cond_7
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    aget-char v5, v5, p1

    invoke-direct {p0, v5, p2, p3, p4}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insert(C[CIC)C

    move-result v5

    aput-char v5, v4, p1

    goto :goto_3
.end method

.method private redimNodeArrays(I)V
    .locals 4
    .param p1, "newsize"    # I

    .prologue
    const/4 v3, 0x0

    .line 332
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    array-length v2, v2

    if-ge p1, v2, :cond_0

    move v0, p1

    .line 333
    .local v0, "len":I
    :goto_0
    new-array v1, p1, [C

    .line 334
    .local v1, "na":[C
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 335
    iput-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    .line 336
    new-array v1, p1, [C

    .line 337
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 338
    iput-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    .line 339
    new-array v1, p1, [C

    .line 340
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 341
    iput-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    .line 342
    new-array v1, p1, [C

    .line 343
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 344
    iput-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    .line 345
    return-void

    .line 332
    .end local v0    # "len":I
    .end local v1    # "na":[C
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    array-length v0, v2

    goto :goto_0
.end method

.method public static strcmp(Ljava/lang/String;[CI)I
    .locals 5
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "a"    # [C
    .param p2, "start"    # I

    .prologue
    .line 250
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 251
    .local v2, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 260
    add-int v3, p2, v1

    aget-char v3, p1, v3

    if-eqz v3, :cond_2

    .line 261
    add-int v3, p2, v1

    aget-char v3, p1, v3

    neg-int v0, v3

    .line 263
    :cond_0
    :goto_1
    return v0

    .line 252
    :cond_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int v4, p2, v1

    aget-char v4, p1, v4

    sub-int v0, v3, v4

    .line 253
    .local v0, "d":I
    if-nez v0, :cond_0

    .line 256
    add-int v3, p2, v1

    aget-char v3, p1, v3

    if-eqz v3, :cond_0

    .line 251
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 263
    .end local v0    # "d":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static strcmp([CI[CI)I
    .locals 2
    .param p0, "a"    # [C
    .param p1, "startA"    # I
    .param p2, "b"    # [C
    .param p3, "startB"    # I

    .prologue
    .line 238
    :goto_0
    aget-char v0, p0, p1

    aget-char v1, p2, p3

    if-eq v0, v1, :cond_0

    .line 243
    aget-char v0, p0, p1

    aget-char v1, p2, p3

    sub-int/2addr v0, v1

    :goto_1
    return v0

    .line 239
    :cond_0
    aget-char v0, p0, p1

    if-nez v0, :cond_1

    .line 240
    const/4 v0, 0x0

    goto :goto_1

    .line 238
    :cond_1
    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p3, p3, 0x1

    goto :goto_0
.end method

.method public static strcpy([CI[CI)V
    .locals 3
    .param p0, "dst"    # [C
    .param p1, "di"    # I
    .param p2, "src"    # [C
    .param p3, "si"    # I

    .prologue
    .line 268
    :goto_0
    aget-char v2, p2, p3

    if-nez v2, :cond_0

    .line 271
    const/4 v2, 0x0

    aput-char v2, p0, p1

    .line 272
    return-void

    .line 269
    :cond_0
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "di":I
    .local v0, "di":I
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "si":I
    .local v1, "si":I
    aget-char v2, p2, p3

    aput-char v2, p0, p1

    move p3, v1

    .end local v1    # "si":I
    .restart local p3    # "si":I
    move p1, v0

    .end local v0    # "di":I
    .restart local p1    # "di":I
    goto :goto_0
.end method

.method public static strlen([C)I
    .locals 1
    .param p0, "a"    # [C

    .prologue
    .line 283
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->strlen([CI)I

    move-result v0

    return v0
.end method

.method public static strlen([CI)I
    .locals 3
    .param p0, "a"    # [C
    .param p1, "start"    # I

    .prologue
    .line 275
    const/4 v1, 0x0

    .line 276
    .local v1, "len":I
    move v0, p1

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    aget-char v2, p0, v0

    if-nez v2, :cond_1

    .line 279
    :cond_0
    return v1

    .line 277
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 276
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public balance()V
    .locals 7

    .prologue
    .line 391
    const/4 v0, 0x0

    .local v0, "i":I
    iget v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->length:I

    .line 392
    .local v4, "n":I
    new-array v3, v4, [Ljava/lang/String;

    .line 393
    .local v3, "k":[Ljava/lang/String;
    new-array v5, v4, [C

    .line 394
    .local v5, "v":[C
    new-instance v2, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;

    invoke-direct {v2, p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;-><init>(Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;)V

    .line 395
    .local v2, "iter":Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;
    :goto_0
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_0

    .line 399
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->init()V

    .line 400
    const/4 v6, 0x0

    invoke-virtual {p0, v3, v5, v6, v4}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insertBalanced([Ljava/lang/String;[CII)V

    .line 405
    return-void

    .line 396
    :cond_0
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->getValue()C

    move-result v6

    aput-char v6, v5, v0

    .line 397
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->nextElement()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->clone()Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;
    .locals 2

    .prologue
    .line 353
    new-instance v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;-><init>()V

    .line 354
    .local v0, "t":Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    invoke-virtual {v1}, [C->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iput-object v1, v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    .line 355
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    invoke-virtual {v1}, [C->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iput-object v1, v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    .line 356
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    invoke-virtual {v1}, [C->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iput-object v1, v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    .line 357
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    invoke-virtual {v1}, [C->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iput-object v1, v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    .line 358
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->clone()Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    .line 359
    iget-char v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->root:C

    iput-char v1, v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->root:C

    .line 360
    iget-char v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->freenode:C

    iput-char v1, v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->freenode:C

    .line 361
    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->length:I

    iput v1, v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->length:I

    .line 363
    return-object v0
.end method

.method public find(Ljava/lang/String;)I
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 287
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 288
    .local v0, "len":I
    add-int/lit8 v2, v0, 0x1

    new-array v1, v2, [C

    .line 289
    .local v1, "strkey":[C
    invoke-virtual {p1, v3, v0, v1, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 290
    aput-char v3, v1, v0

    .line 292
    invoke-virtual {p0, v1, v3}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->find([CI)I

    move-result v2

    return v2
.end method

.method public find([CI)I
    .locals 7
    .param p1, "key"    # [C
    .param p2, "start"    # I

    .prologue
    const/4 v4, -0x1

    .line 297
    iget-char v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->root:C

    .line 298
    .local v3, "p":C
    move v2, p2

    .line 301
    .local v2, "i":I
    :goto_0
    if-nez v3, :cond_1

    .line 323
    :cond_0
    :goto_1
    return v4

    .line 302
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aget-char v5, v5, v3

    const v6, 0xffff

    if-ne v5, v6, :cond_2

    .line 303
    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-virtual {v5}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->getArray()[C

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v6, v6, v3

    invoke-static {p1, v2, v5, v6}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->strcmp([CI[CI)I

    move-result v5

    if-nez v5, :cond_0

    .line 304
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    aget-char v4, v4, v3

    goto :goto_1

    .line 309
    :cond_2
    aget-char v0, p1, v2

    .line 310
    .local v0, "c":C
    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    aget-char v5, v5, v3

    sub-int v1, v0, v5

    .line 311
    .local v1, "d":I
    if-nez v1, :cond_4

    .line 312
    if-nez v0, :cond_3

    .line 313
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    aget-char v4, v4, v3

    goto :goto_1

    .line 315
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 316
    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    aget-char v3, v5, v3

    .line 317
    goto :goto_0

    :cond_4
    if-gez v1, :cond_5

    .line 318
    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    aget-char v3, v5, v3

    .line 319
    goto :goto_0

    .line 320
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    aget-char v3, v5, v3

    goto :goto_0
.end method

.method protected init()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x800

    .line 126
    iput-char v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->root:C

    .line 127
    const/4 v0, 0x1

    iput-char v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->freenode:C

    .line 128
    iput v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->length:I

    .line 129
    new-array v0, v1, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    .line 130
    new-array v0, v1, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    .line 131
    new-array v0, v1, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    .line 132
    new-array v0, v1, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    .line 133
    new-instance v0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    .line 134
    return-void
.end method

.method public insert(Ljava/lang/String;C)V
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "val"    # C

    .prologue
    const/4 v5, 0x0

    .line 144
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    .line 145
    .local v0, "len":I
    iget-char v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->freenode:C

    add-int/2addr v3, v0

    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    array-length v4, v4

    if-le v3, v4, :cond_0

    .line 146
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    array-length v3, v3

    add-int/lit16 v3, v3, 0x800

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->redimNodeArrays(I)V

    .line 148
    :cond_0
    add-int/lit8 v1, v0, -0x1

    .end local v0    # "len":I
    .local v1, "len":I
    new-array v2, v0, [C

    .line 149
    .local v2, "strkey":[C
    invoke-virtual {p1, v5, v1, v2, v5}, Ljava/lang/String;->getChars(II[CI)V

    .line 150
    aput-char v5, v2, v1

    .line 151
    iget-char v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->root:C

    invoke-direct {p0, v3, v2, v5, p2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insert(C[CIC)C

    move-result v3

    iput-char v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->root:C

    .line 152
    return-void
.end method

.method public insert([CIC)V
    .locals 3
    .param p1, "key"    # [C
    .param p2, "start"    # I
    .param p3, "val"    # C

    .prologue
    .line 155
    invoke-static {p1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->strlen([C)I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 156
    .local v0, "len":I
    iget-char v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->freenode:C

    add-int/2addr v1, v0

    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    array-length v2, v2

    if-le v1, v2, :cond_0

    .line 157
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    array-length v1, v1

    add-int/lit16 v1, v1, 0x800

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->redimNodeArrays(I)V

    .line 159
    :cond_0
    iget-char v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->root:C

    invoke-direct {p0, v1, p1, p2, p3}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insert(C[CIC)C

    move-result v1

    iput-char v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->root:C

    .line 160
    return-void
.end method

.method protected insertBalanced([Ljava/lang/String;[CII)V
    .locals 3
    .param p1, "k"    # [Ljava/lang/String;
    .param p2, "v"    # [C
    .param p3, "offset"    # I
    .param p4, "n"    # I

    .prologue
    .line 373
    const/4 v1, 0x1

    if-ge p4, v1, :cond_0

    .line 382
    :goto_0
    return-void

    .line 376
    :cond_0
    shr-int/lit8 v0, p4, 0x1

    .line 378
    .local v0, "m":I
    add-int v1, v0, p3

    aget-object v1, p1, v1

    add-int v2, v0, p3

    aget-char v2, p2, v2

    invoke-virtual {p0, v1, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insert(Ljava/lang/String;C)V

    .line 379
    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insertBalanced([Ljava/lang/String;[CII)V

    .line 381
    add-int v1, p3, v0

    add-int/lit8 v1, v1, 0x1

    sub-int v2, p4, v0

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, p1, p2, v1, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insertBalanced([Ljava/lang/String;[CII)V

    goto :goto_0
.end method

.method public keys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 457
    new-instance v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;-><init>(Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;)V

    return-object v0
.end method

.method public knows(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 327
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->find(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public printStats(Ljava/io/PrintStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    .line 640
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Number of keys = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->length:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 641
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Node count = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-char v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->freenode:C

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 643
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Key Array length = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 652
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 348
    iget v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->length:I

    return v0
.end method

.method public trimToSize()V
    .locals 3

    .prologue
    .line 420
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->balance()V

    .line 423
    iget-char v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->freenode:C

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->redimNodeArrays(I)V

    .line 426
    new-instance v0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;-><init>()V

    .line 427
    .local v0, "kx":Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->alloc(I)I

    .line 428
    new-instance v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    invoke-direct {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;-><init>()V

    .line 429
    .local v1, "map":Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;
    iget-char v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->root:C

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->compact(Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;C)V

    .line 430
    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    .line 431
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->trimToSize()V

    .line 432
    return-void
.end method
