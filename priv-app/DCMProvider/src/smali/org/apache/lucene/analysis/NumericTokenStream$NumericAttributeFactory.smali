.class final Lorg/apache/lucene/analysis/NumericTokenStream$NumericAttributeFactory;
.super Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
.source "NumericTokenStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/NumericTokenStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NumericAttributeFactory"
.end annotation


# instance fields
.field private final delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V
    .locals 0
    .param p1, "delegate"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .prologue
    .line 129
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;-><init>()V

    .line 130
    iput-object p1, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericAttributeFactory;->delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .line 131
    return-void
.end method


# virtual methods
.method public createAttributeInstance(Ljava/lang/Class;)Lorg/apache/lucene/util/AttributeImpl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;)",
            "Lorg/apache/lucene/util/AttributeImpl;"
        }
    .end annotation

    .prologue
    .line 135
    .local p1, "attClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;"
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "NumericTokenStream does not support CharTermAttribute."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericAttributeFactory;->delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->createAttributeInstance(Ljava/lang/Class;)Lorg/apache/lucene/util/AttributeImpl;

    move-result-object v0

    return-object v0
.end method
