.class public Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;
.super Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;
.source "SolrSynonymParser.java"


# instance fields
.field private final analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private final expand:Z


# direct methods
.method public constructor <init>(ZZLorg/apache/lucene/analysis/Analyzer;)V
    .locals 0
    .param p1, "dedup"    # Z
    .param p2, "expand"    # Z
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;-><init>(Z)V

    .line 63
    iput-boolean p2, p0, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->expand:Z

    .line 64
    iput-object p3, p0, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 65
    return-void
.end method

.method private addInternal(Ljava/io/BufferedReader;)V
    .locals 13
    .param p1, "in"    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 81
    const/4 v4, 0x0

    .line 82
    .local v4, "line":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 131
    return-void

    .line 83
    :cond_1
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v4, v11}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x23

    if-eq v8, v9, :cond_0

    .line 91
    const-string v8, "=>"

    invoke-static {v4, v8}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 92
    .local v7, "sides":[Ljava/lang/String;
    array-length v8, v7

    if-le v8, v12, :cond_5

    .line 93
    array-length v8, v7

    const/4 v9, 0x2

    if-eq v8, v9, :cond_2

    .line 94
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "more than one explicit mapping specified on the same line"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 96
    :cond_2
    aget-object v8, v7, v11

    const-string v9, ","

    invoke-static {v8, v9}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "inputStrings":[Ljava/lang/String;
    array-length v8, v1

    new-array v2, v8, [Lorg/apache/lucene/util/CharsRef;

    .line 98
    .local v2, "inputs":[Lorg/apache/lucene/util/CharsRef;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v8, v2

    if-lt v0, v8, :cond_3

    .line 102
    aget-object v8, v7, v12

    const-string v9, ","

    invoke-static {v8, v9}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 103
    .local v5, "outputStrings":[Ljava/lang/String;
    array-length v8, v5

    new-array v6, v8, [Lorg/apache/lucene/util/CharsRef;

    .line 104
    .local v6, "outputs":[Lorg/apache/lucene/util/CharsRef;
    const/4 v0, 0x0

    :goto_1
    array-length v8, v6

    if-lt v0, v8, :cond_4

    .line 125
    .end local v5    # "outputStrings":[Ljava/lang/String;
    :goto_2
    const/4 v0, 0x0

    :goto_3
    array-length v8, v2

    if-ge v0, v8, :cond_0

    .line 126
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_4
    array-length v8, v6

    if-lt v3, v8, :cond_8

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 99
    .end local v3    # "j":I
    .end local v6    # "outputs":[Lorg/apache/lucene/util/CharsRef;
    :cond_3
    iget-object v8, p0, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    aget-object v9, v1, v0

    invoke-direct {p0, v9}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v10}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    invoke-static {v8, v9, v10}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->analyze(Lorg/apache/lucene/analysis/Analyzer;Ljava/lang/String;Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;

    move-result-object v8

    aput-object v8, v2, v0

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    .restart local v5    # "outputStrings":[Ljava/lang/String;
    .restart local v6    # "outputs":[Lorg/apache/lucene/util/CharsRef;
    :cond_4
    iget-object v8, p0, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    aget-object v9, v5, v0

    invoke-direct {p0, v9}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v10}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    invoke-static {v8, v9, v10}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->analyze(Lorg/apache/lucene/analysis/Analyzer;Ljava/lang/String;Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;

    move-result-object v8

    aput-object v8, v6, v0

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 108
    .end local v0    # "i":I
    .end local v1    # "inputStrings":[Ljava/lang/String;
    .end local v2    # "inputs":[Lorg/apache/lucene/util/CharsRef;
    .end local v5    # "outputStrings":[Ljava/lang/String;
    .end local v6    # "outputs":[Lorg/apache/lucene/util/CharsRef;
    :cond_5
    const-string v8, ","

    invoke-static {v4, v8}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 109
    .restart local v1    # "inputStrings":[Ljava/lang/String;
    array-length v8, v1

    new-array v2, v8, [Lorg/apache/lucene/util/CharsRef;

    .line 110
    .restart local v2    # "inputs":[Lorg/apache/lucene/util/CharsRef;
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_5
    array-length v8, v2

    if-lt v0, v8, :cond_6

    .line 113
    iget-boolean v8, p0, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->expand:Z

    if-eqz v8, :cond_7

    .line 114
    move-object v6, v2

    .line 115
    .restart local v6    # "outputs":[Lorg/apache/lucene/util/CharsRef;
    goto :goto_2

    .line 111
    .end local v6    # "outputs":[Lorg/apache/lucene/util/CharsRef;
    :cond_6
    iget-object v8, p0, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    aget-object v9, v1, v0

    invoke-direct {p0, v9}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v10}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    invoke-static {v8, v9, v10}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->analyze(Lorg/apache/lucene/analysis/Analyzer;Ljava/lang/String;Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;

    move-result-object v8

    aput-object v8, v2, v0

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 116
    :cond_7
    new-array v6, v12, [Lorg/apache/lucene/util/CharsRef;

    .line 117
    .restart local v6    # "outputs":[Lorg/apache/lucene/util/CharsRef;
    aget-object v8, v2, v11

    aput-object v8, v6, v11

    goto :goto_2

    .line 127
    .restart local v3    # "j":I
    :cond_8
    aget-object v8, v2, v0

    aget-object v9, v6, v3

    invoke-virtual {p0, v8, v9, v11}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->add(Lorg/apache/lucene/util/CharsRef;Lorg/apache/lucene/util/CharsRef;Z)V

    .line 126
    add-int/lit8 v3, v3, 0x1

    goto :goto_4
.end method

.method private static split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 7
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "separator"    # Ljava/lang/String;

    .prologue
    .line 134
    new-instance v2, Ljava/util/ArrayList;

    const/4 v6, 0x2

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 135
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "pos":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .local v1, "end":I
    move v4, v3

    .line 137
    .end local v3    # "pos":I
    .local v4, "pos":I
    :goto_0
    if-lt v4, v1, :cond_2

    move v3, v4

    .line 157
    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    :cond_0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 158
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    return-object v6

    .line 138
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    :cond_2
    invoke-virtual {p0, p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 139
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_3

    .line 140
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    new-instance v5, Ljava/lang/StringBuilder;

    .end local v5    # "sb":Ljava/lang/StringBuilder;
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int v3, v4, v6

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    move v4, v3

    .line 144
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    goto :goto_0

    .line 147
    :cond_4
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 148
    .local v0, "ch":C
    const/16 v6, 0x5c

    if-ne v0, v6, :cond_5

    .line 149
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 150
    if-ge v3, v1, :cond_0

    .line 151
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v3, v4

    .line 154
    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    :cond_5
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v4, v3

    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    goto :goto_0
.end method

.method private unescape(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 165
    const-string v3, "\\"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_0

    .line 166
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 175
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 177
    .end local v1    # "i":I
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    .end local p1    # "s":Ljava/lang/String;
    :cond_0
    return-object p1

    .line 168
    .restart local v1    # "i":I
    .restart local v2    # "sb":Ljava/lang/StringBuilder;
    .restart local p1    # "s":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 169
    .local v0, "ch":C
    const/16 v3, 0x5c

    if-ne v0, v3, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_2

    .line 170
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 167
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 172
    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method


# virtual methods
.method public add(Ljava/io/Reader;)V
    .locals 5
    .param p1, "in"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Ljava/io/LineNumberReader;

    invoke-direct {v0, p1}, Ljava/io/LineNumberReader;-><init>(Ljava/io/Reader;)V

    .line 70
    .local v0, "br":Ljava/io/LineNumberReader;
    :try_start_0
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->addInternal(Ljava/io/BufferedReader;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    invoke-virtual {v0}, Ljava/io/LineNumberReader;->close()V

    .line 78
    return-void

    .line 71
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    new-instance v2, Ljava/text/ParseException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid synonym rule at line "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/LineNumberReader;->getLineNumber()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    .line 73
    .local v2, "ex":Ljava/text/ParseException;
    invoke-virtual {v2, v1}, Ljava/text/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 74
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .end local v2    # "ex":Ljava/text/ParseException;
    :catchall_0
    move-exception v3

    .line 76
    invoke-virtual {v0}, Ljava/io/LineNumberReader;->close()V

    .line 77
    throw v3
.end method
