.class public Lorg/apache/lucene/analysis/Token;
.super Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
.source "Token.java"

# interfaces
.implements Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;
    }
.end annotation


# static fields
.field public static final TOKEN_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;


# instance fields
.field private endOffset:I

.field private flags:I

.field private payload:Lorg/apache/lucene/util/BytesRef;

.field private positionIncrement:I

.field private positionLength:I

.field private startOffset:I

.field private type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 624
    new-instance v0, Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;

    sget-object v1, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    .line 623
    sput-object v0, Lorg/apache/lucene/analysis/Token;->TOKEN_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .line 624
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 134
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;-><init>()V

    .line 127
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 130
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 131
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionLength:I

    .line 135
    return-void
.end method

.method public constructor <init>(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v1, 0x1

    .line 141
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;-><init>()V

    .line 127
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 130
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 131
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionLength:I

    .line 142
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 143
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 144
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 145
    return-void
.end method

.method public constructor <init>(III)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "flags"    # I

    .prologue
    const/4 v1, 0x1

    .line 166
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;-><init>()V

    .line 127
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 130
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 131
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionLength:I

    .line 167
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 168
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 169
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 170
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 171
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "typ"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 152
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;-><init>()V

    .line 127
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 130
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 131
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionLength:I

    .line 153
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 154
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 155
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 156
    iput-object p3, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v1, 0x1

    .line 182
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;-><init>()V

    .line 127
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 130
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 131
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionLength:I

    .line 183
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 184
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 185
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 186
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 187
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;III)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "flags"    # I

    .prologue
    const/4 v1, 0x1

    .line 216
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;-><init>()V

    .line 127
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 130
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 131
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionLength:I

    .line 217
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 218
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 219
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 220
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 221
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 222
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "typ"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 198
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;-><init>()V

    .line 127
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 130
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 131
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionLength:I

    .line 199
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 200
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 201
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 202
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 203
    iput-object p4, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 204
    return-void
.end method

.method public constructor <init>([CIIII)V
    .locals 2
    .param p1, "startTermBuffer"    # [C
    .param p2, "termBufferOffset"    # I
    .param p3, "termBufferLength"    # I
    .param p4, "start"    # I
    .param p5, "end"    # I

    .prologue
    const/4 v1, 0x1

    .line 234
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;-><init>()V

    .line 127
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 130
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 131
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->positionLength:I

    .line 235
    invoke-direct {p0, p4, p5}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 236
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 237
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 238
    iput p5, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 239
    return-void
.end method

.method private checkOffsets(II)V
    .locals 3
    .param p1, "startOffset"    # I
    .param p2, "endOffset"    # I

    .prologue
    .line 612
    if-ltz p1, :cond_0

    if-ge p2, p1, :cond_1

    .line 613
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startOffset must be non-negative, and endOffset must be >= startOffset, startOffset="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 614
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",endOffset="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 613
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 616
    :cond_1
    return-void
.end method

.method private clearNoTermBuffer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 436
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 437
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 438
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 439
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iput v1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 440
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 441
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 368
    invoke-super {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->clear()V

    .line 369
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 370
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 371
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 372
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iput v1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 373
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 374
    return-void
.end method

.method public clone()Lorg/apache/lucene/analysis/Token;
    .locals 2

    .prologue
    .line 378
    invoke-super {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->clone()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/Token;

    .line 380
    .local v0, "t":Lorg/apache/lucene/analysis/Token;
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_0

    .line 381
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->clone()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 383
    :cond_0
    return-object v0
.end method

.method public clone([CIIII)Lorg/apache/lucene/analysis/Token;
    .locals 6
    .param p1, "newTermBuffer"    # [C
    .param p2, "newTermOffset"    # I
    .param p3, "newTermLength"    # I
    .param p4, "newStartOffset"    # I
    .param p5, "newEndOffset"    # I

    .prologue
    .line 392
    new-instance v0, Lorg/apache/lucene/analysis/Token;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/Token;-><init>([CIIII)V

    .line 393
    .local v0, "t":Lorg/apache/lucene/analysis/Token;
    iget v1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    iput v1, v0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 394
    iget v1, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    iput v1, v0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 395
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    iput-object v1, v0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 396
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_0

    .line 397
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->clone()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 398
    :cond_0
    return-object v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Token;->clone()Lorg/apache/lucene/analysis/Token;

    move-result-object v0

    return-object v0
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 4
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 583
    instance-of v1, p1, Lorg/apache/lucene/analysis/Token;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 584
    check-cast v0, Lorg/apache/lucene/analysis/Token;

    .line 585
    .local v0, "to":Lorg/apache/lucene/analysis/Token;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/analysis/Token;->reinit(Lorg/apache/lucene/analysis/Token;)V

    .line 587
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_0

    .line 588
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->clone()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 598
    .end local v0    # "to":Lorg/apache/lucene/analysis/Token;
    .end local p1    # "target":Lorg/apache/lucene/util/AttributeImpl;
    :cond_0
    :goto_0
    return-void

    .line 591
    .restart local p1    # "target":Lorg/apache/lucene/util/AttributeImpl;
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->copyTo(Lorg/apache/lucene/util/AttributeImpl;)V

    move-object v1, p1

    .line 592
    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    iget v3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    invoke-interface {v1, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    move-object v1, p1

    .line 593
    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    invoke-interface {v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    move-object v1, p1

    .line 594
    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    invoke-interface {v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->setPayload(Lorg/apache/lucene/util/BytesRef;)V

    move-object v1, p1

    .line 595
    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    invoke-interface {v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;->setFlags(I)V

    .line 596
    check-cast p1, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .end local p1    # "target":Lorg/apache/lucene/util/AttributeImpl;
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    invoke-interface {p1, v1}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    goto :goto_0

    .line 594
    .restart local p1    # "target":Lorg/apache/lucene/util/AttributeImpl;
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2}, Lorg/apache/lucene/util/BytesRef;->clone()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    goto :goto_1
.end method

.method public final endOffset()I
    .locals 1

    .prologue
    .line 295
    iget v0, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 403
    if-ne p1, p0, :cond_1

    .line 417
    :cond_0
    :goto_0
    return v1

    .line 406
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/analysis/Token;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 407
    check-cast v0, Lorg/apache/lucene/analysis/Token;

    .line 408
    .local v0, "other":Lorg/apache/lucene/analysis/Token;
    iget v3, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    iget v4, v0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    if-ne v3, v4, :cond_2

    .line 409
    iget v3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iget v4, v0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    if-ne v3, v4, :cond_2

    .line 410
    iget v3, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    iget v4, v0, Lorg/apache/lucene/analysis/Token;->flags:I

    if-ne v3, v4, :cond_2

    .line 411
    iget v3, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    iget v4, v0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    if-ne v3, v4, :cond_2

    .line 412
    iget-object v3, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    if-nez v3, :cond_3

    iget-object v3, v0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 413
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_4

    iget-object v3, v0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_2

    .line 414
    :goto_2
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    .line 408
    goto :goto_0

    .line 412
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 413
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .end local v0    # "other":Lorg/apache/lucene/analysis/Token;
    :cond_5
    move v1, v2

    .line 417
    goto :goto_0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 333
    iget v0, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public getPositionIncrement()I
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    return v0
.end method

.method public getPositionLength()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lorg/apache/lucene/analysis/Token;->positionLength:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 422
    invoke-super {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->hashCode()I

    move-result v0

    .line 423
    .local v0, "code":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    add-int v0, v1, v2

    .line 424
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    add-int v0, v1, v2

    .line 425
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    add-int v0, v1, v2

    .line 426
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    add-int v0, v1, v2

    .line 427
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 428
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 429
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_1

    .line 430
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 431
    :cond_1
    return v0
.end method

.method public reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V
    .locals 3
    .param p1, "reflector"    # Lorg/apache/lucene/util/AttributeReflector;

    .prologue
    .line 602
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V

    .line 603
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    const-string v1, "startOffset"

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 604
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    const-string v1, "endOffset"

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 605
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    const-string v1, "positionIncrement"

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 606
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    const-string v1, "payload"

    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 607
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    const-string v1, "flags"

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 608
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    const-string v1, "type"

    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 609
    return-void
.end method

.method public reinit(Ljava/lang/String;II)Lorg/apache/lucene/analysis/Token;
    .locals 1
    .param p1, "newTerm"    # Ljava/lang/String;
    .param p2, "newStartOffset"    # I
    .param p3, "newEndOffset"    # I

    .prologue
    .line 511
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 512
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Token;->clear()V

    .line 513
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 514
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 515
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 516
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 517
    return-object p0
.end method

.method public reinit(Ljava/lang/String;IIII)Lorg/apache/lucene/analysis/Token;
    .locals 1
    .param p1, "newTerm"    # Ljava/lang/String;
    .param p2, "newTermOffset"    # I
    .param p3, "newTermLength"    # I
    .param p4, "newStartOffset"    # I
    .param p5, "newEndOffset"    # I

    .prologue
    .line 526
    invoke-direct {p0, p4, p5}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 527
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Token;->clear()V

    .line 528
    add-int v0, p2, p3

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 529
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 530
    iput p5, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 531
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 532
    return-object p0
.end method

.method public reinit(Ljava/lang/String;IIIILjava/lang/String;)Lorg/apache/lucene/analysis/Token;
    .locals 1
    .param p1, "newTerm"    # Ljava/lang/String;
    .param p2, "newTermOffset"    # I
    .param p3, "newTermLength"    # I
    .param p4, "newStartOffset"    # I
    .param p5, "newEndOffset"    # I
    .param p6, "newType"    # Ljava/lang/String;

    .prologue
    .line 496
    invoke-direct {p0, p4, p5}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 497
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Token;->clear()V

    .line 498
    add-int v0, p2, p3

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 499
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 500
    iput p5, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 501
    iput-object p6, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 502
    return-object p0
.end method

.method public reinit(Ljava/lang/String;IILjava/lang/String;)Lorg/apache/lucene/analysis/Token;
    .locals 0
    .param p1, "newTerm"    # Ljava/lang/String;
    .param p2, "newStartOffset"    # I
    .param p3, "newEndOffset"    # I
    .param p4, "newType"    # Ljava/lang/String;

    .prologue
    .line 481
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 482
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Token;->clear()V

    .line 483
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 484
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 485
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 486
    iput-object p4, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 487
    return-object p0
.end method

.method public reinit([CIIII)Lorg/apache/lucene/analysis/Token;
    .locals 1
    .param p1, "newTermBuffer"    # [C
    .param p2, "newTermOffset"    # I
    .param p3, "newTermLength"    # I
    .param p4, "newStartOffset"    # I
    .param p5, "newEndOffset"    # I

    .prologue
    .line 466
    invoke-direct {p0, p4, p5}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 467
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Token;->clearNoTermBuffer()V

    .line 468
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 469
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 470
    iput p5, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 471
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 472
    return-object p0
.end method

.method public reinit([CIIIILjava/lang/String;)Lorg/apache/lucene/analysis/Token;
    .locals 1
    .param p1, "newTermBuffer"    # [C
    .param p2, "newTermOffset"    # I
    .param p3, "newTermLength"    # I
    .param p4, "newStartOffset"    # I
    .param p5, "newEndOffset"    # I
    .param p6, "newType"    # Ljava/lang/String;

    .prologue
    .line 449
    invoke-direct {p0, p4, p5}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 450
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Token;->clearNoTermBuffer()V

    .line 451
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 452
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 453
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 454
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 455
    iput p5, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 456
    iput-object p6, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 457
    return-object p0
.end method

.method public reinit(Lorg/apache/lucene/analysis/Token;)V
    .locals 3
    .param p1, "prototype"    # Lorg/apache/lucene/analysis/Token;

    .prologue
    .line 540
    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->buffer()[C

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->length()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 541
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 542
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->flags:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 543
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->startOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 544
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 545
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 546
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 547
    return-void
.end method

.method public reinit(Lorg/apache/lucene/analysis/Token;Ljava/lang/String;)V
    .locals 1
    .param p1, "prototype"    # Lorg/apache/lucene/analysis/Token;
    .param p2, "newTerm"    # Ljava/lang/String;

    .prologue
    .line 555
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Token;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    invoke-interface {v0, p2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 556
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 557
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->flags:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 558
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->startOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 559
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 560
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 561
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 562
    return-void
.end method

.method public reinit(Lorg/apache/lucene/analysis/Token;[CII)V
    .locals 1
    .param p1, "prototype"    # Lorg/apache/lucene/analysis/Token;
    .param p2, "newTermBuffer"    # [C
    .param p3, "offset"    # I
    .param p4, "length"    # I

    .prologue
    .line 572
    invoke-virtual {p0, p2, p3, p4}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 573
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 574
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->flags:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 575
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->startOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 576
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 577
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 578
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 579
    return-void
.end method

.method public setFlags(I)V
    .locals 0
    .param p1, "flags"    # I

    .prologue
    .line 342
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 343
    return-void
.end method

.method public setOffset(II)V
    .locals 0
    .param p1, "startOffset"    # I
    .param p2, "endOffset"    # I

    .prologue
    .line 304
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Token;->checkOffsets(II)V

    .line 305
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 306
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 307
    return-void
.end method

.method public setPayload(Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "payload"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 360
    iput-object p1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 361
    return-void
.end method

.method public setPositionIncrement(I)V
    .locals 3
    .param p1, "positionIncrement"    # I

    .prologue
    .line 247
    if-gez p1, :cond_0

    .line 248
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 249
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Increment must be zero or greater: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 248
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_0
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 251
    return-void
.end method

.method public setPositionLength(I)V
    .locals 0
    .param p1, "positionLength"    # I

    .prologue
    .line 268
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->positionLength:I

    .line 269
    return-void
.end method

.method public final setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 324
    iput-object p1, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 325
    return-void
.end method

.method public final startOffset()I
    .locals 1

    .prologue
    .line 286
    iget v0, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    return v0
.end method

.method public final type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    return-object v0
.end method
