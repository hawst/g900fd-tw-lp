.class abstract Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;
.super Ljava/lang/Object;
.source "HunspellDictionary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "FlagParsingStrategy"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;)V
    .locals 0

    .prologue
    .line 421
    invoke-direct {p0}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method parseFlag(Ljava/lang/String;)C
    .locals 2
    .param p1, "rawFlag"    # Ljava/lang/String;

    .prologue
    .line 430
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;->parseFlags(Ljava/lang/String;)[C

    move-result-object v0

    const/4 v1, 0x0

    aget-char v0, v0, v1

    return v0
.end method

.method abstract parseFlags(Ljava/lang/String;)[C
.end method
