.class public Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "WordDelimiterFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# static fields
.field public static final PROTECTED_TOKENS:Ljava/lang/String; = "protected"

.field public static final TYPES:Ljava/lang/String; = "types"

.field private static typePattern:Ljava/util/regex/Pattern;


# instance fields
.field private final flags:I

.field out:[C

.field private protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

.field typeTable:[B

.field private final types:Ljava/lang/String;

.field private final wordFiles:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    const-string v0, "(.*)\\s*=>\\s*(.*)\\s*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->typePattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 63
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 58
    iput-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->typeTable:[B

    .line 59
    iput-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 167
    const/16 v1, 0x100

    new-array v1, v1, [C

    iput-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->out:[C

    .line 64
    const/4 v0, 0x0

    .line 65
    .local v0, "flags":I
    const-string v1, "generateWordParts"

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    or-int/lit8 v0, v0, 0x1

    .line 68
    :cond_0
    const-string v1, "generateNumberParts"

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    or-int/lit8 v0, v0, 0x2

    .line 71
    :cond_1
    const-string v1, "catenateWords"

    invoke-virtual {p0, p1, v1, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_2

    .line 72
    or-int/lit8 v0, v0, 0x4

    .line 74
    :cond_2
    const-string v1, "catenateNumbers"

    invoke-virtual {p0, p1, v1, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_3

    .line 75
    or-int/lit8 v0, v0, 0x8

    .line 77
    :cond_3
    const-string v1, "catenateAll"

    invoke-virtual {p0, p1, v1, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_4

    .line 78
    or-int/lit8 v0, v0, 0x10

    .line 80
    :cond_4
    const-string v1, "splitOnCaseChange"

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_5

    .line 81
    or-int/lit8 v0, v0, 0x40

    .line 83
    :cond_5
    const-string v1, "splitOnNumerics"

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_6

    .line 84
    or-int/lit16 v0, v0, 0x80

    .line 86
    :cond_6
    const-string v1, "preserveOriginal"

    invoke-virtual {p0, p1, v1, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_7

    .line 87
    or-int/lit8 v0, v0, 0x20

    .line 89
    :cond_7
    const-string v1, "stemEnglishPossessive"

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_8

    .line 90
    or-int/lit16 v0, v0, 0x100

    .line 92
    :cond_8
    const-string v1, "protected"

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->wordFiles:Ljava/lang/String;

    .line 93
    const-string v1, "types"

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->types:Ljava/lang/String;

    .line 94
    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->flags:I

    .line 95
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    .line 96
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown parameters: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 98
    :cond_9
    return-void
.end method

.method private parseString(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 170
    const/4 v2, 0x0

    .line 171
    .local v2, "readPos":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 172
    .local v1, "len":I
    const/4 v4, 0x0

    .local v4, "writePos":I
    move v5, v4

    .end local v4    # "writePos":I
    .local v5, "writePos":I
    move v3, v2

    .line 173
    .end local v2    # "readPos":I
    .local v3, "readPos":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 196
    new-instance v6, Ljava/lang/String;

    iget-object v7, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->out:[C

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v5}, Ljava/lang/String;-><init>([CII)V

    return-object v6

    .line 174
    :cond_0
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 175
    .local v0, "c":C
    const/16 v6, 0x5c

    if-ne v0, v6, :cond_2

    .line 176
    if-lt v2, v1, :cond_1

    .line 177
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid escaped char in ["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 178
    :cond_1
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 179
    sparse-switch v0, :sswitch_data_0

    move v2, v3

    .line 194
    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    :cond_2
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->out:[C

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "writePos":I
    .restart local v4    # "writePos":I
    aput-char v0, v6, v5

    move v5, v4

    .end local v4    # "writePos":I
    .restart local v5    # "writePos":I
    move v3, v2

    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    goto :goto_0

    .line 180
    :sswitch_0
    const/16 v0, 0x5c

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 181
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_1
    const/16 v0, 0xa

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 182
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_2
    const/16 v0, 0x9

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 183
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_3
    const/16 v0, 0xd

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 184
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_4
    const/16 v0, 0x8

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 185
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_5
    const/16 v0, 0xc

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 187
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_6
    add-int/lit8 v6, v3, 0x3

    if-lt v6, v1, :cond_3

    .line 188
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid escaped char in ["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 189
    :cond_3
    add-int/lit8 v6, v3, 0x4

    invoke-virtual {p1, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v6

    int-to-char v0, v6

    .line 190
    add-int/lit8 v2, v3, 0x4

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 179
    :sswitch_data_0
    .sparse-switch
        0x5c -> :sswitch_0
        0x62 -> :sswitch_4
        0x66 -> :sswitch_5
        0x6e -> :sswitch_1
        0x72 -> :sswitch_3
        0x74 -> :sswitch_2
        0x75 -> :sswitch_6
    .end sparse-switch
.end method

.method private parseType(Ljava/lang/String;)Ljava/lang/Byte;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 151
    const-string v0, "LOWER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    .line 164
    :goto_0
    return-object v0

    .line 153
    :cond_0
    const-string v0, "UPPER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0

    .line 155
    :cond_1
    const-string v0, "ALPHA"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 156
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0

    .line 157
    :cond_2
    const-string v0, "DIGIT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 158
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0

    .line 159
    :cond_3
    const-string v0, "ALPHANUM"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 160
    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0

    .line 161
    :cond_4
    const-string v0, "SUBWORD_DELIM"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 162
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0

    .line 164
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private parseTypes(Ljava/util/List;)[B
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .local p1, "rules":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v10, 0x1

    .line 127
    new-instance v6, Ljava/util/TreeMap;

    invoke-direct {v6}, Ljava/util/TreeMap;-><init>()V

    .line 128
    .local v6, "typeMap":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Ljava/lang/Character;Ljava/lang/Byte;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_0

    .line 142
    invoke-interface {v6}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Character;

    invoke-virtual {v8}, Ljava/lang/Character;->charValue()C

    move-result v8

    add-int/lit8 v8, v8, 0x1

    sget-object v9, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->DEFAULT_WORD_DELIM_TABLE:[B

    array-length v9, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    new-array v7, v8, [B

    .line 143
    .local v7, "types":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v8, v7

    if-lt v0, v8, :cond_4

    .line 145
    invoke-interface {v6}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_5

    .line 147
    return-object v7

    .line 128
    .end local v0    # "i":I
    .end local v7    # "types":[B
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 129
    .local v5, "rule":Ljava/lang/String;
    sget-object v9, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->typePattern:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 130
    .local v2, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-nez v9, :cond_1

    .line 131
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Invalid Mapping Rule : ["

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 132
    :cond_1
    invoke-virtual {v2, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->parseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 133
    .local v1, "lhs":Ljava/lang/String;
    const/4 v9, 0x2

    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->parseType(Ljava/lang/String;)Ljava/lang/Byte;

    move-result-object v4

    .line 134
    .local v4, "rhs":Ljava/lang/Byte;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    if-eq v9, v10, :cond_2

    .line 135
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Invalid Mapping Rule : ["

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]. Only a single character is allowed."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 136
    :cond_2
    if-nez v4, :cond_3

    .line 137
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Invalid Mapping Rule : ["

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]. Illegal type."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 138
    :cond_3
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v9

    invoke-interface {v6, v9, v4}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 144
    .end local v1    # "lhs":Ljava/lang/String;
    .end local v2    # "m":Ljava/util/regex/Matcher;
    .end local v4    # "rhs":Ljava/lang/Byte;
    .end local v5    # "rule":Ljava/lang/String;
    .restart local v0    # "i":I
    .restart local v7    # "types":[B
    :cond_4
    invoke-static {v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->getType(I)B

    move-result v8

    aput-byte v8, v7, v0

    .line 143
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 145
    :cond_5
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 146
    .local v3, "mapping":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Character;Ljava/lang/Byte;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Character;

    invoke-virtual {v8}, Ljava/lang/Character;->charValue()C

    move-result v10

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Byte;

    invoke-virtual {v8}, Ljava/lang/Byte;->byteValue()B

    move-result v8

    aput-byte v8, v7, v10

    goto/16 :goto_2
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 118
    new-instance v1, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->typeTable:[B

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->DEFAULT_WORD_DELIM_TABLE:[B

    .line 119
    :goto_0
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->flags:I

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 118
    invoke-direct {v1, p1, v0, v2, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;[BILorg/apache/lucene/analysis/util/CharArraySet;)V

    return-object v1

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->typeTable:[B

    goto :goto_0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 6
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->wordFiles:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 103
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->wordFiles:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p0, p1, v4, v5}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->getWordSet(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 105
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->types:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 106
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->types:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->splitFileNames(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 107
    .local v1, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v3, "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 112
    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->parseTypes(Ljava/util/List;)[B

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->typeTable:[B

    .line 114
    .end local v1    # "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    return-void

    .line 108
    .restart local v1    # "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 109
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, p1, v5}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilterFactory;->getLines(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 110
    .local v2, "lines":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
