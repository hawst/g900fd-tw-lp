.class public Lorg/apache/lucene/analysis/pattern/PatternTokenizerFactory;
.super Lorg/apache/lucene/analysis/util/TokenizerFactory;
.source "PatternTokenizerFactory.java"


# static fields
.field public static final GROUP:Ljava/lang/String; = "group"

.field public static final PATTERN:Ljava/lang/String; = "pattern"


# instance fields
.field protected final group:I

.field protected final pattern:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenizerFactory;-><init>(Ljava/util/Map;)V

    .line 73
    const-string v0, "pattern"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/pattern/PatternTokenizerFactory;->getPattern(Ljava/util/Map;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizerFactory;->pattern:Ljava/util/regex/Pattern;

    .line 74
    const-string v0, "group"

    const/4 v1, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/pattern/PatternTokenizerFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizerFactory;->group:I

    .line 75
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Tokenizer;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/analysis/pattern/PatternTokenizerFactory;->create(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/pattern/PatternTokenizer;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/pattern/PatternTokenizer;
    .locals 3
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    .line 85
    new-instance v0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;

    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizerFactory;->pattern:Ljava/util/regex/Pattern;

    iget v2, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizerFactory;->group:I

    invoke-direct {v0, p1, p2, v1, v2}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;Ljava/util/regex/Pattern;I)V

    return-object v0
.end method
