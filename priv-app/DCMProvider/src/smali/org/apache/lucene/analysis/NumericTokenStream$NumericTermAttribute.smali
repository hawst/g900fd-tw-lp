.class public interface abstract Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;
.super Ljava/lang/Object;
.source "NumericTokenStream.java"

# interfaces
.implements Lorg/apache/lucene/util/Attribute;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/NumericTokenStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NumericTermAttribute"
.end annotation


# virtual methods
.method public abstract getRawValue()J
.end method

.method public abstract getShift()I
.end method

.method public abstract getValueSize()I
.end method

.method public abstract incShift()I
.end method

.method public abstract init(JIII)V
.end method

.method public abstract setShift(I)V
.end method
