.class public Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;
.super Ljava/lang/Object;
.source "TernaryTree.java"

# interfaces
.implements Ljava/util/Enumeration;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Iterator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Enumeration",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field cur:I

.field curkey:Ljava/lang/String;

.field ks:Ljava/lang/StringBuilder;

.field ns:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;)V
    .locals 1

    .prologue
    .line 504
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 505
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    .line 506
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ns:Ljava/util/Stack;

    .line 507
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ks:Ljava/lang/StringBuilder;

    .line 508
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->rewind()V

    .line 509
    return-void
.end method

.method private run()I
    .locals 10

    .prologue
    const v9, 0xffff

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 596
    iget v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    if-ne v6, v4, :cond_0

    .line 634
    :goto_0
    return v4

    .line 600
    :cond_0
    const/4 v1, 0x0

    .line 603
    .local v1, "leaf":Z
    :cond_1
    :goto_1
    iget v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    if-nez v6, :cond_3

    .line 615
    :goto_2
    if-eqz v1, :cond_6

    .line 626
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ks:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 627
    .local v0, "buf":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v4, v4, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    iget v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    aget-char v4, v4, v6

    if-ne v4, v9, :cond_2

    .line 628
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v4, v4, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    iget v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    aget-char v2, v4, v6

    .line 629
    .local v2, "p":I
    :goto_3
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v4, v4, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->get(I)C

    move-result v4

    if-nez v4, :cond_7

    .line 633
    .end local v2    # "p":I
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->curkey:Ljava/lang/String;

    move v4, v5

    .line 634
    goto :goto_0

    .line 604
    .end local v0    # "buf":Ljava/lang/StringBuilder;
    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v6, v6, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    iget v7, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    aget-char v6, v6, v7

    if-ne v6, v9, :cond_4

    .line 605
    const/4 v1, 0x1

    .line 606
    goto :goto_2

    .line 608
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ns:Ljava/util/Stack;

    new-instance v7, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;

    iget v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    int-to-char v8, v8

    invoke-direct {v7, p0, v8, v5}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;-><init>(Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;CC)V

    invoke-virtual {v6, v7}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 609
    iget-object v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v6, v6, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    iget v7, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    aget-char v6, v6, v7

    if-nez v6, :cond_5

    .line 610
    const/4 v1, 0x1

    .line 611
    goto :goto_2

    .line 613
    :cond_5
    iget-object v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v6, v6, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    iget v7, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    aget-char v6, v6, v7

    iput v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    goto :goto_1

    .line 619
    :cond_6
    invoke-direct {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->up()I

    move-result v6

    iput v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    .line 620
    iget v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    if-ne v6, v4, :cond_1

    goto :goto_0

    .line 630
    .restart local v0    # "buf":Ljava/lang/StringBuilder;
    .restart local v2    # "p":I
    :cond_7
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v4, v4, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "p":I
    .local v3, "p":I
    invoke-virtual {v4, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->get(I)C

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v2, v3

    .end local v3    # "p":I
    .restart local v2    # "p":I
    goto :goto_3
.end method

.method private up()I
    .locals 7

    .prologue
    const/4 v3, -0x1

    .line 542
    new-instance v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;

    invoke-direct {v1, p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;-><init>(Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;)V

    .line 543
    .local v1, "i":Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;
    const/4 v2, 0x0

    .line 545
    .local v2, "res":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ns:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->empty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 589
    :cond_0
    :goto_0
    return v3

    .line 549
    :cond_1
    iget v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    if-eqz v4, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v4, v4, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    iget v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    aget-char v4, v4, v5

    if-nez v4, :cond_2

    .line 550
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v3, v3, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->lo:[C

    iget v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    aget-char v3, v3, v4

    goto :goto_0

    .line 553
    :cond_2
    const/4 v0, 0x1

    .line 555
    .local v0, "climb":Z
    :goto_1
    if-nez v0, :cond_3

    move v3, v2

    .line 589
    goto :goto_0

    .line 556
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ns:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "i":Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;
    check-cast v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;

    .line 557
    .restart local v1    # "i":Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;
    iget-char v4, v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->child:C

    add-int/lit8 v4, v4, 0x1

    int-to-char v4, v4

    iput-char v4, v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->child:C

    .line 558
    iget-char v4, v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->child:C

    packed-switch v4, :pswitch_data_0

    .line 582
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ns:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->empty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 585
    const/4 v0, 0x1

    goto :goto_1

    .line 560
    :pswitch_0
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v4, v4, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    iget-char v5, v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->parent:C

    aget-char v4, v4, v5

    if-eqz v4, :cond_4

    .line 561
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v4, v4, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    iget-char v5, v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->parent:C

    aget-char v2, v4, v5

    .line 562
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ns:Ljava/util/Stack;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->clone()Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ks:Ljava/lang/StringBuilder;

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v5, v5, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->sc:[C

    iget-char v6, v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->parent:C

    aget-char v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 569
    :goto_2
    const/4 v0, 0x0

    .line 570
    goto :goto_1

    .line 565
    :cond_4
    iget-char v4, v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->child:C

    add-int/lit8 v4, v4, 0x1

    int-to-char v4, v4

    iput-char v4, v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->child:C

    .line 566
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ns:Ljava/util/Stack;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->clone()Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v4, v4, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    iget-char v5, v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->parent:C

    aget-char v2, v4, v5

    goto :goto_2

    .line 573
    :pswitch_1
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v4, v4, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->hi:[C

    iget-char v5, v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->parent:C

    aget-char v2, v4, v5

    .line 574
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ns:Ljava/util/Stack;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->clone()Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 575
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ks:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_5

    .line 576
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ks:Ljava/lang/StringBuilder;

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ks:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 578
    :cond_5
    const/4 v0, 0x0

    .line 579
    goto/16 :goto_1

    .line 558
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getValue()C
    .locals 2

    .prologue
    .line 527
    iget v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    if-ltz v0, :cond_0

    .line 528
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-object v0, v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->eq:[C

    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    aget-char v0, v0, v1

    .line 530
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMoreElements()Z
    .locals 2

    .prologue
    .line 535
    iget v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic nextElement()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->nextElement()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public nextElement()Ljava/lang/String;
    .locals 2

    .prologue
    .line 520
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->curkey:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 521
    .local v0, "res":Ljava/lang/String;
    invoke-direct {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->up()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    .line 522
    invoke-direct {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->run()I

    .line 523
    return-object v0
.end method

.method public rewind()V
    .locals 2

    .prologue
    .line 512
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ns:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->removeAllElements()V

    .line 513
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->ks:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 514
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->this$0:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    iget-char v0, v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->root:C

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->cur:I

    .line 515
    invoke-direct {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;->run()I

    .line 516
    return-void
.end method
