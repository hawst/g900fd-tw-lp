.class public Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;
.super Lorg/apache/lucene/analysis/util/CharFilterFactory;
.source "PatternReplaceCharFilterFactory.java"


# instance fields
.field private final blockDelimiters:Ljava/lang/String;

.field private final maxBlockChars:I

.field private final pattern:Ljava/util/regex/Pattern;

.field private final replacement:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/CharFilterFactory;-><init>(Ljava/util/Map;)V

    .line 49
    const-string v0, "pattern"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->getPattern(Ljava/util/Map;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->pattern:Ljava/util/regex/Pattern;

    .line 50
    const-string v0, "replacement"

    const-string v1, ""

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->replacement:Ljava/lang/String;

    .line 52
    const-string v0, "maxBlockChars"

    const/16 v1, 0x2710

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->maxBlockChars:I

    .line 53
    const-string v0, "blockDelimiters"

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->blockDelimiters:Ljava/lang/String;

    .line 54
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->create(Ljava/io/Reader;)Lorg/apache/lucene/analysis/CharFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Ljava/io/Reader;)Lorg/apache/lucene/analysis/CharFilter;
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 61
    new-instance v0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->pattern:Ljava/util/regex/Pattern;

    iget-object v2, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->replacement:Ljava/lang/String;

    iget v3, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->maxBlockChars:I

    iget-object v4, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilterFactory;->blockDelimiters:Ljava/lang/String;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;-><init>(Ljava/util/regex/Pattern;Ljava/lang/String;ILjava/lang/String;Ljava/io/Reader;)V

    return-object v0
.end method
