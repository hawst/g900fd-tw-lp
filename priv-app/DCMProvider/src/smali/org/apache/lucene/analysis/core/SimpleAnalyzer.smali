.class public final Lorg/apache/lucene/analysis/core/SimpleAnalyzer;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "SimpleAnalyzer.java"


# instance fields
.field private final matchVersion:Lorg/apache/lucene/util/Version;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/analysis/core/SimpleAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 48
    return-void
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 53
    new-instance v0, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    new-instance v1, Lorg/apache/lucene/analysis/core/LowerCaseTokenizer;

    iget-object v2, p0, Lorg/apache/lucene/analysis/core/SimpleAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v2, p2}, Lorg/apache/lucene/analysis/core/LowerCaseTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;)V

    return-object v0
.end method
