.class public final Lorg/apache/lucene/analysis/standard/StandardTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "StandardTokenizer.java"


# static fields
.field public static final ACRONYM:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACRONYM_DEP:I = 0x8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ALPHANUM:I = 0x0

.field public static final APOSTROPHE:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CJ:I = 0x7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final COMPANY:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final EMAIL:I = 0x4

.field public static final HANGUL:I = 0xd

.field public static final HIRAGANA:I = 0xb

.field public static final HOST:I = 0x5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final IDEOGRAPHIC:I = 0xa

.field public static final KATAKANA:I = 0xc

.field public static final NUM:I = 0x6

.field public static final SOUTHEAST_ASIAN:I = 0x9

.field public static final TOKEN_TYPES:[Ljava/lang/String;


# instance fields
.field private maxTokenLength:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 89
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 90
    const-string v2, "<ALPHANUM>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 91
    const-string v2, "<APOSTROPHE>"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 92
    const-string v2, "<ACRONYM>"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 93
    const-string v2, "<COMPANY>"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 94
    const-string v2, "<EMAIL>"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 95
    const-string v2, "<HOST>"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 96
    const-string v2, "<NUM>"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 97
    const-string v2, "<CJ>"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 98
    const-string v2, "<ACRONYM_DEP>"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 99
    const-string v2, "<SOUTHEAST_ASIAN>"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 100
    const-string v2, "<IDEOGRAPHIC>"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 101
    const-string v2, "<HIRAGANA>"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 102
    const-string v2, "<KATAKANA>"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 103
    const-string v2, "<HANGUL>"

    aput-object v2, v0, v1

    .line 89
    sput-object v0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    .line 104
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    .line 128
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 106
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->maxTokenLength:I

    .line 155
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 156
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 157
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 158
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 129
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->init(Lorg/apache/lucene/util/Version;)V

    .line 130
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "input"    # Ljava/io/Reader;

    .prologue
    .line 136
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 106
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->maxTokenLength:I

    .line 155
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 156
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 157
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 158
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 137
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->init(Lorg/apache/lucene/util/Version;)V

    .line 138
    return-void
.end method

.method private final init(Lorg/apache/lucene/util/Version;)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    const/4 v1, 0x0

    .line 142
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_40:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardTokenizerImpl;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizerImpl;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    .line 151
    :goto_0
    return-void

    .line 144
    :cond_0
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_34:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    new-instance v0, Lorg/apache/lucene/analysis/standard/std34/StandardTokenizerImpl34;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/standard/std34/StandardTokenizerImpl34;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    goto :goto_0

    .line 146
    :cond_1
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 147
    new-instance v0, Lorg/apache/lucene/analysis/standard/std31/StandardTokenizerImpl31;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/standard/std31/StandardTokenizerImpl31;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    goto :goto_0

    .line 149
    :cond_2
    new-instance v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    goto :goto_0
.end method


# virtual methods
.method public final end()V
    .locals 3

    .prologue
    .line 202
    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yychar()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yylength()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->correctOffset(I)I

    move-result v0

    .line 203
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 204
    return-void
.end method

.method public getMaxTokenLength()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->maxTokenLength:I

    return v0
.end method

.method public final incrementToken()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->clearAttributes()V

    .line 168
    const/4 v0, 0x1

    .line 171
    .local v0, "posIncr":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->getNextToken()I

    move-result v2

    .line 173
    .local v2, "tokenType":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 174
    const/4 v3, 0x0

    .line 191
    :goto_1
    return v3

    .line 177
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yylength()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->maxTokenLength:I

    if-gt v3, v4, :cond_2

    .line 178
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 179
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->getText(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)V

    .line 180
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yychar()I

    move-result v1

    .line 181
    .local v1, "start":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->correctOffset(I)I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {p0, v5}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->correctOffset(I)I

    move-result v5

    invoke-interface {v3, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 185
    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 186
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    sget-object v4, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/4 v5, 0x5

    aget-object v4, v4, v5

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 187
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 191
    :goto_2
    const/4 v3, 0x1

    goto :goto_1

    .line 189
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    sget-object v4, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    goto :goto_2

    .line 195
    .end local v1    # "start":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 170
    goto :goto_0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->input:Ljava/io/Reader;

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yyreset(Ljava/io/Reader;)V

    .line 209
    return-void
.end method

.method public setMaxTokenLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 111
    iput p1, p0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->maxTokenLength:I

    .line 112
    return-void
.end method
