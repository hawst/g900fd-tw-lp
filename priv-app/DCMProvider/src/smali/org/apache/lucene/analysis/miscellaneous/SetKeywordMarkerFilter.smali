.class public final Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;
.super Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilter;
.source "SetKeywordMarkerFilter.java"


# instance fields
.field private final keywordSet:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "keywordSet"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 29
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 44
    iput-object p2, p0, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;->keywordSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 45
    return-void
.end method


# virtual methods
.method protected isKeyword()Z
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;->keywordSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v0

    return v0
.end method
