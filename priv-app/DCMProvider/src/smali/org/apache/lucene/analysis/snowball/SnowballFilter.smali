.class public final Lorg/apache/lucene/analysis/snowball/SnowballFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "SnowballFilter.java"


# instance fields
.field private final keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private final stemmer:Lorg/tartarus/snowball/SnowballProgram;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/lang/String;)V
    .locals 5
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 57
    const-class v2, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v2, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 58
    const-class v2, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v2, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 81
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "org.tartarus.snowball.ext."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Stemmer"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lorg/tartarus/snowball/SnowballProgram;

    invoke-virtual {v2, v3}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 82
    .local v1, "stemClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/tartarus/snowball/SnowballProgram;>;"
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/tartarus/snowball/SnowballProgram;

    iput-object v2, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->stemmer:Lorg/tartarus/snowball/SnowballProgram;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    return-void

    .line 83
    .end local v1    # "stemClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/tartarus/snowball/SnowballProgram;>;"
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid stemmer class specified: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/tartarus/snowball/SnowballProgram;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "stemmer"    # Lorg/tartarus/snowball/SnowballProgram;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 57
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 58
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 62
    iput-object p2, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->stemmer:Lorg/tartarus/snowball/SnowballProgram;

    .line 63
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 91
    iget-object v5, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v5}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 92
    iget-object v5, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v5

    if-nez v5, :cond_0

    .line 93
    iget-object v5, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v3

    .line 94
    .local v3, "termBuffer":[C
    iget-object v5, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v1

    .line 95
    .local v1, "length":I
    iget-object v5, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->stemmer:Lorg/tartarus/snowball/SnowballProgram;

    invoke-virtual {v5, v3, v1}, Lorg/tartarus/snowball/SnowballProgram;->setCurrent([CI)V

    .line 96
    iget-object v5, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->stemmer:Lorg/tartarus/snowball/SnowballProgram;

    invoke-virtual {v5}, Lorg/tartarus/snowball/SnowballProgram;->stem()Z

    .line 97
    iget-object v5, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->stemmer:Lorg/tartarus/snowball/SnowballProgram;

    invoke-virtual {v5}, Lorg/tartarus/snowball/SnowballProgram;->getCurrentBuffer()[C

    move-result-object v0

    .line 98
    .local v0, "finalTerm":[C
    iget-object v5, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->stemmer:Lorg/tartarus/snowball/SnowballProgram;

    invoke-virtual {v5}, Lorg/tartarus/snowball/SnowballProgram;->getCurrentBufferLength()I

    move-result v2

    .line 99
    .local v2, "newLength":I
    if-eq v0, v3, :cond_2

    .line 100
    iget-object v5, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5, v0, v4, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 104
    .end local v0    # "finalTerm":[C
    .end local v1    # "length":I
    .end local v2    # "newLength":I
    .end local v3    # "termBuffer":[C
    :cond_0
    :goto_0
    const/4 v4, 0x1

    .line 106
    :cond_1
    return v4

    .line 102
    .restart local v0    # "finalTerm":[C
    .restart local v1    # "length":I
    .restart local v2    # "newLength":I
    .restart local v3    # "termBuffer":[C
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    goto :goto_0
.end method
