.class public final Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "HunspellStemFilter.java"


# instance fields
.field private buffer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;",
            ">;"
        }
    .end annotation
.end field

.field private final dedup:Z

.field private final keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private final posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private savedState:Lorg/apache/lucene/util/AttributeSource$State;

.field private final stemmer:Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "dictionary"    # Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    .prologue
    .line 66
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;Z)V

    .line 67
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;Z)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "dictionary"    # Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;
    .param p3, "dedup"    # Z

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 48
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 49
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 50
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 79
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->dedup:Z

    .line 80
    new-instance v0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;

    invoke-direct {v0, p2}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;-><init>(Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->stemmer:Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;

    .line 81
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 88
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->buffer:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->buffer:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 89
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->buffer:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;

    .line 90
    .local v0, "nextStem":Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 91
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v2, v4}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 92
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->getStem()[C

    move-result-object v5

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->getStemLength()I

    move-result v6

    invoke-interface {v2, v5, v4, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 93
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->getStemLength()I

    move-result v4

    invoke-interface {v2, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move v2, v3

    .line 119
    .end local v0    # "nextStem":Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;
    :goto_0
    return v2

    .line 97
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v4

    .line 98
    goto :goto_0

    .line 101
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    .line 102
    goto :goto_0

    .line 105
    :cond_2
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->dedup:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->stemmer:Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;

    iget-object v5, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v6

    invoke-virtual {v2, v5, v6}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->uniqueStems([CI)Ljava/util/List;

    move-result-object v2

    :goto_1
    iput-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->buffer:Ljava/util/List;

    .line 107
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->buffer:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v3

    .line 108
    goto :goto_0

    .line 105
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->stemmer:Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;

    iget-object v5, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v6

    invoke-virtual {v2, v5, v6}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->stem([CI)Ljava/util/List;

    move-result-object v2

    goto :goto_1

    .line 111
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->buffer:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;

    .line 112
    .local v1, "stem":Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->getStem()[C

    move-result-object v5

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->getStemLength()I

    move-result v6

    invoke-interface {v2, v5, v4, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 113
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->getStemLength()I

    move-result v4

    invoke-interface {v2, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 115
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->buffer:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 116
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    :cond_5
    move v2, v3

    .line 119
    goto :goto_0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;->buffer:Ljava/util/List;

    .line 129
    return-void
.end method
