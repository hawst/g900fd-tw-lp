.class public Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;
.super Ljava/lang/Object;
.source "CharVector.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final DEFAULT_BLOCK_SIZE:I = 0x800


# instance fields
.field private array:[C

.field private blockSize:I

.field private n:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    const/16 v0, 0x800

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;-><init>(I)V

    .line 47
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    if-lez p1, :cond_0

    .line 51
    iput p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->blockSize:I

    .line 55
    :goto_0
    iget v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->blockSize:I

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    .line 57
    return-void

    .line 53
    :cond_0
    const/16 v0, 0x800

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->blockSize:I

    goto :goto_0
.end method

.method public constructor <init>([C)V
    .locals 1
    .param p1, "a"    # [C

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/16 v0, 0x800

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->blockSize:I

    .line 61
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    .line 62
    array-length v0, p1

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    .line 63
    return-void
.end method

.method public constructor <init>([CI)V
    .locals 1
    .param p1, "a"    # [C
    .param p2, "capacity"    # I

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    if-lez p2, :cond_0

    .line 67
    iput p2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->blockSize:I

    .line 71
    :goto_0
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    .line 72
    array-length v0, p1

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    .line 73
    return-void

    .line 69
    :cond_0
    const/16 v0, 0x800

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->blockSize:I

    goto :goto_0
.end method


# virtual methods
.method public alloc(I)I
    .locals 5
    .param p1, "size"    # I

    .prologue
    const/4 v4, 0x0

    .line 116
    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    .line 117
    .local v1, "index":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    array-length v2, v3

    .line 118
    .local v2, "len":I
    iget v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    add-int/2addr v3, p1

    if-lt v3, v2, :cond_0

    .line 119
    iget v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->blockSize:I

    add-int/2addr v3, v2

    new-array v0, v3, [C

    .line 120
    .local v0, "aux":[C
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    invoke-static {v3, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 121
    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    .line 123
    .end local v0    # "aux":[C
    :cond_0
    iget v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    add-int/2addr v3, p1

    iput v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    .line 124
    return v1
.end method

.method public capacity()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    array-length v0, v0

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    .line 80
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->clone()Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;
    .locals 3

    .prologue
    .line 84
    new-instance v0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    invoke-virtual {v1}, [C->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iget v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->blockSize:I

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;-><init>([CI)V

    .line 85
    .local v0, "cv":Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;
    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    iput v1, v0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    .line 86
    return-object v0
.end method

.method public get(I)C
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    aget-char v0, v0, p1

    return v0
.end method

.method public getArray()[C
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    return v0
.end method

.method public put(IC)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "val"    # C

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    aput-char p2, v0, p1

    .line 109
    return-void
.end method

.method public trimToSize()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 128
    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 129
    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    new-array v0, v1, [C

    .line 130
    .local v0, "aux":[C
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    iget v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->n:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131
    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->array:[C

    .line 133
    .end local v0    # "aux":[C
    :cond_0
    return-void
.end method
