.class public final Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "LimitTokenCountFilter.java"


# instance fields
.field private final consumeAllTokens:Z

.field private exhausted:Z

.field private final maxTokenCount:I

.field private tokenCount:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;I)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "maxTokenCount"    # I

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;IZ)V

    .line 54
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;IZ)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "maxTokenCount"    # I
    .param p3, "consumeAllTokens"    # Z

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 43
    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->tokenCount:I

    .line 44
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->exhausted:Z

    .line 64
    iput p2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->maxTokenCount:I

    .line 65
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->consumeAllTokens:Z

    .line 66
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 70
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->exhausted:Z

    if-eqz v2, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 72
    :cond_1
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->tokenCount:I

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->maxTokenCount:I

    if-ge v2, v3, :cond_3

    .line 73
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 74
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->tokenCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->tokenCount:I

    move v0, v1

    .line 75
    goto :goto_0

    .line 77
    :cond_2
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->exhausted:Z

    goto :goto_0

    .line 81
    :cond_3
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->consumeAllTokens:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 89
    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->tokenCount:I

    .line 90
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;->exhausted:Z

    .line 91
    return-void
.end method
