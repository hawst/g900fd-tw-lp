.class Lorg/apache/lucene/analysis/lv/LatvianAnalyzer$DefaultSetHolder;
.super Ljava/lang/Object;
.source "LatvianAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/lv/LatvianAnalyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultSetHolder"
.end annotation


# static fields
.field static final DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 63
    :try_start_0
    const-class v1, Lorg/apache/lucene/analysis/lv/LatvianAnalyzer;

    .line 64
    const-string v2, "stopwords.txt"

    sget-object v3, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    .line 63
    invoke-static {v1, v2, v3}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/lang/Class;Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/io/Reader;

    move-result-object v1

    .line 64
    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    .line 63
    invoke-static {v1, v2}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v1

    sput-object v1, Lorg/apache/lucene/analysis/lv/LatvianAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    return-void

    .line 65
    :catch_0
    move-exception v0

    .line 68
    .local v0, "ex":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to load default stopword set"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
