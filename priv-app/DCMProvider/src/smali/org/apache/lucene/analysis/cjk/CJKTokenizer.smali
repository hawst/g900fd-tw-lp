.class public final Lorg/apache/lucene/analysis/cjk/CJKTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "CJKTokenizer.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final DOUBLE_TOKEN_TYPE:I = 0x2

.field private static final IO_BUFFER_SIZE:I = 0x100

.field private static final MAX_WORD_LEN:I = 0xff

.field static final SINGLE_TOKEN_TYPE:I = 0x1

.field static final TOKEN_TYPE_NAMES:[Ljava/lang/String;

.field static final WORD_TYPE:I


# instance fields
.field private final buffer:[C

.field private bufferIndex:I

.field private dataLen:I

.field private final ioBuffer:[C

.field private offset:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private preIsTokened:Z

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private tokenType:I

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "word"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "single"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "double"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->TOKEN_TYPE_NAMES:[Ljava/lang/String;

    .line 67
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 2
    .param p1, "in"    # Ljava/io/Reader;

    .prologue
    const/4 v1, 0x0

    .line 114
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 72
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    .line 75
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    .line 78
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->dataLen:I

    .line 84
    const/16 v0, 0xff

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->buffer:[C

    .line 90
    const/16 v0, 0x100

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->ioBuffer:[C

    .line 93
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->tokenType:I

    .line 100
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    .line 102
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 103
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 104
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 115
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 2
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    const/4 v1, 0x0

    .line 118
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 72
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    .line 75
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    .line 78
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->dataLen:I

    .line 84
    const/16 v0, 0xff

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->buffer:[C

    .line 90
    const/16 v0, 0x100

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->ioBuffer:[C

    .line 93
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->tokenType:I

    .line 100
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    .line 102
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 103
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 104
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 119
    return-void
.end method


# virtual methods
.method public final end()V
    .locals 2

    .prologue
    .line 295
    iget v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->correctOffset(I)I

    move-result v0

    .line 296
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 297
    return-void
.end method

.method public incrementToken()Z
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, -0x1

    const/4 v11, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 136
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->clearAttributes()V

    .line 141
    :cond_0
    const/4 v2, 0x0

    .line 144
    .local v2, "length":I
    iget v4, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    .line 153
    .local v4, "start":I
    :cond_1
    :goto_0
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    .line 155
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    iget v9, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->dataLen:I

    if-lt v8, v9, :cond_2

    .line 156
    iget-object v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->input:Ljava/io/Reader;

    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->ioBuffer:[C

    invoke-virtual {v8, v9}, Ljava/io/Reader;->read([C)I

    move-result v8

    iput v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->dataLen:I

    .line 157
    iput v6, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    .line 160
    :cond_2
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->dataLen:I

    if-ne v8, v12, :cond_6

    .line 161
    if-lez v2, :cond_5

    .line 162
    iget-boolean v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    if-eqz v8, :cond_4

    .line 163
    const/4 v2, 0x0

    .line 164
    iput-boolean v6, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    .line 277
    :cond_3
    :goto_1
    if-lez v2, :cond_11

    .line 278
    iget-object v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->buffer:[C

    invoke-interface {v8, v9, v6, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 279
    iget-object v6, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->correctOffset(I)I

    move-result v8

    add-int v9, v4, v2

    invoke-virtual {p0, v9}, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->correctOffset(I)I

    move-result v9

    invoke-interface {v6, v8, v9}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 280
    iget-object v6, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    sget-object v8, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->TOKEN_TYPE_NAMES:[Ljava/lang/String;

    iget v9, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->tokenType:I

    aget-object v8, v8, v9

    invoke-interface {v6, v8}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    move v6, v7

    .line 284
    :goto_2
    return v6

    .line 167
    :cond_4
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    goto :goto_1

    .line 172
    :cond_5
    iget v7, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    goto :goto_2

    .line 177
    :cond_6
    iget-object v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->ioBuffer:[C

    iget v9, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    add-int/lit8 v10, v9, 0x1

    iput v10, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    aget-char v0, v8, v9

    .line 180
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v5

    .line 184
    .local v5, "ub":Ljava/lang/Character$UnicodeBlock;
    sget-object v8, Ljava/lang/Character$UnicodeBlock;->BASIC_LATIN:Ljava/lang/Character$UnicodeBlock;

    if-eq v5, v8, :cond_7

    .line 185
    sget-object v8, Ljava/lang/Character$UnicodeBlock;->HALFWIDTH_AND_FULLWIDTH_FORMS:Ljava/lang/Character$UnicodeBlock;

    if-ne v5, v8, :cond_d

    .line 187
    :cond_7
    sget-object v8, Ljava/lang/Character$UnicodeBlock;->HALFWIDTH_AND_FULLWIDTH_FORMS:Ljava/lang/Character$UnicodeBlock;

    if-ne v5, v8, :cond_8

    .line 188
    move v1, v0

    .line 189
    .local v1, "i":I
    const v8, 0xff01

    if-lt v1, v8, :cond_8

    const v8, 0xff5e

    if-gt v1, v8, :cond_8

    .line 191
    const v8, 0xfee0

    sub-int/2addr v1, v8

    .line 192
    int-to-char v0, v1

    .line 197
    .end local v1    # "i":I
    :cond_8
    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v8

    if-nez v8, :cond_9

    .line 198
    const/16 v8, 0x5f

    if-eq v0, v8, :cond_9

    const/16 v8, 0x2b

    if-eq v0, v8, :cond_9

    const/16 v8, 0x23

    if-ne v0, v8, :cond_c

    .line 200
    :cond_9
    if-nez v2, :cond_b

    .line 204
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    add-int/lit8 v4, v8, -0x1

    .line 223
    :cond_a
    iget-object v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->buffer:[C

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "length":I
    .local v3, "length":I
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v9

    aput-char v9, v8, v2

    .line 224
    iput v7, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->tokenType:I

    .line 227
    const/16 v8, 0xff

    if-ne v3, v8, :cond_12

    move v2, v3

    .line 228
    .end local v3    # "length":I
    .restart local v2    # "length":I
    goto/16 :goto_1

    .line 205
    :cond_b
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->tokenType:I

    if-ne v8, v11, :cond_a

    .line 209
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    .line 210
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    .line 212
    iget-boolean v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    if-eqz v8, :cond_3

    .line 214
    const/4 v2, 0x0

    .line 215
    iput-boolean v6, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    goto/16 :goto_1

    .line 230
    :cond_c
    if-lez v2, :cond_1

    .line 231
    iget-boolean v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    if-eqz v8, :cond_3

    .line 232
    const/4 v2, 0x0

    .line 233
    iput-boolean v6, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    goto/16 :goto_0

    .line 240
    :cond_d
    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 241
    if-nez v2, :cond_e

    .line 242
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    add-int/lit8 v4, v8, -0x1

    .line 243
    iget-object v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->buffer:[C

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "length":I
    .restart local v3    # "length":I
    aput-char v0, v8, v2

    .line 244
    iput v11, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->tokenType:I

    move v2, v3

    .line 245
    .end local v3    # "length":I
    .restart local v2    # "length":I
    goto/16 :goto_0

    .line 246
    :cond_e
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->tokenType:I

    if-ne v8, v7, :cond_f

    .line 247
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    .line 248
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    goto/16 :goto_1

    .line 253
    :cond_f
    iget-object v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->buffer:[C

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "length":I
    .restart local v3    # "length":I
    aput-char v0, v8, v2

    .line 254
    iput v11, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->tokenType:I

    .line 256
    if-ne v3, v11, :cond_12

    .line 257
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    .line 258
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    .line 259
    iput-boolean v7, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    move v2, v3

    .line 261
    .end local v3    # "length":I
    .restart local v2    # "length":I
    goto/16 :goto_1

    .line 265
    :cond_10
    if-lez v2, :cond_1

    .line 266
    iget-boolean v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    if-eqz v8, :cond_3

    .line 268
    const/4 v2, 0x0

    .line 269
    iput-boolean v6, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    goto/16 :goto_0

    .line 282
    .end local v0    # "c":C
    .end local v5    # "ub":Ljava/lang/Character$UnicodeBlock;
    :cond_11
    iget v8, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->dataLen:I

    if-ne v8, v12, :cond_0

    .line 283
    iget v7, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    goto/16 :goto_2

    .end local v2    # "length":I
    .restart local v0    # "c":C
    .restart local v3    # "length":I
    .restart local v5    # "ub":Ljava/lang/Character$UnicodeBlock;
    :cond_12
    move v2, v3

    .end local v3    # "length":I
    .restart local v2    # "length":I
    goto/16 :goto_0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 301
    invoke-super {p0}, Lorg/apache/lucene/analysis/Tokenizer;->reset()V

    .line 302
    iput v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->dataLen:I

    iput v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->bufferIndex:I

    iput v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->offset:I

    .line 303
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->preIsTokened:Z

    .line 304
    iput v0, p0, Lorg/apache/lucene/analysis/cjk/CJKTokenizer;->tokenType:I

    .line 305
    return-void
.end method
