.class public final Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "NGramTokenFilter.java"


# static fields
.field public static final DEFAULT_MAX_NGRAM_SIZE:I = 0x2

.field public static final DEFAULT_MIN_NGRAM_SIZE:I = 0x1


# instance fields
.field private curGramSize:I

.field private curPos:I

.field private curTermBuffer:[C

.field private curTermLength:I

.field private hasIllegalOffsets:Z

.field private maxGram:I

.field private minGram:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private tokEnd:I

.field private tokStart:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 70
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;II)V

    .line 71
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;II)V
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "minGram"    # I
    .param p3, "maxGram"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 44
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 45
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 55
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minGram must be greater than zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    if-le p2, p3, :cond_1

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minGram must not be greater than maxGram"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_1
    iput p2, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->minGram:I

    .line 62
    iput p3, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->maxGram:I

    .line 63
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curTermBuffer:[C

    if-nez v0, :cond_1

    .line 78
    iget-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    :goto_1
    return v2

    .line 81
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    invoke-virtual {v0}, [C->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curTermBuffer:[C

    .line 82
    iget-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curTermLength:I

    .line 83
    iget v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->minGram:I

    iput v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curGramSize:I

    .line 84
    iput v2, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curPos:I

    .line 85
    iget-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->tokStart:I

    .line 86
    iget-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->tokEnd:I

    .line 89
    iget v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->tokStart:I

    iget v3, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curTermLength:I

    add-int/2addr v0, v3

    iget v3, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->tokEnd:I

    if-eq v0, v3, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->hasIllegalOffsets:Z

    .line 92
    :cond_1
    :goto_3
    iget v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curGramSize:I

    iget v3, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->maxGram:I

    if-le v0, v3, :cond_3

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curTermBuffer:[C

    goto :goto_0

    :cond_2
    move v0, v2

    .line 89
    goto :goto_2

    .line 93
    :cond_3
    iget v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curPos:I

    iget v3, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curGramSize:I

    add-int/2addr v0, v3

    iget v3, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curTermLength:I

    if-gt v0, v3, :cond_5

    .line 94
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->clearAttributes()V

    .line 95
    iget-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curTermBuffer:[C

    iget v3, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curPos:I

    iget v4, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curGramSize:I

    invoke-interface {v0, v2, v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 96
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->hasIllegalOffsets:Z

    if-eqz v0, :cond_4

    .line 97
    iget-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v2, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->tokStart:I

    iget v3, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->tokEnd:I

    invoke-interface {v0, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 101
    :goto_4
    iget v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curPos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curPos:I

    move v2, v1

    .line 102
    goto :goto_1

    .line 99
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v2, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->tokStart:I

    iget v3, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curPos:I

    add-int/2addr v2, v3

    iget v3, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->tokStart:I

    iget v4, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curPos:I

    add-int/2addr v3, v4

    iget v4, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curGramSize:I

    add-int/2addr v3, v4

    invoke-interface {v0, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    goto :goto_4

    .line 104
    :cond_5
    iget v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curGramSize:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curGramSize:I

    .line 105
    iput v2, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curPos:I

    goto :goto_3
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenFilter;->curTermBuffer:[C

    .line 115
    return-void
.end method
