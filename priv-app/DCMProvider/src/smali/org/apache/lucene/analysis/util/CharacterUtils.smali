.class public abstract Lorg/apache/lucene/analysis/util/CharacterUtils;
.super Ljava/lang/Object;
.source "CharacterUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;,
        Lorg/apache/lucene/analysis/util/CharacterUtils$Java4CharacterUtils;,
        Lorg/apache/lucene/analysis/util/CharacterUtils$Java5CharacterUtils;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final JAVA_4:Lorg/apache/lucene/analysis/util/CharacterUtils$Java4CharacterUtils;

.field private static final JAVA_5:Lorg/apache/lucene/analysis/util/CharacterUtils$Java5CharacterUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/apache/lucene/analysis/util/CharacterUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/util/CharacterUtils;->$assertionsDisabled:Z

    .line 33
    new-instance v0, Lorg/apache/lucene/analysis/util/CharacterUtils$Java4CharacterUtils;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/util/CharacterUtils$Java4CharacterUtils;-><init>()V

    sput-object v0, Lorg/apache/lucene/analysis/util/CharacterUtils;->JAVA_4:Lorg/apache/lucene/analysis/util/CharacterUtils$Java4CharacterUtils;

    .line 34
    new-instance v0, Lorg/apache/lucene/analysis/util/CharacterUtils$Java5CharacterUtils;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/util/CharacterUtils$Java5CharacterUtils;-><init>()V

    sput-object v0, Lorg/apache/lucene/analysis/util/CharacterUtils;->JAVA_5:Lorg/apache/lucene/analysis/util/CharacterUtils$Java5CharacterUtils;

    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharacterUtils;
    .locals 1
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 46
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/lucene/analysis/util/CharacterUtils;->JAVA_5:Lorg/apache/lucene/analysis/util/CharacterUtils$Java5CharacterUtils;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/lucene/analysis/util/CharacterUtils;->JAVA_4:Lorg/apache/lucene/analysis/util/CharacterUtils$Java4CharacterUtils;

    goto :goto_0
.end method

.method public static newCharacterBuffer(I)Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;
    .locals 3
    .param p0, "bufferSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 124
    const/4 v0, 0x2

    if-ge p0, v0, :cond_0

    .line 125
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "buffersize must be >= 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;

    new-array v1, p0, [C

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;-><init>([CII)V

    return-object v0
.end method


# virtual methods
.method public abstract codePointAt(Ljava/lang/CharSequence;I)I
.end method

.method public abstract codePointAt([CI)I
.end method

.method public abstract codePointAt([CII)I
.end method

.method public abstract fill(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;Ljava/io/Reader;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public toLowerCase([CII)V
    .locals 2
    .param p1, "buffer"    # [C
    .param p2, "offset"    # I
    .param p3, "limit"    # I

    .prologue
    .line 139
    sget-boolean v1, Lorg/apache/lucene/analysis/util/CharacterUtils;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    array-length v1, p1

    if-ge v1, p3, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 140
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/analysis/util/CharacterUtils;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    if-gtz p2, :cond_1

    array-length v1, p1

    if-le p2, v1, :cond_2

    :cond_1
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 141
    :cond_2
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_3

    .line 146
    return-void

    .line 144
    :cond_3
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/util/CharacterUtils;->codePointAt([CI)I

    move-result v1

    .line 143
    invoke-static {v1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v1

    .line 144
    invoke-static {v1, p1, v0}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method
