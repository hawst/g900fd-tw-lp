.class public abstract Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "StopwordAnalyzerBase.java"


# instance fields
.field protected final matchVersion:Lorg/apache/lucene/util/Version;

.field protected final stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "version"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 75
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "version"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    .line 61
    iput-object p1, p0, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 63
    if-nez p2, :cond_0

    sget-object v0, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 65
    return-void

    .line 64
    :cond_0
    invoke-static {p1, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    goto :goto_0
.end method

.method protected static loadStopwordSet(Ljava/io/File;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 4
    .param p0, "stopwords"    # Ljava/io/File;
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 123
    const/4 v0, 0x0

    .line 125
    .local v0, "reader":Ljava/io/Reader;
    :try_start_0
    sget-object v1, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-static {p0, v1}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/io/Reader;

    move-result-object v0

    .line 126
    invoke-static {v0, p1}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharArraySet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 127
    new-array v2, v2, [Ljava/io/Closeable;

    .line 128
    aput-object v0, v2, v3

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 126
    return-object v1

    .line 127
    :catchall_0
    move-exception v1

    new-array v2, v2, [Ljava/io/Closeable;

    .line 128
    aput-object v0, v2, v3

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 129
    throw v1
.end method

.method protected static loadStopwordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 3
    .param p0, "stopwords"    # Ljava/io/Reader;
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 148
    :try_start_0
    invoke-static {p0, p1}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharArraySet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 149
    new-array v1, v1, [Ljava/io/Closeable;

    .line 150
    aput-object p0, v1, v2

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 148
    return-object v0

    .line 149
    :catchall_0
    move-exception v0

    new-array v1, v1, [Ljava/io/Closeable;

    .line 150
    aput-object p0, v1, v2

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 151
    throw v0
.end method

.method protected static loadStopwordSet(ZLjava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 6
    .param p0, "ignoreCase"    # Z
    .param p2, "resource"    # Ljava/lang/String;
    .param p3, "comment"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lorg/apache/lucene/analysis/util/CharArraySet;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "aClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/analysis/Analyzer;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 98
    const/4 v0, 0x0

    .line 100
    .local v0, "reader":Ljava/io/Reader;
    :try_start_0
    invoke-virtual {p1, p2}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v1, v2}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Ljava/io/Reader;

    move-result-object v0

    .line 101
    new-instance v1, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3, p0}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    invoke-static {v0, p3, v1}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getWordSet(Ljava/io/Reader;Ljava/lang/String;Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 102
    new-array v2, v5, [Ljava/io/Closeable;

    .line 103
    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 101
    return-object v1

    .line 102
    :catchall_0
    move-exception v1

    new-array v2, v5, [Ljava/io/Closeable;

    .line 103
    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 104
    throw v1
.end method


# virtual methods
.method public getStopwordSet()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method
