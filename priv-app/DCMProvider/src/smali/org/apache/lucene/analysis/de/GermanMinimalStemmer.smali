.class public Lorg/apache/lucene/analysis/de/GermanMinimalStemmer;
.super Ljava/lang/Object;
.source "GermanMinimalStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 5
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v4, 0x6e

    const/4 v3, 0x5

    const/16 v2, 0x65

    .line 65
    if-ge p2, v3, :cond_0

    .line 93
    .end local p2    # "len":I
    :goto_0
    return p2

    .line 68
    .restart local p2    # "len":I
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, p2, :cond_1

    .line 75
    const/4 v1, 0x6

    if-le p2, v1, :cond_2

    add-int/lit8 v1, p2, -0x3

    aget-char v1, p1, v1

    if-ne v1, v4, :cond_2

    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    if-ne v1, v2, :cond_2

    add-int/lit8 v1, p2, -0x1

    aget-char v1, p1, v1

    if-ne v1, v4, :cond_2

    .line 76
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 69
    :cond_1
    aget-char v1, p1, v0

    sparse-switch v1, :sswitch_data_0

    .line 68
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 70
    :sswitch_0
    const/16 v1, 0x61

    aput-char v1, p1, v0

    goto :goto_2

    .line 71
    :sswitch_1
    const/16 v1, 0x6f

    aput-char v1, p1, v0

    goto :goto_2

    .line 72
    :sswitch_2
    const/16 v1, 0x75

    aput-char v1, p1, v0

    goto :goto_2

    .line 78
    :cond_2
    if-le p2, v3, :cond_3

    .line 79
    add-int/lit8 v1, p2, -0x1

    aget-char v1, p1, v1

    sparse-switch v1, :sswitch_data_1

    .line 86
    :cond_3
    add-int/lit8 v1, p2, -0x1

    aget-char v1, p1, v1

    sparse-switch v1, :sswitch_data_2

    goto :goto_0

    .line 90
    :sswitch_3
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 80
    :sswitch_4
    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    if-ne v1, v2, :cond_3

    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 81
    :sswitch_5
    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    const/16 v2, 0x73

    if-ne v1, v2, :cond_3

    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 82
    :sswitch_6
    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    if-ne v1, v2, :cond_3

    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 83
    :sswitch_7
    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    if-ne v1, v2, :cond_3

    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 69
    :sswitch_data_0
    .sparse-switch
        0xe4 -> :sswitch_0
        0xf6 -> :sswitch_1
        0xfc -> :sswitch_2
    .end sparse-switch

    .line 79
    :sswitch_data_1
    .sparse-switch
        0x65 -> :sswitch_5
        0x6e -> :sswitch_4
        0x72 -> :sswitch_7
        0x73 -> :sswitch_6
    .end sparse-switch

    .line 86
    :sswitch_data_2
    .sparse-switch
        0x65 -> :sswitch_3
        0x6e -> :sswitch_3
        0x72 -> :sswitch_3
        0x73 -> :sswitch_3
    .end sparse-switch
.end method
