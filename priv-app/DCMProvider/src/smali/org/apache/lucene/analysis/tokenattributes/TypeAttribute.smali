.class public interface abstract Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;
.super Ljava/lang/Object;
.source "TypeAttribute.java"

# interfaces
.implements Lorg/apache/lucene/util/Attribute;


# static fields
.field public static final DEFAULT_TYPE:Ljava/lang/String; = "word"


# virtual methods
.method public abstract setType(Ljava/lang/String;)V
.end method

.method public abstract type()Ljava/lang/String;
.end method
