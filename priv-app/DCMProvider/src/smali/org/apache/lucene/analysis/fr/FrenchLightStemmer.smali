.class public Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;
.super Ljava/lang/Object;
.source "FrenchLightStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private norm([CI)I
    .locals 6
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v5, 0x65

    const/4 v4, 0x4

    .line 231
    if-le p2, v4, :cond_0

    .line 232
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p2, :cond_6

    .line 247
    const/4 v3, 0x0

    aget-char v0, p1, v3

    .line 248
    .local v0, "ch":C
    const/4 v1, 0x1

    move v2, v1

    .end local v1    # "i":I
    .local v2, "i":I
    :goto_1
    if-lt v2, p2, :cond_7

    .line 256
    .end local v0    # "ch":C
    .end local v2    # "i":I
    :cond_0
    if-le p2, v4, :cond_1

    const-string v3, "ie"

    invoke-static {p1, p2, v3}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 257
    add-int/lit8 p2, p2, -0x2

    .line 259
    :cond_1
    if-le p2, v4, :cond_5

    .line 260
    add-int/lit8 v3, p2, -0x1

    aget-char v3, p1, v3

    const/16 v4, 0x72

    if-ne v3, v4, :cond_2

    add-int/lit8 p2, p2, -0x1

    .line 261
    :cond_2
    add-int/lit8 v3, p2, -0x1

    aget-char v3, p1, v3

    if-ne v3, v5, :cond_3

    add-int/lit8 p2, p2, -0x1

    .line 262
    :cond_3
    add-int/lit8 v3, p2, -0x1

    aget-char v3, p1, v3

    if-ne v3, v5, :cond_4

    add-int/lit8 p2, p2, -0x1

    .line 263
    :cond_4
    add-int/lit8 v3, p2, -0x1

    aget-char v3, p1, v3

    add-int/lit8 v4, p2, -0x2

    aget-char v4, p1, v4

    if-ne v3, v4, :cond_5

    add-int/lit8 v3, p2, -0x1

    aget-char v3, p1, v3

    invoke-static {v3}, Ljava/lang/Character;->isLetter(C)Z

    move-result v3

    if-eqz v3, :cond_5

    add-int/lit8 p2, p2, -0x1

    .line 265
    :cond_5
    return p2

    .line 233
    .restart local v1    # "i":I
    :cond_6
    aget-char v3, p1, v1

    sparse-switch v3, :sswitch_data_0

    .line 232
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 236
    :sswitch_0
    const/16 v3, 0x61

    aput-char v3, p1, v1

    goto :goto_2

    .line 237
    :sswitch_1
    const/16 v3, 0x6f

    aput-char v3, p1, v1

    goto :goto_2

    .line 240
    :sswitch_2
    aput-char v5, p1, v1

    goto :goto_2

    .line 242
    :sswitch_3
    const/16 v3, 0x75

    aput-char v3, p1, v1

    goto :goto_2

    .line 243
    :sswitch_4
    const/16 v3, 0x69

    aput-char v3, p1, v1

    goto :goto_2

    .line 244
    :sswitch_5
    const/16 v3, 0x63

    aput-char v3, p1, v1

    goto :goto_2

    .line 249
    .end local v1    # "i":I
    .restart local v0    # "ch":C
    .restart local v2    # "i":I
    :cond_7
    aget-char v3, p1, v2

    if-ne v3, v0, :cond_8

    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 250
    add-int/lit8 v1, v2, -0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-static {p1, v2, p2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result p2

    .line 248
    :goto_3
    add-int/lit8 v1, v1, 0x1

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_1

    .line 252
    :cond_8
    aget-char v0, p1, v2

    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_3

    .line 233
    :sswitch_data_0
    .sparse-switch
        0xe0 -> :sswitch_0
        0xe1 -> :sswitch_0
        0xe2 -> :sswitch_0
        0xe7 -> :sswitch_5
        0xe8 -> :sswitch_2
        0xe9 -> :sswitch_2
        0xea -> :sswitch_2
        0xee -> :sswitch_4
        0xf4 -> :sswitch_1
        0xf9 -> :sswitch_3
        0xfb -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public stem([CI)I
    .locals 7
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v6, 0x75

    const/16 v5, 0x9

    const/16 v4, 0x8

    const/16 v3, 0x72

    const/16 v2, 0x65

    .line 67
    const/4 v0, 0x5

    if-le p2, v0, :cond_1

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x78

    if-ne v0, v1, :cond_1

    .line 68
    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    const/16 v1, 0x61

    if-ne v0, v1, :cond_0

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    if-ne v0, v6, :cond_0

    add-int/lit8 v0, p2, -0x4

    aget-char v0, p1, v0

    if-eq v0, v2, :cond_0

    .line 69
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x6c

    aput-char v1, p1, v0

    .line 70
    :cond_0
    add-int/lit8 p2, p2, -0x1

    .line 73
    :cond_1
    const/4 v0, 0x3

    if-le p2, v0, :cond_2

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x78

    if-ne v0, v1, :cond_2

    .line 74
    add-int/lit8 p2, p2, -0x1

    .line 76
    :cond_2
    const/4 v0, 0x3

    if-le p2, v0, :cond_3

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x73

    if-ne v0, v1, :cond_3

    .line 77
    add-int/lit8 p2, p2, -0x1

    .line 79
    :cond_3
    if-le p2, v5, :cond_4

    const-string v0, "issement"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 80
    add-int/lit8 p2, p2, -0x6

    .line 81
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    .line 82
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    .line 227
    :goto_0
    return v0

    .line 85
    :cond_4
    if-le p2, v4, :cond_5

    const-string v0, "issant"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 86
    add-int/lit8 p2, p2, -0x4

    .line 87
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    .line 88
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto :goto_0

    .line 91
    :cond_5
    const/4 v0, 0x6

    if-le p2, v0, :cond_7

    const-string v0, "ement"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 92
    add-int/lit8 p2, p2, -0x4

    .line 93
    const/4 v0, 0x3

    if-le p2, v0, :cond_6

    const-string v0, "ive"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 94
    add-int/lit8 p2, p2, -0x1

    .line 95
    add-int/lit8 v0, p2, -0x1

    const/16 v1, 0x66

    aput-char v1, p1, v0

    .line 97
    :cond_6
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto :goto_0

    .line 100
    :cond_7
    const/16 v0, 0xb

    if-le p2, v0, :cond_8

    const-string v0, "ficatrice"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 101
    add-int/lit8 p2, p2, -0x5

    .line 102
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 103
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    .line 104
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto :goto_0

    .line 107
    :cond_8
    const/16 v0, 0xa

    if-le p2, v0, :cond_9

    const-string v0, "ficateur"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 108
    add-int/lit8 p2, p2, -0x4

    .line 109
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 110
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    .line 111
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto :goto_0

    .line 114
    :cond_9
    if-le p2, v5, :cond_a

    const-string v0, "catrice"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 115
    add-int/lit8 p2, p2, -0x3

    .line 116
    add-int/lit8 v0, p2, -0x4

    const/16 v1, 0x71

    aput-char v1, p1, v0

    .line 117
    add-int/lit8 v0, p2, -0x3

    aput-char v6, p1, v0

    .line 118
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 120
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 123
    :cond_a
    if-le p2, v4, :cond_b

    const-string v0, "cateur"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 124
    add-int/lit8 p2, p2, -0x2

    .line 125
    add-int/lit8 v0, p2, -0x4

    const/16 v1, 0x71

    aput-char v1, p1, v0

    .line 126
    add-int/lit8 v0, p2, -0x3

    aput-char v6, p1, v0

    .line 127
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 128
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    .line 129
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 132
    :cond_b
    if-le p2, v4, :cond_c

    const-string v0, "atrice"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 133
    add-int/lit8 p2, p2, -0x4

    .line 134
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 135
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    .line 136
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 139
    :cond_c
    const/4 v0, 0x7

    if-le p2, v0, :cond_d

    const-string v0, "ateur"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 140
    add-int/lit8 p2, p2, -0x3

    .line 141
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 142
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    .line 143
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 146
    :cond_d
    const/4 v0, 0x6

    if-le p2, v0, :cond_e

    const-string v0, "trice"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 147
    add-int/lit8 p2, p2, -0x1

    .line 148
    add-int/lit8 v0, p2, -0x3

    aput-char v2, p1, v0

    .line 149
    add-int/lit8 v0, p2, -0x2

    aput-char v6, p1, v0

    .line 150
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    .line 153
    :cond_e
    const/4 v0, 0x5

    if-le p2, v0, :cond_f

    const-string v0, "i\u00e8me"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 154
    add-int/lit8 v0, p2, -0x4

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 156
    :cond_f
    const/4 v0, 0x7

    if-le p2, v0, :cond_10

    const-string v0, "teuse"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 157
    add-int/lit8 p2, p2, -0x2

    .line 158
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    .line 159
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 162
    :cond_10
    const/4 v0, 0x6

    if-le p2, v0, :cond_11

    const-string v0, "teur"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 163
    add-int/lit8 p2, p2, -0x1

    .line 164
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    .line 165
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 168
    :cond_11
    const/4 v0, 0x5

    if-le p2, v0, :cond_12

    const-string v0, "euse"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 169
    add-int/lit8 v0, p2, -0x2

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 171
    :cond_12
    if-le p2, v4, :cond_13

    const-string/jumbo v0, "\u00e8re"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 172
    add-int/lit8 p2, p2, -0x1

    .line 173
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 174
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 177
    :cond_13
    const/4 v0, 0x7

    if-le p2, v0, :cond_14

    const-string v0, "ive"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 178
    add-int/lit8 p2, p2, -0x1

    .line 179
    add-int/lit8 v0, p2, -0x1

    const/16 v1, 0x66

    aput-char v1, p1, v0

    .line 180
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 183
    :cond_14
    const/4 v0, 0x4

    if-le p2, v0, :cond_16

    .line 184
    const-string v0, "folle"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 185
    const-string v0, "molle"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 186
    :cond_15
    add-int/lit8 p2, p2, -0x2

    .line 187
    add-int/lit8 v0, p2, -0x1

    aput-char v6, p1, v0

    .line 188
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 191
    :cond_16
    if-le p2, v5, :cond_17

    const-string v0, "nnelle"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 192
    add-int/lit8 v0, p2, -0x5

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 194
    :cond_17
    if-le p2, v5, :cond_18

    const-string v0, "nnel"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 195
    add-int/lit8 v0, p2, -0x3

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 197
    :cond_18
    const/4 v0, 0x4

    if-le p2, v0, :cond_19

    const-string/jumbo v0, "\u00e8te"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 198
    add-int/lit8 p2, p2, -0x1

    .line 199
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 202
    :cond_19
    if-le p2, v4, :cond_1a

    const-string v0, "ique"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 203
    add-int/lit8 p2, p2, -0x4

    .line 205
    :cond_1a
    if-le p2, v4, :cond_1b

    const-string v0, "esse"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 206
    add-int/lit8 v0, p2, -0x3

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 208
    :cond_1b
    const/4 v0, 0x7

    if-le p2, v0, :cond_1c

    const-string v0, "inage"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 209
    add-int/lit8 v0, p2, -0x3

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 211
    :cond_1c
    if-le p2, v5, :cond_1e

    const-string v0, "isation"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 212
    add-int/lit8 p2, p2, -0x7

    .line 213
    const/4 v0, 0x5

    if-le p2, v0, :cond_1d

    const-string v0, "ual"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 214
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 215
    :cond_1d
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 218
    :cond_1e
    if-le p2, v5, :cond_1f

    const-string v0, "isateur"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 219
    add-int/lit8 v0, p2, -0x7

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 221
    :cond_1f
    if-le p2, v4, :cond_20

    const-string v0, "ation"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 222
    add-int/lit8 v0, p2, -0x5

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 224
    :cond_20
    if-le p2, v4, :cond_21

    const-string v0, "ition"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 225
    add-int/lit8 v0, p2, -0x5

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0

    .line 227
    :cond_21
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fr/FrenchLightStemmer;->norm([CI)I

    move-result v0

    goto/16 :goto_0
.end method
