.class public final Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountAnalyzer;
.super Lorg/apache/lucene/analysis/AnalyzerWrapper;
.source "LimitTokenCountAnalyzer.java"


# instance fields
.field private final consumeAllTokens:Z

.field private final delegate:Lorg/apache/lucene/analysis/Analyzer;

.field private final maxTokenCount:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;I)V
    .locals 1
    .param p1, "delegate"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p2, "maxTokenCount"    # I

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountAnalyzer;-><init>(Lorg/apache/lucene/analysis/Analyzer;IZ)V

    .line 41
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;IZ)V
    .locals 0
    .param p1, "delegate"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p2, "maxTokenCount"    # I
    .param p3, "consumeAllTokens"    # Z

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/analysis/AnalyzerWrapper;-><init>()V

    .line 49
    iput-object p1, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountAnalyzer;->delegate:Lorg/apache/lucene/analysis/Analyzer;

    .line 50
    iput p2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountAnalyzer;->maxTokenCount:I

    .line 51
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountAnalyzer;->consumeAllTokens:Z

    .line 52
    return-void
.end method


# virtual methods
.method protected getWrappedAnalyzer(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountAnalyzer;->delegate:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LimitTokenCountAnalyzer("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountAnalyzer;->delegate:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxTokenCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountAnalyzer;->maxTokenCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", consumeAllTokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountAnalyzer;->consumeAllTokens:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected wrapComponents(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "components"    # Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    .prologue
    .line 61
    new-instance v0, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->getTokenizer()Lorg/apache/lucene/analysis/Tokenizer;

    move-result-object v1

    .line 62
    new-instance v2, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;

    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->getTokenStream()Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountAnalyzer;->maxTokenCount:I

    iget-boolean v5, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountAnalyzer;->consumeAllTokens:Z

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;IZ)V

    .line 61
    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v0
.end method
