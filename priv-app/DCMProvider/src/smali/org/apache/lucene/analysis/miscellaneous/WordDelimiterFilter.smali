.class public final Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "WordDelimiterFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;
    }
.end annotation


# static fields
.field public static final ALPHA:I = 0x3

.field public static final ALPHANUM:I = 0x7

.field public static final CATENATE_ALL:I = 0x10

.field public static final CATENATE_NUMBERS:I = 0x8

.field public static final CATENATE_WORDS:I = 0x4

.field public static final DIGIT:I = 0x4

.field public static final GENERATE_NUMBER_PARTS:I = 0x2

.field public static final GENERATE_WORD_PARTS:I = 0x1

.field public static final LOWER:I = 0x1

.field public static final PRESERVE_ORIGINAL:I = 0x20

.field public static final SPLIT_ON_CASE_CHANGE:I = 0x40

.field public static final SPLIT_ON_NUMERICS:I = 0x80

.field public static final STEM_ENGLISH_POSSESSIVE:I = 0x100

.field public static final SUBWORD_DELIM:I = 0x8

.field public static final UPPER:I = 0x2


# instance fields
.field private accumPosInc:I

.field private final concat:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

.field private final concatAll:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

.field private final flags:I

.field private hasIllegalOffsets:Z

.field private hasOutputFollowingOriginal:Z

.field private hasOutputToken:Z

.field private hasSavedState:Z

.field private final iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

.field private lastConcatCount:I

.field private final offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field final protWords:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private savedBuffer:[C

.field private savedEndOffset:I

.field private savedStartOffset:I

.field private savedType:Ljava/lang/String;

.field private final termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;ILorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "configurationFlags"    # I
    .param p3, "protWords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 202
    sget-object v0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->DEFAULT_WORD_DELIM_TABLE:[B

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;[BILorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 203
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;[BILorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "charTypeTable"    # [B
    .param p3, "configurationFlags"    # I
    .param p4, "protWords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    const/4 v1, 0x0

    .line 186
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 143
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 144
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 145
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 146
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 152
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-direct {v0, p0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;-><init>(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concat:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    .line 154
    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->lastConcatCount:I

    .line 157
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-direct {v0, p0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;-><init>(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concatAll:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    .line 160
    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    .line 162
    const/16 v0, 0x400

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedBuffer:[C

    .line 166
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasSavedState:Z

    .line 169
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasIllegalOffsets:Z

    .line 172
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasOutputToken:Z

    .line 175
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasOutputFollowingOriginal:Z

    .line 187
    iput p3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->flags:I

    .line 188
    iput-object p4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->protWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 189
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    .line 190
    const/16 v1, 0x40

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->has(I)Z

    move-result v1

    const/16 v2, 0x80

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->has(I)Z

    move-result v2

    const/16 v3, 0x100

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->has(I)Z

    move-result v3

    invoke-direct {v0, p2, v1, v2, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;-><init>([BZZZ)V

    .line 189
    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    .line 191
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasIllegalOffsets:Z

    return v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedStartOffset:I

    return v0
.end method

.method static synthetic access$4(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedEndOffset:I

    return v0
.end method

.method static synthetic access$5(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    return-object v0
.end method

.method static synthetic access$6(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;Z)I
    .locals 1

    .prologue
    .line 433
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->position(Z)I

    move-result v0

    return v0
.end method

.method static synthetic access$7(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    return-object v0
.end method

.method static synthetic access$8(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;I)V
    .locals 0

    .prologue
    .line 160
    iput p1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    return-void
.end method

.method private concatenate(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;)V
    .locals 4
    .param p1, "concatenation"    # Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    .prologue
    .line 393
    invoke-virtual {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedStartOffset:I

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v1, v1, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    add-int/2addr v0, v1

    iput v0, p1, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->startOffset:I

    .line 396
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedBuffer:[C

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v1, v1, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v2, v2, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v3, v3, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    sub-int/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->append([CII)V

    .line 397
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedStartOffset:I

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v1, v1, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    add-int/2addr v0, v1

    iput v0, p1, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->endOffset:I

    .line 398
    return-void
.end method

.method private flushConcatenation(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;)Z
    .locals 2
    .param p1, "concatenation"    # Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    .prologue
    const/4 v0, 0x1

    .line 358
    iget v1, p1, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->subwordCount:I

    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->lastConcatCount:I

    .line 359
    iget v1, p1, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->subwordCount:I

    if-ne v1, v0, :cond_0

    iget v1, p1, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->type:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->shouldGenerateParts(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 360
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->writeAndClear()V

    .line 364
    :goto_0
    return v0

    .line 363
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->clear()V

    .line 364
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private generatePart(Z)V
    .locals 7
    .param p1, "isSingleWord"    # Z

    .prologue
    .line 406
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->clearAttributes()V

    .line 407
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedBuffer:[C

    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v4, v4, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    iget-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v5, v5, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v6, v6, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    sub-int/2addr v5, v6

    invoke-interface {v2, v3, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 409
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedStartOffset:I

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v3, v3, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    add-int v1, v2, v3

    .line 410
    .local v1, "startOffset":I
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedStartOffset:I

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v3, v3, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    add-int v0, v2, v3

    .line 412
    .local v0, "endOffset":I
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasIllegalOffsets:Z

    if-eqz v2, :cond_1

    .line 415
    if-eqz p1, :cond_0

    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedEndOffset:I

    if-gt v1, v2, :cond_0

    .line 416
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedEndOffset:I

    invoke-interface {v2, v1, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 423
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->position(Z)I

    move-result v3

    invoke-interface {v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 424
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedType:Ljava/lang/String;

    invoke-interface {v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 425
    return-void

    .line 418
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedStartOffset:I

    iget v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedEndOffset:I

    invoke-interface {v2, v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    goto :goto_0

    .line 421
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v2, v1, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    goto :goto_0
.end method

.method private has(I)Z
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 500
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->flags:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isAlpha(I)Z
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 460
    and-int/lit8 v0, p0, 0x3

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isDigit(I)Z
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 470
    and-int/lit8 v0, p0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isSubwordDelim(I)Z
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 480
    and-int/lit8 v0, p0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isUpper(I)Z
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 490
    and-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private position(Z)I
    .locals 4
    .param p1, "inject"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 434
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    .line 436
    .local v0, "posInc":I
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasOutputToken:Z

    if-eqz v2, :cond_1

    .line 437
    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    .line 438
    if-eqz p1, :cond_0

    .line 450
    :goto_0
    return v1

    .line 438
    :cond_0
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    .line 441
    :cond_1
    iput-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasOutputToken:Z

    .line 443
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasOutputFollowingOriginal:Z

    if-nez v2, :cond_2

    .line 445
    iput-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasOutputFollowingOriginal:Z

    goto :goto_0

    .line 449
    :cond_2
    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    .line 450
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0
.end method

.method private saveState()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 335
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedStartOffset:I

    .line 336
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedEndOffset:I

    .line 338
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedEndOffset:I

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedStartOffset:I

    sub-int/2addr v0, v3

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasIllegalOffsets:Z

    .line 339
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->type()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedType:Ljava/lang/String;

    .line 341
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedBuffer:[C

    array-length v0, v0

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 342
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v0

    const/4 v3, 0x2

    invoke-static {v0, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedBuffer:[C

    .line 345
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedBuffer:[C

    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v4

    invoke-static {v0, v2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 346
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedBuffer:[C

    iput-object v2, v0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    .line 348
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasSavedState:Z

    .line 349
    return-void

    :cond_1
    move v0, v2

    .line 338
    goto :goto_0
.end method

.method private shouldConcatenate(I)Z
    .locals 1
    .param p1, "wordType"    # I

    .prologue
    .line 374
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isAlpha(I)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isDigit(I)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private shouldGenerateParts(I)Z
    .locals 2
    .param p1, "wordType"    # I

    .prologue
    const/4 v0, 0x1

    .line 384
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isAlpha(I)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->has(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isDigit(I)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    :cond_2
    return v0
.end method


# virtual methods
.method public incrementToken()Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, -0x1

    const/16 v7, 0x20

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 208
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasSavedState:Z

    if-nez v3, :cond_8

    .line 210
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v3

    if-nez v3, :cond_2

    move v5, v4

    .line 309
    :cond_1
    :goto_1
    return v5

    .line 214
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v1

    .line 215
    .local v1, "termLength":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 217
    .local v0, "termBuffer":[C
    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v6

    add-int/2addr v3, v6

    iput v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    .line 219
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    invoke-virtual {v3, v0, v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->setText([CI)V

    .line 220
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->next()I

    .line 223
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v3, v3, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    if-nez v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v3, v3, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    if-eq v3, v1, :cond_4

    .line 224
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->protWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->protWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3, v0, v4, v1}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 225
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iget v6, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    invoke-interface {v3, v6}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 226
    iput v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    goto :goto_1

    .line 231
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v3, v3, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    if-ne v3, v8, :cond_6

    invoke-direct {p0, v7}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->has(I)Z

    move-result v3

    if-nez v3, :cond_6

    .line 233
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 234
    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    goto :goto_0

    .line 239
    :cond_6
    invoke-direct {p0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->saveState()V

    .line 241
    iput-boolean v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasOutputToken:Z

    .line 242
    invoke-direct {p0, v7}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->has(I)Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v4

    :goto_2
    iput-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasOutputFollowingOriginal:Z

    .line 243
    iput v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->lastConcatCount:I

    .line 245
    invoke-direct {p0, v7}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->has(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 246
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iget v6, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    invoke-interface {v3, v6}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 247
    iput v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    goto :goto_1

    :cond_7
    move v3, v5

    .line 242
    goto :goto_2

    .line 253
    .end local v0    # "termBuffer":[C
    .end local v1    # "termLength":I
    :cond_8
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    iget v3, v3, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    if-ne v3, v8, :cond_c

    .line 254
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concat:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    .line 255
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concat:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->flushConcatenation(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 260
    :cond_9
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concatAll:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_b

    .line 262
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concatAll:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    iget v3, v3, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->subwordCount:I

    iget v6, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->lastConcatCount:I

    if-le v3, v6, :cond_a

    .line 263
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concatAll:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->writeAndClear()V

    goto/16 :goto_1

    .line 266
    :cond_a
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concatAll:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->clear()V

    .line 270
    :cond_b
    iput-boolean v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasSavedState:Z

    goto/16 :goto_0

    .line 275
    :cond_c
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->isSingleWord()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 276
    invoke-direct {p0, v5}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->generatePart(Z)V

    .line 277
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->next()I

    goto/16 :goto_1

    .line 281
    :cond_d
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->type()I

    move-result v2

    .line 284
    .local v2, "wordType":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concat:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_f

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concat:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    iget v3, v3, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->type:I

    and-int/2addr v3, v2

    if-nez v3, :cond_f

    .line 285
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concat:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->flushConcatenation(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 286
    iput-boolean v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasOutputToken:Z

    goto/16 :goto_1

    .line 289
    :cond_e
    iput-boolean v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasOutputToken:Z

    .line 293
    :cond_f
    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->shouldConcatenate(I)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 294
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concat:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 295
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concat:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    iput v2, v3, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->type:I

    .line 297
    :cond_10
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concat:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concatenate(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;)V

    .line 301
    :cond_11
    const/16 v3, 0x10

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->has(I)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 302
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concatAll:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concatenate(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;)V

    .line 306
    :cond_12
    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->shouldGenerateParts(I)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 307
    invoke-direct {p0, v4}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->generatePart(Z)V

    .line 308
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->next()I

    goto/16 :goto_1

    .line 312
    :cond_13
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->iterator:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->next()I

    goto/16 :goto_0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 321
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 322
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasSavedState:Z

    .line 323
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concat:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->clear()V

    .line 324
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->concatAll:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->clear()V

    .line 325
    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->accumPosInc:I

    .line 326
    return-void
.end method
