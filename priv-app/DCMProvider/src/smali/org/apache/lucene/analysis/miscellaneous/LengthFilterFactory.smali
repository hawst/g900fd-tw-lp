.class public Lorg/apache/lucene/analysis/miscellaneous/LengthFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "LengthFilterFactory.java"


# static fields
.field public static final MAX_KEY:Ljava/lang/String; = "max"

.field public static final MIN_KEY:Ljava/lang/String; = "min"


# instance fields
.field final enablePositionIncrements:Z

.field final max:I

.field final min:I


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 45
    const-string v0, "min"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/miscellaneous/LengthFilterFactory;->requireInt(Ljava/util/Map;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilterFactory;->min:I

    .line 46
    const-string v0, "max"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/miscellaneous/LengthFilterFactory;->requireInt(Ljava/util/Map;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilterFactory;->max:I

    .line 47
    const-string v0, "enablePositionIncrements"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/miscellaneous/LengthFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilterFactory;->enablePositionIncrements:Z

    .line 48
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/miscellaneous/LengthFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 55
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilterFactory;->enablePositionIncrements:Z

    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilterFactory;->min:I

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilterFactory;->max:I

    invoke-direct {v0, v1, p1, v2, v3}, Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;-><init>(ZLorg/apache/lucene/analysis/TokenStream;II)V

    return-object v0
.end method
