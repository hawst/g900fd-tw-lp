.class public final Lorg/apache/lucene/analysis/it/ItalianAnalyzer;
.super Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;
.source "ItalianAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/it/ItalianAnalyzer$DefaultSetHolder;
    }
.end annotation


# static fields
.field private static final DEFAULT_ARTICLES:Lorg/apache/lucene/analysis/util/CharArraySet;

.field public static final DEFAULT_STOPWORD_FILE:Ljava/lang/String; = "italian_stop.txt"


# instance fields
.field private final stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 61
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    const/16 v2, 0x15

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 63
    const-string v4, "c"

    aput-object v4, v2, v3

    const-string v3, "l"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "all"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "dall"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "dell"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "nell"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "sull"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "coll"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "pell"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    .line 64
    const-string v4, "gl"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "agl"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "dagl"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "degl"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "negl"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "sugl"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "un"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, "m"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string v4, "t"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string v4, "s"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string v4, "v"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string v4, "d"

    aput-object v4, v2, v3

    .line 62
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 61
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    .line 60
    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->DEFAULT_ARTICLES:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 65
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 98
    sget-object v0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 108
    sget-object v0, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 109
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p3, "stemExclusionSet"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 121
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 122
    invoke-static {p1, p3}, Lorg/apache/lucene/analysis/util/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 124
    return-void
.end method

.method public static getDefaultStopSet()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 141
    new-instance v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 142
    .local v2, "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 143
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_32:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 144
    new-instance v1, Lorg/apache/lucene/analysis/util/ElisionFilter;

    sget-object v3, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->DEFAULT_ARTICLES:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/util/ElisionFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 146
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    new-instance v1, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v3, v0}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 147
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v0, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v3, v1, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 148
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 149
    new-instance v1, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 150
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/it/ItalianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 151
    new-instance v1, Lorg/apache/lucene/analysis/it/ItalianLightStemFilter;

    invoke-direct {v1, v0}, Lorg/apache/lucene/analysis/it/ItalianLightStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 155
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v3, v2, v0}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v3

    .line 153
    :cond_2
    new-instance v1, Lorg/apache/lucene/analysis/snowball/SnowballFilter;

    new-instance v3, Lorg/tartarus/snowball/ext/ItalianStemmer;

    invoke-direct {v3}, Lorg/tartarus/snowball/ext/ItalianStemmer;-><init>()V

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/tartarus/snowball/SnowballProgram;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_0
.end method
