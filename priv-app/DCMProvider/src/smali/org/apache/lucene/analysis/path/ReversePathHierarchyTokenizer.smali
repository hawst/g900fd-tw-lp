.class public Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "ReversePathHierarchyTokenizer.java"


# static fields
.field private static final DEFAULT_BUFFER_SIZE:I = 0x400

.field public static final DEFAULT_DELIMITER:C = '/'

.field public static final DEFAULT_SKIP:I


# instance fields
.field private final delimiter:C

.field private delimiterPositions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private delimitersCount:I

.field private endPosition:I

.field private finalOffset:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final replacement:C

.field private resultToken:Ljava/lang/StringBuilder;

.field private resultTokenBuffer:[C

.field private final skip:I

.field private skipped:I

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    const/16 v3, 0x2f

    .line 51
    const/16 v2, 0x400

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;CC)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "delimiter"    # C
    .param p3, "replacement"    # C

    .prologue
    .line 63
    const/16 v2, 0x400

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 64
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;CCI)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "delimiter"    # C
    .param p3, "replacement"    # C
    .param p4, "skip"    # I

    .prologue
    .line 75
    const/16 v2, 0x400

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 76
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;CI)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "delimiter"    # C
    .param p3, "skip"    # I

    .prologue
    .line 71
    const/16 v2, 0x400

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 72
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;I)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "skip"    # I

    .prologue
    const/16 v3, 0x2f

    .line 55
    const/16 v2, 0x400

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;IC)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "bufferSize"    # I
    .param p3, "delimiter"    # C

    .prologue
    .line 59
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;ICC)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "bufferSize"    # I
    .param p3, "delimiter"    # C
    .param p4, "replacement"    # C

    .prologue
    .line 67
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;ICCI)V
    .locals 7
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "bufferSize"    # I
    .param p3, "delimiter"    # C
    .param p4, "replacement"    # C
    .param p5, "skip"    # I

    .prologue
    .line 84
    sget-object v1, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;ICCI)V

    .line 85
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;CCI)V
    .locals 7
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .param p3, "delimiter"    # C
    .param p4, "replacement"    # C
    .param p5, "skip"    # I

    .prologue
    .line 80
    const/16 v3, 0x400

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;ICCI)V

    .line 81
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;ICCI)V
    .locals 2
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .param p3, "bufferSize"    # I
    .param p4, "delimiter"    # C
    .param p5, "replacement"    # C
    .param p6, "skip"    # I

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 112
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 113
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 114
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->posAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 116
    iput v1, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->endPosition:I

    .line 117
    iput v1, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->finalOffset:I

    .line 118
    iput v1, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->skipped:I

    .line 122
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimitersCount:I

    .line 89
    if-gez p3, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bufferSize cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    if-gez p6, :cond_1

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "skip cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0, p3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    .line 96
    iput-char p4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimiter:C

    .line 97
    iput-char p5, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->replacement:C

    .line 98
    iput p6, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->skip:I

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p3}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    .line 100
    new-array v0, p3, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultTokenBuffer:[C

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    div-int/lit8 v1, p3, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimiterPositions:Ljava/util/List;

    .line 102
    return-void
.end method


# virtual methods
.method public final end()V
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v1, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->finalOffset:I

    iget v2, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->finalOffset:I

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 182
    return-void
.end method

.method public final incrementToken()Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 127
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->clearAttributes()V

    .line 128
    iget v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimitersCount:I

    const/4 v7, -0x1

    if-ne v4, v7, :cond_5

    .line 129
    const/4 v2, 0x0

    .line 130
    .local v2, "length":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimiterPositions:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->input:Ljava/io/Reader;

    invoke-virtual {v4}, Ljava/io/Reader;->read()I

    move-result v0

    .line 133
    .local v0, "c":I
    if-gez v0, :cond_3

    .line 145
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimiterPositions:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimitersCount:I

    .line 146
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimiterPositions:Ljava/util/List;

    iget v7, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimitersCount:I

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ge v4, v2, :cond_0

    .line 147
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimiterPositions:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    iget v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimitersCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimitersCount:I

    .line 150
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultTokenBuffer:[C

    array-length v4, v4

    iget-object v7, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-ge v4, v7, :cond_1

    .line 151
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    new-array v4, v4, [C

    iput-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultTokenBuffer:[C

    .line 153
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    iget-object v7, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    iget-object v8, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultTokenBuffer:[C

    invoke-virtual {v4, v6, v7, v8, v6}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    .line 154
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 155
    iget v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimitersCount:I

    add-int/lit8 v4, v4, -0x1

    iget v7, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->skip:I

    sub-int v1, v4, v7

    .line 156
    .local v1, "idx":I
    if-ltz v1, :cond_2

    .line 158
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimiterPositions:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->endPosition:I

    .line 160
    :cond_2
    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->correctOffset(I)I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->finalOffset:I

    .line 161
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->posAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 167
    .end local v0    # "c":I
    .end local v1    # "idx":I
    .end local v2    # "length":I
    :goto_1
    iget v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->skipped:I

    iget v7, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimitersCount:I

    iget v8, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->skip:I

    sub-int/2addr v7, v8

    add-int/lit8 v7, v7, -0x1

    if-ge v4, v7, :cond_6

    .line 168
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimiterPositions:Ljava/util/List;

    iget v6, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->skipped:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 169
    .local v3, "start":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v6, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultTokenBuffer:[C

    iget v7, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->endPosition:I

    sub-int/2addr v7, v3

    invoke-interface {v4, v6, v3, v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 170
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->correctOffset(I)I

    move-result v6

    iget v7, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->endPosition:I

    invoke-virtual {p0, v7}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->correctOffset(I)I

    move-result v7

    invoke-interface {v4, v6, v7}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 171
    iget v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->skipped:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->skipped:I

    move v4, v5

    .line 175
    .end local v3    # "start":I
    :goto_2
    return v4

    .line 136
    .restart local v0    # "c":I
    .restart local v2    # "length":I
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 137
    iget-char v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimiter:C

    if-ne v0, v4, :cond_4

    .line 138
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimiterPositions:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    iget-char v7, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->replacement:C

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 142
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    int-to-char v7, v0

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 164
    .end local v0    # "c":I
    .end local v2    # "length":I
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->posAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v4, v6}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    goto :goto_1

    :cond_6
    move v4, v6

    .line 175
    goto :goto_2
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 186
    invoke-super {p0}, Lorg/apache/lucene/analysis/Tokenizer;->reset()V

    .line 187
    iget-object v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 188
    iput v1, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->finalOffset:I

    .line 189
    iput v1, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->endPosition:I

    .line 190
    iput v1, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->skipped:I

    .line 191
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimitersCount:I

    .line 192
    iget-object v0, p0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;->delimiterPositions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 193
    return-void
.end method
