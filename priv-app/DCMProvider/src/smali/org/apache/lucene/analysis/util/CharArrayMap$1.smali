.class Lorg/apache/lucene/analysis/util/CharArrayMap$1;
.super Lorg/apache/lucene/analysis/util/CharArraySet;
.source "CharArrayMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/analysis/util/CharArrayMap;->keySet()Lorg/apache/lucene/analysis/util/CharArraySet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/analysis/util/CharArrayMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/analysis/util/CharArrayMap;Lorg/apache/lucene/analysis/util/CharArrayMap;)V
    .locals 0

    .prologue
    .line 1
    .local p2, "$anonymous0":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<Ljava/lang/Object;>;"
    iput-object p1, p0, Lorg/apache/lucene/analysis/util/CharArrayMap$1;->this$0:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 382
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 389
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 385
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public add(Ljava/lang/String;)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 393
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public add([C)Z
    .locals 1
    .param p1, "text"    # [C

    .prologue
    .line 397
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
